.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WeatherResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private current_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

.field private dailyForecast_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;",
            ">;"
        }
    .end annotation
.end field

.field private forecastStartDate_:Ljava/lang/String;

.field private hasCurrent:Z

.field private hasForecastStartDate:Z

.field private hasHourlyForecast:Z

.field private hasInMetricUnits:Z

.field private hasLocation:Z

.field private hourlyForecast_:Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

.field private inMetricUnits_:Z

.field private location_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->location_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->current_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->forecastStartDate_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hourlyForecast_:Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->inMetricUnits_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-direct {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    return-object v0
.end method


# virtual methods
.method public addDailyForecast(Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->cachedSize:I

    return v0
.end method

.method public getCurrent()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->current_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    return-object v0
.end method

.method public getDailyForecast(I)Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    return-object v0
.end method

.method public getDailyForecastCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDailyForecastList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->dailyForecast_:Ljava/util/List;

    return-object v0
.end method

.method public getForecastStartDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->forecastStartDate_:Ljava/lang/String;

    return-object v0
.end method

.method public getHourlyForecast()Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hourlyForecast_:Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    return-object v0
.end method

.method public getInMetricUnits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->inMetricUnits_:Z

    return v0
.end method

.method public getLocation()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->location_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getLocation()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasCurrent()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getCurrent()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasForecastStartDate()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getForecastStartDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getDailyForecastList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasHourlyForecast()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getHourlyForecast()Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasInMetricUnits()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getInMetricUnits()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->cachedSize:I

    return v2
.end method

.method public hasCurrent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasCurrent:Z

    return v0
.end method

.method public hasForecastStartDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasForecastStartDate:Z

    return v0
.end method

.method public hasHourlyForecast()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasHourlyForecast:Z

    return v0
.end method

.method public hasInMetricUnits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasInMetricUnits:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasLocation:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->setLocation(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->setCurrent(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->setForecastStartDate(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->addDailyForecast(Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->setHourlyForecast(Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->setInMetricUnits(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    move-result-object v0

    return-object v0
.end method

.method public setCurrent(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasCurrent:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->current_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    return-object p0
.end method

.method public setForecastStartDate(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasForecastStartDate:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->forecastStartDate_:Ljava/lang/String;

    return-object p0
.end method

.method public setHourlyForecast(Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasHourlyForecast:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hourlyForecast_:Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    return-object p0
.end method

.method public setInMetricUnits(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasInMetricUnits:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->inMetricUnits_:Z

    return-object p0
.end method

.method public setLocation(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasLocation:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->location_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getLocation()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherLocation;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasCurrent()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getCurrent()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherState;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasForecastStartDate()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getForecastStartDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getDailyForecastList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$DailyForecast;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasHourlyForecast()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getHourlyForecast()Lcom/google/majel/proto/EcoutezStructuredResponse$HourlyForecast;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->hasInMetricUnits()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->getInMetricUnits()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    return-void
.end method
