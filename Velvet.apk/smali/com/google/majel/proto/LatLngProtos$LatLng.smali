.class public final Lcom/google/majel/proto/LatLngProtos$LatLng;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LatLngProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/LatLngProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatLng"
.end annotation


# instance fields
.field private accuracyMeters_:F

.field private cachedSize:I

.field private hasAccuracyMeters:Z

.field private hasLabel:Z

.field private hasLatDegrees:Z

.field private hasLngDegrees:Z

.field private label_:Ljava/lang/String;

.field private latDegrees_:F

.field private lngDegrees_:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->latDegrees_:F

    iput v1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->lngDegrees_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->label_:Ljava/lang/String;

    iput v1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->accuracyMeters_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAccuracyMeters()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->accuracyMeters_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->cachedSize:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatDegrees()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->latDegrees_:F

    return v0
.end method

.method public getLngDegrees()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->lngDegrees_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLatDegrees()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLatDegrees()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLngDegrees()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLngDegrees()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLabel()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasAccuracyMeters()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getAccuracyMeters()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->cachedSize:I

    return v0
.end method

.method public hasAccuracyMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasAccuracyMeters:Z

    return v0
.end method

.method public hasLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLabel:Z

    return v0
.end method

.method public hasLatDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLatDegrees:Z

    return v0
.end method

.method public hasLngDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLngDegrees:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/LatLngProtos$LatLng;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/LatLngProtos$LatLng;->setLatDegrees(F)Lcom/google/majel/proto/LatLngProtos$LatLng;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/LatLngProtos$LatLng;->setLngDegrees(F)Lcom/google/majel/proto/LatLngProtos$LatLng;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/LatLngProtos$LatLng;->setLabel(Ljava/lang/String;)Lcom/google/majel/proto/LatLngProtos$LatLng;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/LatLngProtos$LatLng;->setAccuracyMeters(F)Lcom/google/majel/proto/LatLngProtos$LatLng;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/LatLngProtos$LatLng;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/LatLngProtos$LatLng;

    move-result-object v0

    return-object v0
.end method

.method public setAccuracyMeters(F)Lcom/google/majel/proto/LatLngProtos$LatLng;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasAccuracyMeters:Z

    iput p1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->accuracyMeters_:F

    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/majel/proto/LatLngProtos$LatLng;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLabel:Z

    iput-object p1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->label_:Ljava/lang/String;

    return-object p0
.end method

.method public setLatDegrees(F)Lcom/google/majel/proto/LatLngProtos$LatLng;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLatDegrees:Z

    iput p1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->latDegrees_:F

    return-object p0
.end method

.method public setLngDegrees(F)Lcom/google/majel/proto/LatLngProtos$LatLng;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLngDegrees:Z

    iput p1, p0, Lcom/google/majel/proto/LatLngProtos$LatLng;->lngDegrees_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLatDegrees()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLatDegrees()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLngDegrees()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLngDegrees()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->hasAccuracyMeters()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/LatLngProtos$LatLng;->getAccuracyMeters()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    return-void
.end method
