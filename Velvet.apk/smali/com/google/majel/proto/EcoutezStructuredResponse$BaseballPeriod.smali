.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BaseballPeriod"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasInningStatus:Z

.field private hasNumOfBalls:Z

.field private hasNumOfOuts:Z

.field private hasNumOfStrikes:Z

.field private inningStatus_:I

.field private numOfBalls_:I

.field private numOfOuts_:I

.field private numOfStrikes_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->inningStatus_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfOuts_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfStrikes_:I

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfBalls_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->cachedSize:I

    return v0
.end method

.method public getInningStatus()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->inningStatus_:I

    return v0
.end method

.method public getNumOfBalls()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfBalls_:I

    return v0
.end method

.method public getNumOfOuts()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfOuts_:I

    return v0
.end method

.method public getNumOfStrikes()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfStrikes_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasInningStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getInningStatus()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfOuts()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfOuts()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfStrikes()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfStrikes()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfBalls()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfBalls()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->cachedSize:I

    return v0
.end method

.method public hasInningStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasInningStatus:Z

    return v0
.end method

.method public hasNumOfBalls()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfBalls:Z

    return v0
.end method

.method public hasNumOfOuts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfOuts:Z

    return v0
.end method

.method public hasNumOfStrikes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfStrikes:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->setInningStatus(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->setNumOfOuts(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->setNumOfStrikes(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->setNumOfBalls(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;

    move-result-object v0

    return-object v0
.end method

.method public setInningStatus(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasInningStatus:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->inningStatus_:I

    return-object p0
.end method

.method public setNumOfBalls(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfBalls:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfBalls_:I

    return-object p0
.end method

.method public setNumOfOuts(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfOuts:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfOuts_:I

    return-object p0
.end method

.method public setNumOfStrikes(I)Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfStrikes:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->numOfStrikes_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasInningStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getInningStatus()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfOuts()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfOuts()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfStrikes()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfStrikes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->hasNumOfBalls()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballPeriod;->getNumOfBalls()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
