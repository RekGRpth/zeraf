.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Match"
.end annotation


# instance fields
.field private baseball_:Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

.field private boxUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private currentPeriodNum_:I

.field private dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

.field private dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

.field private firstIsWinner_:Z

.field private firstScore_:Ljava/lang/String;

.field private firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

.field private firstTeamShortName_:Ljava/lang/String;

.field private firstTeam_:Ljava/lang/String;

.field private forumUrl_:Ljava/lang/String;

.field private hasBaseball:Z

.field private hasBoxUrl:Z

.field private hasCurrentPeriodNum:Z

.field private hasDEPRECATEDFirstTeamLogoUrl:Z

.field private hasDEPRECATEDSecondTeamLogoUrl:Z

.field private hasFirstIsWinner:Z

.field private hasFirstScore:Z

.field private hasFirstTeam:Z

.field private hasFirstTeamLogo:Z

.field private hasFirstTeamShortName:Z

.field private hasForumUrl:Z

.field private hasIsHidden:Z

.field private hasIsHiddenSecondary:Z

.field private hasLiveStreamUrl:Z

.field private hasLiveUpdateUrl:Z

.field private hasPreviewUrl:Z

.field private hasRecapUrl:Z

.field private hasSecondIsWinner:Z

.field private hasSecondScore:Z

.field private hasSecondTeam:Z

.field private hasSecondTeamLogo:Z

.field private hasSecondTeamShortName:Z

.field private hasStartTime:Z

.field private hasStartTimestamp:Z

.field private hasStatus:Z

.field private hasTicketsUrl:Z

.field private isHiddenSecondary_:Z

.field private isHidden_:Z

.field private liveStreamUrl_:Ljava/lang/String;

.field private liveUpdateUrl_:Ljava/lang/String;

.field private period_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Period;",
            ">;"
        }
    .end annotation
.end field

.field private previewUrl_:Ljava/lang/String;

.field private recapUrl_:Ljava/lang/String;

.field private secondIsWinner_:Z

.field private secondScore_:Ljava/lang/String;

.field private secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

.field private secondTeamShortName_:Ljava/lang/String;

.field private secondTeam_:Ljava/lang/String;

.field private startTime_:Ljava/lang/String;

.field private startTimestamp_:J

.field private status_:I

.field private ticketsUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->status_:I

    iput-boolean v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHidden_:Z

    iput-boolean v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHiddenSecondary_:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTimestamp_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTime_:Ljava/lang/String;

    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->currentPeriodNum_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamShortName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstScore_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstIsWinner_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamShortName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondScore_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondIsWinner_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->recapUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->boxUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveUpdateUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveStreamUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->previewUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->ticketsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->forumUrl_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    iput-object v3, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->baseball_:Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPeriod(Lcom/google/majel/proto/EcoutezStructuredResponse$Period;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBaseball()Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->baseball_:Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    return-object v0
.end method

.method public getBoxUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->boxUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->cachedSize:I

    return v0
.end method

.method public getCurrentPeriodNum()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->currentPeriodNum_:I

    return v0
.end method

.method public getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstIsWinner_:Z

    return v0
.end method

.method public getFirstScore()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstScore_:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstTeam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeam_:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object v0
.end method

.method public getFirstTeamShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamShortName_:Ljava/lang/String;

    return-object v0
.end method

.method public getForumUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->forumUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHidden_:Z

    return v0
.end method

.method public getIsHiddenSecondary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHiddenSecondary_:Z

    return v0
.end method

.method public getLiveStreamUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveStreamUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLiveUpdateUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveUpdateUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPeriod(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Period;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    return-object v0
.end method

.method public getPeriodCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPeriodList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Period;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->period_:Ljava/util/List;

    return-object v0
.end method

.method public getPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->previewUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecapUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->recapUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondIsWinner_:Z

    return v0
.end method

.method public getSecondScore()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondScore_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondTeam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeam_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object v0
.end method

.method public getSecondTeamShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamShortName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStatus()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStatus()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHidden()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getIsHidden()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHiddenSecondary()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getIsHiddenSecondary()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTimestamp()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStartTimestamp()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTime()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStartTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasCurrentPeriodNum()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getCurrentPeriodNum()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeam()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeam()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstScore()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstScore()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstIsWinner()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstIsWinner()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeam()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeam()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondScore()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondScore()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondIsWinner()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondIsWinner()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasRecapUrl()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getRecapUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBoxUrl()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBoxUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasPreviewUrl()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPreviewUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasTicketsUrl()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getTicketsUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    const/16 v3, 0x11

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBaseball()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBaseball()Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDFirstTeamLogoUrl()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDSecondTeamLogoUrl()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamLogo()Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_14
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamLogo()Z

    move-result v3

    if-eqz v3, :cond_15

    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_15
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamShortName()Z

    move-result v3

    if-eqz v3, :cond_16

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamShortName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_16
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamShortName()Z

    move-result v3

    if-eqz v3, :cond_17

    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamShortName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_17
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveUpdateUrl()Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v3, 0x19

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_18
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveStreamUrl()Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v3, 0x1a

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getLiveStreamUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_19
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasForumUrl()Z

    move-result v3

    if-eqz v3, :cond_1a

    const/16 v3, 0x1b

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getForumUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1a
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->cachedSize:I

    return v2
.end method

.method public getStartTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTime_:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTimestamp_:J

    return-wide v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->status_:I

    return v0
.end method

.method public getTicketsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->ticketsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBaseball()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBaseball:Z

    return v0
.end method

.method public hasBoxUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBoxUrl:Z

    return v0
.end method

.method public hasCurrentPeriodNum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasCurrentPeriodNum:Z

    return v0
.end method

.method public hasDEPRECATEDFirstTeamLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDFirstTeamLogoUrl:Z

    return v0
.end method

.method public hasDEPRECATEDSecondTeamLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDSecondTeamLogoUrl:Z

    return v0
.end method

.method public hasFirstIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstIsWinner:Z

    return v0
.end method

.method public hasFirstScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstScore:Z

    return v0
.end method

.method public hasFirstTeam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeam:Z

    return v0
.end method

.method public hasFirstTeamLogo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamLogo:Z

    return v0
.end method

.method public hasFirstTeamShortName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamShortName:Z

    return v0
.end method

.method public hasForumUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasForumUrl:Z

    return v0
.end method

.method public hasIsHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHidden:Z

    return v0
.end method

.method public hasIsHiddenSecondary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHiddenSecondary:Z

    return v0
.end method

.method public hasLiveStreamUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveStreamUrl:Z

    return v0
.end method

.method public hasLiveUpdateUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveUpdateUrl:Z

    return v0
.end method

.method public hasPreviewUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasPreviewUrl:Z

    return v0
.end method

.method public hasRecapUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasRecapUrl:Z

    return v0
.end method

.method public hasSecondIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondIsWinner:Z

    return v0
.end method

.method public hasSecondScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondScore:Z

    return v0
.end method

.method public hasSecondTeam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeam:Z

    return v0
.end method

.method public hasSecondTeamLogo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamLogo:Z

    return v0
.end method

.method public hasSecondTeamShortName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamShortName:Z

    return v0
.end method

.method public hasStartTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTime:Z

    return v0
.end method

.method public hasStartTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTimestamp:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStatus:Z

    return v0
.end method

.method public hasTicketsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasTicketsUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setStatus(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setIsHidden(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setIsHiddenSecondary(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setStartTimestamp(J)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setStartTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setCurrentPeriodNum(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setFirstTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setFirstScore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setFirstIsWinner(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setSecondTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setSecondScore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setSecondIsWinner(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setRecapUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setBoxUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setPreviewUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setTicketsUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->addPeriod(Lcom/google/majel/proto/EcoutezStructuredResponse$Period;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setBaseball(Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setDEPRECATEDFirstTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setDEPRECATEDSecondTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setFirstTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setSecondTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setSecondTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setFirstTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setLiveUpdateUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setLiveStreamUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->setForumUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v0

    return-object v0
.end method

.method public setBaseball(Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBaseball:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->baseball_:Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    return-object p0
.end method

.method public setBoxUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBoxUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->boxUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setCurrentPeriodNum(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasCurrentPeriodNum:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->currentPeriodNum_:I

    return-object p0
.end method

.method public setDEPRECATEDFirstTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDFirstTeamLogoUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDSecondTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDSecondTeamLogoUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstIsWinner(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstIsWinner:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstIsWinner_:Z

    return-object p0
.end method

.method public setFirstScore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstScore:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstScore_:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeam:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeam_:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamLogo:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object p0
.end method

.method public setFirstTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamShortName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->firstTeamShortName_:Ljava/lang/String;

    return-object p0
.end method

.method public setForumUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasForumUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->forumUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsHidden(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHidden:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHidden_:Z

    return-object p0
.end method

.method public setIsHiddenSecondary(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHiddenSecondary:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->isHiddenSecondary_:Z

    return-object p0
.end method

.method public setLiveStreamUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveStreamUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveStreamUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLiveUpdateUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveUpdateUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->liveUpdateUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setPreviewUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasPreviewUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->previewUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecapUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasRecapUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->recapUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSecondIsWinner(Z)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondIsWinner:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondIsWinner_:Z

    return-object p0
.end method

.method public setSecondScore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondScore:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondScore_:Ljava/lang/String;

    return-object p0
.end method

.method public setSecondTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeam:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeam_:Ljava/lang/String;

    return-object p0
.end method

.method public setSecondTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamLogo:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object p0
.end method

.method public setSecondTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamShortName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->secondTeamShortName_:Ljava/lang/String;

    return-object p0
.end method

.method public setStartTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTime_:Ljava/lang/String;

    return-object p0
.end method

.method public setStartTimestamp(J)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTimestamp:Z

    iput-wide p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->startTimestamp_:J

    return-object p0
.end method

.method public setStatus(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStatus:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->status_:I

    return-object p0
.end method

.method public setTicketsUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasTicketsUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->ticketsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStatus()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHidden()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getIsHidden()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasIsHiddenSecondary()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getIsHiddenSecondary()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTimestamp()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStartTimestamp()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTime()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStartTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasCurrentPeriodNum()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getCurrentPeriodNum()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeam()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstScore()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstScore()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstIsWinner()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstIsWinner()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeam()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondScore()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondScore()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondIsWinner()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondIsWinner()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasRecapUrl()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getRecapUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBoxUrl()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBoxUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasPreviewUrl()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPreviewUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasTicketsUrl()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getTicketsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    const/16 v2, 0x11

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBaseball()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBaseball()Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDFirstTeamLogoUrl()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasDEPRECATEDSecondTeamLogoUrl()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamLogo()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamLogo()Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamShortName()Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamShortName()Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x18

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveUpdateUrl()Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveStreamUrl()Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x1a

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getLiveStreamUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasForumUrl()Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x1b

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getForumUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1a
    return-void
.end method
