.class public final Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionDateTimeProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionDateTimeProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionTime"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHour:Z

.field private hasMinute:Z

.field private hasSecond:Z

.field private hour_:I

.field private minute_:I

.field private second_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hour_:I

    iput v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->minute_:I

    iput v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->second_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->cachedSize:I

    return v0
.end method

.method public getHour()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hour_:I

    return v0
.end method

.method public getMinute()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->minute_:I

    return v0
.end method

.method public getSecond()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->second_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasHour()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getHour()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasMinute()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getMinute()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasSecond()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getSecond()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->cachedSize:I

    return v0
.end method

.method public hasHour()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasHour:Z

    return v0
.end method

.method public hasMinute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasMinute:Z

    return v0
.end method

.method public hasSecond()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasSecond:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setHour(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setMinute(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->setSecond(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v0

    return-object v0
.end method

.method public setHour(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasHour:Z

    iput p1, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hour_:I

    return-object p0
.end method

.method public setMinute(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasMinute:Z

    iput p1, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->minute_:I

    return-object p0
.end method

.method public setSecond(I)Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasSecond:Z

    iput p1, p0, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->second_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasHour()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getHour()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasMinute()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getMinute()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasSecond()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getSecond()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
