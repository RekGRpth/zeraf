.class public final Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GstaticConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/GstaticConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EndpointerParams"
.end annotation


# instance fields
.field private cachedSize:I

.field private completeSilenceMsec_:I

.field private extraSilenceAfterEndOfSpeechMsec_:I

.field private hasCompleteSilenceMsec:Z

.field private hasExtraSilenceAfterEndOfSpeechMsec:Z

.field private hasNoSpeechDetectedTimeoutMsec:Z

.field private hasPossiblyCompleteSilenceMsec:Z

.field private hasSpeechMinimumLengthMsec:Z

.field private noSpeechDetectedTimeoutMsec_:I

.field private possiblyCompleteSilenceMsec_:I

.field private speechMinimumLengthMsec_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->completeSilenceMsec_:I

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->possiblyCompleteSilenceMsec_:I

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->speechMinimumLengthMsec_:I

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->noSpeechDetectedTimeoutMsec_:I

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->extraSilenceAfterEndOfSpeechMsec_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->cachedSize:I

    return v0
.end method

.method public getCompleteSilenceMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->completeSilenceMsec_:I

    return v0
.end method

.method public getExtraSilenceAfterEndOfSpeechMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->extraSilenceAfterEndOfSpeechMsec_:I

    return v0
.end method

.method public getNoSpeechDetectedTimeoutMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->noSpeechDetectedTimeoutMsec_:I

    return v0
.end method

.method public getPossiblyCompleteSilenceMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->possiblyCompleteSilenceMsec_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasCompleteSilenceMsec()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getCompleteSilenceMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasPossiblyCompleteSilenceMsec()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getPossiblyCompleteSilenceMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasSpeechMinimumLengthMsec()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getSpeechMinimumLengthMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasNoSpeechDetectedTimeoutMsec()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getNoSpeechDetectedTimeoutMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasExtraSilenceAfterEndOfSpeechMsec()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getExtraSilenceAfterEndOfSpeechMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->cachedSize:I

    return v0
.end method

.method public getSpeechMinimumLengthMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->speechMinimumLengthMsec_:I

    return v0
.end method

.method public hasCompleteSilenceMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasCompleteSilenceMsec:Z

    return v0
.end method

.method public hasExtraSilenceAfterEndOfSpeechMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasExtraSilenceAfterEndOfSpeechMsec:Z

    return v0
.end method

.method public hasNoSpeechDetectedTimeoutMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasNoSpeechDetectedTimeoutMsec:Z

    return v0
.end method

.method public hasPossiblyCompleteSilenceMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasPossiblyCompleteSilenceMsec:Z

    return v0
.end method

.method public hasSpeechMinimumLengthMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasSpeechMinimumLengthMsec:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setCompleteSilenceMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setPossiblyCompleteSilenceMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setSpeechMinimumLengthMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setNoSpeechDetectedTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setExtraSilenceAfterEndOfSpeechMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public setCompleteSilenceMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasCompleteSilenceMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->completeSilenceMsec_:I

    return-object p0
.end method

.method public setExtraSilenceAfterEndOfSpeechMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasExtraSilenceAfterEndOfSpeechMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->extraSilenceAfterEndOfSpeechMsec_:I

    return-object p0
.end method

.method public setNoSpeechDetectedTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasNoSpeechDetectedTimeoutMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->noSpeechDetectedTimeoutMsec_:I

    return-object p0
.end method

.method public setPossiblyCompleteSilenceMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasPossiblyCompleteSilenceMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->possiblyCompleteSilenceMsec_:I

    return-object p0
.end method

.method public setSpeechMinimumLengthMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasSpeechMinimumLengthMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->speechMinimumLengthMsec_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasCompleteSilenceMsec()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getCompleteSilenceMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasPossiblyCompleteSilenceMsec()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getPossiblyCompleteSilenceMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasSpeechMinimumLengthMsec()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getSpeechMinimumLengthMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasNoSpeechDetectedTimeoutMsec()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getNoSpeechDetectedTimeoutMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->hasExtraSilenceAfterEndOfSpeechMsec()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getExtraSilenceAfterEndOfSpeechMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
