.class public final Lspeech/DecodedWordProto$DecodedWord;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DecodedWordProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/DecodedWordProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DecodedWord"
.end annotation


# instance fields
.field private cachedSize:I

.field private endFrame_:I

.field private hasEndFrame:Z

.field private hasStartFrame:Z

.field private hasText:Z

.field private startFrame_:I

.field private text_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lspeech/DecodedWordProto$DecodedWord;->text_:Ljava/lang/String;

    iput v1, p0, Lspeech/DecodedWordProto$DecodedWord;->startFrame_:I

    iput v1, p0, Lspeech/DecodedWordProto$DecodedWord;->endFrame_:I

    const/4 v0, -0x1

    iput v0, p0, Lspeech/DecodedWordProto$DecodedWord;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lspeech/DecodedWordProto$DecodedWord;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lspeech/DecodedWordProto$DecodedWord;->cachedSize:I

    return v0
.end method

.method public getEndFrame()I
    .locals 1

    iget v0, p0, Lspeech/DecodedWordProto$DecodedWord;->endFrame_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasText()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasStartFrame()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getStartFrame()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasEndFrame()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getEndFrame()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lspeech/DecodedWordProto$DecodedWord;->cachedSize:I

    return v0
.end method

.method public getStartFrame()I
    .locals 1

    iget v0, p0, Lspeech/DecodedWordProto$DecodedWord;->startFrame_:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/DecodedWordProto$DecodedWord;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public hasEndFrame()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasEndFrame:Z

    return v0
.end method

.method public hasStartFrame()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasStartFrame:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasText:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lspeech/DecodedWordProto$DecodedWord;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/DecodedWordProto$DecodedWord;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/DecodedWordProto$DecodedWord;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lspeech/DecodedWordProto$DecodedWord;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lspeech/DecodedWordProto$DecodedWord;->setText(Ljava/lang/String;)Lspeech/DecodedWordProto$DecodedWord;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lspeech/DecodedWordProto$DecodedWord;->setStartFrame(I)Lspeech/DecodedWordProto$DecodedWord;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lspeech/DecodedWordProto$DecodedWord;->setEndFrame(I)Lspeech/DecodedWordProto$DecodedWord;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setEndFrame(I)Lspeech/DecodedWordProto$DecodedWord;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasEndFrame:Z

    iput p1, p0, Lspeech/DecodedWordProto$DecodedWord;->endFrame_:I

    return-object p0
.end method

.method public setStartFrame(I)Lspeech/DecodedWordProto$DecodedWord;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasStartFrame:Z

    iput p1, p0, Lspeech/DecodedWordProto$DecodedWord;->startFrame_:I

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lspeech/DecodedWordProto$DecodedWord;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/DecodedWordProto$DecodedWord;->hasText:Z

    iput-object p1, p0, Lspeech/DecodedWordProto$DecodedWord;->text_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasStartFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getStartFrame()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->hasEndFrame()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/DecodedWordProto$DecodedWord;->getEndFrame()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt32(II)V

    :cond_2
    return-void
.end method
