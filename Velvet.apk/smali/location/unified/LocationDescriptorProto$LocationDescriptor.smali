.class public final Llocation/unified/LocationDescriptorProto$LocationDescriptor;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LocationDescriptorProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptorProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocationDescriptor"
.end annotation


# instance fields
.field private cachedSize:I

.field private confidence_:I

.field private featureId_:Llocation/unified/LocationDescriptorProto$FeatureIdProto;

.field private hasConfidence:Z

.field private hasFeatureId:Z

.field private hasHistoricalProducer:Z

.field private hasHistoricalProminence:Z

.field private hasHistoricalRole:Z

.field private hasLanguage:Z

.field private hasLatlng:Z

.field private hasLatlngSpan:Z

.field private hasLoc:Z

.field private hasMid:Z

.field private hasProducer:Z

.field private hasProvenance:Z

.field private hasRadius:Z

.field private hasRect:Z

.field private hasRole:Z

.field private hasTimestamp:Z

.field private historicalProducer_:I

.field private historicalProminence_:I

.field private historicalRole_:I

.field private language_:Ljava/lang/String;

.field private latlngSpan_:Llocation/unified/LocationDescriptorProto$LatLng;

.field private latlng_:Llocation/unified/LocationDescriptorProto$LatLng;

.field private loc_:Ljava/lang/String;

.field private mid_:J

.field private producer_:I

.field private provenance_:I

.field private radius_:F

.field private rect_:Llocation/unified/LocationDescriptorProto$LatLngRect;

.field private role_:I

.field private timestamp_:J


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->role_:I

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->producer_:I

    iput-wide v3, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->timestamp_:J

    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->loc_:Ljava/lang/String;

    iput-object v2, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlng_:Llocation/unified/LocationDescriptorProto$LatLng;

    iput-object v2, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlngSpan_:Llocation/unified/LocationDescriptorProto$LatLng;

    iput-object v2, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->rect_:Llocation/unified/LocationDescriptorProto$LatLngRect;

    const/4 v0, 0x0

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->radius_:F

    const/16 v0, 0x64

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->confidence_:I

    iput-object v2, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->featureId_:Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    iput-wide v3, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->mid_:J

    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->language_:Ljava/lang/String;

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->provenance_:I

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalRole_:I

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProducer_:I

    iput v1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProminence_:I

    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getSerializedSize()I

    :cond_0
    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->cachedSize:I

    return v0
.end method

.method public getConfidence()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->confidence_:I

    return v0
.end method

.method public getFeatureId()Llocation/unified/LocationDescriptorProto$FeatureIdProto;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->featureId_:Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    return-object v0
.end method

.method public getHistoricalProducer()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProducer_:I

    return v0
.end method

.method public getHistoricalProminence()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProminence_:I

    return v0
.end method

.method public getHistoricalRole()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalRole_:I

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatlng()Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlng_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object v0
.end method

.method public getLatlngSpan()Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlngSpan_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object v0
.end method

.method public getLoc()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->loc_:Ljava/lang/String;

    return-object v0
.end method

.method public getMid()J
    .locals 2

    iget-wide v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->mid_:J

    return-wide v0
.end method

.method public getProducer()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->producer_:I

    return v0
.end method

.method public getProvenance()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->provenance_:I

    return v0
.end method

.method public getRadius()F
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->radius_:F

    return v0
.end method

.method public getRect()Llocation/unified/LocationDescriptorProto$LatLngRect;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->rect_:Llocation/unified/LocationDescriptorProto$LatLngRect;

    return-object v0
.end method

.method public getRole()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->role_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRole()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRole()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProducer()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getProducer()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasTimestamp()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getTimestamp()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLoc()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLoc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlng()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLatlng()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlngSpan()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLatlngSpan()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRadius()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRadius()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasConfidence()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getConfidence()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProvenance()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getProvenance()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasFeatureId()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getFeatureId()Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLanguage()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalRole()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalRole()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProducer()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalProducer()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRect()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRect()Llocation/unified/LocationDescriptorProto$LatLngRect;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProminence()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalProminence()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasMid()Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getMid()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->cachedSize:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->timestamp_:J

    return-wide v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasConfidence:Z

    return v0
.end method

.method public hasFeatureId()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasFeatureId:Z

    return v0
.end method

.method public hasHistoricalProducer()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProducer:Z

    return v0
.end method

.method public hasHistoricalProminence()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProminence:Z

    return v0
.end method

.method public hasHistoricalRole()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalRole:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLanguage:Z

    return v0
.end method

.method public hasLatlng()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlng:Z

    return v0
.end method

.method public hasLatlngSpan()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlngSpan:Z

    return v0
.end method

.method public hasLoc()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLoc:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasMid:Z

    return v0
.end method

.method public hasProducer()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProducer:Z

    return v0
.end method

.method public hasProvenance()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProvenance:Z

    return v0
.end method

.method public hasRadius()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRadius:Z

    return v0
.end method

.method public hasRect()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRect:Z

    return v0
.end method

.method public hasRole()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRole:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasTimestamp:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setRole(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setProducer(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setTimestamp(J)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setLoc(Ljava/lang/String;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_5
    new-instance v1, Llocation/unified/LocationDescriptorProto$LatLng;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setLatlng(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_6
    new-instance v1, Llocation/unified/LocationDescriptorProto$LatLng;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setLatlngSpan(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setRadius(F)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setConfidence(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setProvenance(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_a
    new-instance v1, Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setFeatureId(Llocation/unified/LocationDescriptorProto$FeatureIdProto;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setLanguage(Ljava/lang/String;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setHistoricalRole(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setHistoricalProducer(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Llocation/unified/LocationDescriptorProto$LatLngRect;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LatLngRect;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setRect(Llocation/unified/LocationDescriptorProto$LatLngRect;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setHistoricalProminence(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->setMid(J)Llocation/unified/LocationDescriptorProto$LocationDescriptor;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public setConfidence(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasConfidence:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->confidence_:I

    return-object p0
.end method

.method public setFeatureId(Llocation/unified/LocationDescriptorProto$FeatureIdProto;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasFeatureId:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->featureId_:Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    return-object p0
.end method

.method public setHistoricalProducer(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProducer:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProducer_:I

    return-object p0
.end method

.method public setHistoricalProminence(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProminence:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalProminence_:I

    return-object p0
.end method

.method public setHistoricalRole(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalRole:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->historicalRole_:I

    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLanguage:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->language_:Ljava/lang/String;

    return-object p0
.end method

.method public setLatlng(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlng:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlng_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object p0
.end method

.method public setLatlngSpan(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlngSpan:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->latlngSpan_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object p0
.end method

.method public setLoc(Ljava/lang/String;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLoc:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->loc_:Ljava/lang/String;

    return-object p0
.end method

.method public setMid(J)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasMid:Z

    iput-wide p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->mid_:J

    return-object p0
.end method

.method public setProducer(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProducer:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->producer_:I

    return-object p0
.end method

.method public setProvenance(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProvenance:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->provenance_:I

    return-object p0
.end method

.method public setRadius(F)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRadius:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->radius_:F

    return-object p0
.end method

.method public setRect(Llocation/unified/LocationDescriptorProto$LatLngRect;)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LatLngRect;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRect:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->rect_:Llocation/unified/LocationDescriptorProto$LatLngRect;

    return-object p0
.end method

.method public setRole(I)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRole:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->role_:I

    return-object p0
.end method

.method public setTimestamp(J)Llocation/unified/LocationDescriptorProto$LocationDescriptor;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasTimestamp:Z

    iput-wide p1, p0, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->timestamp_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRole()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRole()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProducer()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getProducer()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLoc()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLoc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlng()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLatlng()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLatlngSpan()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLatlngSpan()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRadius()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRadius()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasConfidence()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getConfidence()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasProvenance()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getProvenance()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasFeatureId()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getFeatureId()Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalRole()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalRole()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_b
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProducer()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalProducer()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_c
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasRect()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getRect()Llocation/unified/LocationDescriptorProto$LatLngRect;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasHistoricalProminence()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getHistoricalProminence()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_e
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LocationDescriptor;->getMid()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_f
    return-void
.end method
