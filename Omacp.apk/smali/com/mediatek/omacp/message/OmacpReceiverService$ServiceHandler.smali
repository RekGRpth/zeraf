.class final Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;
.super Landroid/os/Handler;
.source "OmacpReceiverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/omacp/message/OmacpReceiverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/omacp/message/OmacpReceiverService;


# direct methods
.method public constructor <init>(Lcom/mediatek/omacp/message/OmacpReceiverService;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;->this$0:Lcom/mediatek/omacp/message/OmacpReceiverService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Intent;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "errorCode"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;->this$0:Lcom/mediatek/omacp/message/OmacpReceiverService;

    invoke-static {v4, v2, v1}, Lcom/mediatek/omacp/message/OmacpReceiverService;->access$000(Lcom/mediatek/omacp/message/OmacpReceiverService;Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;->this$0:Lcom/mediatek/omacp/message/OmacpReceiverService;

    invoke-static {v4, v3}, Lcom/mediatek/omacp/message/OmacpReceiver;->finishStartingService(Landroid/app/Service;I)V

    return-void

    :cond_1
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;->this$0:Lcom/mediatek/omacp/message/OmacpReceiverService;

    invoke-static {v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageNotification;->blockingUpdateNewMessageIndicator(Landroid/content/Context;Z)V

    goto :goto_0
.end method
