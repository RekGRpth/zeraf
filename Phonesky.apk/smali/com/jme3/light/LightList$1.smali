.class final Lcom/jme3/light/LightList$1;
.super Ljava/lang/Object;
.source "LightList.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jme3/light/LightList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/jme3/light/Light;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/jme3/light/Light;Lcom/jme3/light/Light;)I
    .locals 2
    .param p1    # Lcom/jme3/light/Light;
    .param p2    # Lcom/jme3/light/Light;

    iget v0, p1, Lcom/jme3/light/Light;->lastDistance:F

    iget v1, p2, Lcom/jme3/light/Light;->lastDistance:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/jme3/light/Light;->lastDistance:F

    iget v1, p2, Lcom/jme3/light/Light;->lastDistance:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/light/Light;

    check-cast p2, Lcom/jme3/light/Light;

    invoke-virtual {p0, p1, p2}, Lcom/jme3/light/LightList$1;->compare(Lcom/jme3/light/Light;Lcom/jme3/light/Light;)I

    move-result v0

    return v0
.end method
