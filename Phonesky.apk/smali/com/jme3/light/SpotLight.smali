.class public Lcom/jme3/light/SpotLight;
.super Lcom/jme3/light/Light;
.source "SpotLight.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# instance fields
.field protected direction:Lcom/jme3/math/Vector3f;

.field protected invSpotRange:F

.field protected packedAngleCos:F

.field protected position:Lcom/jme3/math/Vector3f;

.field protected spotInnerAngle:F

.field protected spotOuterAngle:F

.field protected spotRange:F


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/jme3/light/Light;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    const/high16 v1, -0x40800000

    invoke-direct {v0, v2, v1, v2}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    iput-object v0, p0, Lcom/jme3/light/SpotLight;->direction:Lcom/jme3/math/Vector3f;

    const v0, 0x3dc90fdb

    iput v0, p0, Lcom/jme3/light/SpotLight;->spotInnerAngle:F

    const v0, 0x3e060a92

    iput v0, p0, Lcom/jme3/light/SpotLight;->spotOuterAngle:F

    const/high16 v0, 0x42c80000

    iput v0, p0, Lcom/jme3/light/SpotLight;->spotRange:F

    iput v2, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    iput v2, p0, Lcom/jme3/light/SpotLight;->packedAngleCos:F

    invoke-direct {p0}, Lcom/jme3/light/SpotLight;->computePackedCos()V

    return-void
.end method

.method private computePackedCos()V
    .locals 3

    iget v2, p0, Lcom/jme3/light/SpotLight;->spotInnerAngle:F

    invoke-static {v2}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v0

    iget v2, p0, Lcom/jme3/light/SpotLight;->spotOuterAngle:F

    invoke-static {v2}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v1

    const/high16 v2, 0x447a0000

    mul-float/2addr v2, v0

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/jme3/light/SpotLight;->packedAngleCos:F

    iget v2, p0, Lcom/jme3/light/SpotLight;->packedAngleCos:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/jme3/light/SpotLight;->packedAngleCos:F

    return-void
.end method


# virtual methods
.method protected computeLastDistance(Lcom/jme3/scene/Spatial;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/Spatial;

    invoke-virtual {p1}, Lcom/jme3/scene/Spatial;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/jme3/scene/Spatial;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Lcom/jme3/bounding/BoundingVolume;->distanceSquaredTo(Lcom/jme3/math/Vector3f;)F

    move-result v1

    iput v1, p0, Lcom/jme3/light/SpotLight;->lastDistance:F

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/Spatial;->getWorldTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v1

    iput v1, p0, Lcom/jme3/light/SpotLight;->lastDistance:F

    goto :goto_0
.end method

.method public getDirection()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/light/SpotLight;->direction:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public getInvSpotRange()F
    .locals 1

    iget v0, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    return v0
.end method

.method public getPackedAngleCos()F
    .locals 1

    iget v0, p0, Lcom/jme3/light/SpotLight;->packedAngleCos:F

    return v0
.end method

.method public getPosition()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public getType()Lcom/jme3/light/Light$Type;
    .locals 1

    sget-object v0, Lcom/jme3/light/Light$Type;->Spot:Lcom/jme3/light/Light$Type;

    return-object v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 4
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/jme3/light/Light;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "spotInnerAngle"

    const v2, 0x3dc90fdb

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/light/SpotLight;->spotInnerAngle:F

    const-string v1, "spotOuterAngle"

    const v2, 0x3e060a92

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/light/SpotLight;->spotOuterAngle:F

    const-string v1, "direction"

    new-instance v2, Lcom/jme3/math/Vector3f;

    invoke-direct {v2}, Lcom/jme3/math/Vector3f;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;

    iput-object v1, p0, Lcom/jme3/light/SpotLight;->direction:Lcom/jme3/math/Vector3f;

    const-string v1, "position"

    new-instance v2, Lcom/jme3/math/Vector3f;

    invoke-direct {v2}, Lcom/jme3/math/Vector3f;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;

    iput-object v1, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    const-string v1, "spotRange"

    const/high16 v2, 0x42c80000

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/light/SpotLight;->spotRange:F

    iget v1, p0, Lcom/jme3/light/SpotLight;->spotRange:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000

    iget v2, p0, Lcom/jme3/light/SpotLight;->spotRange:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    :goto_0
    return-void

    :cond_0
    iput v3, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    goto :goto_0
.end method

.method public setDirection(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/light/SpotLight;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public setPosition(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/light/SpotLight;->position:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public setSpotInnerAngle(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/light/SpotLight;->spotInnerAngle:F

    invoke-direct {p0}, Lcom/jme3/light/SpotLight;->computePackedCos()V

    return-void
.end method

.method public setSpotOuterAngle(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/light/SpotLight;->spotOuterAngle:F

    invoke-direct {p0}, Lcom/jme3/light/SpotLight;->computePackedCos()V

    return-void
.end method

.method public setSpotRange(F)V
    .locals 2
    .param p1    # F

    const/4 v1, 0x0

    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SpotLight range cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p1, p0, Lcom/jme3/light/SpotLight;->spotRange:F

    cmpl-float v0, p1, v1

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000

    div-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    :goto_0
    return-void

    :cond_1
    iput v1, p0, Lcom/jme3/light/SpotLight;->invSpotRange:F

    goto :goto_0
.end method
