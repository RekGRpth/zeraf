.class public final Lcom/jme3/texture/plugins/TGALoader;
.super Ljava/lang/Object;
.source "TGALoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static flipEndian(S)S
    .locals 3
    .param p0    # S

    const v1, 0xffff

    and-int v0, p0, v1

    shl-int/lit8 v1, v0, 0x8

    const v2, 0xff00

    and-int/2addr v2, v0

    ushr-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    int-to-short v1, v1

    return v1
.end method

.method private static getBitsAsByte([BII)B
    .locals 8
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x2

    div-int/lit8 v3, p1, 0x8

    rem-int/lit8 v2, p1, 0x8

    const/4 v4, 0x0

    move v1, p2

    :cond_0
    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_4

    aget-byte v0, p0, v3

    const/4 v6, 0x7

    if-ne v2, v6, :cond_2

    const/4 v5, 0x1

    :goto_1
    and-int v6, v0, v5

    if-eqz v6, :cond_1

    if-nez v1, :cond_3

    add-int/lit8 v4, v4, 0x1

    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    const/16 v6, 0x8

    if-ne v2, v6, :cond_0

    const/4 v2, 0x0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    rsub-int/lit8 v6, v2, 0x6

    shl-int v5, v7, v6

    goto :goto_1

    :cond_3
    add-int/lit8 v6, v1, -0x1

    shl-int v6, v7, v6

    add-int/2addr v4, v6

    goto :goto_2

    :cond_4
    int-to-byte v6, v4

    return v6
.end method

.method public static load(Ljava/io/InputStream;Z)Lcom/jme3/texture/Image;
    .locals 46
    .param p0    # Ljava/io/InputStream;
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v22, 0x0

    new-instance v19, Ljava/io/DataInputStream;

    new-instance v43, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v27

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v15

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v29

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    move-result v43

    invoke-static/range {v43 .. v43}, Lcom/jme3/texture/plugins/TGALoader;->flipEndian(S)S

    move-result v14

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v12

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    move-result v43

    invoke-static/range {v43 .. v43}, Lcom/jme3/texture/plugins/TGALoader;->flipEndian(S)S

    move-result v41

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    move-result v43

    invoke-static/range {v43 .. v43}, Lcom/jme3/texture/plugins/TGALoader;->flipEndian(S)S

    move-result v25

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v33

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v28

    and-int/lit8 v43, v28, 0x20

    if-eqz v43, :cond_0

    if-nez p1, :cond_4

    const/16 p1, 0x1

    :cond_0
    :goto_0
    and-int/lit8 v43, v28, 0x10

    if-eqz v43, :cond_1

    if-nez v22, :cond_5

    const/16 v22, 0x1

    :cond_1
    :goto_1
    if-lez v27, :cond_2

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v43, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v43

    invoke-virtual {v0, v1, v2}, Ljava/io/InputStream;->skip(J)J

    :cond_2
    const/4 v13, 0x0

    if-eqz v15, :cond_7

    mul-int v43, v12, v14

    shr-int/lit8 v9, v43, 0x3

    div-int/lit8 v43, v12, 0x3

    const/16 v44, 0x8

    invoke-static/range {v43 .. v44}, Ljava/lang/Math;->min(II)I

    move-result v7

    new-array v11, v9, [B

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/io/InputStream;->read([B)I

    const/16 v43, 0x1

    move/from16 v0, v29

    move/from16 v1, v43

    if-eq v0, v1, :cond_3

    const/16 v43, 0x9

    move/from16 v0, v29

    move/from16 v1, v43

    if-ne v0, v1, :cond_7

    :cond_3
    new-array v13, v14, [Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;

    mul-int/lit8 v43, v7, 0x3

    sub-int v6, v12, v43

    const/high16 v43, 0x437f0000

    const/high16 v44, 0x40000000

    int-to-float v0, v7

    move/from16 v45, v0

    invoke-static/range {v44 .. v45}, Lcom/jme3/math/FastMath;->pow(FF)F

    move-result v44

    const/high16 v45, 0x3f800000

    sub-float v44, v44, v45

    div-float v38, v43, v44

    const/high16 v43, 0x437f0000

    const/high16 v44, 0x40000000

    int-to-float v0, v6

    move/from16 v45, v0

    invoke-static/range {v44 .. v45}, Lcom/jme3/math/FastMath;->pow(FF)F

    move-result v44

    const/high16 v45, 0x3f800000

    sub-float v44, v44, v45

    div-float v5, v43, v44

    const/16 v26, 0x0

    :goto_2
    move/from16 v0, v26

    if-ge v0, v14, :cond_7

    new-instance v21, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;

    invoke-direct/range {v21 .. v21}, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;-><init>()V

    mul-int v32, v12, v26

    move/from16 v0, v32

    invoke-static {v11, v0, v7}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    move-object/from16 v1, v21

    iput-byte v0, v1, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->red:B

    add-int v43, v32, v7

    move/from16 v0, v43

    invoke-static {v11, v0, v7}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    move-object/from16 v1, v21

    iput-byte v0, v1, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->green:B

    mul-int/lit8 v43, v7, 0x2

    add-int v43, v43, v32

    move/from16 v0, v43

    invoke-static {v11, v0, v7}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    move-object/from16 v1, v21

    iput-byte v0, v1, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->blue:B

    if-gtz v6, :cond_6

    const/16 v43, -0x1

    move/from16 v0, v43

    move-object/from16 v1, v21

    iput-byte v0, v1, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->alpha:B

    :goto_3
    aput-object v21, v13, v26

    add-int/lit8 v26, v26, 0x1

    goto :goto_2

    :cond_4
    const/16 p1, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v22, 0x0

    goto/16 :goto_1

    :cond_6
    mul-int/lit8 v43, v7, 0x3

    add-int v43, v43, v32

    move/from16 v0, v43

    invoke-static {v11, v0, v6}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v5

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    move-object/from16 v1, v21

    iput-byte v0, v1, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->alpha:B

    goto :goto_3

    :cond_7
    const/16 v34, 0x0

    const/16 v43, 0x20

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_b

    mul-int v43, v41, v25

    mul-int/lit8 v43, v43, 0x4

    move/from16 v0, v43

    new-array v0, v0, [B

    move-object/from16 v34, v0

    const/16 v20, 0x4

    :goto_4
    const/16 v35, 0x0

    const/16 v43, 0x2

    move/from16 v0, v29

    move/from16 v1, v43

    if-ne v0, v1, :cond_17

    const/16 v37, 0x0

    const/16 v24, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/16 v43, 0x10

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_f

    const/16 v43, 0x2

    move/from16 v0, v43

    new-array v0, v0, [B

    move-object/from16 v18, v0

    const v38, 0x41039ce7

    const/16 v26, 0x0

    :goto_5
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_d

    if-nez p1, :cond_8

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_8
    const/16 v31, 0x0

    move/from16 v36, v35

    :goto_6
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_c

    const/16 v43, 0x1

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    const/16 v43, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    add-int/lit8 v35, v36, 0x1

    const/16 v43, 0x1

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    aput-byte v43, v34, v36

    add-int/lit8 v36, v35, 0x1

    const/16 v43, 0x6

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    aput-byte v43, v34, v35

    add-int/lit8 v35, v36, 0x1

    const/16 v43, 0xb

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v43, v0

    aput-byte v43, v34, v36

    const/16 v43, 0x4

    move/from16 v0, v20

    move/from16 v1, v43

    if-ne v0, v1, :cond_a

    const/16 v43, 0x0

    const/16 v44, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v4

    const/16 v43, 0x1

    move/from16 v0, v43

    if-ne v4, v0, :cond_9

    const/4 v4, -0x1

    :cond_9
    add-int/lit8 v36, v35, 0x1

    aput-byte v4, v34, v35

    move/from16 v35, v36

    :cond_a
    add-int/lit8 v31, v31, 0x1

    move/from16 v36, v35

    goto/16 :goto_6

    :cond_b
    mul-int v43, v41, v25

    mul-int/lit8 v43, v43, 0x3

    move/from16 v0, v43

    new-array v0, v0, [B

    move-object/from16 v34, v0

    const/16 v20, 0x3

    goto/16 :goto_4

    :cond_c
    add-int/lit8 v26, v26, 0x1

    move/from16 v35, v36

    goto/16 :goto_5

    :cond_d
    const/16 v43, 0x4

    move/from16 v0, v20

    move/from16 v1, v43

    if-ne v0, v1, :cond_e

    sget-object v23, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    :goto_7
    invoke-virtual/range {p0 .. p0}, Ljava/io/InputStream;->close()V

    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v43, v0

    invoke-static/range {v43 .. v43}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual/range {v39 .. v39}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    new-instance v40, Lcom/jme3/texture/Image;

    invoke-direct/range {v40 .. v40}, Lcom/jme3/texture/Image;-><init>()V

    move-object/from16 v0, v40

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Image;->setFormat(Lcom/jme3/texture/Image$Format;)V

    invoke-virtual/range {v40 .. v41}, Lcom/jme3/texture/Image;->setWidth(I)V

    move-object/from16 v0, v40

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Image;->setHeight(I)V

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Image;->setData(Ljava/nio/ByteBuffer;)V

    return-object v40

    :cond_e
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGB8:Lcom/jme3/texture/Image$Format;

    goto :goto_7

    :cond_f
    const/16 v43, 0x18

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_12

    const/16 v42, 0x0

    :goto_8
    move/from16 v0, v42

    move/from16 v1, v25

    if-ge v0, v1, :cond_11

    if-nez p1, :cond_10

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v42

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :goto_9
    mul-int v43, v41, v20

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    move/from16 v2, v35

    move/from16 v3, v43

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/DataInputStream;->readFully([BII)V

    add-int/lit8 v42, v42, 0x1

    goto :goto_8

    :cond_10
    mul-int v43, v42, v41

    mul-int v35, v43, v20

    goto :goto_9

    :cond_11
    sget-object v23, Lcom/jme3/texture/Image$Format;->BGR8:Lcom/jme3/texture/Image$Format;

    goto :goto_7

    :cond_12
    const/16 v43, 0x20

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_16

    const/16 v26, 0x0

    :goto_a
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_15

    if-nez p1, :cond_13

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_13
    const/16 v31, 0x0

    move/from16 v36, v35

    :goto_b
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_14

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v24

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v37

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v4, v34, v35

    add-int/lit8 v31, v31, 0x1

    goto :goto_b

    :cond_14
    add-int/lit8 v26, v26, 0x1

    move/from16 v35, v36

    goto :goto_a

    :cond_15
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    goto/16 :goto_7

    :cond_16
    new-instance v43, Ljava/io/IOException;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "Unsupported TGA true color depth: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43

    :cond_17
    const/16 v43, 0xa

    move/from16 v0, v29

    move/from16 v1, v43

    if-ne v0, v1, :cond_2a

    const/16 v37, 0x0

    const/16 v24, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/16 v43, 0x20

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_1d

    const/16 v26, 0x0

    :goto_c
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_1c

    if-nez p1, :cond_18

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_18
    const/16 v31, 0x0

    :goto_d
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_1b

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x80

    move/from16 v43, v0

    if-eqz v43, :cond_19

    and-int/lit8 v16, v16, 0x7f

    add-int v31, v31, v16

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v24

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v37

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_e
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_1a

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v4, v34, v35

    move/from16 v17, v16

    goto :goto_e

    :cond_19
    add-int v31, v31, v16

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_f
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_1a

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v24

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v37

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v4, v34, v35

    move/from16 v17, v16

    goto :goto_f

    :cond_1a
    move/from16 v35, v36

    add-int/lit8 v31, v31, 0x1

    goto :goto_d

    :cond_1b
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_c

    :cond_1c
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    goto/16 :goto_7

    :cond_1d
    const/16 v43, 0x18

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_23

    const/16 v26, 0x0

    :goto_10
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_22

    if-nez p1, :cond_1e

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_1e
    const/16 v31, 0x0

    :goto_11
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_21

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x80

    move/from16 v43, v0

    if-eqz v43, :cond_1f

    and-int/lit8 v16, v16, 0x7f

    add-int v31, v31, v16

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v24

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v37

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_12
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_20

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    move/from16 v17, v16

    move/from16 v36, v35

    goto :goto_12

    :cond_1f
    add-int v31, v31, v16

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_13
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_20

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v24

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v37

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    move/from16 v17, v16

    move/from16 v36, v35

    goto :goto_13

    :cond_20
    move/from16 v35, v36

    add-int/lit8 v31, v31, 0x1

    goto :goto_11

    :cond_21
    add-int/lit8 v26, v26, 0x1

    goto :goto_10

    :cond_22
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGB8:Lcom/jme3/texture/Image$Format;

    goto/16 :goto_7

    :cond_23
    const/16 v43, 0x10

    move/from16 v0, v33

    move/from16 v1, v43

    if-ne v0, v1, :cond_29

    const/16 v43, 0x2

    move/from16 v0, v43

    new-array v0, v0, [B

    move-object/from16 v18, v0

    const v38, 0x41039ce7

    const/16 v26, 0x0

    :goto_14
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_28

    if-nez p1, :cond_24

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_24
    const/16 v31, 0x0

    :goto_15
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_27

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x80

    move/from16 v43, v0

    if-eqz v43, :cond_25

    and-int/lit8 v16, v16, 0x7f

    add-int v31, v31, v16

    const/16 v43, 0x1

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    const/16 v43, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    const/16 v43, 0x1

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v8, v0

    const/16 v43, 0x6

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v24, v0

    const/16 v43, 0xb

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v37, v0

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_16
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_26

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    move/from16 v17, v16

    move/from16 v36, v35

    goto :goto_16

    :cond_25
    add-int v31, v31, v16

    move/from16 v17, v16

    move/from16 v36, v35

    :goto_17
    add-int/lit8 v16, v17, -0x1

    if-ltz v17, :cond_26

    const/16 v43, 0x1

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    const/16 v43, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readByte()B

    move-result v44

    aput-byte v44, v18, v43

    const/16 v43, 0x1

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v8, v0

    const/16 v43, 0x6

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v24, v0

    const/16 v43, 0xb

    const/16 v44, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v43

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/jme3/texture/plugins/TGALoader;->getBitsAsByte([BII)B

    move-result v43

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v43, v0

    mul-float v43, v43, v38

    move/from16 v0, v43

    float-to-int v0, v0

    move/from16 v43, v0

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v37, v0

    add-int/lit8 v35, v36, 0x1

    aput-byte v37, v34, v36

    add-int/lit8 v36, v35, 0x1

    aput-byte v24, v34, v35

    add-int/lit8 v35, v36, 0x1

    aput-byte v8, v34, v36

    move/from16 v17, v16

    move/from16 v36, v35

    goto :goto_17

    :cond_26
    move/from16 v35, v36

    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_15

    :cond_27
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_14

    :cond_28
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGB8:Lcom/jme3/texture/Image$Format;

    goto/16 :goto_7

    :cond_29
    new-instance v43, Ljava/io/IOException;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "Unsupported TGA true color depth: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43

    :cond_2a
    const/16 v43, 0x1

    move/from16 v0, v29

    move/from16 v1, v43

    if-ne v0, v1, :cond_39

    div-int/lit8 v10, v33, 0x8

    const/16 v43, 0x1

    move/from16 v0, v43

    if-ne v10, v0, :cond_30

    const/16 v26, 0x0

    :goto_18
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_37

    if-nez p1, :cond_2b

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_2b
    const/16 v31, 0x0

    move/from16 v36, v35

    :goto_19
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_2f

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v30

    array-length v0, v13

    move/from16 v43, v0

    move/from16 v0, v30

    move/from16 v1, v43

    if-ge v0, v1, :cond_2c

    if-gez v30, :cond_2d

    :cond_2c
    new-instance v43, Ljava/io/IOException;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "TGA: Invalid color map entry referenced: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43

    :cond_2d
    aget-object v21, v13, v30

    add-int/lit8 v35, v36, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->red:B

    move/from16 v43, v0

    aput-byte v43, v34, v36

    add-int/lit8 v36, v35, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->green:B

    move/from16 v43, v0

    aput-byte v43, v34, v35

    add-int/lit8 v35, v36, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->blue:B

    move/from16 v43, v0

    aput-byte v43, v34, v36

    const/16 v43, 0x4

    move/from16 v0, v20

    move/from16 v1, v43

    if-ne v0, v1, :cond_2e

    add-int/lit8 v36, v35, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->alpha:B

    move/from16 v43, v0

    aput-byte v43, v34, v35

    move/from16 v35, v36

    :cond_2e
    add-int/lit8 v31, v31, 0x1

    move/from16 v36, v35

    goto :goto_19

    :cond_2f
    add-int/lit8 v26, v26, 0x1

    move/from16 v35, v36

    goto/16 :goto_18

    :cond_30
    const/16 v43, 0x2

    move/from16 v0, v43

    if-ne v10, v0, :cond_36

    const/16 v26, 0x0

    :goto_1a
    add-int/lit8 v43, v25, -0x1

    move/from16 v0, v26

    move/from16 v1, v43

    if-gt v0, v1, :cond_37

    if-nez p1, :cond_31

    add-int/lit8 v43, v25, -0x1

    sub-int v43, v43, v26

    mul-int v43, v43, v41

    mul-int v35, v43, v20

    :cond_31
    const/16 v31, 0x0

    move/from16 v36, v35

    :goto_1b
    move/from16 v0, v31

    move/from16 v1, v41

    if-ge v0, v1, :cond_35

    invoke-virtual/range {v19 .. v19}, Ljava/io/DataInputStream;->readShort()S

    move-result v43

    invoke-static/range {v43 .. v43}, Lcom/jme3/texture/plugins/TGALoader;->flipEndian(S)S

    move-result v30

    array-length v0, v13

    move/from16 v43, v0

    move/from16 v0, v30

    move/from16 v1, v43

    if-ge v0, v1, :cond_32

    if-gez v30, :cond_33

    :cond_32
    new-instance v43, Ljava/io/IOException;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "TGA: Invalid color map entry referenced: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43

    :cond_33
    aget-object v21, v13, v30

    add-int/lit8 v35, v36, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->red:B

    move/from16 v43, v0

    aput-byte v43, v34, v36

    add-int/lit8 v36, v35, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->green:B

    move/from16 v43, v0

    aput-byte v43, v34, v35

    add-int/lit8 v35, v36, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->blue:B

    move/from16 v43, v0

    aput-byte v43, v34, v36

    const/16 v43, 0x4

    move/from16 v0, v20

    move/from16 v1, v43

    if-ne v0, v1, :cond_34

    add-int/lit8 v36, v35, 0x1

    move-object/from16 v0, v21

    iget-byte v0, v0, Lcom/jme3/texture/plugins/TGALoader$ColorMapEntry;->alpha:B

    move/from16 v43, v0

    aput-byte v43, v34, v35

    move/from16 v35, v36

    :cond_34
    add-int/lit8 v31, v31, 0x1

    move/from16 v36, v35

    goto :goto_1b

    :cond_35
    add-int/lit8 v26, v26, 0x1

    move/from16 v35, v36

    goto/16 :goto_1a

    :cond_36
    new-instance v43, Ljava/io/IOException;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "TGA: unknown colormap indexing size used: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43

    :cond_37
    const/16 v43, 0x4

    move/from16 v0, v20

    move/from16 v1, v43

    if-ne v0, v1, :cond_38

    sget-object v23, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    :goto_1c
    goto/16 :goto_7

    :cond_38
    sget-object v23, Lcom/jme3/texture/Image$Format;->RGB8:Lcom/jme3/texture/Image$Format;

    goto :goto_1c

    :cond_39
    new-instance v43, Ljava/io/IOException;

    const-string v44, "Grayscale TGA not supported"

    invoke-direct/range {v43 .. v44}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v43
.end method


# virtual methods
.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 5
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v3

    instance-of v3, v3, Lcom/jme3/asset/TextureKey;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Texture assets must be loaded using a TextureKey"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v3

    check-cast v3, Lcom/jme3/asset/TextureKey;

    invoke-virtual {v3}, Lcom/jme3/asset/TextureKey;->isFlipY()Z

    move-result v0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/jme3/texture/plugins/TGALoader;->load(Ljava/io/InputStream;Z)Lcom/jme3/texture/Image;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_1
    return-object v1

    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v3
.end method
