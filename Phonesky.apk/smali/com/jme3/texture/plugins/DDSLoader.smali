.class public Lcom/jme3/texture/plugins/DDSLoader;
.super Ljava/lang/Object;
.source "DDSLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LOG2:D

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private alphaMask:I

.field private blueMask:I

.field private bpp:I

.field private caps1:I

.field private caps2:I

.field private compressed:Z

.field private depth:I

.field private directx10:Z

.field private flags:I

.field private grayscaleOrAlpha:Z

.field private greenMask:I

.field private height:I

.field private in:Ljava/io/DataInput;

.field private mipMapCount:I

.field private normal:Z

.field private pitchOrSize:I

.field private pixelFormat:Lcom/jme3/texture/Image$Format;

.field private redMask:I

.field private sizes:[I

.field private texture3D:Z

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/jme3/texture/plugins/DDSLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/texture/plugins/DDSLoader;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/texture/plugins/DDSLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/jme3/texture/plugins/DDSLoader;->LOG2:D

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static byte2int([B)I
    .locals 6
    .param p0    # [B

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    array-length v1, p0

    if-ne v1, v3, :cond_1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    :cond_0
    :goto_0
    return v0

    :cond_1
    array-length v1, p0

    if-ne v1, v4, :cond_2

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, v3

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    goto :goto_0

    :cond_2
    array-length v1, p0

    if-ne v1, v5, :cond_3

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, v3

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    aget-byte v1, p0, v4

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    goto :goto_0

    :cond_3
    array-length v1, p0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, v3

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    aget-byte v1, p0, v4

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    aget-byte v1, p0, v5

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private static count(I)I
    .locals 3
    .param p0    # I

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    and-int/lit8 v1, p0, 0x1

    if-nez v1, :cond_0

    shr-int/lit8 p0, p0, 0x1

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x20

    if-le v0, v1, :cond_2

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static is(II)Z
    .locals 1
    .param p0    # I
    .param p1    # I

    and-int v0, p0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadDX10Header()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x4

    iget-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const/16 v4, 0x53

    if-eq v1, v4, :cond_0

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Only DXGI_FORMAT_BC5_UNORM is supported for DirectX10 DDS! Got: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    sget-object v4, Lcom/jme3/texture/Image$Format;->LATC:Lcom/jme3/texture/Image$Format;

    iput-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    const/16 v4, 0x8

    iput v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    iput-boolean v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    iget-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v3

    if-ne v3, v5, :cond_1

    iput-boolean v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    :cond_1
    iget-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v2

    iget-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-static {v2, v5}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x6

    if-eq v0, v4, :cond_2

    new-instance v4, Ljava/io/IOException;

    const-string v5, "Cubemaps should consist of 6 images!"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v4, v5}, Ljava/io/DataInput;->skipBytes(I)I

    return-void
.end method

.method private loadHeader()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x6

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const v2, 0x20534444

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const/16 v2, 0x7c

    if-eq v1, v2, :cond_1

    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Not a DDS file"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    const/16 v2, 0x1007

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    invoke-static {v1, v3}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Mandatory flags missing"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    const/16 v2, 0x2c

    invoke-interface {v1, v2}, Ljava/io/DataInput;->skipBytes(I)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    iput-boolean v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->directx10:Z

    invoke-direct {p0}, Lcom/jme3/texture/plugins/DDSLoader;->readPixelFormat()V

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps1:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps2:I

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    const/16 v2, 0xc

    invoke-interface {v1, v2}, Ljava/io/DataInput;->skipBytes(I)I

    iput-boolean v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    iget-boolean v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->directx10:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps1:I

    const/16 v2, 0x1000

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/io/IOException;

    const-string v2, "File is not a texture"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    if-gtz v1, :cond_4

    iput v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    :cond_4
    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps2:I

    const/16 v2, 0x200

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-eqz v1, :cond_5

    iput v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    :cond_5
    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps2:I

    const/high16 v2, 0x200000

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-eqz v1, :cond_6

    iput-boolean v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    :cond_6
    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    iget v2, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->log(D)D

    move-result-wide v1

    sget-wide v3, Lcom/jme3/texture/plugins/DDSLoader;->LOG2:D

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v0, v1, 0x1

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->caps1:I

    const/high16 v2, 0x400000

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-eqz v1, :cond_a

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    const/high16 v2, 0x20000

    invoke-static {v1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v1

    if-nez v1, :cond_9

    iput v0, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    :cond_7
    :goto_0
    iget-boolean v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->directx10:Z

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/jme3/texture/plugins/DDSLoader;->loadDX10Header()V

    :cond_8
    invoke-direct {p0}, Lcom/jme3/texture/plugins/DDSLoader;->loadSizes()V

    return-void

    :cond_9
    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-eq v1, v0, :cond_7

    sget-object v1, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Got {0} mipmaps, expected {1}"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_a
    iput v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    goto :goto_0
.end method

.method private loadSizes()V
    .locals 8

    const/4 v7, 0x1

    iget v2, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    invoke-virtual {v5}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-ge v0, v5, :cond_1

    iget-boolean v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    if-eqz v5, :cond_0

    add-int/lit8 v5, v2, 0x3

    div-int/lit8 v5, v5, 0x4

    add-int/lit8 v6, v1, 0x3

    div-int/lit8 v6, v6, 0x4

    mul-int/2addr v5, v6

    mul-int/2addr v5, v3

    mul-int/lit8 v4, v5, 0x2

    :goto_1
    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    add-int/lit8 v6, v4, 0x3

    div-int/lit8 v6, v6, 0x4

    mul-int/lit8 v6, v6, 0x4

    aput v6, v5, v0

    div-int/lit8 v5, v2, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    div-int/lit8 v5, v1, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    mul-int v5, v2, v1

    mul-int/2addr v5, v3

    div-int/lit8 v4, v5, 0x8

    goto :goto_1

    :cond_1
    return-void
.end method

.method private readPixelFormat()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0x8

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v2

    const/16 v5, 0x20

    if-eq v2, v5, :cond_0

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Pixel format size is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", not 32"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    iput-boolean v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->normal:Z

    invoke-static {v1, v7}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_6

    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v0

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v4

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    const/16 v6, 0x10

    invoke-interface {v5, v6}, Ljava/io/DataInput;->skipBytes(I)I

    sparse-switch v0, :sswitch_data_0

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown fourcc: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Lcom/jme3/texture/plugins/DDSLoader;->string(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_0
    iput v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    invoke-static {v1, v10}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/jme3/texture/Image$Format;->DXT1A:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    :cond_1
    :goto_0
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    add-int/lit8 v5, v5, 0x3

    div-int/lit8 v5, v5, 0x4

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    add-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x4

    mul-int/2addr v5, v6

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    mul-int/2addr v5, v6

    mul-int/lit8 v3, v5, 0x2

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    const/high16 v6, 0x80000

    invoke-static {v5, v6}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    if-nez v5, :cond_4

    sget-object v5, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v6, "Must use linear size with fourcc"

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    iput v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    :cond_2
    :goto_1
    return-void

    :cond_3
    sget-object v5, Lcom/jme3/texture/Image$Format;->DXT1:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_0

    :sswitch_1
    iput v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sget-object v5, Lcom/jme3/texture/Image$Format;->DXT3:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_0

    :sswitch_2
    iput v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sget-object v5, Lcom/jme3/texture/Image$Format;->DXT5:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    const v5, 0x78477852

    if-ne v4, v5, :cond_1

    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->normal:Z

    goto :goto_0

    :sswitch_3
    iput v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sget-object v5, Lcom/jme3/texture/Image$Format;->LTC:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_0

    :sswitch_4
    iput v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sget-object v5, Lcom/jme3/texture/Image$Format;->LATC:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_0

    :sswitch_5
    iput-boolean v11, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->directx10:Z

    goto :goto_1

    :cond_4
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    if-eq v5, v3, :cond_2

    sget-object v5, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Expected size = {0}, real = {1}"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    iget v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    iput v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    goto :goto_1

    :cond_6
    iput-boolean v11, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v5

    iput v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v5

    iput v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v5

    iput v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v5

    iput v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5}, Ljava/io/DataInput;->readInt()I

    move-result v5

    iput v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    const/16 v5, 0x40

    invoke-static {v1, v5}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-static {v1, v10}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_7

    sget-object v5, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    :goto_2
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    div-int/lit8 v5, v5, 0x8

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    mul-int v3, v5, v6

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->flags:I

    const/high16 v6, 0x80000

    invoke-static {v5, v6}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_d

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    if-nez v5, :cond_c

    sget-object v5, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v6, "Linear size said to contain valid value but does not"

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    iput v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    goto/16 :goto_1

    :cond_7
    sget-object v5, Lcom/jme3/texture/Image$Format;->RGB8:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_2

    :cond_8
    const/high16 v5, 0x20000

    invoke-static {v1, v5}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {v1, v10}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_9

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sparse-switch v5, :sswitch_data_1

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported GrayscaleAlpha BPP: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_6
    sget-object v5, Lcom/jme3/texture/Image$Format;->Luminance8Alpha8:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    :goto_3
    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    goto :goto_2

    :sswitch_7
    sget-object v5, Lcom/jme3/texture/Image$Format;->Luminance16Alpha16:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_3

    :cond_9
    const/high16 v5, 0x20000

    invoke-static {v1, v5}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_a

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sparse-switch v5, :sswitch_data_2

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported Grayscale BPP: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_8
    sget-object v5, Lcom/jme3/texture/Image$Format;->Luminance8:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    :goto_4
    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    goto/16 :goto_2

    :sswitch_9
    sget-object v5, Lcom/jme3/texture/Image$Format;->Luminance16:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_4

    :cond_a
    invoke-static {v1, v8}, Lcom/jme3/texture/plugins/DDSLoader;->is(II)Z

    move-result v5

    if-eqz v5, :cond_b

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    sparse-switch v5, :sswitch_data_3

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported Alpha BPP: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_a
    sget-object v5, Lcom/jme3/texture/Image$Format;->Alpha8:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    :goto_5
    iput-boolean v10, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    goto/16 :goto_2

    :sswitch_b
    sget-object v5, Lcom/jme3/texture/Image$Format;->Alpha16:Lcom/jme3/texture/Image$Format;

    iput-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    goto :goto_5

    :cond_b
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Unknown PixelFormat in DDS file"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_c
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    if-eq v5, v3, :cond_2

    sget-object v5, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Expected size = {0}, real = {1}"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    iget v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_d
    iput v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->pitchOrSize:I

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x30315844 -> :sswitch_5
        0x31495441 -> :sswitch_3
        0x31545844 -> :sswitch_0
        0x32495441 -> :sswitch_4
        0x33545844 -> :sswitch_1
        0x35545844 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_6
        0x20 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x8 -> :sswitch_8
        0x10 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x8 -> :sswitch_a
        0x10 -> :sswitch_b
    .end sparse-switch
.end method

.method private static string(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit16 v1, p0, 0xff

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v1, 0xff00

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/high16 v1, 0xff0000

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/high16 v1, 0xff00000

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public flipData([BII)[B
    .locals 4
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    array-length v2, p1

    new-array v0, v2, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p3, :cond_0

    mul-int v2, v1, p2

    sub-int v3, p3, v1

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p2

    invoke-static {p1, v2, v0, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 8
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v0

    instance-of v0, v0, Lcom/jme3/asset/TextureKey;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Texture assets must be loaded using a TextureKey"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v7

    new-instance v0, Lcom/jme3/util/LittleEndien;

    invoke-direct {v0, v7}, Lcom/jme3/util/LittleEndien;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-direct {p0}, Lcom/jme3/texture/plugins/DDSLoader;->loadHeader()V

    iget-boolean v0, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v0

    check-cast v0, Lcom/jme3/asset/TextureKey;

    sget-object v1, Lcom/jme3/texture/Texture$Type;->ThreeDimensional:Lcom/jme3/texture/Texture$Type;

    invoke-virtual {v0, v1}, Lcom/jme3/asset/TextureKey;->setTextureTypeHint(Lcom/jme3/texture/Texture$Type;)V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v0

    check-cast v0, Lcom/jme3/asset/TextureKey;

    invoke-virtual {v0}, Lcom/jme3/asset/TextureKey;->isFlipY()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/jme3/texture/plugins/DDSLoader;->readData(Z)Ljava/util/ArrayList;

    move-result-object v5

    new-instance v0, Lcom/jme3/texture/Image;

    iget-object v1, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    iget v2, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    iget v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    iget-object v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    invoke-direct/range {v0 .. v6}, Lcom/jme3/texture/Image;-><init>(Lcom/jme3/texture/Image$Format;IIILjava/util/ArrayList;[I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :cond_2
    return-object v0

    :cond_3
    :try_start_1
    iget v0, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v0

    check-cast v0, Lcom/jme3/asset/TextureKey;

    sget-object v1, Lcom/jme3/texture/Texture$Type;->CubeMap:Lcom/jme3/texture/Texture$Type;

    invoke-virtual {v0, v1}, Lcom/jme3/asset/TextureKey;->setTextureTypeHint(Lcom/jme3/texture/Texture$Type;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v0
.end method

.method public readDXT2D(ZI)Ljava/nio/ByteBuffer;
    .locals 10
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x1

    sget-object v7, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v8, "Source image format: DXT"

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    const/4 v3, 0x0

    :goto_0
    iget v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-ge v3, v7, :cond_1

    if-eqz p1, :cond_0

    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v7, v7, v3

    new-array v1, v7, [B

    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v7, v1}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    invoke-static {v6, v5, v4, v7}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT(Ljava/nio/ByteBuffer;IILcom/jme3/texture/Image$Format;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    :goto_1
    div-int/lit8 v7, v5, 0x2

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v7, v4, 0x2

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v7, v7, v3

    new-array v1, v7, [B

    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v7, v1}, Ljava/io/DataInput;->readFully([B)V

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    return-object v0
.end method

.method public readDXT3D(ZI)Ljava/nio/ByteBuffer;
    .locals 12
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x1

    sget-object v9, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v10, "Source image format: DXT"

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    iget v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    mul-int/2addr v9, p2

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x0

    :goto_0
    iget v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    if-ge v4, v9, :cond_2

    invoke-static {p2}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    const/4 v5, 0x0

    :goto_1
    iget v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-ge v5, v9, :cond_1

    if-eqz p1, :cond_0

    iget-object v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v9, v9, v5

    new-array v2, v9, [B

    iget-object v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v9, v2}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    invoke-static {v8, v7, v6, v9}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT(Ljava/nio/ByteBuffer;IILcom/jme3/texture/Image$Format;)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    :goto_2
    div-int/lit8 v9, v7, 0x2

    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v7

    div-int/lit8 v9, v6, 0x2

    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    iget-object v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v9, v9, v5

    new-array v2, v9, [B

    iget-object v9, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v9, v2}, Ljava/io/DataInput;->readFully([B)V

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public readData(Z)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    array-length v3, v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v3, v3, v1

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    if-nez v3, :cond_3

    const/4 v1, 0x0

    :goto_1
    iget v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    if-ge v1, v3, :cond_4

    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readDXT2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readGrayscale2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readRGB2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->texture3D:Z

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    if-eqz v3, :cond_5

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readDXT3D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_3
    return-object v0

    :cond_5
    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    if-eqz v3, :cond_6

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readGrayscale3D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readRGB3D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->compressed:Z

    if-eqz v3, :cond_8

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readDXT2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    iget-boolean v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->grayscaleOrAlpha:Z

    if-eqz v3, :cond_9

    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readGrayscale2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    invoke-virtual {p0, p1, v2}, Lcom/jme3/texture/plugins/DDSLoader;->readRGB2D(ZI)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public readGrayscale2D(ZI)Ljava/nio/ByteBuffer;
    .locals 8
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x1

    invoke-static {p2}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    sget-object v5, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v6, "Source image format: R8"

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_0
    sget-boolean v5, Lcom/jme3/texture/plugins/DDSLoader;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    iget-object v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    invoke-virtual {v6}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v6

    if-eq v5, v6, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_1
    iget v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v3, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    const/4 v2, 0x0

    :goto_0
    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-ge v2, v5, :cond_3

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v5, v5, v2

    new-array v1, v5, [B

    iget-object v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v5, v1}, Ljava/io/DataInput;->readFully([B)V

    if-eqz p1, :cond_2

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    mul-int/2addr v5, v4

    div-int/lit8 v5, v5, 0x8

    invoke-virtual {p0, v1, v5, v3}, Lcom/jme3/texture/plugins/DDSLoader;->flipData([BII)[B

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    div-int/lit8 v5, v4, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    div-int/lit8 v5, v3, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public readGrayscale3D(ZI)Ljava/nio/ByteBuffer;
    .locals 9
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x1

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    mul-int/2addr v6, p2

    invoke-static {v6}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    const/16 v7, 0x8

    if-ne v6, v7, :cond_0

    sget-object v6, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v7, "Source image format: R8"

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_0
    sget-boolean v6, Lcom/jme3/texture/plugins/DDSLoader;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    iget-object v7, p0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    invoke-virtual {v7}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v7

    if-eq v6, v7, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    if-ge v2, v6, :cond_4

    iget v5, p0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    iget v4, p0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    const/4 v3, 0x0

    :goto_1
    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    if-ge v3, v6, :cond_3

    iget-object v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->sizes:[I

    aget v6, v6, v3

    new-array v1, v6, [B

    iget-object v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    invoke-interface {v6, v1}, Ljava/io/DataInput;->readFully([B)V

    if-eqz p1, :cond_2

    iget v6, p0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    mul-int/2addr v6, v5

    div-int/lit8 v6, v6, 0x8

    invoke-virtual {p0, v1, v6, v4}, Lcom/jme3/texture/plugins/DDSLoader;->flipData([BII)[B

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    div-int/lit8 v6, v5, 0x2

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v6, v4, 0x2

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    return-object v0
.end method

.method public readRGB2D(ZI)Ljava/nio/ByteBuffer;
    .locals 23
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v21, v0

    const/high16 v22, 0xff0000

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v21, v0

    const v22, 0xff00

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v21, v0

    const/16 v22, 0xff

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v21, v0

    const/high16 v22, -0x1000000

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v21, v0

    const/16 v22, 0x20

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    sget-object v21, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v22, "Data source format: BGRA8"

    invoke-virtual/range {v21 .. v22}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v21, v0

    div-int/lit8 v17, v21, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v21

    div-int/lit8 v18, v21, 0x8

    invoke-static/range {p2 .. p2}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    const/4 v14, 0x0

    move/from16 v0, v17

    new-array v4, v0, [B

    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v11, v0, :cond_6

    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    if-ge v0, v12, :cond_5

    const/16 v19, 0x0

    :goto_3
    move/from16 v0, v19

    if-ge v0, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v4}, Lcom/jme3/texture/plugins/DDSLoader;->byte2int([B)I

    move-result v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v21, v0

    and-int v21, v21, v10

    shr-int v21, v21, v16

    move/from16 v0, v21

    int-to-byte v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v21, v0

    and-int v21, v21, v10

    shr-int v21, v21, v9

    move/from16 v0, v21

    int-to-byte v8, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v21, v0

    and-int v21, v21, v10

    shr-int v21, v21, v6

    move/from16 v0, v21

    int-to-byte v5, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v21, v0

    and-int v21, v21, v10

    shr-int v21, v21, v3

    move/from16 v0, v21

    int-to-byte v2, v0

    if-eqz p1, :cond_1

    sub-int v21, v12, v20

    add-int/lit8 v21, v21, -0x1

    mul-int v21, v21, v13

    add-int v21, v21, v19

    mul-int v21, v21, v18

    add-int v21, v21, v14

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v21, v0

    if-nez v21, :cond_3

    invoke-virtual {v7, v15}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    :goto_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v21, v0

    const/16 v22, 0x18

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    sget-object v21, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v22, "Data source format: BGR8"

    invoke-virtual/range {v21 .. v22}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v7, v15}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_4

    :cond_4
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_2

    :cond_5
    mul-int v21, v13, v12

    mul-int v21, v21, v18

    add-int v14, v14, v21

    div-int/lit8 v21, v13, 0x2

    const/16 v22, 0x1

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v13

    div-int/lit8 v21, v12, 0x2

    const/16 v22, 0x1

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v12

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    :cond_6
    return-object v7
.end method

.method public readRGB3D(ZI)Ljava/nio/ByteBuffer;
    .locals 24
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/jme3/texture/plugins/DDSLoader;->count(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v22, v0

    const/high16 v23, 0xff0000

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v22, v0

    const v23, 0xff00

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v22, v0

    const/16 v23, 0xff

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v22, v0

    const/high16 v23, -0x1000000

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v22, v0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    sget-object v22, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v23, "Data source format: BGRA8"

    invoke-virtual/range {v22 .. v23}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v22, v0

    div-int/lit8 v18, v22, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->pixelFormat:Lcom/jme3/texture/Image$Format;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v22

    div-int/lit8 v19, v22, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    move/from16 v22, v0

    mul-int v22, v22, p2

    invoke-static/range {v22 .. v22}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->depth:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v11, v0, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/jme3/texture/plugins/DDSLoader;->width:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/jme3/texture/plugins/DDSLoader;->height:I

    mul-int v15, v11, p2

    move/from16 v0, v18

    new-array v4, v0, [B

    const/4 v12, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->mipMapCount:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v12, v0, :cond_6

    const/16 v21, 0x0

    :goto_3
    move/from16 v0, v21

    if-ge v0, v13, :cond_5

    const/16 v20, 0x0

    :goto_4
    move/from16 v0, v20

    if-ge v0, v14, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->in:Ljava/io/DataInput;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v4}, Lcom/jme3/texture/plugins/DDSLoader;->byte2int([B)I

    move-result v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->redMask:I

    move/from16 v22, v0

    and-int v22, v22, v10

    shr-int v22, v22, v17

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->greenMask:I

    move/from16 v22, v0

    and-int v22, v22, v10

    shr-int v22, v22, v9

    move/from16 v0, v22

    int-to-byte v8, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->blueMask:I

    move/from16 v22, v0

    and-int v22, v22, v10

    shr-int v22, v22, v6

    move/from16 v0, v22

    int-to-byte v5, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v22, v0

    and-int v22, v22, v10

    shr-int v22, v22, v3

    move/from16 v0, v22

    int-to-byte v2, v0

    if-eqz p1, :cond_1

    sub-int v22, v13, v21

    add-int/lit8 v22, v22, -0x1

    mul-int v22, v22, v14

    add-int v22, v22, v20

    mul-int v22, v22, v19

    add-int v22, v22, v15

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->alphaMask:I

    move/from16 v22, v0

    if-nez v22, :cond_3

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    :goto_5
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/jme3/texture/plugins/DDSLoader;->bpp:I

    move/from16 v22, v0

    const/16 v23, 0x18

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    sget-object v22, Lcom/jme3/texture/plugins/DDSLoader;->logger:Ljava/util/logging/Logger;

    const-string v23, "Data source format: BGR8"

    invoke-virtual/range {v22 .. v23}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_4
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    :cond_5
    mul-int v22, v14, v13

    mul-int v22, v22, v19

    add-int v15, v15, v22

    div-int/lit8 v22, v14, 0x2

    const/16 v23, 0x1

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    move-result v14

    div-int/lit8 v22, v13, 0x2

    const/16 v23, 0x1

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    move-result v13

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    return-object v7
.end method
