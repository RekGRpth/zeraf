.class public final Lcom/jme3/animation/Skeleton;
.super Ljava/lang/Object;
.source "Skeleton.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# instance fields
.field private boneList:[Lcom/jme3/animation/Bone;

.field private rootBones:[Lcom/jme3/animation/Bone;

.field private transient skinningMatrixes:[Lcom/jme3/math/Matrix4f;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jme3/animation/Skeleton;)V
    .locals 5
    .param p1    # Lcom/jme3/animation/Skeleton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v1, p1, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v2, v1

    new-array v2, v2, [Lcom/jme3/animation/Bone;

    iput-object v2, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    new-instance v3, Lcom/jme3/animation/Bone;

    aget-object v4, v1, v0

    invoke-direct {v3, v4}, Lcom/jme3/animation/Bone;-><init>(Lcom/jme3/animation/Bone;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p1, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v2, v2

    new-array v2, v2, [Lcom/jme3/animation/Bone;

    iput-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    iget-object v3, p1, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v3, v3, v0

    invoke-direct {p0, v3}, Lcom/jme3/animation/Skeleton;->recreateBoneStructure(Lcom/jme3/animation/Bone;)Lcom/jme3/animation/Bone;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/jme3/animation/Skeleton;->createSkinningMatrices()V

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    :goto_2
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/jme3/animation/Bone;->update()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method public constructor <init>([Lcom/jme3/animation/Bone;)V
    .locals 5
    .param p1    # [Lcom/jme3/animation/Bone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, p1

    add-int/lit8 v1, v4, -0x1

    :goto_0
    if-ltz v1, :cond_1

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/jme3/animation/Bone;->getParent()Lcom/jme3/animation/Bone;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/jme3/animation/Bone;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/jme3/animation/Bone;

    iput-object v4, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    invoke-direct {p0}, Lcom/jme3/animation/Skeleton;->createSkinningMatrices()V

    iget-object v4, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v4, v4

    add-int/lit8 v1, v4, -0x1

    :goto_1
    if-ltz v1, :cond_2

    iget-object v4, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lcom/jme3/animation/Bone;->update()V

    invoke-virtual {v2}, Lcom/jme3/animation/Bone;->setBindingPose()V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private createSkinningMatrices()V
    .locals 3

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v1, v1

    new-array v1, v1, [Lcom/jme3/math/Matrix4f;

    iput-object v1, p0, Lcom/jme3/animation/Skeleton;->skinningMatrixes:[Lcom/jme3/math/Matrix4f;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->skinningMatrixes:[Lcom/jme3/math/Matrix4f;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->skinningMatrixes:[Lcom/jme3/math/Matrix4f;

    new-instance v2, Lcom/jme3/math/Matrix4f;

    invoke-direct {v2}, Lcom/jme3/math/Matrix4f;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private recreateBoneStructure(Lcom/jme3/animation/Bone;)Lcom/jme3/animation/Bone;
    .locals 6
    .param p1    # Lcom/jme3/animation/Bone;

    invoke-virtual {p1}, Lcom/jme3/animation/Bone;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jme3/animation/Skeleton;->getBone(Ljava/lang/String;)Lcom/jme3/animation/Bone;

    move-result-object v4

    invoke-virtual {p1}, Lcom/jme3/animation/Bone;->getChildren()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/animation/Bone;

    invoke-virtual {v2}, Lcom/jme3/animation/Bone;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jme3/animation/Skeleton;->getBone(Ljava/lang/String;)Lcom/jme3/animation/Bone;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/jme3/animation/Bone;->addChild(Lcom/jme3/animation/Bone;)V

    invoke-direct {p0, v2}, Lcom/jme3/animation/Skeleton;->recreateBoneStructure(Lcom/jme3/animation/Bone;)Lcom/jme3/animation/Bone;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v4
.end method


# virtual methods
.method public computeSkinningMatrices()[Lcom/jme3/math/Matrix4f;
    .locals 8

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v7

    const/4 v6, 0x0

    :goto_0
    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->skinningMatrixes:[Lcom/jme3/math/Matrix4f;

    aget-object v1, v1, v6

    iget-object v2, v7, Lcom/jme3/util/TempVars;->quat1:Lcom/jme3/math/Quaternion;

    iget-object v3, v7, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v4, v7, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v5, v7, Lcom/jme3/util/TempVars;->tempMat3:Lcom/jme3/math/Matrix3f;

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/animation/Bone;->getOffsetTransform(Lcom/jme3/math/Matrix4f;Lcom/jme3/math/Quaternion;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Matrix3f;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v7}, Lcom/jme3/util/TempVars;->release()V

    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->skinningMatrixes:[Lcom/jme3/math/Matrix4f;

    return-object v0
.end method

.method public getBone(I)Lcom/jme3/animation/Bone;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getBone(Ljava/lang/String;)Lcom/jme3/animation/Bone;
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/jme3/animation/Bone;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    aget-object v1, v1, v0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getBoneIndex(Lcom/jme3/animation/Bone;)I
    .locals 2
    .param p1    # Lcom/jme3/animation/Bone;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 11
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v9, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v4

    const-string v7, "rootBones"

    invoke-interface {v4, v7, v10}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v2

    array-length v7, v2

    new-array v7, v7, [Lcom/jme3/animation/Bone;

    iput-object v7, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    iget-object v7, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v8, v2

    invoke-static {v2, v9, v7, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v7, "boneList"

    invoke-interface {v4, v7, v10}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v1

    array-length v7, v1

    new-array v7, v7, [Lcom/jme3/animation/Bone;

    iput-object v7, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    iget-object v7, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v8, v1

    invoke-static {v1, v9, v7, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0}, Lcom/jme3/animation/Skeleton;->createSkinningMatrices()V

    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v0, v3

    invoke-virtual {v6}, Lcom/jme3/animation/Bone;->update()V

    invoke-virtual {v6}, Lcom/jme3/animation/Bone;->setBindingPose()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final reset()V
    .locals 2

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/jme3/animation/Bone;->reset()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final resetAndUpdate()V
    .locals 3

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v1, v2, v0

    invoke-virtual {v1}, Lcom/jme3/animation/Bone;->reset()V

    invoke-virtual {v1}, Lcom/jme3/animation/Bone;->update()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skeleton - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/jme3/animation/Skeleton;->boneList:[Lcom/jme3/animation/Bone;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bones, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " roots\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/jme3/animation/Bone;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public updateWorldVectors()V
    .locals 2

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/animation/Skeleton;->rootBones:[Lcom/jme3/animation/Bone;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/jme3/animation/Bone;->update()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method
