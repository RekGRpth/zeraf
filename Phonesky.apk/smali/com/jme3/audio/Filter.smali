.class public abstract Lcom/jme3/audio/Filter;
.super Lcom/jme3/util/NativeObject;
.source "Filter.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/jme3/audio/Filter;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public deleteObject(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/audio/AudioRenderer;

    invoke-interface {p1, p0}, Lcom/jme3/audio/AudioRenderer;->deleteFilter(Lcom/jme3/audio/Filter;)V

    return-void
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 0
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public resetObject()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/jme3/audio/Filter;->id:I

    invoke-virtual {p0}, Lcom/jme3/audio/Filter;->setUpdateNeeded()V

    return-void
.end method
