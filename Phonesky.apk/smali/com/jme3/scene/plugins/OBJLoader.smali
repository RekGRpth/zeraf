.class public final Lcom/jme3/scene/plugins/OBJLoader;
.super Ljava/lang/Object;
.source "OBJLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/scene/plugins/OBJLoader$Face;,
        Lcom/jme3/scene/plugins/OBJLoader$Vertex;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected assetManager:Lcom/jme3/asset/AssetManager;

.field protected curIndex:I

.field protected currentMatName:Ljava/lang/String;

.field protected final faces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Face;",
            ">;"
        }
    .end annotation
.end field

.field protected geomIndex:I

.field protected final indexVertMap:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Vertex;",
            ">;"
        }
    .end annotation
.end field

.field protected key:Lcom/jme3/asset/ModelKey;

.field protected final matFaces:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Face;",
            ">;>;"
        }
    .end annotation
.end field

.field protected matList:Lcom/jme3/material/MaterialList;

.field protected final norms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/math/Vector3f;",
            ">;"
        }
    .end annotation
.end field

.field protected objName:Ljava/lang/String;

.field protected objNode:Lcom/jme3/scene/Node;

.field protected objectIndex:I

.field protected scan:Ljava/util/Scanner;

.field protected final texCoords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/math/Vector2f;",
            ">;"
        }
    .end annotation
.end field

.field protected final vertIndexMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Vertex;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private vertList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Vertex;",
            ">;"
        }
    .end annotation
.end field

.field protected final verts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/math/Vector3f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/scene/plugins/OBJLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/scene/plugins/OBJLoader;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/scene/plugins/OBJLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/scene/plugins/OBJLoader;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x64

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->verts:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->texCoords:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->norms:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->faces:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0, v2}, Lcom/jme3/util/IntMap;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->indexVertMap:Lcom/jme3/util/IntMap;

    iput v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->curIndex:I

    iput v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->objectIndex:I

    iput v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->geomIndex:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected constructMesh(Ljava/util/ArrayList;)Lcom/jme3/scene/Mesh;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Face;",
            ">;)",
            "Lcom/jme3/scene/Mesh;"
        }
    .end annotation

    new-instance v13, Lcom/jme3/scene/Mesh;

    invoke-direct {v13}, Lcom/jme3/scene/Mesh;-><init>()V

    sget-object v25, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Lcom/jme3/scene/Mesh;->setMode(Lcom/jme3/scene/Mesh$Mode;)V

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v14, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    if-ge v7, v0, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/scene/plugins/OBJLoader$Face;

    iget-object v3, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    array-length v12, v3

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v12, :cond_2

    aget-object v21, v3, v8

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/jme3/scene/plugins/OBJLoader;->findVertexIndex(Lcom/jme3/scene/plugins/OBJLoader$Vertex;)V

    if-nez v6, :cond_0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    const/4 v6, 0x1

    :cond_0
    if-nez v5, :cond_1

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    const/4 v5, 0x1

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/jme3/scene/plugins/OBJLoader;->quadToTriangle(Lcom/jme3/scene/plugins/OBJLoader$Face;)[Lcom/jme3/scene/plugins/OBJLoader$Face;

    move-result-object v19

    const/16 v25, 0x0

    aget-object v25, v19, v25

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v25, 0x1

    aget-object v25, v19, v25

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x3

    invoke-static/range {v25 .. v25}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v17

    const/4 v15, 0x0

    const/16 v20, 0x0

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x3

    invoke-static/range {v25 .. v25}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v15

    sget-object v25, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v26, 0x3

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v13, v0, v1, v15}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    :cond_5
    if-eqz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x2

    invoke-static/range {v25 .. v25}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v20

    sget-object v25, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v26, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v20

    invoke-virtual {v13, v0, v1, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    :cond_6
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->size()I

    move-result v25

    const/high16 v26, 0x10000

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_7

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x3

    invoke-static/range {v25 .. v25}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v9

    sget-object v25, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v26, 0x3

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v13, v0, v1, v9}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/IntBuffer;)V

    new-instance v11, Lcom/jme3/scene/mesh/IndexIntBuffer;

    invoke-direct {v11, v9}, Lcom/jme3/scene/mesh/IndexIntBuffer;-><init>(Ljava/nio/IntBuffer;)V

    :goto_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v16

    const/4 v7, 0x0

    :goto_4
    move/from16 v0, v16

    if-ge v7, v0, :cond_b

    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/scene/plugins/OBJLoader$Face;

    iget-object v0, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_8

    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v25

    mul-int/lit8 v25, v25, 0x3

    invoke-static/range {v25 .. v25}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v18

    sget-object v25, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v26, 0x3

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v18

    invoke-virtual {v13, v0, v1, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/ShortBuffer;)V

    new-instance v11, Lcom/jme3/scene/mesh/IndexShortBuffer;

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Lcom/jme3/scene/mesh/IndexShortBuffer;-><init>(Ljava/nio/ShortBuffer;)V

    goto :goto_3

    :cond_8
    iget-object v0, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aget-object v22, v25, v26

    iget-object v0, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    aget-object v23, v25, v26

    iget-object v0, v4, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    aget-object v24, v25, v26

    move-object/from16 v0, v22

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    if-eqz v15, :cond_9

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, v22

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x3

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :cond_9
    if-eqz v20, :cond_a

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v25, v0

    if-eqz v25, :cond_a

    move-object/from16 v0, v22

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v25

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :cond_a
    mul-int/lit8 v10, v7, 0x3

    move-object/from16 v0, v22

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v11, v10, v0}, Lcom/jme3/scene/mesh/IndexBuffer;->put(II)V

    add-int/lit8 v25, v10, 0x1

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Lcom/jme3/scene/mesh/IndexBuffer;->put(II)V

    add-int/lit8 v25, v10, 0x2

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Lcom/jme3/scene/mesh/IndexBuffer;->put(II)V

    goto/16 :goto_5

    :cond_b
    sget-object v25, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v26, 0x3

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v17

    invoke-virtual {v13, v0, v1, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    invoke-virtual {v13}, Lcom/jme3/scene/Mesh;->setStatic()V

    invoke-virtual {v13}, Lcom/jme3/scene/Mesh;->updateBound()V

    invoke-virtual {v13}, Lcom/jme3/scene/Mesh;->updateCounts()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/scene/plugins/OBJLoader;->indexVertMap:Lcom/jme3/util/IntMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/jme3/util/IntMap;->clear()V

    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/jme3/scene/plugins/OBJLoader;->curIndex:I

    return-object v13
.end method

.method protected createGeometry(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/jme3/scene/Geometry;
    .locals 7
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/scene/plugins/OBJLoader$Face;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/jme3/scene/Geometry;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/io/IOException;

    const-string v4, "No geometry data to generate mesh"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jme3/scene/plugins/OBJLoader;->constructMesh(Ljava/util/ArrayList;)Lcom/jme3/scene/Mesh;

    move-result-object v2

    new-instance v0, Lcom/jme3/scene/Geometry;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-geom-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->geomIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/jme3/scene/plugins/OBJLoader;->geomIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v2}, Lcom/jme3/scene/Geometry;-><init>(Ljava/lang/String;Lcom/jme3/scene/Mesh;)V

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    invoke-virtual {v3, p2}, Lcom/jme3/material/MaterialList;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/material/Material;

    :cond_1
    if-nez v1, :cond_2

    new-instance v1, Lcom/jme3/material/Material;

    iget-object v3, p0, Lcom/jme3/scene/plugins/OBJLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    const-string v4, "Common/MatDefs/Light/Lighting.j3md"

    invoke-direct {v1, v3, v4}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/asset/AssetManager;Ljava/lang/String;)V

    const-string v3, "Shininess"

    const/high16 v4, 0x42800000

    invoke-virtual {v1, v3, v4}, Lcom/jme3/material/Material;->setFloat(Ljava/lang/String;F)V

    :cond_2
    invoke-virtual {v0, v1}, Lcom/jme3/scene/Geometry;->setMaterial(Lcom/jme3/material/Material;)V

    invoke-virtual {v1}, Lcom/jme3/material/Material;->isTransparent()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Transparent:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-virtual {v0, v3}, Lcom/jme3/scene/Geometry;->setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V

    :goto_0
    invoke-virtual {v1}, Lcom/jme3/material/Material;->getMaterialDef()Lcom/jme3/material/MaterialDef;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/material/MaterialDef;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Lighting"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v2, v3}, Lcom/jme3/scene/Mesh;->getFloatBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Ljava/nio/FloatBuffer;

    move-result-object v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/jme3/scene/plugins/OBJLoader;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "OBJ mesh {0} doesn\'t contain normals! It might not display correctly"

    invoke-virtual {v0}, Lcom/jme3/scene/Geometry;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    return-object v0

    :cond_4
    sget-object v3, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Opaque:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-virtual {v0, v3}, Lcom/jme3/scene/Geometry;->setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V

    goto :goto_0
.end method

.method protected findVertexIndex(Lcom/jme3/scene/plugins/OBJLoader$Vertex;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iget-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p1, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->curIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->curIndex:I

    iput v1, p1, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    iget-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    iget v2, p1, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->indexVertMap:Lcom/jme3/util/IntMap;

    iget v2, p1, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->index:I

    invoke-virtual {v1, v2, p1}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 11
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->reset()V

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v7

    check-cast v7, Lcom/jme3/asset/ModelKey;

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getManager()Lcom/jme3/asset/AssetManager;

    move-result-object v7

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    invoke-virtual {v7}, Lcom/jme3/asset/ModelKey;->getName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    invoke-virtual {v7}, Lcom/jme3/asset/ModelKey;->getFolder()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    invoke-virtual {v7}, Lcom/jme3/asset/ModelKey;->getExtension()Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    iget-object v8, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    :cond_0
    new-instance v7, Lcom/jme3/scene/Node;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/jme3/scene/plugins/OBJLoader;->objName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-objnode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/jme3/scene/Node;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v7

    instance-of v7, v7, Lcom/jme3/asset/ModelKey;

    if-nez v7, :cond_1

    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Model assets must be loaded using a ModelKey"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v5

    new-instance v7, Ljava/util/Scanner;

    invoke-direct {v7, v5}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    iput-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/util/Scanner;->useLocale(Ljava/util/Locale;)Ljava/util/Scanner;

    :cond_2
    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->readLine()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_2

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_3
    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    if-lez v7, :cond_6

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, v6, v7}, Lcom/jme3/scene/plugins/OBJLoader;->createGeometry(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/jme3/scene/Geometry;

    move-result-object v3

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    invoke-virtual {v7, v3}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    goto :goto_0

    :catchall_0
    move-exception v7

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_5
    throw v7

    :cond_6
    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->faces:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_7

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->faces:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/jme3/scene/plugins/OBJLoader;->createGeometry(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/jme3/scene/Geometry;

    move-result-object v3

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    invoke-virtual {v7, v3}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    :cond_7
    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    invoke-virtual {v7}, Lcom/jme3/scene/Node;->getQuantity()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    invoke-virtual {v7, v10}, Lcom/jme3/scene/Node;->getChild(I)Lcom/jme3/scene/Spatial;

    move-result-object v7

    :goto_1
    return-object v7

    :cond_8
    iget-object v7, p0, Lcom/jme3/scene/plugins/OBJLoader;->objNode:Lcom/jme3/scene/Node;

    goto :goto_1
.end method

.method protected loadMtlLib(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".mtl"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Expected .mtl file! Got: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/jme3/asset/AssetKey;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    invoke-virtual {v5}, Lcom/jme3/asset/ModelKey;->getFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v4, v3}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/material/MaterialList;

    iput-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;
    :try_end_0
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    invoke-virtual {v4}, Lcom/jme3/material/MaterialList;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v4, Lcom/jme3/scene/plugins/OBJLoader;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v6, "Cannot locate {0} for model {1}"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/jme3/scene/plugins/OBJLoader;->key:Lcom/jme3/asset/ModelKey;

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected nextStatement()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    const-string v2, ".*\r{0,1}\n"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->skip(Ljava/lang/String;)Ljava/util/Scanner;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected quadToTriangle(Lcom/jme3/scene/plugins/OBJLoader$Face;)[Lcom/jme3/scene/plugins/OBJLoader$Face;
    .locals 13
    .param p1    # Lcom/jme3/scene/plugins/OBJLoader$Face;

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    sget-boolean v7, Lcom/jme3/scene/plugins/OBJLoader;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-object v7, p1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    array-length v7, v7

    const/4 v8, 0x4

    if-eq v7, v8, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    :cond_0
    new-array v2, v11, [Lcom/jme3/scene/plugins/OBJLoader$Face;

    new-instance v7, Lcom/jme3/scene/plugins/OBJLoader$Face;

    invoke-direct {v7}, Lcom/jme3/scene/plugins/OBJLoader$Face;-><init>()V

    aput-object v7, v2, v9

    new-instance v7, Lcom/jme3/scene/plugins/OBJLoader$Face;

    invoke-direct {v7}, Lcom/jme3/scene/plugins/OBJLoader$Face;-><init>()V

    aput-object v7, v2, v10

    aget-object v7, v2, v9

    new-array v8, v12, [Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iput-object v8, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aget-object v7, v2, v10

    new-array v8, v12, [Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iput-object v8, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iget-object v7, p1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aget-object v3, v7, v9

    iget-object v7, p1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aget-object v4, v7, v10

    iget-object v7, p1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aget-object v5, v7, v11

    iget-object v7, p1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aget-object v6, v7, v12

    iget-object v7, v3, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    iget-object v8, v5, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    invoke-virtual {v7, v8}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v0

    iget-object v7, v4, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    iget-object v8, v6, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    invoke-virtual {v7, v8}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v1

    cmpg-float v7, v0, v1

    if-gez v7, :cond_1

    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v3, v7, v9

    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v4, v7, v10

    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v6, v7, v11

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v4, v7, v9

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v5, v7, v10

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v6, v7, v11

    :goto_0
    return-object v2

    :cond_1
    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v3, v7, v9

    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v4, v7, v10

    aget-object v7, v2, v9

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v5, v7, v11

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v3, v7, v9

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v5, v7, v10

    aget-object v7, v2, v10

    iget-object v7, v7, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v6, v7, v11

    goto :goto_0
.end method

.method protected readFace()V
    .locals 15

    new-instance v1, Lcom/jme3/scene/plugins/OBJLoader$Face;

    invoke-direct {v1}, Lcom/jme3/scene/plugins/OBJLoader$Face;-><init>()V

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v13}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v13, "\\s"

    invoke-virtual {v5, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_6

    aget-object v8, v0, v3

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const-string v13, "/"

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v13, v6

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    const/4 v13, 0x0

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    :cond_0
    :goto_1
    new-instance v12, Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    invoke-direct {v12}, Lcom/jme3/scene/plugins/OBJLoader$Vertex;-><init>()V

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->verts:Ljava/util/ArrayList;

    add-int/lit8 v14, v7, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/jme3/math/Vector3f;

    iput-object v13, v12, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->v:Lcom/jme3/math/Vector3f;

    if-lez v11, :cond_1

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->texCoords:Ljava/util/ArrayList;

    add-int/lit8 v14, v11, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/jme3/math/Vector2f;

    iput-object v13, v12, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vt:Lcom/jme3/math/Vector2f;

    :cond_1
    if-lez v10, :cond_2

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->norms:Ljava/util/ArrayList;

    add-int/lit8 v14, v10, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/jme3/math/Vector3f;

    iput-object v13, v12, Lcom/jme3/scene/plugins/OBJLoader$Vertex;->vn:Lcom/jme3/math/Vector3f;

    :cond_2
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    array-length v13, v6

    const/4 v14, 0x2

    if-ne v13, v14, :cond_4

    const/4 v13, 0x0

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v13, 0x1

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    :cond_4
    array-length v13, v6

    const/4 v14, 0x3

    if-ne v13, v14, :cond_5

    const/4 v13, 0x1

    aget-object v13, v6, v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_5

    const/4 v13, 0x0

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v13, 0x1

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/4 v13, 0x2

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    :cond_5
    array-length v13, v6

    const/4 v14, 0x3

    if-ne v13, v14, :cond_0

    const/4 v13, 0x0

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v13, 0x2

    aget-object v13, v6, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    :cond_6
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x4

    if-gt v13, v14, :cond_7

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x2

    if-gt v13, v14, :cond_8

    :cond_7
    sget-object v13, Lcom/jme3/scene/plugins/OBJLoader;->logger:Ljava/util/logging/Logger;

    const-string v14, "Edge or polygon detected in OBJ. Ignored."

    invoke-virtual {v13, v14}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_8
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iput-object v13, v1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    const/4 v2, 0x0

    :goto_2
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v2, v13, :cond_9

    iget-object v14, v1, Lcom/jme3/scene/plugins/OBJLoader$Face;->verticies:[Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertList:Ljava/util/ArrayList;

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/jme3/scene/plugins/OBJLoader$Vertex;

    aput-object v13, v14, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    if-eqz v13, :cond_a

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    iget-object v14, p0, Lcom/jme3/scene/plugins/OBJLoader;->currentMatName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    iget-object v14, p0, Lcom/jme3/scene/plugins/OBJLoader;->currentMatName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    return-void

    :cond_a
    iget-object v13, p0, Lcom/jme3/scene/plugins/OBJLoader;->faces:Ljava/util/ArrayList;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method protected readLine()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->nextStatement()Z

    move-result v2

    goto :goto_0

    :cond_1
    const-string v2, "v"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->verts:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->readVector3()Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "vn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->norms:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->readVector3()Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v2, "vt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->texCoords:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->readVector2()Lcom/jme3/math/Vector2f;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v2, "f"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->readFace()V

    goto :goto_1

    :cond_5
    const-string v2, "usemtl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->currentMatName:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v2, "mtllib"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jme3/scene/plugins/OBJLoader;->loadMtlLib(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v2, "s"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->nextStatement()Z

    move-result v2

    goto/16 :goto_0

    :cond_9
    sget-object v2, Lcom/jme3/scene/plugins/OBJLoader;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Unknown statement in OBJ! {0}"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/OBJLoader;->nextStatement()Z

    move-result v2

    goto/16 :goto_0
.end method

.method protected readVector2()Lcom/jme3/math/Vector2f;
    .locals 4

    new-instance v2, Lcom/jme3/math/Vector2f;

    invoke-direct {v2}, Lcom/jme3/math/Vector2f;-><init>()V

    iget-object v3, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, "\\s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jme3/math/Vector2f;->setX(F)Lcom/jme3/math/Vector2f;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jme3/math/Vector2f;->setY(F)Lcom/jme3/math/Vector2f;

    return-object v2
.end method

.method protected readVector3()Lcom/jme3/math/Vector3f;
    .locals 4

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iget-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iget-object v3, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->verts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->texCoords:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->norms:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->faces:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->matFaces:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->vertIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/OBJLoader;->indexVertMap:Lcom/jme3/util/IntMap;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap;->clear()V

    iput-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->currentMatName:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->matList:Lcom/jme3/material/MaterialList;

    iput v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->curIndex:I

    iput v2, p0, Lcom/jme3/scene/plugins/OBJLoader;->geomIndex:I

    iput-object v1, p0, Lcom/jme3/scene/plugins/OBJLoader;->scan:Ljava/util/Scanner;

    return-void
.end method
