.class public Lcom/jme3/scene/mesh/IndexIntBuffer;
.super Lcom/jme3/scene/mesh/IndexBuffer;
.source "IndexIntBuffer.java"


# instance fields
.field private buf:Ljava/nio/IntBuffer;


# direct methods
.method public constructor <init>(Ljava/nio/IntBuffer;)V
    .locals 0
    .param p1    # Ljava/nio/IntBuffer;

    invoke-direct {p0}, Lcom/jme3/scene/mesh/IndexBuffer;-><init>()V

    iput-object p1, p0, Lcom/jme3/scene/mesh/IndexIntBuffer;->buf:Ljava/nio/IntBuffer;

    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/scene/mesh/IndexIntBuffer;->buf:Ljava/nio/IntBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    return v0
.end method

.method public getBuffer()Ljava/nio/Buffer;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/mesh/IndexIntBuffer;->buf:Ljava/nio/IntBuffer;

    return-object v0
.end method

.method public put(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jme3/scene/mesh/IndexIntBuffer;->buf:Ljava/nio/IntBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/mesh/IndexIntBuffer;->buf:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    return v0
.end method
