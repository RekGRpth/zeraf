.class public Lcom/jme3/bounding/BoundingBox;
.super Lcom/jme3/bounding/BoundingVolume;
.source "BoundingBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/bounding/BoundingBox$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field xExtent:F

.field yExtent:F

.field zExtent:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/bounding/BoundingBox;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jme3/bounding/BoundingBox;)V
    .locals 2
    .param p1    # Lcom/jme3/bounding/BoundingBox;

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    iget-object v0, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget-object v1, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v0, p1, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v0, p1, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;FFF)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    iget-object v0, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iput p2, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iput p3, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iput p4, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    invoke-virtual {p0, p1, p2}, Lcom/jme3/bounding/BoundingBox;->setMinMax(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    return-void
.end method

.method public static checkMinMax(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 2
    .param p0    # Lcom/jme3/math/Vector3f;
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    :cond_0
    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p1, Lcom/jme3/math/Vector3f;->x:F

    :cond_1
    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    :cond_2
    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p1, Lcom/jme3/math/Vector3f;->y:F

    :cond_3
    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    :cond_4
    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p1, Lcom/jme3/math/Vector3f;->z:F

    :cond_5
    return-void
.end method

.method private clip(FF[F)Z
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # [F

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    cmpl-float v2, p1, v3

    if-lez v2, :cond_2

    aget v2, p3, v0

    mul-float/2addr v2, p1

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v2, p3, v1

    mul-float/2addr v2, p1

    cmpl-float v2, p2, v2

    if-lez v2, :cond_0

    div-float v2, p2, p1

    aput v2, p3, v1

    goto :goto_0

    :cond_2
    cmpg-float v2, p1, v3

    if-gez v2, :cond_4

    aget v2, p3, v1

    mul-float/2addr v2, p1

    cmpl-float v2, p2, v2

    if-lez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    aget v1, p3, v0

    mul-float/2addr v1, p1

    cmpl-float v1, p2, v1

    if-lez v1, :cond_0

    div-float v1, p2, p1

    aput v1, p3, v0

    goto :goto_0

    :cond_4
    float-to-double v2, p2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private collideWithRay(Lcom/jme3/math/Ray;Lcom/jme3/collision/CollisionResults;)I
    .locals 15
    .param p1    # Lcom/jme3/math/Ray;
    .param p2    # Lcom/jme3/collision/CollisionResults;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v11

    iget-object v12, v11, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v12, v13}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v12

    iget-object v13, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v12, v13}, Lcom/jme3/math/Vector3f;->subtractLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget-object v12, v11, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v12, v13}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    const/4 v12, 0x2

    new-array v10, v12, [F

    fill-array-data v10, :array_0

    const/4 v12, 0x0

    aget v8, v10, v12

    const/4 v12, 0x1

    aget v9, v10, v12

    iget v12, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v13, v1, Lcom/jme3/math/Vector3f;->x:F

    neg-float v13, v13

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, v2, Lcom/jme3/math/Vector3f;->x:F

    neg-float v12, v12

    iget v13, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v13, v1, Lcom/jme3/math/Vector3f;->y:F

    neg-float v13, v13

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, v2, Lcom/jme3/math/Vector3f;->y:F

    neg-float v12, v12

    iget v13, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, v2, Lcom/jme3/math/Vector3f;->z:F

    iget v13, v1, Lcom/jme3/math/Vector3f;->z:F

    neg-float v13, v13

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, v2, Lcom/jme3/math/Vector3f;->z:F

    neg-float v12, v12

    iget v13, v1, Lcom/jme3/math/Vector3f;->z:F

    iget v14, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float/2addr v13, v14

    invoke-direct {p0, v12, v13, v10}, Lcom/jme3/bounding/BoundingBox;->clip(FF[F)Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v11}, Lcom/jme3/util/TempVars;->release()V

    if-eqz v4, :cond_3

    const/4 v12, 0x0

    aget v12, v10, v12

    cmpl-float v12, v12, v8

    if-nez v12, :cond_0

    const/4 v12, 0x1

    aget v12, v10, v12

    cmpl-float v12, v12, v9

    if-eqz v12, :cond_3

    :cond_0
    const/4 v12, 0x1

    aget v12, v10, v12

    const/4 v13, 0x0

    aget v13, v10, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_2

    move-object v3, v10

    const/4 v12, 0x2

    new-array v6, v12, [Lcom/jme3/math/Vector3f;

    const/4 v12, 0x0

    new-instance v13, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v13, v14}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    const/4 v14, 0x0

    aget v14, v3, v14

    invoke-virtual {v13, v14}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v13, v14}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v13

    aput-object v13, v6, v12

    const/4 v12, 0x1

    new-instance v13, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v13, v14}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    const/4 v14, 0x1

    aget v14, v3, v14

    invoke-virtual {v13, v14}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v13, v14}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v13

    aput-object v13, v6, v12

    new-instance v7, Lcom/jme3/collision/CollisionResult;

    const/4 v12, 0x0

    aget-object v12, v6, v12

    const/4 v13, 0x0

    aget v13, v3, v13

    invoke-direct {v7, v12, v13}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    new-instance v7, Lcom/jme3/collision/CollisionResult;

    const/4 v12, 0x1

    aget-object v12, v6, v12

    const/4 v13, 0x1

    aget v13, v3, v13

    invoke-direct {v7, v12, v13}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v12, 0x2

    :goto_1
    return v12

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    new-instance v12, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v12, v13}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    const/4 v13, 0x0

    aget v13, v10, v13

    invoke-virtual {v12, v13}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v12, v13}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    new-instance v7, Lcom/jme3/collision/CollisionResult;

    const/4 v12, 0x0

    aget v12, v10, v12

    invoke-direct {v7, v5, v12}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v12, 0x1

    goto :goto_1

    :cond_3
    const/4 v12, 0x0

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x7f800000
    .end array-data
.end method

.method private merge(Lcom/jme3/math/Vector3f;FFFLcom/jme3/bounding/BoundingBox;)Lcom/jme3/bounding/BoundingBox;
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # Lcom/jme3/bounding/BoundingBox;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v0

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->x:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v2, p2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v2, p2

    iput v2, v1, Lcom/jme3/math/Vector3f;->x:F

    :cond_0
    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->y:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v2, p3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v2, p3

    iput v2, v1, Lcom/jme3/math/Vector3f;->y:F

    :cond_1
    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->z:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v2, p4

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v2, p4

    iput v2, v1, Lcom/jme3/math/Vector3f;->z:F

    :cond_2
    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    add-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->x:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v2, p2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v2, p2

    iput v2, v1, Lcom/jme3/math/Vector3f;->x:F

    :cond_3
    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    add-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->y:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v2, p3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v2, p3

    iput v2, v1, Lcom/jme3/math/Vector3f;->y:F

    :cond_4
    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    add-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->z:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v2, p4

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v2, p4

    iput v2, v1, Lcom/jme3/math/Vector3f;->z:F

    :cond_5
    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget-object v2, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget-object v2, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    const/high16 v2, 0x3f000000

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget-object v1, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v0}, Lcom/jme3/util/TempVars;->release()V

    return-object p5
.end method


# virtual methods
.method public clone(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 6
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v2

    sget-object v3, Lcom/jme3/bounding/BoundingVolume$Type;->AABB:Lcom/jme3/bounding/BoundingVolume$Type;

    if-ne v2, v3, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/jme3/bounding/BoundingBox;

    iget-object v2, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget-object v3, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v3}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iput v2, v0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iput v2, v0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    iput v2, v0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->checkPlane:I

    iput v2, v0, Lcom/jme3/bounding/BoundingBox;->checkPlane:I

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/jme3/bounding/BoundingBox;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/jme3/bounding/BoundingBox;-><init>(Lcom/jme3/math/Vector3f;FFF)V

    move-object v1, v0

    goto :goto_0
.end method

.method public collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/collision/CollisionResults;)I
    .locals 6
    .param p1    # Lcom/jme3/collision/Collidable;
    .param p2    # Lcom/jme3/collision/CollisionResults;

    instance-of v3, p1, Lcom/jme3/math/Ray;

    if-eqz v3, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/jme3/math/Ray;

    invoke-direct {p0, v1, p2}, Lcom/jme3/bounding/BoundingBox;->collideWithRay(Lcom/jme3/math/Ray;Lcom/jme3/collision/CollisionResults;)I

    move-result v3

    :goto_0
    return v3

    :cond_0
    instance-of v3, p1, Lcom/jme3/math/Triangle;

    if-eqz v3, :cond_2

    move-object v2, p1

    check-cast v2, Lcom/jme3/math/Triangle;

    invoke-virtual {v2}, Lcom/jme3/math/Triangle;->get1()Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v2}, Lcom/jme3/math/Triangle;->get2()Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v2}, Lcom/jme3/math/Triangle;->get3()Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/jme3/bounding/BoundingBox;->intersects(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v0, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v0}, Lcom/jme3/collision/CollisionResult;-><init>()V

    invoke-virtual {p2, v0}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/jme3/collision/UnsupportedCollisionException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "With: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/jme3/collision/UnsupportedCollisionException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public computeFromPoints(Ljava/nio/FloatBuffer;)V
    .locals 0
    .param p1    # Ljava/nio/FloatBuffer;

    invoke-virtual {p0, p1}, Lcom/jme3/bounding/BoundingBox;->containAABB(Ljava/nio/FloatBuffer;)V

    return-void
.end method

.method public containAABB(Ljava/nio/FloatBuffer;)V
    .locals 13
    .param p1    # Ljava/nio/FloatBuffer;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v9

    const/4 v10, 0x2

    if-le v9, v10, :cond_0

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v8

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    const/4 v10, 0x0

    invoke-static {v9, p1, v10}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v5, v9, Lcom/jme3/math/Vector3f;->x:F

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v6, v9, Lcom/jme3/math/Vector3f;->y:F

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v7, v9, Lcom/jme3/math/Vector3f;->z:F

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v2, v9, Lcom/jme3/math/Vector3f;->x:F

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v3, v9, Lcom/jme3/math/Vector3f;->y:F

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v4, v9, Lcom/jme3/math/Vector3f;->z:F

    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v9

    div-int/lit8 v1, v9, 0x3

    :goto_1
    if-ge v0, v1, :cond_8

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-static {v9, p1, v0}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->x:F

    cmpg-float v9, v9, v5

    if-gez v9, :cond_5

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v5, v9, Lcom/jme3/math/Vector3f;->x:F

    :cond_2
    :goto_2
    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->y:F

    cmpg-float v9, v9, v6

    if-gez v9, :cond_6

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v6, v9, Lcom/jme3/math/Vector3f;->y:F

    :cond_3
    :goto_3
    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->z:F

    cmpg-float v9, v9, v7

    if-gez v9, :cond_7

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v7, v9, Lcom/jme3/math/Vector3f;->z:F

    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->x:F

    cmpl-float v9, v9, v2

    if-lez v9, :cond_2

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v2, v9, Lcom/jme3/math/Vector3f;->x:F

    goto :goto_2

    :cond_6
    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v9, v9, v3

    if-lez v9, :cond_3

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v3, v9, Lcom/jme3/math/Vector3f;->y:F

    goto :goto_3

    :cond_7
    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->z:F

    cmpl-float v9, v9, v4

    if-lez v9, :cond_4

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v4, v9, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, Lcom/jme3/util/TempVars;->release()V

    iget-object v9, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    add-float v10, v5, v2

    add-float v11, v6, v3

    add-float v12, v7, v4

    invoke-virtual {v9, v10, v11, v12}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    const/high16 v10, 0x3f000000

    invoke-virtual {v9, v10}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->x:F

    sub-float v9, v2, v9

    iput v9, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget-object v9, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->y:F

    sub-float v9, v3, v9

    iput v9, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget-object v9, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v9, v9, Lcom/jme3/math/Vector3f;->z:F

    sub-float v9, v4, v9

    iput v9, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    goto/16 :goto_0
.end method

.method public distanceToEdge(Lcom/jme3/math/Vector3f;)F
    .locals 6
    .param p1    # Lcom/jme3/math/Vector3f;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v3

    iget-object v0, v3, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v4, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v4, v0}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    const/4 v2, 0x0

    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    neg-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    add-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    neg-float v4, v4

    iput v4, v0, Lcom/jme3/math/Vector3f;->x:F

    :cond_0
    :goto_0
    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    neg-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_4

    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    add-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    neg-float v4, v4

    iput v4, v0, Lcom/jme3/math/Vector3f;->y:F

    :cond_1
    :goto_1
    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    neg-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    add-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    neg-float v4, v4

    iput v4, v0, Lcom/jme3/math/Vector3f;->z:F

    :cond_2
    :goto_2
    invoke-virtual {v3}, Lcom/jme3/util/TempVars;->release()V

    invoke-static {v2}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v4

    return v4

    :cond_3
    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iput v4, v0, Lcom/jme3/math/Vector3f;->x:F

    goto :goto_0

    :cond_4
    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iput v4, v0, Lcom/jme3/math/Vector3f;->y:F

    goto :goto_1

    :cond_5
    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float v1, v4, v5

    mul-float v4, v1, v1

    add-float/2addr v2, v4

    iget v4, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    iput v4, v0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_2
.end method

.method public getExtent(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    new-instance p1, Lcom/jme3/math/Vector3f;

    invoke-direct {p1}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v0, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {p1, v0, v1, v2}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    return-object p1
.end method

.method public getMax(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    new-instance p1, Lcom/jme3/math/Vector3f;

    invoke-direct {p1}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v0}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;->addLocal(FFF)Lcom/jme3/math/Vector3f;

    return-object p1
.end method

.method public getMin(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    new-instance p1, Lcom/jme3/math/Vector3f;

    invoke-direct {p1}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget-object v0, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v0}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;->subtractLocal(FFF)Lcom/jme3/math/Vector3f;

    return-object p1
.end method

.method public getType()Lcom/jme3/bounding/BoundingVolume$Type;
    .locals 1

    sget-object v0, Lcom/jme3/bounding/BoundingVolume$Type;->AABB:Lcom/jme3/bounding/BoundingVolume$Type;

    return-object v0
.end method

.method public getXExtent()F
    .locals 1

    iget v0, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    return v0
.end method

.method public getYExtent()F
    .locals 1

    iget v0, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    return v0
.end method

.method public getZExtent()F
    .locals 1

    iget v0, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return v0
.end method

.method public intersects(Lcom/jme3/bounding/BoundingVolume;)Z
    .locals 1
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    invoke-virtual {p1, p0}, Lcom/jme3/bounding/BoundingVolume;->intersectsBoundingBox(Lcom/jme3/bounding/BoundingBox;)Z

    move-result v0

    return v0
.end method

.method public intersects(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Z
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    invoke-static {p0, p1, p2, p3}, Lcom/jme3/bounding/Intersection;->intersect(Lcom/jme3/bounding/BoundingBox;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Z

    move-result v0

    return v0
.end method

.method public intersectsBoundingBox(Lcom/jme3/bounding/BoundingBox;)Z
    .locals 4
    .param p1    # Lcom/jme3/bounding/BoundingBox;

    const/4 v0, 0x0

    sget-boolean v1, Lcom/jme3/bounding/BoundingBox;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-static {v1}, Lcom/jme3/math/Vector3f;->isValidVector(Lcom/jme3/math/Vector3f;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-static {v1}, Lcom/jme3/math/Vector3f;->isValidVector(Lcom/jme3/math/Vector3f;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    add-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    sub-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    add-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    sub-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    add-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    sub-float/2addr v1, v2

    iget-object v2, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p1, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeLocal(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 8
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    sget-object v0, Lcom/jme3/bounding/BoundingBox$1;->$SwitchMap$com$jme3$bounding$BoundingVolume$Type:[I

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/bounding/BoundingVolume$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 p0, 0x0

    goto :goto_0

    :pswitch_0
    move-object v6, p1

    check-cast v6, Lcom/jme3/bounding/BoundingBox;

    iget-object v1, v6, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v2, v6, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v3, v6, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v4, v6, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    move-object v0, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/jme3/bounding/BoundingBox;->merge(Lcom/jme3/math/Vector3f;FFFLcom/jme3/bounding/BoundingBox;)Lcom/jme3/bounding/BoundingBox;

    move-result-object p0

    goto :goto_0

    :pswitch_1
    move-object v7, p1

    check-cast v7, Lcom/jme3/bounding/BoundingSphere;

    iget-object v1, v7, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget v2, v7, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget v3, v7, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget v4, v7, Lcom/jme3/bounding/BoundingSphere;->radius:F

    move-object v0, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/jme3/bounding/BoundingBox;->merge(Lcom/jme3/math/Vector3f;FFFLcom/jme3/bounding/BoundingBox;)Lcom/jme3/bounding/BoundingBox;

    move-result-object p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/jme3/bounding/BoundingVolume;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "xExtent"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    const-string v1, "yExtent"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    const-string v1, "zExtent"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return-void
.end method

.method public setMinMax(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, v1}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v0, p2, Lcom/jme3/math/Vector3f;->z:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return-void
.end method

.method public setXExtent(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    return-void
.end method

.method public setYExtent(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    return-void
.end method

.method public setZExtent(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Center: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  xExtent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  yExtent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  zExtent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lcom/jme3/math/Matrix4f;Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 8
    .param p1    # Lcom/jme3/math/Matrix4f;
    .param p2    # Lcom/jme3/bounding/BoundingVolume;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v4

    sget-object v5, Lcom/jme3/bounding/BoundingVolume$Type;->AABB:Lcom/jme3/bounding/BoundingVolume$Type;

    if-eq v4, v5, :cond_1

    :cond_0
    new-instance v0, Lcom/jme3/bounding/BoundingBox;

    invoke-direct {v0}, Lcom/jme3/bounding/BoundingBox;-><init>()V

    :goto_0
    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v2

    iget-object v4, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget-object v5, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v4, v5}, Lcom/jme3/math/Matrix4f;->multProj(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F

    move-result v3

    iget-object v4, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v3}, Lcom/jme3/math/Vector3f;->divideLocal(F)Lcom/jme3/math/Vector3f;

    iget-object v1, v2, Lcom/jme3/util/TempVars;->tempMat3:Lcom/jme3/math/Matrix3f;

    invoke-virtual {p1, v1}, Lcom/jme3/math/Matrix4f;->toRotationMatrix(Lcom/jme3/math/Matrix3f;)V

    invoke-virtual {v1}, Lcom/jme3/math/Matrix3f;->absoluteLocal()V

    iget-object v4, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v6, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v7, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v4, v5, v6, v7}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v4, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v5, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v4, v5}, Lcom/jme3/math/Matrix3f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v4, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget-object v4, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget-object v4, v2, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v2}, Lcom/jme3/util/TempVars;->release()V

    return-object v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/jme3/bounding/BoundingBox;

    goto :goto_0
.end method

.method public transform(Lcom/jme3/math/Transform;Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 9
    .param p1    # Lcom/jme3/math/Transform;
    .param p2    # Lcom/jme3/bounding/BoundingVolume;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v4

    sget-object v5, Lcom/jme3/bounding/BoundingVolume$Type;->AABB:Lcom/jme3/bounding/BoundingVolume$Type;

    if-eq v4, v5, :cond_1

    :cond_0
    new-instance v0, Lcom/jme3/bounding/BoundingBox;

    invoke-direct {v0}, Lcom/jme3/bounding/BoundingBox;-><init>()V

    :goto_0
    iget-object v4, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getScale()Lcom/jme3/math/Vector3f;

    move-result-object v5

    iget-object v6, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5, v6}, Lcom/jme3/math/Vector3f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getRotation()Lcom/jme3/math/Quaternion;

    move-result-object v4

    iget-object v5, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget-object v6, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5, v6}, Lcom/jme3/math/Quaternion;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v4, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v3

    iget-object v2, v3, Lcom/jme3/util/TempVars;->tempMat3:Lcom/jme3/math/Matrix3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getRotation()Lcom/jme3/math/Quaternion;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/jme3/math/Matrix3f;->set(Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Matrix3f;

    invoke-virtual {v2}, Lcom/jme3/math/Matrix3f;->absoluteLocal()V

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getScale()Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget-object v4, v3, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v5, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v6, v1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v7, v1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    iget v8, v1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v7, v8

    invoke-virtual {v4, v5, v6, v7}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v4, v3, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v5, v3, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v4, v5}, Lcom/jme3/math/Matrix3f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v4, v3, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget-object v4, v3, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget-object v4, v3, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v4

    invoke-static {v4}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v4

    iput v4, v0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v3}, Lcom/jme3/util/TempVars;->release()V

    return-object v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/jme3/bounding/BoundingBox;

    goto :goto_0
.end method

.method public whichSide(Lcom/jme3/math/Plane;)Lcom/jme3/math/Plane$Side;
    .locals 5
    .param p1    # Lcom/jme3/math/Plane;

    iget v2, p0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v3

    add-float v1, v2, v3

    iget-object v2, p0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v2}, Lcom/jme3/math/Plane;->pseudoDistance(Lcom/jme3/math/Vector3f;)F

    move-result v0

    neg-float v2, v1

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    sget-object v2, Lcom/jme3/math/Plane$Side;->Negative:Lcom/jme3/math/Plane$Side;

    :goto_0
    return-object v2

    :cond_0
    cmpl-float v2, v0, v1

    if-lez v2, :cond_1

    sget-object v2, Lcom/jme3/math/Plane$Side;->Positive:Lcom/jme3/math/Plane$Side;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/jme3/math/Plane$Side;->None:Lcom/jme3/math/Plane$Side;

    goto :goto_0
.end method
