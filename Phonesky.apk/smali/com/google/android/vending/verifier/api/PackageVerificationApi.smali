.class public Lcom/google/android/vending/verifier/api/PackageVerificationApi;
.super Ljava/lang/Object;
.source "PackageVerificationApi.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildSha256Digest([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;
    .locals 2
    .param p0    # [B

    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;-><init>()V

    invoke-static {p0}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;->setSha256(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    move-result-object v1

    return-object v1
.end method

.method private static createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    .locals 2
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/net/InetAddress;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->setUrl(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-virtual {v0, p3}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->setType(I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->setReferrer(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->setRemoteIp(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    :cond_1
    return-object v0
.end method

.method public static reportUserDecision(I[BLcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 3
    .param p0    # I
    .param p1    # [B
    .param p2    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[B",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;

    invoke-direct {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;-><init>()V

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->setToken(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;

    :cond_0
    invoke-virtual {v1, p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->setUserDecision(I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;

    new-instance v0, Lcom/google/android/vending/verifier/api/PackageVerificationStatsRequest;

    const-string v2, "https://safebrowsing.google.com/safebrowsing/clientreport/download-stat"

    invoke-direct {v0, v2, p2, v1}, Lcom/google/android/vending/verifier/api/PackageVerificationStatsRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public static verifyApp([BJLjava/lang/String;Ljava/lang/Integer;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;Ljava/lang/String;JLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 10
    .param p0    # [B
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Landroid/net/Uri;
    .param p6    # Landroid/net/Uri;
    .param p7    # Ljava/net/InetAddress;
    .param p8    # Ljava/net/InetAddress;
    .param p9    # [Ljava/lang/String;
    .param p10    # Ljava/lang/String;
    .param p11    # J
    .param p14    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    new-instance v7, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    invoke-direct {v7}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;-><init>()V

    if-nez p3, :cond_0

    if-eqz p4, :cond_3

    :cond_0
    new-instance v2, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-direct {v2}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;-><init>()V

    if-eqz p3, :cond_1

    invoke-virtual {v2, p3}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->setPackageName(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->setVersionCode(I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    :cond_2
    invoke-virtual {v7, v2}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setApkInfo(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    :cond_3
    if-eqz p5, :cond_5

    invoke-virtual {p5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setUrl(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    const/4 v8, 0x0

    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-static {p5, v0, v1, v8}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->addResources(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    if-eqz p6, :cond_4

    const/4 v8, 0x0

    const/4 v9, 0x2

    move-object/from16 v0, p6

    move-object/from16 v1, p8

    invoke-static {v0, v1, v8, v9}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->addResources(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    :cond_4
    :goto_0
    if-eqz p9, :cond_6

    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, p9

    array-length v8, v0

    if-ge v3, v8, :cond_6

    aget-object v8, p9, v3

    invoke-virtual {v7, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->addOriginatingPackages(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    const-string v8, ""

    invoke-virtual {v7, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setUrl(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    goto :goto_0

    :cond_6
    invoke-virtual {v7, p1, p2}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setLength(J)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    invoke-static {p0}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->buildSha256Digest([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setDigests(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setDownloadType(I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    if-eqz p10, :cond_7

    move-object/from16 v0, p10

    invoke-virtual {v7, v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setLocale(Ljava/lang/String;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    :cond_7
    move-wide/from16 v0, p11

    invoke-virtual {v7, v0, v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->setAndroidId(J)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    new-instance v6, Lcom/google/android/vending/verifier/api/PackageVerificationRequest;

    const-string v8, "https://safebrowsing.google.com/safebrowsing/clientreport/download"

    move-object/from16 v0, p13

    move-object/from16 v1, p14

    invoke-direct {v6, v8, v0, v1, v7}, Lcom/google/android/vending/verifier/api/PackageVerificationRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v8

    return-object v8
.end method
