.class public Lcom/google/android/social/api/service/PlusInternalService;
.super Landroid/app/Service;
.source "PlusInternalService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;,
        Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected createService(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/social/api/service/IPlusInternalService$Stub;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;

    invoke-direct {v0, p1, p2}, Lcom/google/android/social/api/service/PlusInternalService$PlusInternalServiceEntity;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;

    invoke-direct {v0, p0, p0}, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;-><init>(Lcom/google/android/social/api/service/PlusInternalService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalService$ServiceBroker;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
