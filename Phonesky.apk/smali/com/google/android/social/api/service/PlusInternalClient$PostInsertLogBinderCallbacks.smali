.class final Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;
.super Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "PostInsertLogBinderCallbacks"
.end annotation


# instance fields
.field private final listener:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;->listener:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    new-instance v1, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;->listener:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    invoke-static {p1, p2}, Lcom/google/android/social/api/service/Results;->getConnectionResult(ILandroid/os/Bundle;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
