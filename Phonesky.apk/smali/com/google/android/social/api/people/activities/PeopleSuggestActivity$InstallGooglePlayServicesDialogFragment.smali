.class public Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "PeopleSuggestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InstallGooglePlayServicesDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newDialogFragment(I)Lvedroid/support/v4/app/DialogFragment;
    .locals 3
    .param p0    # I

    new-instance v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;

    invoke-direct {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "status"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Lvedroid/support/v4/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "status"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/16 v3, 0x7d1

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment$1;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$InstallGooglePlayServicesDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-object v0
.end method
