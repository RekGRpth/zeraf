.class Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "AudiencesListOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/AudiencesListOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AudiencesListRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/google/android/social/api/people/model/AudienceMember;",
        ">;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Lcom/google/api/services/plus/model/AudiencesFeed;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/operations/AudiencesListOperation;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/AudiencesListOperation;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;->this$0:Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    invoke-static {}, Lcom/google/android/social/api/network/ApiaryConfig;->getLoadSocialNetwork()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plus/model/AudiencesFeed;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;->processResponseData(Lcom/google/api/services/plus/model/AudiencesFeed;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected processResponseData(Lcom/google/api/services/plus/model/AudiencesFeed;)Ljava/util/ArrayList;
    .locals 8
    .param p1    # Lcom/google/api/services/plus/model/AudiencesFeed;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plus/model/AudiencesFeed;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p1, Lcom/google/api/services/plus/model/AudiencesFeed;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plus/model/Audience;

    iget-object v3, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;->this$0:Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    # getter for: Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeGroups:Z
    invoke-static {v3}, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->access$000(Lcom/google/android/social/api/people/operations/AudiencesListOperation;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "myCircles"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "YOUR_CIRCLES"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->displayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/social/api/people/model/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/social/api/people/operations/AudiencesListOperation$AudiencesListRequest;->this$0:Lcom/google/android/social/api/people/operations/AudiencesListOperation;

    # getter for: Lcom/google/android/social/api/people/operations/AudiencesListOperation;->includeCircles:Z
    invoke-static {v3}, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->access$100(Lcom/google/android/social/api/people/operations/AudiencesListOperation;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "circle"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v3, v3, Lcom/google/api/services/plus/model/PlusAclentryResource;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->displayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/social/api/people/model/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v3, "extendedCircles"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "EXTENDED_CIRCLES"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->displayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/social/api/people/model/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v3, "public"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "PUBLIC"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->displayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/social/api/people/model/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v3, "domain"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "DASHER_DOMAIN"

    iget-object v4, v0, Lcom/google/api/services/plus/model/Audience;->item:Lcom/google/api/services/plus/model/PlusAclentryResource;

    iget-object v4, v4, Lcom/google/api/services/plus/model/PlusAclentryResource;->displayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/social/api/people/model/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    # getter for: Lcom/google/android/social/api/people/operations/AudiencesListOperation;->log:Lcom/google/android/social/api/internal/GLog;
    invoke-static {}, Lcom/google/android/social/api/people/operations/AudiencesListOperation;->access$200()Lcom/google/android/social/api/internal/GLog;

    move-result-object v3

    const-string v4, "Loaded %d Members"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v1
.end method
