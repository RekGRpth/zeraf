.class public Lcom/google/android/social/api/people/model/Person;
.super Ljava/lang/Object;
.source "Person.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/social/api/people/model/Person;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final circles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final person:Lcom/google/android/social/api/people/model/AudienceMember;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/model/Person$1;

    invoke-direct {v0}, Lcom/google/android/social/api/people/model/Person$1;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/model/Person;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    iput-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/social/api/people/model/AudienceMember;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/social/api/people/model/Person$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/social/api/people/model/Person$1;

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/model/Person;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/social/api/people/model/AudienceMember;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    iput-object p2, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/social/api/people/model/Person;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    iput-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addCircle(Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCircle(I)Lcom/google/android/social/api/people/model/AudienceMember;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    return-object v0
.end method

.method public getCircleCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCirclesList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->getAvatarUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPerson()Lcom/google/android/social/api/people/model/AudienceMember;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    return-object v0
.end method

.method public getStableId()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/AudienceMember;->getStableId()J

    move-result-wide v0

    return-wide v0
.end method

.method public removeCircle(Lcom/google/android/social/api/people/model/AudienceMember;)Z
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->person:Lcom/google/android/social/api/people/model/AudienceMember;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/social/api/people/model/Person;->circles:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void
.end method
