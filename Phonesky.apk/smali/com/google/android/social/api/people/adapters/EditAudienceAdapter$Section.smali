.class public Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Section"
.end annotation


# instance fields
.field public final mAudience:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public final mIndexer:Ljava/lang/String;

.field private mShowHeader:Z

.field public final mTitle:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;)V
    .locals 0
    .param p1    # Ljava/util/List;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mTitle:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mIndexer:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mShowHeader:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;)V
    .locals 0
    .param p1    # Ljava/util/List;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getIndexer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mIndexer:Ljava/lang/String;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mShowHeader:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mTitle:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mShowHeader:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLength()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mShowHeader:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public hideHeader()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mShowHeader:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getIndexer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
