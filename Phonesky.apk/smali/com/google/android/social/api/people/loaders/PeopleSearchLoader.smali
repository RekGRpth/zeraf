.class public Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;
.super Lcom/google/android/social/api/loaders/PlusApiLoader;
.source "PeopleSearchLoader.java"

# interfaces
.implements Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;


# instance fields
.field private personList:Lcom/google/android/social/api/people/model/PersonList;

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/social/api/loaders/PlusApiLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->query:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPersonList()Lcom/google/android/social/api/people/model/PersonList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->personList:Lcom/google/android/social/api/people/model/PersonList;

    return-object v0
.end method

.method protected onConnected(Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v0, p0, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->query:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->query:Ljava/lang/String;

    invoke-virtual {p1, p0, v0}, Lcom/google/android/social/api/service/PlusInternalClient;->searchAudience(Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    iput-object p2, p0, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->personList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/loaders/PeopleSearchLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
