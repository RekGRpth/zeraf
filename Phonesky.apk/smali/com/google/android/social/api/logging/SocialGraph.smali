.class public final Lcom/google/android/social/api/logging/SocialGraph;
.super Lcom/google/android/social/api/logging/AbstractAnalyticsNamespace;
.source "SocialGraph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/logging/SocialGraph$Action;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/social/api/logging/AbstractAnalyticsNamespace;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    return-object v0
.end method

.method private static build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .locals 1
    .param p0    # I

    const-string v0, "sg"

    invoke-static {v0, p0}, Lcom/google/android/social/api/logging/SocialGraph;->buildNamespacedType(Ljava/lang/String;I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    return-object v0
.end method
