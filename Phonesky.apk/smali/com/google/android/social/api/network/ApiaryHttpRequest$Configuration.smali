.class public Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;
.super Ljava/lang/Object;
.source "ApiaryHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/network/ApiaryHttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Configuration"
.end annotation


# instance fields
.field public final host:Ljava/lang/String;

.field public final method:Ljava/lang/String;

.field public final path:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->url:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->host:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->path:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->method:Ljava/lang/String;

    return-void
.end method
