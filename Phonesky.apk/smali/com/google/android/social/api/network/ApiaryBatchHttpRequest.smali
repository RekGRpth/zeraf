.class public Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;
.super Lcom/google/android/social/api/network/ApiaryHttpRequest;
.source "ApiaryBatchHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;,
        Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResultType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/social/api/network/ApiaryHttpRequest",
        "<",
        "Ljava/util/ArrayList",
        "<TResultType;>;>;"
    }
.end annotation


# static fields
.field static final PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

.field static final PATTERN_ID:Ljava/util/regex/Pattern;

.field static final PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;


# instance fields
.field private final requests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/network/ApiaryHttpRequest",
            "<+TResultType;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Content-ID: <response-item:(.+)>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_ID:Ljava/util/regex/Pattern;

    const-string v0, "HTTP/1\\.1 (\\d+) (.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;

    const-string v0, "Content-Length: (\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/social/api/network/ApiaryConfig;->getBatchConfiguration()Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;

    new-instance v0, Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    invoke-direct {v0}, Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;
    .param p4    # Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/social/api/network/ApiaryHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;

    return-object v0
.end method

.method private handleSectionResponse(Ljava/io/DataInputStream;I)Ljava/lang/Object;
    .locals 15
    .param p1    # Ljava/io/DataInputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            "I)TResultType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    const/4 v3, 0x0

    const/16 v8, 0xc8

    const-string v9, ""

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v10, ""

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/social/api/network/ApiaryHttpRequest;

    new-array v1, v3, [B

    const/4 v6, 0x0

    :goto_1
    array-length v10, v1

    if-ge v6, v10, :cond_3

    array-length v10, v1

    sub-int/2addr v10, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v10}, Ljava/io/DataInputStream;->read([BII)I

    move-result v10

    add-int/2addr v6, v10

    goto :goto_1

    :cond_1
    sget-object v10, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    sget-object v10, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    const/16 v10, 0xc8

    if-lt v8, v10, :cond_4

    const/16 v10, 0x12c

    if-ge v8, v10, :cond_4

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v7, v10}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->processResponseData(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v10

    return-object v10

    :cond_4
    new-instance v10, Landroid/accounts/NetworkErrorException;

    const-string v11, "Error results for %s: %d (%s)"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    aput-object v9, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method private readSectionHeader(Ljava/io/DataInputStream;)Ljava/lang/Integer;
    .locals 4
    .param p1    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v3, 0x0

    :goto_1
    return-object v3

    :cond_1
    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->PATTERN_ID:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/ApiaryHttpRequest",
            "<+TResultType;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getRequest(Lcom/google/android/social/api/network/RequestFuture;)Lcom/android/volley/Request;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/RequestFuture",
            "<",
            "Ljava/util/ArrayList",
            "<TResultType;>;>;)",
            "Lcom/android/volley/Request",
            "<",
            "Ljava/util/ArrayList",
            "<TResultType;>;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;

    invoke-direct {v0, p0, p1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;-><init>(Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;Lcom/google/android/social/api/network/RequestFuture;)V

    return-object v0
.end method

.method protected bridge synthetic processResponseData(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->processResponseData(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected processResponseData(Ljava/io/InputStream;)Ljava/util/ArrayList;
    .locals 6
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/ArrayList",
            "<TResultType;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->readSectionHeader(Ljava/io/DataInputStream;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v1, v5}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->handleSectionResponse(Ljava/io/DataInputStream;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
