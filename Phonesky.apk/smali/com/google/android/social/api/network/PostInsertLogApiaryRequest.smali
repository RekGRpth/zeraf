.class public Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "PostInsertLogApiaryRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Ljava/lang/Void;",
        "Lcom/google/api/services/plus/model/ClientOzExtension;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">;"
    }
.end annotation


# instance fields
.field private final appName:Ljava/lang/String;

.field private final isTablet:Z

.field private final logEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientOzEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/social/api/network/ApiaryConfig;->getRpcPostInsertLog()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    iput-object p3, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->logEvents:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->appName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->isTablet:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic buildRequest()Lcom/google/android/apps/plus/json/GenericJson;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->buildRequest()Lcom/google/api/services/plus/model/ClientOzExtension;

    move-result-object v0

    return-object v0
.end method

.method protected buildRequest()Lcom/google/api/services/plus/model/ClientOzExtension;
    .locals 3

    new-instance v0, Lcom/google/api/services/plus/model/ClientOzExtension;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOzExtension;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plus/model/ClientOzExtension;->sendTimeMsec:Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->appName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plus/model/ClientOzExtension;->callingApplication:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->logEvents:Ljava/util/List;

    iput-object v1, v0, Lcom/google/api/services/plus/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    iget-boolean v1, p0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->isTablet:Z

    if-eqz v1, :cond_0

    const-string v1, "10"

    :goto_0
    iput-object v1, v0, Lcom/google/api/services/plus/model/ClientOzExtension;->clientId:Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v1, "4"

    goto :goto_0
.end method

.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Void;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v0, 0x0

    return-object v0
.end method
