.class public abstract Lcom/google/android/apps/plus/json/EsJson;
.super Ljava/lang/Object;
.source "EsJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/json/EsJson$SimpleJson;,
        Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final JSON_KEY:Ljava/lang/Object;

.field protected static final JSON_STRING:Ljava/lang/Object;

.field private static final UTF_8:Ljava/nio/charset/Charset;

.field private static sSimpleJsonMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/plus/json/EsJson$SimpleJson",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mConfiguration:[Ljava/lang/Object;

.field private mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

.field private mTargetClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->JSON_STRING:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->JSON_KEY:Ljava/lang/Object;

    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected varargs constructor <init>(Ljava/lang/Class;[Ljava/lang/Object;)V
    .locals 0
    .param p2    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    return-void
.end method

.method public static getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TE;>;"
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    iput-object p2, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Ljava/lang/String;

    if-ne v0, v2, :cond_2

    iput v1, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    iget v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    :cond_1
    return-void

    :cond_2
    const-class v2, Ljava/lang/Integer;

    if-eq v0, v2, :cond_3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_4

    :cond_3
    iput v3, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_4
    const-class v2, Ljava/lang/Long;

    if-eq v0, v2, :cond_5

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_6

    :cond_5
    iput v4, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_6
    const-class v2, Ljava/lang/Float;

    if-eq v0, v2, :cond_7

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_8

    :cond_7
    iput v5, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_8
    const-class v2, Ljava/lang/Double;

    if-eq v0, v2, :cond_9

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_a

    :cond_9
    iput v6, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_a
    const-class v2, Ljava/lang/Boolean;

    if-eq v0, v2, :cond_b

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_c

    :cond_b
    const/4 v0, 0x5

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_c
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_d

    const/4 v0, 0x6

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_d
    const-class v2, Lcom/google/android/apps/plus/json/DateTime;

    if-ne v0, v2, :cond_e

    const/16 v0, 0x9

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_0

    :cond_e
    const-class v2, Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Class;

    const/4 v2, 0x7

    iput v2, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    const-class v2, Ljava/lang/String;

    if-ne v0, v2, :cond_f

    iput v1, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    :goto_1
    if-eqz p3, :cond_0

    iget v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    move p3, v1

    goto :goto_0

    :cond_f
    const-class v2, Ljava/lang/Integer;

    if-ne v0, v2, :cond_10

    iput v3, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_10
    const-class v2, Ljava/lang/Long;

    if-ne v0, v2, :cond_11

    iput v4, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_11
    const-class v2, Ljava/lang/Float;

    if-ne v0, v2, :cond_12

    iput v5, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_12
    const-class v2, Ljava/lang/Double;

    if-ne v0, v2, :cond_13

    iput v6, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_13
    const-class v2, Ljava/lang/Boolean;

    if-ne v0, v2, :cond_14

    const/4 v0, 0x5

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_14
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_15

    const/4 v0, 0x6

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_15
    const-class v2, Lcom/google/android/apps/plus/json/DateTime;

    if-ne v0, v2, :cond_16

    const/16 v0, 0x9

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_16
    const/16 v0, 0x8

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_1

    :cond_17
    const/16 v0, 0x8

    iput v0, p1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto/16 :goto_0
.end method

.method private initializeFieldConverters()V
    .locals 15

    const/4 v14, 0x0

    const/4 v13, 0x0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    :goto_0
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    array-length v10, v10

    if-ge v6, v10, :cond_4

    new-instance v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    invoke-direct {v1, v14}, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;-><init>(Lcom/google/android/apps/plus/json/EsJson$1;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    aget-object v3, v10, v6

    sget-object v10, Lcom/google/android/apps/plus/json/EsJson;->JSON_KEY:Ljava/lang/Object;

    if-ne v3, v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v6, v7, 0x1

    aget-object v10, v10, v7

    check-cast v10, Ljava/lang/String;

    iput-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    aget-object v3, v10, v6

    move v6, v7

    :goto_1
    const/4 v0, 0x0

    sget-object v10, Lcom/google/android/apps/plus/json/EsJson;->JSON_STRING:Ljava/lang/Object;

    if-ne v3, v10, :cond_0

    const/4 v0, 0x1

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    aget-object v3, v10, v6

    move v6, v7

    :cond_0
    instance-of v10, v3, Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v10, :cond_3

    check-cast v3, Lcom/google/android/apps/plus/json/EsJson;

    iput-object v3, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    aget-object v3, v10, v6

    move v6, v7

    :cond_1
    :goto_2
    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    if-nez v10, :cond_2

    iput-object v5, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    :cond_2
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v10, v13}, Ljava/lang/String;->charAt(I)C

    move-result v10

    iput-char v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    :try_start_0
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v10, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    invoke-direct {p0, v1, v4, v0}, Lcom/google/android/apps/plus/json/EsJson;->initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    instance-of v10, v3, Ljava/lang/Class;

    if-eqz v10, :cond_1

    move-object v8, v3

    check-cast v8, Ljava/lang/Class;

    :try_start_1
    const-string v10, "getInstance"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v8, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/json/EsJson;

    iput-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    aget-object v3, v10, v6

    move v6, v7

    goto :goto_2

    :catch_0
    move-exception v2

    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid EsJson class: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10

    :catch_1
    move-exception v2

    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "No such field: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    iput-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-void

    :cond_5
    move v6, v7

    goto/16 :goto_1
.end method

.method private writeObject(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 10
    .param p1    # Lcom/google/gson/stream/JsonWriter;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    if-nez v8, :cond_0

    new-instance v8, Ljava/lang/UnsupportedOperationException;

    const-string v9, "A JSON class must either configure the automatic parser or override read(Jsonwriter)"

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/EsJson;->initializeFieldConverters()V

    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/json/EsJson;->getValues(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    const/4 v1, 0x0

    :goto_0
    array-length v8, v7

    if-ge v1, v8, :cond_4

    aget-object v8, v7, v1

    if-nez v8, :cond_2

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    aget-object v6, v7, v1

    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v0, v8, v1

    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    iget v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    check-cast v6, Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_2
    check-cast v6, Ljava/lang/Number;

    invoke-virtual {p1, v6}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_3
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_4
    check-cast v6, Ljava/lang/Float;

    invoke-virtual {p1, v6}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_5
    check-cast v6, Ljava/lang/Double;

    invoke-virtual {p1, v6}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_6
    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Z)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_7
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_1

    :pswitch_8
    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v8, p1, v6}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_9
    move-object v4, v6

    check-cast v4, Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginArray()Lcom/google/gson/stream/JsonWriter;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    sparse-switch v8, :sswitch_data_0

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :sswitch_0
    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_1
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {p1, v2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_2
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_3
    check-cast v2, Ljava/lang/Float;

    invoke-virtual {p1, v2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_4
    check-cast v2, Ljava/lang/Double;

    invoke-virtual {p1, v2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_5
    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Z)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_6
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    goto :goto_3

    :sswitch_7
    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v8, p1, v2}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_3

    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endArray()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_9
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_1
        0x8 -> :sswitch_7
        0x9 -> :sswitch_2
        0x20 -> :sswitch_0
        0x21 -> :sswitch_6
        0x22 -> :sswitch_6
        0x23 -> :sswitch_6
        0x24 -> :sswitch_6
        0x25 -> :sswitch_6
        0x26 -> :sswitch_6
        0x29 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method protected buildDefaultConfiguration()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v4, v2

    new-array v4, v4, [Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    iput-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_2

    aget-object v1, v2, v3

    new-instance v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;-><init>(Lcom/google/android/apps/plus/json/EsJson$1;)V

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    invoke-direct {p0, v0, v1, v5}, Lcom/google/android/apps/plus/json/EsJson;->initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V

    iget v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    if-eq v4, v6, :cond_0

    iget v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    if-ne v4, v6, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot use default JSON for object containing fields of non-basic types: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aput-object v0, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public fromByteArray([B)Ljava/lang/Object;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot parse JSON using "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/gson/stream/JsonReader;

    new-instance v2, Ljava/io/InputStreamReader;

    sget-object v3, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v2}, Lcom/google/gson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/gson/stream/JsonReader;->close()V

    return-object v1
.end method

.method protected getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v3, v3

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v3, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot obtain field value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    return-object v2
.end method

.method public newInstance()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot create new instance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 12
    .param p1    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v9, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    if-nez v9, :cond_0

    new-instance v9, Ljava/lang/UnsupportedOperationException;

    const-string v10, "A JSON class must either configure the automatic parser or override read(JsonReader)"

    invoke-direct {v9, v10}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/EsJson;->initializeFieldConverters()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/4 v4, 0x0

    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v9, v9

    if-ge v4, v9, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v0, v9, v4

    iget-char v9, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    if-ne v9, v3, :cond_3

    iget-object v9, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v1, v0

    :cond_2
    if-eqz v1, :cond_7

    iget v9, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    :goto_2
    :try_start_0
    iget-object v9, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v9, v7, v8}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v9, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot assign field value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_2

    :pswitch_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_2

    :pswitch_4
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextLong()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto :goto_2

    :pswitch_5
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    goto :goto_2

    :pswitch_6
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D

    move-result-wide v9

    double-to-float v9, v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    goto :goto_2

    :pswitch_7
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    goto :goto_2

    :pswitch_8
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    goto/16 :goto_2

    :pswitch_9
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    goto/16 :goto_2

    :pswitch_a
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextBoolean()Z

    move-result v9

    if-eqz v9, :cond_4

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    goto/16 :goto_2

    :cond_4
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_3

    :pswitch_b
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v8

    goto/16 :goto_2

    :pswitch_c
    new-instance v8, Ljava/math/BigInteger;

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_d
    new-instance v8, Lcom/google/android/apps/plus/json/DateTime;

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/json/DateTime;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_e
    iget-object v9, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v9, p1}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    goto/16 :goto_2

    :pswitch_f
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginArray()V

    :goto_4
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    iget v9, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    sparse-switch v9, :sswitch_data_0

    goto :goto_4

    :sswitch_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextLong()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D

    move-result-wide v9

    double-to-float v9, v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextBoolean()Z

    move-result v9

    if-eqz v9, :cond_5

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_5
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_5

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :sswitch_b
    new-instance v9, Ljava/math/BigInteger;

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :sswitch_c
    new-instance v9, Lcom/google/android/apps/plus/json/DateTime;

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/apps/plus/json/DateTime;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :sswitch_d
    iget-object v9, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v9, p1}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_6
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endArray()V

    move-object v8, v6

    goto/16 :goto_2

    :cond_7
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    return-object v7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_c
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_5
        0x4 -> :sswitch_7
        0x5 -> :sswitch_9
        0x6 -> :sswitch_b
        0x8 -> :sswitch_d
        0x9 -> :sswitch_c
        0x20 -> :sswitch_0
        0x21 -> :sswitch_2
        0x22 -> :sswitch_4
        0x23 -> :sswitch_6
        0x24 -> :sswitch_8
        0x25 -> :sswitch_a
        0x26 -> :sswitch_b
        0x29 -> :sswitch_c
    .end sparse-switch
.end method

.method public toByteArray(Ljava/lang/Object;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[B"
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/plus/json/EsJson;->writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot generate JSON using "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public toPrettyString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    new-instance v2, Lcom/google/gson/stream/JsonWriter;

    invoke-direct {v2, v1}, Lcom/google/gson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Lcom/google/gson/stream/JsonWriter;->setIndent(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0, v2, p1}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/gson/stream/JsonWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot generate JSON using "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    new-instance v2, Lcom/google/gson/stream/JsonWriter;

    invoke-direct {v2, v1}, Lcom/google/gson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    :try_start_0
    invoke-virtual {p0, v2, p1}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/gson/stream/JsonWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot generate JSON using "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/gson/stream/JsonWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    return-void
.end method

.method public writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/gson/stream/JsonWriter;

    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    const/16 v3, 0x2000

    invoke-direct {v1, v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    invoke-direct {v0, v1}, Lcom/google/gson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/gson/stream/JsonWriter;->flush()V

    return-void
.end method
