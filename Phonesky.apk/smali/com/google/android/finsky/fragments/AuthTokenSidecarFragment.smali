.class public abstract Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "AuthTokenSidecarFragment.java"

# interfaces
.implements Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;


# instance fields
.field private mAsyncAuthenticator:Lcom/google/android/finsky/billing/AsyncAuthenticator;

.field private mAuthToken:Ljava/lang/String;

.field private mHasRetried:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getAccount()Landroid/accounts/Account;
.end method

.method protected getAuthTokenAndContinue(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mHasRetried:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "Invalid token after retry, giving up."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->onInvalidToken()V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "Invalid token, fetching fresh one."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mHasRetried:Z

    iget-object v1, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAsyncAuthenticator:Lcom/google/android/finsky/billing/AsyncAuthenticator;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAuthToken:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, p0, v0}, Lcom/google/android/finsky/billing/AsyncAuthenticator;->getToken(Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected abstract getAuthTokenType()Ljava/lang/String;
.end method

.method public onAuthTokenReceived(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAuthToken:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->performRequestWithToken(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/finsky/billing/AsyncAuthenticator;

    new-instance v1, Lcom/android/volley/toolbox/AndroidAuthenticator;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->getAuthTokenType()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/volley/toolbox/AndroidAuthenticator;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/AsyncAuthenticator;-><init>(Lcom/android/volley/toolbox/Authenticator;)V

    iput-object v0, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAsyncAuthenticator:Lcom/google/android/finsky/billing/AsyncAuthenticator;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected abstract onInvalidToken()V
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "AuthTokenSidecarFragment.has_retried"

    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mHasRetried:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "AuthTokenSidecarFragment.auth_token"

    iget-object v1, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAuthToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract performRequestWithToken(Ljava/lang/String;)V
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    const-string v0, "AuthTokenSidecarFragment.has_retried"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mHasRetried:Z

    const-string v0, "AuthTokenSidecarFragment.auth_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/AuthTokenSidecarFragment;->mAuthToken:Ljava/lang/String;

    return-void
.end method
