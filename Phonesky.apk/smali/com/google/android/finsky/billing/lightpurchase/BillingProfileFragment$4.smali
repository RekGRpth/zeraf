.class Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;
.super Ljava/lang/Object;
.source "BillingProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderOptions(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

.field final synthetic val$option:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->val$option:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x32d

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;->val$option:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    invoke-virtual {v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getTopupInfo()Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->topup(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$900(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)V

    return-void
.end method
