.class public Lcom/google/android/finsky/activities/RedeemGiftCardActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "RedeemGiftCardActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/BillingFlowContext;
.implements Lcom/google/android/finsky/billing/BillingFlowListener;


# instance fields
.field private mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

.field private mFlow:Lcom/google/android/finsky/billing/BillingFlow;

.field private final mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    return-void
.end method

.method public static createIntent(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "backend"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "entry_point_menu"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "referrer_url"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "referrer_url"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "params"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v0
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/layout/CustomActionBar;->updateBreadcrumb(Ljava/lang/String;)V

    return-void
.end method

.method private setupActionBar(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-static {p0}, Lcom/google/android/finsky/layout/CustomActionBarFactory;->getInstance(Landroid/app/Activity;)Lcom/google/android/finsky/layout/CustomActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    iget-object v2, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-interface {v1, v2, p0}, Lcom/google/android/finsky/layout/CustomActionBar;->initialize(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "backend"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/layout/CustomActionBar;->updateCurrentBackendId(I)V

    if-eqz p1, :cond_0

    const-string v1, "last_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "last_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setTitle(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static show(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const-string v1, "Redeem chosen with no current account."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-static {p1, p2, v1, v2, v2}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->createIntent(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public hideFragment(Lvedroid/support/v4/app/Fragment;Z)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.method public hideProgress()V
    .locals 0

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->back()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Cannot interrupt the current flow."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f040018

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setContentView(I)V

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setupActionBar(Landroid/os/Bundle;)V

    new-instance v1, Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "params"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, p0, p0, v2}, Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;->create(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz p1, :cond_0

    const-string v1, "flow_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/BillingFlow;->resumeFromSavedState(Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/BillingFlow;->start()V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->finish()V

    return-void
.end method

.method public onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Z
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "entry_point_menu"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "billing_flow_error"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "billing_flow_canceled"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->finish()V

    return-void

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/BillingFlow;->saveState(Landroid/os/Bundle;)V

    const-string v1, "flow_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    const-string v1, "last_title"

    iget-object v2, p0, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->mActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v2}, Lcom/google/android/finsky/layout/CustomActionBar;->getBreadcrumbText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public persistFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;
    .param p3    # Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V

    return-void
.end method

.method public restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lvedroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public showDialogFragment(Lvedroid/support/v4/app/DialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/DialogFragment;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showFragment(Lvedroid/support/v4/app/Fragment;IZ)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f080041

    invoke-virtual {v0, v1, p1}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setTitle(Ljava/lang/String;)V

    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showProgress(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
