.class public abstract Lcom/google/android/finsky/activities/InstrumentActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "InstrumentActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/billing/BillingFlowContext;
.implements Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;
.implements Lcom/google/android/finsky/billing/BillingFlowListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/InstrumentActivity$Mode;
    }
.end annotation


# instance fields
.field protected mBillingFlowParameters:Landroid/os/Bundle;

.field protected mFragmentContainer:Landroid/view/ViewGroup;

.field private mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

.field private mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

.field protected mMainView:Landroid/view/View;

.field private mNeedsHideProgress:Z

.field private mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field private mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

.field private mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

.field private mSaveInstanceStateCalled:Z

.field protected mSavedFlowState:Landroid/os/Bundle;

.field protected mTitleView:Landroid/widget/TextView;

.field protected mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    sget-object v0, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    iput-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/InstrumentActivity;)Lcom/google/android/finsky/billing/InstrumentFactory;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/InstrumentActivity;

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/InstrumentActivity;Lcom/google/android/finsky/billing/InstrumentFactory;)Lcom/google/android/finsky/billing/InstrumentFactory;
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/InstrumentActivity;
    .param p1    # Lcom/google/android/finsky/billing/InstrumentFactory;

    iput-object p1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/InstrumentActivity;Landroid/os/Bundle;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/InstrumentActivity;
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/InstrumentActivity;->startOrResumeFlow(Landroid/os/Bundle;)V

    return-void
.end method

.method private startOrResumeFlow(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "billing_flow_instrument"

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "billing_flow_fragment"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/BillingFlowFragment;

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Re-attached to billing flow fragment."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    sget-object v2, Lcom/google/android/finsky/activities/InstrumentActivity$Mode;->UPDATE:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v2, "update_address_header"

    iget-object v3, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/billing/InstrumentFactory;->getUpdateAddressText(Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    invoke-virtual {v1, v0, p0, p0, v2}, Lcom/google/android/finsky/billing/InstrumentFactory;->updateAddress(Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-nez v1, :cond_5

    const-string v1, "Couldn\'t instantiate BillingFlow for FOP type %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getInstrumentFamily()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    sget-object v2, Lcom/google/android/finsky/activities/InstrumentActivity$Mode;->ADD:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/billing/InstrumentFactory;->createFragment(Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlowFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getContainerId()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    const-string v4, "billing_flow_fragment"

    invoke-virtual {v1, v2, v3, v4}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    invoke-virtual {v1, v0, p0, p0, v2}, Lcom/google/android/finsky/billing/InstrumentFactory;->create(Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v1, :cond_6

    if-nez p1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/BillingFlow;->start()V

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f080042

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/billing/BillingFlow;->resumeFromSavedState(Landroid/os/Bundle;)V

    goto :goto_2
.end method

.method private stopFlow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    return-void
.end method

.method private useProgressDialog()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    sget-object v1, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    if-eq v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContainerId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    return v0
.end method

.method public hideFragment(Lvedroid/support/v4/app/Fragment;Z)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # Z

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_1
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public hideProgress()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mNeedsHideProgress:Z

    invoke-direct {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->useProgressDialog()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mNeedsHideProgress:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    goto :goto_0

    :cond_2
    const v0, 0x7f08003f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080040

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->back()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Cannot interrupt the current flow."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->back()V

    goto :goto_0

    :cond_2
    const-string v0, "Cannot interrupt the current flow."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    const v7, 0x7f08003d

    const/4 v6, 0x0

    const/16 v5, 0x8

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040012

    invoke-static {p0, v2, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mMainView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mMainView:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/InstrumentActivity;->setContentView(Landroid/view/View;)V

    const v2, 0x7f080041

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->hasAccount(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Invalid account supplied in the intent: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->finish()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ui_mode"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mMainView:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mMainView:Landroid/view/View;

    const v3, 0x7f08003e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "instrument_mode"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    sget-object v3, Lcom/google/android/finsky/activities/InstrumentActivity$Mode;->ADD:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    const v3, 0x7f070024

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "extra_paramters"

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_paramters"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "ui_mode"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "entry_point_menu"

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "entry_point_menu"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "referrer_url"

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "referrer_url"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "referrer_list_cookie"

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "referrer_list_cookie"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    const-string v2, "flow_state"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSavedFlowState:Landroid/os/Bundle;

    const-string v2, "progress_dialog"

    invoke-virtual {p0, p1, v2}, Lcom/google/android/finsky/activities/InstrumentActivity;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    iput-object v8, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    :cond_3
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/google/android/finsky/activities/InstrumentActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/InstrumentActivity$1;-><init>(Lcom/google/android/finsky/activities/InstrumentActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mInstrumentMode:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    sget-object v3, Lcom/google/android/finsky/activities/InstrumentActivity$Mode;->UPDATE:Lcom/google/android/finsky/activities/InstrumentActivity$Mode;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    invoke-direct {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->stopFlow()V

    return-void
.end method

.method public onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "billing_flow_error"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "billing_flow_error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->finish()V

    return-void
.end method

.method public onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2    # Z
    .param p3    # Landroid/os/Bundle;

    const/4 v6, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "billing_flow_error"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v4, "billing_flow_canceled"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    const-string v4, "instrument_id"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "instrument_id"

    const-string v5, "instrument_id"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    const-string v4, "redeemed_offer_message_html"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "redeemed_offer_message_html"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "redeemed_offer_message_html"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->showRedeemedOfferDialog()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0701da

    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "result_intent"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Lvedroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "redeemed_promo_offer"

    invoke-virtual {v1, v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v6, v3}, Lcom/google/android/finsky/activities/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->finish()V

    goto :goto_0
.end method

.method public onFlowCanceled(Lcom/google/android/finsky/billing/BillingFlowFragment;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowFragment;

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1}, Lcom/google/android/finsky/activities/InstrumentActivity;->onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V

    return-void
.end method

.method public onFlowError(Lcom/google/android/finsky/billing/BillingFlowFragment;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/finsky/activities/InstrumentActivity;->onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V

    return-void
.end method

.method public onFlowFinished(Lcom/google/android/finsky/billing/BillingFlowFragment;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/finsky/activities/InstrumentActivity;->onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V

    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const-string v1, "result_intent"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->onActivityResume()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mNeedsHideProgress:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->hideProgress()V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/BillingFlow;->saveState(Landroid/os/Bundle;)V

    const-string v1, "flow_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v1, :cond_1

    const-string v1, "progress_dialog"

    iget-object v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/finsky/activities/InstrumentActivity;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V

    :cond_1
    return-void
.end method

.method public persistFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;
    .param p3    # Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Lvedroid/support/v4/app/Fragment;)V

    return-void
.end method

.method protected removeActionBar()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/finsky/layout/CustomActionBarFactory;->getInstance(Landroid/app/Activity;)Lcom/google/android/finsky/layout/CustomActionBar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/layout/CustomActionBar;->hide()V

    return-void
.end method

.method public restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lvedroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public setHostTitle(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/InstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method protected abstract setTitle(Ljava/lang/String;)V
.end method

.method public showDialogFragment(Lvedroid/support/v4/app/DialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/DialogFragment;
    .param p2    # Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->remove(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showFragment(Lvedroid/support/v4/app/Fragment;IZ)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/activities/InstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/InstrumentActivity;->setTitle(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f080041

    invoke-virtual {v0, v1, p1}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz p3, :cond_1

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_1
    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/InstrumentActivity;->setTitle(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public showProgress(I)V
    .locals 3
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->useProgressDialog()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->newInstance(I)Lcom/google/android/finsky/billing/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f08003f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080040

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected showRedeemedOfferDialog()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
