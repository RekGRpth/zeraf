.class Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;
.super Lcom/google/android/finsky/api/SkyjamJsonObjectRequest;
.source "MusicPreviewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreviewUrlRequest"
.end annotation


# instance fields
.field private final mPriority:Lcom/android/volley/Request$Priority;

.field final synthetic this$0:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Ljava/lang/String;Lcom/android/volley/Request$Priority;)V
    .locals 2
    .param p2    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/volley/Request$Priority;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;->this$0:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest$1;

    invoke-direct {v1, p1, p2}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest$1;-><init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;)V

    invoke-direct {p0, p3, v0, v1, p1}, Lcom/google/android/finsky/api/SkyjamJsonObjectRequest;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iput-object p4, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;->mPriority:Lcom/android/volley/Request$Priority;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Ljava/lang/String;Lcom/android/volley/Request$Priority;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
    .param p2    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/volley/Request$Priority;
    .param p5    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;-><init>(Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$DownloadRequest;Ljava/lang/String;Lcom/android/volley/Request$Priority;)V

    return-void
.end method


# virtual methods
.method public getPriority()Lcom/android/volley/Request$Priority;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$PreviewUrlRequest;->mPriority:Lcom/android/volley/Request$Priority;

    return-object v0
.end method
