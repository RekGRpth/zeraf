.class public Lcom/google/android/finsky/utils/GetTocHelper;
.super Ljava/lang/Object;
.source "GetTocHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/GetTocHelper$Listener;
    }
.end annotation


# static fields
.field private static sSignatureHashHash:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 0
    .param p0    # Z
    .param p1    # Lcom/google/android/finsky/api/DfeApi;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper;->doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    return-void
.end method

.method static synthetic access$100(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 0
    .param p0    # Z
    .param p1    # Lcom/google/android/finsky/api/DfeApi;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    return-void
.end method

.method public static changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 1
    .param p0    # Lcom/google/android/finsky/api/DfeApi;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/GetTocHelper;->getSignatureHashHash(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/finsky/api/DfeApi;->invalidateTocCache(Ljava/lang/String;)V

    return-void
.end method

.method private static doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 6
    .param p0    # Z
    .param p1    # Lcom/google/android/finsky/api/DfeApi;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/GetTocHelper;->getSignatureHashHash(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/google/android/finsky/utils/GetTocHelper$3;

    invoke-direct {v4, p0, p3, p1, p2}, Lcom/google/android/finsky/utils/GetTocHelper$3;-><init>(ZLcom/google/android/finsky/utils/GetTocHelper$Listener;Lcom/google/android/finsky/api/DfeApi;Z)V

    new-instance v5, Lcom/google/android/finsky/utils/GetTocHelper$4;

    invoke-direct {v5, p3}, Lcom/google/android/finsky/utils/GetTocHelper$4;-><init>(Lcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->getToc(ZLjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    return-void
.end method

.method private static doRequestToken(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 1
    .param p0    # Z
    .param p1    # Lcom/google/android/finsky/api/DfeApi;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    new-instance v0, Lcom/google/android/finsky/utils/GetTocHelper$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper$2;-><init>(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->requestToken(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    return-void
.end method

.method public static declared-synchronized getSignatureHashHash(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0    # Landroid/content/Context;

    const-class v6, Lcom/google/android/finsky/utils/GetTocHelper;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lcom/google/android/finsky/utils/GetTocHelper;->sSignatureHashHash:Ljava/lang/String;

    if-nez v5, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v7, 0x40

    invoke-virtual {v5, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v5, 0x0

    aget-object v5, v3, v5

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Md5Util;->secureHash([B)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/google/android/finsky/utils/GetTocHelper;->sSignatureHashHash:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    sget-object v5, Lcom/google/android/finsky/utils/GetTocHelper;->sSignatureHashHash:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v6

    return-object v5

    :catch_0
    move-exception v0

    :try_start_3
    const-string v5, "Unable to find package info for %s - %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v5, "signature-hash-NameNotFoundException"

    sput-object v5, Lcom/google/android/finsky/utils/GetTocHelper;->sSignatureHashHash:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/api/DfeApi;
    .param p1    # Z
    .param p2    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->uploadIfNotRegistered(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;)V

    const/4 v1, 0x1

    invoke-static {v1, p0, p1, p2}, Lcom/google/android/finsky/utils/GetTocHelper;->doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    return-void
.end method

.method public static getTocBlocking(Lcom/google/android/finsky/api/DfeApi;)Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;
    .locals 4
    .param p0    # Lcom/google/android/finsky/api/DfeApi;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    const/4 v2, 0x1

    new-array v0, v2, [Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;

    new-instance v2, Lcom/google/android/finsky/utils/GetTocHelper$1;

    invoke-direct {v2, v0, v1}, Lcom/google/android/finsky/utils/GetTocHelper$1;-><init>([Lcom/google/android/finsky/remoting/protos/Toc$TocResponse;Ljava/util/concurrent/Semaphore;)V

    invoke-static {p0, v3, v2}, Lcom/google/android/finsky/utils/GetTocHelper;->getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    aget-object v2, v0, v3

    return-object v2
.end method
