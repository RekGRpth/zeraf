.class public Lcom/google/android/finsky/layout/play/PlayCardThumbnail;
.super Landroid/widget/RelativeLayout;
.source "PlayCardThumbnail.java"


# instance fields
.field private final mAppThumbnailPadding:I

.field private mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mThumbnail:Lcom/google/android/finsky/layout/DocImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/android/vending/R$styleable;->PlayCardThumbnail:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mAppThumbnailPadding:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private updatePadding()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mThumbnail:Lcom/google/android/finsky/layout/DocImageView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DocImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mAppThumbnailPadding:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mAppThumbnailPadding:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mAppThumbnailPadding:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mAppThumbnailPadding:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->requestLayout()V

    return-void

    :cond_0
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_0
.end method


# virtual methods
.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;[I)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # [I

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->updatePadding()V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mThumbnail:Lcom/google/android/finsky/layout/DocImageView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;[I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f0800be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DocImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->mThumbnail:Lcom/google/android/finsky/layout/DocImageView;

    return-void
.end method
