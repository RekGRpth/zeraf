.class public Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterViewContent.java"


# instance fields
.field private mDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

.field private mRespectChildHeight:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getCellSize(I)F
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getWidth()I

    move-result v0

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    int-to-float v2, v0

    div-float/2addr v1, v2

    return v1
.end method


# virtual methods
.method public addCardFromHeap(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardHeap;)Z
    .locals 4
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayCardHeap;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/finsky/utils/PlayUtils;->shouldDrawWithReasons(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v1

    invoke-virtual {p3, p1, v2, v1}, Lcom/google/android/finsky/layout/play/PlayCardHeap;->getCard(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Z)Lcom/google/android/finsky/layout/play/PlayCardView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/PlayCardView;->setThumbnailAspectRatio(F)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    return v1
.end method

.method public createContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;Ljava/util/Collection;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 24
    .param p1    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Ljava/lang/String;
    .param p5    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p6    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Lcom/google/android/finsky/utils/BitmapLoader;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;",
            "Lcom/google/android/finsky/layout/play/PlayCardHeap;",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")V"
        }
    .end annotation

    const/16 v23, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    move/from16 v0, v23

    if-ge v0, v5, :cond_3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v23

    if-ge v0, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    move/from16 v0, v23

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->addCardFromHeap(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardHeap;)Z

    move-result v22

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v10

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayCardView;

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindNoDocument()V

    :goto_1
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    :cond_1
    if-eqz v22, :cond_2

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v8

    move-object/from16 v5, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v9, p5

    move-object/from16 v11, p7

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindInStream(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;FLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    move/from16 v0, v23

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/api/model/Document;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v3

    move-object/from16 v13, p2

    move-object/from16 v14, p1

    move-object/from16 v15, p3

    move/from16 v19, v10

    move-object/from16 v20, p7

    invoke-virtual/range {v11 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;FLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public getLeadingGap()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getWidth()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getLeadingGap()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/2addr v1, v2

    return v1
.end method

.method public getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    return-object v0
.end method

.method public getTrailingGap()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getWidth()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTrailingGap()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/2addr v1, v2

    return v1
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getWidth()I

    move-result v11

    invoke-direct {p0, v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v6

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v8

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v8, :cond_0

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v11, v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v3

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getYStart()I

    move-result v4

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    int-to-float v11, v3

    mul-float/2addr v11, v5

    float-to-int v11, v11

    add-int v1, v6, v11

    int-to-float v11, v4

    mul-float/2addr v11, v5

    float-to-int v11, v11

    add-int v2, v7, v11

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v11, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v2

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getHeight()I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v12

    const/4 v8, 0x0

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v15, v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v3

    invoke-virtual {v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    int-to-float v15, v3

    mul-float/2addr v15, v7

    float-to-int v6, v15

    int-to-float v15, v5

    mul-float/2addr v15, v7

    float-to-int v4, v15

    const/high16 v15, 0x40000000

    invoke-static {v6, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mRespectChildHeight:Z

    if-eqz v15, :cond_0

    const/4 v15, 0x0

    invoke-virtual {v2, v9, v15}, Landroid/view/View;->measure(II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    invoke-static {v8, v15}, Ljava/lang/Math;->max(II)I

    move-result v8

    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_0
    const/high16 v15, 0x40000000

    invoke-static {v4, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v2, v9, v15}, Landroid/view/View;->measure(II)V

    goto :goto_1

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingBottom()I

    move-result v16

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mRespectChildHeight:Z

    move/from16 v16, v0

    if-eqz v16, :cond_2

    :goto_2
    add-int v11, v15, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMeasuredDimension(II)V

    return-void

    :cond_2
    int-to-float v0, v10

    move/from16 v16, v0

    mul-float v16, v16, v7

    move/from16 v0, v16

    float-to-int v8, v0

    goto :goto_2
.end method

.method public setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Ljava/util/List;Z)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tile / document count inconsistent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tiles defined in metadata, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " documents passed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    iput-boolean p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mRespectChildHeight:Z

    return-void
.end method
