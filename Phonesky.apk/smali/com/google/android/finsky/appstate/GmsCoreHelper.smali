.class public Lcom/google/android/finsky/appstate/GmsCoreHelper;
.super Ljava/lang/Object;
.source "GmsCoreHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;
    }
.end annotation


# static fields
.field private static final PACKAGE_NAME:Ljava/lang/String;


# instance fields
.field private final mCurrentAccount:Landroid/accounts/Account;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/finsky/config/G;->gmsCorePackageName:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->PACKAGE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/library/Libraries;
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->PACKAGE_NAME:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iput-object p2, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mCurrentAccount:Landroid/accounts/Account;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->PACKAGE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private installGmsCore(Lcom/google/android/finsky/api/model/Document;)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {v7, v0}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {v7, v0, v1, v1}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mCurrentAccount:Landroid/accounts/Account;

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v8

    if-nez v8, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mCurrentAccount:Landroid/accounts/Account;

    const/4 v2, 0x1

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseInitiator;->makeFreePurchase(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v8, p1, v3, v3, v3}, Lcom/google/android/finsky/utils/PurchaseInitiator;->initiateDownload(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;)V

    goto :goto_0
.end method

.method public static isGmsCore(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p0    # Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isGmsCore(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->PACKAGE_NAME:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public insertGmsCore(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updateGmsCore(Lcom/google/android/finsky/api/model/Document;)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {p1, v7, v8}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v5, "GMS Core is not available"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v5, "Unable to install something without app details"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-boolean v7, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabledByUser:Z

    if-eqz v7, :cond_3

    const-string v7, "Not updating %s (disabled)"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    aput-object v8, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget v1, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iget-boolean v7, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    if-eqz v7, :cond_6

    iget-boolean v7, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isUpdatedSystemApp:Z

    if-nez v7, :cond_6

    move v4, v5

    :cond_4
    :goto_1
    const/4 v7, -0x1

    if-eq v1, v7, :cond_5

    if-eqz v4, :cond_7

    :cond_5
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v7

    sget-object v8, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v7, v8, :cond_7

    const-string v7, "Not updating %s (was removed)"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mPackageName:Ljava/lang/String;

    aput-object v8, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    move v4, v6

    goto :goto_1

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getVersionCode()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->installGmsCore(Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_0
.end method
