.class public final Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingConfig"
.end annotation


# instance fields
.field private cachedSize:I

.field private carrierBillingConfig_:Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

.field private hasCarrierBillingConfig:Z

.field private hasMaxIabApiVersion:Z

.field private maxIabApiVersion_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->carrierBillingConfig_:Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->maxIabApiVersion_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->cachedSize:I

    return v0
.end method

.method public getCarrierBillingConfig()Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->carrierBillingConfig_:Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    return-object v0
.end method

.method public getMaxIabApiVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->maxIabApiVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasCarrierBillingConfig()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->getCarrierBillingConfig()Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasMaxIabApiVersion()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->getMaxIabApiVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->cachedSize:I

    return v0
.end method

.method public hasCarrierBillingConfig()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasCarrierBillingConfig:Z

    return v0
.end method

.method public hasMaxIabApiVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->setCarrierBillingConfig(Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->setMaxIabApiVersion(I)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;

    move-result-object v0

    return-object v0
.end method

.method public setCarrierBillingConfig(Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasCarrierBillingConfig:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->carrierBillingConfig_:Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    return-object p0
.end method

.method public setMaxIabApiVersion(I)Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->maxIabApiVersion_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasCarrierBillingConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->getCarrierBillingConfig()Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->hasMaxIabApiVersion()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$BillingConfig;->getMaxIabApiVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    return-void
.end method
