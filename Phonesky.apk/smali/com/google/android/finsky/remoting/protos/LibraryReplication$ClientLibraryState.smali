.class public final Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LibraryReplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/LibraryReplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientLibraryState"
.end annotation


# instance fields
.field private cachedSize:I

.field private corpus_:I

.field private hasCorpus:Z

.field private hasHashCodeSum:Z

.field private hasLibraryId:Z

.field private hasLibrarySize:Z

.field private hasServerToken:Z

.field private hashCodeSum_:J

.field private libraryId_:Ljava/lang/String;

.field private librarySize_:I

.field private serverToken_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v2, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->corpus_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->libraryId_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->serverToken_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hashCodeSum_:J

    iput v2, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->librarySize_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->cachedSize:I

    return v0
.end method

.method public getCorpus()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->corpus_:I

    return v0
.end method

.method public getHashCodeSum()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hashCodeSum_:J

    return-wide v0
.end method

.method public getLibraryId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->libraryId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLibrarySize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->librarySize_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasCorpus()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getCorpus()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasServerToken()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getServerToken()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasHashCodeSum()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getHashCodeSum()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibrarySize()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getLibrarySize()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibraryId()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getLibraryId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->cachedSize:I

    return v0
.end method

.method public getServerToken()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->serverToken_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public hasCorpus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasCorpus:Z

    return v0
.end method

.method public hasHashCodeSum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasHashCodeSum:Z

    return v0
.end method

.method public hasLibraryId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibraryId:Z

    return v0
.end method

.method public hasLibrarySize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibrarySize:Z

    return v0
.end method

.method public hasServerToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasServerToken:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->setCorpus(I)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->setServerToken(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->setHashCodeSum(J)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->setLibrarySize(I)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->setLibraryId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;

    move-result-object v0

    return-object v0
.end method

.method public setCorpus(I)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasCorpus:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->corpus_:I

    return-object p0
.end method

.method public setHashCodeSum(J)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasHashCodeSum:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hashCodeSum_:J

    return-object p0
.end method

.method public setLibraryId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibraryId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->libraryId_:Ljava/lang/String;

    return-object p0
.end method

.method public setLibrarySize(I)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibrarySize:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->librarySize_:I

    return-object p0
.end method

.method public setServerToken(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasServerToken:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->serverToken_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasCorpus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getCorpus()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasServerToken()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getServerToken()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasHashCodeSum()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getHashCodeSum()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibrarySize()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getLibrarySize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->hasLibraryId()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/LibraryReplication$ClientLibraryState;->getLibraryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
