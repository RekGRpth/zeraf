.class public final Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Library.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Library;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryMutation"
.end annotation


# instance fields
.field private appDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

.field private cachedSize:I

.field private deleted_:Z

.field private docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

.field private documentHash_:J

.field private hasAppDetails:Z

.field private hasDeleted:Z

.field private hasDocid:Z

.field private hasDocumentHash:Z

.field private hasInAppDetails:Z

.field private hasOfferType:Z

.field private hasSubscriptionDetails:Z

.field private hasValidUntilTimestampMsec:Z

.field private inAppDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

.field private offerType_:I

.field private subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

.field private validUntilTimestampMsec_:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->offerType_:I

    iput-wide v2, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->documentHash_:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->deleted_:Z

    iput-wide v2, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->validUntilTimestampMsec_:J

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->appDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->inAppDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->appDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->cachedSize:I

    return v0
.end method

.method public getDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->deleted_:Z

    return v0
.end method

.method public getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    return-object v0
.end method

.method public getDocumentHash()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->documentHash_:J

    return-wide v0
.end method

.method public getInAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->inAppDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    return-object v0
.end method

.method public getOfferType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->offerType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocid()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasOfferType()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getOfferType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocumentHash()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDocumentHash()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDeleted()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDeleted()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasAppDetails()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasSubscriptionDetails()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasInAppDetails()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getInAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasValidUntilTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getValidUntilTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->cachedSize:I

    return v0
.end method

.method public getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    return-object v0
.end method

.method public getValidUntilTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->validUntilTimestampMsec_:J

    return-wide v0
.end method

.method public hasAppDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasAppDetails:Z

    return v0
.end method

.method public hasDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDeleted:Z

    return v0
.end method

.method public hasDocid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocid:Z

    return v0
.end method

.method public hasDocumentHash()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocumentHash:Z

    return v0
.end method

.method public hasInAppDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasInAppDetails:Z

    return v0
.end method

.method public hasOfferType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasOfferType:Z

    return v0
.end method

.method public hasSubscriptionDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasSubscriptionDetails:Z

    return v0
.end method

.method public hasValidUntilTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasValidUntilTimestampMsec:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$Docid;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setDocid(Lcom/google/android/finsky/remoting/protos/Common$Docid;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setOfferType(I)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setDocumentHash(J)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setDeleted(Z)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setAppDetails(Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setSubscriptionDetails(Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setInAppDetails(Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->setValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;

    move-result-object v0

    return-object v0
.end method

.method public setAppDetails(Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasAppDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->appDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    return-object p0
.end method

.method public setDeleted(Z)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDeleted:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->deleted_:Z

    return-object p0
.end method

.method public setDocid(Lcom/google/android/finsky/remoting/protos/Common$Docid;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$Docid;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocid:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    return-object p0
.end method

.method public setDocumentHash(J)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocumentHash:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->documentHash_:J

    return-object p0
.end method

.method public setInAppDetails(Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasInAppDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->inAppDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    return-object p0
.end method

.method public setOfferType(I)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasOfferType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->offerType_:I

    return-object p0
.end method

.method public setSubscriptionDetails(Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasSubscriptionDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    return-object p0
.end method

.method public setValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasValidUntilTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->validUntilTimestampMsec_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasOfferType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getOfferType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDocumentHash()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDocumentHash()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasDeleted()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getDeleted()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasAppDetails()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryAppDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasSubscriptionDetails()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/Library$LibrarySubscriptionDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasInAppDetails()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getInAppDetails()Lcom/google/android/finsky/remoting/protos/Library$LibraryInAppDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->hasValidUntilTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Library$LibraryMutation;->getValidUntilTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_7
    return-void
.end method
