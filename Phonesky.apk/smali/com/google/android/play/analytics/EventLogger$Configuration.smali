.class public Lcom/google/android/play/analytics/EventLogger$Configuration;
.super Ljava/lang/Object;
.source "EventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/EventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Configuration"
.end annotation


# instance fields
.field public delayBetweenUploadsMs:J

.field public maxNumberOfRedirects:I

.field public maxStorageSize:J

.field public minDelayBetweenUploadsMs:J

.field public numberOfFiles:I

.field public recommendedLogFileSize:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    const-wide/32 v0, 0x200000

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    const-wide/32 v0, 0xc800

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/analytics/EventLogger$Configuration;)V
    .locals 2
    .param p1    # Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    const-wide/32 v0, 0x200000

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    const-wide/32 v0, 0xc800

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    iget v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    iget-wide v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    iget-wide v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    iget-wide v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    iget-wide v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    iget v0, p1, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    iput v0, p0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    return-void
.end method
