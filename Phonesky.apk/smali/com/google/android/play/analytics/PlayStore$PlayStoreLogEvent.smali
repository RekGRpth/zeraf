.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreLogEvent"
.end annotation


# instance fields
.field private backgroundAction_:Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

.field private cachedSize:I

.field private click_:Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

.field private deepLink_:Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

.field private hasBackgroundAction:Z

.field private hasClick:Z

.field private hasDeepLink:Z

.field private hasImpression:Z

.field private hasSearch:Z

.field private impression_:Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

.field private search_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->impression_:Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->click_:Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction_:Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->search_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->deepLink_:Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clearImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clearClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clearBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clearSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clearDeepLink()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    return-object p0
.end method

.method public clearBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasBackgroundAction:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction_:Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    return-object p0
.end method

.method public clearClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasClick:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->click_:Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    return-object p0
.end method

.method public clearDeepLink()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasDeepLink:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->deepLink_:Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    return-object p0
.end method

.method public clearImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasImpression:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->impression_:Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    return-object p0
.end method

.method public clearSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasSearch:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->search_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    return-object p0
.end method

.method public getBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction_:Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    return v0
.end method

.method public getClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->click_:Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    return-object v0
.end method

.method public getDeepLink()Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->deepLink_:Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    return-object v0
.end method

.method public getImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->impression_:Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    return-object v0
.end method

.method public getSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->search_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasImpression()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasClick()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasBackgroundAction()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasSearch()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasDeepLink()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getDeepLink()Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    return v0
.end method

.method public hasBackgroundAction()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasBackgroundAction:Z

    return v0
.end method

.method public hasClick()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasClick:Z

    return v0
.end method

.method public hasDeepLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasDeepLink:Z

    return v0
.end method

.method public hasImpression()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasImpression:Z

    return v0
.end method

.method public hasSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasSearch:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setImpression(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setClick(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setBackgroundAction(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setSearch(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->setDeepLink(Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public setBackgroundAction(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasBackgroundAction:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction_:Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    return-object p0
.end method

.method public setClick(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasClick:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->click_:Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    return-object p0
.end method

.method public setDeepLink(Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasDeepLink:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->deepLink_:Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    return-object p0
.end method

.method public setImpression(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasImpression:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->impression_:Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    return-object p0
.end method

.method public setSearch(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasSearch:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->search_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasImpression()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasClick()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasBackgroundAction()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasSearch()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->hasDeepLink()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getDeepLink()Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
