.class public Lcom/google/android/play/analytics/ProtoCache;
.super Ljava/lang/Object;
.source "ProtoCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    }
.end annotation


# static fields
.field private static INSTANCE:Lcom/google/android/play/analytics/ProtoCache;


# instance fields
.field private final mCacheLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCacheLogEventKeyValues:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreBackgroundAction:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreClick:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreImpression:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreSearch:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachePlayStoreUIElement:Lcom/google/android/play/analytics/ProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/ProtoCache$ElementCache",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/play/analytics/ProtoCache;->INSTANCE:Lcom/google/android/play/analytics/ProtoCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    const/16 v4, 0x32

    const/16 v3, 0xa

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-direct {v0, v1, v4}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEventKeyValues:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-direct {v0, v1, v3}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreImpression:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    invoke-direct {v0, v1, v3}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreClick:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-direct {v0, v1, v3}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreBackgroundAction:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-direct {v0, v1, v3}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreSearch:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    const-class v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v0, v1, v4}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreUIElement:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/google/android/play/analytics/ProtoCache;
    .locals 2

    const-class v1, Lcom/google/android/play/analytics/ProtoCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/play/analytics/ProtoCache;->INSTANCE:Lcom/google/android/play/analytics/ProtoCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/play/analytics/ProtoCache;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ProtoCache;-><init>()V

    sput-object v0, Lcom/google/android/play/analytics/ProtoCache;->INSTANCE:Lcom/google/android/play/analytics/ProtoCache;

    :cond_0
    sget-object v0, Lcom/google/android/play/analytics/ProtoCache;->INSTANCE:Lcom/google/android/play/analytics/ProtoCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;)V
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->clear()Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEventKeyValues:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public obtainEvent()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    return-object v0
.end method

.method public obtainKeyValue()Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEventKeyValues:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    return-object v0
.end method

.method public obtainPlayStoreBackgroundActionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreBackgroundAction:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    return-object v0
.end method

.method public obtainPlayStoreClickEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreClick:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    return-object v0
.end method

.method public obtainPlayStoreImpressionEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreImpression:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    return-object v0
.end method

.method public obtainPlayStoreLogEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    return-object v0
.end method

.method public obtainPlayStoreSearchEvent()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreSearch:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    return-object v0
.end method

.method public obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreUIElement:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V
    .locals 3
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->getValueList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-direct {p0, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->getStore()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->getStore()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->clear()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    iget-object v2, p0, Lcom/google/android/play/analytics/ProtoCache;->mCacheLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreBackgroundAction:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V
    .locals 3
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;->getElementPathList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    iget-object v2, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreClick:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V
    .locals 3
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getReferrerPathList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    iget-object v2, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreImpression:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;)V
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getImpression()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getClick()Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getBackgroundAction()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->getSearch()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreLogEvent:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;)V
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    iget-object v0, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreSearch:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V
    .locals 3
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p0, Lcom/google/android/play/analytics/ProtoCache;->mCachePlayStoreUIElement:Lcom/google/android/play/analytics/ProtoCache$ElementCache;

    invoke-virtual {v2, p1}, Lcom/google/android/play/analytics/ProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    return-void
.end method

.method public recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    .locals 3
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    invoke-virtual {p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getLogEventList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method
