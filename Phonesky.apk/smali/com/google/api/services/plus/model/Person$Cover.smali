.class public final Lcom/google/api/services/plus/model/Person$Cover;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Person.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/plus/model/Person;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Cover"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/plus/model/Person$Cover$CoverPhoto;,
        Lcom/google/api/services/plus/model/Person$Cover$CoverInfo;
    }
.end annotation


# instance fields
.field public coverInfo:Lcom/google/api/services/plus/model/Person$Cover$CoverInfo;

.field public coverPhoto:Lcom/google/api/services/plus/model/Person$Cover$CoverPhoto;

.field public layout:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
