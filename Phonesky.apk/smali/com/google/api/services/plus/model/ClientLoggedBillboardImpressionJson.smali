.class public final Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedBillboardImpressionJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "billboardId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;->billboardId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;->getValues(Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientLoggedBillboardImpressionJson;->newInstance()Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

    move-result-object v0

    return-object v0
.end method
