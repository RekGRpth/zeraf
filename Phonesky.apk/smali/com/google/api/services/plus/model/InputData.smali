.class public final Lcom/google/api/services/plus/model/InputData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "InputData.java"


# instance fields
.field public interestsAdded:Ljava/lang/Integer;

.field public interestsRemoved:Ljava/lang/Integer;

.field public invitesSent:Ljava/lang/Integer;

.field public profileCreationInfo:Lcom/google/api/services/plus/model/ProfileCreationInfo;

.field public profileEdit:Lcom/google/api/services/plus/model/ProfileEdit;

.field public socialCircleRawQuery:Ljava/lang/String;

.field public upgradeInfo:Lcom/google/api/services/plus/model/UpgradeInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
