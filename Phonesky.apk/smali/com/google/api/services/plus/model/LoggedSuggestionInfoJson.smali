.class public final Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedSuggestionInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedSuggestionInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "friendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "friendSuggestionSummarizedAdditionalInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "explanationType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "experimentNames"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numberOfCircleMembersAdded"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "score"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "queryId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "suggestedCircleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "placement"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "suggestionType"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "explanationsTypesBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "suggestedCircle"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "numberOfCircleMembersRemoved"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedSuggestionInfo;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedSuggestionInfo;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->explanationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->experimentNames:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->numberOfCircleMembersAdded:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->score:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->queryId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->suggestedCircleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->deprecatedFriendSuggestionSummarizedInfoBitmask:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->placement:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->suggestionType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->explanationsTypesBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->suggestedCircle:Lcom/google/api/services/plus/model/LoggedCircle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;->numberOfCircleMembersRemoved:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;->getValues(Lcom/google/api/services/plus/model/LoggedSuggestionInfo;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedSuggestionInfo;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedSuggestionInfo;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;->newInstance()Lcom/google/api/services/plus/model/LoggedSuggestionInfo;

    move-result-object v0

    return-object v0
.end method
