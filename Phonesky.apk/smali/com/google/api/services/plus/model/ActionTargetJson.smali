.class public final Lcom/google/api/services/plus/model/ActionTargetJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActionTargetJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ActionTarget;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ActionTargetJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ActionTargetJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ActionTargetJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ActionTargetJson;->INSTANCE:Lcom/google/api/services/plus/model/ActionTargetJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ActionTarget;

    const/16 v1, 0x4e

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/SettingsNotificationTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "settingsNotificationType"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "labelId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "iphFlowId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plus/model/LoggedFrameJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "frame"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "notificationWidgetPreReloadBuildLabel"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "gadgetPlayId"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plus/model/VolumeChangeJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "volumeChange"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plus/model/ActivityDetailsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "activityDetails"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "tab"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "notificationWidgetUpTimeBeforeReload"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "promoType"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "updateStreamPosition"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "photoAlbumIdDeprecated"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "isUnreadNotification"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "plusEventId"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "gadgetId"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "numUnreadNotifications"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "commentId"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "externalUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "iphStepId"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "questionsOneboxQuery"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string v3, "deprecatedCircleId"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string v3, "connectSiteId"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-class v3, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "rhsComponent"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string v3, "notificationSlot"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string v3, "photoCount"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-class v3, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string v3, "shareboxInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string v3, "notificationId"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-class v3, Lcom/google/api/services/plus/model/LoggedBillboardImpressionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string v3, "billboardImpression"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string v3, "gaiaId"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string v3, "notificationWidgetPostReloadBuildLabel"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-class v3, Lcom/google/api/services/plus/model/LoggedAutoCompleteJson;

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string v3, "autoComplete"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-class v3, Lcom/google/api/services/plus/model/LoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string v3, "suggestionSummary"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string v3, "actionSource"

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-class v3, Lcom/google/api/services/plus/model/LoggedIntrCelebsClickJson;

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-string v3, "intrCelebsClick"

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-string v3, "deprecatedSettingsNotificationType"

    aput-object v3, v1, v2

    const/16 v2, 0x37

    sget-object v3, Lcom/google/api/services/plus/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-class v3, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-string v3, "photoAlbumId"

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-class v3, Lcom/google/api/services/plus/model/LoggedBillboardPromoActionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string v3, "billboardPromoAction"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string v3, "region"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-string v3, "profileData"

    aput-object v3, v1, v2

    const/16 v2, 0x40

    const-string v3, "categoryId"

    aput-object v3, v1, v2

    const/16 v2, 0x41

    const-class v3, Lcom/google/api/services/plus/model/NotificationTypesJson;

    aput-object v3, v1, v2

    const/16 v2, 0x42

    const-string v3, "notificationTypes"

    aput-object v3, v1, v2

    const/16 v2, 0x43

    const-string v3, "entityTypeId"

    aput-object v3, v1, v2

    const/16 v2, 0x44

    const-string v3, "photoAlbumType"

    aput-object v3, v1, v2

    const/16 v2, 0x45

    const-string v3, "shortcutTask"

    aput-object v3, v1, v2

    const/16 v2, 0x46

    const-string v3, "previousNumUnreadNotifications"

    aput-object v3, v1, v2

    const/16 v2, 0x47

    const-class v3, Lcom/google/api/services/plus/model/LoggedRibbonClickJson;

    aput-object v3, v1, v2

    const/16 v2, 0x48

    const-string v3, "ribbonClick"

    aput-object v3, v1, v2

    const/16 v2, 0x49

    const-string v3, "page"

    aput-object v3, v1, v2

    const/16 v2, 0x4a

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4b

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x4c

    const-class v3, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4d

    const-string v3, "ribbonOrder"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ActionTargetJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ActionTargetJson;->INSTANCE:Lcom/google/api/services/plus/model/ActionTargetJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ActionTarget;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ActionTarget;

    const/16 v0, 0x36

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->settingsNotificationType:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->labelId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->iphFlowId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->frame:Lcom/google/api/services/plus/model/LoggedFrame;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationWidgetPreReloadBuildLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->gadgetPlayId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->volumeChange:Lcom/google/api/services/plus/model/VolumeChange;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->activityDetails:Lcom/google/api/services/plus/model/ActivityDetails;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->tab:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationWidgetUpTimeBeforeReload:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->promoType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->updateStreamPosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->photoAlbumIdDeprecated:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->isUnreadNotification:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->plusEventId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->gadgetId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->numUnreadNotifications:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->commentId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->externalUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->iphStepId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->questionsOneboxQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->deprecatedCircleId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->connectSiteId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->rhsComponent:Lcom/google/api/services/plus/model/LoggedRhsComponent;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationSlot:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->photoCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->shareboxInfo:Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->billboardImpression:Lcom/google/api/services/plus/model/LoggedBillboardImpression;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->gaiaId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationWidgetPostReloadBuildLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->autoComplete:Lcom/google/api/services/plus/model/LoggedAutoComplete;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->suggestionSummary:Lcom/google/api/services/plus/model/LoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->actionSource:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->intrCelebsClick:Lcom/google/api/services/plus/model/LoggedIntrCelebsClick;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->deprecatedSettingsNotificationType:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->photoId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->photoAlbumId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->billboardPromoAction:Lcom/google/api/services/plus/model/LoggedBillboardPromoAction;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->region:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->profileData:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->categoryId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->notificationTypes:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->entityTypeId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->photoAlbumType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->shortcutTask:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->previousNumUnreadNotifications:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->ribbonClick:Lcom/google/api/services/plus/model/LoggedRibbonClick;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->page:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActionTarget;->ribbonOrder:Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ActionTarget;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ActionTargetJson;->getValues(Lcom/google/api/services/plus/model/ActionTarget;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ActionTarget;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ActionTarget;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ActionTarget;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ActionTargetJson;->newInstance()Lcom/google/api/services/plus/model/ActionTarget;

    move-result-object v0

    return-object v0
.end method
