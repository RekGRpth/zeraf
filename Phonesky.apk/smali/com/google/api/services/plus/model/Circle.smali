.class public final Lcom/google/api/services/plus/model/Circle;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Circle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/plus/model/Circle$People;
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public etag:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public kind:Ljava/lang/String;

.field public people:Lcom/google/api/services/plus/model/Circle$People;

.field public selfLink:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
