.class public final Lcom/google/api/services/plus/model/ClientOzExtensionJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientOzExtensionJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientOzExtension;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientOzExtensionJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientOzExtensionJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOzExtensionJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientOzExtensionJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientOzExtension;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "callingApplication"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "sendTimeMsec"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/ClientOzEventJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "clientEvent"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "clientVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "clientId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientOzExtensionJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientOzExtensionJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientOzExtension;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientOzExtension;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOzExtension;->callingApplication:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOzExtension;->sendTimeMsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOzExtension;->clientVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOzExtension;->clientId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientOzExtension;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->getValues(Lcom/google/api/services/plus/model/ClientOzExtension;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientOzExtension;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientOzExtension;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOzExtension;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->newInstance()Lcom/google/api/services/plus/model/ClientOzExtension;

    move-result-object v0

    return-object v0
.end method
