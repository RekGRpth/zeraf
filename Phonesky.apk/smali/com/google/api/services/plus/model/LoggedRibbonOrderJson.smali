.class public final Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedRibbonOrderJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedRibbonOrder;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/LoggedRibbonOrderOrderJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "newOrder"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "droppedArea"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/LoggedRibbonOrderOrderJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "prevOrder"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "componentId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedRibbonOrder;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRibbonOrder;->newOrder:Lcom/google/api/services/plus/model/LoggedRibbonOrderOrder;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRibbonOrder;->droppedArea:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRibbonOrder;->prevOrder:Lcom/google/api/services/plus/model/LoggedRibbonOrderOrder;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRibbonOrder;->componentId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;->getValues(Lcom/google/api/services/plus/model/LoggedRibbonOrder;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedRibbonOrder;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedRibbonOrder;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedRibbonOrderJson;->newInstance()Lcom/google/api/services/plus/model/LoggedRibbonOrder;

    move-result-object v0

    return-object v0
.end method
