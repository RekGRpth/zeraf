.class public final Lcom/google/api/services/plus/model/LoggedRhsComponentJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedRhsComponentJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedRhsComponent;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedRhsComponentJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedRhsComponentJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedRhsComponent;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "index"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plus/model/LoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "suggestionSummaryInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/LoggedRhsComponentTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "neighborInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plus/model/LoggedRhsComponentTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "componentType"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plus/model/LoggedRhsComponentItemJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "item"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "barType"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedRhsComponentJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedRhsComponentJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedRhsComponent;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedRhsComponent;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->index:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->suggestionSummaryInfo:Lcom/google/api/services/plus/model/LoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->neighborInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->componentType:Lcom/google/api/services/plus/model/LoggedRhsComponentType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->item:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;->barType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedRhsComponent;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;->getValues(Lcom/google/api/services/plus/model/LoggedRhsComponent;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedRhsComponent;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedRhsComponent;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedRhsComponent;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedRhsComponentJson;->newInstance()Lcom/google/api/services/plus/model/LoggedRhsComponent;

    move-result-object v0

    return-object v0
.end method
