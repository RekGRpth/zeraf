.class public final Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "InterstitialRedirectorDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/InterstitialRedirectorData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;->INSTANCE:Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/InterstitialRedirectorData;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "decision"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "birthdayCompleted"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "hasSeenRecently"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "hasProfilePhoto"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;->INSTANCE:Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/InterstitialRedirectorData;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/InterstitialRedirectorData;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/InterstitialRedirectorData;->decision:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/InterstitialRedirectorData;->birthdayCompleted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/InterstitialRedirectorData;->hasSeenRecently:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/InterstitialRedirectorData;->hasProfilePhoto:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/InterstitialRedirectorData;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;->getValues(Lcom/google/api/services/plus/model/InterstitialRedirectorData;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/InterstitialRedirectorData;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/InterstitialRedirectorData;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/InterstitialRedirectorData;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/InterstitialRedirectorDataJson;->newInstance()Lcom/google/api/services/plus/model/InterstitialRedirectorData;

    move-result-object v0

    return-object v0
.end method
