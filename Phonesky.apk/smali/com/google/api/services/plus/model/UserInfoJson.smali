.class public final Lcom/google/api/services/plus/model/UserInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "UserInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/UserInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/UserInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/UserInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/UserInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/UserInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/UserInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/UserInfo;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/api/services/plus/model/UserInfoJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "gaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "plusPageType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "entityTypeId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "userContext"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/UserInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/UserInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/UserInfoJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/UserInfo;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/UserInfo;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/UserInfo;->gaiaId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/UserInfo;->plusPageType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/UserInfo;->entityTypeId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/UserInfo;->userContext:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/UserInfo;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/UserInfoJson;->getValues(Lcom/google/api/services/plus/model/UserInfo;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/UserInfo;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/UserInfo;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/UserInfo;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/UserInfoJson;->newInstance()Lcom/google/api/services/plus/model/UserInfo;

    move-result-object v0

    return-object v0
.end method
