.class public final Lcom/google/api/services/plus/model/Person$Name;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Person.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/plus/model/Person;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Name"
.end annotation


# instance fields
.field public familyName:Ljava/lang/String;

.field public formatted:Ljava/lang/String;

.field public givenName:Ljava/lang/String;

.field public honorificPrefix:Ljava/lang/String;

.field public honorificSuffix:Ljava/lang/String;

.field public middleName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
