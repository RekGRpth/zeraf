.class public Lcom/mediatek/CellConnService/ConfirmDlgActivity;
.super Landroid/app/Activity;
.source "ConfirmDlgActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field public static final CANCEL_BUTTON:I = 0x7f070004

.field private static final LOGTAG:Ljava/lang/String; = "ConfirmDlgActivity"

.field public static final OK_BUTTON:I = 0x7f070005

.field private static final SYSTEM_DIALOG_REASON_KEY:Ljava/lang/String; = "reason"

.field private static final SYSTEM_DIALOG_REASON_RECENT_APPS:Ljava/lang/String; = "recentapps"


# instance fields
.field private mAlertDlg:Landroid/app/AlertDialog;

.field private mBReceiverIsUnregistered:Z

.field private mButtonText:Ljava/lang/String;

.field private mConfirmType:I

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mNegativeExit:Z

.field private mPreferSlot:I

.field private mRButtonText:Ljava/lang/String;

.field private mResultSent:Z

.field private mRoamingWithPrefer:Z

.field private mSlot:I

.field private mText:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mBReceiverIsUnregistered:Z

    new-instance v0, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity$1;-><init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V

    iput-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/CellConnService/ConfirmDlgActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/ConfirmDlgActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mNegativeExit:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/CellConnService/ConfirmDlgActivity;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/CellConnService/ConfirmDlgActivity;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(IZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/CellConnService/ConfirmDlgActivity;

    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    return v0
.end method

.method private dismissAlertDialog()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "dismissAlertDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "Trying to dismiss a dialog not connected to the current UI"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initFromIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Intent;

    const v4, 0x7f050017

    const/4 v5, 0x1

    const v7, 0x104000a

    const/4 v6, 0x0

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "initFromIntent ++ "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v6, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-eqz p1, :cond_a

    const-string v1, "confirm_slot"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    const-string v1, "ConfirmDlgActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initFromIntent mSlot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    iput v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mPreferSlot:I

    const-string v1, "confirm_type"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    const-string v1, "ConfirmDlgActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initFromIntent confirmType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    invoke-static {v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmTypeToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x191

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050004

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "confirm_cardName"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    if-ne v0, v5, :cond_0

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    invoke-direct {p0, v1}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->isRadioOffBySimManagement(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050006

    new-array v4, v5, [Ljava/lang/Object;

    const-string v5, "confirm_cardName"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    :cond_1
    :goto_1
    const-string v1, "ConfirmDlgActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initFromIntent - ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/16 v1, 0x192

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const/16 v1, 0x193

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_5

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    const/16 v1, 0x194

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    const/16 v1, 0x195

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_8

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    const-string v1, "confirm_roamingWithPrefer"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRoamingWithPrefer:Z

    iget-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRoamingWithPrefer:Z

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_8
    const/16 v1, 0x196

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_9

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_9
    const/16 v1, 0x197

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1
.end method

.method private isRadioOffBySimManagement(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "RADIO_STATUS"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v2

    const/4 v1, 0x1

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v3, v2, Landroid/provider/Telephony$SIMInfo;->mICCId:Ljava/lang/String;

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    return v1
.end method

.method private sendConfirmResult(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v1, "ConfirmDlgActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendConfirmResult confirmType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmTypeToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " nRet = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmResultToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "start_type"

    const-string v3, "response"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "sendConfirmResult new retIntent failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "confirm_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "confirm_result_preferSlot"

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mPreferSlot:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mNegativeExit:Z

    if-nez v1, :cond_2

    const/16 v1, 0x195

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRoamingWithPrefer:Z

    if-eqz v1, :cond_2

    const-string v1, "confirm_result"

    const/16 v2, 0x1c3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_1
    iget-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mBReceiverIsUnregistered:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mBReceiverIsUnregistered:Z

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-direct {p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->dismissAlertDialog()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const-string v1, "confirm_result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method private sendConfirmResult(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    const v6, 0x7f050018

    const/16 v5, 0x195

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "ConfirmDlgActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendConfirmResult confirmType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->confirmTypeToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bRet = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mResultSent:Z

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v5, v2, :cond_3

    iget-boolean v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRoamingWithPrefer:Z

    if-eqz v2, :cond_3

    if-eqz p2, :cond_2

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    if-nez v2, :cond_1

    :goto_0
    iput v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mPreferSlot:I

    :cond_0
    :goto_1
    if-eqz p2, :cond_7

    const/16 v0, 0x1c3

    invoke-direct {p0, p1, v0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(II)V

    :goto_2
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mSlot:I

    iput v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mPreferSlot:I

    goto :goto_1

    :cond_3
    if-nez p2, :cond_0

    const/16 v0, 0x191

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v0, v2, :cond_4

    const v0, 0x7f050019

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    const/16 v0, 0x192

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v0, v2, :cond_5

    invoke-static {p0, v6, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_5
    const/16 v0, 0x193

    iget v2, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v0, v2, :cond_6

    invoke-static {p0, v6, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v5, v0, :cond_0

    const v0, 0x7f05001a

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_7
    const/16 v0, 0x1c4

    invoke-direct {p0, p1, v0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(II)V

    goto :goto_2
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onClick is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onClick is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(IZ)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    if-nez v0, :cond_0

    const-string v1, "ConfirmDlgActivity"

    const-string v2, "onCreate new intent failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRoamingWithPrefer:Z

    iput v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mPreferSlot:I

    iput-boolean v3, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mNegativeExit:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mBReceiverIsUnregistered:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mBReceiverIsUnregistered:Z

    iget-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const-string v1, "ConfirmDlgActivity"

    const-string v2, "onKeyDown back confirm result is false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mNegativeExit:Z

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    invoke-direct {p0, v1, v0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->sendConfirmResult(IZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->initFromIntent(Landroid/content/Intent;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 4

    const v3, 0x1080027

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "ConfirmDlgActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x194

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x196

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x197

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/CellConnService/ConfirmDlgActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity$2;-><init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    :goto_0
    iget-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    new-instance v1, Lcom/mediatek/CellConnService/ConfirmDlgActivity$5;

    invoke-direct {v1, p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity$5;-><init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mResultSent:Z

    return-void

    :cond_1
    const/16 v0, 0x195

    iget v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mConfirmType:I

    if-ne v0, v1, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mRButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/CellConnService/ConfirmDlgActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity$3;-><init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/CellConnService/ConfirmDlgActivity$4;

    invoke-direct {v1, p0}, Lcom/mediatek/CellConnService/ConfirmDlgActivity$4;-><init>(Lcom/mediatek/CellConnService/ConfirmDlgActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/CellConnService/ConfirmDlgActivity;->mAlertDlg:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method
