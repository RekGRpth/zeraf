.class public Lcom/mediatek/todos/TodoAsyncQuery;
.super Landroid/content/AsyncQueryHandler;
.source "TodoAsyncQuery.java"


# static fields
.field public static final DONE_SELECTION:Ljava/lang/String; = "status= 0"

.field private static final TAG:Ljava/lang/String; = "AsyncQuery"

.field public static final TODO_SELECTION:Ljava/lang/String; = "status= 1"

.field public static final TODO_URI:Landroid/net/Uri;

.field public static final TOKEN_DELETE_COMPLETE:I = 0x4

.field public static final TOKEN_DONE:I = 0x2

.field public static final TOKEN_INSERT_COMPLETE:I = 0x3

.field public static final TOKEN_QUERY_COMPLETE:I = 0x6

.field public static final TOKEN_TODO:I = 0x1

.field public static final TOKEN_UPDATE_COMPLETE:I = 0x5

.field private static sInstance:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/mediatek/todos/TodoAsyncQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.mediatek.todos/todos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/todos/TodoAsyncQuery;->TODO_URI:Landroid/net/Uri;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method

.method public static free(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static freeAll()V
    .locals 1

    sget-object v0, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public static getInstatnce(Landroid/content/Context;)Lcom/mediatek/todos/TodoAsyncQuery;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodoAsyncQuery;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodoAsyncQuery;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v1, Lcom/mediatek/todos/TodoAsyncQuery;->sInstance:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoAsyncQuery;

    return-object v1
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const-string v1, "AsyncQuery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeleteComplete token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v1, p2, Lcom/mediatek/todos/QueryListener;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/mediatek/todos/QueryListener;

    invoke-interface {v0, p1, p3}, Lcom/mediatek/todos/QueryListener;->onDeleteComplete(II)V

    :cond_0
    return-void
.end method

.method protected onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;

    const-string v1, "AsyncQuery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInsertComplete token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v1, p2, Lcom/mediatek/todos/QueryListener;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/mediatek/todos/QueryListener;

    invoke-interface {v0, p1, p3}, Lcom/mediatek/todos/QueryListener;->onInsertComplete(ILandroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const-string v1, "AsyncQuery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onQueryComplete token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v1, p2, Lcom/mediatek/todos/QueryListener;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/mediatek/todos/QueryListener;

    invoke-interface {v0, p1, p3}, Lcom/mediatek/todos/QueryListener;->onQueryComplete(ILandroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    const-string v1, "AsyncQuery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cookie is another object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onUpdateComplete(ILjava/lang/Object;I)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const-string v1, "AsyncQuery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpdateComplete token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v1, p2, Lcom/mediatek/todos/QueryListener;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/mediatek/todos/QueryListener;

    invoke-interface {v0, p1, p3}, Lcom/mediatek/todos/QueryListener;->onUpdateComplete(II)V

    :cond_0
    return-void
.end method
