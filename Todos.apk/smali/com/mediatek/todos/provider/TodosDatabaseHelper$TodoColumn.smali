.class public interface abstract Lcom/mediatek/todos/provider/TodosDatabaseHelper$TodoColumn;
.super Ljava/lang/Object;
.source "TodosDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/provider/TodosDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TodoColumn"
.end annotation


# static fields
.field public static final COMPLETE_TIME:Ljava/lang/String; = "complete_time"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DTEND:Ljava/lang/String; = "dtend"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final PRIORITY:Ljava/lang/String; = "priority"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final TITLE:Ljava/lang/String; = "title"
