.class Lcom/mediatek/todos/TimeChangeReceiver$1;
.super Landroid/os/Handler;
.source "TimeChangeReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/TimeChangeReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/todos/TimeChangeReceiver;


# direct methods
.method constructor <init>(Lcom/mediatek/todos/TimeChangeReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/todos/TimeChangeReceiver$1;->this$0:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/todos/TimeChangeReceiver$1;->this$0:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-static {v2}, Lcom/mediatek/todos/TimeChangeReceiver;->access$000(Lcom/mediatek/todos/TimeChangeReceiver;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;

    invoke-interface {v1}, Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;->onDateChange()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/todos/TimeChangeReceiver$1;->this$0:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-static {v2}, Lcom/mediatek/todos/TimeChangeReceiver;->access$000(Lcom/mediatek/todos/TimeChangeReceiver;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;

    invoke-interface {v1}, Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;->onTimePick()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
