.class final Lcom/mediatek/todos/TodoEditText$1;
.super Landroid/text/InputFilter$LengthFilter;
.source "TodoEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/todos/TodoEditText;->registerLengthFilter(Landroid/content/Context;Landroid/widget/EditText;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$maxLength:I


# direct methods
.method constructor <init>(IILandroid/content/Context;)V
    .locals 0
    .param p1    # I

    iput p2, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    iput-object p3, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/Spanned;
    .param p5    # I
    .param p6    # I

    invoke-interface {p4, p5, p6}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v6

    invoke-static {v1}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v7

    sub-int v0, v6, v7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v4

    add-int v6, v0, v4

    iget v7, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    if-le v6, v7, :cond_1

    iget-object v6, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    const v7, 0x7f070019

    invoke-static {v6, v7}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    iget-object v6, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    iget-object v7, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    const-string v7, "vibrator"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Vibrator;

    invoke-virtual {v5}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v6, 0x2

    new-array v6, v6, [J

    fill-array-data v6, :array_0

    const/4 v7, -0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Vibrator;->vibrate([JI)V

    :cond_0
    const-string v6, "TodoEditText"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "input out of range,hasVibrator:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    if-lt v0, v6, :cond_2

    const-string p1, ""

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    sub-int/2addr v7, v0

    invoke-static {v6, v7}, Lcom/mediatek/todos/TodoEditText;->access$100(Ljava/lang/String;I)I

    move-result v3

    invoke-interface {p1, p2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    nop

    :array_0
    .array-data 8
        0x64
        0x64
    .end array-data
.end method
