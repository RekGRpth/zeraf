.class Lcom/mediatek/apkinstaller/APKInstaller$3;
.super Landroid/os/storage/StorageEventListener;
.source "APKInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apkinstaller/APKInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/apkinstaller/APKInstaller;


# direct methods
.method constructor <init>(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Landroid/os/storage/StorageEventListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorageStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x1

    const-string v2, "APKInstaller"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received storage state changed notification that "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " changed state from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v2, v2, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v2}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1100(Lcom/mediatek/apkinstaller/APKInstaller;)Landroid/os/storage/StorageManager;

    move-result-object v4

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v2, v2, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "mounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v3

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v2, v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1502(Lcom/mediatek/apkinstaller/APKInstaller;Z)Z

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :cond_2
    const-string v2, "mounted"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v2}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1600(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->stopWatching()V

    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v2, v2, Lcom/mediatek/apkinstaller/APKInstaller;->mUiHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v2}, Lcom/mediatek/apkinstaller/APKInstaller;->access$000(Lcom/mediatek/apkinstaller/APKInstaller;)Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_4
    const-string v2, "mounted"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$3;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v2}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1600(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    goto :goto_2
.end method
