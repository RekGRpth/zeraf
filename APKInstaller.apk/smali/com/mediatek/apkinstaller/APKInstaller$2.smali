.class Lcom/mediatek/apkinstaller/APKInstaller$2;
.super Landroid/os/Handler;
.source "APKInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apkinstaller/APKInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/apkinstaller/APKInstaller;


# direct methods
.method constructor <init>(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$200(Lcom/mediatek/apkinstaller/APKInstaller;)V

    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$300(Lcom/mediatek/apkinstaller/APKInstaller;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->removeAll()V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$400(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const-string v3, "APKInstaller"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mAppDetail.size() when refreshing UI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$400(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;

    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v4, v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    iget-object v5, v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;->name:Ljava/lang/String;

    iget-object v6, v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;->intent:Landroid/content/Intent;

    invoke-static {v3, v4, v5, v6}, Lcom/mediatek/apkinstaller/APKInstaller;->access$500(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/content/Intent;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$600(Lcom/mediatek/apkinstaller/APKInstaller;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$2;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-virtual {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->startObserver()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
