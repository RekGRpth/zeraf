.class Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "CdsRadioInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/connectivity/CdsRadioInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GeminiPhoneStateListener"
.end annotation


# instance fields
.field mListenSimID:I

.field final synthetic this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/connectivity/CdsRadioInfoActivity;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->mListenSimID:I

    iput p2, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->mListenSimID:I

    return-void
.end method


# virtual methods
.method public onCallForwardingIndicatorChanged(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .locals 3
    .param p1    # Landroid/telephony/CellLocation;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "cds_phone"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sim id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->mListenSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v1}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$500(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$600(Lcom/mediatek/connectivity/CdsRadioInfoActivity;Landroid/telephony/CellLocation;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$400(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)V

    return-void
.end method

.method public onDataConnectionStateChanged(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$000(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)V

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$100(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)V

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$200(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)V

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsRadioInfoActivity$GeminiPhoneStateListener;->this$0:Lcom/mediatek/connectivity/CdsRadioInfoActivity;

    invoke-static {v0}, Lcom/mediatek/connectivity/CdsRadioInfoActivity;->access$300(Lcom/mediatek/connectivity/CdsRadioInfoActivity;)V

    return-void
.end method

.method public onMessageWaitingIndicatorChanged(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method
