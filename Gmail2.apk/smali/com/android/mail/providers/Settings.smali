.class public Lcom/android/mail/providers/Settings;
.super Ljava/lang/Object;
.source "Settings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/mail/providers/Settings;",
            ">;"
        }
    .end annotation
.end field

.field static final EMPTY_SETTINGS:Lcom/android/mail/providers/Settings;

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final sDefault:Lcom/android/mail/providers/Settings;


# instance fields
.field public final confirmArchive:Z

.field public final confirmDelete:Z

.field public final confirmSend:Z

.field public final conversationViewMode:I

.field public final defaultInbox:Landroid/net/Uri;

.field public final defaultInboxName:Ljava/lang/String;

.field public final forceReplyFromDefault:Z

.field public final hideCheckboxes:Z

.field private final mAutoAdvance:I

.field private mHashCode:I

.field private mTransientAutoAdvance:Ljava/lang/Integer;

.field public final maxAttachmentSize:I

.field public final messageTextSize:I

.field public final priorityArrowsEnabled:Z

.field public final replyBehavior:I

.field public final setupIntentUri:Landroid/net/Uri;

.field public final signature:Ljava/lang/String;

.field public final snapHeaders:I

.field public final swipe:I

.field public final veiledAddressPattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/providers/Settings;->LOG_TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/mail/providers/Settings;

    invoke-direct {v0}, Lcom/android/mail/providers/Settings;-><init>()V

    sput-object v0, Lcom/android/mail/providers/Settings;->EMPTY_SETTINGS:Lcom/android/mail/providers/Settings;

    sget-object v0, Lcom/android/mail/providers/Settings;->EMPTY_SETTINGS:Lcom/android/mail/providers/Settings;

    sput-object v0, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    new-instance v0, Lcom/android/mail/providers/Settings$1;

    invoke-direct {v0}, Lcom/android/mail/providers/Settings$1;-><init>()V

    sput-object v0, Lcom/android/mail/providers/Settings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    iput v1, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    iput v1, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    iput v1, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    iput v1, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    iput v1, p0, Lcom/android/mail/providers/Settings;->swipe:I

    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    iput-object v2, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    const/16 v0, 0x1d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    const/16 v0, 0x1e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    const/16 v0, 0x1f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    const/16 v0, 0x20

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    const/16 v0, 0x21

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    const/16 v0, 0x22

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    const/16 v0, 0x23

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    const/16 v0, 0x26

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    const/16 v0, 0x27

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->swipe:I

    const/16 v0, 0x28

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    const/16 v0, 0x29

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    const/16 v0, 0x2a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    const/16 v0, 0x2b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method

.method private constructor <init>(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    const-string v0, "signature"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v1, v1, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    const-string v0, "auto_advance"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    invoke-virtual {v1}, Lcom/android/mail/providers/Settings;->getAutoAdvanceSetting()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    const-string v0, "message_text_size"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    const-string v0, "snap_headers"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    const-string v0, "reply_behavior"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    const-string v0, "hide_checkboxes"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    const-string v0, "confirm_delete"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    const-string v0, "confirm_archive"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    const-string v0, "confirm_send"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->confirmSend:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    const-string v0, "default_inbox"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    const-string v0, "default_inbox_name"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v1, v1, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    const-string v0, "force_reply_from_default"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    const-string v0, "max_attachment_size"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    const-string v0, "swipe"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->swipe:I

    const-string v0, "priority_inbox_arrows_enabled"

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    const-string v0, "setup_intent_uri"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getValidUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    const-string v0, "conversation_view_mode"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    const-string v0, "veiled_address_pattern"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    return-void
.end method

.method private final calculateHashCode()I
    .locals 4

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    iget v3, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget v3, p0, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    iget v3, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x11

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public static getDefaultInboxUri(Lcom/android/mail/providers/Settings;)Landroid/net/Uri;
    .locals 2
    .param p0    # Lcom/android/mail/providers/Settings;

    if-nez p0, :cond_0

    sget-object v0, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v0, v0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v1, v1, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    goto :goto_0
.end method

.method private static final getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    if-nez p0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p0

    goto :goto_0
.end method

.method public static getSwipeSetting(Lcom/android/mail/providers/Settings;)I
    .locals 1
    .param p0    # Lcom/android/mail/providers/Settings;

    if-eqz p0, :cond_0

    iget v0, p0, Lcom/android/mail/providers/Settings;->swipe:I

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget v0, v0, Lcom/android/mail/providers/Settings;->swipe:I

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/android/mail/providers/Settings;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v3, Lcom/android/mail/providers/Settings;

    invoke-direct {v3, v2}, Lcom/android/mail/providers/Settings;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    :goto_1
    sget-object v3, Lcom/android/mail/providers/Settings;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Could not create an settings from this input: \"%s\""

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v3, v0, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v3, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static newInstance(Lorg/json/JSONObject;)Lcom/android/mail/providers/Settings;
    .locals 1
    .param p0    # Lorg/json/JSONObject;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/mail/providers/Settings;

    invoke-direct {v0, p0}, Lcom/android/mail/providers/Settings;-><init>(Lorg/json/JSONObject;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v3, Lcom/android/mail/providers/Settings;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Settings.equals(%s)"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/android/mail/providers/Settings;

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    iget-object v4, v0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->swipe:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->swipe:I

    if-ne v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    iget-boolean v4, v0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    iget v4, v0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto/16 :goto_0
.end method

.method public getAutoAdvanceSetting()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/mail/providers/Settings;->mAutoAdvance:I

    goto :goto_0
.end method

.method public getMaxAttachmentSize()I
    .locals 1

    iget v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    if-gtz v0, :cond_0

    const/high16 v0, 0x500000

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/android/mail/providers/Settings;->mHashCode:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/providers/Settings;->calculateHashCode()I

    move-result v0

    iput v0, p0, Lcom/android/mail/providers/Settings;->mHashCode:I

    :cond_0
    iget v0, p0, Lcom/android/mail/providers/Settings;->mHashCode:I

    return v0
.end method

.method public declared-synchronized serialize()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/mail/providers/Settings;->toJSON()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setAutoAdvanceSetting(I)V
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/Settings;->mTransientAutoAdvance:Ljava/lang/Integer;

    return-void
.end method

.method public declared-synchronized toJSON()Lorg/json/JSONObject;
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v2, "signature"

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    sget-object v4, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v4, v4, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "auto_advance"

    invoke-virtual {p0}, Lcom/android/mail/providers/Settings;->getAutoAdvanceSetting()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "message_text_size"

    iget v3, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "snap_headers"

    iget v3, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "reply_behavior"

    iget v3, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "hide_checkboxes"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "confirm_delete"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "confirm_archive"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "confirm_send"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "default_inbox"

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    sget-object v4, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v4, v4, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-static {v3, v4}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "default_inbox_name"

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    sget-object v4, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v4, v4, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "force_reply_from_default"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "max_attachment_size"

    iget v3, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "swipe"

    iget v3, p0, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "priority_inbox_arrows_enabled"

    iget-boolean v3, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v2, "setup_intent_uri"

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "conversation_view_mode"

    iget v3, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "veiled_address_pattern"

    iget-object v3, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v2, Lcom/android/mail/providers/Settings;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Could not serialize settings"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    sget-object v3, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v3, v3, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/mail/providers/Settings;->getAutoAdvanceSetting()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v3, v3, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    sget-object v3, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v3, v3, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    sget-object v1, Lcom/android/mail/providers/Settings;->sDefault:Lcom/android/mail/providers/Settings;

    iget-object v1, v1, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/android/mail/providers/Settings;->getNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
