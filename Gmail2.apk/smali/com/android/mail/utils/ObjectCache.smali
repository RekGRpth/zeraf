.class public Lcom/android/mail/utils/ObjectCache;
.super Ljava/lang/Object;
.source "ObjectCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/utils/ObjectCache$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/mail/utils/ObjectCache$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache$Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mDataStore:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mMaxSize:I


# direct methods
.method public constructor <init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V
    .locals 1
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/utils/ObjectCache$Callback",
            "<TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    iput-object p1, p0, Lcom/android/mail/utils/ObjectCache;->mCallback:Lcom/android/mail/utils/ObjectCache$Callback;

    iput p2, p0, Lcom/android/mail/utils/ObjectCache;->mMaxSize:I

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/utils/ObjectCache;->mCallback:Lcom/android/mail/utils/ObjectCache$Callback;

    invoke-interface {v1}, Lcom/android/mail/utils/ObjectCache$Callback;->newInstance()Ljava/lang/Object;

    move-result-object v0

    :cond_0
    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public release(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    iget v2, p0, Lcom/android/mail/utils/ObjectCache;->mMaxSize:I

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/mail/utils/ObjectCache;->mCallback:Lcom/android/mail/utils/ObjectCache$Callback;

    invoke-interface {v0, p1}, Lcom/android/mail/utils/ObjectCache$Callback;->onObjectReleased(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/mail/utils/ObjectCache;->mDataStore:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
