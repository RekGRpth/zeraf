.class public Lcom/android/mail/utils/MatrixCursorWithExtra;
.super Lcom/android/mail/utils/MatrixCursorWithCachedColumns;
.source "MatrixCursorWithExtra.java"


# instance fields
.field private final mExtras:Landroid/os/Bundle;


# direct methods
.method public constructor <init>([Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # [Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/utils/MatrixCursorWithCachedColumns;-><init>([Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/android/mail/utils/MatrixCursorWithExtra;->mExtras:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/utils/MatrixCursorWithExtra;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method
