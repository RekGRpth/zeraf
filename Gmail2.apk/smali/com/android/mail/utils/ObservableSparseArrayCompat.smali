.class public Lcom/android/mail/utils/ObservableSparseArrayCompat;
.super Lvedroid/support/v4/util/SparseArrayCompat;
.source "ObservableSparseArrayCompat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lvedroid/support/v4/util/SparseArrayCompat",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final mDataSetObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>()V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/android/mail/utils/ObservableSparseArrayCompat;->mDataSetObservable:Landroid/database/DataSetObservable;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>(I)V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/android/mail/utils/ObservableSparseArrayCompat;->mDataSetObservable:Landroid/database/DataSetObservable;

    return-void
.end method

.method private notifyChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/utils/ObservableSparseArrayCompat;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 0

    invoke-super {p0}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    invoke-direct {p0}, Lcom/android/mail/utils/ObservableSparseArrayCompat;->notifyChanged()V

    return-void
.end method

.method public delete(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Lvedroid/support/v4/util/SparseArrayCompat;->delete(I)V

    invoke-direct {p0}, Lcom/android/mail/utils/ObservableSparseArrayCompat;->notifyChanged()V

    return-void
.end method

.method public getDataSetObservable()Landroid/database/DataSetObservable;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/utils/ObservableSparseArrayCompat;->mDataSetObservable:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method public put(ILjava/lang/Object;)V
    .locals 0
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/mail/utils/ObservableSparseArrayCompat;->notifyChanged()V

    return-void
.end method

.method public remove(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Lvedroid/support/v4/util/SparseArrayCompat;->remove(I)V

    invoke-direct {p0}, Lcom/android/mail/utils/ObservableSparseArrayCompat;->notifyChanged()V

    return-void
.end method
