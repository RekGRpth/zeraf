.class public abstract Lcom/android/mail/utils/FragmentStatePagerAdapter2;
.super Lvedroid/support/v4/view/PagerAdapter;
.source "FragmentStatePagerAdapter2.java"


# instance fields
.field private mCurTransaction:Landroid/app/FragmentTransaction;

.field private mCurrentPrimaryItem:Landroid/app/Fragment;

.field private mEnableSavedStates:Z

.field private final mFragmentManager:Landroid/app/FragmentManager;

.field private mFragments:Lvedroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private mSavedState:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;Z)V
    .locals 2
    .param p1    # Landroid/app/FragmentManager;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    new-instance v0, Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v0}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    iput-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    iput-object p1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    iput-boolean p2, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mEnableSavedStates:Z

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    move-object v0, p3

    check-cast v0, Landroid/app/Fragment;

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    :cond_0
    iget-boolean v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mEnableSavedStates:Z

    if-eqz v1, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, p2, :cond_1

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v2, v0}, Landroid/app/FragmentManager;->saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, p2}, Lvedroid/support/v4/util/SparseArrayCompat;->delete(I)V

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    return-void
.end method

.method public getFragmentAt(I)Landroid/app/Fragment;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0, p1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    return-object v0
.end method

.method public abstract getItem(I)Landroid/app/Fragment;
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I

    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3, p2}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    :cond_1
    invoke-virtual {p0, p2}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->getItem(I)Landroid/app/Fragment;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mEnableSavedStates:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, p2, :cond_2

    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment$SavedState;

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Landroid/app/Fragment;->setInitialSavedState(Landroid/app/Fragment$SavedState;)V

    :cond_2
    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    if-eq v1, v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setItemVisible(Landroid/app/Fragment;Z)V

    :cond_3
    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3, p2, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v4

    invoke-virtual {v3, v4, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object v0, v1

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/app/Fragment;

    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 7

    new-instance v2, Lvedroid/support/v4/util/SparseArrayCompat;

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v6

    invoke-direct {v2, v6}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v6

    if-ge v1, v6, :cond_2

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v4

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->getItemPosition(Ljava/lang/Object;)I

    move-result v3

    const/4 v6, -0x2

    if-eq v3, v6, :cond_0

    if-ltz v3, :cond_1

    move v5, v3

    :goto_1
    invoke-virtual {v2, v5, v0}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v5, v4

    goto :goto_1

    :cond_2
    iput-object v2, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-super {p0}, Lvedroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 11
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Ljava/lang/ClassLoader;

    if-eqz p1, :cond_3

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v0, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v8, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    iget-boolean v8, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mEnableSavedStates:Z

    if-eqz v8, :cond_0

    const-string v8, "states"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    iget-object v8, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    array-length v8, v2

    if-ge v3, v8, :cond_0

    iget-object v9, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    aget-object v8, v2, v3

    check-cast v8, Landroid/app/Fragment$SavedState;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v8, "f"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iget-object v8, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v8, v0, v6}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v8, 0x0

    invoke-virtual {p0, v1, v8}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setItemVisible(Landroid/app/Fragment;Z)V

    iget-object v8, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, v5, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    const-string v8, "FragmentStatePagerAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad fragment at key "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 8

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mEnableSavedStates:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Landroid/app/Fragment$SavedState;

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mSavedState:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v6, "states"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v2}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v4

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragments:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v2}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    if-nez v5, :cond_1

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "f"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v6, v5, v3, v0}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v5
.end method

.method public setItemVisible(Landroid/app/Fragment;Z)V
    .locals 0
    .param p1    # Landroid/app/Fragment;
    .param p2    # Z

    invoke-static {p1, p2}, Lvedroid/support/v13/app/FragmentCompat;->setMenuVisibility(Landroid/app/Fragment;Z)V

    invoke-static {p1, p2}, Lvedroid/support/v13/app/FragmentCompat;->setUserVisibleHint(Landroid/app/Fragment;Z)V

    return-void
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    move-object v0, p3

    check-cast v0, Landroid/app/Fragment;

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setItemVisible(Landroid/app/Fragment;Z)V

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setItemVisible(Landroid/app/Fragment;Z)V

    :cond_1
    iput-object v0, p0, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->mCurrentPrimaryItem:Landroid/app/Fragment;

    :cond_2
    return-void
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;

    return-void
.end method
