.class public final Lcom/android/mail/utils/VeiledAddressMatcher;
.super Ljava/lang/Object;
.source "VeiledAddressMatcher.java"


# instance fields
.field private mMatcher:Ljava/util/regex/Pattern;

.field private final mObserver:Lcom/android/mail/providers/AccountObserver;

.field private mProfilePatternLastHash:I

.field protected mVeiledMatchingEnabled:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mMatcher:Ljava/util/regex/Pattern;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mVeiledMatchingEnabled:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mProfilePatternLastHash:I

    new-instance v0, Lcom/android/mail/utils/VeiledAddressMatcher$1;

    invoke-direct {v0, p0}, Lcom/android/mail/utils/VeiledAddressMatcher$1;-><init>(Lcom/android/mail/utils/VeiledAddressMatcher;)V

    iput-object v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mObserver:Lcom/android/mail/providers/AccountObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/utils/VeiledAddressMatcher;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/mail/utils/VeiledAddressMatcher;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mail/utils/VeiledAddressMatcher;->loadPattern(Ljava/lang/String;)V

    return-void
.end method

.method private final loadPattern(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mProfilePatternLastHash:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mProfilePatternLastHash:I

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mMatcher:Ljava/util/regex/Pattern;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mVeiledMatchingEnabled:Z

    :cond_0
    return-void
.end method

.method public static final newInstance(Landroid/content/res/Resources;)Lcom/android/mail/utils/VeiledAddressMatcher;
    .locals 2
    .param p0    # Landroid/content/res/Resources;

    new-instance v0, Lcom/android/mail/utils/VeiledAddressMatcher;

    invoke-direct {v0}, Lcom/android/mail/utils/VeiledAddressMatcher;-><init>()V

    const v1, 0x7f0f0005

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/mail/utils/VeiledAddressMatcher;->mVeiledMatchingEnabled:Z

    iget-boolean v1, v0, Lcom/android/mail/utils/VeiledAddressMatcher;->mVeiledMatchingEnabled:Z

    if-eqz v1, :cond_0

    const v1, 0x7f09012a

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/mail/utils/VeiledAddressMatcher;->loadPattern(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final initialize(Lcom/android/mail/ui/AccountController;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/AccountController;

    iget-object v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0, p1}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    return-void
.end method

.method public final isVeiledAddress(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mVeiledMatchingEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mMatcher:Ljava/util/regex/Pattern;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/utils/VeiledAddressMatcher;->mMatcher:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method
