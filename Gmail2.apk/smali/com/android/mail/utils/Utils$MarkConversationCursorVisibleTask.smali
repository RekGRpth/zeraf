.class Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;
.super Landroid/os/AsyncTask;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MarkConversationCursorVisibleTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private final mIsFirstSeen:Z

.field private final mVisible:Z


# direct methods
.method public constructor <init>(Landroid/database/Cursor;ZZ)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mCursor:Landroid/database/Cursor;

    iput-boolean p2, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mVisible:Z

    iput-boolean p3, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mIsFirstSeen:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1    # [Ljava/lang/Void;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_0

    :goto_0
    return-object v4

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-boolean v2, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mIsFirstSeen:Z

    if-eqz v2, :cond_1

    const-string v2, "enteredFolder"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    const-string v0, "setVisibility"

    const-string v2, "setVisibility"

    iget-boolean v3, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mVisible:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/android/mail/utils/Utils$MarkConversationCursorVisibleTask;->mCursor:Landroid/database/Cursor;

    const-string v3, "setVisibility"

    # invokes: Lcom/android/mail/utils/Utils;->executeConversationCursorCommand(Landroid/database/Cursor;Landroid/os/Bundle;Ljava/lang/String;)Z
    invoke-static {v2, v1, v3}, Lcom/android/mail/utils/Utils;->access$000(Landroid/database/Cursor;Landroid/os/Bundle;Ljava/lang/String;)Z

    goto :goto_0
.end method
