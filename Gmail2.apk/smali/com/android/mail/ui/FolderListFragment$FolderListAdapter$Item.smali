.class Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;
.super Ljava/lang/Object;
.source "FolderListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Item"
.end annotation


# instance fields
.field public final mFolder:Lcom/android/mail/providers/Folder;

.field public final mFolderType:I

.field public mPosition:I

.field public final mResource:I

.field public final mType:I

.field final synthetic this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;


# direct methods
.method private constructor <init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    iput p2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mResource:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mType:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolderType:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;ILcom/android/mail/ui/FolderListFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;
    .param p2    # I
    .param p3    # Lcom/android/mail/ui/FolderListFragment$1;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;-><init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;I)V

    return-void
.end method

.method private constructor <init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;Lcom/android/mail/providers/Folder;II)V
    .locals 1
    .param p2    # Lcom/android/mail/providers/Folder;
    .param p3    # I
    .param p4    # I

    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mResource:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mType:I

    iput p3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolderType:I

    iput p4, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mPosition:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;Lcom/android/mail/providers/Folder;IILcom/android/mail/ui/FolderListFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;
    .param p2    # Lcom/android/mail/providers/Folder;
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/android/mail/ui/FolderListFragment$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;-><init>(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;Lcom/android/mail/providers/Folder;II)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private final getFolderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/android/mail/ui/FolderItemView;

    :goto_0
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v3, v3, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;
    invoke-static {v3}, Lcom/android/mail/ui/FolderListFragment;->access$000(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/ui/ControllableActivity;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/android/mail/ui/FolderItemView;->bind(Lcom/android/mail/providers/Folder;Lcom/android/mail/ui/FolderItemView$DropHandler;)V

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v2, v2, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment;->access$500(Lcom/android/mail/ui/FolderListFragment;)Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolderType:I

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v3, v3, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I
    invoke-static {v3}, Lcom/android/mail/ui/FolderListFragment;->access$600(Lcom/android/mail/ui/FolderListFragment;)I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v3, v3, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/android/mail/ui/FolderListFragment;->access$700(Lcom/android/mail/ui/FolderListFragment;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v2, v2, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment;->access$500(Lcom/android/mail/ui/FolderListFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v2, v2, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    iget v2, v2, Lcom/android/mail/providers/Folder;->unreadCount:I

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v3, v3, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v3}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v3

    iget v3, v3, Lcom/android/mail/providers/Folder;->unreadCount:I

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    iget-object v2, v2, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v2

    iget v2, v2, Lcom/android/mail/providers/Folder;->unreadCount:I

    invoke-virtual {v0, v2}, Lcom/android/mail/ui/FolderItemView;->overrideUnreadCount(I)V

    :cond_1
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    const v3, 0x7f0800b2

    invoke-virtual {v0, v3}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mail/providers/Folder;->setFolderBlockColor(Lcom/android/mail/providers/Folder;Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolder:Lcom/android/mail/providers/Folder;

    const v2, 0x7f080032

    invoke-virtual {v0, v2}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-static {v3, v2}, Lcom/android/mail/providers/Folder;->setIcon(Lcom/android/mail/providers/Folder;Landroid/widget/ImageView;)V

    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    # getter for: Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->access$400(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040039

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderItemView;

    goto/16 :goto_0
.end method

.method private final getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-eqz p2, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    :goto_0
    iget v1, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mResource:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->this$1:Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    # getter for: Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v1}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;->access$400(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method private final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget v0, p0, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mType:I

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->getFolderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
