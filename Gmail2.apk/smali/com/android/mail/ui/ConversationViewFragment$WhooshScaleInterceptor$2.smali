.class Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;
.super Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->loadSingleMessageView(ZII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

.field final synthetic val$curve:Landroid/view/animation/Interpolator;

.field final synthetic val$fadeIn:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;Ljava/lang/String;Landroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iput-object p3, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->val$curve:Landroid/view/animation/Interpolator;

    iput-object p4, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->val$fadeIn:Landroid/animation/Animator$AnimatorListener;

    iget-object v0, p1, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0, v0, p2}, Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public go()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageHeaderView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setTranslationY(F)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x3e99999a

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->val$curve:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;->val$fadeIn:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method
