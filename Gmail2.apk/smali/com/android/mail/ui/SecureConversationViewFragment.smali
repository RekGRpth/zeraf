.class public Lcom/android/mail/ui/SecureConversationViewFragment;
.super Lcom/android/mail/ui/AbstractConversationViewFragment;
.source "SecureConversationViewFragment.java"

# interfaces
.implements Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

.field private mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

.field private mMessageFooterView:Lcom/android/mail/browse/MessageFooterView;

.field private mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/SecureConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;-><init>()V

    new-instance v0, Lcom/android/mail/ui/SecureConversationViewFragment$1;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/SecureConversationViewFragment$1;-><init>(Lcom/android/mail/ui/SecureConversationViewFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    return-void
.end method

.method public static newInstance(Landroid/os/Bundle;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/SecureConversationViewFragment;
    .locals 3
    .param p0    # Landroid/os/Bundle;
    .param p1    # Lcom/android/mail/providers/Conversation;

    new-instance v1, Lcom/android/mail/ui/SecureConversationViewFragment;

    invoke-direct {v1}, Lcom/android/mail/ui/SecureConversationViewFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string v2, "conversation"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/SecureConversationViewFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private renderMessageBodies(Lcom/android/mail/browse/MessageCursor;Z)V
    .locals 13
    .param p1    # Lcom/android/mail/browse/MessageCursor;
    .param p2    # Z

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xc

    invoke-virtual {p1, v1}, Lcom/android/mail/browse/MessageCursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xd

    invoke-virtual {p1, v1}, Lcom/android/mail/browse/MessageCursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    new-instance v1, Landroid/text/SpannedString;

    invoke-direct {v1, v10}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v1}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v10

    :cond_0
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v1, v1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mBaseUri:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/mail/browse/ConversationViewAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/mail/browse/ConversationViewAdapter;-><init>(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/browse/ConversationAccountController;Landroid/app/LoaderManager;Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;Lcom/android/mail/ContactInfoSource;Lcom/android/mail/browse/ConversationViewHeader$ConversationViewHeaderCallbacks;Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;Ljava/util/Map;Lcom/android/mail/FormattedDateBuilder;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v3, v3, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mail/browse/ConversationViewAdapter;->newMessageHeaderItem(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    move-result-object v12

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

    iget-object v3, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mAddressCache:Ljava/util/Map;

    invoke-virtual {v1, v2, p0, v3}, Lcom/android/mail/browse/MessageHeaderView;->initialize(Lcom/android/mail/FormattedDateBuilder;Lcom/android/mail/browse/ConversationAccountController;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/MessageHeaderView;->setExpandMode(I)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/android/mail/browse/MessageHeaderView;->bind(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;Z)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v1, v1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->hasAttachments:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageFooterView:Lcom/android/mail/browse/MessageFooterView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/MessageFooterView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageFooterView:Lcom/android/mail/browse/MessageFooterView;

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/mail/browse/MessageFooterView;->initialize(Landroid/app/LoaderManager;Landroid/app/FragmentManager;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageFooterView:Lcom/android/mail/browse/MessageFooterView;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/android/mail/browse/MessageFooterView;->bind(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getWebView()Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method protected markUnread()V
    .locals 8

    invoke-super {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->markUnread()V

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    if-nez v2, :cond_2

    :cond_0
    sget-object v4, Lcom/android/mail/ui/SecureConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "ignoring markUnread for conv=%s"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v2, v2, Lcom/android/mail/providers/Conversation;->id:J

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    return-void

    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v2, v2, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v4, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v4}, Lcom/android/mail/ui/ConversationViewState;->getConversationInfo()[B

    move-result-object v4

    invoke-interface {v2, v3, v1, v4}, Lcom/android/mail/ui/ConversationUpdater;->markConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V

    goto :goto_1
.end method

.method public onAccountChanged(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Account;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Account;

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

    invoke-virtual {v1, p0, p0}, Lcom/android/mail/browse/ConversationViewHeader;->setCallbacks(Lcom/android/mail/browse/ConversationViewHeader$ConversationViewHeaderCallbacks;Lcom/android/mail/browse/ConversationAccountController;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

    invoke-virtual {v1, v4}, Lcom/android/mail/browse/ConversationViewHeader;->setFoldersVisible(Z)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getSubjectDisplayChanger()Lcom/android/mail/ui/SubjectDisplayChanger;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v1, v1, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/SubjectDisplayChanger;->setSubject(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v2, v2, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/ConversationViewHeader;->setSubject(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getContactInfoSource()Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/MessageHeaderView;->setContactInfoSource(Lcom/android/mail/ContactInfoSource;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v1, p0}, Lcom/android/mail/browse/MessageHeaderView;->setCallbacks(Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;)V

    iget-object v1, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v1, v4}, Lcom/android/mail/browse/MessageHeaderView;->setExpandable(Z)V

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mail/ui/AccountController;->getVeiledAddressMatcher()Lcom/android/mail/utils/VeiledAddressMatcher;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/mail/browse/MessageHeaderView;->setVeiledMatcher(Lcom/android/mail/utils/VeiledAddressMatcher;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->getMessageLoaderCallbacks()Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->showLoadingStatus()V

    return-void
.end method

.method public onConversationUpdated(Lcom/android/mail/providers/Conversation;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationViewHeader;->onConversationUpdated(Lcom/android/mail/providers/Conversation;)V

    iget-object v1, p1, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationViewHeader;->setSubject(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onConversationViewHeaderHeightChange(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v2, 0x7f04005d

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0800dc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mScrollView:Landroid/widget/ScrollView;

    const v2, 0x7f0800de

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mail/browse/ConversationViewHeader;

    iput-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mConversationHeaderView:Lcom/android/mail/browse/ConversationViewHeader;

    const v2, 0x7f0800df

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mail/browse/MessageHeaderView;

    iput-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageHeaderView:Lcom/android/mail/browse/MessageHeaderView;

    const v2, 0x7f0800e0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mail/browse/MessageFooterView;

    iput-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mMessageFooterView:Lcom/android/mail/browse/MessageFooterView;

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/SecureConversationViewFragment;->instantiateProgressIndicators(Landroid/view/View;)V

    const v2, 0x7f0800a4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->setFocusable(Z)V

    iget-object v2, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    return-object v0
.end method

.method protected onMessageCursorLoadFinished(Landroid/content/Loader;Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor;)V
    .locals 3
    .param p2    # Lcom/android/mail/browse/MessageCursor;
    .param p3    # Lcom/android/mail/browse/MessageCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/android/mail/browse/MessageCursor;",
            "Lcom/android/mail/browse/MessageCursor;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/android/mail/browse/MessageCursor;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    sget-object v0, Lcom/android/mail/ui/SecureConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "CONV RENDER: existing cursor is null, rendering from scratch"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mEnableContentReadySignal:Z

    invoke-direct {p0, p2, v0}, Lcom/android/mail/ui/SecureConversationViewFragment;->renderMessageBodies(Lcom/android/mail/browse/MessageCursor;Z)V

    goto :goto_0
.end method

.method public onUserVisibleHintChanged()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->isUserVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    invoke-virtual {p0}, Lcom/android/mail/ui/SecureConversationViewFragment;->onConversationSeen()V

    goto :goto_0
.end method

.method public setMessageDetailsExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;ZI)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # Z
    .param p3    # I

    return-void
.end method

.method public setMessageExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # I

    return-void
.end method

.method public setMessageSpacerHeight(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # I

    return-void
.end method

.method public showExternalResources(Lcom/android/mail/providers/Message;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Message;

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    return-void
.end method

.method public showExternalResources(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/ui/SecureConversationViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    return-void
.end method
