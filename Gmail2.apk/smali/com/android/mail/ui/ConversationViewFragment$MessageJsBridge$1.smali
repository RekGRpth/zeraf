.class Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;
.super Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->onContentReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    iget-object v0, p1, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0, v0, p2}, Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public go()V
    .locals 8

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$2600(Lcom/android/mail/ui/ConversationViewFragment;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/ui/ConversationViewFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IN MESSAGEVIEW.onContentReady, f=%s vis=%s t=%sms"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-virtual {v4}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    iget-object v6, v6, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J
    invoke-static {v6}, Lcom/android/mail/ui/ConversationViewFragment;->access$2600(Lcom/android/mail/ui/ConversationViewFragment;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    return-void
.end method
