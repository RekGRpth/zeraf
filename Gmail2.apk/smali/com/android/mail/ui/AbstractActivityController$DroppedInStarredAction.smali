.class Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;
.super Ljava/lang/Object;
.source "AbstractActivityController.java"

# interfaces
.implements Lcom/android/mail/ui/DestructiveAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/AbstractActivityController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DroppedInStarredAction"
.end annotation


# instance fields
.field private mConversations:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialFolder:Lcom/android/mail/providers/Folder;

.field private mStarred:Lcom/android/mail/providers/Folder;

.field final synthetic this$0:Lcom/android/mail/ui/AbstractActivityController;


# direct methods
.method public constructor <init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p3    # Lcom/android/mail/providers/Folder;
    .param p4    # Lcom/android/mail/providers/Folder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/providers/Folder;",
            "Lcom/android/mail/providers/Folder;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mConversations:Ljava/util/Collection;

    iput-object p3, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mInitialFolder:Lcom/android/mail/providers/Folder;

    iput-object p4, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mStarred:Lcom/android/mail/providers/Folder;

    return-void
.end method


# virtual methods
.method public performAction()V
    .locals 13

    const/4 v12, 0x1

    new-instance v10, Lcom/android/mail/ui/ToastBarOperation;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mConversations:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    const v4, 0x7f08011c

    const/4 v11, 0x0

    invoke-direct {v10, v0, v4, v11, v12}, Lcom/android/mail/ui/ToastBarOperation;-><init>(IIIZ)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v0, v10}, Lcom/android/mail/ui/AbstractActivityController;->onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mConversations:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Conversation;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mStarred:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mInitialFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/android/mail/providers/Conversation;->getRawFolders()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/providers/Folder;->hashMapForFolders(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v9

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mStarred:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mStarred:Lcom/android/mail/providers/Folder;

    invoke-virtual {v9, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->mInitialFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/mail/providers/UIProvider$ConversationColumns;->STARRED:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/browse/ConversationCursor;->getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;Landroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v4, v4, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v4, v8}, Lcom/android/mail/browse/ConversationCursor;->updateBulkValues(Landroid/content/Context;Ljava/util/Collection;)I

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;
    invoke-static {v0}, Lcom/android/mail/ui/AbstractActivityController;->access$700(Lcom/android/mail/ui/AbstractActivityController;)Lcom/android/mail/ui/ConversationSelectionSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    return-void
.end method
