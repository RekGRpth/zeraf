.class public Lcom/android/mail/ui/DividedImageCanvas$Dimensions;
.super Ljava/lang/Object;
.source "DividedImageCanvas.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/DividedImageCanvas;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Dimensions"
.end annotation


# instance fields
.field public final height:I

.field public final scale:F

.field final synthetic this$0:Lcom/android/mail/ui/DividedImageCanvas;

.field public final width:I


# direct methods
.method public constructor <init>(Lcom/android/mail/ui/DividedImageCanvas;IIF)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .param p4    # F

    iput-object p1, p0, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->this$0:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->width:I

    iput p3, p0, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->height:I

    iput p4, p0, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->scale:F

    return-void
.end method
