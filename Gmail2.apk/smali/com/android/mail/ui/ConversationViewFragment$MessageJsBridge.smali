.class Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;
.super Ljava/lang/Object;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/ConversationViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageJsBridge"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mail/ui/ConversationViewFragment;


# direct methods
.method private constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p2    # Lcom/android/mail/ui/ConversationViewFragment$1;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    return-void
.end method


# virtual methods
.method public getScrollYPercent()F
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public onContentReady()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationViewFragment;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;

    const-string v2, "onMessageContentReady"

    invoke-direct {v1, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge$1;-><init>(Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onWebContentGeometryChange([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    return-void
.end method
