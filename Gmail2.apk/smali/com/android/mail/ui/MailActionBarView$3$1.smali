.class Lcom/android/mail/ui/MailActionBarView$3$1;
.super Ljava/lang/Object;
.source "MailActionBarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/MailActionBarView$3;->onLayoutChange(Landroid/view/View;IIIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/MailActionBarView$3;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/MailActionBarView$3;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/MailActionBarView$3$1;->this$1:Lcom/android/mail/ui/MailActionBarView$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView$3$1;->this$1:Lcom/android/mail/ui/MailActionBarView$3;

    iget-object v0, v0, Lcom/android/mail/ui/MailActionBarView$3;->this$0:Lcom/android/mail/ui/MailActionBarView;

    iget-object v0, v0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    # invokes: Lcom/android/mail/ui/MailActionBarView;->actionBarReportsMultipleLineTitle(Landroid/app/ActionBar;)Z
    invoke-static {v0}, Lcom/android/mail/ui/MailActionBarView;->access$300(Landroid/app/ActionBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView$3$1;->this$1:Lcom/android/mail/ui/MailActionBarView$3;

    iget-object v0, v0, Lcom/android/mail/ui/MailActionBarView$3;->this$0:Lcom/android/mail/ui/MailActionBarView;

    const/16 v1, 0x28

    # invokes: Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V
    invoke-static {v0, v1}, Lcom/android/mail/ui/MailActionBarView;->access$400(Lcom/android/mail/ui/MailActionBarView;I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView$3$1;->this$1:Lcom/android/mail/ui/MailActionBarView$3;

    iget-object v0, v0, Lcom/android/mail/ui/MailActionBarView$3;->this$0:Lcom/android/mail/ui/MailActionBarView;

    iget-object v0, v0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView$3$1;->this$1:Lcom/android/mail/ui/MailActionBarView$3;

    iget-object v1, v1, Lcom/android/mail/ui/MailActionBarView$3;->this$0:Lcom/android/mail/ui/MailActionBarView;

    iget-object v1, v1, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
