.class Lcom/android/mail/ui/ConversationViewFragment$7$1;
.super Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment$7;->onHeightChange(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

.field final synthetic val$h:I


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment$7;Ljava/lang/String;I)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iput p3, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->val$h:I

    iget-object v0, p1, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0, v0, p2}, Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public go()V
    .locals 9

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/ui/ConversationViewFragment;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "message view content height=%d initialScrollX/Y=%s/%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->val$h:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v8, v8, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F
    invoke-static {v8}, Lcom/android/mail/ui/ConversationViewFragment;->access$1000(Lcom/android/mail/ui/ConversationViewFragment;)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v8, v8, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F
    invoke-static {v8}, Lcom/android/mail/ui/ConversationViewFragment;->access$1100(Lcom/android/mail/ui/ConversationViewFragment;)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageView;->computeHorizontalScrollRange()I

    move-result v4

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v5, v5, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mail/browse/MessageView;->computeHorizontalScrollExtent()I

    move-result v5

    sub-int v0, v4, v5

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageView;->computeVerticalScrollRange()I

    move-result v4

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v5, v5, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mail/browse/MessageView;->computeVerticalScrollExtent()I

    move-result v5

    sub-int v1, v4, v5

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v5, v5, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$1000(Lcom/android/mail/ui/ConversationViewFragment;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v4, v1

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v5, v5, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$1100(Lcom/android/mail/ui/ConversationViewFragment;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment$7$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$7;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/android/mail/browse/MessageView;->scrollTo(II)V

    return-void
.end method
