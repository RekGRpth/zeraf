.class public Lcom/android/mail/ui/ConversationPositionTracker;
.super Ljava/lang/Object;
.source "ConversationPositionTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;
    }
.end annotation


# static fields
.field protected static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

.field private mConversation:Lcom/android/mail/providers/Conversation;

.field private mCursorDirty:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/ConversationPositionTracker;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCursorDirty:Z

    iput-object p1, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    return-void
.end method

.method private calculatePosition()I
    .locals 10

    const/4 v9, 0x0

    const/4 v1, -0x1

    const/4 v2, -0x1

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    invoke-interface {v5}, Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    iget-boolean v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCursorDirty:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    iget v1, v5, Lcom/android/mail/providers/Conversation;->position:I

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v5, :cond_0

    iput-boolean v9, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCursorDirty:Z

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->getCount()I

    move-result v3

    invoke-static {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v5, v5, Lcom/android/mail/providers/Conversation;->id:J

    invoke-virtual {v0, v5, v6}, Lcom/android/mail/browse/ConversationCursor;->getConversationPosition(J)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    iput v1, v5, Lcom/android/mail/providers/Conversation;->position:I

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0, v5}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_2
    if-lt v1, v3, :cond_4

    add-int/lit8 v4, v3, -0x1

    :goto_1
    invoke-static {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-ltz v4, :cond_3

    sget-object v5, Lcom/android/mail/ui/ConversationPositionTracker;->LOG_TAG:Ljava/lang/String;

    const-string v6, "ConversationPositionTracker: Could not find conversation %s in the cursor. Moving to position %d "

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v8}, Lcom/android/mail/providers/Conversation;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0, v4}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    new-instance v5, Lcom/android/mail/providers/Conversation;

    invoke-direct {v5, v0}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v5, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    iput v4, v5, Lcom/android/mail/providers/Conversation;->position:I

    :cond_3
    move v1, v4

    goto :goto_0

    :cond_4
    move v4, v1

    goto :goto_1
.end method

.method private conversationAtPosition(I)Lcom/android/mail/providers/Conversation;
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    invoke-interface {v2}, Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput p1, v0, Lcom/android/mail/providers/Conversation;->position:I

    return-object v0
.end method

.method private getCount()I
    .locals 2

    iget-object v1, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    invoke-interface {v1}, Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->getCount()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getNewer(Ljava/util/Collection;)Lcom/android/mail/providers/Conversation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)",
            "Lcom/android/mail/providers/Conversation;"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->calculatePosition()I

    move-result v1

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded()Z

    move-result v3

    if-eqz v3, :cond_0

    if-gez v1, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_3

    invoke-direct {p0, v1}, Lcom/android/mail/ui/ConversationPositionTracker;->conversationAtPosition(I)Lcom/android/mail/providers/Conversation;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/mail/providers/Conversation;->contains(Ljava/util/Collection;Lcom/android/mail/providers/Conversation;)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method private getOlder(Ljava/util/Collection;)Lcom/android/mail/providers/Conversation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)",
            "Lcom/android/mail/providers/Conversation;"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->calculatePosition()I

    move-result v1

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded()Z

    move-result v3

    if-eqz v3, :cond_0

    if-gez v1, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    :goto_1
    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    invoke-direct {p0, v1}, Lcom/android/mail/ui/ConversationPositionTracker;->conversationAtPosition(I)Lcom/android/mail/providers/Conversation;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/mail/providers/Conversation;->contains(Ljava/util/Collection;Lcom/android/mail/providers/Conversation;)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method private isDataLoaded()Z
    .locals 2

    iget-object v1, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCallbacks:Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;

    invoke-interface {v1}, Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->isDataLoaded(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v1

    return v1
.end method

.method private static isDataLoaded(Lcom/android/mail/browse/ConversationCursor;)Z
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getNextConversation(ILjava/util/Collection;)Lcom/android/mail/providers/Conversation;
    .locals 10
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)",
            "Lcom/android/mail/providers/Conversation;"
        }
    .end annotation

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    if-ne p1, v9, :cond_0

    move v0, v3

    :goto_0
    if-ne p1, v3, :cond_1

    move v1, v3

    :goto_1
    if-eqz v0, :cond_2

    invoke-direct {p0, p2}, Lcom/android/mail/ui/ConversationPositionTracker;->getNewer(Ljava/util/Collection;)Lcom/android/mail/providers/Conversation;

    move-result-object v2

    :goto_2
    sget-object v5, Lcom/android/mail/ui/ConversationPositionTracker;->LOG_TAG:Ljava/lang/String;

    const-string v6, "ConversationPositionTracker.getNextConversation: getNewer = %b, getOlder = %b, Next conversation is %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v3

    aput-object v2, v7, v9

    invoke-static {v5, v6, v7}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    return-object v2

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    move v1, v4

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-direct {p0, p2}, Lcom/android/mail/ui/ConversationPositionTracker;->getOlder(Ljava/util/Collection;)Lcom/android/mail/providers/Conversation;

    move-result-object v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public initialize(Lcom/android/mail/providers/Conversation;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mConversation:Lcom/android/mail/providers/Conversation;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCursorDirty:Z

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationPositionTracker;->calculatePosition()I

    return-void
.end method

.method public onCursorUpdated()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/ConversationPositionTracker;->mCursorDirty:Z

    return-void
.end method
