.class public interface abstract Lcom/android/mail/ui/AccountController;
.super Ljava/lang/Object;
.source "AccountController.java"


# virtual methods
.method public abstract getAccount()Lcom/android/mail/providers/Account;
.end method

.method public abstract getVeiledAddressMatcher()Lcom/android/mail/utils/VeiledAddressMatcher;
.end method

.method public abstract registerAccountObserver(Landroid/database/DataSetObserver;)V
.end method

.method public abstract unregisterAccountObserver(Landroid/database/DataSetObserver;)V
.end method
