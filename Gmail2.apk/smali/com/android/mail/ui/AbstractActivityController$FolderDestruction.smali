.class Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;
.super Ljava/lang/Object;
.source "AbstractActivityController.java"

# interfaces
.implements Lcom/android/mail/ui/DestructiveAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/AbstractActivityController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FolderDestruction"
.end annotation


# instance fields
.field private mAction:I

.field private mCompleted:Z

.field private final mFolderOps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/ui/FolderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsDestructive:Z

.field private mIsSelectedSet:Z

.field private mShowUndo:Z

.field private final mTarget:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/mail/ui/AbstractActivityController;


# direct methods
.method private constructor <init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Ljava/util/Collection;ZZZI)V
    .locals 1
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/ui/FolderOperation;",
            ">;ZZZI)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mFolderOps:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mTarget:Ljava/util/Collection;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mFolderOps:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-boolean p4, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsDestructive:Z

    iput-boolean p5, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsSelectedSet:Z

    iput-boolean p6, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mShowUndo:Z

    iput p7, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mAction:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Ljava/util/Collection;ZZZILcom/android/mail/ui/AbstractActivityController$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/AbstractActivityController;
    .param p2    # Ljava/util/Collection;
    .param p3    # Ljava/util/Collection;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # Lcom/android/mail/ui/AbstractActivityController$1;

    invoke-direct/range {p0 .. p7}, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Ljava/util/Collection;ZZZI)V

    return-void
.end method

.method private declared-synchronized isPerformed()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mCompleted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mCompleted:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public performAction()V
    .locals 13

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->isPerformed()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsDestructive:Z

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mShowUndo:Z

    if-eqz v9, :cond_2

    new-instance v8, Lcom/android/mail/ui/ToastBarOperation;

    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mTarget:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->size()I

    move-result v9

    iget v10, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mAction:I

    const/4 v11, 0x0

    iget-boolean v12, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsSelectedSet:Z

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/android/mail/ui/ToastBarOperation;-><init>(IIIZ)V

    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v9, v8}, Lcom/android/mail/ui/AbstractActivityController;->onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V

    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mTarget:Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/mail/providers/Conversation;

    invoke-virtual {v6}, Lcom/android/mail/providers/Conversation;->getRawFolders()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/android/mail/providers/Folder;->hashMapForFolders(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsDestructive:Z

    if-eqz v9, :cond_3

    const/4 v9, 0x1

    iput-boolean v9, v6, Lcom/android/mail/providers/Conversation;->localDeleteOnUpdate:Z

    :cond_3
    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mFolderOps:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/mail/ui/FolderOperation;

    iget-object v9, v4, Lcom/android/mail/ui/FolderOperation;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v9, v9, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v9, v4, Lcom/android/mail/ui/FolderOperation;->mAdd:Z

    if-eqz v9, :cond_4

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v9, v4, Lcom/android/mail/ui/FolderOperation;->mAdd:Z

    if-eqz v9, :cond_5

    iget-object v9, v4, Lcom/android/mail/ui/FolderOperation;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v9, v9, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v10, v4, Lcom/android/mail/ui/FolderOperation;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v7, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_3

    :cond_5
    iget-object v9, v4, Lcom/android/mail/ui/FolderOperation;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v9, v9, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v9, v9, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-virtual {v9, v6, v1, v0, v10}, Lcom/android/mail/browse/ConversationCursor;->getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v9, v9, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v9, v9, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v10, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v10, v10, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10, v5}, Lcom/android/mail/browse/ConversationCursor;->updateBulkValues(Landroid/content/Context;Ljava/util/Collection;)I

    :cond_8
    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v9}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    iget-boolean v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->mIsSelectedSet:Z

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;
    invoke-static {v9}, Lcom/android/mail/ui/AbstractActivityController;->access$700(Lcom/android/mail/ui/AbstractActivityController;)Lcom/android/mail/ui/ConversationSelectionSet;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    goto/16 :goto_0
.end method
