.class public final Lcom/android/mail/ui/TwoPaneController;
.super Lcom/android/mail/ui/AbstractActivityController;
.source "TwoPaneController.java"


# instance fields
.field private mConversationToShow:Lcom/android/mail/providers/Conversation;

.field private mLayout:Lcom/android/mail/ui/TwoPaneLayout;


# direct methods
.method public constructor <init>(Lcom/android/mail/ui/MailActivity;Lcom/android/mail/ui/ViewMode;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/MailActivity;
    .param p2    # Lcom/android/mail/ui/ViewMode;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;-><init>(Lcom/android/mail/ui/MailActivity;Lcom/android/mail/ui/ViewMode;)V

    return-void
.end method

.method private createFolderListFragment(Lcom/android/mail/providers/Folder;Landroid/net/Uri;)V
    .locals 4
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/TwoPaneController;->setHierarchyFolder(Lcom/android/mail/providers/Folder;)V

    const/4 v2, 0x1

    invoke-static {p1, p2, v2}, Lcom/android/mail/ui/FolderListFragment;->newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;Z)Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/utils/Utils;->useFolderListFragmentTransition(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    :cond_0
    const v2, 0x7f0800b4

    const-string v3, "tag-folder-list"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->resetActionBarIcon()V

    :cond_1
    return-void
.end method

.method private final enableOrDisableCab()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v0}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->disableCabMode()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->enableCabMode()V

    goto :goto_0
.end method

.method private getUndoBarWidth(ILcom/android/mail/ui/ToastBarOperation;)I
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/mail/ui/ToastBarOperation;

    const/4 v1, -0x1

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    :cond_0
    return v1

    :pswitch_1
    const v0, 0x7f0c000f

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0c000c

    goto :goto_0

    :pswitch_3
    invoke-virtual {p2}, Lcom/android/mail/ui/ToastBarOperation;->isBatchUndo()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v2}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v2

    if-nez v2, :cond_1

    const v0, 0x7f0c000e

    goto :goto_0

    :cond_1
    const v0, 0x7f0c000d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private goUpFolderHierarchy(Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    iget-object v1, v0, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    iget-object v2, v0, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->childFoldersListUri:Landroid/net/Uri;

    invoke-direct {p0, v1, v2}, Lcom/android/mail/ui/TwoPaneController;->createFolderListFragment(Lcom/android/mail/providers/Folder;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/mail/ui/TwoPaneController;->onFolderSelected(Lcom/android/mail/providers/Folder;)V

    goto :goto_0
.end method

.method private initializeConversationListFragment(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v0, "android.intent.action.SEARCH"

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->shouldEnterSearchConvMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsConversationMode()Z

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->renderConversationList()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsListMode()Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z

    goto :goto_0
.end method

.method private renderConversationList()V
    .locals 4

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const/16 v2, 0x1003

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v2}, Lcom/android/mail/ui/ConversationListFragment;->newInstance(Lcom/android/mail/ConversationListContext;)Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    const v2, 0x7f0800f1

    const-string v3, "tag-conversation-list"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method private renderFolderList()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->folderListUri:Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/TwoPaneController;->createFolderListFragment(Lcom/android/mail/providers/Folder;Landroid/net/Uri;)V

    goto :goto_0
.end method


# virtual methods
.method public doesActionChangeConversationListVisibility(I)Z
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080008 -> :sswitch_0
        0x7f080036 -> :sswitch_0
        0x7f080115 -> :sswitch_0
        0x7f080116 -> :sswitch_0
        0x7f080117 -> :sswitch_0
        0x7f080126 -> :sswitch_0
    .end sparse-switch
.end method

.method public exitSearchMode()V
    .locals 2

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    :cond_1
    return-void
.end method

.method public handleBackPress()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/ActionableToastBar;->hide(Z)V

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/TwoPaneController;->popView(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public handleUpPress()Z
    .locals 3

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->handleBackPress()Z

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v1}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v1}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->handleBackPress()Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0

    :cond_5
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/TwoPaneController;->popView(Z)V

    goto :goto_0
.end method

.method protected hideOrRepositionToastBar(Z)V
    .locals 5
    .param p1    # Z

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    new-instance v2, Lcom/android/mail/ui/TwoPaneController$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/mail/ui/TwoPaneController$1;-><init>(Lcom/android/mail/ui/TwoPaneController;IZ)V

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/mail/ui/TwoPaneLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected hideWaitForInitialization()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-super {p0}, Lcom/android/mail/ui/AbstractActivityController;->hideWaitForInitialization()V

    goto :goto_0
.end method

.method protected isConversationListVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v0}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccountChanged(Lcom/android/mail/providers/Account;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onAccountChanged(Lcom/android/mail/providers/Account;)V

    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->renderFolderList()V

    return-void
.end method

.method public onConversationListVisibilityChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onConversationListVisibilityChanged(Z)V

    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->enableOrDisableCab()V

    return-void
.end method

.method public onConversationVisibilityChanged(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onConversationVisibilityChanged(Z)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0, v4}, Lcom/android/mail/browse/ConversationPagerController;->hide(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mConversationToShow:Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mConversationToShow:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/mail/browse/ConversationPagerController;->show(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Conversation;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mConversationToShow:Lcom/android/mail/providers/Conversation;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)Z
    .locals 4
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v2, 0x7f040068

    invoke-interface {v1, v2}, Lcom/android/mail/ui/ControllableActivity;->setContentView(I)V

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v2, 0x7f0800f0

    invoke-interface {v1, v2}, Lcom/android/mail/ui/ControllableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/TwoPaneLayout;

    iput-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/mail/ui/TwoPaneController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "mLayout is null!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    const-string v2, "android.intent.action.SEARCH"

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, p0, v2}, Lcom/android/mail/ui/TwoPaneLayout;->setController(Lcom/android/mail/ui/AbstractActivityController;Z)V

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/ViewMode;->addListener(Lcom/android/mail/ui/ViewMode$ModeChangeListener;)V

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onCreate(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public onError(Lcom/android/mail/providers/Folder;Z)V
    .locals 4
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Z

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v2}, Lcom/android/mail/ui/ActionableToastBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/TwoPaneController;->showErrorToast(Lcom/android/mail/providers/Folder;Z)V

    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v2}, Lcom/android/mail/ui/TwoPaneLayout;->computeConversationListWidth()I

    move-result v2

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/16 v2, 0x55

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v2, v1}, Lcom/android/mail/ui/ActionableToastBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x53

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v2}, Lcom/android/mail/ui/TwoPaneLayout;->computeConversationListWidth()I

    move-result v2

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v2, v1}, Lcom/android/mail/ui/ActionableToastBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onFolderChanged(Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Folder;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->exitCabMode()V

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getFolderListFragment()Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->renderFolderList()V

    :cond_0
    return-void
.end method

.method public onFolderSelected(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-boolean v0, p1, Lcom/android/mail/providers/Folder;->hasChildren:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getHierarchyFolder()Lcom/android/mail/providers/Folder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->childFoldersListUri:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/TwoPaneController;->createFolderListFragment(Lcom/android/mail/providers/Folder;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onFolderSelected(Lcom/android/mail/providers/Folder;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/mail/ui/TwoPaneController;->setHierarchyFolder(Lcom/android/mail/providers/Folder;)V

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onFolderSelected(Lcom/android/mail/providers/Folder;)V

    goto :goto_0
.end method

.method public onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V
    .locals 10
    .param p1    # Lcom/android/mail/ui/ToastBarOperation;

    const v5, 0x7f0900ae

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v8

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/TwoPaneController;->repositionToastBar(Lcom/android/mail/ui/ToastBarOperation;)V

    packed-switch v9, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v8}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/TwoPaneController;->getUndoClickedListener(Lcom/android/mail/ui/AnimatedAdapter;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mail/ui/TwoPaneController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v3, v6}, Lcom/android/mail/ui/ToastBarOperation;->getDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v6, v4

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/mail/ui/ActionableToastBar;->show(Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;ILjava/lang/CharSequence;ZIZLcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :pswitch_2
    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v8}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/TwoPaneController;->getUndoClickedListener(Lcom/android/mail/ui/AnimatedAdapter;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mail/ui/TwoPaneController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v3, v6}, Lcom/android/mail/ui/ToastBarOperation;->getDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v6, v4

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/mail/ui/ActionableToastBar;->show(Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;ILjava/lang/CharSequence;ZIZLcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onViewModeChanged(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onViewModeChanged(I)V

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->hideWaitForInitialization()V

    :cond_0
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->enableOrDisableCab()V

    :cond_2
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v0}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/TwoPaneController;->informCursorVisiblity(Z)V

    :cond_0
    return-void
.end method

.method protected popView(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z

    goto :goto_0

    :cond_2
    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsListMode()Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getFolderListFragment()Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderListFragment;->showingHierarchy()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getHierarchyFolder()Lcom/android/mail/providers/Folder;

    move-result-object v1

    iget-object v3, v1, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    if-eqz v3, :cond_4

    invoke-direct {p0, v1}, Lcom/android/mail/ui/TwoPaneController;->goUpFolderHierarchy(Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/TwoPaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->folderListUri:Landroid/net/Uri;

    invoke-direct {p0, v3, v4}, Lcom/android/mail/ui/TwoPaneController;->createFolderListFragment(Lcom/android/mail/providers/Folder;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->loadAccountInbox()V

    goto :goto_0

    :cond_5
    if-nez p1, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0
.end method

.method public repositionToastBar(Lcom/android/mail/ui/ToastBarOperation;)V
    .locals 7
    .param p1    # Lcom/android/mail/ui/ToastBarOperation;

    const/16 v6, 0x55

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3}, Lcom/android/mail/ui/ActionableToastBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p0, v0, p1}, Lcom/android/mail/ui/TwoPaneController;->getUndoBarWidth(ILcom/android/mail/ui/ToastBarOperation;)I

    move-result v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int v3, v2, v3

    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v6, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v1}, Lcom/android/mail/ui/ActionableToastBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v5}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/android/mail/ui/ToastBarOperation;->isBatchUndo()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mLayout:Lcom/android/mail/ui/TwoPaneLayout;

    invoke-virtual {v3}, Lcom/android/mail/ui/TwoPaneLayout;->isConversationListCollapsed()Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x53

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int v3, v2, v3

    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v1}, Lcom/android/mail/ui/ActionableToastBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v5}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    goto :goto_0

    :cond_0
    iput v6, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int v3, v2, v3

    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v1}, Lcom/android/mail/ui/ActionableToastBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/android/mail/ui/TwoPaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public resetActionBarIcon()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->removeBackButton()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/TwoPaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    goto :goto_0
.end method

.method public setCurrentConversation(Lcom/android/mail/providers/Conversation;)V
    .locals 7
    .param p1    # Lcom/android/mail/providers/Conversation;

    const-wide/16 v2, -0x1

    iget-object v6, p0, Lcom/android/mail/ui/TwoPaneController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/mail/ui/TwoPaneController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v4, v6, Lcom/android/mail/providers/Conversation;->id:J

    :goto_0
    if-eqz p1, :cond_0

    iget-wide v2, p1, Lcom/android/mail/providers/Conversation;->id:J

    :cond_0
    cmp-long v6, v4, v2

    if-eqz v6, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget v6, p1, Lcom/android/mail/providers/Conversation;->position:I

    invoke-virtual {v0, v6, v1}, Lcom/android/mail/ui/ConversationListFragment;->setSelected(IZ)V

    :cond_1
    return-void

    :cond_2
    move-wide v4, v2

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public shouldShowFirstConversation()Z
    .locals 2

    const-string v0, "android.intent.action.SEARCH"

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->shouldEnterSearchConvMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showConversation(Lcom/android/mail/providers/Conversation;Z)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;Z)V

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->handleBackPress()Z

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/ui/TwoPaneController;->enableOrDisableCab()V

    iput-object p1, p0, Lcom/android/mail/ui/TwoPaneController;->mConversationToShow:Lcom/android/mail/providers/Conversation;

    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v1

    const/4 v0, 0x0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    const/4 v2, 0x5

    if-ne v1, v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsConversationMode()Z

    move-result v0

    :goto_1
    if-nez v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/TwoPaneController;->onConversationVisibilityChanged(Z)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/mail/ui/TwoPaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterConversationMode()Z

    move-result v0

    goto :goto_1
.end method

.method public showConversationList(Lcom/android/mail/ConversationListContext;)V
    .locals 1
    .param p1    # Lcom/android/mail/ConversationListContext;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/ui/TwoPaneController;->initializeConversationListFragment(Z)V

    return-void
.end method

.method public showFolderList()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->handleUpPress()Z

    return-void
.end method

.method public showWaitForInitialization()V
    .locals 4

    invoke-super {p0}, Lcom/android/mail/ui/AbstractActivityController;->showWaitForInitialization()V

    iget-object v1, p0, Lcom/android/mail/ui/TwoPaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    const v1, 0x7f08003b

    invoke-virtual {p0}, Lcom/android/mail/ui/TwoPaneController;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v2

    const-string v3, "wait-fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method
