.class public final Lcom/android/mail/ui/ConversationViewFragment;
.super Lcom/android/mail/ui/AbstractConversationViewFragment;
.source "ConversationViewFragment.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;
.implements Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;,
        Lcom/android/mail/ui/ConversationViewFragment$CssScaleInterceptor;,
        Lcom/android/mail/ui/ConversationViewFragment$SetCookieTask;,
        Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;,
        Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;,
        Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;,
        Lcom/android/mail/ui/ConversationViewFragment$ConversationWebViewClient;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_WEBVIEW_Y_PERCENT:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final LOAD_NOW:I

.field private final LOAD_WAIT_FOR_INITIAL_CONVERSATION:I

.field private final LOAD_WAIT_UNTIL_VISIBLE:I

.field private final WHOOSH_SCALE_MINIMUM:F

.field private mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

.field private mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

.field private mDiff:I

.field private mEnableContentReadySignal:Z

.field private mIgnoreOnScaleCalls:Z

.field private final mJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;

.field private mLoadWaitReason:I

.field private final mLoadedObserver:Landroid/database/DataSetObserver;

.field private mMaxAutoLoadMessages:I

.field private mMessageGroup:Landroid/view/View;

.field private mMessageInnerGroup:Landroid/view/View;

.field private final mMessageJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

.field private mMessageScrollXFraction:F

.field private mMessageScrollYFraction:F

.field private mMessageView:Lcom/android/mail/browse/MessageView;

.field private mMessageViewIsLoading:Z

.field private mMessageViewLoadStartMs:J

.field private mNeedRender:Z

.field private mNewMessageBar:Landroid/view/View;

.field private mOnMessageLoadComplete:Ljava/lang/Runnable;

.field private final mOnProgressDismiss:Ljava/lang/Runnable;

.field private mScrollIndicators:Lcom/android/mail/browse/ScrollIndicatorsView;

.field private mSideMarginPx:I

.field private mTempBodiesHtml:Ljava/lang/String;

.field private mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

.field private mViewsCreated:Z

.field private mWebView:Lcom/android/mail/browse/ConversationWebView;

.field private final mWebViewClient:Landroid/webkit/WebViewClient;

.field private mWebViewLoadStartMs:J

.field private mWebViewLoadedData:Z

.field private mWebViewSizeChangeListener:Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;

.field private mWebViewYPercent:F

.field private mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/android/mail/ui/ConversationViewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "webview-y-percent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/ConversationViewFragment;->BUNDLE_KEY_WEBVIEW_Y_PERCENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;-><init>()V

    iput v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mDiff:I

    iput v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->LOAD_NOW:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->LOAD_WAIT_FOR_INITIAL_CONVERSATION:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->LOAD_WAIT_UNTIL_VISIBLE:I

    const v0, 0x3dcccccd

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->WHOOSH_SCALE_MINIMUM:F

    iput-boolean v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    invoke-direct {v0, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;

    invoke-direct {v0, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$ConversationWebViewClient;

    invoke-direct {v0, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$ConversationWebViewClient;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    iput v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$1;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/ConversationViewFragment$1;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadedObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$2;

    const-string v1, "onProgressDismiss"

    invoke-direct {v0, p0, v1}, Lcom/android/mail/ui/ConversationViewFragment$2;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mOnProgressDismiss:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mail/ui/ConversationViewFragment;)F
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F

    return v0
.end method

.method static synthetic access$1002(Lcom/android/mail/ui/ConversationViewFragment;F)F
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # F

    iput p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F

    return p1
.end method

.method static synthetic access$1100(Lcom/android/mail/ui/ConversationViewFragment;)F
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F

    return v0
.end method

.method static synthetic access$1102(Lcom/android/mail/ui/ConversationViewFragment;F)F
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # F

    iput p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F

    return p1
.end method

.method static synthetic access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/mail/ui/ConversationViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewsCreated:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/android/mail/ui/ConversationViewFragment;)J
    .locals 2
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-wide v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewLoadStartMs:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->ensureContentSizeChangeListener()V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/mail/ui/ConversationViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mEnableContentReadySignal:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->revealConversation()V

    return-void
.end method

.method static synthetic access$2000([Ljava/lang/String;[Ljava/lang/String;)[Lcom/android/mail/browse/ConversationContainer$OverlayPosition;
    .locals 1
    .param p0    # [Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/mail/ui/ConversationViewFragment;->parsePositions([Ljava/lang/String;[Ljava/lang/String;)[Lcom/android/mail/browse/ConversationContainer$OverlayPosition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationContainer;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/mail/ui/ConversationViewFragment;)I
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mDiff:I

    return v0
.end method

.method static synthetic access$2202(Lcom/android/mail/ui/ConversationViewFragment;I)I
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # I

    iput p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mDiff:I

    return p1
.end method

.method static synthetic access$2300(Lcom/android/mail/ui/ConversationViewFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTempBodiesHtml:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTempBodiesHtml:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/ui/HtmlConversationTemplates;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/mail/ui/ConversationViewFragment;)F
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewYPercent:F

    return v0
.end method

.method static synthetic access$2600(Lcom/android/mail/ui/ConversationViewFragment;)J
    .locals 2
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-wide v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J

    return-wide v0
.end method

.method static synthetic access$2602(Lcom/android/mail/ui/ConversationViewFragment;J)J
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J

    return-wide p1
.end method

.method static synthetic access$2700(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/String;)Lcom/android/mail/providers/Address;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/ConversationViewFragment;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/mail/ui/ConversationViewFragment;)I
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mSideMarginPx:I

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/mail/ui/ConversationViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/android/mail/ui/ConversationViewFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/android/mail/providers/Account;)Z
    .locals 1
    .param p0    # Lcom/android/mail/providers/Account;

    invoke-static {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationViewAdapter;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/mail/ui/ConversationViewFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mOnMessageLoadComplete:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mOnMessageLoadComplete:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->handleDelayedConversationLoad()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->showConversation()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->onNewMessageBarClick()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->dismissWhooshMode()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mail/ui/ConversationViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewIsLoading:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/mail/ui/ConversationViewFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewIsLoading:Z

    return p1
.end method

.method private calculateScrollYPercent()F
    .locals 6

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v1

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->getHeight()I

    move-result v2

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->getContentHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v5}, Lcom/android/mail/browse/ConversationWebView;->getScale()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v3, v4

    if-eqz v3, :cond_0

    if-gt v3, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    add-int v4, v1, v2

    if-lt v4, v3, :cond_2

    const/high16 v0, 0x3f800000

    goto :goto_0

    :cond_2
    int-to-float v4, v1

    int-to-float v5, v3

    div-float v0, v4, v5

    goto :goto_0
.end method

.method private dismissWhooshMode()V
    .locals 3

    const v2, 0x3f4ccccd

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$9;

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    invoke-direct {v1, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$9;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private ensureContentSizeChangeListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewSizeChangeListener:Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$8;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/ConversationViewFragment$8;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewSizeChangeListener:Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewSizeChangeListener:Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->setContentSizeChangeListener(Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;)V

    return-void
.end method

.method private getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAddressCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Address;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/android/mail/providers/Address;->getEmailAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAddressCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private getNewIncomingMessagesInfo(Lcom/android/mail/browse/MessageCursor;)Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V

    const/4 v0, -0x1

    :cond_0
    :goto_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v3, v2}, Lcom/android/mail/ui/ConversationViewState;->contains(Lcom/android/mail/providers/Message;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "conversation diff: found new msg: %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, v2, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/mail/ui/ConversationViewFragment;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v3}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/android/mail/providers/Account;->ownsFromAddress(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "found message from self: %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    aput-object v2, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget v2, v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->countFromSelf:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->countFromSelf:I

    goto :goto_0

    :cond_1
    iget v3, v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->count:I

    invoke-virtual {v2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->senderAddress:Ljava/lang/String;

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private handleDelayedConversationLoad()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->resetLoadWaiting()V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->startConversationLoad()V

    return-void
.end method

.method private initHeaderView(Lcom/android/mail/browse/MessageHeaderView;Lcom/android/mail/FormattedDateBuilder;)V
    .locals 1
    .param p1    # Lcom/android/mail/browse/MessageHeaderView;
    .param p2    # Lcom/android/mail/FormattedDateBuilder;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAddressCache:Ljava/util/Map;

    invoke-virtual {p1, p2, p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->initialize(Lcom/android/mail/FormattedDateBuilder;Lcom/android/mail/browse/ConversationAccountController;Ljava/util/Map;)V

    invoke-virtual {p1, p0}, Lcom/android/mail/browse/MessageHeaderView;->setCallbacks(Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getContactInfoSource()Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mail/browse/MessageHeaderView;->setContactInfoSource(Lcom/android/mail/ContactInfoSource;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/mail/ui/AccountController;->getVeiledAddressMatcher()Lcom/android/mail/utils/VeiledAddressMatcher;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mail/browse/MessageHeaderView;->setVeiledMatcher(Lcom/android/mail/utils/VeiledAddressMatcher;)V

    return-void
.end method

.method private isLoadWaiting()Z
    .locals 1

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isOverviewMode(Lcom/android/mail/providers/Account;)Z
    .locals 1
    .param p0    # Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v0, v0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private measureOverlayHeight(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationViewAdapter;->getItem(I)Lcom/android/mail/browse/ConversationOverlayItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(Lcom/android/mail/browse/ConversationOverlayItem;)I

    move-result v0

    return v0
.end method

.method private measureOverlayHeight(Lcom/android/mail/browse/ConversationOverlayItem;)I
    .locals 7
    .param p1    # Lcom/android/mail/browse/ConversationOverlayItem;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationOverlayItem;->getType()I

    move-result v3

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v4, v3}, Lcom/android/mail/browse/ConversationContainer;->getScrapView(I)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    const/4 v6, 0x1

    invoke-virtual {v4, p1, v0, v5, v6}, Lcom/android/mail/browse/ConversationViewAdapter;->getView(Lcom/android/mail/browse/ConversationOverlayItem;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v4, v3, v2}, Lcom/android/mail/browse/ConversationContainer;->addScrapView(ILandroid/view/View;)V

    :cond_0
    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v4, v2}, Lcom/android/mail/browse/ConversationContainer;->measureOverlay(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/mail/browse/ConversationOverlayItem;->setHeight(I)Z

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationOverlayItem;->markMeasurementValid()V

    return v1
.end method

.method public static newInstance(Landroid/os/Bundle;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/ConversationViewFragment;
    .locals 3
    .param p0    # Landroid/os/Bundle;
    .param p1    # Lcom/android/mail/providers/Conversation;

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {v1}, Lcom/android/mail/ui/ConversationViewFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string v2, "conversation"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/ConversationViewFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onNewMessageBarClick()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNewMessageBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->renderConversation(Lcom/android/mail/browse/MessageCursor;)V

    return-void
.end method

.method private static parsePositions([Ljava/lang/String;[Ljava/lang/String;)[Lcom/android/mail/browse/ConversationContainer$OverlayPosition;
    .locals 6
    .param p0    # [Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    array-length v1, p0

    new-array v2, v1, [Lcom/android/mail/browse/ConversationContainer$OverlayPosition;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v3, Lcom/android/mail/browse/ConversationContainer$OverlayPosition;

    aget-object v4, p0, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aget-object v5, p1, v0

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/android/mail/browse/ConversationContainer$OverlayPosition;-><init>(II)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private processInPlaceUpdates(Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor;)Z
    .locals 12

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p2, v0}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0, v3}, Lcom/android/mail/browse/ConversationContainer;->onOverlayModelUpdate(Ljava/util/List;)V

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    const-string v1, "javascript:replaceMessageBodies([%s]);"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, ","

    invoke-static {v5, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    invoke-virtual {p2}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v5

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFrom()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFrom()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v6, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isSending:Z

    iget-boolean v7, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isSending:Z

    if-eq v6, v7, :cond_4

    :cond_3
    iget-object v6, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v6, v4, v3}, Lcom/android/mail/browse/ConversationViewAdapter;->updateItemsForMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;Ljava/util/List;)V

    sget-object v6, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v7, "msg #%d (%d): detected from/sending change. isSending=%s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-wide v10, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->id:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-boolean v10, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isSending:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_4
    iget-object v6, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->bodyHtml:Ljava/lang/String;

    iget-object v7, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->bodyHtml:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->bodyText:Ljava/lang/String;

    iget-object v5, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->bodyText:Ljava/lang/String;

    invoke-static {v6, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x22

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v6, v4}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x22

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v6, "msg #%d (%d): detected body change"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-wide v9, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->id:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v5, v6, v7}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method private processNewOutgoingMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;)V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->reset()V

    const/4 v0, 0x1

    iget-boolean v1, p1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/android/mail/ui/ConversationViewFragment;->renderMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->emit()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTempBodiesHtml:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    sget v1, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->EXPANDED:I

    invoke-virtual {v0, p1, v1}, Lcom/android/mail/ui/ConversationViewState;->setExpansionState(Lcom/android/mail/providers/Message;I)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/mail/ui/ConversationViewState;->setReadState(Lcom/android/mail/providers/Message;Z)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationContainer;->invalidateSpacerGeometry()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    const-string v1, "javascript:appendMessageHtml();"

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private renderCollapsedHeaders(Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;)Ljava/lang/String;
    .locals 16
    .param p1    # Lcom/android/mail/browse/MessageCursor;
    .param p2    # Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v1}, Lcom/android/mail/ui/HtmlConversationTemplates;->reset()V

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->getStart()I

    move-result v14

    invoke-virtual/range {p2 .. p2}, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->getEnd()I

    move-result v8

    :goto_0
    if-gt v14, v8, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v4, v2}, Lcom/android/mail/ui/ConversationViewState;->getShouldShowImages(Lcom/android/mail/providers/Message;)Z

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/mail/browse/ConversationViewAdapter;->newMessageHeaderItem(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v1, v12}, Lcom/android/mail/browse/ConversationViewAdapter;->newMessageFooterItem(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;)Lcom/android/mail/browse/ConversationViewAdapter$MessageFooterItem;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(Lcom/android/mail/browse/ConversationOverlayItem;)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(Lcom/android/mail/browse/ConversationOverlayItem;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v13}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPxError(I)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v3, v11}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPxError(I)F

    move-result v3

    add-float/2addr v1, v3

    add-float/2addr v9, v1

    const/4 v7, 0x0

    const/high16 v1, 0x3f800000

    cmpl-float v1, v9, v1

    if-ltz v1, :cond_0

    const/4 v7, 0x1

    const/high16 v1, 0x3f800000

    sub-float/2addr v9, v1

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v5, v13}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v5

    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v6, v11}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/android/mail/ui/HtmlConversationTemplates;->appendMessageHtml(Lcom/android/mail/providers/Message;ZZII)V

    invoke-interface {v15, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v15, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    sget v3, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->COLLAPSED:I

    invoke-virtual {v1, v2, v3}, Lcom/android/mail/ui/ConversationViewState;->setExpansionState(Lcom/android/mail/providers/Message;I)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0, v15}, Lcom/android/mail/browse/ConversationViewAdapter;->replaceSuperCollapsedBlock(Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v1}, Lcom/android/mail/ui/HtmlConversationTemplates;->emit()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private renderConversation(Lcom/android/mail/browse/MessageCursor;)V
    .locals 6
    .param p1    # Lcom/android/mail/browse/MessageCursor;

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mEnableContentReadySignal:Z

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/ConversationViewFragment;->renderMessageBodies(Lcom/android/mail/browse/MessageCursor;Z)Ljava/lang/String;

    move-result-object v2

    iget-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewLoadedData:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->calculateScrollYPercent()F

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewYPercent:F

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/browse/ConversationWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewLoadedData:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewLoadStartMs:J

    return-void
.end method

.method private renderMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)V
    .locals 11
    .param p1    # Lcom/android/mail/browse/MessageCursor$ConversationMessage;
    .param p2    # Z
    .param p3    # Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v1, p1}, Lcom/android/mail/ui/ConversationViewState;->getShouldShowImages(Lcom/android/mail/providers/Message;)Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/mail/browse/ConversationViewAdapter;->addMessageHeader(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)I

    move-result v9

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v0, v9}, Lcom/android/mail/browse/ConversationViewAdapter;->getItem(I)Lcom/android/mail/browse/ConversationOverlayItem;

    move-result-object v8

    check-cast v8, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v0, v8}, Lcom/android/mail/browse/ConversationViewAdapter;->addMessageFooter(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;)I

    move-result v6

    invoke-direct {p0, v9}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(I)I

    move-result v10

    invoke-direct {p0, v6}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(I)I

    move-result v7

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v10}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v4

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v7}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v5

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->appendMessageHtml(Lcom/android/mail/providers/Message;ZZII)V

    return-void
.end method

.method private renderMessageBodies(Lcom/android/mail/browse/MessageCursor;Z)Ljava/lang/String;
    .locals 22
    .param p1    # Lcom/android/mail/browse/MessageCursor;
    .param p2    # Z

    const/16 v16, -0x1

    sget-object v3, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "IN renderMessageBodies, fragment=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationContainer;->invalidateSpacerGeometry()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationViewAdapter;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    move-object/from16 v19, v0

    new-instance v3, Lcom/android/mail/ui/ConversationViewState;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/android/mail/ui/ConversationViewState;-><init>(Lcom/android/mail/ui/ConversationViewState;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationViewAdapter;->addConversationHeader(Lcom/android/mail/providers/Conversation;)I

    move-result v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(I)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/mail/ui/ConversationViewFragment;->mSideMarginPx:I

    invoke-virtual {v4, v5}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v5, v13}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->startConversation(II)V

    const/4 v11, -0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    :goto_0
    add-int/lit8 v16, v16, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v15

    iget-boolean v3, v15, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    if-nez v3, :cond_0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/mail/ui/ConversationViewState;->getShouldShowImages(Lcom/android/mail/providers/Message;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/16 v20, 0x1

    :goto_1
    or-int v10, v10, v20

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/mail/ui/ConversationViewState;->getExpansionState(Lcom/android/mail/providers/Message;)Ljava/lang/Integer;

    move-result-object v21

    if-eqz v21, :cond_4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->isSuperCollapsed(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/MessageCursor;->isLast()Z

    move-result v3

    if-eqz v3, :cond_3

    sget v14, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->EXPANDED:I

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/mail/ui/ConversationViewState;->getShouldShowImages(Lcom/android/mail/providers/Message;)Z

    move-result v4

    invoke-virtual {v3, v15, v4}, Lcom/android/mail/ui/ConversationViewState;->setShouldShowImages(Lcom/android/mail/providers/Message;Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v3, v15, v14}, Lcom/android/mail/ui/ConversationViewState;->setExpansionState(Lcom/android/mail/providers/Message;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    iget-boolean v3, v15, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->read:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/mail/ui/ConversationViewState;->isUnread(Lcom/android/mail/providers/Message;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    invoke-virtual {v4, v15, v3}, Lcom/android/mail/ui/ConversationViewState;->setReadState(Lcom/android/mail/providers/Message;Z)V

    invoke-static {v14}, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->isSuperCollapsed(I)Z

    move-result v3

    if-eqz v3, :cond_8

    if-gez v11, :cond_1

    move/from16 v11, v16

    :cond_1
    move-object/from16 v17, v15

    move/from16 v18, v20

    goto :goto_0

    :cond_2
    const/16 v20, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v14

    goto :goto_2

    :cond_4
    iget-boolean v3, v15, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->read:Z

    if-eqz v3, :cond_5

    iget-boolean v3, v15, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->starred:Z

    if-nez v3, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/MessageCursor;->isLast()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    sget v14, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->EXPANDED:I

    :goto_4
    goto :goto_2

    :cond_6
    sget v14, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->SUPER_COLLAPSED:I

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    :cond_8
    if-ltz v11, :cond_9

    sub-int v3, v16, v11

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v3, v2}, Lcom/android/mail/ui/ConversationViewFragment;->renderMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)V

    :goto_5
    const/16 v17, 0x0

    const/4 v11, -0x1

    :cond_9
    invoke-static {v14}, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->isExpanded(I)Z

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v15, v3, v1}, Lcom/android/mail/ui/ConversationViewFragment;->renderMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)V

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v3, v16, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3}, Lcom/android/mail/ui/ConversationViewFragment;->renderSuperCollapsedBlock(II)V

    goto :goto_5

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    if-nez v10, :cond_c

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v4, v3}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/android/mail/providers/Conversation;->getBaseUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x140

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v7}, Lcom/android/mail/browse/ConversationWebView;->getViewportWidth()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v8}, Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z

    move-result v9

    move/from16 v8, p2

    invoke-virtual/range {v3 .. v9}, Lcom/android/mail/ui/HtmlConversationTemplates;->endConversation(Ljava/lang/String;Ljava/lang/String;IIZZ)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_c
    const/4 v3, 0x0

    goto :goto_6
.end method

.method private renderSuperCollapsedBlock(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/android/mail/browse/ConversationViewAdapter;->addSuperCollapsedBlock(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->measureOverlayHeight(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v3, v1}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/android/mail/ui/HtmlConversationTemplates;->appendSuperCollapsedHtml(II)V

    return-void
.end method

.method private resetLoadWaiting()V
    .locals 2

    iget v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getListController()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadedObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ConversationUpdater;->unregisterConversationLoadedObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    return-void
.end method

.method private revealConversation()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mOnProgressDismiss:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->dismissLoadingStatus(Ljava/lang/Runnable;)V

    return-void
.end method

.method private setupOverviewMode()V
    .locals 5

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z

    move-result v1

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/mail/preferences/MailPrefs;->get(Landroid/content/Context;)Lcom/android/mail/preferences/MailPrefs;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/preferences/MailPrefs;->isWhooshZoomEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    invoke-direct {v0, p0, v3}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V

    :goto_0
    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v3, v0}, Lcom/android/mail/browse/ConversationWebView;->setOnScaleGestureListener(Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    return-void

    :cond_0
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    if-eqz v1, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    :cond_1
    move-object v0, v3

    check-cast v0, Lcom/android/mail/ui/ConversationViewFragment$CssScaleInterceptor;

    goto :goto_0
.end method

.method private showConversation()V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "SHOWCONV: CVF is user-visible, immediately loading conversation (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-static {v4, v5, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    :goto_0
    iput v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    iget v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadWaitReason:I

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->startConversationLoad()V

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v4, v4, Lcom/android/mail/providers/Conversation;->isRemote:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v4}, Lcom/android/mail/providers/Conversation;->getNumMessages()I

    move-result v4

    iget v5, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMaxAutoLoadMessages:I

    if-le v4, v5, :cond_3

    :cond_2
    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    const/4 v1, 0x2

    sget-object v4, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "SHOWCONV: CVF waiting until visible to load (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-static {v4, v5, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getListController()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/mail/ui/ConversationUpdater;->isInitialConversationLoading()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v1, 0x1

    sget-object v4, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "SHOWCONV: CVF waiting for initial to finish (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-static {v4, v5, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getListController()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mLoadedObserver:Landroid/database/DataSetObserver;

    invoke-interface {v2, v3}, Lcom/android/mail/ui/ConversationUpdater;->registerConversationLoadedObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    :cond_5
    sget-object v4, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "SHOWCONV: CVF is not visible, but no reason to wait. loading now. (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-static {v4, v5, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showNewMessageNotification(Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;)V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNewMessageBar:Landroid/view/View;

    const v1, 0x7f0800c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->getNotificationText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNewMessageBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private startConversationLoad()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v4}, Lcom/android/mail/browse/ConversationWebView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageLoaderCallbacks()Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getSubjectDisplayChanger()Lcom/android/mail/ui/SubjectDisplayChanger;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v1, v1, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/SubjectDisplayChanger;->setSubject(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->showLoadingStatus()V

    return-void
.end method


# virtual methods
.method protected getWebView()Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    return-object v0
.end method

.method protected markUnread()V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->markUnread()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-nez v0, :cond_0

    sget-object v1, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ignoring markUnread for conv=%s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v4, v4, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ignoring markUnread for conv with no view state (%d)"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v4, v4, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationViewState;->getUnreadMessageUris()Ljava/util/Set;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v4}, Lcom/android/mail/ui/ConversationViewState;->getConversationInfo()[B

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/android/mail/ui/ConversationUpdater;->markConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V

    goto :goto_0
.end method

.method public onAccountChanged(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Account;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Account;

    invoke-static {p1}, Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z

    move-result v1

    invoke-static {p2}, Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z

    move-result v2

    if-eq v1, v2, :cond_1

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->setupOverviewMode()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->renderConversation(Lcom/android/mail/browse/MessageCursor;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v13, 0x1

    const/4 v12, 0x0

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IN CVF.onActivityCreated, this=%s visible=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v12

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getContext()Landroid/content/Context;

    move-result-object v10

    new-instance v0, Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-direct {v0, v10}, Lcom/android/mail/ui/HtmlConversationTemplates;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    new-instance v9, Lcom/android/mail/FormattedDateBuilder;

    invoke-direct {v9, v10}, Lcom/android/mail/FormattedDateBuilder;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/mail/browse/ConversationViewAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getContactInfoSource()Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    move-result-object v5

    iget-object v8, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAddressCache:Ljava/util/Map;

    move-object v2, p0

    move-object v4, p0

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v9}, Lcom/android/mail/browse/ConversationViewAdapter;-><init>(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/browse/ConversationAccountController;Landroid/app/LoaderManager;Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;Lcom/android/mail/ContactInfoSource;Lcom/android/mail/browse/ConversationViewHeader$ConversationViewHeaderCallbacks;Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;Ljava/util/Map;Lcom/android/mail/FormattedDateBuilder;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationContainer;->setOverlayAdapter(Lcom/android/mail/browse/ConversationViewAdapter;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationContainer;->getSnapHeader()Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v11

    invoke-direct {p0, v11, v9}, Lcom/android/mail/ui/ConversationViewFragment;->initHeaderView(Lcom/android/mail/browse/MessageHeaderView;Lcom/android/mail/FormattedDateBuilder;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    const v1, 0x7f0800a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/MessageHeaderView;

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;

    invoke-direct {p0, v0, v9}, Lcom/android/mail/ui/ConversationViewFragment;->initHeaderView(Lcom/android/mail/browse/MessageHeaderView;Lcom/android/mail/FormattedDateBuilder;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v0, v13}, Lcom/android/mail/browse/MessageHeaderView;->setSnappy(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v0, v12}, Lcom/android/mail/browse/MessageHeaderView;->setExpandable(Z)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMaxAutoLoadMessages:I

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mSideMarginPx:I

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    new-instance v1, Lcom/android/mail/browse/WebViewContextMenu;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/mail/browse/WebViewContextMenu;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->setupOverviewMode()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$3;

    const-string v2, "showConversation"

    invoke-direct {v1, p0, v2}, Lcom/android/mail/ui/ConversationViewFragment$3;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v0, v0, Lcom/android/mail/providers/Conversation;->conversationBaseUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->accoutCookieQueryUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->isEmpty(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mail/ui/ConversationViewFragment$SetCookieTask;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-object v2, v2, Lcom/android/mail/providers/Conversation;->conversationBaseUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->accoutCookieQueryUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/mail/ui/ConversationViewFragment$SetCookieTask;-><init>(Lcom/android/mail/ui/ConversationViewFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V

    new-array v1, v12, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/ConversationViewFragment$SetCookieTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->dismissWhooshMode()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConversationUpdated(Lcom/android/mail/providers/Conversation;)V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    const v1, 0x7f0800ad

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/ConversationViewHeader;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationViewHeader;->onConversationUpdated(Lcom/android/mail/providers/Conversation;)V

    iget-object v1, p1, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationViewHeader;->setSubject(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onConversationViewHeaderHeightChange(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    const-string v2, "javascript:setConversationHeaderSpacerHeight(%s);"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->BUNDLE_KEY_WEBVIEW_Y_PERCENT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewYPercent:F

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v9, 0x0

    const/4 v8, 0x1

    const v7, 0x7f040030

    invoke-virtual {p1, v7, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v7, 0x7f0800a3

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/mail/browse/ConversationContainer;

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v7, p0}, Lcom/android/mail/browse/ConversationContainer;->setAccountController(Lcom/android/mail/browse/ConversationAccountController;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    const v10, 0x7f0800c0

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/ConversationContainer;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNewMessageBar:Landroid/view/View;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNewMessageBar:Landroid/view/View;

    new-instance v10, Lcom/android/mail/ui/ConversationViewFragment$4;

    invoke-direct {v10, p0}, Lcom/android/mail/ui/ConversationViewFragment$4;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/ConversationViewFragment;->instantiateProgressIndicators(Landroid/view/View;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    const v10, 0x7f0800a4

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/ConversationContainer;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/mail/browse/ConversationWebView;

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-object v10, p0, Lcom/android/mail/ui/ConversationViewFragment;->mJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MailJsBridge;

    const-string v11, "mail"

    invoke-virtual {v7, v10, v11}, Lcom/android/mail/browse/ConversationWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/mail/utils/Utils;->isRunningJellybeanOrLater()Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mEnableContentReadySignal:Z

    iget-object v10, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-boolean v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mEnableContentReadySignal:Z

    if-nez v7, :cond_0

    move v7, v8

    :goto_0
    invoke-virtual {v10, v7}, Lcom/android/mail/browse/ConversationWebView;->setUseSoftwareLayer(Z)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v10

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/ConversationWebView;->onUserVisibilityChanged(Z)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    iget-object v10, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/ConversationWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-instance v6, Lcom/android/mail/ui/ConversationViewFragment$5;

    invoke-direct {v6, p0}, Lcom/android/mail/ui/ConversationViewFragment$5;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v7, v6}, Lcom/android/mail/browse/ConversationWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v7}, Lcom/android/mail/browse/ConversationWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    const v7, 0x7f0800ac

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/mail/browse/ScrollIndicatorsView;

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mScrollIndicators:Lcom/android/mail/browse/ScrollIndicatorsView;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mScrollIndicators:Lcom/android/mail/browse/ScrollIndicatorsView;

    iget-object v10, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/ScrollIndicatorsView;->setSourceView(Lcom/android/mail/browse/ScrollNotifier;)V

    invoke-virtual {v3, v8}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v1, v7, Landroid/content/res/Configuration;->fontScale:F

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f0b0027

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f0b0028

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getTextZoom()I

    move-result v4

    mul-int v7, v4, v0

    div-int v4, v7, v5

    int-to-float v7, v4

    mul-float/2addr v7, v1

    float-to-int v4, v7

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    const v7, 0x7f0800a7

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    const v10, 0x7f0800a9

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    const v10, 0x7f0800aa

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/mail/browse/MessageView;

    iput-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;

    const v10, 0x7f0800ab

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v10, Lcom/android/mail/ui/ConversationViewFragment$6;

    invoke-direct {v10, p0}, Lcom/android/mail/ui/ConversationViewFragment$6;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    invoke-virtual {v7, v6}, Lcom/android/mail/browse/MessageView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    new-instance v10, Lcom/android/mail/ui/ConversationViewFragment$7;

    invoke-direct {v10, p0}, Lcom/android/mail/ui/ConversationViewFragment$7;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    invoke-virtual {v7, v10}, Lcom/android/mail/browse/MessageView;->setContentSizeChangeListener(Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    invoke-virtual {v7}, Lcom/android/mail/browse/MessageView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v7, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;

    iget-object v10, p0, Lcom/android/mail/ui/ConversationViewFragment;->mMessageJsBridge:Lcom/android/mail/ui/ConversationViewFragment$MessageJsBridge;

    const-string v11, "mail"

    invoke-virtual {v7, v10, v11}, Lcom/android/mail/browse/MessageView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewsCreated:Z

    iput-boolean v9, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebViewLoadedData:Z

    return-object v2

    :cond_0
    move v7, v9

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationContainer;->setOverlayAdapter(Lcom/android/mail/browse/ConversationViewAdapter;)V

    iput-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->resetLoadWaiting()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewsCreated:Z

    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNeedRender:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationContainer;->getWidth()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNeedRender:Z

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v1, p0}, Lcom/android/mail/browse/ConversationContainer;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/mail/ui/ConversationViewFragment;->renderConversation(Lcom/android/mail/browse/MessageCursor;)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onMessageCursorLoadFinished(Landroid/content/Loader;Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/android/mail/browse/MessageCursor;",
            "Lcom/android/mail/browse/MessageCursor;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p3, :cond_6

    invoke-virtual {p3}, Lcom/android/mail/browse/MessageCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0, p2}, Lcom/android/mail/ui/ConversationViewFragment;->getNewIncomingMessagesInfo(Lcom/android/mail/browse/MessageCursor;)Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;

    move-result-object v3

    iget v0, v3, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->count:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "CONV RENDER: conversation updated, holding cursor for new incoming message (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v3}, Lcom/android/mail/ui/ConversationViewFragment;->showNewMessageNotification(Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/android/mail/browse/MessageCursor;->getStateHashCode()I

    move-result v4

    invoke-virtual {p2}, Lcom/android/mail/browse/MessageCursor;->getStateHashCode()I

    move-result v0

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    invoke-direct {p0, p2, p3}, Lcom/android/mail/ui/ConversationViewFragment;->processInPlaceUpdates(Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CONV RENDER: processed update(s) in place (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CONV RENDER: uninteresting update, ignoring this conversation update (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_3
    iget v0, v3, Lcom/android/mail/ui/ConversationViewFragment$NewMessagesInfo;->countFromSelf:I

    if-ne v0, v1, :cond_5

    invoke-virtual {p2, v1}, Lcom/android/mail/browse/MessageCursor;->getStateHashCode(I)I

    move-result v0

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CONV RENDER: update is a single new message from self (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p2}, Lcom/android/mail/browse/MessageCursor;->moveToLast()Z

    invoke-virtual {p2}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationViewFragment;->processNewOutgoingMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CONV RENDER: conversation updated, but not due to incoming message. rendering. (%s)"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_3
    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationContainer;->getWidth()I

    move-result v0

    if-nez v0, :cond_7

    iput-boolean v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mNeedRender:Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0, p0}, Lcom/android/mail/browse/ConversationContainer;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CONV RENDER: initial render. (%s)"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    :cond_7
    invoke-direct {p0, p2}, Lcom/android/mail/ui/ConversationViewFragment;->renderConversation(Lcom/android/mail/browse/MessageCursor;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget-object v0, Lcom/android/mail/ui/ConversationViewFragment;->BUNDLE_KEY_WEBVIEW_Y_PERCENT:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->calculateScrollYPercent()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getUpOrBackController()Lcom/android/mail/ui/UpOrBackController;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/android/mail/ui/UpOrBackController;->addUpOrBackHandler(Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onStop()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getUpOrBackController()Lcom/android/mail/ui/UpOrBackController;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/android/mail/ui/UpOrBackController;->removeUpOrBackHandler(Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;)V

    :cond_0
    return-void
.end method

.method public onSuperCollapsedClick(Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewsCreated:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/android/mail/ui/ConversationViewFragment;->renderCollapsedHeaders(Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTempBodiesHtml:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:replaceSuperCollapsedBlock("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->getStart()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUpPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onUserVisibleHintChanged()V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isUserVisible()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->dismissLoadingStatus()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationWebView;->onUserVisibilityChanged(Z)V

    :cond_1
    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewsCreated:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Fragment is now user-visible, onConversationSeen: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->onConversationSeen()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->isLoadWaiting()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Fragment is now user-visible, showing conversation: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationViewFragment;->handleDelayedConversationLoad()V

    goto :goto_0
.end method

.method public setMessageDetailsExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;ZI)V
    .locals 2
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # Z
    .param p3    # I

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getHeight()I

    move-result v1

    sub-int/2addr v1, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mDiff:I

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setMessageExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V
    .locals 9

    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationContainer;->invalidateSpacerGeometry()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v0, p2}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v0

    const-string v1, "ConvLayout"

    const-string v2, "setting HTML spacer expanded=%s h=%dwebPx (%dscreenPx)"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    const-string v2, "javascript:setMessageBodyVisible(\'%s\', %s, %s);"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->EXPANDED:I

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/android/mail/ui/ConversationViewState;->setExpansionState(Lcom/android/mail/providers/Message;I)V

    return-void

    :cond_0
    sget v0, Lcom/android/mail/ui/ConversationViewState$ExpansionState;->COLLAPSED:I

    goto :goto_0
.end method

.method public setMessageSpacerHeight(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationContainer;->invalidateSpacerGeometry()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v0, p2}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v0

    const-string v1, "ConvLayout"

    const-string v2, "setting HTML spacer h=%dwebPx (%dscreenPx)"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    const-string v2, "javascript:setMessageHeaderSpacerHeight(\'%s\', %s);"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public showExternalResources(Lcom/android/mail/providers/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/mail/ui/ConversationViewState;->setShouldShowImages(Lcom/android/mail/providers/Message;Z)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:unblockImages([\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v2, p1}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\']);"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public showExternalResources(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    invoke-direct {p0, p1}, Lcom/android/mail/ui/ConversationViewFragment;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, -0x1

    :cond_0
    :goto_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Lcom/android/mail/browse/MessageCursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/android/mail/browse/MessageCursor;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFrom()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/ui/ConversationViewFragment;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/mail/providers/Address;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iput-boolean v6, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {v5, v4, v6}, Lcom/android/mail/ui/ConversationViewState;->setShouldShowImages(Lcom/android/mail/providers/Message;Z)V

    iget-object v5, p0, Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;

    invoke-virtual {v5, v4}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, "javascript:unblockImages([\'%s\']);"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "\',\'"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
