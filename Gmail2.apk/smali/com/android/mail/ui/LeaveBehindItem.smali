.class public Lcom/android/mail/ui/LeaveBehindItem;
.super Landroid/widget/FrameLayout;
.source "LeaveBehindItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/mail/ui/SwipeableItemView;


# static fields
.field private static INVISIBLE:F

.field private static OPAQUE:F

.field private static sFadeInAnimationDuration:I

.field private static sScrollSlop:F

.field private static sShrinkAnimationDuration:I


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

.field private mAnimatedHeight:I

.field private mAnimating:Z

.field private mData:Lcom/android/mail/providers/Conversation;

.field private mFadeIn:Landroid/animation/ObjectAnimator;

.field private mFadingInText:Z

.field private mInert:Z

.field private mSwipeableContent:Landroid/view/View;

.field private mText:Landroid/widget/TextView;

.field private mUndoOp:Lcom/android/mail/ui/ToastBarOperation;

.field private mWidth:I

.field public position:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/ui/LeaveBehindItem;->sShrinkAnimationDuration:I

    sput v0, Lcom/android/mail/ui/LeaveBehindItem;->sFadeInAnimationDuration:I

    const/high16 v0, 0x3f800000

    sput v0, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/ui/LeaveBehindItem;->INVISIBLE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/LeaveBehindItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/LeaveBehindItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimatedHeight:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mInert:Z

    sget v1, Lcom/android/mail/ui/LeaveBehindItem;->sShrinkAnimationDuration:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/LeaveBehindItem;->sShrinkAnimationDuration:I

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/LeaveBehindItem;->sFadeInAnimationDuration:I

    const v1, 0x7f0b0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    sput v1, Lcom/android/mail/ui/LeaveBehindItem;->sScrollSlop:F

    :cond_0
    return-void
.end method


# virtual methods
.method public bindOperations(ILcom/android/mail/providers/Account;Lcom/android/mail/ui/AnimatedAdapter;Lcom/android/mail/ui/ToastBarOperation;Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/mail/providers/Account;
    .param p3    # Lcom/android/mail/ui/AnimatedAdapter;
    .param p4    # Lcom/android/mail/ui/ToastBarOperation;
    .param p5    # Lcom/android/mail/providers/Conversation;
    .param p6    # Lcom/android/mail/providers/Folder;

    iput p1, p0, Lcom/android/mail/ui/LeaveBehindItem;->position:I

    iput-object p4, p0, Lcom/android/mail/ui/LeaveBehindItem;->mUndoOp:Lcom/android/mail/ui/ToastBarOperation;

    iput-object p2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAccount:Lcom/android/mail/providers/Account;

    iput-object p3, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {p0, p5}, Lcom/android/mail/ui/LeaveBehindItem;->setData(Lcom/android/mail/providers/Conversation;)V

    const v0, 0x7f0800e8

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/LeaveBehindItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    sget v1, Lcom/android/mail/ui/LeaveBehindItem;->INVISIBLE:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    const v0, 0x7f0800e9

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/LeaveBehindItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mUndoOp:Lcom/android/mail/ui/ToastBarOperation;

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p6}, Lcom/android/mail/ui/ToastBarOperation;->getSingularDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public canChildBeDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mInert:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelFadeInTextAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public cancelFadeInTextAnimationIfNotStarted()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->cancelFadeInTextAnimation()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelFadeOutText()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    sget v1, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public commit()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v1}, Lcom/android/mail/ui/AnimatedAdapter;->getConversationCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/browse/ConversationCursor;->delete(Landroid/content/Context;Ljava/util/Collection;)I

    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mData:Lcom/android/mail/providers/Conversation;

    iget-wide v1, v1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/ui/AnimatedAdapter;->fadeOutSpecificLeaveBehindItem(J)V

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getConversationId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/mail/providers/Conversation;->id:J

    return-wide v0
.end method

.method public getData()Lcom/android/mail/providers/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mData:Lcom/android/mail/providers/Conversation;

    return-object v0
.end method

.method public getLeaveBehindData()Lcom/android/mail/ui/LeaveBehindData;
    .locals 3

    new-instance v0, Lcom/android/mail/ui/LeaveBehindData;

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mUndoOp:Lcom/android/mail/ui/ToastBarOperation;

    invoke-direct {v0, v1, v2}, Lcom/android/mail/ui/LeaveBehindData;-><init>(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ToastBarOperation;)V

    return-object v0
.end method

.method public getMinAllowScrollDistance()F
    .locals 1

    sget v0, Lcom/android/mail/ui/LeaveBehindItem;->sScrollSlop:F

    return v0
.end method

.method public getSwipeableView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    return-object v0
.end method

.method public increaseFadeInDelay(I)V
    .locals 5
    .param p1    # I

    iget-boolean v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v2

    sget v3, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->getStartDelay()J

    move-result-wide v0

    int-to-long v2, p1

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    int-to-long v3, p1

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public isAnimating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    return v0
.end method

.method public makeInert()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mInert:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->undoUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mInert:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/AnimatedAdapter;->setSwipeUndo(Z)V

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getConversationId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/android/mail/ui/AnimatedAdapter;->clearLeaveBehind(J)V

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v2}, Lcom/android/mail/ui/AnimatedAdapter;->getConversationCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->undoUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Lcom/android/mail/browse/ConversationCursor;->undo(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0800e8
        :pswitch_0
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimatedHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mWidth:I

    iget v1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimatedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/ui/LeaveBehindItem;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setAnimatedHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimatedHeight:I

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->requestLayout()V

    return-void
.end method

.method public setData(Lcom/android/mail/providers/Conversation;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Conversation;

    iput-object p1, p0, Lcom/android/mail/ui/LeaveBehindItem;->mData:Lcom/android/mail/providers/Conversation;

    return-void
.end method

.method public setTextAlpha(F)V
    .locals 2
    .param p1    # F

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    sget v1, Lcom/android/mail/ui/LeaveBehindItem;->INVISIBLE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method public startFadeInTextAnimation(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    iget-boolean v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v2

    sget v3, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    iput-boolean v6, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadingInText:Z

    sget v1, Lcom/android/mail/ui/LeaveBehindItem;->INVISIBLE:F

    sget v0, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    const-string v3, "alpha"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v1, v4, v5

    aput v0, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mSwipeableContent:Landroid/view/View;

    sget v3, Lcom/android/mail/ui/LeaveBehindItem;->INVISIBLE:F

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    int-to-long v3, p1

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    sget v4, Lcom/android/mail/ui/LeaveBehindItem;->OPAQUE:F

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    sget v3, Lcom/android/mail/ui/LeaveBehindItem;->sFadeInAnimationDuration:I

    div-int/lit8 v3, v3, 0x2

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/android/mail/ui/LeaveBehindItem;->mFadeIn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    :cond_1
    return-void
.end method

.method public startShrinkAnimation(Lcom/android/mail/ui/ViewMode;Landroid/animation/Animator$AnimatorListener;)V
    .locals 8
    .param p1    # Lcom/android/mail/ui/ViewMode;
    .param p2    # Landroid/animation/Animator$AnimatorListener;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-boolean v4, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimating:Z

    if-nez v4, :cond_0

    iput-boolean v7, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimating:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/LeaveBehindItem;->setMinimumHeight(I)V

    move v3, v2

    const/4 v0, 0x0

    const-string v4, "animatedHeight"

    const/4 v5, 0x2

    new-array v5, v5, [I

    aput v3, v5, v6

    aput v6, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput v3, p0, Lcom/android/mail/ui/LeaveBehindItem;->mAnimatedHeight:I

    invoke-virtual {p0}, Lcom/android/mail/ui/LeaveBehindItem;->getMeasuredWidth()I

    move-result v4

    iput v4, p0, Lcom/android/mail/ui/LeaveBehindItem;->mWidth:I

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v5, 0x3fe00000

    invoke-direct {v4, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget v4, Lcom/android/mail/ui/LeaveBehindItem;->sShrinkAnimationDuration:I

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, p2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void
.end method
