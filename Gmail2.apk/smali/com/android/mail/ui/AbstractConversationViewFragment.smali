.class public abstract Lcom/android/mail/ui/AbstractConversationViewFragment;
.super Landroid/app/Fragment;
.source "AbstractConversationViewFragment.java"

# interfaces
.implements Lcom/android/mail/browse/ConversationAccountController;
.implements Lcom/android/mail/browse/ConversationViewHeader$ConversationViewHeaderCallbacks;
.implements Lcom/android/mail/browse/MessageCursor$ConversationController;
.implements Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/AbstractConversationViewFragment$FragmentRunnable;,
        Lcom/android/mail/ui/AbstractConversationViewFragment$AbstractConversationWebViewClient;,
        Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;,
        Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;,
        Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;
    }
.end annotation


# static fields
.field private static final BUNDLE_USER_VISIBLE:Ljava/lang/String;

.field private static final BUNDLE_VIEW_STATE:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String;

.field private static sMinDelay:I

.field private static sMinShowTime:I


# instance fields
.field protected mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountObserver:Lcom/android/mail/providers/AccountObserver;

.field protected mActivity:Lcom/android/mail/ui/ControllableActivity;

.field protected final mAddressCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/Address;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundView:Landroid/view/View;

.field protected mBaseUri:Ljava/lang/String;

.field private mChangeFoldersMenuItem:Landroid/view/MenuItem;

.field private final mContactLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

.field private mContext:Landroid/content/Context;

.field protected mConversation:Lcom/android/mail/providers/Conversation;

.field private mCursor:Lcom/android/mail/browse/MessageCursor;

.field protected mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

.field private final mDelayedShow:Ljava/lang/Runnable;

.field protected mEnableContentReadySignal:Z

.field protected mFolder:Lcom/android/mail/providers/Folder;

.field private final mHandler:Landroid/os/Handler;

.field private mLoadingShownTime:J

.field private final mMessageLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

.field private mProgressView:Landroid/view/View;

.field private mSuppressMarkingViewed:Z

.field private mUserVisible:Z

.field protected mViewState:Lcom/android/mail/ui/ConversationViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, -0x1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    sput v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinDelay:I

    sput v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinShowTime:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/android/mail/ui/AbstractConversationViewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "viewstate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_VIEW_STATE:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/android/mail/ui/AbstractConversationViewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "uservisible"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_USER_VISIBLE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Lcom/android/mail/ui/AbstractConversationViewFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mMessageLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

    new-instance v0, Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mContactLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAddressCache:Ljava/util/Map;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mLoadingShownTime:J

    new-instance v0, Lcom/android/mail/ui/AbstractConversationViewFragment$1;

    const-string v1, "mDelayedShow"

    invoke-direct {v0, p0, v1}, Lcom/android/mail/ui/AbstractConversationViewFragment$1;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mDelayedShow:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/mail/ui/AbstractConversationViewFragment$2;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractConversationViewFragment$2;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    return-void
.end method

.method static synthetic access$102(Lcom/android/mail/ui/AbstractConversationViewFragment;J)J
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mLoadingShownTime:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/android/mail/ui/AbstractConversationViewFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mProgressView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->dismiss(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/mail/ui/AbstractConversationViewFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/mail/ui/AbstractConversationViewFragment;)Lcom/android/mail/browse/MessageCursor;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mCursor:Lcom/android/mail/browse/MessageCursor;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/mail/ui/AbstractConversationViewFragment;Lcom/android/mail/browse/MessageCursor;)Lcom/android/mail/browse/MessageCursor;
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;
    .param p1    # Lcom/android/mail/browse/MessageCursor;

    iput-object p1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mCursor:Lcom/android/mail/browse/MessageCursor;

    return-object p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mail/ui/AbstractConversationViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/mail/ui/AbstractConversationViewFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractConversationViewFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onError()V

    return-void
.end method

.method private animateDismiss(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->enableHardwareLayer(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f050003

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    new-instance v1, Lcom/android/mail/ui/AbstractConversationViewFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment$4;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private dismiss(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mLoadingShownTime:J

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mProgressView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->animateDismiss(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private static isConversationViewModeSet(Lcom/android/mail/providers/Account;)Z
    .locals 2
    .param p0    # Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v0, v0, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeBasicArgs(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Landroid/os/Bundle;
    .locals 2
    .param p0    # Lcom/android/mail/providers/Account;
    .param p1    # Lcom/android/mail/providers/Folder;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "folder"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method private onError()V
    .locals 3

    sget-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "CVF: visible conv has no messages, exiting conv mode"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->popOut()V

    return-void
.end method

.method private popOut()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mail/ui/AbstractConversationViewFragment$5;

    const-string v2, "popOut"

    invoke-direct {v1, p0, v2}, Lcom/android/mail/ui/AbstractConversationViewFragment$5;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private showAutoFitPrompt()V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    if-ne v5, v4, :cond_2

    move v1, v4

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->isUserVisible()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v5}, Lcom/android/mail/ui/AbstractConversationViewFragment;->isConversationViewModeSet(Lcom/android/mail/providers/Account;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/webkit/WebView;->canScrollHorizontally(I)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->canScrollHorizontally(I)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    move v2, v4

    :goto_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "AutoFitPromptDialogFragment"

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    if-eqz v5, :cond_4

    move v0, v4

    :goto_2
    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->updateSettingsUri:Landroid/net/Uri;

    invoke-static {v3}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->newInstance(Landroid/net/Uri;)Lcom/android/mail/ui/AutoFitPromptDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "AutoFitPromptDialogFragment"

    invoke-virtual {v3, v4, v5}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method protected dismissLoadingStatus()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->dismissLoadingStatus(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected dismissLoadingStatus(Ljava/lang/Runnable;)V
    .locals 6
    .param p1    # Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mLoadingShownTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mDelayedShow:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->dismiss(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mLoadingShownTime:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    sget v2, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinShowTime:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment;->dismiss(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/mail/ui/AbstractConversationViewFragment$3;

    const-string v4, "dismissLoadingStatus"

    invoke-direct {v3, p0, v4, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment$3;-><init>(Lcom/android/mail/ui/AbstractConversationViewFragment;Ljava/lang/String;Ljava/lang/Runnable;)V

    sget v4, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinShowTime:I

    int-to-long v4, v4

    sub-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public getAccount()Lcom/android/mail/providers/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method public getContactInfoSource()Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mContactLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$ContactLoaderCallbacks;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getConversation()Lcom/android/mail/providers/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getListController()Lcom/android/mail/ui/ConversationUpdater;
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMessageCursor()Lcom/android/mail/browse/MessageCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mCursor:Lcom/android/mail/browse/MessageCursor;

    return-object v0
.end method

.method public getMessageLoaderCallbacks()Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mMessageLoaderCallbacks:Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoaderCallbacks;

    return-object v0
.end method

.method protected getNewViewState()Lcom/android/mail/ui/ConversationViewState;
    .locals 1

    new-instance v0, Lcom/android/mail/ui/ConversationViewState;

    invoke-direct {v0}, Lcom/android/mail/ui/ConversationViewState;-><init>()V

    return-object v0
.end method

.method public getSubjectRemainder(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getSubjectDisplayChanger()Lcom/android/mail/ui/SubjectDisplayChanger;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-interface {v0, p1}, Lcom/android/mail/ui/SubjectDisplayChanger;->getUnshownSubject(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected abstract getWebView()Landroid/webkit/WebView;
.end method

.method public instantiateProgressIndicators(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f080060

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    const v0, 0x7f080061

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mProgressView:Landroid/view/View;

    return-void
.end method

.method public isUserVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    return v0
.end method

.method protected markUnread()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mSuppressMarkingViewed:Z

    return-void
.end method

.method protected abstract onAccountChanged(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Account;)V
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/mail/ui/ControllableActivity;

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ConversationViewFragment expects only a ControllableActivity tocreate it. Cannot proceed."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object v1, v0

    check-cast v1, Lcom/android/mail/ui/ControllableActivity;

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/mail/FormattedDateBuilder;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/android/mail/FormattedDateBuilder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    goto :goto_0
.end method

.method protected onConversationSeen()V
    .locals 8

    const/4 v7, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    if-nez v0, :cond_0

    sget-object v2, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "ignoring onConversationSeen for conv=%s"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v5, v5, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/ConversationViewState;->setInfoForConversation(Lcom/android/mail/providers/Conversation;)V

    iget-boolean v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mSuppressMarkingViewed:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v2}, Lcom/android/mail/providers/Conversation;->isViewed()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/mail/browse/MessageCursor;->isConversationRead()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v2

    new-array v3, v5, [Lcom/android/mail/providers/Conversation;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    aput-object v4, v3, v7

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3, v5, v5}, Lcom/android/mail/ui/ConversationUpdater;->markConversationsRead(Ljava/util/Collection;ZZ)V

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/mail/browse/MessageCursor;->markMessagesRead()V

    :cond_2
    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getListHandler()Lcom/android/mail/ui/ConversationListCallbacks;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-interface {v2, v3}, Lcom/android/mail/ui/ConversationListCallbacks;->onConversationSeen(Lcom/android/mail/providers/Conversation;)V

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->showAutoFitPrompt()V

    goto :goto_0
.end method

.method public abstract onConversationUpdated(Lcom/android/mail/providers/Conversation;)V
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Account;

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    const-string v1, "conversation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Conversation;

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    const-string v1, "folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Folder;

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mFolder:Lcom/android/mail/providers/Folder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "x-thread://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    iget-wide v2, v2, Lcom/android/mail/providers/Conversation;->id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBaseUri:Ljava/lang/String;

    iput-boolean v4, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mEnableContentReadySignal:Z

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onCreate in ConversationViewFragment (this=%s)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0, v5}, Lcom/android/mail/ui/AbstractConversationViewFragment;->setHasOptionsMenu(Z)V

    if-eqz p1, :cond_0

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_VIEW_STATE:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/ConversationViewState;

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_USER_VISIBLE:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getNewViewState()Lcom/android/mail/ui/ConversationViewState;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const v0, 0x7f08011c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mChangeFoldersMenuItem:Landroid/view/MenuItem;

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    return-void
.end method

.method public onFoldersClicked()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mChangeFoldersMenuItem:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "unable to open \'change folders\' dialog for a conversation"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mChangeFoldersMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ControllableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method protected abstract onMessageCursorLoadFinished(Landroid/content/Loader;Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/android/mail/browse/MessageCursor;",
            "Lcom/android/mail/browse/MessageCursor;",
            ")V"
        }
    .end annotation
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->isUserVisible()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ACVF ignoring onOptionsItemSelected b/c userVisibleHint is false. f=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v0

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    invoke-static {p0}, Lcom/android/mail/utils/Utils;->dumpFragment(Landroid/app/Fragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->markUnread()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08011d
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_VIEW_STATE:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mViewState:Lcom/android/mail/ui/ConversationViewState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    sget-object v0, Lcom/android/mail/ui/AbstractConversationViewFragment;->BUNDLE_USER_VISIBLE:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public abstract onUserVisibleHintChanged()V
.end method

.method public setExtraUserVisibleHint(Z)V
    .locals 6
    .param p1    # Z

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "in CVF.setHint, val=%s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    if-eq v1, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageCursor;->isLoaded()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onError()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onUserVisibleHintChanged()V

    goto :goto_0
.end method

.method protected showLoadingStatus()V
    .locals 5

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mUserVisible:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinDelay:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinDelay:I

    const v1, 0x7f0b002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinShowTime:I

    :cond_1
    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mBackgroundView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mDelayedShow:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mDelayedShow:Ljava/lang/Runnable;

    sget v3, Lcom/android/mail/ui/AbstractConversationViewFragment;->sMinDelay:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-super {p0}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/mail/ui/AbstractConversationViewFragment;->LOG_TAG:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conv="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
