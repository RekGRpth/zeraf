.class Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;
.super Ljava/lang/Object;
.source "AbstractActivityController.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/AbstractActivityController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConversationListLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/android/mail/browse/ConversationCursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mail/ui/AbstractActivityController;


# direct methods
.method private constructor <init>(Lcom/android/mail/ui/AbstractActivityController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/ui/AbstractActivityController$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/AbstractActivityController;
    .param p2    # Lcom/android/mail/ui/AbstractActivityController$1;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/mail/browse/ConversationCursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/ui/ConversationCursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v1, v1, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v2, v2, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v3, v3, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v3, v3, Lcom/android/mail/providers/Folder;->conversationListUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v4, v4, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v4, v4, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/mail/ui/ConversationCursorLoader;-><init>(Landroid/app/Activity;Lcom/android/mail/providers/Account;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 8
    .param p2    # Lcom/android/mail/browse/ConversationCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/mail/browse/ConversationCursor;",
            ">;",
            "Lcom/android/mail/browse/ConversationCursor;",
            ")V"
        }
    .end annotation

    const/4 v7, 0x1

    sget-object v3, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "IN AAC.ConversationCursor.onLoadFinished, data=%s loader=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    aput-object p1, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    const/4 v4, 0x0

    # invokes: Lcom/android/mail/ui/AbstractActivityController;->destroyPending(Lcom/android/mail/ui/DestructiveAction;)V
    invoke-static {v3, v4}, Lcom/android/mail/ui/AbstractActivityController;->access$800(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/ui/DestructiveAction;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iput-object p2, v3, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v3, v3, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationCursor;->addListener(Lcom/android/mail/browse/ConversationCursor$ConversationListener;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v3, v3, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationPositionTracker;->onCursorUpdated()V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;
    invoke-static {v3}, Lcom/android/mail/ui/AbstractActivityController;->access$900(Lcom/android/mail/ui/AbstractActivityController;)Landroid/database/DataSetObservable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/DataSetObservable;->notifyChanged()V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/android/mail/ui/AbstractActivityController;->access$1000(Lcom/android/mail/ui/AbstractActivityController;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/AbstractActivityController$LoadFinishedCallback;

    invoke-interface {v0}, Lcom/android/mail/ui/AbstractActivityController$LoadFinishedCallback;->onLoadFinished()V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/android/mail/ui/AbstractActivityController;->access$1000(Lcom/android/mail/ui/AbstractActivityController;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v3}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # invokes: Lcom/android/mail/ui/AbstractActivityController;->isFragmentVisible(Landroid/app/Fragment;)Z
    invoke-static {v3, v1}, Lcom/android/mail/ui/AbstractActivityController;->access$1100(Lcom/android/mail/ui/AbstractActivityController;Landroid/app/Fragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v3, v7}, Lcom/android/mail/ui/AbstractActivityController;->informCursorVisiblity(Z)V

    :cond_1
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # invokes: Lcom/android/mail/ui/AbstractActivityController;->perhapsShowFirstSearchResult()V
    invoke-static {v3}, Lcom/android/mail/ui/AbstractActivityController;->access$1200(Lcom/android/mail/ui/AbstractActivityController;)V

    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->onLoadFinished(Landroid/content/Loader;Lcom/android/mail/browse/ConversationCursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/mail/browse/ConversationCursor;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IN AAC.ConversationCursor.onLoaderReset, data=%s loader=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v4, v4, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationCursor;->removeListener(Lcom/android/mail/browse/ConversationCursor$ConversationListener;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, v0, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->onCursorUpdated()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;->this$0:Lcom/android/mail/ui/AbstractActivityController;

    # getter for: Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;
    invoke-static {v0}, Lcom/android/mail/ui/AbstractActivityController;->access$900(Lcom/android/mail/ui/AbstractActivityController;)Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    :cond_0
    return-void
.end method
