.class public Lcom/android/mail/ui/SwipeableListView;
.super Landroid/widget/ListView;
.source "SwipeableListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/mail/ui/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;,
        Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

.field private mEnableSwipe:Z

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mScrolling:Z

.field private mSwipeAction:I

.field private mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

.field private mSwipedListener:Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/SwipeableListView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/SwipeableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/SwipeableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v2, p0, Lcom/android/mail/ui/SwipeableListView;->mEnableSwipe:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v5, v0

    new-instance v0, Lcom/android/mail/ui/SwipeHelper;

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/ui/SwipeHelper;-><init>(Landroid/content/Context;ILcom/android/mail/ui/SwipeHelper$Callback;FF)V

    iput-object v0, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

    invoke-virtual {p0, p0}, Lcom/android/mail/ui/SwipeableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method private getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/AnimatedAdapter;

    return-object v0
.end method


# virtual methods
.method public canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z
    .locals 1
    .param p1    # Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {p1}, Lcom/android/mail/ui/SwipeableItemView;->canChildBeDismissed()Z

    move-result v0

    return v0
.end method

.method public cancelDismissCounter()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->cancelDismissCounter()V

    :cond_0
    return-void
.end method

.method public commitDestructiveActions(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->commitLeaveBehindItems(Z)V

    :cond_0
    return-void
.end method

.method public destroyItems(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)Z
    .locals 5
    .param p2    # Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;",
            ")Z"
        }
    .end annotation

    const/4 v1, 0x0

    if-nez p1, :cond_0

    sget-object v2, Lcom/android/mail/ui/SwipeableListView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "SwipeableListView.destroyItems: null conversations."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v2, Lcom/android/mail/ui/SwipeableListView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "SwipeableListView.destroyItems: Cannot destroy: adapter is null."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/android/mail/ui/AnimatedAdapter;->swipeDelete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public dismissChild(Lcom/android/mail/browse/ConversationItemView;)V
    .locals 18
    .param p1    # Lcom/android/mail/browse/ConversationItemView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/mail/ui/SwipeableListView;->getContext()Landroid/content/Context;

    move-result-object v5

    new-instance v12, Lcom/android/mail/ui/ToastBarOperation;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/mail/ui/SwipeableListView;->mSwipeAction:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v12, v14, v15, v0, v1}, Lcom/android/mail/ui/ToastBarOperation;-><init>(IIIZ)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/ConversationItemView;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/android/mail/browse/ConversationItemView;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/android/mail/ui/SwipeableListView;->findConversation(Lcom/android/mail/browse/ConversationItemView;Lcom/android/mail/providers/Conversation;)I

    move-result v15

    iput v15, v14, Lcom/android/mail/providers/Conversation;->position:I

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v14, v6, Lcom/android/mail/providers/Conversation;->position:I

    invoke-virtual {v2, v6, v12, v14}, Lcom/android/mail/ui/AnimatedAdapter;->setupLeaveBehind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ToastBarOperation;I)Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v2}, Lcom/android/mail/ui/AnimatedAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    check-cast v4, Lcom/android/mail/browse/ConversationCursor;

    invoke-static {v6}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/mail/ui/SwipeableListView;->mSwipeAction:I

    packed-switch v14, :pswitch_data_0

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mSwipedListener:Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mSwipedListener:Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;

    invoke-interface {v14, v7}, Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;->onListItemSwiped(Ljava/util/Collection;)V

    :cond_2
    invoke-virtual {v2}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v14}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v14, v6}, Lcom/android/mail/ui/ConversationSelectionSet;->contains(Lcom/android/mail/providers/Conversation;)Z

    move-result v14

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v6}, Lcom/android/mail/ui/ConversationSelectionSet;->toggle(Lcom/android/mail/browse/ConversationItemView;Lcom/android/mail/providers/Conversation;)V

    invoke-virtual {v6}, Lcom/android/mail/providers/Conversation;->isMostlyDead()Z

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v14}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_0

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/mail/ui/SwipeableListView;->commitDestructiveActions(Z)V

    goto :goto_0

    :pswitch_0
    new-instance v8, Lcom/android/mail/ui/FolderOperation;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mFolder:Lcom/android/mail/providers/Folder;

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-direct {v8, v14, v15}, Lcom/android/mail/ui/FolderOperation;-><init>(Lcom/android/mail/providers/Folder;Ljava/lang/Boolean;)V

    invoke-virtual {v6}, Lcom/android/mail/providers/Conversation;->getRawFolders()Ljava/util/List;

    move-result-object v14

    invoke-static {v14}, Lcom/android/mail/providers/Folder;->hashMapForFolders(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v11

    iget-object v14, v8, Lcom/android/mail/ui/FolderOperation;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v14, v14, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v11, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v14

    invoke-static {v14}, Lcom/android/mail/providers/FolderList;->copyOf(Ljava/util/Collection;)Lcom/android/mail/providers/FolderList;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/android/mail/providers/Conversation;->setRawFolders(Lcom/android/mail/providers/FolderList;)V

    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/ui/SwipeableListView;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v14, v14, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v14, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, v9, v3, v13}, Lcom/android/mail/browse/ConversationCursor;->addFolderUpdates(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v14

    invoke-virtual {v4, v14, v13}, Lcom/android/mail/browse/ConversationCursor;->addTargetFolders(Ljava/util/Collection;Landroid/content/ContentValues;)V

    invoke-static {v6}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v14

    invoke-virtual {v4, v5, v14, v13}, Lcom/android/mail/browse/ConversationCursor;->mostlyDestructiveUpdate(Landroid/content/Context;Ljava/util/Collection;Landroid/content/ContentValues;)I

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {v4, v5, v7}, Lcom/android/mail/browse/ConversationCursor;->mostlyArchive(Landroid/content/Context;Ljava/util/Collection;)I

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {v4, v5, v7}, Lcom/android/mail/browse/ConversationCursor;->mostlyDelete(Landroid/content/Context;Ljava/util/Collection;)I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x7f080118
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public enableSwipe(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/ui/SwipeableListView;->mEnableSwipe:Z

    return-void
.end method

.method public findConversation(Lcom/android/mail/browse/ConversationItemView;Lcom/android/mail/providers/Conversation;)I
    .locals 13
    .param p1    # Lcom/android/mail/browse/ConversationItemView;
    .param p2    # Lcom/android/mail/providers/Conversation;

    const/4 v12, -0x1

    iget v8, p2, Lcom/android/mail/providers/Conversation;->position:I

    iget-wide v1, p2, Lcom/android/mail/providers/Conversation;->id:J

    if-ne v8, v12, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/mail/ui/SwipeableListView;->getPositionForView(Landroid/view/View;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    :cond_0
    :goto_0
    if-ne v8, v12, :cond_1

    const/4 v7, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getChildCount()I

    move-result v9

    if-ge v7, v9, :cond_1

    invoke-virtual {p0, v7}, Lcom/android/mail/ui/SwipeableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v9, v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-eqz v9, :cond_2

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    invoke-virtual {v0}, Lcom/android/mail/browse/SwipeableConversationItemView;->getSwipeableItemView()Lcom/android/mail/browse/ConversationItemView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/mail/browse/ConversationItemView;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v4

    iget-wide v5, v4, Lcom/android/mail/providers/Conversation;->id:J

    cmp-long v9, v5, v1

    if-nez v9, :cond_2

    move v8, v7

    :cond_1
    return v8

    :catch_0
    move-exception v3

    const/4 v8, -0x1

    sget-object v9, Lcom/android/mail/ui/SwipeableListView;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Exception finding position; using alternate strategy"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getChildCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/SwipeableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v4

    if-gt v3, v4, :cond_0

    instance-of v4, v2, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-eqz v4, :cond_2

    check-cast v2, Lcom/android/mail/browse/SwipeableConversationItemView;

    invoke-virtual {v2}, Lcom/android/mail/browse/SwipeableConversationItemView;->getSwipeableItemView()Lcom/android/mail/browse/ConversationItemView;

    move-result-object v2

    :cond_2
    :goto_1
    return-object v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getLastSwipedItem()Lcom/android/mail/ui/LeaveBehindItem;
    .locals 2

    invoke-direct {p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->getLastLeaveBehindItem()Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSwipeAction()I
    .locals 1

    iget v0, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeAction:I

    return v0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/SwipeableListView;->requestDisallowInterceptTouchEvent(Z)V

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/android/mail/browse/ConversationItemView;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SwipeableConversationItemView;->addBackground(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SwipeableConversationItemView;->setBackgroundVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->cancelDismissCounter()V

    return-void
.end method

.method public onChildDismissed(Lcom/android/mail/ui/SwipeableItemView;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/SwipeableItemView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/android/mail/ui/SwipeableItemView;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/widget/ListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    iget-object v2, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

    invoke-virtual {v2, v0}, Lcom/android/mail/ui/SwipeHelper;->setDensityScale(F)V

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    int-to-float v1, v2

    iget-object v2, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

    invoke-virtual {v2, v1}, Lcom/android/mail/ui/SwipeHelper;->setPagingTouchSlop(F)V

    return-void
.end method

.method public onDragCancelled(Lcom/android/mail/ui/SwipeableItemView;)V
    .locals 3
    .param p1    # Lcom/android/mail/ui/SwipeableItemView;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/android/mail/browse/ConversationItemView;

    if-eqz v2, :cond_0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/SwipeableConversationItemView;

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/mail/browse/SwipeableConversationItemView;->removeBackground()V

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/SwipeableListView;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->startDismissCounter()V

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->cancelFadeOutLastLeaveBehindItemText()V

    :cond_2
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # Landroid/graphics/Rect;

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v0, "MailBlankFragment"

    const-string v1, "START CLF-ListView.onFocusChanged layoutRequested=%s root.layoutRequested=%s"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->isLayoutRequested()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->isLayoutRequested()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    const-string v0, "MailBlankFragment"

    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1}, Ljava/lang/Error;-><init>()V

    const-string v2, "FINISH CLF-ListView.onFocusChanged layoutRequested=%s root.layoutRequested=%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->isLayoutRequested()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0}, Lcom/android/mail/ui/SwipeableListView;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->isLayoutRequested()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/android/mail/ui/SwipeableListView;->mScrolling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/ui/SwipeableListView;->mEnableSwipe:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScroll()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/SwipeableListView;->commitDestructiveActions(Z)V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/SwipeableListView;->mScrolling:Z

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/SwipeableListView;->mScrolling:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/android/mail/ui/SwipeableListView;->mEnableSwipe:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeHelper:Lcom/android/mail/ui/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # J

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/SwipeableListView;->commitDestructiveActions(Z)V

    return v0
.end method

.method public setCurrentFolder(Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/SwipeableListView;->mFolder:Lcom/android/mail/providers/Folder;

    return-void
.end method

.method public setSelectionSet(Lcom/android/mail/ui/ConversationSelectionSet;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ConversationSelectionSet;

    iput-object p1, p0, Lcom/android/mail/ui/SwipeableListView;->mConvSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    return-void
.end method

.method public setSwipeAction(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipeAction:I

    return-void
.end method

.method public setSwipedListener(Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;

    iput-object p1, p0, Lcom/android/mail/ui/SwipeableListView;->mSwipedListener:Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;

    return-void
.end method
