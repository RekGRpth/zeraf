.class public abstract Lcom/android/mail/ui/AbstractActivityController;
.super Ljava/lang/Object;
.source "AbstractActivityController.java"

# interfaces
.implements Lcom/android/mail/ui/ActivityController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;,
        Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;,
        Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;,
        Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;,
        Lcom/android/mail/ui/AbstractActivityController$ConversationAction;,
        Lcom/android/mail/ui/AbstractActivityController$LoadFinishedCallback;
    }
.end annotation


# static fields
.field protected static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected isLoaderInitialized:Z

.field protected mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountObservers:Landroid/database/DataSetObservable;

.field protected mActionBarView:Lcom/android/mail/ui/MailActionBarView;

.field protected final mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private mAsyncRefreshTask:Lcom/android/mail/ui/AsyncRefreshTask;

.field mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

.field protected final mContext:Landroid/content/Context;

.field protected mConvListContext:Lcom/android/mail/ConversationListContext;

.field protected mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

.field private final mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/ui/AbstractActivityController$LoadFinishedCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mConversationListObservable:Landroid/database/DataSetObservable;

.field private mConversationListRefreshTask:Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;

.field private final mCurrentAccountUris:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentConversation:Lcom/android/mail/providers/Conversation;

.field private mDestroyed:Z

.field private mDetachedConvUri:Landroid/net/Uri;

.field private mDialogAction:I

.field private mDialogFromSelectedSet:Z

.field private mDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mEnableShareIntents:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field protected mFolder:Lcom/android/mail/providers/Folder;

.field private mFolderChanged:Z

.field private final mFolderItemUpdateDelayMs:I

.field private mFolderListFolder:Lcom/android/mail/providers/Folder;

.field private final mFolderObservable:Landroid/database/DataSetObservable;

.field protected mFolderSyncTask:Lcom/android/mail/ui/AsyncRefreshTask;

.field private final mFragmentManager:Landroid/app/FragmentManager;

.field protected mHandler:Landroid/os/Handler;

.field private mHaveSearchResults:Z

.field private mIsDragHappening:Z

.field private final mIsTablet:Z

.field private final mListCursorCallbacks:Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;

.field private mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

.field protected mPagerController:Lcom/android/mail/browse/ConversationPagerController;

.field private mPendingDestruction:Lcom/android/mail/ui/DestructiveAction;

.field protected final mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

.field private final mRecentFolderObservers:Landroid/database/DataSetObservable;

.field private mRecentsDataUpdated:Z

.field protected mResolver:Landroid/content/ContentResolver;

.field private mSafeToModifyFragments:Z

.field private final mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

.field private mShowUndoBarDelay:I

.field protected mToastBar:Lcom/android/mail/ui/ActionableToastBar;

.field protected final mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

.field private final mUndoNotificationObserver:Landroid/database/DataSetObserver;

.field private final mUpOrBackHandlers:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

.field protected final mViewMode:Lcom/android/mail/ui/ViewMode;

.field private mWaitFragment:Lcom/android/mail/ui/WaitFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/mail/ui/MailActivity;Lcom/android/mail/ui/ViewMode;)V
    .locals 4
    .param p1    # Lcom/android/mail/ui/MailActivity;
    .param p2    # Lcom/android/mail/ui/ViewMode;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderChanged:Z

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mHandler:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/android/mail/ui/AbstractActivityController;->isLoaderInitialized:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSafeToModifyFragments:Z

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentAccountUris:Ljava/util/Set;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$1;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/AbstractActivityController$1;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$2;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/AbstractActivityController$2;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccountObservers:Landroid/database/DataSetObservable;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$3;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/AbstractActivityController$3;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderObservers:Landroid/database/DataSetObservable;

    new-instance v1, Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-direct {v1}, Lcom/android/mail/ui/ConversationSelectionSet;-><init>()V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;

    invoke-direct {v1, p0, v3}, Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;-><init>(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/ui/AbstractActivityController$1;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mListCursorCallbacks:Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;

    new-instance v1, Landroid/database/DataSetObservable;

    invoke-direct {v1}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderObservable:Landroid/database/DataSetObservable;

    iput-boolean v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mHaveSearchResults:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$4;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/AbstractActivityController$4;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mUndoNotificationObserver:Landroid/database/DataSetObserver;

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    iput-object p2, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {p1}, Lcom/android/mail/ui/MailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/mail/ui/RecentFolderList;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/mail/ui/RecentFolderList;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    new-instance v1, Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/ConversationPositionTracker;-><init>(Lcom/android/mail/ui/ConversationPositionTracker$Callbacks;)V

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/ConversationSelectionSet;->addObserver(Lcom/android/mail/ui/ConversationSetObserver;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderItemUpdateDelayMs:I

    const v1, 0x7f0b0025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mShowUndoBarDelay:I

    invoke-virtual {p1}, Lcom/android/mail/ui/MailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/VeiledAddressMatcher;->newInstance(Landroid/content/res/Resources;)Lcom/android/mail/utils/VeiledAddressMatcher;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    return-void
.end method

.method static synthetic access$100(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Ljava/util/Set;
    .param p3    # [B

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController;->doMarkConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mail/ui/AbstractActivityController;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/mail/ui/AbstractActivityController;Landroid/app/Fragment;)Z
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Landroid/app/Fragment;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->isFragmentVisible(Landroid/app/Fragment;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/android/mail/ui/AbstractActivityController;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->perhapsShowFirstSearchResult()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/mail/ui/AbstractActivityController;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->startAsyncRefreshTask(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/providers/Account;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->promptUserForAuthentication(Lcom/android/mail/providers/Account;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/mail/ui/AbstractActivityController;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->showStorageErrorDialog()V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/mail/ui/AbstractActivityController;Landroid/content/DialogInterface$OnClickListener;I)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Landroid/content/DialogInterface$OnClickListener;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->setListener(Landroid/content/DialogInterface$OnClickListener;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;ZZZ)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Ljava/util/Collection;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/mail/ui/AbstractActivityController;->markConversationsRead(Ljava/util/Collection;ZZZ)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/mail/ui/AbstractActivityController;)I
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    iget v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mShowUndoBarDelay:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/mail/ui/AbstractActivityController;)Lcom/android/mail/ui/ConversationSelectionSet;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/ui/DestructiveAction;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;
    .param p1    # Lcom/android/mail/ui/DestructiveAction;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->destroyPending(Lcom/android/mail/ui/DestructiveAction;)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mail/ui/AbstractActivityController;)Landroid/database/DataSetObservable;
    .locals 1
    .param p0    # Lcom/android/mail/ui/AbstractActivityController;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method private accountsUpdated(Landroid/database/Cursor;)Z
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v3, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentAccountUris:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v1, 0x0

    :cond_2
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentAccountUris:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private attachActionBar()V
    .locals 6

    const/4 v5, -0x1

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const/16 v2, 0x1a

    const/16 v1, 0x12

    const/16 v3, 0x12

    const/16 v4, 0x1a

    invoke-virtual {v0, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v3}, Lcom/android/mail/ui/MailActionBarView;->attach()V

    :cond_0
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/ViewMode;->addListener(Lcom/android/mail/ui/ViewMode$ModeChangeListener;)V

    return-void
.end method

.method private cancelRefreshTask()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListRefreshTask:Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListRefreshTask:Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;

    invoke-virtual {v0}, Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;->cancel()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListRefreshTask:Lcom/android/mail/ui/AbstractActivityController$RefreshTimerTask;

    :cond_0
    return-void
.end method

.method private final changeFolder(Lcom/android/mail/providers/Folder;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->commitDestructiveActions(Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v0}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->setListContext(Lcom/android/mail/providers/Folder;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->resetActionBarIcon()V

    return-void
.end method

.method private clearDetachedMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->revertChoiceMode()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "AAC.clearDetachedMode(): CLF = null on tablet!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private confirmAndDelete(ILjava/util/Collection;ZI)V
    .locals 4
    .param p1    # I
    .param p3    # Z
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;ZI)V"
        }
    .end annotation

    const/4 v3, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1, v3}, Lcom/android/mail/ui/AbstractActivityController;->makeDialogListener(IZ)V

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v2, p4, v3}, Lcom/android/mail/utils/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/browse/ConfirmDialogFragment;->newInstance(Ljava/lang/CharSequence;)Lcom/android/mail/browse/ConfirmDialogFragment;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/mail/browse/ConfirmDialogFragment;->displayDialog(Landroid/app/FragmentManager;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v3, p2, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0
.end method

.method private final destroyPending(Lcom/android/mail/ui/DestructiveAction;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/DestructiveAction;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPendingDestruction:Lcom/android/mail/ui/DestructiveAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPendingDestruction:Lcom/android/mail/ui/DestructiveAction;

    invoke-interface {v0}, Lcom/android/mail/ui/DestructiveAction;->performAction()V

    :cond_0
    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mPendingDestruction:Lcom/android/mail/ui/DestructiveAction;

    return-void
.end method

.method private disableNotifications()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/android/mail/ui/SuppressNotificationReceiver;->activate(Landroid/content/Context;Lcom/android/mail/ui/AbstractActivityController;)Z

    return-void
.end method

.method private disableNotificationsOnAccountChange(Lcom/android/mail/providers/Account;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    invoke-virtual {v0}, Lcom/android/mail/ui/SuppressNotificationReceiver;->activated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SuppressNotificationReceiver;->notificationsDisabledForAccount(Lcom/android/mail/providers/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    invoke-virtual {v0}, Lcom/android/mail/ui/SuppressNotificationReceiver;->deactivate()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/android/mail/ui/SuppressNotificationReceiver;->activate(Landroid/content/Context;Lcom/android/mail/ui/AbstractActivityController;)Z

    :cond_0
    return-void
.end method

.method private displayAutoAdvanceDialogAndPerformAction(Ljava/lang/Runnable;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e0007

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0e0009

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    const v4, 0x7f09011b

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_1

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$10;

    invoke-direct {v1, p0, v3, p1}, Lcom/android/mail/ui/AbstractActivityController$10;-><init>(Lcom/android/mail/ui/AbstractActivityController;[Ljava/lang/String;Ljava/lang/Runnable;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v4}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f09011a

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private doMarkConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Conversation;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;[B)V"
        }
    .end annotation

    const/4 v8, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/android/mail/providers/Conversation;->getNumMessages()I

    move-result v4

    if-le v4, v3, :cond_1

    if-lez v0, :cond_1

    if-ge v0, v4, :cond_1

    move v2, v3

    :goto_1
    sget-object v5, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v6, "markConversationMessagesUnread(conv=%s), numMessages=%d, unreadCount=%d, subsetIsUnread=%b"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-nez v2, :cond_2

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, ". . doing full mark unread"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/android/mail/ui/AbstractActivityController;->markConversationsRead(Ljava/util/Collection;ZZZ)V

    :goto_2
    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p3}, Lcom/android/mail/providers/ConversationInfo;->fromBlob([B)Lcom/android/mail/providers/ConversationInfo;

    move-result-object v0

    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, ". . doing subset mark unread, originalConversationInfo = %s"

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-static {v2, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v2, p1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    sget-object v4, Lcom/android/mail/providers/UIProvider$ConversationColumns;->READ:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v4, v5}, Lcom/android/mail/browse/ConversationCursor;->setConversationColumn(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v2, p1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    const-string v4, "conversationInfo"

    invoke-virtual {v0, v2, v4, p3}, Lcom/android/mail/browse/ConversationCursor;->setConversationColumn(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez v2, :cond_5

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    sget-object v7, Lcom/android/mail/providers/UIProvider$MessageColumns;->READ:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v7, ". . Adding op: read=0, uri=%s"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v0, v8, v1

    invoke-static {v6, v7, v8}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v5, ". . operations = %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v4, v3, v1

    invoke-static {v0, v5, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$7;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractActivityController$7;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/mail/ui/AbstractActivityController$7;->run(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_2
.end method

.method private enableNotifications()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    invoke-virtual {v0}, Lcom/android/mail/ui/SuppressNotificationReceiver;->deactivate()V

    return-void
.end method

.method private fetchSearchFolder(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "query"

    const-string v2, "query"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method private final getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;
    .locals 1
    .param p1    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;Z)",
            "Lcom/android/mail/ui/DestructiveAction;"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$ConversationAction;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController$ConversationAction;-><init>(Lcom/android/mail/ui/AbstractActivityController;ILjava/util/Collection;Z)V

    return-object v0
.end method

.method private getInternalErrorClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;
    .locals 1

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$17;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractActivityController$17;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    return-object v0
.end method

.method private getRetryClickedListener(Lcom/android/mail/providers/Folder;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$14;

    invoke-direct {v0, p0, p1}, Lcom/android/mail/ui/AbstractActivityController$14;-><init>(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/providers/Folder;)V

    return-object v0
.end method

.method private getSignInClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;
    .locals 1

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$15;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractActivityController$15;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    return-object v0
.end method

.method private getStorageErrorClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;
    .locals 1

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$16;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractActivityController$16;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    return-object v0
.end method

.method private handleDragFromStarred(Lcom/android/mail/providers/Folder;)V
    .locals 7

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "AAC.requestDelete: ListFragment is handling delete."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/android/mail/providers/Conversation;->getRawFolders()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/android/mail/providers/Folder;->hashMapForFolders(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v5

    iget-object v6, p1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v5, v6, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v6, v0, v3, v4, v5}, Lcom/android/mail/browse/ConversationCursor;->getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/android/mail/browse/ConversationCursor;->updateBulkValues(Landroid/content/Context;Ljava/util/Collection;)I

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    :cond_2
    return-void
.end method

.method private handleDropInStarred(Lcom/android/mail/providers/Folder;)V
    .locals 5

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "AAC.requestDelete: ListFragment is handling delete."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const v2, 0x7f08011c

    new-instance v3, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-direct {v3, p0, v0, v4, p1}, Lcom/android/mail/ui/AbstractActivityController$DroppedInStarredAction;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Folder;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/android/mail/ui/ConversationListFragment;->requestDelete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    :cond_0
    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :try_start_0
    const-string v3, "account"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "account"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/providers/Account;->newinstance(Ljava/lang/String;)Lcom/android/mail/providers/Account;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/mail/ui/AbstractActivityController;->setAccount(Lcom/android/mail/providers/Account;)V

    :cond_0
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v3, "conversationUri"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v4}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v4}, Lcom/android/mail/ui/ViewMode;->enterConversationMode()Z

    :goto_1
    const-string v4, "folder"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v0, "folder"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/providers/Folder;->fromString(Ljava/lang/String;)Lcom/android/mail/providers/Folder;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_b

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    move v0, v1

    :goto_2
    if-eqz v3, :cond_5

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "SHOW THE CONVERSATION at %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "conversationUri"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, "conversationUri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_4

    iget v3, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-gez v3, :cond_4

    const/4 v3, 0x0

    iput v3, v0, Lcom/android/mail/providers/Conversation;->position:I

    :cond_4
    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;)V
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :cond_5
    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->loadAccountInbox()V

    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->restartOptionalLoader(I)V

    goto :goto_0

    :cond_7
    :try_start_1
    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v4}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z
    :try_end_1
    .catch Landroid/os/BadParcelableException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Error parsing intent"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_8
    const-string v3, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "account"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    iput-boolean v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mHaveSearchResults:Z

    const-string v2, "query"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    const v4, 0x7f090023

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/provider/SearchRecentSuggestions;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v3, v1}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {v4, v2, v0}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Account;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->setAccount(Lcom/android/mail/providers/Account;)V

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->fetchSearchFolder(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->shouldEnterSearchConvMode()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsConversationMode()Z

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsListMode()Z

    goto :goto_3

    :cond_a
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Missing account extra from search intent.  Finishing"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_2
.end method

.method private inWaitMode()Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/WaitFragment;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private initializeActionBar()V
    .locals 8

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v4}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "android.intent.action.SEARCH"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v7, 0x1

    :goto_1
    if-eqz v7, :cond_2

    const v0, 0x7f04005b

    :goto_2
    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/MailActionBarView;

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/MailActionBarView;->initialize(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ActivityController;Lcom/android/mail/ui/ViewMode;Landroid/app/ActionBar;Lcom/android/mail/ui/RecentFolderList;)V

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    :cond_2
    const v0, 0x7f040009

    goto :goto_2
.end method

.method private isDragging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsDragHappening:Z

    return v0
.end method

.method private final isFragmentVisible(Landroid/app/Fragment;)Z
    .locals 1
    .param p1    # Landroid/app/Fragment;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isValidFragment(Landroid/app/Fragment;)Z
    .locals 1
    .param p0    # Landroid/app/Fragment;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadRecentFolders(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/RecentFolderList;->loadFromUiProvider(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentsDataUpdated:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    goto :goto_0
.end method

.method private markConversationsRead(Ljava/util/Collection;ZZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;ZZZ)V"
        }
    .end annotation

    const/4 v6, 0x1

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "performing markConversationsRead"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-eqz p4, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$9;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController$9;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;ZZZ)V

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/AbstractActivityController;->showNextConversation(Ljava/util/Collection;Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Lcom/android/mail/providers/UIProvider$ConversationColumns;->READ:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v4, "suppress_undo"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    if-eqz p3, :cond_2

    const-string v4, "viewed"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_2
    iget-object v4, v0, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    if-eqz v4, :cond_3

    invoke-virtual {v4, p2}, Lcom/android/mail/providers/ConversationInfo;->markRead(Z)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "conversationInfo"

    invoke-virtual {v4}, Lcom/android/mail/providers/ConversationInfo;->toBlob()[B

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_3
    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    const/4 v5, 0x2

    invoke-virtual {v4, v0, v5, v3}, Lcom/android/mail/browse/ConversationCursor;->getOperationForConversation(Lcom/android/mail/providers/Conversation;ILandroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-boolean p2, v0, Lcom/android/mail/providers/Conversation;->read:Z

    if-eqz p3, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/providers/Conversation;->markViewed()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/android/mail/browse/ConversationCursor;->updateBulkValues(Landroid/content/Context;Ljava/util/Collection;)I

    goto :goto_0
.end method

.method private final perhapsShowFirstSearchResult()V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.SEARCH"

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v4}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationCursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mHaveSearchResults:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->shouldShowFirstConversation()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v1, v3}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    new-instance v0, Lcom/android/mail/providers/Conversation;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput v3, v0, Lcom/android/mail/providers/Conversation;->position:I

    invoke-virtual {p0, v0, v2}, Lcom/android/mail/ui/AbstractActivityController;->onConversationSelected(Lcom/android/mail/providers/Conversation;Z)V

    goto :goto_1
.end method

.method private promptUserForAuthentication(Lcom/android/mail/providers/Account;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/mail/providers/Account;->reauthenticationIntentUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->isEmpty(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p1, Lcom/android/mail/providers/Account;->reauthenticationIntentUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2}, Lcom/android/mail/ui/ControllableActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method private final registerDestructiveAction(Lcom/android/mail/ui/DestructiveAction;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/DestructiveAction;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->destroyPending(Lcom/android/mail/ui/DestructiveAction;)V

    return-void
.end method

.method private requestFolderRefresh()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAsyncRefreshTask:Lcom/android/mail/ui/AsyncRefreshTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAsyncRefreshTask:Lcom/android/mail/ui/AsyncRefreshTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AsyncRefreshTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/mail/ui/AsyncRefreshTask;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->refreshUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/android/mail/ui/AsyncRefreshTask;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAsyncRefreshTask:Lcom/android/mail/ui/AsyncRefreshTask;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAsyncRefreshTask:Lcom/android/mail/ui/AsyncRefreshTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AsyncRefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method private requestUpdate(Ljava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V
    .locals 0
    .param p2    # Lcom/android/mail/ui/DestructiveAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/DestructiveAction;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lcom/android/mail/ui/DestructiveAction;->performAction()V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    return-void
.end method

.method private restartOptionalLoader(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, p1, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method private final restoreSelectedConversations(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "saved-selected-set"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ConversationSelectionSet;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/ConversationSelectionSet;->putAll(Lcom/android/mail/ui/ConversationSelectionSet;)V

    goto :goto_0
.end method

.method private setAccount(Lcom/android/mail/providers/Account;)V
    .locals 5

    const/4 v4, 0x0

    if-nez p1, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1}, Ljava/lang/Error;-><init>()V

    const-string v2, "AAC ignoring null (presumably invalid) account restoration"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "AbstractActivityController.setAccount(): account = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->restartOptionalLoader(I)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->disableNotificationsOnAccountChange(Lcom/android/mail/providers/Account;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->restartOptionalLoader(I)V

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/MailAppProvider;->setLastViewedAccount(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1}, Ljava/lang/Error;-><init>()V

    const-string v2, "AAC ignoring account with null settings."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccountObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->perhapsEnterWaitMode()V

    goto :goto_0
.end method

.method private final setHasFolderChanged(Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v1, v1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderChanged:Z

    goto :goto_0
.end method

.method private setListContext(Lcom/android/mail/providers/Folder;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->updateFolder(Lcom/android/mail/providers/Folder;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, v1, p2}, Lcom/android/mail/ConversationListContext;->forSearchQuery(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Ljava/lang/String;)Lcom/android/mail/ConversationListContext;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    :goto_0
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->cancelRefreshTask()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, v1}, Lcom/android/mail/ConversationListContext;->forFolder(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Lcom/android/mail/ConversationListContext;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    goto :goto_0
.end method

.method private setListener(Landroid/content/DialogInterface$OnClickListener;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface$OnClickListener;
    .param p2    # I

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    iput p2, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    return-void
.end method

.method private final showConversation(Lcom/android/mail/providers/Conversation;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;Z)V

    return-void
.end method

.method private showNextConversation(Ljava/util/Collection;Ljava/lang/Runnable;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/lang/Runnable;",
            ")Z"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {p1, v0}, Lcom/android/mail/providers/Conversation;->contains(Ljava/util/Collection;Lcom/android/mail/providers/Conversation;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-virtual {v0}, Lcom/android/mail/providers/Settings;->getAutoAdvanceSetting()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    if-eqz v3, :cond_2

    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->displayAutoAdvanceDialogAndPerformAction(Ljava/lang/Runnable;)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    const/4 v0, 0x3

    :cond_3
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-virtual {v3, v0, p1}, Lcom/android/mail/ui/ConversationPositionTracker;->getNextConversation(ILjava/util/Collection;)Lcom/android/mail/providers/Conversation;

    move-result-object v0

    sget-object v3, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "showNextConversation: showing %s next."

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;)V

    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private showStorageErrorDialog()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "SyncErrorDialogFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/mail/browse/SyncErrorDialogFragment;->newInstance()Lcom/android/mail/browse/SyncErrorDialogFragment;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "SyncErrorDialogFragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startAsyncRefreshTask(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderSyncTask:Lcom/android/mail/ui/AsyncRefreshTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderSyncTask:Lcom/android/mail/ui/AsyncRefreshTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AsyncRefreshTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/mail/ui/AsyncRefreshTask;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/mail/ui/AsyncRefreshTask;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderSyncTask:Lcom/android/mail/ui/AsyncRefreshTask;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderSyncTask:Lcom/android/mail/ui/AsyncRefreshTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AsyncRefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateAccounts(Landroid/database/Cursor;)Z
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/android/mail/providers/Account;->getAllAccounts(Landroid/database/Cursor;)[Lcom/android/mail/providers/Account;

    move-result-object v5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentAccountUris:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v0, v5, v4

    sget-object v7, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v8, "updateAccounts(%s)"

    new-array v9, v2, [Ljava/lang/Object;

    aput-object v0, v9, v3

    invoke-static {v7, v8, v9}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v7, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentAccountUris:Ljava/util/Set;

    iget-object v8, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v7, :cond_9

    iget-object v7, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v8, v8, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    aget-object v0, v5, v3

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v1, v4}, Lcom/android/mail/providers/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/AbstractActivityController;->onAccountChanged(Lcom/android/mail/providers/Account;)V

    :cond_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0, v5}, Lcom/android/mail/ui/MailActionBarView;->setAccounts([Lcom/android/mail/providers/Account;)V

    array-length v0, v5

    if-lez v0, :cond_6

    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mail/providers/MailAppProvider;->getLastViewedAccount()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    array-length v7, v5

    move v4, v3

    :goto_4
    if-ge v4, v7, :cond_7

    aget-object v1, v5, v4

    iget-object v8, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v0, v2

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    :cond_6
    move v0, v3

    goto :goto_0

    :cond_7
    move-object v1, v0

    move v0, v2

    goto :goto_3

    :cond_8
    move-object v1, v0

    move v0, v3

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method private final updateConversationListFragment()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->isFragmentVisible(Landroid/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/AbstractActivityController;->informCursorVisiblity(Z)V

    :cond_0
    return-void
.end method

.method private updateFolder(Lcom/android/mail/providers/Folder;)V
    .locals 10
    .param p1    # Lcom/android/mail/providers/Folder;

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/mail/providers/Folder;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    sget-object v4, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/Error;

    invoke-direct {v5}, Ljava/lang/Error;-><init>()V

    const-string v6, "AAC.setFolder(%s): Bad input"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v4, v5, v6, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v4}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v5, "AAC.setFolder(%s): Input matches mFolder"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v4, v5, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-nez v4, :cond_4

    move v1, v2

    :goto_1
    sget-object v4, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v5, "AbstractActivityController.setFolder(%s)"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, p1, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    aput-object v6, v2, v3

    invoke-static {v4, v5, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->setHasFolderChanged(Lcom/android/mail/providers/Folder;)V

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/MailActionBarView;->setFolder(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {v0, v9}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v2

    if-nez v2, :cond_5

    invoke-virtual {v0, v9, v8, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    :goto_2
    if-nez v1, :cond_3

    invoke-virtual {v0, v7}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v2

    if-nez v2, :cond_6

    :cond_3
    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mListCursorCallbacks:Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;

    invoke-virtual {v0, v7, v8, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v9, v8, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v7}, Landroid/app/LoaderManager;->destroyLoader(I)V

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mListCursorCallbacks:Lcom/android/mail/ui/AbstractActivityController$ConversationListLoaderCallbacks;

    invoke-virtual {v0, v7, v8, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method private updateRecentFolderList()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/ui/RecentFolderList;->touchFolder(Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Account;)V

    :cond_0
    return-void
.end method

.method private updateWaitMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "wait-fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/WaitFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/WaitFragment;->updateAccount(Lcom/android/mail/providers/Account;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addUpOrBackHandler(Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final assignFolder(Ljava/util/Collection;Ljava/util/Collection;ZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/ui/FolderOperation;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;ZZ)V"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {p1, v0}, Lcom/android/mail/ui/FolderOperation;->isDestructive(Ljava/util/Collection;Lcom/android/mail/providers/Folder;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v1

    :goto_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onFolderChangesCommit: isDestructive = %b"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0, v2, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    iput-boolean v1, v0, Lcom/android/mail/providers/Conversation;->localDeleteOnUpdate:Z

    goto :goto_1

    :cond_0
    move v3, v6

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    invoke-virtual {p0, v6, p2, v0}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    :goto_2
    return-void

    :cond_2
    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController;->getFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/android/mail/ui/AbstractActivityController;->requestUpdate(Ljava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_2
.end method

.method public commitDestructiveActions(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/ConversationListFragment;->commitDestructiveActions(Z)V

    :cond_0
    return-void
.end method

.method public delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/DestructiveAction;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$12;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController$12;-><init>(Lcom/android/mail/ui/AbstractActivityController;ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    invoke-direct {p0, p2, v0}, Lcom/android/mail/ui/AbstractActivityController;->showNextConversation(Ljava/util/Collection;Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "AAC.requestDelete: ListFragment is handling delete."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/mail/ui/ConversationListFragment;->requestDelete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :cond_1
    invoke-interface {p3}, Lcom/android/mail/ui/DestructiveAction;->performAction()V

    goto :goto_0
.end method

.method protected disableCabMode()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->commitDestructiveActions(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

    invoke-virtual {v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->deactivate()V

    :cond_0
    return-void
.end method

.method public disablePagerUpdates()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationPagerController;->stopListening()V

    return-void
.end method

.method public abstract doesActionChangeConversationListVisibility(I)Z
.end method

.method protected enableCabMode()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

    invoke-virtual {v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->activate()V

    :cond_0
    return-void
.end method

.method public executeSearch(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->collapseSearch()V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1, v0}, Lcom/android/mail/ui/ControllableActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected final exitCabMode()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    return-void
.end method

.method public exitSearchMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    :cond_0
    return-void
.end method

.method public getAccount()Lcom/android/mail/providers/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method public final getBatchAction(I)Lcom/android/mail/ui/DestructiveAction;
    .locals 3
    .param p1    # I

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$ConversationAction;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/android/mail/ui/AbstractActivityController$ConversationAction;-><init>(Lcom/android/mail/ui/AbstractActivityController;ILjava/util/Collection;Z)V

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->registerDestructiveAction(Lcom/android/mail/ui/DestructiveAction;)V

    return-object v0
.end method

.method public final getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    return-object v0
.end method

.method protected getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "tag-conversation-list"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/ui/AbstractActivityController;->isValidFragment(Landroid/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/mail/ui/ConversationListFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentAccount()Lcom/android/mail/providers/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method public getCurrentConversation()Lcom/android/mail/providers/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    return-object v0
.end method

.method public getCurrentListContext()Lcom/android/mail/ConversationListContext;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    return-object v0
.end method

.method public final getDeferredBatchAction(I)Lcom/android/mail/ui/DestructiveAction;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    return-object v0
.end method

.method public final getDeferredFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/ui/FolderOperation;",
            ">;ZZZ)",
            "Lcom/android/mail/ui/DestructiveAction;"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;

    const v7, 0x7f08011c

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Ljava/util/Collection;ZZZILcom/android/mail/ui/AbstractActivityController$1;)V

    return-object v0
.end method

.method public final getDeferredRemoveFolder(Ljava/util/Collection;Lcom/android/mail/providers/Folder;ZZZ)Lcom/android/mail/ui/DestructiveAction;
    .locals 9
    .param p2    # Lcom/android/mail/providers/Folder;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/providers/Folder;",
            "ZZZ)",
            "Lcom/android/mail/ui/DestructiveAction;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/mail/ui/FolderOperation;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/android/mail/ui/FolderOperation;-><init>(Lcom/android/mail/providers/Folder;Ljava/lang/Boolean;)V

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;

    const v7, 0x7f080119

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/android/mail/ui/AbstractActivityController$FolderDestruction;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;Ljava/util/Collection;ZZZILcom/android/mail/ui/AbstractActivityController$1;)V

    return-object v0
.end method

.method public getFolder()Lcom/android/mail/providers/Folder;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    return-object v0
.end method

.method public final getFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;
    .locals 1
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/ui/FolderOperation;",
            ">;ZZZ)",
            "Lcom/android/mail/ui/DestructiveAction;"
        }
    .end annotation

    invoke-virtual/range {p0 .. p5}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->registerDestructiveAction(Lcom/android/mail/ui/DestructiveAction;)V

    return-object v0
.end method

.method protected getFolderListFragment()Lcom/android/mail/ui/FolderListFragment;
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "tag-folder-list"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/ui/AbstractActivityController;->isValidFragment(Landroid/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/mail/ui/FolderListFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHelpContext()Ljava/lang/String;
    .locals 3

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const v0, 0x7f09001e

    :goto_0
    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :pswitch_0
    const v0, 0x7f09001d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public getHierarchyFolder()Lcom/android/mail/providers/Folder;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderListFolder:Lcom/android/mail/providers/Folder;

    return-object v0
.end method

.method public getListener()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method public getRecentFolders()Lcom/android/mail/ui/RecentFolderList;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    return-object v0
.end method

.method public getSelectedSet()Lcom/android/mail/ui/ConversationSelectionSet;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    return-object v0
.end method

.method public getSubjectDisplayChanger()Lcom/android/mail/ui/SubjectDisplayChanger;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    return-object v0
.end method

.method protected final getUndoClickedListener(Lcom/android/mail/ui/AnimatedAdapter;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;
    .locals 1
    .param p1    # Lcom/android/mail/ui/AnimatedAdapter;

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$13;

    invoke-direct {v0, p0, p1}, Lcom/android/mail/ui/AbstractActivityController$13;-><init>(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/ui/AnimatedAdapter;)V

    return-object v0
.end method

.method public getVeiledAddressMatcher()Lcom/android/mail/utils/VeiledAddressMatcher;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    return-object v0
.end method

.method protected final getWaitFragment()Lcom/android/mail/ui/WaitFragment;
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "wait-fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/WaitFragment;

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mWaitFragment:Lcom/android/mail/ui/WaitFragment;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mWaitFragment:Lcom/android/mail/ui/WaitFragment;

    return-object v0
.end method

.method protected abstract handleBackPress()Z
.end method

.method public handleDrop(Landroid/view/DragEvent;Lcom/android/mail/providers/Folder;)V
    .locals 9
    .param p1    # Landroid/view/DragEvent;
    .param p2    # Lcom/android/mail/providers/Folder;

    const/4 v5, 0x7

    const/4 v7, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->supportsDrag(Landroid/view/DragEvent;Lcom/android/mail/providers/Folder;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p2, Lcom/android/mail/providers/Folder;->type:I

    if-ne v0, v5, :cond_1

    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->handleDropInStarred(Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget v0, v0, Lcom/android/mail/providers/Folder;->type:I

    if-ne v0, v5, :cond_2

    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->handleDragFromStarred(Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v1

    new-instance v0, Lcom/android/mail/ui/FolderOperation;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v0, p2, v5}, Lcom/android/mail/ui/FolderOperation;-><init>(Lcom/android/mail/providers/Folder;Ljava/lang/Boolean;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0}, Lcom/android/mail/providers/Folder;->isViewAll()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v3, v4

    :goto_1
    if-eqz v3, :cond_3

    new-instance v0, Lcom/android/mail/ui/FolderOperation;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v0, v5, v8}, Lcom/android/mail/ui/FolderOperation;-><init>(Lcom/android/mail/providers/Folder;Ljava/lang/Boolean;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController;->getFolderChange(Ljava/util/Collection;Ljava/util/Collection;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v6

    if-eqz v3, :cond_5

    invoke-virtual {p0, v7, v1, v6}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :cond_4
    move v3, v7

    goto :goto_1

    :cond_5
    invoke-interface {v6}, Lcom/android/mail/ui/DestructiveAction;->performAction()V

    goto :goto_0
.end method

.method protected abstract handleUpPress()Z
.end method

.method protected abstract hideOrRepositionToastBar(Z)V
.end method

.method protected hideWaitForInitialization()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mWaitFragment:Lcom/android/mail/ui/WaitFragment;

    return-void
.end method

.method protected declared-synchronized informCursorVisiblity(Z)V
    .locals 2
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderChanged:Z

    invoke-static {v0, p1, v1}, Lcom/android/mail/utils/Utils;->setConversationCursorVisibility(Landroid/database/Cursor;ZZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderChanged:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isAnimating()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->isAnimating()Z

    move-result v2

    :cond_0
    return v2
.end method

.method protected abstract isConversationListVisible()Z
.end method

.method public isDestroyed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDestroyed:Z

    return v0
.end method

.method public isInitialConversationLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationPagerController;->isInitialConversationLoading()Z

    move-result v0

    return v0
.end method

.method public loadAccountInbox()V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->restartOptionalLoader(I)V

    return-void
.end method

.method public makeDialogListener(IZ)V
    .locals 5

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v1

    iput p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    iput-boolean p2, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogFromSelectedSet:Z

    new-instance v2, Lcom/android/mail/ui/AbstractActivityController$18;

    invoke-direct {v2, p0, p1, v0, v1}, Lcom/android/mail/ui/AbstractActivityController$18;-><init>(Lcom/android/mail/ui/AbstractActivityController;ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    iput-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    return-void

    :cond_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Will act upon %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public markConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Conversation;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;[B)V"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;)V

    iput-boolean v5, p1, Lcom/android/mail/providers/Conversation;->read:Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "markConversationMessagesUnread(id=%d), deferring"

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v3, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$6;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController$6;-><init>(Lcom/android/mail/ui/AbstractActivityController;Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "markConversationMessagesUnread(id=%d), performing"

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v3, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController;->doMarkConversationMessagesUnread(Lcom/android/mail/providers/Conversation;Ljava/util/Set;[B)V

    goto :goto_0
.end method

.method public markConversationsRead(Ljava/util/Collection;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;ZZ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "markConversationsRead(targets=%s), deferring"

    invoke-interface {p1}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListLoadFinishedCallbacks:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/mail/ui/AbstractActivityController$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/mail/ui/AbstractActivityController$8;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/util/Collection;ZZ)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/mail/ui/AbstractActivityController;->markConversationsRead(Ljava/util/Collection;ZZZ)V

    goto :goto_0
.end method

.method public onAccessibilityStateChanged()V
    .locals 2

    invoke-static {}, Lcom/android/mail/browse/ConversationItemViewModel;->onAccessibilityUpdated()V

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetInvalidated()V

    :cond_0
    return-void
.end method

.method public onAccountChanged(Lcom/android/mail/providers/Account;)V
    .locals 7
    .param p1    # Lcom/android/mail/providers/Account;

    const/4 v0, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v5, :cond_2

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    iget-object v5, p1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v6, v6, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_0
    :goto_1
    if-nez v0, :cond_4

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {p1, v5}, Lcom/android/mail/providers/Account;->settingsDiffer(Lcom/android/mail/providers/Account;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_1

    :cond_4
    if-nez p1, :cond_5

    sget-object v5, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v6, "AAC.onAccountChanged(null) called."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_5
    iget-object v1, p1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/android/mail/ui/AbstractActivityController$5;

    invoke-direct {v6, p0, v1}, Lcom/android/mail/ui/AbstractActivityController$5;-><init>(Lcom/android/mail/ui/AbstractActivityController;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    if-eqz v0, :cond_6

    invoke-virtual {p0, v4}, Lcom/android/mail/ui/AbstractActivityController;->commitDestructiveActions(Z)V

    :cond_6
    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->setAccount(Lcom/android/mail/providers/Account;)V

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->cancelRefreshTask()V

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->loadAccountInbox()V

    :cond_7
    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v4, :cond_1

    sget-object v4, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, v5, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v5, v5, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.EDIT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v4, v4, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v4, v3}, Lcom/android/mail/ui/ControllableActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x0

    const/4 v1, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v1, Lcom/android/mail/providers/Folder;->refreshUri:Landroid/net/Uri;

    :cond_2
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->startAsyncRefreshTask(Landroid/net/Uri;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAnimationEnd(Lcom/android/mail/ui/AnimatedAdapter;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "null ConversationCursor in onAnimationEnd"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isRefreshReady()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Stopped animating: try sync"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->onRefreshReady()V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isRefreshRequired()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Stopped animating: refresh"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->refresh()Z

    :cond_3
    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentsDataUpdated:Z

    if-eqz v0, :cond_4

    iput-boolean v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentsDataUpdated:Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    :cond_4
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getFolderListFragment()Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderListFragment;->onAnimationEnd()V

    goto :goto_0
.end method

.method public final onBackPressed()Z
    .locals 3

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;

    invoke-interface {v0}, Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;->onBackPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->handleBackPress()Z

    move-result v2

    goto :goto_0
.end method

.method public onConversationListVisibilityChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->informCursorVisiblity(Z)V

    return-void
.end method

.method public onConversationSeen(Lcom/android/mail/providers/Conversation;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationPagerController;->onConversationSeen(Lcom/android/mail/providers/Conversation;)V

    return-void
.end method

.method public final onConversationSelected(Lcom/android/mail/providers/Conversation;Z)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Z

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->commitDestructiveActions(Z)V

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;Z)V

    return-void
.end method

.method public onConversationVisibilityChanged(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)Z
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->initializeActionBar()V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Lcom/android/mail/ui/ControllableActivity;->setDefaultKeyMode(I)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mResolver:Landroid/content/ContentResolver;

    new-instance v3, Lcom/android/mail/ui/SuppressNotificationReceiver;

    invoke-direct {v3}, Lcom/android/mail/ui/SuppressNotificationReceiver;-><init>()V

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mNewEmailReceiver:Lcom/android/mail/ui/SuppressNotificationReceiver;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/RecentFolderList;->initialize(Lcom/android/mail/ui/ControllableActivity;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    invoke-virtual {v3, p0}, Lcom/android/mail/utils/VeiledAddressMatcher;->initialize(Lcom/android/mail/ui/AccountController;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3, p0}, Lcom/android/mail/ui/ViewMode;->addListener(Lcom/android/mail/ui/ViewMode$ModeChangeListener;)V

    new-instance v3, Lcom/android/mail/browse/ConversationPagerController;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-direct {v3, v4, p0}, Lcom/android/mail/browse/ConversationPagerController;-><init>(Lcom/android/mail/ui/RestrictedActivity;Lcom/android/mail/ui/ActivityController;)V

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v4, 0x7f0800c5

    invoke-interface {v3, v4}, Lcom/android/mail/ui/ControllableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/mail/ui/ActionableToastBar;

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->attachActionBar()V

    invoke-static {}, Lcom/android/mail/ui/FolderSelectionDialog;->setDialogDismissed()V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz p1, :cond_4

    const-string v3, "saved-account"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "saved-account"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/android/mail/providers/Account;

    invoke-direct {p0, v3}, Lcom/android/mail/ui/AbstractActivityController;->setAccount(Lcom/android/mail/providers/Account;)V

    :cond_0
    const-string v3, "saved-folder"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "saved-folder"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    const-string v3, "saved-query"

    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/android/mail/ui/AbstractActivityController;->setListContext(Lcom/android/mail/providers/Folder;Ljava/lang/String;)V

    :cond_1
    const-string v3, "saved-action"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "saved-action"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    :cond_2
    const-string v3, "saved-action-from-selected"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogFromSelectedSet:Z

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3, p1}, Lcom/android/mail/ui/ViewMode;->handleRestore(Landroid/os/Bundle;)V

    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    const/4 v3, 0x1

    return v3

    :cond_4
    if-eqz v1, :cond_3

    invoke-direct {p0, v1}, Lcom/android/mail/ui/AbstractActivityController;->handleIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Loader returned unexpected id: %d"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getAccountsUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/android/mail/providers/UIProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderItemUpdateDelayMs:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->recentFolderListUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->recentFolderListUri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-static {v0}, Lcom/android/mail/providers/Settings;->getDefaultInboxUri(Lcom/android/mail/providers/Settings;)Landroid/net/Uri;

    move-result-object v2

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v0, Lcom/android/mail/providers/Account;->folderListUri:Landroid/net/Uri;

    :cond_1
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Loading the default inbox: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v0, v1, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-eqz v2, :cond_0

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const-string v1, "query"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/mail/providers/Folder;->forSearchResults(Lcom/android/mail/providers/Account;Ljava/lang/String;Landroid/content/Context;)Landroid/content/CursorLoader;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->getOptionsMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1, p1}, Lcom/android/mail/ui/MailActionBarView;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/4 v1, 0x1

    return v1
.end method

.method public final onDataSetChanged()V
    .locals 2

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->updateConversationListFragment()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/ConversationSelectionSet;->validateAgainstCursor(Lcom/android/mail/browse/ConversationCursor;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0, p0}, Lcom/android/mail/browse/ConversationCursor;->removeListener(Lcom/android/mail/browse/ConversationCursor$ConversationListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationPagerController;->onDestroy()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->onDestroy()V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderList:Lcom/android/mail/ui/RecentFolderList;

    invoke-virtual {v0}, Lcom/android/mail/ui/RecentFolderList;->destroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDestroyed:Z

    return-void
.end method

.method public onFolderChanged(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/AbstractActivityController;->changeFolder(Lcom/android/mail/providers/Folder;Ljava/lang/String;)V

    return-void
.end method

.method public onFolderSelected(Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    return-void
.end method

.method public onFooterViewErrorActionClick(Lcom/android/mail/providers/Folder;I)V
    .locals 4
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # I

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/android/mail/providers/Folder;->refreshUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->refreshUri:Landroid/net/Uri;

    :cond_1
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->startAsyncRefreshTask(Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-direct {p0, v1}, Lcom/android/mail/ui/AbstractActivityController;->promptUserForAuthentication(Lcom/android/mail/providers/Account;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->showStorageErrorDialog()V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/Utils;->sendFeedback(Lcom/android/mail/ui/FeedbackEnabledActivity;Lcom/android/mail/providers/Account;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onFooterViewLoadMoreClick(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->loadMoreUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->loadMoreUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->startAsyncRefreshTask(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Received null cursor from loader id: %d"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "accounts_loaded"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    :cond_2
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/mail/providers/MailAppProvider;->getNoAccountIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2, v1, v0}, Lcom/android/mail/ui/ControllableActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->accountsUpdated(Landroid/database/Cursor;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->isLoaderInitialized:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_1

    :cond_4
    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->updateAccounts(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->isLoaderInitialized:Z

    goto :goto_0

    :pswitch_2
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lcom/android/mail/providers/Account;

    invoke-direct {v2, p2}, Lcom/android/mail/providers/Account;-><init>(Landroid/database/Cursor;)V

    iget-object v3, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iput-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "AbstractActivityController.onLoadFinished(): mAccount = %s"

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, v5, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    aput-object v5, v0, v1

    invoke-static {v2, v4, v0}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-static {v0, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccountObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    :cond_5
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->perhapsEnterWaitMode()V

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Got update for account: %s with current account: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    aput-object v2, v5, v1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->restartOptionalLoader(I)V

    goto/16 :goto_0

    :pswitch_3
    if-eqz p2, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Lcom/android/mail/providers/Folder;

    invoke-direct {v2, p2}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    sget-object v3, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "FOLDER STATUS = %d"

    new-array v0, v0, [Ljava/lang/Object;

    iget v5, v2, Lcom/android/mail/providers/Folder;->syncStatus:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-static {v3, v4, v0}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v2}, Lcom/android/mail/ui/AbstractActivityController;->setHasFolderChanged(Lcom/android/mail/providers/Folder;)V

    iput-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    goto/16 :goto_0

    :cond_7
    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Unable to get the folder %s"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    :goto_1
    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_8
    const-string v0, ""

    goto :goto_1

    :pswitch_4
    if-eqz p2, :cond_9

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gt v2, v0, :cond_9

    iget-boolean v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->defaultRecentFolderListUri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Default recents at %s"

    new-array v5, v0, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v3, Lcom/android/mail/ui/AbstractActivityController$1PopulateDefault;

    invoke-direct {v3, p0}, Lcom/android/mail/ui/AbstractActivityController$1PopulateDefault;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    new-array v0, v0, [Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-virtual {v3, v0}, Lcom/android/mail/ui/AbstractActivityController$1PopulateDefault;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Reading recent folders from the cursor."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/android/mail/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, p2}, Lcom/android/mail/ui/AbstractActivityController;->loadRecentFolders(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :pswitch_5
    if-eqz p2, :cond_a

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v0, Lcom/android/mail/providers/Folder;

    invoke-direct {v0, p2}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    goto/16 :goto_0

    :cond_a
    sget-object v2, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Unable to get the account inbox for account %s"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    :goto_2
    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_b
    const-string v0, ""

    goto :goto_2

    :pswitch_6
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v2, Lcom/android/mail/providers/Folder;

    invoke-direct {v2, p2}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p0, v2}, Lcom/android/mail/ui/AbstractActivityController;->updateFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v5, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v5}, Lcom/android/mail/ui/ControllableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "query"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/android/mail/ConversationListContext;->forSearchQuery(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Ljava/lang/String;)Lcom/android/mail/ConversationListContext;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-virtual {p0, v3}, Lcom/android/mail/ui/AbstractActivityController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->invalidateOptionsMenu()V

    iget v2, v2, Lcom/android/mail/providers/Folder;->totalCount:I

    if-lez v2, :cond_c

    :goto_3
    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mHaveSearchResults:Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_3

    :cond_d
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Null or empty cursor returned by LOADER_SEARCH loader"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "AbstractController.onOptionsItemSelected(%d) called."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v9, 0x1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v0, :cond_1

    const/4 v11, 0x0

    :goto_0
    invoke-virtual {p0, v10}, Lcom/android/mail/ui/AbstractActivityController;->doesActionChangeConversationListVisibility(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->commitDestructiveActions(Z)V

    sparse-switch v10, :sswitch_data_0

    const/4 v9, 0x0

    :cond_0
    :goto_2
    return v9

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v11, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_0
    if-eqz v11, :cond_3

    iget-boolean v0, v11, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    if-eqz v0, :cond_3

    const/4 v12, 0x1

    :goto_3
    const v0, 0x7f100005

    invoke-direct {p0, v10, v1, v12, v0}, Lcom/android/mail/ui/AbstractActivityController;->confirmAndDelete(ILjava/util/Collection;ZI)V

    goto :goto_2

    :cond_3
    const/4 v12, 0x0

    goto :goto_3

    :sswitch_1
    const v6, 0x7f080119

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredRemoveFolder(Ljava/util/Collection;Lcom/android/mail/providers/Folder;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    invoke-virtual {p0, v6, v1, v0}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_2

    :sswitch_2
    if-eqz v11, :cond_4

    iget-boolean v0, v11, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    if-eqz v0, :cond_4

    const/4 v12, 0x1

    :goto_4
    const v0, 0x7f100004

    invoke-direct {p0, v10, v1, v12, v0}, Lcom/android/mail/ui/AbstractActivityController;->confirmAndDelete(ILjava/util/Collection;ZI)V

    goto :goto_2

    :cond_4
    const/4 v12, 0x0

    goto :goto_4

    :sswitch_3
    if-eqz v11, :cond_5

    iget-boolean v0, v11, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    if-eqz v0, :cond_5

    const/4 v12, 0x1

    :goto_5
    const v0, 0x7f100006

    invoke-direct {p0, v10, v1, v12, v0}, Lcom/android/mail/ui/AbstractActivityController;->confirmAndDelete(ILjava/util/Collection;ZI)V

    goto :goto_2

    :cond_5
    const/4 v12, 0x0

    goto :goto_5

    :sswitch_4
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v0

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->PRIORITY:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/mail/ui/AbstractActivityController;->updateConversation(Ljava/util/Collection;Ljava/lang/String;I)V

    goto :goto_2

    :sswitch_5
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0}, Lcom/android/mail/providers/Folder;->isImportantOnly()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f08011f

    const v2, 0x7f08011f

    const/4 v3, 0x0

    invoke-direct {p0, v2, v1, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v0

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->PRIORITY:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/mail/ui/AbstractActivityController;->updateConversation(Ljava/util/Collection;Ljava/lang/String;I)V

    goto :goto_2

    :sswitch_6
    const v0, 0x7f080120

    const v2, 0x7f080120

    const/4 v3, 0x0

    invoke-direct {p0, v2, v1, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto/16 :goto_2

    :sswitch_7
    const v0, 0x7f080121

    const v2, 0x7f080121

    const/4 v3, 0x0

    invoke-direct {p0, v2, v1, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto/16 :goto_2

    :sswitch_8
    const v0, 0x7f080122

    const v2, 0x7f080122

    const/4 v3, 0x0

    invoke-direct {p0, v2, v1, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto/16 :goto_2

    :sswitch_9
    const v0, 0x7f080123

    const v2, 0x7f080123

    const/4 v3, 0x0

    invoke-direct {p0, v2, v1, v3}, Lcom/android/mail/ui/AbstractActivityController;->getDeferredAction(ILjava/util/Collection;Z)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/ui/AbstractActivityController;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto/16 :goto_2

    :sswitch_a
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->onUpPressed()Z

    goto/16 :goto_2

    :sswitch_b
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v0, v2}, Lcom/android/mail/compose/ComposeActivity;->compose(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    goto/16 :goto_2

    :sswitch_c
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->showFolderList()V

    goto/16 :goto_2

    :sswitch_d
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->requestFolderRefresh()V

    goto/16 :goto_2

    :sswitch_e
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v0, v2}, Lcom/android/mail/utils/Utils;->showSettings(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    goto/16 :goto_2

    :sswitch_f
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/Utils;->showFolderSettings(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)V

    goto/16 :goto_2

    :sswitch_10
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getHelpContext()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/Utils;->showHelp(Landroid/content/Context;Lcom/android/mail/providers/Account;Ljava/lang/String;)V

    goto/16 :goto_2

    :sswitch_11
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/Utils;->sendFeedback(Lcom/android/mail/ui/FeedbackEnabledActivity;Lcom/android/mail/providers/Account;Z)V

    goto/16 :goto_2

    :sswitch_12
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v0, v2}, Lcom/android/mail/utils/Utils;->showManageFolder(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    goto/16 :goto_2

    :sswitch_13
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0}, Lcom/android/mail/providers/Conversation;->listOf(Lcom/android/mail/providers/Conversation;)Ljava/util/Collection;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    move-object v4, p0

    invoke-static/range {v2 .. v7}, Lcom/android/mail/ui/FolderSelectionDialog;->getInstance(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/ui/ConversationUpdater;Ljava/util/Collection;ZLcom/android/mail/providers/Folder;)Lcom/android/mail/ui/FolderSelectionDialog;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/android/mail/ui/FolderSelectionDialog;->show()V

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f080008 -> :sswitch_12
        0x7f080036 -> :sswitch_b
        0x7f080115 -> :sswitch_e
        0x7f080116 -> :sswitch_11
        0x7f080117 -> :sswitch_10
        0x7f080118 -> :sswitch_0
        0x7f080119 -> :sswitch_1
        0x7f08011a -> :sswitch_2
        0x7f08011b -> :sswitch_3
        0x7f08011c -> :sswitch_13
        0x7f08011e -> :sswitch_4
        0x7f08011f -> :sswitch_5
        0x7f080120 -> :sswitch_6
        0x7f080121 -> :sswitch_7
        0x7f080122 -> :sswitch_8
        0x7f080123 -> :sswitch_9
        0x7f080125 -> :sswitch_d
        0x7f080126 -> :sswitch_f
        0x7f080127 -> :sswitch_c
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->isLoaderInitialized:Z

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->enableNotifications()V

    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/MailActionBarView;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onRefreshReady()V
    .locals 5

    const/4 v4, 0x0

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Received refresh ready callback for folder %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget v0, v0, Lcom/android/mail/providers/Folder;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDestroyed:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "ignoring onRefreshReady on destroyed AAC"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    return-void

    :cond_0
    const-string v0, "-1"

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->sync()V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationPositionTracker;->onCursorUpdated()V

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->perhapsShowFirstSearchResult()V

    goto :goto_1
.end method

.method public final onRefreshRequired()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onRefreshRequired: delay until animating done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isRefreshRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->refresh()Z

    goto :goto_0
.end method

.method public onRestart()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "SyncErrorDialogFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/ActionableToastBar;->hide(Z)V

    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "saved-detached-conv-uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    const-string v0, "saved-conversation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "saved-conversation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-gez v1, :cond_0

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/mail/providers/Conversation;->position:I

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;)V

    :cond_1
    const-string v0, "saved-toast-bar-op"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "saved-toast-bar-op"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ToastBarOperation;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/mail/ui/ToastBarOperation;->getType()I

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V

    :cond_2
    :goto_0
    const-string v0, "saved-hierarchical-folder"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/android/mail/providers/Folder;->fromString(Ljava/lang/String;)Lcom/android/mail/providers/Folder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderListFolder:Lcom/android/mail/providers/Folder;

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_4
    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->restoreSelectedConversations(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogFromSelectedSet:Z

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/ui/AbstractActivityController;->makeDialogListener(IZ)V

    :cond_5
    return-void

    :cond_6
    invoke-virtual {v0}, Lcom/android/mail/ui/ToastBarOperation;->getType()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v0, v2}, Lcom/android/mail/ui/AbstractActivityController;->onError(Lcom/android/mail/providers/Folder;Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->disableNotifications()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSafeToModifyFragments:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/ViewMode;->handleSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    const-string v0, "saved-account"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_1

    const-string v0, "saved-folder"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v0}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "saved-query"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    iget-object v1, v1, Lcom/android/mail/ConversationListContext;->searchQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isConversationMode()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "saved-conversation"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "saved-selected-set"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_4
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0}, Lcom/android/mail/ui/ActionableToastBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "saved-toast-bar-op"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v1}, Lcom/android/mail/ui/ActionableToastBar;->getOperation()Lcom/android/mail/ui/ToastBarOperation;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_6
    iget v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const-string v0, "saved-action"

    iget v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogAction:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "saved-action-from-selected"

    iget-boolean v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDialogFromSelectedSet:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_7
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    if-eqz v0, :cond_8

    const-string v0, "saved-detached-conv-uri"

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSafeToModifyFragments:Z

    const-string v1, "saved-hierarchical-folder"

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderListFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderListFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0}, Lcom/android/mail/providers/Folder;->toString(Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSetChanged(Lcom/android/mail/ui/ConversationSelectionSet;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ConversationSelectionSet;

    return-void
.end method

.method public onSetEmpty()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/AbstractActivityController;->setListener(Landroid/content/DialogInterface$OnClickListener;I)V

    return-void
.end method

.method public onSetPopulated(Lcom/android/mail/ui/ConversationSelectionSet;)V
    .locals 3
    .param p1    # Lcom/android/mail/ui/ConversationSelectionSet;

    new-instance v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-direct {v0, v1, p1, v2}, Lcom/android/mail/browse/SelectedConversationsActionMenu;-><init>(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;)V

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCabActionMenu:Lcom/android/mail/browse/SelectedConversationsActionMenu;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isConversationMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->enableCabMode()V

    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSafeToModifyFragments:Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mUndoNotificationObserver:Landroid/database/DataSetObserver;

    invoke-static {v0}, Lcom/android/mail/utils/NotificationActionUtils;->registerUndoNotificationObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mEnableShareIntents:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mEnableShareIntents:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mUndoNotificationObserver:Landroid/database/DataSetObserver;

    invoke-static {v0}, Lcom/android/mail/utils/NotificationActionUtils;->unregisterUndoNotificationObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/ActionableToastBar;->isEventInToastBar(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->hideOrRepositionToastBar(Z)V

    :cond_0
    return-void
.end method

.method public final onUpPressed()Z
    .locals 3

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;

    invoke-interface {v0}, Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;->onUpPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->handleUpPress()Z

    move-result v2

    goto :goto_0
.end method

.method public onViewModeChanged(I)V
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/android/mail/ui/ViewMode;->isConversationMode(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AbstractActivityController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->resetActionBarIcon()V

    :cond_1
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/AbstractActivityController;->informCursorVisiblity(Z)V

    :cond_0
    return-void
.end method

.method final perhapsEnterWaitMode()V
    .locals 3

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v2}, Lcom/android/mail/providers/Account;->isAccountInitializationRequired()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->showWaitForInitialization()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->inWaitMode()Z

    move-result v0

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v2}, Lcom/android/mail/providers/Account;->isAccountSyncRequired()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->updateWaitMode()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->showWaitForInitialization()V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->hideWaitForInitialization()V

    goto :goto_0
.end method

.method public final refreshConversationList()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->requestListRefresh()V

    goto :goto_0
.end method

.method public registerAccountObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccountObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public registerConversationListObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public registerConversationLoadedObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationPagerController;->registerConversationLoadedObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public registerFolderObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public registerRecentFolderObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public removeUpOrBackHandler(Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/UpOrBackController$UpOrBackHandler;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mUpOrBackHandlers:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method protected abstract resetActionBarIcon()V
.end method

.method protected safeToModifyFragments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mSafeToModifyFragments:Z

    return v0
.end method

.method public setCurrentConversation(Lcom/android/mail/providers/Conversation;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    iget-object v1, p1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->clearDetachedMode()V

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mTracker:Lcom/android/mail/ui/ConversationPositionTracker;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/ConversationPositionTracker;->initialize(Lcom/android/mail/providers/Conversation;)V

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/MailActionBarView;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-object v1, v1, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/MailActionBarView;->setSubject(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->invalidateOptionsMenu()V

    :cond_2
    return-void
.end method

.method public setDetachedMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationListFragment;->setChoiceNone()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-object v0, v0, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mDetachedConvUri:Landroid/net/Uri;

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsTablet:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "AAC.setDetachedMode(): CLF = null!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public setHierarchyFolder(Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderListFolder:Lcom/android/mail/providers/Folder;

    return-void
.end method

.method protected final shouldEnterSearchConvMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mHaveSearchResults:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showConversation(Lcom/android/mail/providers/Conversation;Z)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Z

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->updateRecentFolderList()V

    return-void
.end method

.method public showConversationList(Lcom/android/mail/ConversationListContext;)V
    .locals 0
    .param p1    # Lcom/android/mail/ConversationListContext;

    return-void
.end method

.method protected final showErrorToast(Lcom/android/mail/providers/Folder;Z)V
    .locals 8

    const/4 v6, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, v4}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    iget v3, p1, Lcom/android/mail/providers/Folder;->lastSyncResult:I

    and-int/lit8 v0, v3, 0xf

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    shr-int/lit8 v1, v3, 0x4

    and-int/lit8 v0, v1, 0x1

    if-eqz v0, :cond_2

    move v0, v6

    :goto_1
    if-nez v0, :cond_3

    iget v0, p1, Lcom/android/mail/providers/Folder;->syncWindow:I

    if-gtz v0, :cond_1

    and-int/lit8 v0, v1, 0x4

    if-eqz v0, :cond_3

    :cond_1
    move v0, v6

    :goto_2
    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->getRetryClickedListener(Lcom/android/mail/providers/Folder;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    const v5, 0x7f090012

    :goto_3
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    const v2, 0x7f02002c

    iget-object v7, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v7}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/android/mail/utils/Utils;->getSyncStatusText(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v7, Lcom/android/mail/ui/ToastBarOperation;

    invoke-direct {v7, v6, v4, v6, v4}, Lcom/android/mail/ui/ToastBarOperation;-><init>(IIIZ)V

    move v6, p2

    invoke-virtual/range {v0 .. v7}, Lcom/android/mail/ui/ActionableToastBar;->show(Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;ILjava/lang/CharSequence;ZIZLcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    move v0, v4

    goto :goto_2

    :pswitch_2
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->getSignInClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    const v5, 0x7f090105

    goto :goto_3

    :pswitch_3
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->getStorageErrorClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    const v5, 0x7f090106

    goto :goto_3

    :pswitch_4
    invoke-direct {p0}, Lcom/android/mail/ui/AbstractActivityController;->getInternalErrorClickedListener()Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    const v5, 0x7f090107

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public showNextConversation(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/AbstractActivityController;->showNextConversation(Ljava/util/Collection;Ljava/lang/Runnable;)Z

    return-void
.end method

.method public showWaitForInitialization()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterWaitingForInitializationMode()Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v0}, Lcom/android/mail/ui/WaitFragment;->newInstance(Lcom/android/mail/providers/Account;)Lcom/android/mail/ui/WaitFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mWaitFragment:Lcom/android/mail/ui/WaitFragment;

    return-void
.end method

.method public starMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;Z)V
    .locals 10
    .param p1    # Lcom/android/mail/browse/MessageCursor$ConversationMessage;
    .param p2    # Z

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->starred:Z

    if-ne v2, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p2, p1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->starred:Z

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isConversationStarred()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    move v7, v1

    :goto_1
    invoke-virtual {p1}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v6

    iget-boolean v2, v6, Lcom/android/mail/providers/Conversation;->starred:Z

    if-eq v7, v2, :cond_2

    iput-boolean v7, v6, Lcom/android/mail/providers/Conversation;->starred:Z

    iget-object v2, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v5, v6, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    sget-object v8, Lcom/android/mail/providers/UIProvider$ConversationColumns;->STARRED:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v2, v5, v8, v9}, Lcom/android/mail/browse/ConversationCursor;->setConversationColumn(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    sget-object v2, Lcom/android/mail/providers/UIProvider$MessageColumns;->STARRED:Ljava/lang/String;

    if-eqz p2, :cond_4

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v0, Lcom/android/mail/ui/AbstractActivityController$11;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AbstractActivityController$11;-><init>(Lcom/android/mail/ui/AbstractActivityController;)V

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->uri:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/ui/AbstractActivityController$11;->run(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v7, v0

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_2
.end method

.method public startDragMode()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsDragHappening:Z

    return-void
.end method

.method public startSearch()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "AbstractActivityController.startSearch(): null account"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->expandSearch()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0900b3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public stopDragMode()V
    .locals 4

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/mail/ui/AbstractActivityController;->mIsDragHappening:Z

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isRefreshReady()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Stopped animating: try sync"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->onRefreshReady()V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isRefreshRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Stopped animating: refresh"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->refresh()Z

    :cond_1
    return-void
.end method

.method public supportsDrag(Landroid/view/DragEvent;Lcom/android/mail/providers/Folder;)Z
    .locals 2
    .param p1    # Landroid/view/DragEvent;
    .param p2    # Lcom/android/mail/providers/Folder;

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v1, p2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterAccountObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mAccountObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterConversationListObserver(Landroid/database/DataSetObserver;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "unregisterConversationListObserver called for an observer that hasn\'t been registered"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public unregisterConversationLoadedObserver(Landroid/database/DataSetObserver;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationPagerController;->unregisterConversationLoadedObserver(Landroid/database/DataSetObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "unregisterConversationLoadedObserver called for an observer that hasn\'t been registered"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public unregisterFolderObserver(Landroid/database/DataSetObserver;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mFolderObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/ui/AbstractActivityController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "unregisterFolderObserver called for an observer that hasn\'t been registered"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public unregisterRecentFolderObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mRecentFolderObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public updateConversation(Ljava/util/Collection;Ljava/lang/String;I)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/android/mail/browse/ConversationCursor;->updateInt(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;I)I

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    return-void
.end method

.method public updateConversation(Ljava/util/Collection;Ljava/lang/String;Z)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AbstractActivityController;->mConversationListCursor:Lcom/android/mail/browse/ConversationCursor;

    iget-object v1, p0, Lcom/android/mail/ui/AbstractActivityController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/android/mail/browse/ConversationCursor;->updateBoolean(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;Z)I

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractActivityController;->refreshConversationList()V

    return-void
.end method
