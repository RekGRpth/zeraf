.class public Lcom/android/mail/ui/AnimatedAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "AnimatedAdapter.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/AnimatedAdapter$Listener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static sDismissAllLongDelay:I

.field private static sDismissAllShortDelay:I


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountListener:Lcom/android/mail/providers/AccountObserver;

.field private mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private final mAnimatingViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mail/browse/SwipeableConversationItemView;",
            ">;"
        }
    .end annotation
.end field

.field private final mBatchConversations:Lcom/android/mail/ui/ConversationSelectionSet;

.field private final mContext:Landroid/content/Context;

.field private mCountDown:Ljava/lang/Runnable;

.field private final mDeletingItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mFadeLeaveBehindItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mail/ui/LeaveBehindItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mFooter:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private final mLastDeletingItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected mLastLeaveBehind:J

.field private final mLeaveBehindItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mail/ui/LeaveBehindItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mListView:Lcom/android/mail/ui/SwipeableListView;

.field private mPendingDestruction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

.field private mPriorityMarkersEnabled:Z

.field private final mRefreshAction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

.field private mShowFooter:Z

.field private final mSwipeDeletingItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSwipeEnabled:Z

.field private final mSwipeUndoingItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mUndoingItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllShortDelay:I

    sput v0, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllLongDelay:I

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/AnimatedAdapter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/SwipeableListView;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;
    .param p3    # Lcom/android/mail/ui/ConversationSelectionSet;
    .param p4    # Lcom/android/mail/ui/ControllableActivity;
    .param p5    # Lcom/android/mail/ui/SwipeableListView;

    const/4 v6, 0x0

    const/4 v2, -0x1

    sget-object v4, Lcom/android/mail/providers/UIProvider;->CONVERSATION_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAnimatingViews:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    new-instance v0, Lcom/android/mail/ui/AnimatedAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AnimatedAdapter$1;-><init>(Lcom/android/mail/ui/AnimatedAdapter;)V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mRefreshAction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    new-instance v0, Lcom/android/mail/ui/AnimatedAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/AnimatedAdapter$2;-><init>(Lcom/android/mail/ui/AnimatedAdapter;)V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccountListener:Lcom/android/mail/providers/AccountObserver;

    iput-object p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mBatchConversations:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccountListener:Lcom/android/mail/providers/AccountObserver;

    invoke-interface {p4}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->setAccount(Lcom/android/mail/providers/Account;)V

    iput-object p4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iput-boolean v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    iput-object p5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mHandler:Landroid/os/Handler;

    sget v0, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllShortDelay:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllShortDelay:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllLongDelay:I

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/ui/AnimatedAdapter;Lcom/android/mail/providers/Account;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AnimatedAdapter;
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->setAccount(Lcom/android/mail/providers/Account;)V

    return-void
.end method

.method private cancelLeaveBehindFadeInAnimation()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->getLastLeaveBehindItem()Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/LeaveBehindItem;->cancelFadeInTextAnimation()V

    :cond_0
    return-void
.end method

.method private delete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;Ljava/util/HashSet;)V
    .locals 7
    .param p2    # Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v4}, Lcom/android/mail/ui/SwipeableListView;->getFirstVisiblePosition()I

    move-result v3

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v4}, Lcom/android/mail/ui/SwipeableListView;->getLastVisiblePosition()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    iget v4, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-lt v4, v3, :cond_0

    iget v4, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-gt v4, v1, :cond_0

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    iget-wide v5, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p2}, Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;->onListItemsRemoved()V

    :goto_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    return-void

    :cond_2
    invoke-direct {p0, p2}, Lcom/android/mail/ui/AnimatedAdapter;->performAndSetNextAction(Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    goto :goto_1
.end method

.method private getDeletingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Lcom/android/mail/providers/Conversation;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Z

    iput p1, p2, Lcom/android/mail/providers/Conversation;->position:I

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAnimatingViews:Ljava/util/HashMap;

    iget-wide v2, p2, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p3, p2}, Lcom/android/mail/ui/AnimatedAdapter;->newConversationItemView(ILandroid/view/ViewGroup;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/browse/SwipeableConversationItemView;

    move-result-object v0

    invoke-virtual {v0, p0, p4}, Lcom/android/mail/browse/SwipeableConversationItemView;->startDeleteAnimation(Lcom/android/mail/ui/AnimatedAdapter;Z)V

    :cond_0
    return-object v0
.end method

.method private getFadeLeaveBehindItem(ILcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/LeaveBehindItem;
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v1, p2, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/LeaveBehindItem;

    return-object v0
.end method

.method private getLeaveBehindItem(Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/LeaveBehindItem;
    .locals 3
    .param p1    # Lcom/android/mail/providers/Conversation;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v1, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/LeaveBehindItem;

    return-object v0
.end method

.method private getUndoingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Lcom/android/mail/providers/Conversation;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Z

    iput p1, p2, Lcom/android/mail/providers/Conversation;->position:I

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAnimatingViews:Ljava/util/HashMap;

    iget-wide v2, p2, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p3, p2}, Lcom/android/mail/ui/AnimatedAdapter;->newConversationItemView(ILandroid/view/ViewGroup;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/browse/SwipeableConversationItemView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v1

    invoke-virtual {v0, v1, p0, p4}, Lcom/android/mail/browse/SwipeableConversationItemView;->startUndoAnimation(Lcom/android/mail/ui/ViewMode;Lcom/android/mail/ui/AnimatedAdapter;Z)V

    :cond_0
    return-object v0
.end method

.method private hasFadeLeaveBehinds()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasLeaveBehinds()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPositionDeleting(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPositionFadeLeaveBehind(Lcom/android/mail/providers/Conversation;)Z
    .locals 3
    .param p1    # Lcom/android/mail/providers/Conversation;

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v1, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/mail/providers/Conversation;->isMostlyDead()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPositionLeaveBehind(Lcom/android/mail/providers/Conversation;)Z
    .locals 3
    .param p1    # Lcom/android/mail/providers/Conversation;

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v1, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/mail/providers/Conversation;->isMostlyDead()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPositionSwipeDeleting(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPositionUndoing(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPositionUndoingSwipe(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private newConversationItemView(ILandroid/view/ViewGroup;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/browse/SwipeableConversationItemView;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Lcom/android/mail/providers/Conversation;

    const/4 v1, 0x0

    invoke-super {p0, p1, v1, p2}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    invoke-virtual {v0}, Lcom/android/mail/browse/SwipeableConversationItemView;->reset()V

    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mBatchConversations:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v5, v1, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    :goto_0
    iget-boolean v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeEnabled:Z

    iget-boolean v7, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPriorityMarkersEnabled:Z

    move-object v1, p3

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/mail/browse/SwipeableConversationItemView;->bind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAnimatingViews:Ljava/util/HashMap;

    iget-wide v2, p3, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private final performAndSetNextAction(Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPendingDestruction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPendingDestruction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    invoke-interface {v0}, Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;->onListItemsRemoved()V

    :cond_0
    iput-object p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPendingDestruction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    return-void
.end method

.method private final setAccount(Lcom/android/mail/providers/Account;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v0, v0, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    iput-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPriorityMarkersEnabled:Z

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeEnabled:Z

    return-void
.end method

.method private updateAnimatingConversationItems(Ljava/lang/Object;Ljava/util/HashSet;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    instance-of v3, p1, Lcom/android/mail/browse/ConversationItemView;

    if-eqz v3, :cond_0

    move-object v2, p1

    check-cast v2, Lcom/android/mail/browse/ConversationItemView;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationItemView;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v3

    iget-wide v0, v3, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAnimatingViews:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/mail/ui/AnimatedAdapter;->performAndSetNextAction(Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public addFooter(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFooter:Landroid/view/View;

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    instance-of v0, p1, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mBatchConversations:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v5, v1, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    :goto_1
    iget-boolean v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeEnabled:Z

    iget-boolean v7, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPriorityMarkersEnabled:Z

    move-object v1, p3

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/mail/browse/SwipeableConversationItemView;->bind(Landroid/database/Cursor;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public cancelDismissCounter()V
    .locals 2

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->cancelLeaveBehindFadeInAnimation()V

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public cancelFadeOutLastLeaveBehindItemText()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->getLastLeaveBehindItem()Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/LeaveBehindItem;->cancelFadeOutText()V

    :cond_0
    return-void
.end method

.method public clearLeaveBehind(J)V
    .locals 3
    .param p1    # J

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/mail/ui/AnimatedAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Trying to clear a non-existant leave behind"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public commitLeaveBehindItems(Z)V
    .locals 6
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/LeaveBehindItem;

    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->getConversationId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->commit()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez p1, :cond_4

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->commit()V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x1

    :cond_4
    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_6
    return-void
.end method

.method public createConversationItemView(Lcom/android/mail/browse/SwipeableConversationItemView;Landroid/content/Context;Lcom/android/mail/providers/Conversation;)Landroid/view/View;
    .locals 9
    .param p1    # Lcom/android/mail/browse/SwipeableConversationItemView;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/android/mail/providers/Conversation;

    if-nez p1, :cond_0

    new-instance p1, Lcom/android/mail/browse/SwipeableConversationItemView;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-direct {p1, p2, v0}, Lcom/android/mail/browse/SwipeableConversationItemView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mBatchConversations:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v5, v0, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    :goto_0
    iget-boolean v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeEnabled:Z

    iget-boolean v7, p0, Lcom/android/mail/ui/AnimatedAdapter;->mPriorityMarkersEnabled:Z

    move-object v0, p1

    move-object v1, p3

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/mail/browse/SwipeableConversationItemView;->bind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V

    return-object p1

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public delete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V
    .locals 1
    .param p2    # Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/AnimatedAdapter;->delete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;Ljava/util/HashSet;)V

    return-void
.end method

.method public final destroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccountListener:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    return-void
.end method

.method public fadeOutLeaveBehindItems()V
    .locals 7

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    if-nez v3, :cond_2

    new-instance v3, Lcom/android/mail/ui/AnimatedAdapter$3;

    invoke-direct {v3, p0}, Lcom/android/mail/ui/AnimatedAdapter$3;-><init>(Lcom/android/mail/ui/AnimatedAdapter;)V

    iput-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    :goto_0
    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    iget-wide v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/android/mail/providers/Conversation;->id:J

    iget-wide v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    :cond_1
    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->cancelFadeInTextAnimation()V

    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->makeInert()V

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->startDismissCounter()V

    return-void
.end method

.method public fadeOutSpecificLeaveBehindItem(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->startFadeOutLeaveBehindItemsAnimations()V

    return-void
.end method

.method public getConversationCursor()Lcom/android/mail/browse/ConversationCursor;
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/ConversationCursor;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFooter:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastLeaveBehindItem()Lcom/android/mail/ui/LeaveBehindItem;
    .locals 4

    iget-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/LeaveBehindItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getListView()Lcom/android/mail/ui/SwipeableListView;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-boolean v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eqz v4, :cond_0

    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v4

    if-ne p1, v4, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFooter:Landroid/view/View;

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/ConversationCursor;

    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-direct {p0, v4, v5}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionUndoing(J)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p1, v0, p3, v6}, Lcom/android/mail/ui/AnimatedAdapter;->getUndoingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-direct {p0, v4, v5}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionUndoingSwipe(J)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p1, v0, p3, v7}, Lcom/android/mail/ui/AnimatedAdapter;->getUndoingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-direct {p0, v4, v5}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionDeleting(J)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, p1, v0, p3, v6}, Lcom/android/mail/ui/AnimatedAdapter;->getDeletingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_3
    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-direct {p0, v4, v5}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionSwipeDeleting(J)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, p1, v0, p3, v7}, Lcom/android/mail/ui/AnimatedAdapter;->getDeletingView(ILcom/android/mail/providers/Conversation;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionFadeLeaveBehind(Lcom/android/mail/providers/Conversation;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/AnimatedAdapter;->getFadeLeaveBehindItem(ILcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v2

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v4}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v4

    invoke-virtual {v2, v4, p0}, Lcom/android/mail/ui/LeaveBehindItem;->startShrinkAnimation(Lcom/android/mail/ui/ViewMode;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionLeaveBehind(Lcom/android/mail/providers/Conversation;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->getLeaveBehindItem(Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v3

    iget-wide v4, v0, Lcom/android/mail/providers/Conversation;->id:J

    iget-wide v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_8

    invoke-virtual {v3}, Lcom/android/mail/ui/LeaveBehindItem;->isAnimating()Z

    move-result v4

    if-eqz v4, :cond_7

    sget v4, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllLongDelay:I

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/LeaveBehindItem;->increaseFadeInDelay(I)V

    :cond_6
    :goto_1
    move-object v2, v3

    goto/16 :goto_0

    :cond_7
    sget v4, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllLongDelay:I

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/LeaveBehindItem;->startFadeInTextAnimation(I)V

    goto :goto_1

    :cond_8
    sget v4, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllShortDelay:I

    invoke-virtual {v3, v4}, Lcom/android/mail/ui/LeaveBehindItem;->startFadeInTextAnimation(I)V

    goto :goto_1

    :cond_9
    if-eqz p2, :cond_b

    instance-of v4, p2, Lcom/android/mail/browse/SwipeableConversationItemView;

    if-nez v4, :cond_b

    sget-object v4, Lcom/android/mail/ui/AnimatedAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Incorrect convert view received; nulling it out"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4, v1, p3}, Lcom/android/mail/ui/AnimatedAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_a
    :goto_2
    move-object v4, p2

    check-cast v4, Lcom/android/mail/browse/SwipeableConversationItemView;

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4, v5, v0}, Lcom/android/mail/ui/AnimatedAdapter;->createConversationItemView(Lcom/android/mail/browse/SwipeableConversationItemView;Landroid/content/Context;Lcom/android/mail/providers/Conversation;)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_0

    :cond_b
    if-eqz p2, :cond_a

    move-object v4, p2

    check-cast v4, Lcom/android/mail/browse/SwipeableConversationItemView;

    invoke-virtual {v4}, Lcom/android/mail/browse/SwipeableConversationItemView;->reset()V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hideFooter()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->setFooterVisibility(Z)V

    return-void
.end method

.method public isAnimating()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionDeleting(J)Z

    move-result v0

    if-nez v0, :cond_0

    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/AnimatedAdapter;->isPositionUndoing(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/android/mail/browse/SwipeableConversationItemView;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/android/mail/browse/SwipeableConversationItemView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AnimatedAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8
    .param p1    # Landroid/animation/Animator;

    const/4 v7, 0x0

    instance-of v5, p1, Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_3

    move-object v4, p1

    check-cast v4, Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v2

    :goto_0
    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    invoke-direct {p0, v2, v5}, Lcom/android/mail/ui/AnimatedAdapter;->updateAnimatingConversationItems(Ljava/lang/Object;Ljava/util/HashSet;)V

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    invoke-direct {p0, v2, v5}, Lcom/android/mail/ui/AnimatedAdapter;->updateAnimatingConversationItems(Ljava/lang/Object;Ljava/util/HashSet;)V

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    invoke-direct {p0, v2, v5}, Lcom/android/mail/ui/AnimatedAdapter;->updateAnimatingConversationItems(Ljava/lang/Object;Ljava/util/HashSet;)V

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-direct {p0, v2, v5}, Lcom/android/mail/ui/AnimatedAdapter;->updateAnimatingConversationItems(Ljava/lang/Object;Ljava/util/HashSet;)V

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v5

    if-eqz v5, :cond_1

    instance-of v5, v2, Lcom/android/mail/ui/LeaveBehindItem;

    if-eqz v5, :cond_1

    move-object v3, v2

    check-cast v3, Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v3}, Lcom/android/mail/ui/LeaveBehindItem;->getConversationId()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/android/mail/ui/AnimatedAdapter;->clearLeaveBehind(J)V

    invoke-virtual {v3}, Lcom/android/mail/ui/LeaveBehindItem;->commit()V

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasFadeLeaveBehinds()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->getLastLeaveBehindItem()Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/LeaveBehindItem;->cancelFadeInTextAnimationIfNotStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v7}, Lcom/android/mail/ui/LeaveBehindItem;->startFadeInTextAnimation(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->isAnimating()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v5, p0}, Lcom/android/mail/ui/ControllableActivity;->onAnimationEnd(Lcom/android/mail/ui/AnimatedAdapter;)V

    :cond_2
    return-void

    :cond_3
    check-cast p1, Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mDeletingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const-string v3, "last_deleting_items"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "last_deleting_items"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    aget-wide v4, v1, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v3, "leave_behind_item_data"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "leave_behind_item_data"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/LeaveBehindData;

    iget-object v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    const-string v4, "leave_behind_item_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/android/mail/ui/LeaveBehindData;->data:Lcom/android/mail/providers/Conversation;

    iget-object v6, v2, Lcom/android/mail/ui/LeaveBehindData;->op:Lcom/android/mail/ui/ToastBarOperation;

    iget-object v7, v2, Lcom/android/mail/ui/LeaveBehindData;->data:Lcom/android/mail/providers/Conversation;

    iget v7, v7, Lcom/android/mail/providers/Conversation;->position:I

    invoke-virtual {p0, v5, v6, v7}, Lcom/android/mail/ui/AnimatedAdapter;->setupLeaveBehind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ToastBarOperation;I)Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v8, -0x1

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v3, v4, [J

    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v4, "last_deleting_items"

    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-wide v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_1

    const-string v5, "leave_behind_item_data"

    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v4}, Lcom/android/mail/ui/LeaveBehindItem;->getLeaveBehindData()Lcom/android/mail/ui/LeaveBehindData;

    move-result-object v4

    invoke-virtual {p1, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "leave_behind_item_id"

    iget-wide v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-virtual {p1, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/LeaveBehindItem;

    iget-wide v4, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v4

    iget-wide v4, v4, Lcom/android/mail/providers/Conversation;->id:J

    iget-wide v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    :cond_3
    invoke-virtual {v2}, Lcom/android/mail/ui/LeaveBehindItem;->commit()V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public setFolder(Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    return-void
.end method

.method public setFooterVisibility(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mShowFooter:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setSwipeUndo(Z)V
    .locals 5
    .param p1    # Z

    const-wide/16 v3, -0x1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeUndoingItems:Ljava/util/HashSet;

    iget-wide v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iput-wide v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mRefreshAction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->performAndSetNextAction(Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    :cond_2
    return-void
.end method

.method public setUndo(Z)V
    .locals 5
    .param p1    # Z

    const-wide/16 v3, -0x1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-wide v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mUndoingItems:Ljava/util/HashSet;

    iget-wide v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iput-wide v3, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mRefreshAction:Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AnimatedAdapter;->performAndSetNextAction(Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    :cond_2
    return-void
.end method

.method public setupLeaveBehind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ToastBarOperation;I)Lcom/android/mail/ui/LeaveBehindItem;
    .locals 8
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/ui/ToastBarOperation;
    .param p3    # I

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->cancelLeaveBehindFadeInAnimation()V

    iget-wide v1, p1, Lcom/android/mail/providers/Conversation;->id:J

    iput-wide v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->fadeOutLeaveBehindItems()V

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v1

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->isWideMode(I)Z

    move-result v7

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    if-eqz v7, :cond_0

    const v1, 0x7f040064

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/LeaveBehindItem;

    iget-object v2, p0, Lcom/android/mail/ui/AnimatedAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v6, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    move v1, p3

    move-object v3, p0

    move-object v4, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/android/mail/ui/LeaveBehindItem;->bindOperations(ILcom/android/mail/providers/Account;Lcom/android/mail/ui/AnimatedAdapter;Lcom/android/mail/ui/ToastBarOperation;Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;)V

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v2, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    iget-wide v2, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_0
    const v1, 0x7f040062

    goto :goto_0
.end method

.method public startDismissCounter()V
    .locals 4

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    sget v2, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllLongDelay:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/AnimatedAdapter;->mCountDown:Ljava/lang/Runnable;

    sget v2, Lcom/android/mail/ui/AnimatedAdapter;->sDismissAllShortDelay:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected startFadeOutLeaveBehindItemsAnimations()V
    .locals 9

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v5}, Lcom/android/mail/ui/SwipeableListView;->getFirstVisiblePosition()I

    move-result v4

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v5}, Lcom/android/mail/ui/SwipeableListView;->getLastVisiblePosition()I

    move-result v1

    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->hasLeaveBehinds()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLeaveBehindItems:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mail/ui/LeaveBehindItem;

    invoke-virtual {v3}, Lcom/android/mail/ui/LeaveBehindItem;->getData()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    iget-wide v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_1

    iget-wide v5, v0, Lcom/android/mail/providers/Conversation;->id:J

    iget-wide v7, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastLeaveBehind:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    :cond_1
    iget v5, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-lt v5, v4, :cond_2

    iget v5, v0, Lcom/android/mail/providers/Conversation;->position:I

    if-gt v5, v1, :cond_2

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mFadeLeaveBehindItems:Ljava/util/HashMap;

    iget-wide v6, v0, Lcom/android/mail/providers/Conversation;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lcom/android/mail/ui/LeaveBehindItem;->commit()V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/ui/AnimatedAdapter;->cancelLeaveBehindFadeInAnimation()V

    :cond_4
    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/android/mail/ui/AnimatedAdapter;->mLastDeletingItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    :cond_5
    invoke-virtual {p0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public swipeDelete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V
    .locals 1
    .param p2    # Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AnimatedAdapter;->mSwipeDeletingItems:Ljava/util/HashSet;

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/AnimatedAdapter;->delete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;Ljava/util/HashSet;)V

    return-void
.end method
