.class public final Lcom/android/mail/ui/ConversationListFragment;
.super Landroid/app/ListFragment;
.source "ConversationListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;
.implements Lcom/android/mail/ui/ViewMode$ModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/ConversationListFragment$ConversationCursorObserver;,
        Lcom/android/mail/ui/ConversationListFragment$FolderObserver;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static TIMESTAMP_UPDATE_INTERVAL:I

.field private static mTabletDevice:Z


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountObserver:Lcom/android/mail/providers/AccountObserver;

.field private mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

.field private mConversationCursorHash:I

.field private mConversationCursorObserver:Landroid/database/DataSetObserver;

.field private mEmptyView:Landroid/view/View;

.field private mErrorListener:Lcom/android/mail/ui/ErrorListener;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mFolderObserver:Landroid/database/DataSetObserver;

.field private mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

.field private final mHandler:Landroid/os/Handler;

.field private mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

.field private mListView:Lcom/android/mail/ui/SwipeableListView;

.field private mSearchResultCountTextView:Landroid/widget/TextView;

.field private mSearchStatusTextView:Landroid/widget/TextView;

.field private mSearchStatusView:Landroid/view/View;

.field private mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

.field private mUpdateTimestampsRunnable:Ljava/lang/Runnable;

.field private mUpdater:Lcom/android/mail/ui/ConversationUpdater;

.field private mViewContext:Lcom/android/mail/ConversationListContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/ConversationListFragment;->LOG_TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/ui/ConversationListFragment;->TIMESTAMP_UPDATE_INTERVAL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdateTimestampsRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/mail/ui/ConversationListFragment$1;

    invoke-direct {v0, p0}, Lcom/android/mail/ui/ConversationListFragment$1;-><init>(Lcom/android/mail/ui/ConversationListFragment;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/android/mail/ui/ConversationListFragment;Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/Account;
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/mail/ui/ConversationListFragment;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->setSwipeAction()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mail/ui/ConversationListFragment;)Lcom/android/mail/ui/ControllableActivity;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/mail/ui/ConversationListFragment;)Lcom/android/mail/ui/SwipeableListView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mail/ui/ConversationListFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdateTimestampsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/android/mail/ui/ConversationListFragment;->TIMESTAMP_UPDATE_INTERVAL:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/mail/ui/ConversationListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mail/ui/ConversationListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    invoke-interface {v0}, Lcom/android/mail/ui/ConversationListCallbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final getDefaultChoiceMode(Z)I
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeUiForFirstDisplay()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v1, 0x7f0800d9

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ControllableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchStatusView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v1, 0x7f0800da

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ControllableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchStatusTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v1, 0x7f0800db

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ControllableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchResultCountTextView:Landroid/widget/TextView;

    return-void
.end method

.method public static newInstance(Lcom/android/mail/ConversationListContext;)Lcom/android/mail/ui/ConversationListFragment;
    .locals 4
    .param p0    # Lcom/android/mail/ConversationListContext;

    new-instance v1, Lcom/android/mail/ui/ConversationListFragment;

    invoke-direct {v1}, Lcom/android/mail/ui/ConversationListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "conversation-list"

    invoke-virtual {p0}, Lcom/android/mail/ConversationListContext;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/ConversationListFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onCursorUpdated()V
    .locals 5

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    invoke-interface {v3}, Lcom/android/mail/ui/ConversationListCallbacks;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v3, v1}, Lcom/android/mail/ui/AnimatedAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    if-nez v1, :cond_3

    const/4 v2, 0x0

    :goto_1
    iget v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorHash:I

    if-ne v3, v2, :cond_2

    iget v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorHash:I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v3}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    :cond_2
    iput v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorHash:I

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    invoke-interface {v3}, Lcom/android/mail/ui/ConversationListCallbacks;->getCurrentConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v3}, Lcom/android/mail/ui/SwipeableListView;->getChoiceMode()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v3}, Lcom/android/mail/ui/SwipeableListView;->getCheckedItemPosition()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    iget v3, v0, Lcom/android/mail/providers/Conversation;->position:I

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/android/mail/ui/ConversationListFragment;->setSelected(IZ)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationCursor;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method private onFolderStatusUpdated()V
    .locals 7

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    :goto_0
    const-string v6, "cursor_error"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "cursor_error"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    :goto_1
    const-string v6, "cursor_status"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iget-object v6, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v6, :cond_5

    iget-object v5, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    iget v3, v5, Lcom/android/mail/providers/Folder;->totalCount:I

    :goto_2
    if-nez v1, :cond_0

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    :cond_0
    if-lez v3, :cond_2

    :cond_1
    invoke-direct {p0, v3}, Lcom/android/mail/ui/ConversationListFragment;->updateSearchResultHeader(I)V

    if-nez v3, :cond_2

    iget-object v5, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v6, p0, Lcom/android/mail/ui/ConversationListFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/android/mail/ui/SwipeableListView;->setEmptyView(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0

    :cond_4
    move v1, v5

    goto :goto_1

    :cond_5
    move v3, v5

    goto :goto_2
.end method

.method private final setChoiceMode(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SwipeableListView;->setChoiceMode(I)V

    return-void
.end method

.method private setSwipeAction()V
    .locals 5

    const/16 v4, 0x8

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-static {v2}, Lcom/android/mail/providers/Settings;->getSwipeSetting(Lcom/android/mail/providers/Settings;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v3, 0x4000

    invoke-virtual {v2, v3}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v2}, Lcom/android/mail/providers/Folder;->isTrash()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/SwipeableListView;->enableSwipe(Z)V

    :goto_0
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/SwipeableListView;->setCurrentFolder(Lcom/android/mail/providers/Folder;)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/SwipeableListView;->enableSwipe(Z)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v2}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    iget v2, v2, Lcom/android/mail/providers/Folder;->type:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_3

    :cond_2
    const v0, 0x7f08011a

    :goto_1
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v2, v0}, Lcom/android/mail/ui/SwipeableListView;->setSwipeAction(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    if-nez v2, :cond_4

    const v0, 0x7f080119

    goto :goto_1

    :cond_4
    packed-switch v1, :pswitch_data_0

    :cond_5
    const v0, 0x7f08011a

    goto :goto_1

    :pswitch_0
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v2, v4}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v2

    if-eqz v2, :cond_6

    const v0, 0x7f080118

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v2, v4}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const v0, 0x7f080119

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private showList()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/SwipeableListView;->setEmptyView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getFolderController()Lcom/android/mail/ui/FolderController;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/mail/ui/FolderController;->getFolder()Lcom/android/mail/providers/Folder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/ConversationListFragment;->onFolderUpdated(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->onConversationListStatusUpdated()V

    return-void
.end method

.method private updateSearchResultHeader(I)V
    .locals 7
    .param p1    # I

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v2}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchStatusTextView:Landroid/widget/TextView;

    const v3, 0x7f0900b2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchResultCountTextView:Landroid/widget/TextView;

    const v3, 0x7f0900f3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public commitDestructiveActions(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SwipeableListView;->commitDestructiveActions(Z)V

    :cond_0
    return-void
.end method

.method configureSearchResultHeader()V
    .locals 7

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v4}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchStatusTextView:Landroid/widget/TextView;

    const v6, 0x7f0900bb

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchResultCountTextView:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v6, p0, Lcom/android/mail/ui/ConversationListFragment;->mSearchStatusView:Landroid/view/View;

    if-eqz v3, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_3

    const v4, 0x7f0c0047

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    :goto_2
    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v4}, Lcom/android/mail/ui/SwipeableListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v4, v0}, Lcom/android/mail/ui/SwipeableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    const/16 v4, 0x8

    goto :goto_1

    :cond_3
    move v1, v5

    goto :goto_2
.end method

.method public getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    instance-of v0, v6, Lcom/android/mail/ui/ControllableActivity;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/ConversationListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "ConversationListFragment expects only a ControllableActivity tocreate it. Cannot proceed."

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    check-cast v6, Lcom/android/mail/ui/ControllableActivity;

    iput-object v6, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getListHandler()Lcom/android/mail/ui/ConversationListCallbacks;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getErrorListener()Lcom/android/mail/ui/ErrorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mErrorListener:Lcom/android/mail/ui/ErrorListener;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040021

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/ConversationListFooterView;

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationListFooterView;->setClickListener(Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;)V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v2

    new-instance v0, Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getSelectedSet()Lcom/android/mail/ui/ConversationSelectionSet;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v5, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/ui/AnimatedAdapter;-><init>(Landroid/content/Context;Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/SwipeableListView;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AnimatedAdapter;->addFooter(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/SwipeableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getSelectedSet()Lcom/android/mail/ui/ConversationSelectionSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/SwipeableListView;->setSelectionSet(Lcom/android/mail/ui/ConversationSelectionSet;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->hideFooter()V

    new-instance v0, Lcom/android/mail/ui/ConversationListFragment$FolderObserver;

    invoke-direct {v0, p0, v10}, Lcom/android/mail/ui/ConversationListFragment$FolderObserver;-><init>(Lcom/android/mail/ui/ConversationListFragment;Lcom/android/mail/ui/ConversationListFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolderObserver:Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getFolderController()Lcom/android/mail/ui/FolderController;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/FolderController;->registerFolderObserver(Landroid/database/DataSetObserver;)V

    new-instance v0, Lcom/android/mail/ui/ConversationListFragment$ConversationCursorObserver;

    invoke-direct {v0, p0, v10}, Lcom/android/mail/ui/ConversationListFragment$ConversationCursorObserver;-><init>(Lcom/android/mail/ui/ConversationListFragment;Lcom/android/mail/ui/ConversationListFragment$1;)V

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorObserver:Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ConversationUpdater;->registerConversationListObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v0

    sput-boolean v0, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->initializeUiForFirstDisplay()V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->configureSearchResultHeader()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/ConversationListFragment;->onViewModeChanged(I)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mail/ui/ViewMode;->addListener(Lcom/android/mail/ui/ViewMode$ModeChangeListener;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-nez v2, :cond_5

    move v0, v9

    :goto_1
    iput v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorHash:I

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationCursor;->isRefreshReady()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationCursor;->sync()V

    :cond_3
    sget-boolean v0, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    invoke-static {v0}, Lcom/android/mail/ui/ConversationListFragment;->getDefaultChoiceMode(Z)I

    move-result v7

    if-eqz p1, :cond_4

    const-string v0, "choice-mode-key"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-string v0, "list-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0}, Lcom/android/mail/ui/SwipeableListView;->clearChoices()V

    :cond_4
    invoke-direct {p0, v7}, Lcom/android/mail/ui/ConversationListFragment;->setChoiceMode(I)V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->showList()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getPendingToastOperation()Lcom/android/mail/ui/ToastBarOperation;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0, v10}, Lcom/android/mail/ui/ControllableActivity;->setPendingToastOperation(Lcom/android/mail/ui/ToastBarOperation;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0, v8}, Lcom/android/mail/ui/ControllableActivity;->onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationCursor;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public onConversationListStatusUpdated()V
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    invoke-virtual {v2, v0}, Lcom/android/mail/browse/ConversationListFooterView;->updateStatus(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->onFolderStatusUpdated()V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v2, v1}, Lcom/android/mail/ui/AnimatedAdapter;->setFooterVisibility(Z)V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->onCursorUpdated()V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    sput v2, Lcom/android/mail/ui/ConversationListFragment;->TIMESTAMP_UPDATE_INTERVAL:I

    new-instance v2, Lcom/android/mail/ui/ConversationListFragment$2;

    invoke-direct {v2, p0}, Lcom/android/mail/ui/ConversationListFragment$2;-><init>(Lcom/android/mail/ui/ConversationListFragment;)V

    iput-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdateTimestampsRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "conversation-list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/ConversationListContext;->forBundle(Landroid/os/Bundle;)Lcom/android/mail/ConversationListContext;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    iget-object v2, v2, Lcom/android/mail/ConversationListContext;->account:Lcom/android/mail/providers/Account;

    iput-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/ConversationListFragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f040020

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mEmptyView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/SwipeableListView;

    iput-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/SwipeableListView;->setHeaderDividersEnabled(Z)V

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/SwipeableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v3, 0x4000

    invoke-virtual {v2, v3}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/SwipeableListView;->enableSwipe(Z)V

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/SwipeableListView;->setSwipedListener(Lcom/android/mail/ui/SwipeableListView$ListItemSwipedListener;)V

    if-eqz p3, :cond_0

    const-string v1, "list-state"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const-string v2, "list-state"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/SwipeableListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->destroy()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0, v2}, Lcom/android/mail/ui/SwipeableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0, p0}, Lcom/android/mail/ui/ControllableActivity;->unsetViewModeListener(Lcom/android/mail/ui/ViewMode$ModeChangeListener;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolderObserver:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getFolderController()Lcom/android/mail/ui/FolderController;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/FolderController;->unregisterFolderObserver(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolderObserver:Landroid/database/DataSetObserver;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorObserver:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ConversationUpdater;->unregisterConversationListObserver(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mConversationCursorObserver:Landroid/database/DataSetObserver;

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    return-void
.end method

.method public onFolderUpdated(Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->setSwipeAction()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AnimatedAdapter;->setFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationListFooterView;->setFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0}, Lcom/android/mail/providers/Folder;->wasSyncSuccessful()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mErrorListener:Lcom/android/mail/ui/ErrorListener;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/mail/ui/ErrorListener;->onError(Lcom/android/mail/providers/Folder;Z)V

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->onFolderStatusUpdated()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemViewModel;->onFolderUpdated(Lcom/android/mail/providers/Folder;)V

    goto :goto_0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const/4 v1, 0x1

    instance-of v0, p2, Lcom/android/mail/browse/ConversationItemView;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    check-cast p2, Lcom/android/mail/browse/ConversationItemView;

    invoke-virtual {p2}, Lcom/android/mail/browse/ConversationItemView;->toggleCheckMarkOrBeginDrag()V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    instance-of v1, p2, Lcom/android/mail/browse/ToggleableItem;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mSelectedSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/android/mail/browse/ToggleableItem;

    invoke-interface {v0}, Lcom/android/mail/browse/ToggleableItem;->toggleCheckMarkOrBeginDrag()V

    :goto_1
    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/ConversationListFragment;->commitDestructiveActions(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p3}, Lcom/android/mail/ui/ConversationListFragment;->viewConversation(I)V

    goto :goto_1
.end method

.method public onListItemSwiped(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-interface {v0, p1}, Lcom/android/mail/ui/ConversationUpdater;->showNextConversation(Ljava/util/Collection;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->handleNotificationActions()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    if-eqz v0, :cond_0

    const-string v0, "list-state"

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v1}, Lcom/android/mail/ui/SwipeableListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "choice-mode-key"

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v1}, Lcom/android/mail/ui/SwipeableListView;->getChoiceMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    invoke-super {p0}, Landroid/app/ListFragment;->onStart()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdateTimestampsRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/android/mail/ui/ConversationListFragment;->TIMESTAMP_UPDATE_INTERVAL:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListFragment;->onStop()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mUpdateTimestampsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onViewModeChanged(I)V
    .locals 2
    .param p1    # I

    sget-boolean v0, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/mail/ui/ViewMode;->isConversationMode(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const v1, 0x7f02008c

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/SwipeableListView;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mFooterView:Lcom/android/mail/browse/ConversationListFooterView;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationListFooterView;->onViewModeChanged(I)V

    :cond_1
    return-void

    :cond_2
    invoke-static {p1}, Lcom/android/mail/ui/ViewMode;->isListMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/SwipeableListView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0}, Lcom/android/mail/ui/SwipeableListView;->clearChoices()V

    goto :goto_0
.end method

.method public requestDelete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V
    .locals 7
    .param p1    # I
    .param p3    # Lcom/android/mail/ui/DestructiveAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/DestructiveAction;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/android/mail/providers/Conversation;->localDeleteOnUpdate:Z

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/android/mail/ui/ConversationListFragment$3;

    invoke-direct {v3, p0, p3}, Lcom/android/mail/ui/ConversationListFragment$3;-><init>(Lcom/android/mail/ui/ConversationListFragment;Lcom/android/mail/ui/DestructiveAction;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/ConversationListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v2}, Lcom/android/mail/ui/SwipeableListView;->getSwipeAction()I

    move-result v4

    if-ne v4, p1, :cond_2

    invoke-virtual {v2, p2, v3}, Lcom/android/mail/ui/SwipeableListView;->destroyItems(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/android/mail/ui/ConversationListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "ConversationListFragment.requestDelete: listView failed to destroy items."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {p3}, Lcom/android/mail/ui/DestructiveAction;->performAction()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v4, p2, v3}, Lcom/android/mail/ui/AnimatedAdapter;->delete(Ljava/util/Collection;Lcom/android/mail/ui/SwipeableListView$ListItemsRemovedListener;)V

    goto :goto_1
.end method

.method public requestListRefresh()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/AnimatedAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final revertChoiceMode()V
    .locals 1

    sget-boolean v0, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    invoke-static {v0}, Lcom/android/mail/ui/ConversationListFragment;->getDefaultChoiceMode(Z)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/ConversationListFragment;->setChoiceMode(I)V

    goto :goto_0
.end method

.method public final setChoiceNone()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v2, Lcom/android/mail/ui/ConversationListFragment;->mTabletDevice:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v2}, Lcom/android/mail/ui/SwipeableListView;->getCheckedItemPosition()I

    move-result v0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v2}, Lcom/android/mail/ui/SwipeableListView;->clearChoices()V

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v2, v0}, Lcom/android/mail/ui/SwipeableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Landroid/view/View;->setActivated(Z)V

    :cond_1
    invoke-direct {p0, v3}, Lcom/android/mail/ui/ConversationListFragment;->setChoiceMode(I)V

    goto :goto_0
.end method

.method public setSelected(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0}, Lcom/android/mail/ui/SwipeableListView;->getChoiceMode()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/SwipeableListView;->smoothScrollToPosition(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/ConversationListFragment;->mListView:Lcom/android/mail/ui/SwipeableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/mail/ui/SwipeableListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-super {p0}, Landroid/app/ListFragment;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " folder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mViewContext:Lcom/android/mail/ConversationListContext;

    iget-object v2, v2, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected viewConversation(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x1

    const/4 v6, 0x0

    sget-object v2, Lcom/android/mail/ui/ConversationListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "ConversationListFragment.viewConversation(%d)"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0, p1, v7}, Lcom/android/mail/ui/ConversationListFragment;->setSelected(IZ)V

    invoke-direct {p0}, Lcom/android/mail/ui/ConversationListFragment;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput p1, v0, Lcom/android/mail/providers/Conversation;->position:I

    iget-object v2, p0, Lcom/android/mail/ui/ConversationListFragment;->mCallbacks:Lcom/android/mail/ui/ConversationListCallbacks;

    invoke-interface {v2, v0, v6}, Lcom/android/mail/ui/ConversationListCallbacks;->onConversationSelected(Lcom/android/mail/providers/Conversation;Z)V

    :cond_0
    return-void
.end method
