.class public Lcom/android/mail/ui/MailSpinner;
.super Landroid/widget/FrameLayout;
.source "MailSpinner.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountName:Landroid/widget/TextView;

.field private final mContainer:Landroid/widget/LinearLayout;

.field private mController:Lcom/android/mail/ui/ActivityController;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private final mFolderCount:Landroid/widget/TextView;

.field private final mFolderName:Landroid/widget/TextView;

.field private final mListPopupWindow:Landroid/widget/ListPopupWindow;

.field private mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/mail/ui/MailSpinner;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/mail/ui/MailSpinner;->$assertionsDisabled:Z

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/MailSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/MailSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/widget/ListPopupWindow;

    invoke-direct {v1, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    invoke-virtual {p0}, Lcom/android/mail/ui/MailSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040005

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/MailSpinner;->addView(Landroid/view/View;)V

    const v1, 0x7f08001b

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/MailSpinner;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mAccountName:Landroid/widget/TextView;

    const v1, 0x7f08001a

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/MailSpinner;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolderName:Landroid/widget/TextView;

    const v1, 0x7f080018

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/MailSpinner;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolderCount:Landroid/widget/TextView;

    const v1, 0x7f080019

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/MailSpinner;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final changeEnabledState(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/MailSpinner;->setEnabled(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f020097

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/mail/ui/MailSpinner;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->commitDestructiveActions(Z)V

    :cond_0
    return-void
.end method

.method public onFolderUpdated(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->onFolderUpdated(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/MailSpinner;->setFolder(Lcom/android/mail/providers/Folder;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    sget-object v4, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onNavigationItemSelected(%d, %d) called"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v4, p3}, Lcom/android/mail/AccountSpinnerAdapter;->getItemViewType(I)I

    move-result v3

    const/4 v1, 0x0

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    sget-object v4, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    const-string v5, "MailSpinner.onItemClick(%d): Strange click ignored: type %d."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_0
    return-void

    :pswitch_1
    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v4, p3}, Lcom/android/mail/AccountSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Account;

    sget-object v4, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onNavigationItemSelected: Selecting account: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v5, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v4}, Lcom/android/mail/ui/ActivityController;->loadAccountInbox()V

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v4, v0}, Lcom/android/mail/ui/ActivityController;->onAccountChanged(Lcom/android/mail/providers/Account;)V

    goto :goto_1

    :pswitch_2
    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v4, p3}, Lcom/android/mail/AccountSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    sget-boolean v4, Lcom/android/mail/ui/MailSpinner;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    instance-of v4, v2, Lcom/android/mail/providers/Folder;

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :cond_2
    sget-object v5, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    const-string v6, "onNavigationItemSelected: Selecting folder: %s"

    const/4 v4, 0x1

    new-array v7, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v4, v2

    check-cast v4, Lcom/android/mail/providers/Folder;

    iget-object v4, v4, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    aput-object v4, v7, v8

    invoke-static {v5, v6, v7}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    check-cast v2, Lcom/android/mail/providers/Folder;

    invoke-interface {v4, v2}, Lcom/android/mail/ui/ActivityController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v4}, Lcom/android/mail/ui/ActivityController;->showFolderList()V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_4
    sget-object v4, Lcom/android/mail/ui/MailSpinner;->LOG_TAG:Ljava/lang/String;

    const-string v5, "MailSpinner.onItemClick(): Got unexpected click on header."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setAccount(Lcom/android/mail/providers/Account;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/ui/MailSpinner;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mAccountName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setAdapter(Lcom/android/mail/AccountSpinnerAdapter;)V
    .locals 2
    .param p1    # Lcom/android/mail/AccountSpinnerAdapter;

    iput-object p1, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    iget-object v0, p0, Lcom/android/mail/ui/MailSpinner;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setController(Lcom/android/mail/ui/ActivityController;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ActivityController;

    iput-object p1, p0, Lcom/android/mail/ui/MailSpinner;->mController:Lcom/android/mail/ui/ActivityController;

    return-void
.end method

.method public setFolder(Lcom/android/mail/providers/Folder;)V
    .locals 4
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/MailSpinner;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolderName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/ui/MailSpinner;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->getFolderUnreadDisplayCount(Lcom/android/mail/providers/Folder;)I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolderCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/mail/ui/MailSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/mail/utils/Utils;->getUnreadCountString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mFolderCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/mail/ui/MailSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100002

    invoke-static {v2, v3, v0}, Lcom/android/mail/utils/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/MailSpinner;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    iget-object v2, p0, Lcom/android/mail/ui/MailSpinner;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v1, v2}, Lcom/android/mail/AccountSpinnerAdapter;->setCurrentFolder(Lcom/android/mail/providers/Folder;)Z

    :cond_0
    return-void
.end method
