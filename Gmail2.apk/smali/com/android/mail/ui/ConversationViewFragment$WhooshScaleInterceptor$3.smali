.class Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;
.super Ljava/lang/Object;
.source "ConversationViewFragment.java"

# interfaces
.implements Landroid/webkit/WebView$PictureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->setupPictureListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V
    .locals 8
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/graphics/Picture;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/ui/ConversationViewFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MessageView.onNewPicture, t=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v6, v6, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J
    invoke-static {v6}, Lcom/android/mail/ui/ConversationViewFragment;->access$2600(Lcom/android/mail/ui/ConversationViewFragment;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mOnMessageLoadComplete:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3500(Lcom/android/mail/ui/ConversationViewFragment;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v0, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mOnMessageLoadComplete:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$3500(Lcom/android/mail/ui/ConversationViewFragment;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method
