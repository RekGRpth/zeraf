.class public Lcom/android/mail/ui/LeaveBehindData;
.super Ljava/lang/Object;
.source "LeaveBehindData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$ClassLoaderCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$ClassLoaderCreator",
            "<",
            "Lcom/android/mail/ui/LeaveBehindData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final data:Lcom/android/mail/providers/Conversation;

.field final op:Lcom/android/mail/ui/ToastBarOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/mail/ui/LeaveBehindData$1;

    invoke-direct {v0}, Lcom/android/mail/ui/LeaveBehindData$1;-><init>()V

    sput-object v0, Lcom/android/mail/ui/LeaveBehindData;->CREATOR:Landroid/os/Parcelable$ClassLoaderCreator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # Ljava/lang/ClassLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    iput-object v0, p0, Lcom/android/mail/ui/LeaveBehindData;->data:Lcom/android/mail/providers/Conversation;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/ToastBarOperation;

    iput-object v0, p0, Lcom/android/mail/ui/LeaveBehindData;->op:Lcom/android/mail/ui/ToastBarOperation;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;Lcom/android/mail/ui/LeaveBehindData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Ljava/lang/ClassLoader;
    .param p3    # Lcom/android/mail/ui/LeaveBehindData$1;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/LeaveBehindData;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ToastBarOperation;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/ui/ToastBarOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mail/ui/LeaveBehindData;->data:Lcom/android/mail/providers/Conversation;

    iput-object p2, p0, Lcom/android/mail/ui/LeaveBehindData;->op:Lcom/android/mail/ui/ToastBarOperation;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindData;->data:Lcom/android/mail/providers/Conversation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/android/mail/ui/LeaveBehindData;->op:Lcom/android/mail/ui/ToastBarOperation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
