.class Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;
.super Ljava/lang/Object;
.source "ConversationViewFragment.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/ConversationViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WhooshScaleInterceptor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mail/ui/ConversationViewFragment;


# direct methods
.method private constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;Lcom/android/mail/ui/ConversationViewFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ConversationViewFragment;
    .param p2    # Lcom/android/mail/ui/ConversationViewFragment$1;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;-><init>(Lcom/android/mail/ui/ConversationViewFragment;)V

    return-void
.end method

.method private loadSingleMessageView(ZII)V
    .locals 30
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->computeHorizontalScrollRange()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->computeHorizontalScrollExtent()I

    move-result v4

    if-le v3, v4, :cond_1

    const/16 v18, 0x1

    :goto_0
    if-nez v18, :cond_2

    if-nez p1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v18, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v3, v3, Lcom/android/mail/ui/ConversationViewFragment;->mAccount:Lcom/android/mail/providers/Account;

    # invokes: Lcom/android/mail/ui/ConversationViewFragment;->isOverviewMode(Lcom/android/mail/providers/Account;)Z
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3100(Lcom/android/mail/providers/Account;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p1, :cond_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v3

    add-int v28, p3, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->computeVerticalScrollRange()I

    move-result v22

    const/16 v23, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationViewAdapter;->getCount()I

    move-result v20

    :goto_2
    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mAdapter:Lcom/android/mail/browse/ConversationViewAdapter;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationViewAdapter;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/android/mail/browse/ConversationViewAdapter;->getItem(I)Lcom/android/mail/browse/ConversationOverlayItem;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/android/mail/browse/ConversationOverlayItem;->getTop()I

    move-result v3

    move/from16 v0, v28

    if-ge v0, v3, :cond_5

    invoke-virtual/range {v19 .. v19}, Lcom/android/mail/browse/ConversationOverlayItem;->getTop()I

    move-result v22

    :cond_4
    if-nez v23, :cond_7

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/ui/ConversationViewFragment;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ignoring scaleBegin on y=%d, no matching message."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_5
    move-object/from16 v0, v19

    instance-of v3, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v3, :cond_6

    invoke-virtual/range {v19 .. v19}, Lcom/android/mail/browse/ConversationOverlayItem;->canBecomeSnapHeader()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v23, v19

    check-cast v23, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->getScrollX()I

    move-result v3

    if-lez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->computeHorizontalScrollOffset()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mail/browse/ConversationWebView;->computeHorizontalScrollRange()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v7}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/mail/browse/ConversationWebView;->computeHorizontalScrollExtent()I

    move-result v7

    sub-int/2addr v5, v7

    int-to-float v5, v5

    div-float/2addr v4, v5

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1002(Lcom/android/mail/ui/ConversationViewFragment;F)F

    :goto_3
    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getTop()I

    move-result v3

    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getHeight()I

    move-result v4

    add-int v21, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v3

    move/from16 v0, v21

    if-ge v0, v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v4

    int-to-float v4, v4

    move/from16 v0, v21

    int-to-float v5, v0

    sub-float/2addr v4, v5

    sub-int v5, v22, v21

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v7}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/mail/browse/ConversationWebView;->computeVerticalScrollExtent()I

    move-result v7

    sub-int/2addr v5, v7

    int-to-float v5, v5

    div-float/2addr v4, v5

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1102(Lcom/android/mail/ui/ConversationViewFragment;F)F

    :goto_4
    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v3

    iget-boolean v3, v3, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->alwaysShowImages:Z

    if-nez v3, :cond_8

    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getShowImages()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_8
    const/4 v6, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$2400(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/ui/HtmlConversationTemplates;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mSideMarginPx:I
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$2900(Lcom/android/mail/ui/ConversationViewFragment;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->startConversation(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$2400(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/ui/HtmlConversationTemplates;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/android/mail/ui/HtmlConversationTemplates;->appendMessageHtml(Lcom/android/mail/providers/Message;ZZII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mTemplates:Lcom/android/mail/ui/HtmlConversationTemplates;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$2400(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/ui/HtmlConversationTemplates;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v8, v3, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v3, v3, Lcom/android/mail/ui/ConversationViewFragment;->mConversation:Lcom/android/mail/providers/Conversation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v4, v4, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/mail/providers/Conversation;->getBaseUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v7 .. v13}, Lcom/android/mail/ui/HtmlConversationTemplates;->endConversation(Ljava/lang/String;Ljava/lang/String;IIZZ)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    iget-object v8, v3, Lcom/android/mail/ui/ConversationViewFragment;->mBaseUri:Ljava/lang/String;

    const-string v10, "text/html"

    const-string v11, "utf-8"

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Lcom/android/mail/browse/MessageView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewLoadStartMs:J
    invoke-static {v3, v4, v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$2602(Lcom/android/mail/ui/ConversationViewFragment;J)J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v4

    if-eqz p1, :cond_d

    const/16 v3, 0xc8

    :goto_6
    invoke-virtual {v4, v3}, Lcom/android/mail/browse/MessageView;->setInitialScale(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    const/4 v4, 0x1

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewIsLoading:Z
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$902(Lcom/android/mail/ui/ConversationViewFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    if-eqz p1, :cond_e

    const v26, 0x3f933333

    :goto_7
    const/4 v3, 0x0

    invoke-virtual/range {v23 .. v23}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getTop()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v24

    new-instance v14, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40200000

    invoke-direct {v14, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    move/from16 v0, p2

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationWebView;->setPivotX(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    move/from16 v0, p3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationWebView;->setPivotY(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationWebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const v4, 0x3e99999a

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    move/from16 v0, v24

    neg-int v4, v0

    int-to-float v4, v4

    const v5, 0x3f99999a

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lcom/android/mail/utils/HardwareLayerEnabler;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v5}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/mail/utils/HardwareLayerEnabler;-><init>(Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->bindView(Landroid/view/View;Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v3

    move/from16 v0, v24

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/MessageHeaderView;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWhooshHeader:Lcom/android/mail/browse/MessageHeaderView;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3300(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageHeaderView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/MessageHeaderView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$2100(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationContainer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/android/mail/browse/ConversationContainer;->getViewForItem(Lcom/android/mail/browse/ConversationOverlayItem;)Landroid/view/View;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mConversationContainer:Lcom/android/mail/browse/ConversationContainer;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$2100(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/browse/ConversationContainer;->getOverlayViews()Ljava/util/List;

    move-result-object v25

    if-eqz v29, :cond_9

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 v3, 0x4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/view/View;

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0x96

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lcom/android/mail/utils/HardwareLayerEnabler;

    move-object/from16 v0, v27

    invoke-direct {v4, v0}, Lcom/android/mail/utils/HardwareLayerEnabler;-><init>(Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_8

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    const/4 v4, 0x0

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollXFraction:F
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1002(Lcom/android/mail/ui/ConversationViewFragment;F)F

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    const/4 v4, 0x0

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageScrollYFraction:F
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$1102(Lcom/android/mail/ui/ConversationViewFragment;F)F

    goto/16 :goto_4

    :cond_c
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_d
    const/4 v3, 0x1

    goto/16 :goto_6

    :cond_e
    const v26, 0x3f59999a

    goto/16 :goto_7

    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;
    invoke-static {v3}, Lcom/android/mail/ui/ConversationViewFragment;->access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v3

    const v4, 0x3c23d70a

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    new-instance v15, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v25

    invoke-direct {v15, v0, v1, v2}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;-><init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;Landroid/view/View;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    new-instance v4, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;

    const-string v5, "onMessageLoadComplete"

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v14, v15}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$2;-><init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;Ljava/lang/String;Landroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)V

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mOnMessageLoadComplete:Ljava/lang/Runnable;
    invoke-static {v3, v4}, Lcom/android/mail/ui/ConversationViewFragment;->access$3502(Lcom/android/mail/ui/ConversationViewFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->setupPictureListener()V

    goto/16 :goto_1
.end method

.method private setupPictureListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$3;-><init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;)V

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v2, 0x1

    const/high16 v5, 0x3f800000

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z
    invoke-static {v1}, Lcom/android/mail/ui/ConversationViewFragment;->access$3000(Lcom/android/mail/ui/ConversationViewFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    sub-float v1, v0, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v4, 0x3dcccccd

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    cmpl-float v1, v0, v5

    if-lez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    float-to-int v5, v5

    invoke-direct {p0, v1, v4, v5}, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->loadSingleMessageView(ZII)V

    iget-object v1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z
    invoke-static {v1, v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$3002(Lcom/android/mail/ui/ConversationViewFragment;Z)Z

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mIgnoreOnScaleCalls:Z
    invoke-static {v0, v1}, Lcom/android/mail/ui/ConversationViewFragment;->access$3002(Lcom/android/mail/ui/ConversationViewFragment;Z)Z

    return-void
.end method
