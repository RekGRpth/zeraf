.class public Lcom/android/mail/NotificationActionIntentService;
.super Landroid/app/IntentService;
.source "NotificationActionIntentService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "NotificationActionIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const/4 v10, 0x0

    const/4 v9, 0x1

    move-object v2, p0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v8, "com.android.mail.extra.EXTRA_NOTIFICATION_ACTION"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;

    invoke-virtual {v5}, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;->getMessage()Lcom/android/mail/providers/Message;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/mail/NotificationActionIntentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v8, "com.android.mail.action.NOTIF_UNDO"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {v2, v5}, Lcom/android/mail/utils/NotificationActionUtils;->cancelUndoTimeout(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    invoke-static {v2, v5}, Lcom/android/mail/utils/NotificationActionUtils;->cancelUndoNotification(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    :goto_0
    return-void

    :cond_0
    const-string v8, "com.android.mail.action.NOTIF_ARCHIVE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "com.android.mail.action.NOTIF_DELETE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_1
    invoke-static {v2, v5}, Lcom/android/mail/utils/NotificationActionUtils;->createUndoNotification(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    invoke-static {v2, v5}, Lcom/android/mail/utils/NotificationActionUtils;->registerUndoTimeout(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    goto :goto_0

    :cond_2
    const-string v8, "com.android.mail.action.NOTIF_UNDO_TIMEOUT"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "com.android.mail.action.NOTIF_DESTRUCT"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_3
    invoke-static {p0, v5}, Lcom/android/mail/utils/NotificationActionUtils;->cancelUndoTimeout(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    invoke-static {p0, v5}, Lcom/android/mail/utils/NotificationActionUtils;->processUndoNotification(Landroid/content/Context;Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;)V

    :cond_4
    :goto_1
    invoke-static {v2}, Lcom/android/mail/utils/NotificationActionUtils;->resendNotifications(Landroid/content/Context;)V

    goto :goto_0

    :cond_5
    const-string v8, "com.android.mail.action.NOTIF_MARK_READ"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v6, v4, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7, v9}, Landroid/content/ContentValues;-><init>(I)V

    sget-object v8, Lcom/android/mail/providers/UIProvider$MessageColumns;->READ:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1, v6, v7, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method
