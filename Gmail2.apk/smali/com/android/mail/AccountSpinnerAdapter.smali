.class public Lcom/android/mail/AccountSpinnerAdapter;
.super Landroid/widget/BaseAdapter;
.source "AccountSpinnerAdapter.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field final mAccountObserver:Lcom/android/mail/providers/AccountObserver;

.field private mAllAccounts:[Lcom/android/mail/providers/Account;

.field private final mContext:Landroid/content/Context;

.field private mCurrentAccount:Lcom/android/mail/providers/Account;

.field private mCurrentFolder:Lcom/android/mail/providers/Folder;

.field private final mFolderWatcher:Lcom/android/mail/providers/FolderWatcher;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mNumAccounts:I

.field private mRecentFolderController:Lcom/android/mail/ui/RecentFolderController;

.field private mRecentFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/providers/Folder;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

.field private mRecentFolders:Lcom/android/mail/ui/RecentFolderList;

.field private mRecentFoldersVisible:Z

.field private final mShowAllFoldersItem:Z

.field private mSpinnerRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/AccountSpinnerAdapter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/mail/ui/ControllableActivity;Landroid/content/Context;Z)V
    .locals 2
    .param p1    # Lcom/android/mail/ui/ControllableActivity;
    .param p2    # Landroid/content/Context;
    .param p3    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    iput v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    new-array v0, v1, [Lcom/android/mail/providers/Account;

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAllAccounts:[Lcom/android/mail/providers/Account;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/mail/AccountSpinnerAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/mail/AccountSpinnerAdapter$1;-><init>(Lcom/android/mail/AccountSpinnerAdapter;)V

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mSpinnerRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    new-instance v0, Lcom/android/mail/AccountSpinnerAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/mail/AccountSpinnerAdapter$2;-><init>(Lcom/android/mail/AccountSpinnerAdapter;)V

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    iput-object p2, p0, Lcom/android/mail/AccountSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-boolean p3, p0, Lcom/android/mail/AccountSpinnerAdapter;->mShowAllFoldersItem:Z

    new-instance v0, Lcom/android/mail/providers/FolderWatcher;

    invoke-direct {v0, p1, p0}, Lcom/android/mail/providers/FolderWatcher;-><init>(Lcom/android/mail/ui/RestrictedActivity;Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mFolderWatcher:Lcom/android/mail/providers/FolderWatcher;

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getRecentFolderController()Lcom/android/mail/ui/RecentFolderController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderController:Lcom/android/mail/ui/RecentFolderController;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/AccountSpinnerAdapter;)V
    .locals 0
    .param p0    # Lcom/android/mail/AccountSpinnerAdapter;

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->requestRecentFolders()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/mail/AccountSpinnerAdapter;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/mail/AccountSpinnerAdapter;

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->getCurrentAccountUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/mail/AccountSpinnerAdapter;Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/Account;
    .locals 0
    .param p0    # Lcom/android/mail/AccountSpinnerAdapter;
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/mail/AccountSpinnerAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/mail/AccountSpinnerAdapter;

    iget-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/mail/AccountSpinnerAdapter;)[Lcom/android/mail/providers/Account;
    .locals 1
    .param p0    # Lcom/android/mail/AccountSpinnerAdapter;

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAllAccounts:[Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/AccountSpinnerAdapter;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getAccount(I)Lcom/android/mail/providers/Account;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAllAccounts:[Lcom/android/mail/providers/Account;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method private getCurrentAccountName()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->isCurrentAccountInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private getCurrentAccountUri()Landroid/net/Uri;
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->isCurrentAccountInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    goto :goto_0
.end method

.method private final getRecentOffset(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private isCurrentAccountInvalid()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentAccount:Lcom/android/mail/providers/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestRecentFolders()V
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentFolder:Lcom/android/mail/providers/Folder;

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolders:Lcom/android/mail/ui/RecentFolderList;

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/RecentFolderList;->getRecentFolderList(Landroid/net/Uri;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/mail/AccountSpinnerAdapter;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method private static setText(Landroid/view/View;ILjava/lang/String;)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private final setUnreadCount(Landroid/view/View;II)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-gtz p3, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, p3}, Lcom/android/mail/utils/Utils;->getUnreadCountString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/RecentFolderObserver;->unregisterAndDestroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    :cond_0
    return-void
.end method

.method public disableRecentFolders()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/RecentFolderObserver;->unregisterAndDestroy()V

    iput-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    :cond_0
    iput-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolders:Lcom/android/mail/ui/RecentFolderList;

    invoke-virtual {p0}, Lcom/android/mail/AccountSpinnerAdapter;->notifyDataSetChanged()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    :cond_1
    return-void
.end method

.method public enableRecentFolders()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mSpinnerRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderObserver:Lcom/android/mail/providers/RecentFolderObserver;

    iget-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderController:Lcom/android/mail/ui/RecentFolderController;

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/RecentFolderObserver;->initialize(Lcom/android/mail/ui/RecentFolderController;)Lcom/android/mail/ui/RecentFolderList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolders:Lcom/android/mail/ui/RecentFolderList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->requestRecentFolders()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/AccountSpinnerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    move v1, v2

    :goto_0
    iget-boolean v3, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFoldersVisible:Z

    if-eqz v3, :cond_2

    if-lez v1, :cond_2

    add-int/lit8 v3, v1, 0x1

    iget-boolean v4, p0, Lcom/android/mail/AccountSpinnerAdapter;->mShowAllFoldersItem:Z

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    add-int v0, v3, v2

    :goto_1
    iget v2, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    add-int/2addr v2, v0

    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getRecentOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getAccount(I)Lcom/android/mail/providers/Account;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-string v0, "account spinner header"

    goto :goto_0

    :pswitch_3
    const-string v0, "show all folders"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getRecentOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Folder;

    iget-object v1, v1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    int-to-long v1, v1

    :goto_0
    return-wide v1

    :pswitch_1
    int-to-long v1, v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getAccount(I)Lcom/android/mail/providers/Account;

    move-result-object v1

    iget-object v1, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    int-to-long v1, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    if-ne p1, v0, :cond_1

    const/4 v0, -0x2

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mShowAllFoldersItem:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getRecentOffset(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object/from16 v8, p2

    invoke-virtual {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getItemViewType(I)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    sget-object v9, Lcom/android/mail/AccountSpinnerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v10, "AccountSpinnerAdapter.getView(): Unknown type: %d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    move-object v9, v8

    :goto_1
    return-object v9

    :pswitch_1
    if-nez v8, :cond_1

    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040001

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v9, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getAccount(I)Lcom/android/mail/providers/Account;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v9, Lcom/android/mail/AccountSpinnerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v10, "AccountSpinnerAdapter(%d): Null account at position."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    iget v2, v1, Lcom/android/mail/providers/Account;->color:I

    const v9, 0x7f080015

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v2, :cond_3

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_2
    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mFolderWatcher:Lcom/android/mail/providers/FolderWatcher;

    iget-object v10, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v10, v10, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Lcom/android/mail/providers/FolderWatcher;->get(Landroid/net/Uri;)Lcom/android/mail/providers/Folder;

    move-result-object v5

    if-eqz v5, :cond_4

    iget v7, v5, Lcom/android/mail/providers/Folder;->unreadCount:I

    :goto_3
    const v9, 0x7f080013

    iget-object v10, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v10, v10, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/android/mail/AccountSpinnerAdapter;->setText(Landroid/view/View;ILjava/lang/String;)V

    const v9, 0x7f080014

    iget-object v10, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/android/mail/AccountSpinnerAdapter;->setText(Landroid/view/View;ILjava/lang/String;)V

    const v9, 0x7f080016

    invoke-direct {p0, v8, v9, v7}, Lcom/android/mail/AccountSpinnerAdapter;->setUnreadCount(Landroid/view/View;II)V

    goto :goto_0

    :cond_3
    const/16 v9, 0x8

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_3

    :pswitch_2
    if-nez v8, :cond_5

    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040004

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v9, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    :cond_5
    const v9, 0x7f080017

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/android/mail/AccountSpinnerAdapter;->setText(Landroid/view/View;ILjava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    if-nez v8, :cond_6

    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040002

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v9, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    :cond_6
    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getRecentOffset(I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/mail/providers/Folder;

    const v9, 0x7f080015

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/android/mail/providers/Folder;->setFolderBlockColor(Lcom/android/mail/providers/Folder;Landroid/view/View;)V

    const v9, 0x7f080013

    iget-object v10, v4, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/android/mail/AccountSpinnerAdapter;->setText(Landroid/view/View;ILjava/lang/String;)V

    const v9, 0x7f080016

    iget v10, v4, Lcom/android/mail/providers/Folder;->unreadCount:I

    invoke-direct {p0, v8, v9, v10}, Lcom/android/mail/AccountSpinnerAdapter;->setUnreadCount(Landroid/view/View;II)V

    goto/16 :goto_0

    :pswitch_4
    if-nez v8, :cond_0

    iget-object v9, p0, Lcom/android/mail/AccountSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040003

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v9, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final hasRecentFolders()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mRecentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/AccountSpinnerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onFolderUpdated(Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0}, Lcom/android/mail/AccountSpinnerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setAccountArray([Lcom/android/mail/providers/Account;)V
    .locals 9
    .param p1    # [Lcom/android/mail/providers/Account;

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->getCurrentAccountUri()Landroid/net/Uri;

    move-result-object v0

    iput-object p1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAllAccounts:[Lcom/android/mail/providers/Account;

    array-length v4, p1

    iput v4, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->isCurrentAccountInvalid()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p1, v0}, Lcom/android/mail/providers/Account;->findPosition([Lcom/android/mail/providers/Account;Landroid/net/Uri;)I

    move-result v2

    sget-object v4, Lcom/android/mail/AccountSpinnerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v5, "setAccountArray: mCurrentAccountPos = %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget v4, p0, Lcom/android/mail/AccountSpinnerAdapter;->mNumAccounts:I

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/android/mail/AccountSpinnerAdapter;->mAllAccounts:[Lcom/android/mail/providers/Account;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v3, v4, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/AccountSpinnerAdapter;->mFolderWatcher:Lcom/android/mail/providers/FolderWatcher;

    invoke-virtual {v4, v3}, Lcom/android/mail/providers/FolderWatcher;->startWatching(Landroid/net/Uri;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/AccountSpinnerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setCurrentFolder(Lcom/android/mail/providers/Folder;)Z
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentFolder:Lcom/android/mail/providers/Folder;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/android/mail/AccountSpinnerAdapter;->mCurrentFolder:Lcom/android/mail/providers/Folder;

    invoke-direct {p0}, Lcom/android/mail/AccountSpinnerAdapter;->requestRecentFolders()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
