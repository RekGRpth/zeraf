.class public Lcom/android/mail/browse/ConversationItemView;
.super Landroid/view/View;
.source "ConversationItemView.java"

# interfaces
.implements Lcom/android/mail/browse/ToggleableItem;
.implements Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;
.implements Lcom/android/mail/ui/SwipeableItemView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;,
        Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;
    }
.end annotation


# static fields
.field private static ATTACHMENT:Landroid/graphics/Bitmap;

.field private static CHECKMARK_OFF:Landroid/graphics/Bitmap;

.field private static CHECKMARK_ON:Landroid/graphics/Bitmap;

.field public static final DEFAULT_AVATAR_PROVIDER:Lcom/android/mail/photomanager/LetterTileProvider;

.field private static IMPORTANT_ONLY_TO_ME:Landroid/graphics/Bitmap;

.field private static IMPORTANT_TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

.field private static IMPORTANT_TO_OTHERS:Landroid/graphics/Bitmap;

.field private static final LOG_TAG:Ljava/lang/String;

.field private static MORE_FOLDERS:Landroid/graphics/Bitmap;

.field private static ONLY_TO_ME:Landroid/graphics/Bitmap;

.field private static STAR_OFF:Landroid/graphics/Bitmap;

.field private static STAR_ON:Landroid/graphics/Bitmap;

.field private static STATE_CALENDAR_INVITE:Landroid/graphics/Bitmap;

.field private static STATE_FORWARDED:Landroid/graphics/Bitmap;

.field private static STATE_REPLIED:Landroid/graphics/Bitmap;

.field private static STATE_REPLIED_AND_FORWARDED:Landroid/graphics/Bitmap;

.field private static TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

.field private static sActivatedTextColor:I

.field private static sActivatedTextSpan:Landroid/text/style/CharacterStyle;

.field private static sAnimatingBackgroundColor:I

.field private static sContactPhotoManager:Lcom/android/mail/photomanager/ContactPhotoManager;

.field private static sDateTextColor:I

.field private static sElidedPaddingToken:Ljava/lang/String;

.field private static sFadedActivatedColor:I

.field private static sFoldersLeftPadding:I

.field private static sFoldersPaint:Landroid/text/TextPaint;

.field private static sLayoutCount:I

.field private static sPaint:Landroid/text/TextPaint;

.field private static sScrollSlop:I

.field private static sSendersSplitToken:Ljava/lang/String;

.field private static sSendersTextColorRead:I

.field private static sSendersTextColorUnread:I

.field private static sSendersTextViewHeight:I

.field private static sSendersTextViewTopPadding:I

.field private static sShrinkAnimationDuration:I

.field private static sSlideAnimationDuration:I

.field private static sSnippetTextReadSpan:Landroid/text/style/ForegroundColorSpan;

.field private static sSnippetTextUnreadSpan:Landroid/text/style/ForegroundColorSpan;

.field private static sStandardScaledDimen:I

.field private static sSubjectTextReadSpan:Landroid/text/style/TextAppearanceSpan;

.field private static sSubjectTextUnreadSpan:Landroid/text/style/TextAppearanceSpan;

.field private static sTimer:Lcom/android/mail/perf/Timer;

.field private static sTouchSlop:I


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

.field private mAnimatedHeight:I

.field private mBackgroundOverride:I

.field private final mBackgrounds:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckboxesEnabled:Z

.field private mChecked:Z

.field private mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

.field private final mContext:Landroid/content/Context;

.field private mConvListPhotosEnabled:Z

.field mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

.field private mDateTextView:Landroid/widget/TextView;

.field private mDateX:I

.field private mDisplayedFolder:Lcom/android/mail/providers/Folder;

.field private mDownEvent:Z

.field private mFoldersXEnd:I

.field public mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

.field private mLastTouchX:I

.field private mLastTouchY:I

.field private mMode:I

.field private mPaperclipX:I

.field private mPreviousMode:I

.field private mPriorityMarkersEnabled:Z

.field private mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

.field private mSendersTextView:Landroid/widget/TextView;

.field private mSendersWidth:I

.field private mStarEnabled:Z

.field private mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

.field private mSwipeEnabled:Z

.field private final mTabletDevice:Z

.field private mTesting:Z

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/browse/ConversationItemView;->sLayoutCount:I

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationItemView;->LOG_TAG:Ljava/lang/String;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/browse/ConversationItemView;->sFadedActivatedColor:I

    new-instance v0, Lcom/android/mail/photomanager/LetterTileProvider;

    invoke-direct {v0}, Lcom/android/mail/photomanager/LetterTileProvider;-><init>()V

    sput-object v0, Lcom/android/mail/browse/ConversationItemView;->DEFAULT_AVATAR_PROVIDER:Lcom/android/mail/photomanager/LetterTileProvider;

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, -0x1

    const/4 v4, -0x2

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgrounds:Landroid/util/SparseArray;

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mViewWidth:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mTesting:Z

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    invoke-virtual {p0, v5}, Lcom/android/mail/browse/ConversationItemView;->setClickable(Z)V

    invoke-virtual {p0, v5}, Lcom/android/mail/browse/ConversationItemView;->setLongClickable(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mTabletDevice:Z

    iput-object p2, p0, Lcom/android/mail/browse/ConversationItemView;->mAccount:Ljava/lang/String;

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->CHECKMARK_OFF:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    const v1, 0x7f020008

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->CHECKMARK_OFF:Landroid/graphics/Bitmap;

    const v1, 0x7f020009

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->CHECKMARK_ON:Landroid/graphics/Bitmap;

    const v1, 0x7f02000c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STAR_OFF:Landroid/graphics/Bitmap;

    const v1, 0x7f020010

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STAR_ON:Landroid/graphics/Bitmap;

    const v1, 0x7f020041

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->ONLY_TO_ME:Landroid/graphics/Bitmap;

    const v1, 0x7f020044

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

    const v1, 0x7f020042

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_ONLY_TO_ME:Landroid/graphics/Bitmap;

    const v1, 0x7f020045

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

    const v1, 0x7f020043

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_TO_OTHERS:Landroid/graphics/Bitmap;

    const v1, 0x7f020038

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->ATTACHMENT:Landroid/graphics/Bitmap;

    const v1, 0x7f020047

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->MORE_FOLDERS:Landroid/graphics/Bitmap;

    const v1, 0x7f02003c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STATE_REPLIED:Landroid/graphics/Bitmap;

    const v1, 0x7f020039

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STATE_FORWARDED:Landroid/graphics/Bitmap;

    const v1, 0x7f02003b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STATE_REPLIED_AND_FORWARDED:Landroid/graphics/Bitmap;

    const v1, 0x7f02003a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->STATE_CALENDAR_INVITE:Landroid/graphics/Bitmap;

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextColor:I

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    sget v2, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextColor:I

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextSpan:Landroid/text/style/CharacterStyle;

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sSendersTextColorRead:I

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sSendersTextColorUnread:I

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0055

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sSubjectTextUnreadSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    const v3, 0x7f0d0056

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sSubjectTextReadSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sSnippetTextUnreadSpan:Landroid/text/style/ForegroundColorSpan;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sSnippetTextReadSpan:Landroid/text/style/ForegroundColorSpan;

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sDateTextColor:I

    const v1, 0x7f0c002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sTouchSlop:I

    const v1, 0x7f0c002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sStandardScaledDimen:I

    const v1, 0x7f0b000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sShrinkAnimationDuration:I

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sSlideAnimationDuration:I

    const v1, 0x7f09010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sSendersSplitToken:Ljava/lang/String;

    const v1, 0x7f090111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sElidedPaddingToken:Ljava/lang/String;

    const v1, 0x7f0a002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sAnimatingBackgroundColor:I

    const v1, 0x7f0c0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewTopPadding:I

    const v1, 0x7f0c006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewHeight:I

    const v1, 0x7f0b0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sScrollSlop:I

    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemView;->sFoldersLeftPadding:I

    invoke-static {p1}, Lcom/android/mail/photomanager/ContactPhotoManager;->createContactPhotoManager(Landroid/content/Context;)Lcom/android/mail/photomanager/ContactPhotoManager;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/ConversationItemView;->sContactPhotoManager:Lcom/android/mail/photomanager/ContactPhotoManager;

    :cond_0
    new-instance v1, Lcom/android/mail/ui/EllipsizedMultilineTextView;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/mail/ui/EllipsizedMultilineTextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->setMaxLines(I)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mDateTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mDateTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mDateTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Lcom/android/mail/ui/DividedImageCanvas;

    invoke-direct {v1, p1, p0}, Lcom/android/mail/ui/DividedImageCanvas;-><init>(Landroid/content/Context;Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;)V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    return-void
.end method

.method static synthetic access$000()Landroid/text/TextPaint;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method static synthetic access$100(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->getPadding(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$200()Landroid/graphics/Bitmap;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->MORE_FOLDERS:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$400()Landroid/text/TextPaint;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method private beginDragMode()V
    .locals 8

    const/4 v1, 0x1

    const/4 v6, 0x0

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchX:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchY:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleCheckMark()V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->size()I

    move-result v3

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    const v2, 0x7f100001

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/android/mail/providers/Conversation;->MOVE_CONVERSATIONS_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v7

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    new-instance v4, Landroid/content/ClipData$Item;

    iget v0, v0, Lcom/android/mail/providers/Conversation;->position:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v4}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getHeight()I

    move-result v4

    if-ltz v2, :cond_4

    if-gez v4, :cond_5

    :cond_4
    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "ConversationItemView: dimension is negative: width=%d, height=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v3, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_5
    move v0, v6

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->startDragMode()V

    new-instance v0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;

    iget v4, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchX:I

    iget v5, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchY:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;-><init>(Lcom/android/mail/browse/ConversationItemView;Landroid/view/View;III)V

    const/4 v1, 0x0

    invoke-virtual {p0, v7, v0, v1, v6}, Lcom/android/mail/browse/ConversationItemView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto/16 :goto_0
.end method

.method private bind(Lcom/android/mail/browse/ConversationItemViewModel;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V
    .locals 6
    .param p1    # Lcom/android/mail/browse/ConversationItemViewModel;
    .param p2    # Lcom/android/mail/ui/ControllableActivity;
    .param p3    # Lcom/android/mail/ui/ConversationSelectionSet;
    .param p4    # Lcom/android/mail/providers/Folder;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-virtual {v2}, Lcom/android/mail/ui/DividedImageCanvas;->getDivisionIds()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sContactPhotoManager:Lcom/android/mail/photomanager/ContactPhotoManager;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v4, v1, v2}, Lcom/android/mail/ui/DividedImageCanvas;->generateHash(Lcom/android/mail/ui/DividedImageCanvas;ILjava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/android/mail/photomanager/ContactPhotoManager;->removePhoto(Ljava/lang/Long;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iput-object p2, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iput-object p3, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    iput-object p4, p0, Lcom/android/mail/browse/ConversationItemView;->mDisplayedFolder:Lcom/android/mail/providers/Folder;

    if-nez p5, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCheckboxesEnabled:Z

    invoke-interface {p2}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/preferences/MailPrefs;->get(Landroid/content/Context;)Lcom/android/mail/preferences/MailPrefs;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mail/preferences/MailPrefs;->areConvListPhotosEnabled()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lcom/android/mail/providers/Folder;->isTrash()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mStarEnabled:Z

    iput-boolean p6, p0, Lcom/android/mail/browse/ConversationItemView;->mSwipeEnabled:Z

    iput-boolean p7, p0, Lcom/android/mail/browse/ConversationItemView;->mPriorityMarkersEnabled:Z

    iput-object p8, p0, Lcom/android/mail/browse/ConversationItemView;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->setContentDescription()V

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->requestLayout()V

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private calculateCoordinates()V
    .locals 10

    const/4 v8, 0x0

    const-string v0, "CCHV.coordinates"

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemView;->startTimer(Ljava/lang/String;)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateXEnd:I

    sget-object v2, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->dateText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->dateText:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    sub-int v0, v1, v0

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mDateX:I

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mDateX:I

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->ATTACHMENT:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mPaperclipX:I

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->isWideMode(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersXEnd:I

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mFoldersXEnd:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersWidth:I

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    :goto_1
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersY:I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersAscent:I

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->styledSenders:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->ellipsizeStyledSenders()I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->layoutSenders(Landroid/text/SpannableStringBuilder;)V

    :goto_2
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-gez v0, :cond_0

    iput v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    :cond_0
    const-string v0, "CCHV.coordinates"

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersXEnd:I

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mFoldersXEnd:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->paperclip:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mPaperclipX:I

    :goto_3
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mDateX:I

    goto :goto_3

    :cond_4
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->senderFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v8

    move v2, v8

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;

    iget-object v5, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->style:Landroid/text/style/CharacterStyle;

    iget v6, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->start:I

    iget v7, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->end:I

    sget-object v9, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v5, v9}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    sget-object v5, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v9, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v9, v9, Lcom/android/mail/browse/ConversationItemViewModel;->sendersText:Ljava/lang/String;

    invoke-virtual {v5, v9, v6, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->width:I

    iget-boolean v5, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->isFixed:Z

    if-eqz v5, :cond_5

    iget v5, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->width:I

    add-int/2addr v1, v5

    :cond_5
    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->width:I

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_4

    :cond_6
    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->displaySendersInline(I)Z

    move-result v0

    if-nez v0, :cond_9

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-gt v2, v0, :cond_8

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineHeight:I

    div-int/lit8 v0, v0, 0x2

    :goto_5
    add-int/2addr v0, v3

    :goto_6
    iget v2, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-gez v2, :cond_7

    iput v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    :cond_7
    invoke-direct {p0, v1, v0}, Lcom/android/mail/browse/ConversationItemView;->ellipsize(II)I

    iget-object v9, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    sget-object v2, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, v9, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayLayout:Landroid/text/StaticLayout;

    goto/16 :goto_2

    :cond_8
    move v0, v8

    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_6
.end method

.method private calculateTextsAndBitmaps()V
    .locals 15

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x1

    const-string v1, "CCHV.txtsbmps"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->startTimer(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showFolders:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    :goto_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mDisplayedFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v1, v2, v4}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->loadConversationFolders(Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;)V

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v1, v2}, Lcom/android/mail/ui/ConversationSelectionSet;->contains(Lcom/android/mail/providers/Conversation;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    :cond_1
    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCheckboxesEnabled:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    if-nez v1, :cond_3

    move v1, v12

    :goto_1
    iput-boolean v1, v2, Lcom/android/mail/browse/ConversationItemViewModel;->checkboxVisible:Z

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v9, v1, Lcom/android/mail/browse/ConversationItemViewModel;->unread:Z

    invoke-direct {p0, v9}, Lcom/android/mail/browse/ConversationItemView;->updateBackground(Z)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v1, v1, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    invoke-static {v0, v2}, Lcom/android/mail/browse/SendersView;->createMessageInfo(Landroid/content/Context;Lcom/android/mail/providers/Conversation;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->messageInfoString:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v2, v2, Lcom/android/mail/providers/Conversation;->hasAttachments:Z

    invoke-static {v0, v1, v2}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getSendersLength(Landroid/content/Context;IZ)I

    move-result v3

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderNames:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->styledSenders:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v1, v1, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->messageInfoString:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v4, v4, Lcom/android/mail/browse/ConversationItemViewModel;->styledSenders:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v5, v5, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderNames:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v6, v6, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/mail/browse/ConversationItemView;->mAccount:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/android/mail/browse/SendersView;->format(Landroid/content/Context;Lcom/android/mail/providers/ConversationInfo;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->loadSenderImages()V

    :goto_2
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v4, v4, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-wide v4, v4, Lcom/android/mail/providers/Conversation;->dateMs:J

    invoke-static {v2, v4, v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->dateText:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/ConversationItemViewModel;->isLayoutValid(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "CCHV.txtsbmps"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    :goto_3
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->reset()V

    goto/16 :goto_0

    :cond_3
    move v1, v13

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mail/browse/SendersView;->formatSenders(Lcom/android/mail/browse/ConversationItemViewModel;Landroid/content/Context;)V

    goto :goto_2

    :cond_5
    const-string v1, "CCHV.folders"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->startTimer(Ljava/lang/String;)V

    const-string v1, "CCHV.folders"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iput-object v14, v1, Lcom/android/mail/browse/ConversationItemViewModel;->paperclip:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v1, v1, Lcom/android/mail/providers/Conversation;->hasAttachments:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    sget-object v2, Lcom/android/mail/browse/ConversationItemView;->ATTACHMENT:Landroid/graphics/Bitmap;

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->paperclip:Landroid/graphics/Bitmap;

    :cond_6
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iput-object v14, v1, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showPersonalLevel:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget v10, v1, Lcom/android/mail/providers/Conversation;->personalLevel:I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget v1, v1, Lcom/android/mail/providers/Conversation;->priority:I

    if-ne v1, v12, :cond_8

    move v8, v12

    :goto_4
    if-eqz v8, :cond_9

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mPriorityMarkersEnabled:Z

    if-eqz v1, :cond_9

    move v11, v12

    :goto_5
    const/4 v1, 0x2

    if-ne v10, v1, :cond_b

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    if-eqz v11, :cond_a

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_ONLY_TO_ME:Landroid/graphics/Bitmap;

    :goto_6
    iput-object v1, v2, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    :cond_7
    :goto_7
    const-string v1, "CCHV.sendersubj"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->startTimer(Ljava/lang/String;)V

    const-string v1, "CCHV.sendersubj"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    const-string v1, "CCHV.txtsbmps"

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    move v8, v13

    goto :goto_4

    :cond_9
    move v11, v13

    goto :goto_5

    :cond_a
    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->ONLY_TO_ME:Landroid/graphics/Bitmap;

    goto :goto_6

    :cond_b
    if-ne v10, v12, :cond_d

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    if-eqz v11, :cond_c

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

    :goto_8
    iput-object v1, v2, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    goto :goto_7

    :cond_c
    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->TO_ME_AND_OTHERS:Landroid/graphics/Bitmap;

    goto :goto_8

    :cond_d
    if-eqz v11, :cond_7

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    sget-object v2, Lcom/android/mail/browse/ConversationItemView;->IMPORTANT_TO_OTHERS:Landroid/graphics/Bitmap;

    iput-object v2, v1, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    goto :goto_7
.end method

.method private canFitFragment(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineCount:I

    if-ne p2, v2, :cond_2

    add-int v2, p1, p3

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-gt v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-le p1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;
    .locals 4
    .param p1    # [Landroid/text/style/CharacterStyle;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v3, 0x0

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    aget-object v1, p1, v3

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-object v0
.end method

.method private createHeightAnimation(Z)Landroid/animation/ObjectAnimator;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v0

    if-eqz p1, :cond_0

    move v2, v1

    :goto_0
    if-eqz p1, :cond_1

    :goto_1
    const-string v3, "animatedHeight"

    const/4 v4, 0x2

    new-array v4, v4, [I

    aput v2, v4, v1

    const/4 v1, 0x1

    aput v0, v4, v1

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget v1, Lcom/android/mail/browse/ConversationItemView;->sShrinkAnimationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private createSubject(ZZ)V
    .locals 12
    .param p1    # Z
    .param p2    # Z

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v8, v8, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v8, v8, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/android/mail/browse/ConversationItemView;->filterTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v8, v8, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v8}, Lcom/android/mail/providers/Conversation;->getSnippet()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v8, v4, v2}, Lcom/android/mail/providers/Conversation;->getSubjectAndSnippetForDisplay(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz p1, :cond_4

    sget-object v8, Lcom/android/mail/browse/ConversationItemView;->sSubjectTextUnreadSpan:Landroid/text/style/TextAppearanceSpan;

    :goto_1
    invoke-static {v8}, Landroid/text/style/TextAppearanceSpan;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x21

    invoke-virtual {v0, v8, v9, v6, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    move v3, v6

    if-eqz p1, :cond_5

    sget-object v8, Lcom/android/mail/browse/ConversationItemView;->sSnippetTextUnreadSpan:Landroid/text/style/ForegroundColorSpan;

    :goto_2
    invoke-static {v8}, Landroid/text/style/ForegroundColorSpan;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v8

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    const/16 v10, 0x21

    invoke-virtual {v0, v8, v3, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->isActivated()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->showActivatedText()Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextSpan:Landroid/text/style/CharacterStyle;

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    const/16 v11, 0x12

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    iget v8, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    invoke-static {v8}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->isWideMode(I)Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v8, v8, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showFolders:Z

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v8, v8, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v8, v8, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    invoke-virtual {v8}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->hasVisibleFolders()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v8, v8, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectWidth:I

    iget-object v9, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget v10, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    invoke-static {v9, v10}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getFoldersWidth(Landroid/content/Context;I)I

    move-result v9

    iget-object v10, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v10, v10, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    iget v11, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    # invokes: Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->measureFolders(I)I
    invoke-static {v10, v11}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->access$300(Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;I)I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    sub-int/2addr v8, v9

    sget v9, Lcom/android/mail/browse/ConversationItemView;->sFoldersLeftPadding:I

    sub-int v1, v8, v9

    :goto_3
    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v7, v8, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectWidth:I

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    invoke-virtual {v8}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->getLineHeight()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    iget-object v9, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    invoke-virtual {v9}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9}, Landroid/text/TextPaint;->descent()F

    move-result v9

    add-float/2addr v8, v9

    float-to-int v5, v8

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    const/high16 v9, 0x40000000

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/high16 v10, 0x40000000

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->measure(II)V

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v7, v5}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->layout(IIII)V

    iget-object v8, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    invoke-virtual {v8, v0, v1}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->setText(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    return-void

    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_4
    sget-object v8, Lcom/android/mail/browse/ConversationItemView;->sSubjectTextReadSpan:Landroid/text/style/TextAppearanceSpan;

    goto/16 :goto_1

    :cond_5
    sget-object v8, Lcom/android/mail/browse/ConversationItemView;->sSnippetTextReadSpan:Landroid/text/style/ForegroundColorSpan;

    goto/16 :goto_2

    :cond_6
    const/4 v1, -0x1

    goto :goto_3
.end method

.method private createTranslateXAnimation(Z)Landroid/animation/ObjectAnimator;
    .locals 5

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/SwipeableListView;->getMeasuredWidth()I

    move-result v1

    move v2, v1

    :goto_0
    if-eqz p1, :cond_1

    int-to-float v1, v2

    :goto_1
    if-eqz p1, :cond_2

    :goto_2
    const-string v2, "translationX"

    const/4 v4, 0x2

    new-array v4, v4, [F

    aput v1, v4, v3

    const/4 v1, 0x1

    aput v0, v4, v1

    invoke-static {p0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget v1, Lcom/android/mail/browse/ConversationItemView;->sSlideAnimationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    int-to-float v0, v2

    goto :goto_2
.end method

.method private drawContactImages(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesX:F

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesY:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawSenders(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->inlinePersonalLevel:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showPersonalLevel:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    :goto_0
    int-to-float v1, v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersY:I

    sget v3, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewTopPadding:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->personalLevelX:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    goto :goto_0
.end method

.method private drawSubject(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectX:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectY:I

    sget v2, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewTopPadding:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mSubjectTextView:Lcom/android/mail/ui/EllipsizedMultilineTextView;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/EllipsizedMultilineTextView;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawText(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IILandroid/text/TextPaint;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/text/TextPaint;

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v4, p3

    int-to-float v5, p4

    move-object v0, p1

    move-object v1, p2

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private ellipsize(II)I
    .locals 18
    .param p1    # I
    .param p2    # I

    const/4 v12, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->senderFragments:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;

    iget-object v11, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->style:Landroid/text/style/CharacterStyle;

    iget v10, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->start:I

    iget v4, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->end:I

    iget v13, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->width:I

    iget-boolean v7, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->isFixed:Z

    sget-object v14, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v11, v14}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    if-eqz v3, :cond_0

    if-nez v7, :cond_0

    const/4 v14, 0x0

    iput-boolean v14, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->shouldDisplay:Z

    goto :goto_0

    :cond_0
    const/4 v14, 0x0

    iput-object v14, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->ellipsizedText:Ljava/lang/String;

    if-eqz v7, :cond_1

    sub-int p1, p1, v13

    :cond_1
    add-int v14, v12, v13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v14, v2, v1}, Lcom/android/mail/browse/ConversationItemView;->canFitFragment(III)Z

    move-result v14

    if-nez v14, :cond_4

    if-nez v12, :cond_5

    const/4 v3, 0x1

    :cond_2
    :goto_1
    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    sub-int v13, v14, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v14, v14, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineCount:I

    if-ne v2, v14, :cond_3

    sub-int v13, v13, p1

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->sendersText:Ljava/lang/String;

    invoke-virtual {v14, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    int-to-float v0, v13

    move/from16 v16, v0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static/range {v14 .. v17}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->ellipsizedText:Ljava/lang/String;

    sget-object v14, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v15, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->ellipsizedText:Ljava/lang/String;

    invoke-virtual {v14, v15}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v14

    float-to-int v13, v14

    :cond_4
    const/4 v14, 0x1

    iput-boolean v14, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->shouldDisplay:Z

    add-int/2addr v12, v13

    iget-object v14, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->ellipsizedText:Ljava/lang/String;

    if-eqz v14, :cond_7

    iget-object v5, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->ellipsizedText:Ljava/lang/String;

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v14}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v14, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    iget-object v15, v8, Lcom/android/mail/browse/ConversationItemViewModel$SenderFragment;->style:Landroid/text/style/CharacterStyle;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayText:Landroid/text/SpannableStringBuilder;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v16

    const/16 v17, 0x21

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v15, v9, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v14, v14, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineCount:I

    if-ge v2, v14, :cond_6

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v14, v14, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineHeight:I

    add-int p2, p2, v14

    const/4 v12, 0x0

    add-int v14, v12, v13

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    if-le v14, v15, :cond_2

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v14, v14, Lcom/android/mail/browse/ConversationItemViewModel;->sendersText:Ljava/lang/String;

    invoke-virtual {v14, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_8
    return v12
.end method

.method private ellipsizeStyledSenders()I
    .locals 14

    const/4 v3, 0x0

    const/4 v5, 0x0

    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v9, v0, Lcom/android/mail/browse/ConversationItemViewModel;->messageInfoString:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v2, Landroid/text/style/CharacterStyle;

    invoke-virtual {v9, v5, v0, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/CharacterStyle;

    array-length v2, v0

    if-lez v2, :cond_0

    aget-object v0, v0, v5

    sget-object v2, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    :cond_0
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    add-float/2addr v0, v1

    :goto_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->styledSenders:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-object v2, v3

    move v4, v5

    move v6, v0

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    if-eqz v0, :cond_1

    if-eqz v4, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    iput v1, v0, Lcom/android/mail/browse/ConversationItemViewModel;->styledMessageInfoStringOffset:I

    if-eqz v9, :cond_3

    invoke-virtual {v8, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iput-object v8, v0, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    float-to-int v0, v6

    return v0

    :cond_4
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v7, Landroid/text/style/CharacterStyle;

    invoke-virtual {v0, v5, v1, v7}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/CharacterStyle;

    array-length v7, v1

    if-lez v7, :cond_5

    aget-object v7, v1, v5

    sget-object v11, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v11}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    :cond_5
    sget-object v7, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/mail/browse/ConversationItemView;->sElidedPaddingToken:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/android/mail/browse/ConversationItemView;->sElidedPaddingToken:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/mail/browse/ConversationItemView;->copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    move-object v13, v2

    move-object v2, v0

    move-object v0, v13

    :goto_2
    array-length v7, v1

    if-lez v7, :cond_6

    aget-object v7, v1, v5

    sget-object v11, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v11}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    :cond_6
    sget-object v7, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    int-to-float v7, v7

    add-float v11, v7, v6

    iget v12, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    int-to-float v12, v12

    cmpl-float v11, v11, v12

    if-lez v11, :cond_b

    const/4 v7, 0x1

    iget v4, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    int-to-float v4, v4

    sub-float/2addr v4, v6

    sget-object v11, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v11, v4, v12}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {p0, v1, v4}, Lcom/android/mail/browse/ConversationItemView;->copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    sget-object v4, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    move v13, v4

    move v4, v7

    move v7, v13

    :goto_3
    add-float/2addr v6, v7

    if-eqz v1, :cond_7

    move-object v0, v1

    :cond_7
    invoke-virtual {v8, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_a

    if-eqz v2, :cond_9

    sget-object v7, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/mail/browse/ConversationItemView;->sSendersSplitToken:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/mail/browse/ConversationItemView;->copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    move-object v13, v2

    move-object v2, v0

    move-object v0, v13

    goto :goto_2

    :cond_a
    move-object v2, v0

    goto :goto_2

    :cond_b
    move-object v1, v3

    goto :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method private filterTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5b

    if-ne v1, v2, :cond_0

    const/16 v1, 0x5d

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x7

    invoke-static {v2, v4}, Lcom/android/mail/utils/Utils;->ellipsize(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v5

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private final getCheckedActivatedBackground()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mTabletDevice:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020072

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020082

    goto :goto_0
.end method

.method private getFontColor(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->isActivated()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->showActivatedText()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    sget p1, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextColor:I

    :cond_0
    return p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getListView()Lcom/android/mail/ui/SwipeableListView;
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/SwipeableConversationItemView;

    invoke-virtual {v1}, Lcom/android/mail/browse/SwipeableConversationItemView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/SwipeableListView;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v1}, Lcom/android/mail/ui/AnimatedAdapter;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static getPadding(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    sub-int v0, p0, p1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getStarBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v0, v0, Lcom/android/mail/providers/Conversation;->starred:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STAR_ON:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STAR_OFF:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private isTouchInCheckmark(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->checkboxVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    sget v1, Lcom/android/mail/browse/ConversationItemView;->sTouchSlop:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTouchInStar(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mStarEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starX:I

    sget v1, Lcom/android/mail/browse/ConversationItemView;->sTouchSlop:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutSenders(Landroid/text/SpannableStringBuilder;)V
    .locals 9
    .param p1    # Landroid/text/SpannableStringBuilder;

    const/high16 v8, 0x40000000

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->isActivated()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->showActivatedText()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    sget-object v4, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextSpan:Landroid/text/style/CharacterStyle;

    iget-object v5, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget v5, v5, Lcom/android/mail/browse/ConversationItemViewModel;->styledMessageInfoStringOffset:I

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v7, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_0
    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    sget v3, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewHeight:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->measure(II)V

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mSendersWidth:I

    sget v4, Lcom/android/mail/browse/ConversationItemView;->sSendersTextViewHeight:I

    invoke-virtual {v1, v7, v7, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->styledSendersString:Landroid/text/SpannableStringBuilder;

    sget-object v4, Lcom/android/mail/browse/ConversationItemView;->sActivatedTextSpan:Landroid/text/style/CharacterStyle;

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private loadSenderImages()V
    .locals 8

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesWidth:I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/ui/DividedImageCanvas;->setDimensions(II)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/DividedImageCanvas;->setDivisionIds(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v6, 0x0

    :goto_0
    const/4 v0, 0x4

    if-ge v6, v0, :cond_0

    if-ge v6, v7, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderEmails:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sContactPhotoManager:Lcom/android/mail/photomanager/ContactPhotoManager;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-static {v1, v6, v4}, Lcom/android/mail/ui/DividedImageCanvas;->generateHash(Lcom/android/mail/ui/DividedImageCanvas;ILjava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContactImagesHolder:Lcom/android/mail/ui/DividedImageCanvas;

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->displayableSenderNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v5, Lcom/android/mail/browse/ConversationItemView;->DEFAULT_AVATAR_PROVIDER:Lcom/android/mail/photomanager/LetterTileProvider;

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/photomanager/ContactPhotoManager;->loadThumbnail(Ljava/lang/Long;Lcom/android/mail/ui/DividedImageCanvas;Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private measureHeight(II)I
    .locals 4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/high16 v1, 0x40000000

    if-ne v2, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getHeight(Landroid/content/Context;I)I

    move-result v1

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private onTouchEventNoSwipe(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v1, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    iput v1, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchX:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchY:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_1
    return v0

    :pswitch_1
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-direct {p0, v3, v4}, Lcom/android/mail/browse/ConversationItemView;->isTouchInCheckmark(FF)Z

    move-result v3

    if-nez v3, :cond_2

    int-to-float v3, v1

    int-to-float v4, v2

    invoke-direct {p0, v3, v4}, Lcom/android/mail/browse/ConversationItemView;->isTouchInStar(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    goto :goto_0

    :pswitch_3
    iget-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    if-eqz v3, :cond_0

    int-to-float v3, v1

    int-to-float v4, v2

    invoke-direct {p0, v3, v4}, Lcom/android/mail/browse/ConversationItemView;->isTouchInCheckmark(FF)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleCheckMark()V

    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-direct {p0, v3, v4}, Lcom/android/mail/browse/ConversationItemView;->isTouchInStar(FF)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleStar()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static pauseTimer(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    invoke-virtual {v0, p0}, Lcom/android/mail/perf/Timer;->pause(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setContentDescription()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->isAccessibilityEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationItemViewModel;->resetContentDescription()V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationItemViewModel;->getContentDescription(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private showActivatedText()Z
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mTabletDevice:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static startTimer(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    invoke-virtual {v0, p0}, Lcom/android/mail/perf/Timer;->start(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private toggleCheckMark()V
    .locals 4

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/SwipeableListView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    :goto_1
    iput v2, v0, Lcom/android/mail/providers/Conversation;->position:I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v2, p0, v0}, Lcom/android/mail/ui/ConversationSelectionSet;->toggle(Lcom/android/mail/browse/ConversationItemView;Lcom/android/mail/providers/Conversation;)V

    :cond_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mSelectedConversationSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v2}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v3}, Lcom/android/mail/ui/SwipeableListView;->commitDestructiveActions(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->requestLayout()V

    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private updateBackground(Z)V
    .locals 2

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mTabletDevice:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz p1, :cond_5

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v0, :cond_2

    const v0, 0x7f02007a

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const v0, 0x7f02001a

    goto :goto_2

    :cond_3
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getCheckedActivatedBackground()I

    move-result v0

    goto :goto_2

    :cond_4
    const v0, 0x7f020018

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v0, :cond_6

    const v0, 0x7f020076

    goto :goto_2

    :cond_6
    const v0, 0x7f020019

    goto :goto_2

    :cond_7
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getCheckedActivatedBackground()I

    move-result v0

    goto :goto_2

    :cond_8
    const v0, 0x7f020017

    goto :goto_2
.end method


# virtual methods
.method public bind(Landroid/database/Cursor;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V
    .locals 9
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/android/mail/ui/ControllableActivity;
    .param p3    # Lcom/android/mail/ui/ConversationSelectionSet;
    .param p4    # Lcom/android/mail/providers/Folder;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mAccount:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/mail/browse/ConversationItemViewModel;->forCursor(Ljava/lang/String;Landroid/database/Cursor;)Lcom/android/mail/browse/ConversationItemViewModel;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/android/mail/browse/ConversationItemView;->bind(Lcom/android/mail/browse/ConversationItemViewModel;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V

    return-void
.end method

.method public bind(Lcom/android/mail/providers/Conversation;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V
    .locals 9
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/ui/ControllableActivity;
    .param p3    # Lcom/android/mail/ui/ConversationSelectionSet;
    .param p4    # Lcom/android/mail/providers/Folder;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Lcom/android/mail/ui/AnimatedAdapter;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mAccount:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/mail/browse/ConversationItemViewModel;->forConversation(Ljava/lang/String;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/browse/ConversationItemViewModel;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/android/mail/browse/ConversationItemView;->bind(Lcom/android/mail/browse/ConversationItemViewModel;Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;ZZZLcom/android/mail/ui/AnimatedAdapter;)V

    return-void
.end method

.method public canChildBeDismissed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public dismiss()V
    .locals 2

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/SwipeableListView;->dismissChild(Lcom/android/mail/browse/ConversationItemView;)V

    :cond_0
    return-void
.end method

.method public getConversation()Lcom/android/mail/providers/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    return-object v0
.end method

.method public getMinAllowScrollDistance()F
    .locals 1

    sget v0, Lcom/android/mail/browse/ConversationItemView;->sScrollSlop:I

    int-to-float v0, v0

    return v0
.end method

.method public getSwipeableView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 1
    .param p1    # Landroid/view/DragEvent;

    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->stopDragMode()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->drawContactImages(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showPersonalLevel:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->personalLevelBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->personalLevelX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->personalLevelY:I

    int-to-float v2, v2

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->unread:Z

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_c

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersFontSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-static {v0}, Lcom/android/mail/browse/SendersView;->getTypeface(Z)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    if-eqz v0, :cond_b

    sget v0, Lcom/android/mail/browse/ConversationItemView;->sSendersTextColorUnread:I

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->getFontColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersY:I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getTopPadding()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->sendersDisplayLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->drawSubject(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showFolders:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->folderDisplayer:Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, p0, Lcom/android/mail/browse/ConversationItemView;->mFoldersXEnd:I

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->drawFolders(Landroid/graphics/Canvas;Lcom/android/mail/browse/ConversationItemViewCoordinates;II)V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget v0, v0, Lcom/android/mail/providers/Conversation;->color:I

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget v1, v1, Lcom/android/mail/providers/Conversation;->color:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getColorBlockWidth(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getColorBlockHeight(Landroid/content/Context;)I

    move-result v4

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateXEnd:I

    sub-int v0, v1, v0

    int-to-float v1, v0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateXEnd:I

    int-to-float v3, v0

    int-to-float v4, v4

    sget-object v5, Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showReplyState:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->hasBeenRepliedTo:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->hasBeenForwarded:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STATE_REPLIED_AND_FORWARDED:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_4
    :goto_3
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    sget v1, Lcom/android/mail/browse/ConversationItemView;->sDateTextColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v0, Lcom/android/mail/browse/ConversationItemViewModel;->dateText:Ljava/lang/CharSequence;

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mDateX:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateY:I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateAscent:I

    sub-int v4, v0, v1

    sget-object v5, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/ConversationItemView;->drawText(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IILandroid/text/TextPaint;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->paperclip:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->paperclip:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/mail/browse/ConversationItemView;->mPaperclipX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->paperclipY:I

    int-to-float v2, v2

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->faded:Z

    if-eqz v0, :cond_7

    sget v0, Lcom/android/mail/browse/ConversationItemView;->sFadedActivatedColor:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/mail/browse/ConversationItemView;->sFadedActivatedColor:I

    :cond_6
    sget v0, Lcom/android/mail/browse/ConversationItemView;->sFadedActivatedColor:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0030

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v3, v4, v5, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_7
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mStarEnabled:Z

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getStarBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starY:I

    int-to-float v2, v2

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->checkboxVisible:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mChecked:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->CHECKMARK_ON:Landroid/graphics/Bitmap;

    :goto_4
    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->checkmarkX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->checkmarkY:I

    int-to-float v2, v2

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->CHECKMARK_OFF:Landroid/graphics/Bitmap;

    goto :goto_4

    :cond_b
    sget v0, Lcom/android/mail/browse/ConversationItemView;->sSendersTextColorRead:I

    goto/16 :goto_1

    :cond_c
    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->drawSenders(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    :cond_d
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->hasBeenRepliedTo:Z

    if-eqz v0, :cond_e

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STATE_REPLIED:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    :cond_e
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->hasBeenForwarded:Z

    if-eqz v0, :cond_f

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STATE_FORWARDED:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    :cond_f
    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationItemViewModel;->isInvite:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/mail/browse/ConversationItemView;->STATE_CALENDAR_INVITE:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const-string v3, "CCHV.layout"

    invoke-static {v3}, Lcom/android/mail/browse/ConversationItemView;->startTimer(Ljava/lang/String;)V

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v2, p4, p2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mViewWidth:I

    if-ne v2, v3, :cond_0

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView;->mPreviousMode:I

    if-eq v3, v0, :cond_1

    :cond_0
    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mViewWidth:I

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView;->mPreviousMode:I

    iget-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mTesting:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/android/mail/browse/ConversationItemView;->mPreviousMode:I

    invoke-static {v3, v4}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;I)I

    move-result v3

    iput v3, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    :cond_1
    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget v4, p0, Lcom/android/mail/browse/ConversationItemView;->mViewWidth:I

    iput v4, v3, Lcom/android/mail/browse/ConversationItemViewModel;->viewWidth:I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    const v4, 0x7f0c002d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Lcom/android/mail/browse/ConversationItemViewModel;->standardScaledDimen:I

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->standardScaledDimen:I

    sget v4, Lcom/android/mail/browse/ConversationItemView;->sStandardScaledDimen:I

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->standardScaledDimen:I

    sput v3, Lcom/android/mail/browse/ConversationItemView;->sStandardScaledDimen:I

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->refreshConversationHeights(Landroid/content/Context;)V

    :cond_2
    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/android/mail/browse/ConversationItemView;->mViewWidth:I

    iget v5, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    iget-object v6, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget v6, v6, Lcom/android/mail/browse/ConversationItemViewModel;->standardScaledDimen:I

    iget-boolean v7, p0, Lcom/android/mail/browse/ConversationItemView;->mConvListPhotosEnabled:Z

    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->forWidth(Landroid/content/Context;IIIZ)Lcom/android/mail/browse/ConversationItemViewCoordinates;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->calculateTextsAndBitmaps()V

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->calculateCoordinates()V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-boolean v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->unread:Z

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->showActivatedText()Z

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/android/mail/browse/ConversationItemView;->createSubject(ZZ)V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationItemViewModel;->isLayoutValid(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->setContentDescription()V

    :cond_3
    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/ConversationItemViewModel;->validate(Landroid/content/Context;)V

    const-string v3, "CCHV.layout"

    invoke-static {v3}, Lcom/android/mail/browse/ConversationItemView;->pauseTimer(Ljava/lang/String;)V

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    if-eqz v3, :cond_4

    sget v3, Lcom/android/mail/browse/ConversationItemView;->sLayoutCount:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/android/mail/browse/ConversationItemView;->sLayoutCount:I

    const/16 v4, 0x32

    if-lt v3, v4, :cond_4

    sget-object v3, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    invoke-virtual {v3}, Lcom/android/mail/perf/Timer;->dumpResults()V

    new-instance v3, Lcom/android/mail/perf/Timer;

    invoke-direct {v3}, Lcom/android/mail/perf/Timer;-><init>()V

    sput-object v3, Lcom/android/mail/browse/ConversationItemView;->sTimer:Lcom/android/mail/perf/Timer;

    const/4 v3, 0x0

    sput v3, Lcom/android/mail/browse/ConversationItemView;->sLayoutCount:I

    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget v1, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v1

    invoke-direct {p0, p2, v1}, Lcom/android/mail/browse/ConversationItemView;->measureHeight(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/browse/ConversationItemView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v2, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/android/mail/browse/ConversationItemView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v1, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v2, v4

    iput v1, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchX:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mLastTouchY:I

    iget-boolean v4, p0, Lcom/android/mail/browse/ConversationItemView;->mSwipeEnabled:Z

    if-nez v4, :cond_1

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->onTouchEventNoSwipe(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    move v0, v3

    goto :goto_0

    :pswitch_0
    int-to-float v4, v1

    int-to-float v5, v2

    invoke-direct {p0, v4, v5}, Lcom/android/mail/browse/ConversationItemView;->isTouchInCheckmark(FF)Z

    move-result v4

    if-nez v4, :cond_3

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-direct {p0, v4, v5}, Lcom/android/mail/browse/ConversationItemView;->isTouchInStar(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_3
    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    move v0, v3

    goto :goto_0

    :pswitch_1
    iget-boolean v4, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    if-eqz v4, :cond_2

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-direct {p0, v4, v5}, Lcom/android/mail/browse/ConversationItemView;->isTouchInCheckmark(FF)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-boolean v6, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleCheckMark()V

    move v0, v3

    goto :goto_0

    :cond_4
    int-to-float v4, v1

    int-to-float v5, v2

    invoke-direct {p0, v4, v5}, Lcom/android/mail/browse/ConversationItemView;->isTouchInStar(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-boolean v6, p0, Lcom/android/mail/browse/ConversationItemView;->mDownEvent:Z

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleStar()V

    move v0, v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public performClick()Z
    .locals 5

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getListView()Lcom/android/mail/ui/SwipeableListView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/mail/ui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v1, p0, v3}, Lcom/android/mail/ui/SwipeableListView;->findConversation(Lcom/android/mail/browse/ConversationItemView;Lcom/android/mail/providers/Conversation;)I

    move-result v2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-wide v3, v3, Lcom/android/mail/providers/Conversation;->id:J

    invoke-virtual {v1, p0, v2, v3, v4}, Lcom/android/mail/ui/SwipeableListView;->performItemClick(Landroid/view/View;IJ)Z

    :cond_0
    return v0
.end method

.method public reset()V
    .locals 2

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setAlpha(F)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setTranslationX(F)V

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/ConversationItemView;->setAnimatedHeight(I)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setMinimumHeight(I)V

    return-void
.end method

.method public setAnimatedHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->requestLayout()V

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgrounds:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgrounds:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eq v1, v0, :cond_1

    invoke-super {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method public setItemAlpha(F)V
    .locals 0
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationItemView;->setAlpha(F)V

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationItemView;->invalidate()V

    return-void
.end method

.method setMode(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/mail/browse/ConversationItemView;->mMode:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView;->mTesting:Z

    return-void
.end method

.method public startDestroyAnimation(Landroid/animation/Animator$AnimatorListener;)V
    .locals 5
    .param p1    # Landroid/animation/Animator$AnimatorListener;

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/mail/browse/ConversationItemView;->createHeightAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v1

    invoke-virtual {p0, v4}, Lcom/android/mail/browse/ConversationItemView;->setMinimumHeight(I)V

    sget v2, Lcom/android/mail/browse/ConversationItemView;->sAnimatingBackgroundColor:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    iget v2, p0, Lcom/android/mail/browse/ConversationItemView;->mBackgroundOverride:I

    invoke-virtual {p0, v2}, Lcom/android/mail/browse/ConversationItemView;->setBackgroundColor(I)V

    iput v1, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public startDestroyWithSwipeAnimation(Landroid/animation/Animator$AnimatorListener;)V
    .locals 5
    .param p1    # Landroid/animation/Animator$AnimatorListener;

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/mail/browse/ConversationItemView;->createTranslateXAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-direct {p0, v4}, Lcom/android/mail/browse/ConversationItemView;->createHeightAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    invoke-virtual {v2, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public startSwipeUndoAnimation(Lcom/android/mail/ui/ViewMode;Landroid/animation/Animator$AnimatorListener;)V
    .locals 2
    .param p1    # Lcom/android/mail/ui/ViewMode;
    .param p2    # Landroid/animation/Animator$AnimatorListener;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/mail/browse/ConversationItemView;->createTranslateXAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public startUndoAnimation(Lcom/android/mail/ui/ViewMode;Landroid/animation/Animator$AnimatorListener;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationItemView;->setMinimumHeight(I)V

    iput v4, p0, Lcom/android/mail/browse/ConversationItemView;->mAnimatedHeight:I

    invoke-direct {p0, v5}, Lcom/android/mail/browse/ConversationItemView;->createHeightAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string v1, "itemAlpha"

    new-array v2, v6, [F

    fill-array-data v2, :array_0

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    sget v2, Lcom/android/mail/browse/ConversationItemView;->sShrinkAnimationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40000000

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v2, p2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method

.method public toggleCheckMarkOrBeginDrag()V
    .locals 2

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getViewMode()Lcom/android/mail/ui/ViewMode;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationItemView;->mTabletDevice:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->toggleCheckMark()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->beginDragMode()V

    goto :goto_0
.end method

.method public toggleStar()V
    .locals 7

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v2, v2, Lcom/android/mail/providers/Conversation;->starred:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, v3, Lcom/android/mail/providers/Conversation;->starred:Z

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationItemView;->getStarBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starX:I

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v3, v3, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starY:I

    iget-object v4, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v4, v4, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starX:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v5, v5, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starY:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/android/mail/browse/ConversationItemView;->postInvalidate(IIII)V

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mAdapter:Lcom/android/mail/ui/AnimatedAdapter;

    invoke-virtual {v2}, Lcom/android/mail/ui/AnimatedAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/ConversationCursor;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    sget-object v4, Lcom/android/mail/providers/UIProvider$ConversationColumns;->STARRED:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/ConversationItemView;->mHeader:Lcom/android/mail/browse/ConversationItemViewModel;

    iget-object v5, v5, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v5, v5, Lcom/android/mail/providers/Conversation;->starred:Z

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/mail/browse/ConversationCursor;->updateBoolean(Landroid/content/Context;Lcom/android/mail/providers/Conversation;Ljava/lang/String;Z)I

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
