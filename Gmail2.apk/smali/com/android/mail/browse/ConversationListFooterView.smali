.class public final Lcom/android/mail/browse/ConversationListFooterView;
.super Landroid/widget/LinearLayout;
.source "ConversationListFooterView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/mail/ui/ViewMode$ModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;
    }
.end annotation


# static fields
.field private static sNormalBackground:Landroid/graphics/drawable/Drawable;

.field private static sWideBackground:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mClickListener:Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;

.field private mErrorActionButton:Landroid/widget/Button;

.field private mErrorStatus:I

.field private mErrorText:Landroid/widget/TextView;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mLoadMore:Landroid/view/View;

.field private mLoadMoreUri:Landroid/net/Uri;

.field private mLoading:Landroid/view/View;

.field private mNetworkError:Landroid/view/View;

.field private final mTabletDevice:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mTabletDevice:Z

    return-void
.end method

.method private getBackground(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationListFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private getNormalBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sNormalBackground:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const v0, 0x7f020018

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->getBackground(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sNormalBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    sget-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sNormalBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getWideBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sWideBackground:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const v0, 0x7f02001a

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->getBackground(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sWideBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    sget-object v0, Lcom/android/mail/browse/ConversationListFooterView;->sWideBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/android/mail/browse/ConversationListFooterView;->mClickListener:Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;

    iget v3, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    invoke-interface {v2, v0, v3}, Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;->onFooterViewErrorActionClick(Lcom/android/mail/providers/Folder;I)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/mail/browse/ConversationListFooterView;->mClickListener:Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;

    invoke-interface {v2, v0}, Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;->onFooterViewLoadMoreClick(Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08005d
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f08005e

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoading:Landroid/view/View;

    const v0, 0x7f08005b

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    const v0, 0x7f08005f

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08005d

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorActionButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorActionButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08005c

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorText:Landroid/widget/TextView;

    return-void
.end method

.method public onViewModeChanged(I)V
    .locals 2
    .param p1    # I

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationListFooterView;->mTabletDevice:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationListFooterView;->getWideBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationListFooterView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationListFooterView;->getNormalBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public setClickListener(Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;

    iput-object p1, p0, Lcom/android/mail/browse/ConversationListFooterView;->mClickListener:Lcom/android/mail/browse/ConversationListFooterView$FooterViewClickListener;

    return-void
.end method

.method public setFolder(Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/browse/ConversationListFooterView;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorActionButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationListFooterView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationListFooterView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->loadMoreUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMoreUri:Landroid/net/Uri;

    return-void
.end method

.method public updateStatus(Lcom/android/mail/browse/ConversationCursor;)Z
    .locals 9
    .param p1    # Lcom/android/mail/browse/ConversationCursor;

    const/4 v5, 0x0

    const/16 v6, 0x8

    if-nez p1, :cond_0

    :goto_0
    return v5

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "cursor_status"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v4, "cursor_error"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "cursor_error"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    :goto_1
    iput v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    invoke-static {v1}, Lcom/android/mail/providers/UIProvider$CursorStatus;->isWaitingForResults(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoading:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    move v5, v3

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1

    :cond_2
    iget v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationListFooterView;->getContext()Landroid/content/Context;

    move-result-object v7

    iget v8, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    invoke-static {v7, v8}, Lcom/android/mail/utils/Utils;->getSyncStatusText(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoading:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorActionButton:Landroid/widget/Button;

    iget v7, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_3

    :goto_3
    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorStatus:I

    packed-switch v4, :pswitch_data_0

    const v0, 0x7f090012

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mErrorActionButton:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    :cond_3
    move v5, v6

    goto :goto_3

    :pswitch_0
    const v0, 0x7f090012

    goto :goto_4

    :pswitch_1
    const v0, 0x7f090105

    goto :goto_4

    :pswitch_2
    const v0, 0x7f090012

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :pswitch_3
    const v0, 0x7f090106

    goto :goto_4

    :pswitch_4
    const v0, 0x7f090107

    goto :goto_4

    :cond_4
    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMoreUri:Landroid/net/Uri;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoading:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mNetworkError:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationListFooterView;->mLoadMore:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
