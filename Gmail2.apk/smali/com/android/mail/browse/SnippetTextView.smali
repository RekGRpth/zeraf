.class public Lcom/android/mail/browse/SnippetTextView;
.super Landroid/widget/TextView;
.source "SnippetTextView.java"


# instance fields
.field private mLastHSpec:I

.field private mLastWSpec:I

.field private mMaxLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/SnippetTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getTextRemainder(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move-object p1, v2

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/browse/SnippetTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/mail/browse/SnippetTextView;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v3

    invoke-virtual {p0, v2}, Lcom/android/mail/browse/SnippetTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/SnippetTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/mail/browse/SnippetTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_3

    iget v5, p0, Lcom/android/mail/browse/SnippetTextView;->mLastWSpec:I

    iget v6, p0, Lcom/android/mail/browse/SnippetTextView;->mLastHSpec:I

    invoke-virtual {p0, v5, v6}, Lcom/android/mail/browse/SnippetTextView;->measure(II)V

    invoke-virtual {p0}, Lcom/android/mail/browse/SnippetTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    iget v5, p0, Lcom/android/mail/browse/SnippetTextView;->mMaxLines:I

    if-gt v1, v5, :cond_4

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v3}, Lcom/android/mail/browse/SnippetTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, v4}, Lcom/android/mail/browse/SnippetTextView;->setText(Ljava/lang/CharSequence;)V

    move-object p1, v2

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/android/mail/browse/SnippetTextView;->mMaxLines:I

    invoke-virtual {v0, v5}, Landroid/text/Layout;->getLineStart(I)I

    move-result v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    iput p1, p0, Lcom/android/mail/browse/SnippetTextView;->mLastWSpec:I

    iput p2, p0, Lcom/android/mail/browse/SnippetTextView;->mLastHSpec:I

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    iput p1, p0, Lcom/android/mail/browse/SnippetTextView;->mMaxLines:I

    return-void
.end method
