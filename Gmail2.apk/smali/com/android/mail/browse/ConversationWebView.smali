.class public Lcom/android/mail/browse/ConversationWebView;
.super Lcom/android/mail/browse/MailWebView;
.source "ConversationWebView.java"

# interfaces
.implements Lcom/android/mail/browse/ScrollNotifier;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private final mDensity:F

.field private mHandlingTouch:Z

.field private mIgnoringTouch:Z

.field private final mNotifyPageRenderedInHardwareLayer:Ljava/lang/Runnable;

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private final mScrollListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/browse/ScrollNotifier$ScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field private mUseSoftwareLayer:Z

.field private final mViewportWidth:I

.field private mVisible:Z

.field private final mWebviewInitialDelay:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationWebView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/ConversationWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/browse/MailWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Lcom/android/mail/browse/ConversationWebView$1;

    invoke-direct {v1, p0}, Lcom/android/mail/browse/ConversationWebView$1;-><init>(Lcom/android/mail/browse/ConversationWebView;)V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationWebView;->mNotifyPageRenderedInHardwareLayer:Ljava/lang/Runnable;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, p0, Lcom/android/mail/browse/ConversationWebView;->mScrollListeners:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/mail/browse/ConversationWebView;->mViewportWidth:I

    const v1, 0x7f0b0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/mail/browse/ConversationWebView;->mWebviewInitialDelay:I

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/android/mail/browse/ConversationWebView;->mDensity:F

    return-void
.end method

.method static synthetic access$002(Lcom/android/mail/browse/ConversationWebView;Z)Z
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationWebView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationWebView;->mUseSoftwareLayer:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/mail/browse/ConversationWebView;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationWebView;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationWebView;->destroyBitmap()V

    return-void
.end method

.method private destroyBitmap()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    :cond_0
    return-void
.end method


# virtual methods
.method public addScrollListener(Lcom/android/mail/browse/ScrollNotifier$ScrollListener;)V
    .locals 1
    .param p1    # Lcom/android/mail/browse/ScrollNotifier$ScrollListener;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mScrollListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public destroy()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationWebView;->destroyBitmap()V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mNotifyPageRenderedInHardwareLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationWebView;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-super {p0}, Lcom/android/mail/browse/MailWebView;->destroy()V

    return-void
.end method

.method public getInitialScale()F
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationWebView;->mDensity:F

    return v0
.end method

.method public getViewportWidth()I
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationWebView;->mViewportWidth:I

    return v0
.end method

.method public isHandlingTouch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationWebView;->mHandlingTouch:Z

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/mail/browse/MailWebView;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v3, p0, Lcom/android/mail/browse/ConversationWebView;->mUseSoftwareLayer:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/mail/browse/ConversationWebView;->mVisible:Z

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getWidth()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getHeight()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    neg-int v4, v1

    int-to-float v4, v4

    neg-int v5, v2

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    invoke-super {p0, v3}, Lcom/android/mail/browse/MailWebView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    iput-object v6, p0, Lcom/android/mail/browse/ConversationWebView;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v6, p0, Lcom/android/mail/browse/ConversationWebView;->mCanvas:Landroid/graphics/Canvas;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationWebView;->mUseSoftwareLayer:Z

    goto :goto_0
.end method

.method public onRenderComplete()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationWebView;->mUseSoftwareLayer:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mNotifyPageRenderedInHardwareLayer:Ljava/lang/Runnable;

    iget v1, p0, Lcom/android/mail/browse/ConversationWebView;->mWebviewInitialDelay:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/mail/browse/ConversationWebView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/mail/browse/MailWebView;->onScrollChanged(IIII)V

    iget-object v2, p0, Lcom/android/mail/browse/ConversationWebView;->mScrollListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/ScrollNotifier$ScrollListener;

    invoke-interface {v1, p1, p2}, Lcom/android/mail/browse/ScrollNotifier$ScrollListener;->onNotifierScroll(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-boolean v4, p0, Lcom/android/mail/browse/ConversationWebView;->mIgnoringTouch:Z

    if-nez v4, :cond_1

    invoke-super {p0, p1}, Lcom/android/mail/browse/MailWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/mail/browse/ConversationWebView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_3
    return v2

    :pswitch_1
    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationWebView;->mHandlingTouch:Z

    goto :goto_0

    :pswitch_2
    sget-object v4, Lcom/android/mail/browse/ConversationWebView;->LOG_TAG:Ljava/lang/String;

    const-string v5, "WebView disabling intercepts: POINTER_DOWN"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0, v3}, Lcom/android/mail/browse/ConversationWebView;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v4, p0, Lcom/android/mail/browse/ConversationWebView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v4, :cond_0

    iput-boolean v3, p0, Lcom/android/mail/browse/ConversationWebView;->mIgnoringTouch:Z

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-super {p0, v1}, Lcom/android/mail/browse/MailWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_3
    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationWebView;->mHandlingTouch:Z

    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationWebView;->mIgnoringTouch:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onUserVisibilityChanged(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationWebView;->mVisible:Z

    return-void
.end method

.method public screenPxToWebPx(I)I
    .locals 2
    .param p1    # I

    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getInitialScale()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public screenPxToWebPxError(I)F
    .locals 2
    .param p1    # I

    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getInitialScale()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationWebView;->screenPxToWebPx(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public setOnScaleGestureListener(Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationWebView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    goto :goto_0
.end method

.method public setUseSoftwareLayer(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationWebView;->mUseSoftwareLayer:Z

    return-void
.end method
