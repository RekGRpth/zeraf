.class public Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;
.super Lcom/android/mail/browse/ConversationOverlayItem;
.source "ConversationViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuperCollapsedBlockItem"
.end annotation


# instance fields
.field private mEnd:I

.field private final mStart:I

.field final synthetic this$0:Lcom/android/mail/browse/ConversationViewAdapter;


# direct methods
.method private constructor <init>(Lcom/android/mail/browse/ConversationViewAdapter;II)V
    .locals 0
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationOverlayItem;-><init>()V

    iput p2, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->mStart:I

    iput p3, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->mEnd:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/browse/ConversationViewAdapter;IILcom/android/mail/browse/ConversationViewAdapter$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/android/mail/browse/ConversationViewAdapter$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;-><init>(Lcom/android/mail/browse/ConversationViewAdapter;II)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    move-object v0, p1

    check-cast v0, Lcom/android/mail/browse/SuperCollapsedBlock;

    invoke-virtual {v0, p0}, Lcom/android/mail/browse/SuperCollapsedBlock;->bind(Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;)V

    return-void
.end method

.method public canPushSnapHeader()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public createView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040061

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SuperCollapsedBlock;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mSuperCollapsedListener:Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;
    invoke-static {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->access$900(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SuperCollapsedBlock;->initialize(Lcom/android/mail/browse/SuperCollapsedBlock$OnClickListener;)V

    return-object v0
.end method

.method public getEnd()I
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->mEnd:I

    return v0
.end method

.method public getStart()I
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$SuperCollapsedBlockItem;->mStart:I

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isContiguous()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
