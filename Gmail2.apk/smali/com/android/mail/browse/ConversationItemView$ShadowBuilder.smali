.class Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;
.super Landroid/view/View$DragShadowBuilder;
.source "ConversationItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShadowBuilder"
.end annotation


# instance fields
.field private final mBackground:Landroid/graphics/drawable/Drawable;

.field private final mDragDesc:Ljava/lang/String;

.field private mDragDescX:I

.field private mDragDescY:I

.field private final mTouchX:I

.field private final mTouchY:I

.field private final mView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/mail/browse/ConversationItemView;


# direct methods
.method public constructor <init>(Lcom/android/mail/browse/ConversationItemView;Landroid/view/View;III)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->this$0:Lcom/android/mail/browse/ConversationItemView;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f100001

    invoke-static {v0, v1, p3}, Lcom/android/mail/utils/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDesc:Ljava/lang/String;

    iput p4, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mTouchX:I

    iput p5, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mTouchY:I

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$400()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->this$0:Lcom/android/mail/browse/ConversationItemView;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v1, v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDesc:Ljava/lang/String;

    iget v1, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDescX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDescY:I

    int-to-float v2, v2

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$400()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1    # Landroid/graphics/Point;
    .param p2    # Landroid/graphics/Point;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->this$0:Lcom/android/mail/browse/ConversationItemView;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDescX:I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->this$0:Lcom/android/mail/browse/ConversationItemView;

    iget-object v2, v2, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v2, v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectFontSize:I

    # invokes: Lcom/android/mail/browse/ConversationItemView;->getPadding(II)I
    invoke-static {v0, v2}, Lcom/android/mail/browse/ConversationItemView;->access$100(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->this$0:Lcom/android/mail/browse/ConversationItemView;

    iget-object v3, v3, Lcom/android/mail/browse/ConversationItemView;->mCoordinates:Lcom/android/mail/browse/ConversationItemViewCoordinates;

    iget v3, v3, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectAscent:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mDragDescY:I

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    iget v2, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mTouchX:I

    iget v3, p0, Lcom/android/mail/browse/ConversationItemView$ShadowBuilder;->mTouchY:I

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    return-void
.end method
