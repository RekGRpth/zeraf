.class public abstract Lcom/android/mail/photomanager/ContactPhotoManager;
.super Ljava/lang/Object;
.source "ContactPhotoManager.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;
    }
.end annotation


# static fields
.field public static final DEFAULT_AVATAR:Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/mail/photomanager/LetterTileProvider;

    invoke-direct {v0}, Lcom/android/mail/photomanager/LetterTileProvider;-><init>()V

    sput-object v0, Lcom/android/mail/photomanager/ContactPhotoManager;->DEFAULT_AVATAR:Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized createContactPhotoManager(Landroid/content/Context;)Lcom/android/mail/photomanager/ContactPhotoManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/android/mail/photomanager/ContactPhotoManager;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    invoke-direct {v0, p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract loadThumbnail(Ljava/lang/Long;Lcom/android/mail/ui/DividedImageCanvas;Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;)V
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    return-void
.end method

.method public onLowMemory()V
    .locals 0

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public abstract removePhoto(Ljava/lang/Long;)V
.end method
