.class Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;
.super Landroid/os/HandlerThread;
.source "ContactPhotoManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoaderThread"
.end annotation


# instance fields
.field private final DATA_COLS:[Ljava/lang/String;

.field private mLoaderThreadHandler:Landroid/os/Handler;

.field private final mNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;",
            ">;"
        }
    .end annotation
.end field

.field private final mPhotoIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mPhotoIdsAsStrings:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPhotoUris:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreloadPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPreloadStatus:I

.field private final mResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;


# direct methods
.method public constructor <init>(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Landroid/content/ContentResolver;)V
    .locals 3
    .param p2    # Landroid/content/ContentResolver;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const-string v0, "ContactPhotoLoader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIdsAsStrings:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoUris:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mNames:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    iput v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->DATA_COLS:[Ljava/lang/String;

    iput-object p2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method private createInQuery(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->appendQuestionMarks(Ljava/lang/StringBuilder;I)V

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private loadEmailAddressBasedPhotos(Z)V
    .locals 32
    .param p1    # Z

    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    new-instance v25, Ljava/util/HashSet;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashSet;-><init>()V

    new-instance v26, Ljava/util/HashSet;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashSet;-><init>()V

    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mNames:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    invoke-virtual/range {v31 .. v31}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getEmailAddress()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPhotoIdCache:Landroid/util/LruCache;
    invoke-static {v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$700(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/util/LruCache;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    if-eqz v22, :cond_0

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->size()I

    move-result v4

    new-array v8, v4, [Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const/16 v27, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mimetype"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "data1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->size()I

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->appendQuestionMarks(Ljava/lang/StringBuilder;I)V

    const/16 v4, 0x29

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->DATA_COLS:[Ljava/lang/String;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    if-eqz v27, :cond_4

    :cond_2
    :goto_1
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    const/4 v4, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cachePhotoId(Ljava/lang/Long;Ljava/lang/String;)V
    invoke-static {v4, v0, v1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$800(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Long;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    if-eqz v27, :cond_3

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v4

    :cond_4
    if-eqz v27, :cond_5

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    :cond_5
    if-eqz v25, :cond_8

    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_8

    const/16 v28, 0x0

    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v26

    move-object/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadThumbnails(ZLjava/util/Set;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v28

    if-eqz v28, :cond_7

    :goto_2
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    const/4 v6, -0x1

    move-object/from16 v0, v17

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v4, v5, v0, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v4

    if-eqz v28, :cond_6

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4

    :cond_7
    if-eqz v28, :cond_8

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    :cond_8
    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_9
    :goto_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Long;

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/provider/ContactsContract;->isProfileId(J)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v29, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$500()[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    if-eqz v29, :cond_a

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v5, 0x1

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    const/4 v6, -0x1

    move-object/from16 v0, v23

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v4, v0, v5, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_4
    if-eqz v29, :cond_9

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object/from16 v0, v23

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v4, v0, v5, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_4

    :catchall_2
    move-exception v4

    if-eqz v29, :cond_b

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v4

    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object/from16 v0, v23

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v4, v0, v5, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V

    goto :goto_3

    :cond_d
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_e
    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v5, 0x0

    const/4 v6, -0x1

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v4, v15, v5, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V

    goto :goto_5

    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$400(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private loadPhotosInBackground()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIdsAsStrings:Ljava/util/Set;

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoUris:Ljava/util/Set;

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mNames:Ljava/util/Set;

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->obtainPhotoIdsAndUrisToLoad(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$200(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct {p0, v5}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadThumbnails(Z)V

    invoke-direct {p0, v5}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadEmailAddressBasedPhotos(Z)V

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->requestPreloading()V

    return-void
.end method

.method private loadProfileThumbnails(ZLjava/util/Set;)V
    .locals 11
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v10, -0x1

    const/4 v9, 0x0

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/provider/ContactsContract;->isProfileId(J)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v8, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$500()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const/4 v3, -0x1

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v0, v1, v2, v3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v1, 0x0

    const/4 v2, -0x1

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v0, v7, v1, v2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :cond_3
    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v0, v7, v9, v10}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private loadThumbnails(ZLjava/util/Set;Ljava/util/Set;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v5

    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    :cond_2
    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$500()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id"

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->createInQuery(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->EMPTY_STRING_ARRAY:[Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$600()[Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method private loadThumbnails(Z)V
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIdsAsStrings:Ljava/util/Set;

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-direct {p0, p1, v3, v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadThumbnails(ZLjava/util/Set;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    const/4 v4, -0x1

    # invokes: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V
    invoke-static {v3, v2, v0, v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v3

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-direct {p0, p1, v3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadProfileThumbnails(ZLjava/util/Set;)V

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$400(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private preloadPhotosInBackground()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x2

    iget v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    if-ne v3, v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    if-nez v3, :cond_2

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->queryPhotosForPreload()V

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iput v5, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    :goto_1
    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->requestPreloading()V

    goto :goto_0

    :cond_1
    iput v6, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;
    invoke-static {v3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$000(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/LruCache;->size()I

    move-result v3

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCacheRedZoneBytes:I
    invoke-static {v4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$100(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)I

    move-result v4

    if-le v3, v4, :cond_3

    iput v5, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIdsAsStrings:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    :goto_2
    if-lez v2, :cond_4

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    const/16 v4, 0x19

    if-ge v3, v4, :cond_4

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIds:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPhotoIdsAsStrings:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    invoke-direct {p0, v6}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadThumbnails(Z)V

    if-nez v2, :cond_5

    iput v5, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    :cond_5
    const-string v3, "ContactPhotoManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Preloaded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " photos.  Cached bytes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->this$0:Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    # getter for: Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;
    invoke-static {v5}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->access$000(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/util/LruCache;

    move-result-object v5

    invoke-virtual {v5}, Landroid/util/LruCache;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->requestPreloading()V

    goto/16 :goto_0
.end method

.method private queryPhotosForPreload()V
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "directory"

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "photo_id"

    aput-object v4, v2, v3

    const-string v3, "photo_id NOT NULL AND photo_id!=0"

    const/4 v4, 0x0

    const-string v5, "starred DESC, last_time_contacted DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadPhotoIds:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    return-void
.end method


# virtual methods
.method appendQuestionMarks(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    const/16 v2, 0x3f

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/16 v2, 0x2c

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public ensureHandler()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->preloadPhotosInBackground()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->loadPhotosInBackground()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requestLoading()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->ensureHandler()V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public requestPreloading()V
    .locals 4

    iget v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mPreloadStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->ensureHandler()V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
