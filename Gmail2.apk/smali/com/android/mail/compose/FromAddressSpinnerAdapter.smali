.class public Lcom/android/mail/compose/FromAddressSpinnerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FromAddressSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/mail/providers/ReplyFromAccount;",
        ">;"
    }
.end annotation


# static fields
.field public static ACCOUNT_ADDRESS:I

.field public static ACCOUNT_DISPLAY:I

.field public static REAL_ACCOUNT:I

.field private static sFormatString:Ljava/lang/String;


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    sput v0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->REAL_ACCOUNT:I

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->ACCOUNT_DISPLAY:I

    const/4 v0, 0x1

    sput v0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->ACCOUNT_ADDRESS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const v0, 0x7f04003f

    const v1, 0x7f0800ae

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->sFormatString:Ljava/lang/String;

    return-void
.end method

.method private static formatAddress(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->sFormatString:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public addAccounts(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/ReplyFromAccount;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/ReplyFromAccount;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/ReplyFromAccount;

    iget-boolean v4, v2, Lcom/android/mail/providers/ReplyFromAccount;->isCustomFrom:Z

    if-eqz v4, :cond_1

    const v3, 0x7f040034

    :goto_0
    invoke-virtual {p0}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v4, 0x7f0800ae

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, v2, Lcom/android/mail/providers/ReplyFromAccount;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v4, v2, Lcom/android/mail/providers/ReplyFromAccount;->isCustomFrom:Z

    if-eqz v4, :cond_0

    const v4, 0x7f0800af

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, v2, Lcom/android/mail/providers/ReplyFromAccount;->address:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->formatAddress(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v1

    :cond_1
    const v3, 0x7f04003e

    goto :goto_0
.end method

.method protected getInflater()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/ReplyFromAccount;

    iget-boolean v0, v0, Lcom/android/mail/providers/ReplyFromAccount;->isCustomFrom:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/ReplyFromAccount;

    iget-boolean v3, v1, Lcom/android/mail/providers/ReplyFromAccount;->isCustomFrom:Z

    if-eqz v3, :cond_1

    const v2, 0x7f040035

    :goto_0
    if-nez p2, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_1
    const v3, 0x7f0800ae

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v1, Lcom/android/mail/providers/ReplyFromAccount;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, v1, Lcom/android/mail/providers/ReplyFromAccount;->isCustomFrom:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0800af

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v1, Lcom/android/mail/providers/ReplyFromAccount;->address:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/mail/compose/FromAddressSpinnerAdapter;->formatAddress(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v0

    :cond_1
    const v2, 0x7f04003f

    goto :goto_0

    :cond_2
    move-object v0, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
