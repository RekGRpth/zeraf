.class public Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;
.super Ljava/lang/Object;
.source "ComposeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/compose/ComposeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SendOrSaveTask"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

.field public final mSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

.field public final mSendOrSaveMessage:Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;Lcom/android/mail/providers/ReplyFromAccount;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;
    .param p3    # Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;
    .param p4    # Lcom/android/mail/providers/ReplyFromAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    iput-object p2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveMessage:Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;

    iput-object p4, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    return-void
.end method

.method private callAccountSendSaveMethod(Landroid/content/ContentResolver;Lcom/android/mail/providers/Account;Ljava/lang/String;Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)Landroid/os/Bundle;
    .locals 13
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Lcom/android/mail/providers/Account;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;

    new-instance v6, Landroid/os/Bundle;

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v8}, Landroid/content/ContentValues;->size()I

    move-result v8

    invoke-direct {v6, v8}, Landroid/os/Bundle;-><init>(I)V

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v8}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    instance-of v8, v2, Ljava/lang/String;

    if-eqz v8, :cond_0

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    instance-of v8, v2, Ljava/lang/Boolean;

    if-eqz v8, :cond_1

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {v6, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    instance-of v8, v2, Ljava/lang/Integer;

    if-eqz v8, :cond_2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v6, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    instance-of v8, v2, Ljava/lang/Long;

    if-eqz v8, :cond_3

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v5, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    :cond_3
    # getter for: Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/compose/ComposeActivity;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Unexpected object type: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_4
    invoke-virtual/range {p4 .. p4}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->attachmentFds()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v8, "opened_fds"

    invoke-virtual {v6, v8, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    iget-object v8, p2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v9, p2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p3

    invoke-virtual {p1, v8, v0, v9, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v8

    return-object v8
.end method

.method private closeOpenedAttachmentFds(Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)V
    .locals 6
    .param p1    # Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;

    invoke-virtual {p1}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->attachmentFds()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0

    :cond_1
    return-void
.end method

.method private sendOrSaveMessage(JLcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;Lcom/android/mail/providers/ReplyFromAccount;Lcom/android/mail/providers/Message;)V
    .locals 12
    .param p1    # J
    .param p3    # Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;
    .param p4    # Lcom/android/mail/providers/ReplyFromAccount;
    .param p5    # Lcom/android/mail/providers/Message;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-wide/16 v3, -0x1

    cmp-long v3, p1, v3

    if-eqz v3, :cond_1

    const/4 v10, 0x1

    :goto_0
    iget-boolean v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mSave:Z

    if-eqz v3, :cond_2

    const-string v7, "save_message"

    :goto_1
    if-eqz v10, :cond_4

    :try_start_0
    iget-object v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    const-string v4, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-direct {p0, v1, v3, v7, p3}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->callAccountSendSaveMethod(Landroid/content/ContentResolver;Lcom/android/mail/providers/Account;Ljava/lang/String;Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)Landroid/os/Bundle;

    move-result-object v9

    if-nez v9, :cond_0

    iget-boolean v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mSave:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/android/mail/providers/Message;->saveUri:Ljava/lang/String;

    :goto_2
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    iget-object v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v11, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_3
    invoke-direct {p0, p3}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->closeOpenedAttachmentFds(Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)V

    return-void

    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    const-string v7, "send_message"

    goto :goto_1

    :cond_3
    :try_start_1
    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/android/mail/providers/Message;->sendUri:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-direct {p0, v1, v3, v7, p3}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->callAccountSendSaveMethod(Landroid/content/ContentResolver;Lcom/android/mail/providers/Account;Ljava/lang/String;Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)Landroid/os/Bundle;

    move-result-object v9

    if-eqz v9, :cond_6

    const-string v3, "messageUri"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    :goto_4
    iget-boolean v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mSave:Z

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    sget-object v3, Lcom/android/mail/providers/UIProvider;->MESSAGE_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    if-eqz v8, :cond_0

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    new-instance v4, Lcom/android/mail/providers/Message;

    invoke-direct {v4, v8}, Lcom/android/mail/providers/Message;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v3, p3, v4}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;->notifyMessageIdAllocated(Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;Lcom/android/mail/providers/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_5
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v3

    invoke-direct {p0, p3}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->closeOpenedAttachmentFds(Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;)V

    throw v3

    :cond_6
    :try_start_4
    iget-boolean v3, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mSave:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->saveDraftUri:Landroid/net/Uri;

    :goto_5
    iget-object v4, p3, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_4

    :cond_7
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->sendMessageUri:Landroid/net/Uri;

    goto :goto_5

    :catchall_1
    move-exception v3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method


# virtual methods
.method public run()V
    .locals 14

    const-wide/16 v12, -0x1

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveMessage:Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;

    iget-object v8, v7, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    invoke-interface {v0}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;->getMessage()Lcom/android/mail/providers/Message;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-wide v10, v9, Lcom/android/mail/providers/Message;->id:J

    :goto_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    if-eqz v0, :cond_1

    iget-object v0, v8, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v2, v2, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    cmp-long v0, v10, v12

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v0, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->expungeMessageUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/mail/utils/ContentProviderTask$UpdateTask;

    invoke-direct {v0}, Lcom/android/mail/utils/ContentProviderTask$UpdateTask;-><init>()V

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mExistingDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v2, v2, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->expungeMessageUri:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/utils/ContentProviderTask$UpdateTask;->run(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    const-wide/16 v10, -0x1

    :cond_1
    move-wide v5, v10

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->sendOrSaveMessage(JLcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;Lcom/android/mail/providers/ReplyFromAccount;Lcom/android/mail/providers/Message;)V

    iget-boolean v0, v7, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mSave:Z

    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    iget-object v0, v7, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    const-string v4, "toAddresses"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/android/mail/providers/UIProvider;->incrementRecipientsTimesContacted(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    iget-object v0, v7, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    const-string v4, "ccAddresses"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/android/mail/providers/UIProvider;->incrementRecipientsTimesContacted(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mContext:Landroid/content/Context;

    iget-object v0, v7, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->mValues:Landroid/content/ContentValues;

    const-string v4, "bccAddresses"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/android/mail/providers/UIProvider;->incrementRecipientsTimesContacted(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;->mSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    const/4 v2, 0x1

    invoke-interface {v0, p0, v2}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;->sendOrSaveFinished(Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;Z)V

    return-void

    :cond_3
    move-wide v10, v12

    goto/16 :goto_0
.end method
