.class public Lcom/android/mail/photo/MailPhotoViewActivity;
.super Lcom/android/ex/photo/PhotoViewActivity;
.source "MailPhotoViewActivity.java"


# instance fields
.field private mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

.field private mMenu:Landroid/view/Menu;

.field private mSaveAllItem:Landroid/view/MenuItem;

.field private mSaveItem:Landroid/view/MenuItem;

.field private mShareAllItem:Landroid/view/MenuItem;

.field private mShareItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/ex/photo/PhotoViewActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/photo/MailPhotoViewActivity;)V
    .locals 0
    .param p0    # Lcom/android/mail/photo/MailPhotoViewActivity;

    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->downloadAttachment()V

    return-void
.end method

.method private downloadAttachment()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/AttachmentActionHandler;->setAttachment(Lcom/android/mail/providers/Attachment;)V

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(I)V

    :cond_0
    return-void
.end method

.method private getAllAttachments()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    :cond_2
    new-instance v2, Lcom/android/mail/providers/Attachment;

    invoke-direct {v2, v0}, Lcom/android/mail/providers/Attachment;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method private saveAllAttachments()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v1, -0x1

    :goto_0
    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/mail/providers/Attachment;

    invoke-direct {v2, v0}, Lcom/android/mail/providers/Attachment;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p0, v2}, Lcom/android/mail/photo/MailPhotoViewActivity;->saveAttachment(Lcom/android/mail/providers/Attachment;)V

    goto :goto_0
.end method

.method private saveAttachment()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/photo/MailPhotoViewActivity;->saveAttachment(Lcom/android/mail/providers/Attachment;)V

    return-void
.end method

.method private saveAttachment(Lcom/android/mail/providers/Attachment;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Attachment;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/AttachmentActionHandler;->setAttachment(Lcom/android/mail/providers/Attachment;)V

    iget-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(I)V

    :cond_0
    return-void
.end method

.method private shareAllAttachments()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, -0x1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/android/mail/providers/Attachment;

    invoke-direct {v3, v0}, Lcom/android/mail/providers/Attachment;-><init>(Landroid/database/Cursor;)V

    iget-object v3, v3, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->normalizeUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v3, v2}, Lcom/android/mail/browse/AttachmentActionHandler;->shareAttachments(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private shareAttachment()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/photo/MailPhotoViewActivity;->shareAttachment(Lcom/android/mail/providers/Attachment;)V

    return-void
.end method

.method private shareAttachment(Lcom/android/mail/providers/Attachment;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Attachment;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/AttachmentActionHandler;->setAttachment(Lcom/android/mail/providers/Attachment;)V

    iget-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v0}, Lcom/android/mail/browse/AttachmentActionHandler;->shareAttachment()V

    :cond_0
    return-void
.end method

.method private updateProgressAndEmptyViews(Lcom/android/ex/photo/fragments/PhotoViewFragment;Lcom/android/mail/providers/Attachment;)V
    .locals 6
    .param p1    # Lcom/android/ex/photo/fragments/PhotoViewFragment;
    .param p2    # Lcom/android/mail/providers/Attachment;

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getPhotoProgressBar()Lcom/android/ex/photo/views/ProgressBarWrapper;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getEmptyText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getRetryButton()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {p2}, Lcom/android/mail/providers/Attachment;->shouldShowProgress()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p2, Lcom/android/mail/providers/Attachment;->size:I

    invoke-virtual {v1, v3}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setMax(I)V

    iget v3, p2, Lcom/android/mail/providers/Attachment;->downloadedSize:I

    invoke-virtual {v1, v3}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setProgress(I)V

    invoke-virtual {v1, v4}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setIndeterminate(Z)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/android/mail/providers/Attachment;->isDownloadFailed()Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f0900ff

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v3, Lcom/android/mail/photo/MailPhotoViewActivity$1;

    invoke-direct {v3, p0}, Lcom/android/mail/photo/MailPhotoViewActivity$1;-><init>(Lcom/android/mail/photo/MailPhotoViewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v5}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->isProgressBarNeeded()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setIndeterminate(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected getCurrentAttachment()Lcom/android/mail/providers/Attachment;
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/android/mail/providers/Attachment;

    invoke-direct {v1, v0}, Lcom/android/mail/providers/Attachment;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/mail/photo/MailPhotoViewActivity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/mail/browse/AttachmentActionHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/mail/browse/AttachmentActionHandler;-><init>(Landroid/content/Context;Lcom/android/mail/browse/AttachmentViewInterface;)V

    iput-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    iget-object v0, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/AttachmentActionHandler;->initialize(Landroid/app/FragmentManager;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iput-object p1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    const v2, 0x7f080132

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mSaveItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    const v2, 0x7f080133

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mSaveAllItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    const v2, 0x7f080134

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    const v2, 0x7f080135

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareAllItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    return v1
.end method

.method public onCursorChanged(Lcom/android/ex/photo/fragments/PhotoViewFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Lcom/android/ex/photo/fragments/PhotoViewFragment;
    .param p2    # Landroid/database/Cursor;

    invoke-super {p0, p1, p2}, Lcom/android/ex/photo/PhotoViewActivity;->onCursorChanged(Lcom/android/ex/photo/fragments/PhotoViewFragment;Landroid/database/Cursor;)V

    new-instance v0, Lcom/android/mail/providers/Attachment;

    invoke-direct {v0, p2}, Lcom/android/mail/providers/Attachment;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p0, p1, v0}, Lcom/android/mail/photo/MailPhotoViewActivity;->updateProgressAndEmptyViews(Lcom/android/ex/photo/fragments/PhotoViewFragment;Lcom/android/mail/providers/Attachment;)V

    return-void
.end method

.method public onFragmentVisible(Lcom/android/ex/photo/fragments/PhotoViewFragment;)V
    .locals 3
    .param p1    # Lcom/android/ex/photo/fragments/PhotoViewFragment;

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onFragmentVisible(Lcom/android/ex/photo/fragments/PhotoViewFragment;)V

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v0

    iget v1, v0, Lcom/android/mail/providers/Attachment;->state:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/AttachmentActionHandler;->setAttachment(Lcom/android/mail/providers/Attachment;)V

    iget-object v1, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    iget v2, v0, Lcom/android/mail/providers/Attachment;->destination:I

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(I)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->finish()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->saveAttachment()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->saveAllAttachments()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->shareAttachment()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->shareAllAttachments()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f080132 -> :sswitch_1
        0x7f080133 -> :sswitch_2
        0x7f080134 -> :sswitch_3
        0x7f080135 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->updateActionItems()V

    const/4 v0, 0x1

    return v0
.end method

.method protected updateActionBar()V
    .locals 7

    const/4 v5, 0x1

    invoke-super {p0}, Lcom/android/ex/photo/PhotoViewActivity;->updateActionBar()V

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v3, v1, Lcom/android/mail/providers/Attachment;->size:I

    int-to-long v3, v3

    invoke-static {p0, v3, v4}, Lcom/android/mail/utils/AttachmentUtils;->convertToHumanReadableSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->isSavedToExternal()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09008b

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->updateActionItems()V

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v1, Lcom/android/mail/providers/Attachment;->destination:I

    if-ne v3, v5, :cond_1

    const v3, 0x7f090091

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSubtitle(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected updateActionItems()V
    .locals 9

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/mail/utils/Utils;->isRunningJellybeanOrLater()Z

    move-result v5

    invoke-virtual {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getCurrentAttachment()Lcom/android/mail/providers/Attachment;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mSaveItem:Landroid/view/MenuItem;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareItem:Landroid/view/MenuItem;

    if-eqz v6, :cond_7

    iget-object v8, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mSaveItem:Landroid/view/MenuItem;

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->isSavedToExternal()Z

    move-result v6

    if-nez v6, :cond_6

    const/4 v6, 0x1

    :goto_0
    invoke-interface {v8, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareItem:Landroid/view/MenuItem;

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->canShare()Z

    move-result v8

    invoke-interface {v6, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/android/mail/photo/MailPhotoViewActivity;->getAllAttachments()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isSavedToExternal()Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v3, 0x1

    :cond_1
    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mSaveAllItem:Landroid/view/MenuItem;

    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->canShare()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v3, 0x0

    :cond_3
    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareAllItem:Landroid/view/MenuItem;

    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_4
    if-nez v5, :cond_5

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareItem:Landroid/view/MenuItem;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mShareAllItem:Landroid/view/MenuItem;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    :goto_1
    return-void

    :cond_6
    move v6, v7

    goto :goto_0

    :cond_7
    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/mail/photo/MailPhotoViewActivity;->mMenu:Landroid/view/Menu;

    const v8, 0x7f080131

    invoke-interface {v6, v8, v7}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    goto :goto_1
.end method
