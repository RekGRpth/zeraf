.class interface abstract Lcom/android/ex/chips/RecipientChip;
.super Ljava/lang/Object;
.source "RecipientChip.java"


# virtual methods
.method public abstract draw(Landroid/graphics/Canvas;)V
.end method

.method public abstract getBounds()Landroid/graphics/Rect;
.end method

.method public abstract getContactId()J
.end method

.method public abstract getDataId()J
.end method

.method public abstract getEntry()Lcom/android/ex/chips/RecipientEntry;
.end method

.method public abstract getOriginalText()Ljava/lang/CharSequence;
.end method

.method public abstract getValue()Ljava/lang/CharSequence;
.end method

.method public abstract isSelected()Z
.end method

.method public abstract setOriginalText(Ljava/lang/String;)V
.end method

.method public abstract setSelected(Z)V
.end method
