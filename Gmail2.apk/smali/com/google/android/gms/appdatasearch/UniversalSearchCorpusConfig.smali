.class public final Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/cn;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/au;


# instance fields
.field public D:I

.field public av:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/au;

    invoke-direct {v0}, Lcom/google/android/gms/internal/au;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/internal/au;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->D:I

    invoke-static {}, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->getSectionsCount()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->av:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/internal/au;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->CREATOR:Lcom/google/android/gms/internal/au;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/au;->a(Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;Landroid/os/Parcel;I)V

    return-void
.end method
