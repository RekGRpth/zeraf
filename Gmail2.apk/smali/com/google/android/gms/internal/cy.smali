.class public Lcom/google/android/gms/internal/cy;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/appdatasearch/SearchResults;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/internal/w;->c(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->D:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/w;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aD:[I

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aE:[B

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[BZ)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aF:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aG:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aH:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aI:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/w;->b(Landroid/os/Parcel;II)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aJ:[I

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[IZ)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->aK:[Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;I[Ljava/lang/String;Z)V

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/w;->c(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public C(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 4

    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/ca;->r(Landroid/os/Parcel;)I

    move-result v2

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v2, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/internal/ca;->q(Landroid/os/Parcel;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/internal/ca;->u(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->e(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->h(Landroid/os/Parcel;I)I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->D:I

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->s(Landroid/os/Parcel;I)[I

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aD:[I

    goto :goto_0

    :sswitch_3
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->q(Landroid/os/Parcel;I)[B

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aE:[B

    goto :goto_0

    :sswitch_4
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/internal/ca;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aF:[Landroid/os/Bundle;

    goto :goto_0

    :sswitch_5
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/internal/ca;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aG:[Landroid/os/Bundle;

    goto :goto_0

    :sswitch_6
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/internal/ca;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aH:[Landroid/os/Bundle;

    goto :goto_0

    :sswitch_7
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->h(Landroid/os/Parcel;I)I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aI:I

    goto :goto_0

    :sswitch_8
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->s(Landroid/os/Parcel;I)[I

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aJ:[I

    goto :goto_0

    :sswitch_9
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ca;->y(Landroid/os/Parcel;I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->aK:[Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v2, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Overread allowed size end="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    return-object v1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public H(I)[Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/cy;->C(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/cy;->H(I)[Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    return-object v0
.end method
