.class final Lcom/google/android/gms/internal/af$c;
.super Lcom/google/android/gms/internal/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "c"
.end annotation


# instance fields
.field final synthetic aA:Lcom/google/android/gms/internal/af;

.field private final fk:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

.field private final fl:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

.field private final fm:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    invoke-direct {p0}, Lcom/google/android/gms/internal/b$a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/af$c;->fk:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

    iput-object p2, p0, Lcom/google/android/gms/internal/af$c;->fl:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

    iput-object p3, p0, Lcom/google/android/gms/internal/af$c;->fm:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;ILandroid/content/Intent;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->fm:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/af;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/af$c;->fm:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    :cond_0
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_1
    new-instance v3, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v3, p1, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->fk:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    new-instance v0, Lcom/google/android/gms/internal/af$a;

    iget-object v1, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    iget-object v2, p0, Lcom/google/android/gms/internal/af$c;->fk:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/af$a;-><init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;Lcom/google/android/gms/common/ConnectionResult;ILandroid/content/Intent;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/af;->a(Lcom/google/android/gms/internal/t$b;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    new-instance v1, Lcom/google/android/gms/internal/af$b;

    iget-object v2, p0, Lcom/google/android/gms/internal/af$c;->aA:Lcom/google/android/gms/internal/af;

    iget-object v4, p0, Lcom/google/android/gms/internal/af$c;->fl:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

    invoke-direct {v1, v2, v4, v3, p4}, Lcom/google/android/gms/internal/af$b;-><init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/af;->a(Lcom/google/android/gms/internal/t$b;)V

    goto :goto_0
.end method
