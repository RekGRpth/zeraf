.class public Lcom/google/android/gm/GoogleMailSwitchService;
.super Landroid/app/IntentService;
.source "GoogleMailSwitchService.java"


# static fields
.field private static final GMAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

.field private static final GOOGLE_MAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gm"

    const-string v2, "com.google.android.gm.widget.GmailWidgetProvider"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gm/GoogleMailSwitchService;->GMAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gm"

    const-string v2, "com.google.android.gm.widget.GoogleMailWidgetProvider"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gm/GoogleMailSwitchService;->GOOGLE_MAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "GoogleMailSwitchService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private validateGmailWidgets()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gm/GoogleMailSwitchService;->validateWidgetProvider()V

    invoke-static {p0}, Lcom/google/android/gm/Utils;->enableLabelShortcutActivity(Landroid/content/Context;)V

    return-void
.end method

.method private validateWidgetProvider()V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/gm/GoogleMailSwitchService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/gm/Utils;->haveGoogleMailActivitiesBeenEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/google/android/gm/GoogleMailSwitchService;->GOOGLE_MAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    :goto_0
    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/gm/GoogleMailSwitchService;->GMAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    :goto_1
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    invoke-virtual {v3, v1, v5, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return-void

    :cond_0
    sget-object v1, Lcom/google/android/gm/GoogleMailSwitchService;->GMAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gm/GoogleMailSwitchService;->GOOGLE_MAIL_WIDGET_PROVIDER_COMPONENT_NAME:Landroid/content/ComponentName;

    goto :goto_1
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gm/GoogleMailSwitchService;->validateGmailWidgets()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gm/GoogleMailDeviceStartupReceiver;->enableReceiver(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/gm/GoogleMailSwitchService;->validateGmailWidgets()V

    goto :goto_0
.end method
