.class public Lcom/google/android/gm/ApplicationMenuHandler;
.super Ljava/lang/Object;
.source "ApplicationMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleApplicationMenu(ILandroid/content/Context;Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;)Z
    .locals 3
    .param p0    # I
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v0

    sparse-switch p0, :sswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :sswitch_0
    invoke-static {p1}, Lcom/google/android/gm/Utils;->showSettings(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gm/persistence/Persistence;->getActiveAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gm/Utils;->startSync(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    invoke-static {p1, p2}, Lcom/google/android/gm/Utils;->showHelp(Landroid/content/Context;Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;)V

    goto :goto_0

    :sswitch_3
    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/gm/Utils;->launchGoogleFeedback(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080116 -> :sswitch_3
        0x7f080117 -> :sswitch_2
        0x7f080125 -> :sswitch_1
        0x7f08012d -> :sswitch_0
    .end sparse-switch
.end method

.method public static handleApplicationMenu(Landroid/view/MenuItem;Landroid/content/Context;Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;)Z
    .locals 1
    .param p0    # Landroid/view/MenuItem;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;

    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gm/ApplicationMenuHandler;->handleApplicationMenu(ILandroid/content/Context;Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;)Z

    move-result v0

    return v0
.end method
