.class public Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;
.super Lcom/android/mail/utils/MatrixCursorWithCachedColumns;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AppDataSearchCursor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gm/SuggestionsProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/gm/SuggestionsProvider;[Ljava/lang/String;)V
    .locals 0
    .param p2    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;->this$0:Lcom/google/android/gm/SuggestionsProvider;

    invoke-direct {p0, p2}, Lcom/android/mail/utils/MatrixCursorWithCachedColumns;-><init>([Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public query(Ljava/lang/String;Landroid/database/Cursor;)Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/Cursor;

    const/4 v10, 0x0

    const/4 v3, 0x0

    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    if-eqz p2, :cond_4

    const v15, 0x7f020049

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v15

    if-eqz v15, :cond_3

    const/4 v15, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v15, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v13, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-interface {v13, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v11, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    aput-object v14, v15, v16

    const/16 v16, 0x3

    aput-object v7, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;->addRow([Ljava/lang/Object;)V

    move v10, v11

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    new-instance v15, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;

    invoke-direct {v15, v4, v14, v7}, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-static {}, Lcom/google/android/gm/provider/MailProvider;->getAppDataSearch()Lcom/google/android/gm/provider/AppDataSearch;

    move-result-object v12

    invoke-static {}, Lcom/google/android/gm/provider/UiProvider;->getLastActiveAccount()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    const/16 v15, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v1, v15}, Lcom/google/android/gm/provider/AppDataSearch;->suggest(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SuggestionResults;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v9}, Lcom/google/android/gms/appdatasearch/SuggestionResults;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/appdatasearch/SuggestionResults$Result;

    invoke-virtual {v8}, Lcom/google/android/gms/appdatasearch/SuggestionResults$Result;->getQuery()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_5

    invoke-virtual {v8}, Lcom/google/android/gms/appdatasearch/SuggestionResults$Result;->getDisplayText()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move-object v4, v14

    :cond_6
    invoke-interface {v13, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v11, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    aput-object v14, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;->this$0:Lcom/google/android/gm/SuggestionsProvider;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/gm/SuggestionsProvider;->mEmptyIcon:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/gm/SuggestionsProvider;->access$000(Lcom/google/android/gm/SuggestionsProvider;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;->addRow([Ljava/lang/Object;)V

    move v10, v11

    goto :goto_1

    :cond_7
    if-eqz v3, :cond_9

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;

    iget-object v15, v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mDisplayText:Ljava/lang/String;

    invoke-interface {v13, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_8

    iget-object v15, v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mDisplayText:Ljava/lang/String;

    invoke-interface {v13, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v11, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    iget-object v0, v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mDisplayText:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x2

    iget-object v0, v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mSuggestion:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x3

    iget-object v0, v2, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mIcon:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/gm/SuggestionsProvider$AppDataSearchCursor;->addRow([Ljava/lang/Object;)V

    move v10, v11

    goto :goto_2

    :cond_9
    return-object p0
.end method
