.class public Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;
.super Landroid/app/DialogFragment;
.source "DisableAccountNotificationsDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;
    }
.end annotation


# instance fields
.field private mListener:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->mListener:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    iget-object v0, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->mListener:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;
    .locals 3
    .param p0    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    invoke-direct {v1}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "accountName"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09016d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09016e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900e1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900e0

    new-instance v3, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;-><init>(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public setListener(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->mListener:Ljava/lang/ref/WeakReference;

    return-void
.end method
