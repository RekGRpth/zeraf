.class public Lcom/google/android/gm/preference/FancySummaryListPreference;
.super Landroid/preference/ListPreference;
.source "FancySummaryListPreference.java"


# instance fields
.field private mEntrySummaries:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gm/preference/FancySummaryListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/google/android/gm/R$styleable;->FancySummaryListPreference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/preference/FancySummaryListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;

    return-void
.end method

.method private getSummaryForValue(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gm/preference/FancySummaryListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gm/preference/FancySummaryListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gm/preference/FancySummaryListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/gm/preference/FancySummaryListPreference;->getSummaryForValue(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gm/preference/FancySummaryListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
