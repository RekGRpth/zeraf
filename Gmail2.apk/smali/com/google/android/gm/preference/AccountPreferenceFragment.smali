.class public final Lcom/google/android/gm/preference/AccountPreferenceFragment;
.super Lcom/google/android/gm/preference/GmailPreferenceFragment;
.source "AccountPreferenceFragment.java"

# interfaces
.implements Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;
.implements Lcom/google/android/gm/preference/IntegerPickerPreference$Callbacks;


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mGmail:Lcom/google/android/gm/provider/Gmail;

.field private mPersistence:Lcom/google/android/gm/persistence/Persistence;

.field private mSettings:Lcom/google/android/gm/provider/Gmail$Settings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gm/preference/AccountPreferenceFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/AccountPreferenceFragment;

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gm/preference/AccountPreferenceFragment;Lcom/google/android/gm/provider/LabelList;Landroid/preference/CheckBoxPreference;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/AccountPreferenceFragment;
    .param p1    # Lcom/google/android/gm/provider/LabelList;
    .param p2    # Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->promptDisableNotifications(Lcom/google/android/gm/provider/LabelList;Landroid/preference/CheckBoxPreference;)V

    return-void
.end method

.method private attachDisableNotificationsDialogListener()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DisableAccountNotificationsDialogFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->setListener(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;)V

    :cond_0
    return-void
.end method

.method private initializeSyncCheckbox()V
    .locals 6

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    new-instance v0, Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    const-string v5, "com.google"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "gmail-ls"

    invoke-static {v0, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    const-string v5, "sync_status"

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {p0, v5, v4}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const-string v4, "sync_status"

    invoke-virtual {p0, v4}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-eqz v2, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const v4, 0x7f09019f

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1
.end method

.method private migrateNotificationSettings(Z)V
    .locals 7
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v3, "^i"

    :goto_0
    if-eqz p1, :cond_1

    const-string v2, "^iim"

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v5, v3, v6}, Lcom/google/android/gm/persistence/Persistence;->getNotificationLabelInformation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, v3}, Lcom/google/android/gm/persistence/Persistence;->clearNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, v2}, Lcom/google/android/gm/persistence/Persistence;->clearNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_0
    const-string v3, "^iim"

    goto :goto_0

    :cond_1
    const-string v2, "^i"

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, v2, v1}, Lcom/google/android/gm/persistence/Persistence;->addNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    goto :goto_2
.end method

.method private promptDisableNotifications(Lcom/google/android/gm/provider/LabelList;Landroid/preference/CheckBoxPreference;)V
    .locals 9
    .param p1    # Lcom/google/android/gm/provider/LabelList;
    .param p2    # Landroid/preference/CheckBoxPreference;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gm/provider/LabelList;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/gm/provider/LabelList;->get(I)Lcom/google/android/gm/provider/Label;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gm/provider/Label;->getCanonicalName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/gm/persistence/Persistence;->shouldNotifyForLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "^i"

    invoke-virtual {v2}, Lcom/google/android/gm/provider/Label;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v3, 0x1

    :cond_0
    const/4 v4, 0x1

    if-eqz v3, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p2, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->setListener(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "DisableAccountNotificationsDialogFragment"

    invoke-virtual {v0, v5, v6}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/gm/persistence/Persistence;->setEnableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gm/preference/PreferenceUtils;->validateNotificationsForAccount(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private setInboxSettingsState(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const-string v7, "inbox-settings"

    invoke-virtual {p0, v7}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "vibrator"

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/Vibrator;

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v7, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v8, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v7, p1, v8}, Lcom/google/android/gm/persistence/Persistence;->getPriorityInboxDefault(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v0, :cond_3

    if-eqz v5, :cond_2

    const v4, 0x7f09019c

    :goto_1
    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v7, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {p1, v7}, Lcom/google/android/gm/persistence/Persistence;->getAccountInbox(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {p1, v7, v1}, Lcom/google/android/gm/Utils;->getLabelNotificationSummary(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v4, 0x7f09019a

    goto :goto_1

    :cond_3
    if-eqz v5, :cond_4

    const v4, 0x7f09019d

    goto :goto_1

    :cond_4
    const v4, 0x7f09019b

    goto :goto_1
.end method

.method private setNotificationSettingState(Landroid/app/Activity;Lcom/google/android/gm/persistence/Persistence;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/gm/persistence/Persistence;

    const-string v0, "enable-notifications"

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {p2, p1, v1}, Lcom/google/android/gm/persistence/Persistence;->getEnableNotifications(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    invoke-direct {p0, p1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->setInboxSettingsState(Landroid/content/Context;)V

    return-void
.end method

.method private setPreferenceChangeListener(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private updatePreferenceList()V
    .locals 6

    const-string v3, "number-picker"

    invoke-virtual {p0, v3}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/preference/IntegerPickerPreference;

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v3}, Lcom/google/android/gm/provider/Gmail$Settings;->getConversationAgeDays()J

    move-result-wide v3

    long-to-int v3, v3

    invoke-virtual {v0, p0, v3}, Lcom/google/android/gm/preference/IntegerPickerPreference;->bind(Lcom/google/android/gm/preference/IntegerPickerPreference$Callbacks;I)V

    const-string v3, "signature"

    invoke-virtual {p0, v3}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gm/persistence/Persistence;->getSignature(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f090170

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeSyncCheckbox()V

    return-void

    :cond_0
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private useMultiPaneUI()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v2, v0, Landroid/preference/PreferenceActivity;

    if-eqz v2, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/preference/PreferenceActivity;

    invoke-virtual {v1}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v2

    goto :goto_0
.end method

.method private validateSyncSetForInboxSetting(Landroid/preference/Preference;)V
    .locals 7
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/google/android/gm/persistence/Persistence;->getAccountInbox(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gm/comm/longshadow/LongShadowUtils;->getContentProviderMailAccess(Landroid/content/ContentResolver;)Lcom/google/android/gm/provider/Gmail;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v0, v6}, Lcom/google/android/gm/provider/Gmail;->getSettings(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gm/provider/Gmail$Settings;

    move-result-object v5

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/gm/provider/Gmail$Settings;->getLabelsPartial()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/gm/provider/Gmail$Settings;->getLabelsIncluded()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5, v4}, Lcom/google/android/gm/provider/Gmail$Settings;->setLabelsPartial(Ljava/util/Collection;)V

    iget-object v6, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v6, v5}, Lcom/google/android/gm/provider/Gmail;->setSettings(Ljava/lang/String;Lcom/google/android/gm/provider/Gmail$Settings;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->useMultiPaneUI()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gm/comm/longshadow/LongShadowUtils;->getContentProviderMailAccess(Landroid/content/ContentResolver;)Lcom/google/android/gm/provider/Gmail;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mGmail:Lcom/google/android/gm/provider/Gmail;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gm/provider/Gmail;->getSettings(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gm/provider/Gmail$Settings;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    const/high16 v1, 0x7f060000

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->updatePreferenceList()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onNotificationsDisabled()V
    .locals 4

    const-string v1, "enable-notifications"

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gm/persistence/Persistence;->getEnableNotifications(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method public onNumberChanged(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gm/provider/Gmail$Settings;->setConversationAgeDays(J)V

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->updatePreferenceList()V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gm/provider/Gmail;->setSettings(Ljava/lang/String;Lcom/google/android/gm/provider/Gmail$Settings;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v4, "signature"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, v3}, Lcom/google/android/gm/persistence/Persistence;->setSignature(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->updatePreferenceList()V

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v3, "enable-notifications"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "enable-notifications"

    invoke-virtual {p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/gm/preference/AccountPreferenceFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment$1;-><init>(Lcom/google/android/gm/preference/AccountPreferenceFragment;Landroid/preference/CheckBoxPreference;)V

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gm/persistence/Persistence;->getAccountInbox(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/gm/persistence/Persistence;->getNotificationLabelInformation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ON:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ON:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0, v2}, Lcom/google/android/gm/persistence/Persistence;->addNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->setInboxSettingsState(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gm/persistence/Persistence;->setEnableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gm/preference/PreferenceUtils;->validateNotificationsForAccount(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v3, "action-strip-action-reply-all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    check-cast p2, Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gm/persistence/Persistence;->setActionStripActionReplyAll(Landroid/content/Context;Z)V

    goto/16 :goto_1

    :cond_4
    const-string v3, "priority-inbox-key"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v0, "priority-inbox-key"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/gm/persistence/Persistence;->setPriorityInboxDefault(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->migrateNotificationSettings(Z)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-direct {p0, v0, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->setNotificationSettingState(Landroid/app/Activity;Lcom/google/android/gm/persistence/Persistence;)V

    invoke-direct {p0, p2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->validateSyncSetForInboxSetting(Landroid/preference/Preference;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gm/preference/PreferenceUtils;->validateNotificationsForAccount(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const-string v3, "prefetch-attachments"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    check-cast p2, Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gm/persistence/Persistence;->setPrefetchAttachments(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_6
    const-string v3, "manage-labels"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/google/android/gm/LabelsActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_key"

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_7
    const-string v3, "inbox-settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gm/persistence/Persistence;->getAccountInbox(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/google/android/gm/LabelsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_key"

    iget-object v4, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "label"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    const-string v3, "sync_status"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-nez v2, :cond_9

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.settings.SYNC_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "authorities"

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "gmail-ls"

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_9
    const-string v0, "sync_status"

    invoke-virtual {p0, v0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "gmail-ls"

    invoke-static {v2, v3, v0}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeSyncCheckbox()V

    const-string v1, "prefetch-attachments"

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gm/persistence/Persistence;->getPrefetchAttachments(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const-string v1, "signature"

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gm/persistence/Persistence;->getSignature(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeEditText(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->setNotificationSettingState(Landroid/app/Activity;Lcom/google/android/gm/persistence/Persistence;)V

    const-string v1, "priority-inbox-key"

    iget-object v2, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mPersistence:Lcom/google/android/gm/persistence/Persistence;

    iget-object v3, p0, Lcom/google/android/gm/preference/AccountPreferenceFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gm/persistence/Persistence;->getPriorityInboxDefault(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const-string v1, "signature"

    invoke-direct {p0, v1}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->setPreferenceChangeListener(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gm/preference/AccountPreferenceFragment;->attachDisableNotificationsDialogListener()V

    return-void
.end method
