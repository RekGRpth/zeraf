.class public Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;
.super Lcom/google/android/common/html/parser/HtmlTree$DefaultPlainTextConverter;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GmailMessagePlainTextConverter"
.end annotation


# static fields
.field private static final ELIDED_TEXT_ATTRIBUTE:Lcom/google/android/common/html/parser/HTML$Attribute;

.field private static final ELIDED_TEXT_REPLACEMENT_NODE:Lcom/google/android/common/html/parser/HtmlDocument$Node;


# instance fields
.field private mEndNodeElidedTextBlock:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/common/html/parser/HTML$Attribute;

    const-string v1, "class"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/common/html/parser/HTML$Attribute;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->ELIDED_TEXT_ATTRIBUTE:Lcom/google/android/common/html/parser/HTML$Attribute;

    sget-object v0, Lcom/google/android/common/html/parser/HTML4;->BR_ELEMENT:Lcom/google/android/common/html/parser/HTML$Element;

    invoke-static {v0, v3, v3, v3}, Lcom/google/android/common/html/parser/HtmlDocument;->createSelfTerminatingTag(Lcom/google/android/common/html/parser/HTML$Element;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->ELIDED_TEXT_REPLACEMENT_NODE:Lcom/google/android/common/html/parser/HtmlDocument$Node;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/common/html/parser/HtmlTree$DefaultPlainTextConverter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->mEndNodeElidedTextBlock:I

    return-void
.end method


# virtual methods
.method public addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V
    .locals 8
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Node;
    .param p2    # I
    .param p3    # I

    iget v6, p0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->mEndNodeElidedTextBlock:I

    if-ge p2, v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v6, p0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->mEndNodeElidedTextBlock:I

    if-ne p2, v6, :cond_2

    sget-object v6, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->ELIDED_TEXT_REPLACEMENT_NODE:Lcom/google/android/common/html/parser/HtmlDocument$Node;

    invoke-super {p0, v6, p2, p3}, Lcom/google/android/common/html/parser/HtmlTree$DefaultPlainTextConverter;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    goto :goto_0

    :cond_2
    instance-of v6, p1, Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    if-eqz v6, :cond_5

    const/4 v2, 0x0

    move-object v4, p1

    check-cast v4, Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    invoke-virtual {v4}, Lcom/google/android/common/html/parser/HtmlDocument$Tag;->getElement()Lcom/google/android/common/html/parser/HTML$Element;

    move-result-object v3

    const-string v6, "div"

    invoke-virtual {v3}, Lcom/google/android/common/html/parser/HTML$Element;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget-object v6, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->ELIDED_TEXT_ATTRIBUTE:Lcom/google/android/common/html/parser/HTML$Attribute;

    invoke-virtual {v4, v6}, Lcom/google/android/common/html/parser/HtmlDocument$Tag;->getAttributes(Lcom/google/android/common/html/parser/HTML$Attribute;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/common/html/parser/HtmlDocument$TagAttribute;

    const-string v6, "elided-text"

    invoke-virtual {v0}, Lcom/google/android/common/html/parser/HtmlDocument$TagAttribute;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iput p3, p0, Lcom/google/android/gm/Utils$GmailMessagePlainTextConverter;->mEndNodeElidedTextBlock:I

    const/4 v2, 0x1

    :cond_4
    if-nez v2, :cond_0

    :cond_5
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/common/html/parser/HtmlTree$DefaultPlainTextConverter;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    goto :goto_0
.end method
