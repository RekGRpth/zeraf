.class Lcom/google/android/gm/SuggestionsProvider$ContactInfo;
.super Ljava/lang/Object;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ContactInfo"
.end annotation


# instance fields
.field public final mDisplayText:Ljava/lang/String;

.field public final mIcon:Ljava/lang/String;

.field public final mSuggestion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mDisplayText:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mSuggestion:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gm/SuggestionsProvider$ContactInfo;->mIcon:Ljava/lang/String;

    return-void
.end method
