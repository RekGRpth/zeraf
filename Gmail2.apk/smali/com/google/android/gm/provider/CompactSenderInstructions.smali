.class public Lcom/google/android/gm/provider/CompactSenderInstructions;
.super Ljava/lang/Object;
.source "CompactSenderInstructions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/provider/CompactSenderInstructions$2;
    }
.end annotation


# static fields
.field private static final SENDER_LIST_CACHE:Lcom/android/mail/utils/ObjectCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;


# instance fields
.field private mHasErrors:Z

.field private mHasSending:Z

.field private mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    sget-object v1, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    sput-object v0, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;

    new-instance v0, Lcom/android/mail/utils/ObjectCache;

    new-instance v1, Lcom/google/android/gm/provider/CompactSenderInstructions$1;

    invoke-direct {v1}, Lcom/google/android/gm/provider/CompactSenderInstructions$1;-><init>()V

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/android/mail/utils/ObjectCache;-><init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V

    sput-object v0, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_CACHE:Lcom/android/mail/utils/ObjectCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gm/provider/SenderInstructions;

    invoke-direct {v0}, Lcom/google/android/gm/provider/SenderInstructions;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    iput-boolean v1, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasSending:Z

    iput-boolean v1, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasErrors:Z

    return-void
.end method

.method private static appendElided(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;

    const-string v0, "e"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static appendNumDrafts(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # I

    if-eqz p1, :cond_0

    const-string v0, "d"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private static appendNumMessages(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # I

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    const-string v0, "n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private static appendSendFailed(Ljava/lang/StringBuilder;Z)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v0, "f"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private static appendSender(Ljava/lang/StringBuilder;ZILjava/lang/String;Z)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Z
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-eqz p4, :cond_0

    invoke-static {p3}, Lcom/google/android/gm/provider/CompactSenderInstructions;->shortNameFromLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static appendSending(Ljava/lang/StringBuilder;Z)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v0, "s"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/gm/provider/Gmail;->SENDER_LIST_SEPARATOR:Ljava/lang/Character;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private static constructString(Ljava/util/Collection;IIIZZ)Ljava/lang/String;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gm/provider/SenderInstructions$Sender;",
            ">;IIIZZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v4, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p4}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendSending(Ljava/lang/StringBuilder;Z)V

    invoke-static {v0, p5}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendSendFailed(Ljava/lang/StringBuilder;Z)V

    invoke-static {v0, p1}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendNumMessages(Ljava/lang/StringBuilder;I)V

    invoke-static {v0, p2}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendNumDrafts(Ljava/lang/StringBuilder;I)V

    if-le p3, v4, :cond_1

    :goto_0
    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    sget-object v5, Lcom/google/android/gm/provider/CompactSenderInstructions$2;->$SwitchMap$com$google$android$gm$provider$SenderInstructions$Visibility:[I

    iget-object v6, v3, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    invoke-virtual {v6}, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-boolean v5, v3, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    iget v6, v3, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    iget-object v7, v3, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    invoke-static {v0, v5, v6, v7, v4}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendSender(Ljava/lang/StringBuilder;ZILjava/lang/String;Z)V

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :pswitch_1
    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/gm/provider/CompactSenderInstructions;->appendElided(Ljava/lang/StringBuilder;)V

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static instructionsStringFromProto(Lcom/google/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 14
    .param p0    # Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v13, 0x5

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {p0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {p0, v12, v9}, Lcom/google/android/gm/provider/ProtoBufHelpers;->getAllProtoBufs(Lcom/google/common/io/protocol/ProtoBuf;ILjava/util/Collection;)V

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/common/io/protocol/ProtoBuf;

    invoke-virtual {v8, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    invoke-direct {v7}, Lcom/google/android/gm/provider/SenderInstructions$Sender;-><init>()V

    sget-object v5, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->HIDDEN:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    invoke-direct {v7}, Lcom/google/android/gm/provider/SenderInstructions$Sender;-><init>()V

    invoke-virtual {v8, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v5

    iput-boolean v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    invoke-virtual {v8, v12}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    iput v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    const/4 v5, 0x4

    invoke-virtual {v8, v5}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    invoke-virtual {v8, v13}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v8, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    :cond_1
    sget-object v5, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->VISIBLE:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v5, v7, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/CompactSenderInstructions;->constructString(Ljava/util/Collection;IIIZZ)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static instructionsStringFromXml(Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;)Ljava/lang/String;
    .locals 11
    .param p0    # Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/parser/xml/SimplePullParser$ParseException;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const-string v5, "numMessages"

    invoke-virtual {p0, v10, v5}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getIntAttribute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const-string v5, "numDrafts"

    invoke-virtual {p0, v10, v5}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getIntAttribute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getDepth()I

    move-result v6

    :cond_0
    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->nextTag(I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    const-string v5, "sender"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    invoke-direct {v8}, Lcom/google/android/gm/provider/SenderInstructions$Sender;-><init>()V

    const-string v5, "unread"

    invoke-virtual {p0, v7, v5}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getIntAttribute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    iput-boolean v5, v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    const-string v5, "priority"

    invoke-virtual {p0, v7, v5}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getIntAttribute(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    iput v5, v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    const-string v5, "name"

    invoke-virtual {p0, v7, v5}, Lcom/google/wireless/gdata2/parser/xml/SimplePullParser;->getStringAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->VISIBLE:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v5, v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v5, v4

    goto :goto_1

    :cond_2
    const-string v5, "elided"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    invoke-direct {v8}, Lcom/google/android/gm/provider/SenderInstructions$Sender;-><init>()V

    sget-object v5, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->HIDDEN:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v5, v8, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/CompactSenderInstructions;->constructString(Ljava/util/Collection;IIIZZ)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static parseCompactSenderInstructions(Ljava/lang/String;Lcom/google/android/gm/provider/SenderInstructions;)V
    .locals 16
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/gm/provider/SenderInstructions;

    sget-object v1, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1}, Lcom/android/mail/utils/ObjectCache;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    :try_start_0
    invoke-interface {v14}, Ljava/util/List;->clear()V

    sget-object v3, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v1, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    :goto_0
    sget-object v1, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-virtual {v1}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_SPLITTER:Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-virtual {v1}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v1

    sget-object v3, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v3, v14}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v11, 0x0

    move v12, v11

    :goto_1
    :try_start_4
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v1

    if-ge v12, v1, :cond_a

    add-int/lit8 v11, v12, 0x1

    invoke-interface {v14, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_2
    move v12, v11

    goto :goto_1

    :cond_2
    const-string v1, "e"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "n"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_3
    const-string v1, "d"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v12, v11, 0x1

    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gm/provider/SenderInstructions;->setNumDrafts(I)V

    move v11, v12

    goto :goto_2

    :cond_4
    const-string v1, "l"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    add-int/lit8 v12, v11, 0x1

    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    const/4 v6, 0x1

    :goto_3
    const/4 v7, -0x1

    move-object/from16 v1, p1

    move-object v3, v2

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gm/provider/SenderInstructions;->addMessage(Ljava/lang/String;Ljava/lang/String;ZZZI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v11, v12

    :cond_5
    :goto_4
    sget-object v1, Lcom/google/android/gm/provider/CompactSenderInstructions;->SENDER_LIST_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1, v14}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gm/provider/SenderInstructions;->calculateVisibility(I)V

    return-void

    :cond_6
    const/4 v6, 0x0

    goto :goto_3

    :cond_7
    :try_start_5
    const-string v1, "s"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "f"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    add-int/lit8 v1, v11, 0x2

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    if-gt v1, v3, :cond_5

    move-object v15, v10

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const/4 v7, 0x1

    :goto_5
    add-int/lit8 v12, v11, 0x1

    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    add-int/lit8 v11, v12, 0x1

    invoke-interface {v14, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    const/4 v8, 0x1

    :goto_6
    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/gm/provider/SenderInstructions;->addMessage(Ljava/lang/String;Ljava/lang/String;ZZZI)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_2

    :cond_8
    const/4 v7, 0x0

    goto :goto_5

    :cond_9
    const/4 v8, 0x0

    goto :goto_6

    :cond_a
    move v11, v12

    goto :goto_4
.end method

.method public static parseCompactSenderInstructions([BLcom/google/android/gm/provider/SenderInstructions;)V
    .locals 17
    .param p0    # [B
    .param p1    # Lcom/google/android/gm/provider/SenderInstructions;

    :try_start_0
    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/gm/provider/protos/GmsProtosMessageTypes;->SENDER_INSTRUCTIONS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    const/4 v1, 0x1

    invoke-virtual {v14, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    const/4 v1, 0x2

    invoke-virtual {v14, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v10

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v16

    const/4 v1, 0x3

    move-object/from16 v0, v16

    invoke-static {v14, v1, v0}, Lcom/google/android/gm/provider/ProtoBufHelpers;->getAllProtoBufs(Lcom/google/common/io/protocol/ProtoBuf;ILjava/util/Collection;)V

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v12, 0x1

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/common/io/protocol/ProtoBuf;

    new-instance v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    invoke-direct {v13}, Lcom/google/android/gm/provider/SenderInstructions$Sender;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v12, :cond_1

    const/4 v1, 0x4

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->HIDDEN:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    const-string v2, "e"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, -0x1

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gm/provider/SenderInstructions;->addMessage(Ljava/lang/String;Ljava/lang/String;ZZZI)V

    goto :goto_1

    :catch_0
    move-exception v8

    const-string v1, "TAG"

    const-string v2, "Unable to parse sender instructions protobuf"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_2
    return-void

    :cond_0
    const/4 v12, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    iput-boolean v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    const/4 v1, 0x3

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    const/4 v1, 0x4

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {v15, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    :cond_2
    sget-object v1, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->VISIBLE:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iput-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    iget-object v2, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    iget-object v3, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    iget-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v6, 0x1

    :goto_3
    iget v7, v13, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gm/provider/SenderInstructions;->addMessage(Ljava/lang/String;Ljava/lang/String;ZZZI)V

    goto/16 :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/gm/provider/SenderInstructions;->setNumMessages(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/gm/provider/SenderInstructions;->setNumDrafts(I)V

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gm/provider/SenderInstructions;->calculateVisibility(I)V

    goto :goto_2
.end method

.method private static shortNameFromLongName(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0    # Ljava/lang/String;

    const/4 v11, 0x2

    const/4 v10, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    const-string v9, "\""

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "\""

    invoke-virtual {p0, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v9, v11, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    :cond_0
    move-object v5, p0

    const/16 v9, 0x2c

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v1, v9, :cond_2

    const/4 v9, 0x0

    invoke-virtual {v5, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v9, "\\s+"

    invoke-static {v4, v9}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v9, v8

    if-ge v2, v9, :cond_1

    aget-object v6, v8, v2

    const-string v9, "."

    invoke-virtual {v6, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    add-int/lit8 v3, v3, 0x1

    if-lt v3, v11, :cond_4

    :cond_1
    if-ne v3, v10, :cond_2

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    :cond_2
    const-string v0, "the "

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "the "

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "the "

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    :cond_3
    const-string v9, "\\s+"

    invoke-static {v5, v9}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x0

    :goto_1
    array-length v9, v7

    if-ge v2, v9, :cond_6

    aget-object v6, v7, v2

    const-string v9, "."

    invoke-virtual {v6, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    :goto_2
    return-object v6

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    move-object v6, p0

    goto :goto_2
.end method


# virtual methods
.method public addMessage(Ljava/lang/String;ZZZZZ)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    const/4 v2, 0x0

    const/4 v5, 0x1

    if-eqz p5, :cond_2

    iput-boolean v5, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasSending:Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    if-nez p1, :cond_3

    move-object v1, v2

    :goto_1
    if-nez p1, :cond_4

    :goto_2
    if-nez p4, :cond_1

    if-nez p5, :cond_1

    if-eqz p6, :cond_5

    :cond_1
    :goto_3
    const/4 v6, -0x1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gm/provider/SenderInstructions;->addMessage(Ljava/lang/String;Ljava/lang/String;ZZZI)V

    return-void

    :cond_2
    if-eqz p6, :cond_0

    iput-boolean v5, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasErrors:Z

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/google/android/gm/provider/Gmail;->getNameFromAddressString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-static {p1}, Lcom/google/android/gm/provider/Gmail;->getEmailFromAddressString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public setNumMessages(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    invoke-virtual {v0, p1}, Lcom/google/android/gm/provider/SenderInstructions;->setNumMessages(I)V

    return-void
.end method

.method public toByteArray()[B
    .locals 14

    iget-object v11, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Lcom/google/android/gm/provider/SenderInstructions;->calculateVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    new-instance v7, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v11, Lcom/google/android/gm/provider/protos/GmsProtosMessageTypes;->SENDER_INSTRUCTIONS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v11}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v11, 0x1

    invoke-virtual {v4}, Lcom/google/android/gm/provider/SenderInstructions;->getNumMessages()I

    move-result v12

    invoke-virtual {v7, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v11, 0x2

    invoke-virtual {v4}, Lcom/google/android/gm/provider/SenderInstructions;->getNumDrafts()I

    move-result v12

    invoke-virtual {v7, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {v4}, Lcom/google/android/gm/provider/SenderInstructions;->getSenders()Ljava/util/Collection;

    move-result-object v9

    invoke-virtual {v4}, Lcom/google/android/gm/provider/SenderInstructions;->getNumVisible()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_1

    const/4 v10, 0x1

    :goto_0
    new-instance v1, Lcom/google/android/common/html/parser/HtmlParser;

    invoke-direct {v1}, Lcom/google/android/common/html/parser/HtmlParser;-><init>()V

    new-instance v2, Lcom/google/android/common/html/parser/HtmlTreeBuilder;

    invoke-direct {v2}, Lcom/google/android/common/html/parser/HtmlTreeBuilder;-><init>()V

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    sget-object v12, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->SKIPPED:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    if-eq v11, v12, :cond_0

    const/4 v11, 0x3

    invoke-virtual {v7, v11}, Lcom/google/common/io/protocol/ProtoBuf;->addNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/4 v12, 0x1

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    sget-object v13, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->HIDDEN:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    if-ne v11, v13, :cond_2

    const/4 v11, 0x1

    :goto_2
    invoke-virtual {v8, v12, v11}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/4 v11, 0x2

    iget-boolean v12, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    invoke-virtual {v8, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/4 v11, 0x3

    iget v12, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    invoke-virtual {v8, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    sget-object v12, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->HIDDEN:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    if-ne v11, v12, :cond_3

    const-string v5, "e"

    :goto_3
    const/4 v11, 0x4

    invoke-virtual {v8, v11, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v12, 0x5

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    if-eqz v11, :cond_5

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/gm/provider/Gmail;->getEmailFromAddressString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    :goto_4
    invoke-virtual {v8, v12, v11}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    :cond_3
    if-eqz v10, :cond_4

    iget-object v11, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/gm/provider/CompactSenderInstructions;->shortNameFromLongName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_5
    invoke-static {v5, v1, v2}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;Lcom/google/android/common/html/parser/HtmlParser;Lcom/google/android/common/html/parser/HtmlTreeBuilder;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_4
    iget-object v5, v6, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    goto :goto_5

    :cond_5
    const-string v11, ""

    goto :goto_4

    :cond_6
    :try_start_0
    invoke-virtual {v7}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    :goto_6
    return-object v11

    :catch_0
    move-exception v0

    const/4 v11, 0x0

    goto :goto_6
.end method

.method public toInstructionString(I)Ljava/lang/String;
    .locals 6
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    invoke-virtual {v1, p1}, Lcom/google/android/gm/provider/SenderInstructions;->calculateVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/SenderInstructions;->getSenders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    invoke-virtual {v2}, Lcom/google/android/gm/provider/SenderInstructions;->getNumDrafts()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mSenderInstructions:Lcom/google/android/gm/provider/SenderInstructions;

    invoke-virtual {v3}, Lcom/google/android/gm/provider/SenderInstructions;->getNumVisible()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasSending:Z

    iget-boolean v5, p0, Lcom/google/android/gm/provider/CompactSenderInstructions;->mHasErrors:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/CompactSenderInstructions;->constructString(Ljava/util/Collection;IIIZZ)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
