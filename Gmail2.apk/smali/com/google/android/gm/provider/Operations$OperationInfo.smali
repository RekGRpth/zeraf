.class public Lcom/google/android/gm/provider/Operations$OperationInfo;
.super Ljava/lang/Object;
.source "Operations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/Operations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OperationInfo"
.end annotation


# instance fields
.field public mAction:Ljava/lang/String;

.field public mConversationId:J

.field public mDelay:I

.field public mLabelId:J

.field public mMessageId:J

.field public mNextTimeToAttempt:J

.field public mNumAttempts:I


# direct methods
.method public constructor <init>(JJLjava/lang/String;J)V
    .locals 12
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gm/provider/Operations$OperationInfo;-><init>(JJLjava/lang/String;JIIJ)V

    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;JIIJ)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # I
    .param p9    # I
    .param p10    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mConversationId:J

    iput-wide p3, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mMessageId:J

    iput-object p5, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    iput-wide p6, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mLabelId:J

    iput p8, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNumAttempts:I

    iput p9, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mDelay:I

    iput-wide p10, p0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNextTimeToAttempt:J

    return-void
.end method
