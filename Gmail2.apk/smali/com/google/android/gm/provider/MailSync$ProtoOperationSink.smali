.class Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;
.super Lcom/google/android/gm/provider/MailSync$AbstractOperationSink;
.source "MailSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/MailSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProtoOperationSink"
.end annotation


# instance fields
.field private final mParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private mParts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/common/http/Part;",
            ">;"
        }
    .end annotation
.end field

.field mUphillSyncProto:Lcom/google/common/io/protocol/ProtoBuf;

.field final synthetic this$0:Lcom/google/android/gm/provider/MailSync;


# direct methods
.method constructor <init>(Lcom/google/android/gm/provider/MailSync;Lcom/google/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p2    # Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->this$0:Lcom/google/android/gm/provider/MailSync;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gm/provider/MailSync$AbstractOperationSink;-><init>(Lcom/google/android/gm/provider/MailSync;Lcom/google/android/gm/provider/MailSync$1;)V

    iput-object p2, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mUphillSyncProto:Lcom/google/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParams:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    return-void
.end method

.method private newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p1    # J

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mUphillSyncProto:Lcom/google/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->addNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2, p1, p2}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    return-object v0
.end method

.method private shouldIgnoreLabelOperation(J)Z
    .locals 2
    .param p1    # J

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public conversationLabelAddedOrRemoved(JJJZ)V
    .locals 5
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # Z

    const/4 v3, 0x3

    invoke-direct {p0, p5, p6}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->shouldIgnoreLabelOperation(J)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v3, p7}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p3, p4}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    invoke-static {p5, p6}, Lcom/google/android/gm/provider/MailCore;->isLabelIdLocal(J)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v0, v2, p5, p6}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x4

    sget-object v2, Lcom/google/android/gm/provider/Gmail;->LOCAL_PRIORITY_LABELS:Lcom/google/common/collect/BiMap;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getNumOperations()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mUphillSyncProto:Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    return v0
.end method

.method public getParts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/common/http/Part;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hasAttachments()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public messageExpunged(JJ)V
    .locals 2
    .param p1    # J
    .param p3    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3, p4}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    return-void
.end method

.method public messageLabelAdded(JJJ)V
    .locals 5
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-direct {p0, p5, p6}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->shouldIgnoreLabelOperation(J)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    invoke-virtual {v0, v3, p3, p4}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    invoke-static {p5, p6}, Lcom/google/android/gm/provider/MailCore;->isLabelIdLocal(J)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v4, p5, p6}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x4

    sget-object v2, Lcom/google/android/gm/provider/Gmail;->LOCAL_PRIORITY_LABELS:Lcom/google/common/collect/BiMap;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public messageLabelRemoved(JJJ)V
    .locals 5
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const/4 v4, 0x2

    invoke-direct {p0, p5, p6}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->shouldIgnoreLabelOperation(J)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p3, p4}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    invoke-virtual {v0, v4, p5, p6}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    goto :goto_0
.end method

.method public messageSavedOrSent(JLcom/google/android/gm/provider/MailSync$Message;JJZ)V
    .locals 27
    .param p1    # J
    .param p3    # Lcom/google/android/gm/provider/MailSync$Message;
    .param p4    # J
    .param p6    # J
    .param p8    # Z

    const-string v22, "Gmail"

    const-string v23, "MailSync posting operation %d (messageSavedOrSent)"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/google/android/gm/provider/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct/range {p0 .. p2}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->newOperationProto(J)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v15

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v18

    const/16 v22, 0xa

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, p8

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/16 v22, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-wide/from16 v2, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/16 v22, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-wide/from16 v2, p6

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/16 v22, 0x3

    const-string v23, ", "

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->toAddresses:Ljava/util/List;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v22, 0x4

    const-string v23, ", "

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->ccAddresses:Ljava/util/List;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v22, 0x5

    const-string v23, ", "

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->bccAddresses:Ljava/util/List;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v23, 0x6

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->subject:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->subject:Ljava/lang/String;

    move-object/from16 v22, v0

    :goto_0
    move-object/from16 v0, v18

    move/from16 v1, v23

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v23, 0x7

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->body:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_4

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->body:Ljava/lang/String;

    move-object/from16 v22, v0

    :goto_1
    move-object/from16 v0, v18

    move/from16 v1, v23

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->clientCreated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->includeQuotedText:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    const/16 v22, 0xb

    const/16 v23, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/16 v22, 0xd

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->quoteStartPos:J

    move-wide/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-wide/from16 v2, v23

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->forward:Z

    move/from16 v22, v0

    if-eqz v22, :cond_0

    const/16 v22, 0xc

    const/16 v23, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    :cond_0
    :goto_2
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->customFromAddress:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1

    const/16 v22, 0xe

    const/16 v23, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    const/16 v22, 0xf

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->customFromAddress:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const-string v22, "Gmail"

    const-string v23, "Uphill Sync operation with custom from address: %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->customFromAddress:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/google/android/gm/provider/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    const/16 v19, 0x1

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->rawAttachments:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_c

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;

    iget v0, v4, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->origin:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    new-instance v22, Ljava/lang/IllegalArgumentException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Unknown origin: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget v0, v4, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->origin:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22

    :cond_3
    const-string v22, ""

    goto/16 :goto_0

    :cond_4
    const-string v22, ""

    goto/16 :goto_1

    :cond_5
    const/16 v22, 0xb

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    goto/16 :goto_2

    :pswitch_0
    const/16 v22, 0x8

    invoke-virtual {v4}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->getServerExtras()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_1
    iget-object v0, v4, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_7

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->messageId:J

    move-wide/from16 v23, v0

    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v19, v19, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->this$0:Lcom/google/android/gm/provider/MailSync;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/gm/provider/MailSync;->mResolver:Landroid/content/ContentResolver;
    invoke-static/range {v22 .. v22}, Lcom/google/android/gm/provider/MailSync;->access$300(Lcom/google/android/gm/provider/MailSync;)Landroid/content/ContentResolver;

    move-result-object v22

    const-string v23, "gmail_use_multipart_protobuf"

    const/16 v24, 0x1

    invoke-static/range {v22 .. v24}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v22

    if-eqz v22, :cond_8

    const/16 v21, 0x1

    :goto_5
    const/4 v10, 0x0

    if-eqz v21, :cond_9

    const/16 v16, 0x0

    :try_start_0
    new-instance v17, Lcom/google/android/gm/provider/MailSync$AttachmentPartSource;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->this$0:Lcom/google/android/gm/provider/MailSync;

    move-object/from16 v22, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->messageId:J

    move-wide/from16 v23, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move-wide/from16 v2, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gm/provider/MailSync$AttachmentPartSource;-><init>(Lcom/google/android/gm/provider/MailSync;JLcom/google/android/gm/provider/uiprovider/GmailAttachment;)V

    new-instance v16, Lcom/google/android/common/http/FilePart;

    invoke-virtual {v4}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->getContentType()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/common/http/FilePart;-><init>(Ljava/lang/String;Lcom/google/android/common/http/PartSource;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    if-nez v22, :cond_6

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->mParts:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_6
    const/16 v22, 0x9

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->addNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v20

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v22, 0x2

    invoke-virtual {v4}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->getName()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v22, 0x3

    invoke-virtual {v4}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->getContentType()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    if-nez v21, :cond_2

    const/16 v22, 0x4

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1, v10}, Lcom/google/common/io/protocol/ProtoBuf;->setBytes(I[B)V

    goto/16 :goto_3

    :cond_7
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->messageId:J

    move-wide/from16 v23, v0

    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v4, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    :cond_8
    const/16 v21, 0x0

    goto/16 :goto_5

    :catch_0
    move-exception v9

    const-string v22, "Gmail"

    const-string v23, "IO error while reading attachment: %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v5, v24, v25

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->notifyAttachmentFailed(Lcom/google/android/gm/provider/MailSync$Message;Lcom/google/android/gm/provider/uiprovider/GmailAttachment;)V

    goto/16 :goto_3

    :catch_1
    move-exception v9

    const-string v22, "Gmail"

    const-string v23, "SecurityException while reading attachment: %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v5, v24, v25

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->notifyAttachmentFailed(Lcom/google/android/gm/provider/MailSync$Message;Lcom/google/android/gm/provider/uiprovider/GmailAttachment;)V

    goto/16 :goto_3

    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->this$0:Lcom/google/android/gm/provider/MailSync;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/gm/provider/MailSync;->mStore:Lcom/google/android/gm/provider/MailStore;
    invoke-static/range {v22 .. v22}, Lcom/google/android/gm/provider/MailSync;->access$100(Lcom/google/android/gm/provider/MailSync;)Lcom/google/android/gm/provider/MailStore;

    move-result-object v22

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->messageId:J

    move-wide/from16 v23, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-interface {v0, v1, v2, v4}, Lcom/google/android/gm/provider/MailStore;->getInputStreamForUploadedAttachment(JLcom/google/android/gm/provider/uiprovider/GmailAttachment;)Ljava/io/InputStream;

    move-result-object v12

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v22, 0x400

    move/from16 v0, v22

    new-array v8, v0, [B

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    :cond_a
    invoke-virtual {v12, v8}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-gez v7, :cond_b

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    goto/16 :goto_6

    :cond_b
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v6, v8, v0, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v13

    const-wide/32 v24, 0x36ee80

    cmp-long v22, v22, v24

    if-lez v22, :cond_a

    new-instance v22, Ljava/io/IOException;

    const-string v23, "Timed out reading attachment"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3

    :catch_2
    move-exception v9

    const-string v22, "Gmail"

    const-string v23, "IO error while reading attachment: %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v5, v24, v25

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->notifyAttachmentFailed(Lcom/google/android/gm/provider/MailSync$Message;Lcom/google/android/gm/provider/uiprovider/GmailAttachment;)V

    goto/16 :goto_3

    :catch_3
    move-exception v9

    const-string v22, "Gmail"

    const-string v23, "Security exception while reading attachment: %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v5, v24, v25

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gm/provider/MailSync$ProtoOperationSink;->notifyAttachmentFailed(Lcom/google/android/gm/provider/MailSync$Message;Lcom/google/android/gm/provider/uiprovider/GmailAttachment;)V

    goto/16 :goto_3

    :cond_c
    const-string v22, "Gmail"

    const-string v23, "messageSavedOrSent: message = %s"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    invoke-static/range {v22 .. v24}, Lcom/google/android/gm/provider/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
