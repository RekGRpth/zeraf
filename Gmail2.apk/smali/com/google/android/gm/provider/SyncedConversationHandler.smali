.class public Lcom/google/android/gm/provider/SyncedConversationHandler;
.super Lcom/google/android/gm/provider/ConversationHandler;
.source "SyncedConversationHandler.java"


# direct methods
.method protected constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gm/provider/MailCore;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Lcom/google/android/gm/provider/MailCore;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gm/provider/ConversationHandler;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gm/provider/MailCore;)V

    return-void
.end method


# virtual methods
.method protected onConversationChangedImpl(JLcom/google/android/gm/provider/MailSync$SyncRationale;Ljava/lang/String;Ljava/util/Map;JLjava/util/Map;ZLandroid/util/TimingLogger;)Z
    .locals 61
    .param p1    # J
    .param p3    # Lcom/google/android/gm/provider/MailSync$SyncRationale;
    .param p4    # Ljava/lang/String;
    .param p6    # J
    .param p9    # Z
    .param p10    # Landroid/util/TimingLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/gm/provider/MailSync$SyncRationale;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/gm/provider/LabelRecord;",
            ">;J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/gm/provider/LabelRecord;",
            ">;Z",
            "Landroid/util/TimingLogger;",
            ")Z"
        }
    .end annotation

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v22

    const-wide/high16 v15, -0x8000000000000000L

    const/high16 v50, -0x80000000

    const/16 v17, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v48, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    sget-object v19, Lcom/google/android/gm/provider/MailSync$SyncRationale;->NONE:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    const-wide/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "SELECT \n  messageId,\n  fromAddress,\n  group_concat(labels_id, \' \'),\n  subject,\n  snippet,\n  personalLevel,\n  length(joinedAttachmentInfos) > 0 as hasAttachments,\n  dateReceivedMs,\n  error\nFROM\n  messages LEFT OUTER JOIN   message_labels ON messageId = message_messageId\nWHERE\n  synced = 1 AND conversation = ?\nGROUP BY messageId\nORDER BY messageId"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v22, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    const-string v9, "fetch messages"

    move-object/from16 v0, p10

    invoke-virtual {v0, v9}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gm/provider/CompactSenderInstructions;

    invoke-direct {v4}, Lcom/google/android/gm/provider/CompactSenderInstructions;-><init>()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdDraft()J

    move-result-wide v33

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdUnread()J

    move-result-wide v44

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdSent()J

    move-result-wide v37

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdOutbox()J

    move-result-wide v35

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdSpam()J

    move-result-wide v39

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mLabelMap:Lcom/google/android/gm/provider/Gmail$LabelMap;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$LabelMap;->getLabelIdTrash()J

    move-result-wide v42

    :goto_0
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_15

    const/4 v9, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    const/4 v9, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x7

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    const/4 v9, 0x2

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    if-eqz v47, :cond_2

    sget-object v9, Lcom/google/android/gm/provider/Gmail;->SPACE_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v47

    invoke-static {v0, v9}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/lang/String;

    move-result-object v56

    :goto_1
    const/16 v55, 0x0

    move-object/from16 v18, v56

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v49, v0

    const/16 v29, 0x0

    :goto_2
    move/from16 v0, v29

    move/from16 v1, v49

    if-ge v0, v1, :cond_3

    aget-object v41, v18, v29

    invoke-static/range {v41 .. v41}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v31

    cmp-long v9, v39, v31

    if-eqz v9, :cond_0

    cmp-long v9, v42, v31

    if-nez v9, :cond_1

    :cond_0
    const/16 v55, 0x1

    :cond_1
    add-int/lit8 v29, v29, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    new-array v0, v9, [Ljava/lang/String;

    move-object/from16 v56, v0

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v54, 0x0

    move-object/from16 v18, v56

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v49, v0

    const/16 v29, 0x0

    :goto_3
    move/from16 v0, v29

    move/from16 v1, v49

    if-ge v0, v1, :cond_e

    aget-object v41, v18, v29

    invoke-static/range {v41 .. v41}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v31

    cmp-long v9, v33, v31

    if-nez v9, :cond_9

    const/4 v6, 0x1

    :cond_4
    :goto_4
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-interface {v0, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_c

    new-instance v46, Lcom/google/android/gm/provider/LabelRecord;

    invoke-direct/range {v46 .. v46}, Lcom/google/android/gm/provider/LabelRecord;-><init>()V

    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p8

    move-object/from16 v1, v46

    invoke-interface {v0, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    move-object/from16 v0, v46

    iget-wide v9, v0, Lcom/google/android/gm/provider/LabelRecord;->sortMessageId:J

    move-wide/from16 v0, v52

    invoke-static {v9, v10, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    move-object/from16 v0, v46

    iput-wide v9, v0, Lcom/google/android/gm/provider/LabelRecord;->sortMessageId:J

    move-object/from16 v0, v46

    iget-wide v9, v0, Lcom/google/android/gm/provider/LabelRecord;->dateReceived:J

    move-wide/from16 v0, v25

    invoke-static {v9, v10, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    move-object/from16 v0, v46

    iput-wide v9, v0, Lcom/google/android/gm/provider/LabelRecord;->dateReceived:J

    if-nez v55, :cond_5

    const/4 v9, 0x0

    move-object/from16 v0, v46

    iput-boolean v9, v0, Lcom/google/android/gm/provider/LabelRecord;->isZombie:Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mMailCore:Lcom/google/android/gm/provider/MailCore;

    iget-object v9, v9, Lcom/google/android/gm/provider/MailCore;->mLabelIdsIncludedOrPartial:Ljava/util/Map;

    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Boolean;

    if-nez v55, :cond_6

    if-eqz v30, :cond_6

    sget-object v9, Lcom/google/android/gm/provider/MailSync$SyncRationale;->NONE:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    move-object/from16 v0, p3

    if-eq v0, v9, :cond_6

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_d

    sget-object v19, Lcom/google/android/gm/provider/MailSync$SyncRationale;->LABEL:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    :cond_6
    :goto_6
    cmp-long v9, v52, p6

    if-lez v9, :cond_8

    if-nez v17, :cond_7

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v17

    :cond_7
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_3

    :cond_9
    cmp-long v9, v44, v31

    if-nez v9, :cond_a

    const/4 v7, 0x1

    goto :goto_4

    :cond_a
    cmp-long v9, v37, v31

    if-nez v9, :cond_b

    const/4 v8, 0x1

    goto/16 :goto_4

    :cond_b
    cmp-long v9, v35, v31

    if-nez v9, :cond_4

    const/16 v54, 0x1

    goto/16 :goto_4

    :cond_c
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/google/android/gm/provider/LabelRecord;

    goto :goto_5

    :cond_d
    sget-object v9, Lcom/google/android/gm/provider/MailSync$SyncRationale;->LABEL:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    move-object/from16 v0, v19

    if-eq v0, v9, :cond_6

    sget-object v19, Lcom/google/android/gm/provider/MailSync$SyncRationale;->DURATION:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    move-wide/from16 v0, v20

    move-wide/from16 v2, v52

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v20

    goto :goto_6

    :cond_e
    if-nez v59, :cond_f

    const/4 v9, 0x3

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v59

    :cond_f
    const/4 v9, 0x4

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v48

    if-nez v60, :cond_10

    if-eqz v7, :cond_10

    move-object/from16 v60, v48

    :cond_10
    const/4 v9, 0x5

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v58

    move/from16 v0, v50

    move/from16 v1, v58

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v50

    move-wide v0, v15

    move-wide/from16 v2, v52

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v15

    const/4 v9, 0x6

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_11

    const/4 v9, 0x1

    :goto_7
    or-int v27, v27, v9

    const/16 v9, 0x8

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_12

    const/16 v51, 0x1

    :goto_8
    or-int v28, v28, v51

    if-eqz v54, :cond_13

    if-nez v51, :cond_13

    const/4 v9, 0x1

    :goto_9
    if-eqz v54, :cond_14

    if-eqz v51, :cond_14

    const/4 v10, 0x1

    :goto_a
    invoke-virtual/range {v4 .. v10}, Lcom/google/android/gm/provider/CompactSenderInstructions;->addMessage(Ljava/lang/String;ZZZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v9

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v9

    :cond_11
    const/4 v9, 0x0

    goto :goto_7

    :cond_12
    const/16 v51, 0x0

    goto :goto_8

    :cond_13
    const/4 v9, 0x0

    goto :goto_9

    :cond_14
    const/4 v10, 0x0

    goto :goto_a

    :cond_15
    const-wide/16 v9, 0x0

    cmp-long v9, v20, v9

    if-eqz v9, :cond_17

    :try_start_1
    sget-object v9, Lcom/google/android/gm/provider/MailSync$SyncRationale;->DURATION:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    move-object/from16 v0, p3

    if-eq v0, v9, :cond_16

    sget-object v9, Lcom/google/android/gm/provider/MailSync$SyncRationale;->LABEL:Lcom/google/android/gm/provider/MailSync$SyncRationale;

    move-object/from16 v0, p3

    if-ne v0, v9, :cond_17

    :cond_16
    move-object/from16 v0, p3

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_17

    const-string v9, "Gmail"

    const-string v10, "Server sent rational %s but we calculated %s with messageId %d"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p3, v11, v12

    const/4 v12, 0x1

    aput-object v19, v11, v12

    const/4 v12, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_17
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v57

    move/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/google/android/gm/provider/CompactSenderInstructions;->setNumMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    const-string v9, "process messages"

    move-object/from16 v0, p10

    invoke-virtual {v0, v9}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    move-object/from16 v9, p0

    move-wide/from16 v10, p1

    move-object/from16 v12, p3

    move-object/from16 v13, p5

    move-object/from16 v14, p8

    invoke-virtual/range {v9 .. v17}, Lcom/google/android/gm/provider/SyncedConversationHandler;->updateLabelInfo(JLcom/google/android/gm/provider/MailSync$SyncRationale;Ljava/util/Map;Ljava/util/Map;JLjava/util/Set;)V

    const-string v9, "process labels"

    move-object/from16 v0, p10

    invoke-virtual {v0, v9}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    if-nez v57, :cond_18

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "conversations"

    const-string v11, "_id = ? AND queryId = 0"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v22, v12, v13

    invoke-virtual {v9, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v9, 0x1

    :goto_b
    return v9

    :cond_18
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "_id"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "queryId"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "subject"

    move-object/from16 v0, v23

    move-object/from16 v1, v59

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "snippet"

    if-eqz v60, :cond_19

    :goto_c
    move-object/from16 v0, v23

    move-object/from16 v1, v60

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "personalLevel"

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "numMessages"

    invoke-static/range {v57 .. v57}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "maxMessageId"

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/gm/provider/Gmail;->getLabelIdsStringFromLabelIds(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v47

    const-string v9, "labelIds"

    move-object/from16 v0, v23

    move-object/from16 v1, v47

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "hasAttachments"

    if-eqz v27, :cond_1a

    const/4 v9, 0x1

    :goto_d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v23

    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "hasMessagesWithErrors"

    if-eqz v28, :cond_1b

    const/4 v9, 0x1

    :goto_e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v23

    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "syncRationale"

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gm/provider/MailSync$SyncRationale;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "syncRationaleMessageId"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "fromProtoBuf"

    invoke-virtual {v4}, Lcom/google/android/gm/provider/CompactSenderInstructions;->toByteArray()[B

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gm/provider/SyncedConversationHandler;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "conversations"

    const/4 v11, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v9, v10, v11, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const/4 v9, 0x0

    goto/16 :goto_b

    :cond_19
    move-object/from16 v60, v48

    goto/16 :goto_c

    :cond_1a
    const/4 v9, 0x0

    goto :goto_d

    :cond_1b
    const/4 v9, 0x0

    goto :goto_e
.end method
