.class public Lcom/google/android/gm/provider/uiprovider/UILabelCursor;
.super Lcom/google/android/gm/provider/uiprovider/UICursorWapper;
.source "UILabelCursor.java"


# static fields
.field private static final ALLOW_MARK_NOT_SPAM_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ARCHIVABLE_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DELETE_PROHIBITED_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DESTRUCTIVE_MUTE_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final HIDDEN_REPORT_PHISHING_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final HIDDEN_REPORT_SPAM_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTINGS_PROHIBITED_LABELS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private mCanonicalName:Ljava/lang/String;

.field private final mCanonicalNameIndex:I

.field private mConversationListUri:Landroid/net/Uri;

.field private final mEngine:Lcom/google/android/gm/provider/MailEngine;

.field private mGmailLabelColor:Ljava/lang/String;

.field private final mGmailLabelColorIndex:I

.field private final mIdColumnIndex:I

.field private final mNameColumnIndex:I

.field private final mNumConversationsIndex:I

.field private final mNumUnreadConversationsIndex:I

.field private final mSynchronizedLabelSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSystemLabelIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-string v0, "^i"

    const-string v1, "^iim"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->ARCHIVABLE_LABELS:Ljava/util/Set;

    const-string v0, "^k"

    const-string v1, "^r"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->DELETE_PROHIBITED_LABELS:Ljava/util/Set;

    const-string v0, "^i"

    const-string v1, "^iim"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->DESTRUCTIVE_MUTE_LABELS:Ljava/util/Set;

    const-string v0, "^k"

    const-string v1, "^b"

    const-string v2, "^^out"

    const-string v3, "^r"

    const-string v4, "^all"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->SETTINGS_PROHIBITED_LABELS:Ljava/util/Set;

    const-string v0, "^s"

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->HIDDEN_REPORT_SPAM_LABELS:Ljava/util/Set;

    sget-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->HIDDEN_REPORT_SPAM_LABELS:Ljava/util/Set;

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->ALLOW_MARK_NOT_SPAM_LABELS:Ljava/util/Set;

    const-string v0, "^s"

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->HIDDEN_REPORT_PHISHING_LABELS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Lcom/google/android/gm/provider/MailEngine;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/gm/provider/MailEngine;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    invoke-direct {p0, p1, p4}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;-><init>(Landroid/database/Cursor;[Ljava/lang/String;)V

    iput-object p3, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v0}, Lcom/google/android/gm/provider/MailEngine;->getSynchronizedLabelSet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mSynchronizedLabelSet:Ljava/util/Set;

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mIdColumnIndex:I

    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNameColumnIndex:I

    const-string v0, "canonicalName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalNameIndex:I

    const-string v0, "numConversations"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNumConversationsIndex:I

    const-string v0, "numUnreadConversations"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNumUnreadConversationsIndex:I

    const-string v0, "systemLabel"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mSystemLabelIndex:I

    const-string v0, "color"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mGmailLabelColorIndex:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cachePositionValues()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalNameIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mIdColumnIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gm/provider/UiProvider;->getLabelConversationListUri(Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mConversationListUri:Landroid/net/Uri;

    :goto_0
    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mGmailLabelColorIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mGmailLabelColor:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gm/provider/UiProvider;->getLabelConversationListFromNameUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mConversationListUri:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public getInt(I)I
    .locals 10
    .param p1    # I

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->cachePositionValues()V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v7, "Gmail"

    const-string v8, "UILabelCursor.getInt(%d): Unexpected column"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-static {v7, v8, v5}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v6

    :goto_0
    :pswitch_1
    return v6

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    goto :goto_0

    :pswitch_3
    iget v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNumUnreadConversationsIndex:I

    invoke-super {p0, v5}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v6

    goto :goto_0

    :pswitch_4
    iget v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNumConversationsIndex:I

    invoke-super {p0, v5}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v6

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->ARCHIVABLE_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    or-int/lit8 v0, v0, 0x10

    :cond_0
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->SETTINGS_PROHIBITED_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    or-int/lit16 v0, v0, 0x200

    :cond_1
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->DELETE_PROHIBITED_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    or-int/lit8 v0, v0, 0x20

    :cond_2
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->HIDDEN_REPORT_SPAM_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    or-int/lit8 v0, v0, 0x40

    :cond_3
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->ALLOW_MARK_NOT_SPAM_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    or-int/lit16 v0, v0, 0x80

    :cond_4
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->HIDDEN_REPORT_PHISHING_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    or-int/lit16 v0, v0, 0x2000

    :cond_5
    sget-object v5, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->DESTRUCTIVE_MUTE_LABELS:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    or-int/lit16 v0, v0, 0x100

    :cond_6
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gm/provider/Gmail;->isLabelUserSettable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    or-int/lit8 v0, v0, 0x8

    :cond_7
    const-string v5, "^io_im"

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string v5, "^iim"

    iget-object v6, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_8
    or-int/lit16 v0, v0, 0x400

    :cond_9
    move v6, v0

    goto/16 :goto_0

    :pswitch_6
    iget-object v7, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mSynchronizedLabelSet:Ljava/util/Set;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mSynchronizedLabelSet:Ljava/util/Set;

    iget-object v8, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    move v1, v5

    :goto_1
    if-eqz v1, :cond_b

    :goto_2
    move v6, v5

    goto/16 :goto_0

    :cond_a
    move v1, v6

    goto :goto_1

    :cond_b
    move v5, v6

    goto :goto_2

    :pswitch_7
    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v5}, Lcom/google/android/gm/provider/MailEngine;->isBackgroundSyncInProgress()Z

    move-result v5

    if-eqz v5, :cond_c

    or-int/lit8 v3, v3, 0x4

    :cond_c
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v5}, Lcom/google/android/gm/provider/MailEngine;->isLiveQueryInProgress()Z

    move-result v5

    if-eqz v5, :cond_d

    or-int/lit8 v3, v3, 0x2

    :cond_d
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v5}, Lcom/google/android/gm/provider/MailEngine;->isHandlingUserRefresh()Z

    move-result v5

    if-eqz v5, :cond_e

    or-int/lit8 v3, v3, 0x1

    :cond_e
    move v6, v3

    goto/16 :goto_0

    :pswitch_8
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v5}, Lcom/google/android/gm/provider/MailEngine;->getLastSyncResult()I

    move-result v2

    :goto_3
    move v6, v2

    goto/16 :goto_0

    :cond_f
    move v2, v6

    goto :goto_3

    :pswitch_9
    iget v7, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mSystemLabelIndex:I

    invoke-super {p0, v7}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_10

    move v4, v5

    :goto_4
    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/gm/provider/UiProvider;->getFolderType(ZLjava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :cond_10
    move v4, v6

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public getLong(I)J
    .locals 5
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->cachePositionValues()V

    sparse-switch p1, :sswitch_data_0

    const-string v0, "Gmail"

    const-string v1, "UILabelCursor.getLong(%d): Unexpected column"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    :sswitch_1
    const-wide/16 v0, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public getString(I)Ljava/lang/String;
    .locals 7
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->cachePositionValues()V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v2, "Gmail"

    const-string v3, "UILabelCursor.getString(%d): Unexpected column"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    :pswitch_1
    return-object v0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gm/provider/UiProvider;->getAccountLabelUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget v2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mNameColumnIndex:I

    invoke-super {p0, v2}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mConversationListUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mConversationListUri:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/android/gm/provider/UiProvider;->getLabelRefreshUri(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mGmailLabelColor:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/gm/provider/Label;->getBackgroundColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mAccount:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mGmailLabelColor:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/gm/provider/Label;->getTextColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected resetCursorRowState()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UILabelCursor;->mCanonicalName:Ljava/lang/String;

    return-void
.end method
