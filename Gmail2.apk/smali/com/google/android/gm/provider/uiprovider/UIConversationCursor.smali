.class public Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;
.super Lcom/google/android/gm/provider/uiprovider/UICursorWapper;
.source "UIConversationCursor.java"


# static fields
.field private static final CONVERSATION_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache",
            "<",
            "Lcom/android/mail/providers/ConversationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final GMAIL_ERROR_UI_ERROR_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final GMAIL_STATUS_UI_STATUS_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MESSAGE_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache",
            "<",
            "Lcom/android/mail/providers/MessageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final SENDER_INSTRUCTIONS_CACHE:Lcom/android/mail/utils/ObjectCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache",
            "<",
            "Lcom/google/android/gm/provider/SenderInstructions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mBaseUriString:Ljava/lang/String;

.field private final mCachedLabels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gm/provider/Label;",
            ">;"
        }
    .end annotation
.end field

.field private final mConversationIdIndex:I

.field private mCurrentConversationUri:Ljava/lang/String;

.field private final mCurrentLabels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gm/provider/Label;",
            ">;"
        }
    .end annotation
.end field

.field private final mDateIndex:I

.field private final mFromIndex:I

.field private final mFromProtoBufIndex:I

.field private final mHasAttachmentsIndex:I

.field private final mLabelColorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLabelToFolderMap:Lvedroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/LongSparseArray",
            "<",
            "Lcom/android/mail/providers/Folder;",
            ">;"
        }
    .end annotation
.end field

.field private final mLabelsIndex:I

.field private final mNumMessagesIndex:I

.field private final mPersonalLevelIndex:I

.field private mRowDataCached:Z

.field private mSnippet:Ljava/lang/String;

.field private final mSnippetIndex:I

.field private final mSubjectIndex:I

.field private final mSyncedIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x2

    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorStatus;->LOADED:Lcom/google/android/gm/provider/Gmail$CursorStatus;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorStatus;->LOADING:Lcom/google/android/gm/provider/Gmail$CursorStatus;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorStatus;->SEARCHING:Lcom/google/android/gm/provider/Gmail$CursorStatus;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorStatus;->ERROR:Lcom/google/android/gm/provider/Gmail$CursorStatus;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorStatus;->COMPLETE:Lcom/google/android/gm/provider/Gmail$CursorStatus;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->GMAIL_STATUS_UI_STATUS_MAP:Ljava/util/Map;

    new-instance v0, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorError;->AUTH_ERROR:Lcom/google/android/gm/provider/Gmail$CursorError;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorError;->DB_ERROR:Lcom/google/android/gm/provider/Gmail$CursorError;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorError;->IO_ERROR:Lcom/google/android/gm/provider/Gmail$CursorError;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorError;->PARSE_ERROR:Lcom/google/android/gm/provider/Gmail$CursorError;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gm/provider/Gmail$CursorError;->UNKNOWN_ERROR:Lcom/google/android/gm/provider/Gmail$CursorError;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Gmail$CursorError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->GMAIL_ERROR_UI_ERROR_MAP:Ljava/util/Map;

    new-instance v0, Lcom/android/mail/utils/ObjectCache;

    new-instance v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$1;

    invoke-direct {v1}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$1;-><init>()V

    invoke-direct {v0, v1, v3}, Lcom/android/mail/utils/ObjectCache;-><init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->SENDER_INSTRUCTIONS_CACHE:Lcom/android/mail/utils/ObjectCache;

    new-instance v0, Lcom/android/mail/utils/ObjectCache;

    new-instance v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$2;

    invoke-direct {v1}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$2;-><init>()V

    invoke-direct {v0, v1, v3}, Lcom/android/mail/utils/ObjectCache;-><init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->CONVERSATION_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    new-instance v0, Lcom/android/mail/utils/ObjectCache;

    new-instance v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$3;

    invoke-direct {v1}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor$3;-><init>()V

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/mail/utils/ObjectCache;-><init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V

    sput-object v0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->MESSAGE_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0, p1, p3}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;-><init>(Landroid/database/Cursor;[Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCachedLabels:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mRowDataCached:Z

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    new-instance v0, Lvedroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Lvedroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelToFolderMap:Lvedroid/support/v4/util/LongSparseArray;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelColorMap:Ljava/util/HashMap;

    iput-object p2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mConversationIdIndex:I

    const-string v0, "subject"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSubjectIndex:I

    const-string v0, "snippet"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSnippetIndex:I

    const-string v0, "fromAddress"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mFromIndex:I

    const-string v0, "date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mDateIndex:I

    const-string v0, "personalLevel"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mPersonalLevelIndex:I

    const-string v0, "numMessages"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mNumMessagesIndex:I

    const-string v0, "hasAttachments"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mHasAttachmentsIndex:I

    const-string v0, "conversationLabels"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelsIndex:I

    const-string v0, "synced"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSyncedIndex:I

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gm/provider/UiProvider;->getConversationBaseUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mBaseUriString:Ljava/lang/String;

    const-string v0, "fromProtoBuf"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mFromProtoBufIndex:I

    return-void
.end method

.method private cachePositionValues(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mRowDataCached:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentConversationUri:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentConversationUri:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getConversationId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/UiProvider;->getConversationUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentConversationUri:Ljava/lang/String;

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->loadLabels(Ljava/util/Map;)V

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getSnippet()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSnippet:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mRowDataCached:Z

    :cond_2
    return-void
.end method

.method public static generateConversationInfo(ILjava/lang/String;Lcom/android/mail/providers/ConversationInfo;Lcom/google/android/gm/provider/SenderInstructions;ZZ)V
    .locals 16
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/mail/providers/ConversationInfo;
    .param p3    # Lcom/google/android/gm/provider/SenderInstructions;
    .param p4    # Z
    .param p5    # Z

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gm/provider/SenderInstructions;->getNumDrafts()I

    move-result v3

    move-object/from16 v4, p1

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gm/provider/SenderInstructions;->getSenders()Ljava/util/Collection;

    move-result-object v15

    move-object/from16 v1, p2

    move/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Lcom/android/mail/providers/ConversationInfo;->set(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;

    sget-object v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->MESSAGE_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1}, Lcom/android/mail/utils/ObjectCache;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mail/providers/MessageInfo;

    iget-object v1, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->visibility:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    sget-object v2, Lcom/google/android/gm/provider/SenderInstructions$Visibility;->VISIBLE:Lcom/google/android/gm/provider/SenderInstructions$Visibility;

    if-ne v1, v2, :cond_0

    if-nez p4, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    :cond_1
    iget-boolean v1, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->unread:Z

    if-nez v1, :cond_2

    const/4 v8, 0x1

    :goto_1
    iget-object v10, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->name:Ljava/lang/String;

    iget v11, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->priority:I

    iget-object v12, v14, Lcom/google/android/gm/provider/SenderInstructions$Sender;->email:Ljava/lang/String;

    move/from16 v9, p5

    invoke-virtual/range {v7 .. v12}, Lcom/android/mail/providers/MessageInfo;->set(ZZLjava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/android/mail/providers/ConversationInfo;->addMessage(Lcom/android/mail/providers/MessageInfo;)V

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method private generateConversationInfo()[B
    .locals 8

    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mFromProtoBufIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getBlob(I)[B

    move-result-object v7

    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mNumMessagesIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v0

    sget-object v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->CONVERSATION_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1}, Lcom/android/mail/utils/ObjectCache;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/ConversationInfo;

    const/4 v6, 0x0

    sget-object v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->SENDER_INSTRUCTIONS_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1}, Lcom/android/mail/utils/ObjectCache;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/provider/SenderInstructions;

    :try_start_0
    invoke-virtual {v2}, Lcom/android/mail/providers/ConversationInfo;->reset()V

    invoke-virtual {v3}, Lcom/google/android/gm/provider/SenderInstructions;->reset()V

    if-eqz v7, :cond_0

    array-length v1, v7

    if-lez v1, :cond_0

    invoke-static {v7, v3}, Lcom/google/android/gm/provider/CompactSenderInstructions;->parseCompactSenderInstructions([BLcom/google/android/gm/provider/SenderInstructions;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSnippet:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^u"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->generateConversationInfo(ILjava/lang/String;Lcom/android/mail/providers/ConversationInfo;Lcom/google/android/gm/provider/SenderInstructions;ZZ)V

    invoke-virtual {v2}, Lcom/android/mail/providers/ConversationInfo;->toBlob()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    sget-object v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->CONVERSATION_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1, v2}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->SENDER_INSTRUCTIONS_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v1, v3}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    return-object v6

    :cond_0
    :try_start_1
    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mFromIndex:I

    invoke-virtual {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getStringInColumn(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/gm/provider/CompactSenderInstructions;->parseCompactSenderInstructions(Ljava/lang/String;Lcom/google/android/gm/provider/SenderInstructions;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    sget-object v4, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->CONVERSATION_INFO_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v4, v2}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    sget-object v4, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->SENDER_INSTRUCTIONS_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual {v4, v3}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    throw v1
.end method

.method private getConversationId()J
    .locals 2

    iget v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mConversationIdIndex:I

    invoke-super {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private getConversationPersonalLevel()I
    .locals 4

    iget v3, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mPersonalLevelIndex:I

    invoke-super {p0, v3}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gm/provider/Gmail$PersonalLevel;->fromInt(I)Lcom/google/android/gm/provider/Gmail$PersonalLevel;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gm/provider/Gmail$PersonalLevel;->NOT_TO_ME:Lcom/google/android/gm/provider/Gmail$PersonalLevel;

    if-ne v0, v3, :cond_1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/google/android/gm/provider/Gmail$PersonalLevel;->ONLY_TO_ME:Lcom/google/android/gm/provider/Gmail$PersonalLevel;

    if-ne v0, v3, :cond_2

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/google/android/gm/provider/Gmail$PersonalLevel;->TO_ME_AND_OTHERS:Lcom/google/android/gm/provider/Gmail$PersonalLevel;

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getRawLabels()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelsIndex:I

    invoke-super {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSnippet()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSnippetIndex:I

    invoke-virtual {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getStringInColumn(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSubject()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSubjectIndex:I

    invoke-virtual {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getStringInColumn(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadLabels(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gm/provider/Label;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getRawLabels()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCachedLabels:Ljava/util/Map;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gm/provider/LabelManager;->parseLabelQueryResult(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public getBlob(I)[B
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->cachePositionValues(Z)V

    sparse-switch p1, :sswitch_data_0

    const-string v1, "Gmail"

    const-string v2, "UIConversationCursor.getBlob(%d): Unexpected column"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getBlob(I)[B

    move-result-object v1

    :goto_0
    return-object v1

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->generateConversationInfo()[B

    move-result-object v1

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelToFolderMap:Lvedroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mLabelColorMap:Ljava/util/HashMap;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gm/provider/UiProvider;->getLabelsAsFolders(Ljava/lang/String;Ljava/util/Map;Lvedroid/support/v4/util/LongSparseArray;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/providers/FolderList;->listToBlob(Ljava/util/List;)[B

    move-result-object v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 8

    invoke-super {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string v6, "status"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "error"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->GMAIL_STATUS_UI_STATUS_MAP:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v6, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->GMAIL_ERROR_UI_ERROR_MAP:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const-string v6, "cursor_status"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v1, :cond_0

    const-string v6, "cursor_error"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-object v0
.end method

.method public getInt(I)I
    .locals 7
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->cachePositionValues(Z)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v4, "Gmail"

    const-string v5, "UIConversationCursor.getInt(%d): Unexpected column"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v4, v5, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v2

    :cond_0
    :goto_0
    return v2

    :pswitch_1
    iget v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mHasAttachmentsIndex:I

    invoke-super {p0, v2}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v2

    goto :goto_0

    :pswitch_2
    iget v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mNumMessagesIndex:I

    invoke-super {p0, v2}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v2

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^r"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v4, "^^out"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v4, "^f"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    const/4 v2, 0x3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0

    :pswitch_5
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^io_im"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^u"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    move v2, v3

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :pswitch_7
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^t"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_8
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^s"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_9
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^p"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_a
    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    const-string v5, "^g"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_b
    move v2, v3

    goto :goto_0

    :pswitch_c
    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getConversationPersonalLevel()I

    move-result v2

    goto/16 :goto_0

    :pswitch_d
    move v2, v3

    goto/16 :goto_0

    :pswitch_e
    iget v4, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSyncedIndex:I

    invoke-super {p0, v4}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public getLong(I)J
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mResultProjection:[Ljava/lang/String;

    aget-object v0, v1, p1

    const-string v1, "_id"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getConversationId()J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-string v1, "dateReceivedMs"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mDateIndex:I

    invoke-super {p0, v1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getLong(I)J

    move-result-wide v1

    goto :goto_0

    :cond_1
    const-string v1, "Gmail"

    const-string v2, "UIConversationCursor.getLong(%d): Unexpected column"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getLong(I)J

    move-result-wide v1

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->cachePositionValues(Z)V

    sparse-switch p1, :sswitch_data_0

    const-string v0, "Gmail"

    const-string v3, "UIConversationCursor.getString(%d): Unexpected column"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentConversationUri:Ljava/lang/String;

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getSubject()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mSnippet:Ljava/lang/String;

    goto :goto_1

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->getConversationId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/UiProvider;->getConversationMessageListUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :sswitch_4
    move-object v0, v3

    goto :goto_1

    :sswitch_5
    move-object v0, v3

    goto :goto_1

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mBaseUriString:Ljava/lang/String;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x15 -> :sswitch_5
        0x16 -> :sswitch_4
        0x17 -> :sswitch_6
    .end sparse-switch
.end method

.method protected resetCursorRowState()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentLabels:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mCurrentConversationUri:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/UIConversationCursor;->mRowDataCached:Z

    return-void
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10
    .param p1    # Landroid/os/Bundle;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v1, "allowNetwork"

    const-string v8, "allowNetwork"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "allowNetwork"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v9, "command"

    if-eqz v0, :cond_2

    const-string v8, "activate"

    :goto_0
    invoke-virtual {v2, v9, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, v2}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    const-string v8, "ok"

    const-string v9, "commandResponse"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const-string v9, "allowNetwork"

    if-eqz v6, :cond_3

    const-string v8, "ok"

    :goto_1
    invoke-virtual {v4, v9, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v5, "setVisibility"

    const-string v8, "setVisibility"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "setVisibility"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    const-string v8, "command"

    const-string v9, "setVisible"

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "visible"

    invoke-virtual {v2, v8, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, v2}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    const-string v8, "ok"

    const-string v9, "commandResponse"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const-string v9, "setVisibility"

    if-eqz v6, :cond_4

    const-string v8, "ok"

    :goto_2
    invoke-virtual {v4, v9, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v4

    :cond_2
    const-string v8, "deactivate"

    goto :goto_0

    :cond_3
    const-string v8, "failed"

    goto :goto_1

    :cond_4
    const-string v8, "failed"

    goto :goto_2
.end method
