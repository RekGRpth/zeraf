.class public Lcom/google/android/gm/provider/uiprovider/ConversationState;
.super Ljava/lang/Object;
.source "ConversationState.java"

# interfaces
.implements Lcom/google/android/gm/provider/AttachmentStatusListener$AttachmentStatusListenerCallback;


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

.field private final mContext:Landroid/content/Context;

.field private final mConversationId:J

.field private final mEngine:Lcom/google/android/gm/provider/MailEngine;

.field private final mLoaderHandler:Landroid/os/Handler;

.field private final mLoaderLock:Ljava/lang/Object;

.field private final mMessageAttachmentCursors:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/database/Cursor;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mMessageStateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/gm/provider/uiprovider/MessageState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JLcom/google/android/gm/provider/MailEngine;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Lcom/google/android/gm/provider/MailEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mLoaderLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAccount:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mConversationId:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mLoaderHandler:Landroid/os/Handler;

    iput-object p5, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gm/provider/uiprovider/ConversationState;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mLoaderLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gm/provider/uiprovider/ConversationState;)Lcom/google/android/gm/provider/AttachmentStatusListener;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/gm/provider/uiprovider/ConversationState;Lcom/google/android/gm/provider/AttachmentStatusListener;)Lcom/google/android/gm/provider/AttachmentStatusListener;
    .locals 0
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;
    .param p1    # Lcom/google/android/gm/provider/AttachmentStatusListener;

    iput-object p1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/gm/provider/uiprovider/ConversationState;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gm/provider/uiprovider/ConversationState;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gm/provider/uiprovider/ConversationState;)J
    .locals 2
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-wide v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mConversationId:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/google/android/gm/provider/uiprovider/ConversationState;)Lcom/google/android/gm/provider/MailEngine;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/uiprovider/ConversationState;

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    return-object v0
.end method

.method private createMessageStateFromMessage(Lcom/google/android/gm/provider/MailSync$Message;)Lcom/google/android/gm/provider/uiprovider/MessageState;
    .locals 10
    .param p1    # Lcom/google/android/gm/provider/MailSync$Message;

    iget-wide v5, p1, Lcom/google/android/gm/provider/MailSync$Message;->messageId:J

    iget-object v9, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    monitor-enter v9

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/gm/provider/uiprovider/MessageState;

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAccount:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mConversationId:J

    iget-wide v7, p1, Lcom/google/android/gm/provider/MailSync$Message;->localMessageId:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gm/provider/uiprovider/MessageState;-><init>(Landroid/content/Context;Ljava/lang/String;JJJ)V

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0, p1}, Lcom/google/android/gm/provider/uiprovider/MessageState;->initializeAttachmentsFromMessage(Lcom/google/android/gm/provider/MailSync$Message;)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/uiprovider/MessageState;

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public addAttachmentCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ensureAttachmentStatusLoaderStarted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mLoaderHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gm/provider/uiprovider/ConversationState$1;

    invoke-direct {v1, p0}, Lcom/google/android/gm/provider/uiprovider/ConversationState$1;-><init>(Lcom/google/android/gm/provider/uiprovider/ConversationState;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public getMessageState(J)Lcom/google/android/gm/provider/uiprovider/MessageState;
    .locals 4
    .param p1    # J

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/uiprovider/MessageState;

    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getOrCreateMessageState(J)Lcom/google/android/gm/provider/uiprovider/MessageState;
    .locals 5
    .param p1    # J

    const/4 v4, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gm/provider/uiprovider/ConversationState;->getMessageState(J)Lcom/google/android/gm/provider/uiprovider/MessageState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v2, p1, p2, v4}, Lcom/google/android/gm/provider/MailEngine;->getMessage(JZ)Lcom/google/android/gm/provider/MailSync$Message;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "Gmail"

    const-string v3, "Message not found"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/gm/provider/uiprovider/ConversationState;->createMessageStateFromMessage(Lcom/google/android/gm/provider/MailSync$Message;)Lcom/google/android/gm/provider/uiprovider/MessageState;

    move-result-object v1

    :cond_1
    :goto_1
    move-object v2, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gm/provider/uiprovider/MessageState;->gmailAttachmentDataLoaded()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/gm/provider/uiprovider/MessageState;->populateGmailAttachmentData(Lcom/google/android/gm/provider/MailSync$Message;)V

    goto :goto_1
.end method

.method public handleCursorClose(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageAttachmentCursors:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mLoaderLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

    if-eqz v1, :cond_1

    const-string v1, "Gmail"

    const-string v3, "attachment cursor closed, and stopping loader"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/AttachmentStatusListener;->stop()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mAttachmentStatusLoader:Lcom/google/android/gm/provider/AttachmentStatusListener;

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method notifyAttachmentChanges()V
    .locals 5

    iget-object v4, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gm/provider/uiprovider/MessageState;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/uiprovider/MessageState;->notifyAttachmentChange()V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onAttachmentsChanged()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/ConversationState;->mMessageStateMap:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/ConversationState;->notifyAttachmentChanges()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
