.class public Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;
.super Ljava/lang/Object;
.source "GmailAttachment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/uiprovider/GmailAttachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UriParser"
.end annotation


# instance fields
.field public final mAccount:Ljava/lang/String;

.field public final mContentTypeQueryParameters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mConversationId:J

.field public final mLocalMessageId:J

.field public final mPartId:Ljava/lang/String;

.field public final mServerMessageId:J


# direct methods
.method private constructor <init>(Ljava/lang/String;JJJLjava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J
    .param p6    # J
    .param p8    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJJ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mAccount:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mConversationId:J

    iput-wide p4, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mServerMessageId:J

    iput-wide p6, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mLocalMessageId:J

    iput-object p8, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mPartId:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;->mContentTypeQueryParameters:Ljava/util/List;

    return-void
.end method

.method public static parse(Landroid/net/Uri;)Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;
    .locals 12
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v0, "contentType"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    const/4 v0, 0x2

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v0, 0x3

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    const/4 v11, 0x5

    if-lt v0, v11, :cond_0

    const/4 v0, 0x4

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    :goto_0
    const-string v0, "serverMessageId"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    new-instance v0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment$UriParser;-><init>(Ljava/lang/String;JJJLjava/lang/String;Ljava/util/List;)V

    return-object v0

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method
