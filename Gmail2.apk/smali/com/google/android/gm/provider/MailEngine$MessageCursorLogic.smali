.class Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;
.super Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;
.source "MailEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/MailEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MessageCursorLogic"
.end annotation


# instance fields
.field mConfigured:Z

.field final mConversationId:J

.field final synthetic this$0:Lcom/google/android/gm/provider/MailEngine;


# direct methods
.method constructor <init>(Lcom/google/android/gm/provider/MailEngine;J)V
    .locals 1
    .param p2    # J

    iput-object p1, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    invoke-direct {p0, p1}, Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;-><init>(Lcom/google/android/gm/provider/MailEngine;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConfigured:Z

    iput-wide p2, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConversationId:J

    return-void
.end method


# virtual methods
.method public configure(JLcom/google/android/gm/provider/MailEngine$NetworkCursor;Z)V
    .locals 15
    .param p1    # J
    .param p3    # Lcom/google/android/gm/provider/MailEngine$NetworkCursor;
    .param p4    # Z

    iget-wide v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mQueryId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConfigured:Z

    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v10, v10, Lcom/google/android/gm/provider/MailEngine;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v11, "SELECT COUNT(*) FROM conversations WHERE _id = ?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-gtz v10, :cond_1

    const-string v10, "Gmail"

    const-string v11, "Didn\'t find conversation entry for this conversation"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p4, :cond_2

    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v10, v10, Lcom/google/android/gm/provider/MailEngine;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v11, "SELECT COUNT(*) FROM messages WHERE conversation = ? AND queryId = ?"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v6, v12, v13

    invoke-static {v10, v11, v12}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v10, 0x0

    cmp-long v10, v0, v10

    if-gtz v10, :cond_0

    :cond_2
    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v10, v10, Lcom/google/android/gm/provider/MailEngine;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v11, "SELECT COUNT(*) FROM messages WHERE conversation = ? AND queryId = 0"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v7

    const-wide/16 v10, 0x0

    cmp-long v10, v7, v10

    if-nez v10, :cond_0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "queryId NOT IN (0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mMessageCursorLogicMapLock:Ljava/lang/Object;
    invoke-static {v10}, Lcom/google/android/gm/provider/MailEngine;->access$5000(Lcom/google/android/gm/provider/MailEngine;)Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    :try_start_0
    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mMessageCursorLogicMap:Ljava/util/Map;
    invoke-static {v10}, Lcom/google/android/gm/provider/MailEngine;->access$5100(Lcom/google/android/gm/provider/MailEngine;)Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mMessageCursorLogicMap:Ljava/util/Map;
    invoke-static {v10}, Lcom/google/android/gm/provider/MailEngine;->access$5100(Lcom/google/android/gm/provider/MailEngine;)Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;

    iget-wide v12, v10, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mQueryId:J

    invoke-virtual {v9, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_1

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    :cond_3
    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v10, v10, Lcom/google/android/gm/provider/MailEngine;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v11, "messages"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gm/provider/MailEngine$NetworkCursor;->requery()Z

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->startThread()Z

    goto/16 :goto_0
.end method

.method public declared-synchronized respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    monitor-enter p0

    :try_start_0
    const-string v2, "command"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "setVisible"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "visible"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mMessageCursorLogicMapLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/gm/provider/MailEngine;->access$5000(Lcom/google/android/gm/provider/MailEngine;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mMessageCursorLogicMap:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/gm/provider/MailEngine;->access$5100(Lcom/google/android/gm/provider/MailEngine;)Ljava/util/Map;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConversationId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    const-string v2, "commandResponse"

    const-string v3, "ok"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_5
    invoke-super {p0, p1}, Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v1

    goto :goto_0
.end method

.method public runInternal()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/gm/provider/MailEngine$AuthenticationException;,
            Lcom/google/android/gm/provider/MailSync$ResponseParseException;
        }
    .end annotation

    const/high16 v12, 0x200000

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v1, Lcom/google/android/gm/provider/MailSync$ConversationInfo;

    iget-wide v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConversationId:J

    const-wide/16 v7, 0x0

    invoke-direct {v1, v5, v6, v7, v8}, Lcom/google/android/gm/provider/MailSync$ConversationInfo;-><init>(JJ)V

    const-string v5, "Gmail"

    const-string v6, "MessageCursor requesting conversation %d"

    new-array v7, v11, [Ljava/lang/Object;

    iget-wide v8, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mConversationId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {}, Lcom/google/android/gm/provider/Gmail;->isRunningICSOrLater()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # getter for: Lcom/google/android/gm/provider/MailEngine;->mAccount:Landroid/accounts/Account;
    invoke-static {v5}, Lcom/google/android/gm/provider/MailEngine;->access$1400(Lcom/google/android/gm/provider/MailEngine;)Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v0

    const/high16 v3, 0x200000

    or-int v5, v0, v3

    invoke-static {v5}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v5, v5, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    invoke-virtual {v5, v1}, Lcom/google/android/gm/provider/MailSync;->newFetchConversationRequest(Lcom/google/android/gm/provider/MailSync$ConversationInfo;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # invokes: Lcom/google/android/gm/provider/MailEngine;->runHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    invoke-static {v5, v2}, Lcom/google/android/gm/provider/MailEngine;->access$4700(Lcom/google/android/gm/provider/MailEngine;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    :try_start_1
    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    iget-object v5, v5, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    iget-wide v6, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->mQueryId:J

    invoke-virtual {v5, v4, v6, v7}, Lcom/google/android/gm/provider/MailSync;->handleFetchConversationResponse(Lorg/apache/http/HttpResponse;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # invokes: Lcom/google/android/gm/provider/MailEngine;->closeResponse(Lorg/apache/http/HttpResponse;)V
    invoke-static {v5, v4}, Lcom/google/android/gm/provider/MailEngine;->access$5200(Lcom/google/android/gm/provider/MailEngine;Lorg/apache/http/HttpResponse;)V

    const-string v5, "Gmail"

    const-string v6, "All messages received for conversation."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v5, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    const/4 v6, 0x0

    # invokes: Lcom/google/android/gm/provider/MailEngine;->notifyDatasetChanged(Z)V
    invoke-static {v5, v6}, Lcom/google/android/gm/provider/MailEngine;->access$4000(Lcom/google/android/gm/provider/MailEngine;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Lcom/google/android/gm/provider/Gmail;->isRunningICSOrLater()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v12, v11}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    :cond_1
    return-void

    :catchall_0
    move-exception v5

    :try_start_3
    iget-object v6, p0, Lcom/google/android/gm/provider/MailEngine$MessageCursorLogic;->this$0:Lcom/google/android/gm/provider/MailEngine;

    # invokes: Lcom/google/android/gm/provider/MailEngine;->closeResponse(Lorg/apache/http/HttpResponse;)V
    invoke-static {v6, v4}, Lcom/google/android/gm/provider/MailEngine;->access$5200(Lcom/google/android/gm/provider/MailEngine;Lorg/apache/http/HttpResponse;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v5

    invoke-static {}, Lcom/google/android/gm/provider/Gmail;->isRunningICSOrLater()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v12, v11}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    :cond_2
    throw v5
.end method
