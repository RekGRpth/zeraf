.class public Lcom/android/launcher2/SpringLoadedDragController;
.super Ljava/lang/Object;
.source "SpringLoadedDragController.java"

# interfaces
.implements Lcom/android/launcher2/OnAlarmListener;


# instance fields
.field final ENTER_SPRING_LOAD_CANCEL_HOVER_TIME:J

.field final ENTER_SPRING_LOAD_HOVER_TIME:J

.field final EXIT_SPRING_LOAD_HOVER_TIME:J

.field mAlarm:Lcom/android/launcher2/Alarm;

.field private mLauncher:Lcom/android/launcher2/Launcher;

.field private mScreen:Lcom/android/launcher2/CellLayout;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .locals 2
    .param p1    # Lcom/android/launcher2/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->ENTER_SPRING_LOAD_HOVER_TIME:J

    const-wide/16 v0, 0x3b6

    iput-wide v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->ENTER_SPRING_LOAD_CANCEL_HOVER_TIME:J

    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->EXIT_SPRING_LOAD_HOVER_TIME:J

    iput-object p1, p0, Lcom/android/launcher2/SpringLoadedDragController;->mLauncher:Lcom/android/launcher2/Launcher;

    new-instance v0, Lcom/android/launcher2/Alarm;

    invoke-direct {v0}, Lcom/android/launcher2/Alarm;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->mAlarm:Lcom/android/launcher2/Alarm;

    iget-object v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->mAlarm:Lcom/android/launcher2/Alarm;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Alarm;->setOnAlarmListener(Lcom/android/launcher2/OnAlarmListener;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->mAlarm:Lcom/android/launcher2/Alarm;

    invoke-virtual {v0}, Lcom/android/launcher2/Alarm;->cancelAlarm()V

    return-void
.end method

.method public onAlarm(Lcom/android/launcher2/Alarm;)V
    .locals 3
    .param p1    # Lcom/android/launcher2/Alarm;

    iget-object v2, p0, Lcom/android/launcher2/SpringLoadedDragController;->mScreen:Lcom/android/launcher2/CellLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/launcher2/SpringLoadedDragController;->mLauncher:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/SpringLoadedDragController;->mScreen:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v1}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v2

    if-eq v0, v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/launcher2/SpringLoadedDragController;->mLauncher:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getDragController()Lcom/android/launcher2/DragController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/DragController;->cancelDrag()V

    goto :goto_0
.end method

.method public setAlarm(Lcom/android/launcher2/CellLayout;)V
    .locals 3
    .param p1    # Lcom/android/launcher2/CellLayout;

    iget-object v0, p0, Lcom/android/launcher2/SpringLoadedDragController;->mAlarm:Lcom/android/launcher2/Alarm;

    invoke-virtual {v0}, Lcom/android/launcher2/Alarm;->cancelAlarm()V

    iget-object v2, p0, Lcom/android/launcher2/SpringLoadedDragController;->mAlarm:Lcom/android/launcher2/Alarm;

    if-nez p1, :cond_0

    const-wide/16 v0, 0x3b6

    :goto_0
    invoke-virtual {v2, v0, v1}, Lcom/android/launcher2/Alarm;->setAlarm(J)V

    iput-object p1, p0, Lcom/android/launcher2/SpringLoadedDragController;->mScreen:Lcom/android/launcher2/CellLayout;

    return-void

    :cond_0
    const-wide/16 v0, 0x1f4

    goto :goto_0
.end method
