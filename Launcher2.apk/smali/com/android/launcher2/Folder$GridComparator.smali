.class Lcom/android/launcher2/Folder$GridComparator;
.super Ljava/lang/Object;
.source "Folder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher2/Folder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GridComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/launcher2/ShortcutInfo;",
        ">;"
    }
.end annotation


# instance fields
.field mNumCols:I

.field final synthetic this$0:Lcom/android/launcher2/Folder;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Folder;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/android/launcher2/Folder$GridComparator;->this$0:Lcom/android/launcher2/Folder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/launcher2/Folder$GridComparator;->mNumCols:I

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/launcher2/ShortcutInfo;Lcom/android/launcher2/ShortcutInfo;)I
    .locals 4
    .param p1    # Lcom/android/launcher2/ShortcutInfo;
    .param p2    # Lcom/android/launcher2/ShortcutInfo;

    iget v2, p1, Lcom/android/launcher2/ItemInfo;->cellY:I

    iget v3, p0, Lcom/android/launcher2/Folder$GridComparator;->mNumCols:I

    mul-int/2addr v2, v3

    iget v3, p1, Lcom/android/launcher2/ItemInfo;->cellX:I

    add-int v0, v2, v3

    iget v2, p2, Lcom/android/launcher2/ItemInfo;->cellY:I

    iget v3, p0, Lcom/android/launcher2/Folder$GridComparator;->mNumCols:I

    mul-int/2addr v2, v3

    iget v3, p2, Lcom/android/launcher2/ItemInfo;->cellX:I

    add-int v1, v2, v3

    sub-int v2, v0, v1

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/launcher2/ShortcutInfo;

    check-cast p2, Lcom/android/launcher2/ShortcutInfo;

    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/Folder$GridComparator;->compare(Lcom/android/launcher2/ShortcutInfo;Lcom/android/launcher2/ShortcutInfo;)I

    move-result v0

    return v0
.end method
