.class public Lcom/android/launcher2/Cling;
.super Landroid/widget/FrameLayout;
.source "Cling.java"


# static fields
.field static final ALLAPPS_CLING_DISMISSED_KEY:Ljava/lang/String; = "cling.allapps.dismissed"

.field private static ALLAPPS_LANDSCAPE:Ljava/lang/String; = null

.field private static ALLAPPS_LARGE:Ljava/lang/String; = null

.field private static ALLAPPS_PORTRAIT:Ljava/lang/String; = null

.field static final FOLDER_CLING_DISMISSED_KEY:Ljava/lang/String; = "cling.folder.dismissed"

.field private static FOLDER_LANDSCAPE:Ljava/lang/String; = null

.field private static FOLDER_LARGE:Ljava/lang/String; = null

.field private static FOLDER_PORTRAIT:Ljava/lang/String; = null

.field static final WORKSPACE_CLING_DISMISSED_KEY:Ljava/lang/String; = "cling.workspace.dismissed"

.field private static WORKSPACE_CUSTOM:Ljava/lang/String;

.field private static WORKSPACE_LANDSCAPE:Ljava/lang/String;

.field private static WORKSPACE_LARGE:Ljava/lang/String;

.field private static WORKSPACE_PORTRAIT:Ljava/lang/String;


# instance fields
.field private mAppIconSize:I

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mButtonBarHeight:I

.field private mDrawIdentifier:Ljava/lang/String;

.field private mErasePaint:Landroid/graphics/Paint;

.field private mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

.field private mIsInitialized:Z

.field private mLauncher:Lcom/android/launcher2/Launcher;

.field private mPositionData:[I

.field private mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

.field private mPunchThroughGraphicCenterRadius:I

.field private mRevealRadius:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "workspace_portrait"

    sput-object v0, Lcom/android/launcher2/Cling;->WORKSPACE_PORTRAIT:Ljava/lang/String;

    const-string v0, "workspace_landscape"

    sput-object v0, Lcom/android/launcher2/Cling;->WORKSPACE_LANDSCAPE:Ljava/lang/String;

    const-string v0, "workspace_large"

    sput-object v0, Lcom/android/launcher2/Cling;->WORKSPACE_LARGE:Ljava/lang/String;

    const-string v0, "workspace_custom"

    sput-object v0, Lcom/android/launcher2/Cling;->WORKSPACE_CUSTOM:Ljava/lang/String;

    const-string v0, "all_apps_portrait"

    sput-object v0, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    const-string v0, "all_apps_landscape"

    sput-object v0, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    const-string v0, "all_apps_large"

    sput-object v0, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    const-string v0, "folder_portrait"

    sput-object v0, Lcom/android/launcher2/Cling;->FOLDER_PORTRAIT:Ljava/lang/String;

    const-string v0, "folder_landscape"

    sput-object v0, Lcom/android/launcher2/Cling;->FOLDER_LANDSCAPE:Ljava/lang/String;

    const-string v0, "folder_large"

    sput-object v0, Lcom/android/launcher2/Cling;->FOLDER_LARGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/launcher2/Cling;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/Cling;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/android/launcher/R$styleable;->Cling:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method private getPunchThroughPositions()[I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->WORKSPACE_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-array v3, v5, [I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    aput v4, v3, v6

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v5, p0, Lcom/android/launcher2/Cling;->mButtonBarHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    aput v4, v3, v7

    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->WORKSPACE_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-array v3, v5, [I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/android/launcher2/Cling;->mButtonBarHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    aput v4, v3, v6

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    aput v4, v3, v7

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->WORKSPACE_LARGE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getScreenDensity()F

    move-result v2

    const/high16 v3, 0x41700000

    mul-float/2addr v3, v2

    float-to-int v0, v3

    const/high16 v3, 0x41200000

    mul-float/2addr v3, v2

    float-to-int v1, v3

    new-array v3, v5, [I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v4, v0

    aput v4, v3, v6

    aput v1, v3, v7

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v4, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/android/launcher2/Cling;->mPositionData:[I

    goto :goto_0

    :cond_4
    new-array v3, v5, [I

    fill-array-data v3, :array_0

    goto :goto_0

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method


# virtual methods
.method cleanup()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Cling;->mIsInitialized:Z

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/launcher2/Cling;->mIsInitialized:Z

    if-eqz v12, :cond_e

    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mLauncher:Lcom/android/launcher2/Launcher;

    invoke-virtual {v12}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v12

    invoke-interface {v12}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    if-nez v12, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->WORKSPACE_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->WORKSPACE_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->WORKSPACE_LARGE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020004

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_1
    const/4 v3, -0x1

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/launcher2/Cling;->mRevealRadius:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/launcher2/Cling;->mPunchThroughGraphicCenterRadius:I

    int-to-float v13, v13

    div-float v11, v12, v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v11

    float-to-int v6, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v11

    float-to-int v5, v12

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Cling;->getPunchThroughPositions()[I

    move-result-object v10

    const/4 v7, 0x0

    :goto_2
    array-length v12, v10

    if-ge v7, v12, :cond_a

    aget v3, v10, v7

    add-int/lit8 v12, v7, 0x1

    aget v4, v10, v12

    const/4 v12, -0x1

    if-le v3, v12, :cond_2

    const/4 v12, -0x1

    if-le v4, v12, :cond_2

    int-to-float v12, v3

    int-to-float v13, v4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/launcher2/Cling;->mRevealRadius:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/Cling;->mErasePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    div-int/lit8 v13, v6, 0x2

    sub-int v13, v3, v13

    div-int/lit8 v14, v5, 0x2

    sub-int v14, v4, v14

    div-int/lit8 v15, v6, 0x2

    add-int/2addr v15, v3

    div-int/lit8 v16, v5, 0x2

    add-int v16, v16, v4

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    add-int/lit8 v7, v7, 0x2

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020005

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->FOLDER_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_6

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->FOLDER_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    :cond_6
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020006

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->FOLDER_LARGE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020007

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->WORKSPACE_CUSTOM:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020008

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mBackground:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_9
    const/high16 v12, -0x67000000

    invoke-virtual {v2, v12}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v13, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    :cond_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    if-nez v12, :cond_c

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020016

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    :cond_c
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/launcher2/Cling;->mAppIconSize:I

    div-int/lit8 v9, v12, 0x4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    add-int v13, v3, v9

    add-int v14, v4, v9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v15

    add-int/2addr v15, v3

    add-int/2addr v15, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v16

    add-int v16, v16, v4

    add-int v16, v16, v9

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Cling;->mHandTouchGraphic:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_d
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_e
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public focusSearch(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p0, p1}, Lcom/android/launcher2/Cling;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getDrawIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method init(Lcom/android/launcher2/Launcher;[I)V
    .locals 4
    .param p1    # Lcom/android/launcher2/Launcher;
    .param p2    # [I

    iget-boolean v1, p0, Lcom/android/launcher2/Cling;->mIsInitialized:Z

    if-nez v1, :cond_0

    iput-object p1, p0, Lcom/android/launcher2/Cling;->mLauncher:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/Cling;->mPositionData:[I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/Cling;->mPunchThroughGraphic:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Cling;->mPunchThroughGraphicCenterRadius:I

    const v1, 0x7f0b0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Cling;->mAppIconSize:I

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/android/launcher2/Cling;->mRevealRadius:F

    const v1, 0x7f0b0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Cling;->mButtonBarHeight:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/launcher2/Cling;->mErasePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/launcher2/Cling;->mErasePaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v1, p0, Lcom/android/launcher2/Cling;->mErasePaint:Landroid/graphics/Paint;

    const v2, 0xffffff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/launcher2/Cling;->mErasePaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher2/Cling;->mIsInitialized:Z

    :cond_0
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->WORKSPACE_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->WORKSPACE_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->WORKSPACE_LARGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v1, Lcom/android/launcher2/Cling;->WORKSPACE_CUSTOM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    const-wide/high16 v11, 0x4000000000000000L

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->WORKSPACE_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->WORKSPACE_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->WORKSPACE_LARGE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->ALLAPPS_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->ALLAPPS_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->ALLAPPS_LARGE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/android/launcher2/Cling;->getPunchThroughPositions()[I

    move-result-object v4

    const/4 v3, 0x0

    :goto_0
    array-length v7, v4

    if-ge v3, v7, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    aget v8, v4, v3

    int-to-float v8, v8

    sub-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    add-int/lit8 v10, v3, 0x1

    aget v10, v4, v10

    int-to-float v10, v10

    sub-float/2addr v9, v10

    float-to-double v9, v9

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    add-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget v7, p0, Lcom/android/launcher2/Cling;->mRevealRadius:F

    float-to-double v7, v7

    cmpg-double v7, v0, v7

    if-gez v7, :cond_2

    :cond_1
    :goto_1
    return v6

    :cond_2
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->FOLDER_PORTRAIT:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->FOLDER_LANDSCAPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/android/launcher2/Cling;->mDrawIdentifier:Ljava/lang/String;

    sget-object v8, Lcom/android/launcher2/Cling;->FOLDER_LARGE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    iget-object v7, p0, Lcom/android/launcher2/Cling;->mLauncher:Lcom/android/launcher2/Launcher;

    invoke-virtual {v7}, Lcom/android/launcher2/Launcher;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v2

    if-eqz v2, :cond_5

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v5, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_5
    const/4 v6, 0x1

    goto :goto_1
.end method
