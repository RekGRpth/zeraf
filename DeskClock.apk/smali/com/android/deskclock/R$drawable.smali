.class public final Lcom/android/deskclock/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_hairline:I = 0x7f020000

.field public static final activated_background_holo_dark:I = 0x7f020001

.field public static final add_alarm:I = 0x7f020002

.field public static final alarm_alert_fullscreen_bg:I = 0x7f020003

.field public static final appwidget_analog_clock_preview:I = 0x7f020004

.field public static final appwidget_clock_dial:I = 0x7f020005

.field public static final appwidget_clock_hour:I = 0x7f020006

.field public static final appwidget_clock_minute:I = 0x7f020007

.field public static final appwidget_digital_clock_preview:I = 0x7f020008

.field public static final blackish_transition:I = 0x7f020009

.field public static final btn_alarm_alert:I = 0x7f02000a

.field public static final btn_alarm_focused:I = 0x7f02000b

.field public static final btn_alarm_normal:I = 0x7f02000c

.field public static final btn_alarm_pressed:I = 0x7f02000d

.field public static final btn_in_call_round_disable:I = 0x7f02000e

.field public static final btn_in_call_round_disable_focused:I = 0x7f02000f

.field public static final btn_in_call_round_normal:I = 0x7f020010

.field public static final btn_in_call_round_pressed:I = 0x7f020011

.field public static final btn_in_call_round_selected:I = 0x7f020012

.field public static final btn_strip_trans_left:I = 0x7f020013

.field public static final btn_strip_trans_left_normal:I = 0x7f020014

.field public static final btn_strip_trans_left_pressed:I = 0x7f020015

.field public static final btn_strip_trans_left_selected:I = 0x7f020016

.field public static final btn_strip_trans_middle:I = 0x7f020017

.field public static final btn_strip_trans_middle_normal:I = 0x7f020018

.field public static final btn_strip_trans_middle_pressed:I = 0x7f020019

.field public static final btn_strip_trans_middle_selected:I = 0x7f02001a

.field public static final btn_strip_trans_right:I = 0x7f02001b

.field public static final btn_strip_trans_right_normal:I = 0x7f02001c

.field public static final btn_strip_trans_right_pressed:I = 0x7f02001d

.field public static final btn_strip_trans_right_selected:I = 0x7f02001e

.field public static final clock_analog_dial:I = 0x7f02001f

.field public static final clock_analog_hour:I = 0x7f020020

.field public static final clock_analog_minute:I = 0x7f020021

.field public static final clock_analog_second:I = 0x7f020022

.field public static final clock_selector:I = 0x7f020023

.field public static final clock_tab:I = 0x7f020024

.field public static final dialog:I = 0x7f020025

.field public static final dialog_divider_horizontal_light:I = 0x7f020026

.field public static final footer_bg:I = 0x7f020027

.field public static final header_bg:I = 0x7f020028

.field public static final header_bg_bar:I = 0x7f020029

.field public static final ic_add:I = 0x7f02002a

.field public static final ic_alarm:I = 0x7f02002b

.field public static final ic_alarm_alert_dismiss:I = 0x7f02002c

.field public static final ic_alarm_alert_outerring:I = 0x7f02002d

.field public static final ic_alarm_alert_snooze:I = 0x7f02002e

.field public static final ic_alarm_alert_touch_handle:I = 0x7f02002f

.field public static final ic_alarm_small:I = 0x7f020030

.field public static final ic_backspace:I = 0x7f020031

.field public static final ic_backspace_disabled:I = 0x7f020032

.field public static final ic_backspace_normal:I = 0x7f020033

.field public static final ic_clock_add_alarm:I = 0x7f020034

.field public static final ic_clock_add_alarm_selected:I = 0x7f020035

.field public static final ic_clock_alarm_on:I = 0x7f020036

.field public static final ic_clock_alarm_selected:I = 0x7f020037

.field public static final ic_clock_strip_music:I = 0x7f020038

.field public static final ic_delete:I = 0x7f020039

.field public static final ic_delete_normal:I = 0x7f02003a

.field public static final ic_delete_pressed:I = 0x7f02003b

.field public static final ic_dialog_time:I = 0x7f02003c

.field public static final ic_discard_holo_dark:I = 0x7f02003d

.field public static final ic_expand_down:I = 0x7f02003e

.field public static final ic_expand_up:I = 0x7f02003f

.field public static final ic_indicator_off:I = 0x7f020040

.field public static final ic_indicator_on:I = 0x7f020041

.field public static final ic_label:I = 0x7f020042

.field public static final ic_label_normal:I = 0x7f020043

.field public static final ic_label_pressed:I = 0x7f020044

.field public static final ic_lap:I = 0x7f020045

.field public static final ic_lap_normal:I = 0x7f020046

.field public static final ic_lap_pressed:I = 0x7f020047

.field public static final ic_location:I = 0x7f020048

.field public static final ic_lockscreen_alarm:I = 0x7f020049

.field public static final ic_lockscreen_glowdot:I = 0x7f02004a

.field public static final ic_lockscreen_handle_pressed:I = 0x7f02004b

.field public static final ic_lockscreen_snooze_activated:I = 0x7f02004c

.field public static final ic_lockscreen_snooze_normal:I = 0x7f02004d

.field public static final ic_lockscreen_wakeup_activated:I = 0x7f02004e

.field public static final ic_lockscreen_wakeup_normal:I = 0x7f02004f

.field public static final ic_menu_add:I = 0x7f020050

.field public static final ic_menu_desk_clock:I = 0x7f020051

.field public static final ic_menu_done_holo_dark:I = 0x7f020052

.field public static final ic_menu_revert_holo_dark:I = 0x7f020053

.field public static final ic_notify_lap:I = 0x7f020054

.field public static final ic_notify_reset:I = 0x7f020055

.field public static final ic_notify_start:I = 0x7f020056

.field public static final ic_notify_stop:I = 0x7f020057

.field public static final ic_plusone:I = 0x7f020058

.field public static final ic_plusone_normal:I = 0x7f020059

.field public static final ic_plusone_pressed:I = 0x7f02005a

.field public static final ic_reset:I = 0x7f02005b

.field public static final ic_reset_normal:I = 0x7f02005c

.field public static final ic_reset_pressed:I = 0x7f02005d

.field public static final ic_ringtone:I = 0x7f02005e

.field public static final ic_share:I = 0x7f02005f

.field public static final ic_share_normal:I = 0x7f020060

.field public static final ic_share_pressed:I = 0x7f020061

.field public static final ic_tab_clock:I = 0x7f020062

.field public static final ic_tab_clock_activated:I = 0x7f020063

.field public static final ic_tab_clock_normal:I = 0x7f020064

.field public static final ic_tab_stopwatch:I = 0x7f020065

.field public static final ic_tab_stopwatch_activated:I = 0x7f020066

.field public static final ic_tab_stopwatch_normal:I = 0x7f020067

.field public static final ic_tab_timer:I = 0x7f020068

.field public static final ic_tab_timer_activated:I = 0x7f020069

.field public static final ic_tab_timer_normal:I = 0x7f02006a

.field public static final incall_round_button:I = 0x7f02006b

.field public static final indicator_bar_onoff:I = 0x7f02006c

.field public static final indicator_clock_onoff:I = 0x7f02006d

.field public static final item_background:I = 0x7f02006e

.field public static final list_activated_holo:I = 0x7f02006f

.field public static final list_selector_background_pressed:I = 0x7f020070

.field public static final notification_bg:I = 0x7f020071

.field public static final notification_bg_normal:I = 0x7f020072

.field public static final notification_bg_normal_pressed:I = 0x7f020073

.field public static final notification_template_icon_bg:I = 0x7f02007d

.field public static final notification_template_icon_low_bg:I = 0x7f02007e

.field public static final panel_undo_holo:I = 0x7f020074

.field public static final stat_notify_alarm:I = 0x7f020075

.field public static final stat_notify_stopwatch:I = 0x7f020076

.field public static final stat_notify_timer:I = 0x7f020077

.field public static final stopwatch_tab:I = 0x7f020078

.field public static final timer_tab:I = 0x7f020079

.field public static final toggle_underline:I = 0x7f02007a

.field public static final toggle_underline_activated:I = 0x7f02007b

.field public static final toggle_underline_normal:I = 0x7f02007c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
