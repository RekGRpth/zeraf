.class public final Lcom/android/deskclock/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_button:I = 0x7f0e0003

.field public static final action_icon:I = 0x7f0e0005

.field public static final action_text:I = 0x7f0e0006

.field public static final alarm_clock_left:I = 0x7f0e000f

.field public static final alarm_delete:I = 0x7f0e005a

.field public static final alarm_item:I = 0x7f0e0019

.field public static final alarm_revert:I = 0x7f0e0059

.field public static final alarm_save:I = 0x7f0e005b

.field public static final alarms_button:I = 0x7f0e0033

.field public static final alarms_list:I = 0x7f0e0016

.field public static final alertTitle:I = 0x7f0e0007

.field public static final am_pm:I = 0x7f0e000b

.field public static final ampm_label:I = 0x7f0e007b

.field public static final analog_appwidget:I = 0x7f0e0027

.field public static final analog_clock:I = 0x7f0e003a

.field public static final cancelButton:I = 0x7f0e004c

.field public static final cancel_button:I = 0x7f0e0073

.field public static final choose_ringtone:I = 0x7f0e0024

.field public static final cities:I = 0x7f0e0030

.field public static final cities_button:I = 0x7f0e0034

.field public static final cities_list:I = 0x7f0e0028

.field public static final city_day:I = 0x7f0e0099

.field public static final city_day_left:I = 0x7f0e00a0

.field public static final city_day_right:I = 0x7f0e00a4

.field public static final city_left:I = 0x7f0e009a

.field public static final city_name:I = 0x7f0e002a

.field public static final city_name_layout:I = 0x7f0e0098

.field public static final city_name_left:I = 0x7f0e009f

.field public static final city_name_right:I = 0x7f0e00a3

.field public static final city_onoff:I = 0x7f0e002c

.field public static final city_right:I = 0x7f0e009b

.field public static final city_time:I = 0x7f0e002b

.field public static final clock_background:I = 0x7f0e002d

.field public static final clock_buttons:I = 0x7f0e0032

.field public static final clock_footer:I = 0x7f0e0031

.field public static final clock_foreground:I = 0x7f0e002e

.field public static final clock_time:I = 0x7f0e003f

.field public static final collapse:I = 0x7f0e0026

.field public static final date:I = 0x7f0e003b

.field public static final daysOfWeek:I = 0x7f0e001e

.field public static final delete:I = 0x7f0e007c

.field public static final delete_alarm:I = 0x7f0e00ac

.field public static final description_icon:I = 0x7f0e0001

.field public static final description_text:I = 0x7f0e0002

.field public static final desk_clock:I = 0x7f0e002f

.field public static final desk_clock_pager:I = 0x7f0e0038

.field public static final digitalClock:I = 0x7f0e0008

.field public static final digital_appwidget:I = 0x7f0e0047

.field public static final digital_appwidget_listview:I = 0x7f0e0048

.field public static final digital_clock:I = 0x7f0e001a

.field public static final divider:I = 0x7f0e0012

.field public static final edit_alarm:I = 0x7f0e00ab

.field public static final edit_label:I = 0x7f0e0021

.field public static final enable_alarm:I = 0x7f0e00aa

.field public static final everyday:I = 0x7f0e0053

.field public static final expand:I = 0x7f0e001f

.field public static final expand_area:I = 0x7f0e0020

.field public static final first:I = 0x7f0e007d

.field public static final fourth:I = 0x7f0e0080

.field public static final fragment_container:I = 0x7f0e0085

.field public static final glow_pad_view:I = 0x7f0e000e

.field public static final hairline:I = 0x7f0e001c

.field public static final header:I = 0x7f0e0029

.field public static final header_label:I = 0x7f0e0037

.field public static final header_time:I = 0x7f0e0036

.field public static final hours_label:I = 0x7f0e0081

.field public static final hours_ones:I = 0x7f0e0077

.field public static final hours_seperator:I = 0x7f0e0078

.field public static final hours_tens:I = 0x7f0e0076

.field public static final icon:I = 0x7f0e0057

.field public static final indicator_icon:I = 0x7f0e000c

.field public static final indicator_text:I = 0x7f0e000d

.field public static final info_area:I = 0x7f0e001d

.field public static final key_left:I = 0x7f0e006f

.field public static final key_middle:I = 0x7f0e0070

.field public static final key_right:I = 0x7f0e0071

.field public static final label:I = 0x7f0e0018

.field public static final labelBox:I = 0x7f0e004b

.field public static final lap_number:I = 0x7f0e004e

.field public static final lap_time:I = 0x7f0e004f

.field public static final lap_total:I = 0x7f0e0050

.field public static final laps_list:I = 0x7f0e0064

.field public static final leftClock1:I = 0x7f0e009d

.field public static final leftClock2:I = 0x7f0e009e

.field public static final line1:I = 0x7f0e0067

.field public static final list_item:I = 0x7f0e0056

.field public static final main:I = 0x7f0e0015

.field public static final main_clock:I = 0x7f0e0039

.field public static final main_clock_frame:I = 0x7f0e0051

.field public static final menu_button:I = 0x7f0e0035

.field public static final menu_delete:I = 0x7f0e00ae

.field public static final menu_item_add_alarm:I = 0x7f0e00a6

.field public static final menu_item_delete_alarm:I = 0x7f0e00a5

.field public static final menu_item_desk_clock:I = 0x7f0e00a9

.field public static final menu_item_help:I = 0x7f0e00a8

.field public static final menu_item_night_mode:I = 0x7f0e00ad

.field public static final menu_item_settings:I = 0x7f0e00a7

.field public static final minutes_label:I = 0x7f0e0082

.field public static final minutes_ones:I = 0x7f0e007a

.field public static final minutes_picker:I = 0x7f0e005d

.field public static final minutes_tens:I = 0x7f0e0079

.field public static final new_timer_page:I = 0x7f0e008a

.field public static final nextAlarm:I = 0x7f0e003c

.field public static final noRepeats:I = 0x7f0e0054

.field public static final notification_icon:I = 0x7f0e0066

.field public static final numbers_key:I = 0x7f0e0000

.field public static final onoff:I = 0x7f0e001b

.field public static final power_off:I = 0x7f0e0014

.field public static final power_on:I = 0x7f0e0013

.field public static final repeatList:I = 0x7f0e0055

.field public static final repeat_days:I = 0x7f0e0023

.field public static final repeat_onoff:I = 0x7f0e0022

.field public static final rightClock1:I = 0x7f0e00a1

.field public static final rightClock2:I = 0x7f0e00a2

.field public static final save_menu_item:I = 0x7f0e005c

.field public static final second:I = 0x7f0e007e

.field public static final seconds:I = 0x7f0e0083

.field public static final seconds_label:I = 0x7f0e0084

.field public static final separator:I = 0x7f0e0004

.field public static final setButton:I = 0x7f0e004d

.field public static final set_button:I = 0x7f0e0074

.field public static final snooze:I = 0x7f0e0011

.field public static final stopwatch_circle:I = 0x7f0e005e

.field public static final stopwatch_left_button:I = 0x7f0e0061

.field public static final stopwatch_share_button:I = 0x7f0e0063

.field public static final stopwatch_stop:I = 0x7f0e0062

.field public static final stopwatch_time:I = 0x7f0e0060

.field public static final stopwatch_time_text:I = 0x7f0e005f

.field public static final swn_collapsed_chronometer:I = 0x7f0e0068

.field public static final swn_collapsed_hitspace:I = 0x7f0e0065

.field public static final swn_collapsed_laps:I = 0x7f0e0069

.field public static final swn_expanded_chronometer:I = 0x7f0e006b

.field public static final swn_expanded_hitspace:I = 0x7f0e006a

.field public static final swn_expanded_laps:I = 0x7f0e006c

.field public static final swn_left_button:I = 0x7f0e006d

.field public static final swn_right_button:I = 0x7f0e006e

.field public static final the_clock1:I = 0x7f0e0049

.field public static final the_clock2:I = 0x7f0e004a

.field public static final third:I = 0x7f0e007f

.field public static final time:I = 0x7f0e003e

.field public static final timeDisplay:I = 0x7f0e0010

.field public static final timeDisplayHours:I = 0x7f0e0009

.field public static final timeDisplayMinutes:I = 0x7f0e000a

.field public static final time_date:I = 0x7f0e003d

.field public static final time_picker:I = 0x7f0e0072

.field public static final timer_add_timer:I = 0x7f0e0089

.field public static final timer_button_sep:I = 0x7f0e008d

.field public static final timer_cancel:I = 0x7f0e008c

.field public static final timer_circle:I = 0x7f0e008f

.field public static final timer_delete:I = 0x7f0e0097

.field public static final timer_footer:I = 0x7f0e0088

.field public static final timer_frame:I = 0x7f0e0090

.field public static final timer_label:I = 0x7f0e0092

.field public static final timer_label_icon:I = 0x7f0e0094

.field public static final timer_label_text:I = 0x7f0e0093

.field public static final timer_plus_one:I = 0x7f0e0095

.field public static final timer_setup:I = 0x7f0e008b

.field public static final timer_start:I = 0x7f0e008e

.field public static final timer_stop:I = 0x7f0e0096

.field public static final timer_time:I = 0x7f0e0091

.field public static final timer_time_text:I = 0x7f0e0075

.field public static final timers_list:I = 0x7f0e0087

.field public static final timers_list_page:I = 0x7f0e0086

.field public static final title:I = 0x7f0e0058

.field public static final undo_bar:I = 0x7f0e0017

.field public static final vibrate_onoff:I = 0x7f0e0025

.field public static final weather:I = 0x7f0e0040

.field public static final weather_high_temperature:I = 0x7f0e0045

.field public static final weather_icon:I = 0x7f0e0042

.field public static final weather_location:I = 0x7f0e0046

.field public static final weather_low_temperature:I = 0x7f0e0044

.field public static final weather_temp_icon_cluster:I = 0x7f0e0041

.field public static final weather_temperature:I = 0x7f0e0043

.field public static final weekdays:I = 0x7f0e0052

.field public static final widget_item:I = 0x7f0e009c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
