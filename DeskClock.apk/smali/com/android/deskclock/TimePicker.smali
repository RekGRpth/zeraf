.class public Lcom/android/deskclock/TimePicker;
.super Lcom/android/deskclock/TimerSetupView;
.source "TimePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/TimePicker$1;,
        Lcom/android/deskclock/TimePicker$SavedState;
    }
.end annotation


# static fields
.field private static final AMPM_NOT_SELECTED:I = 0x0

.field private static final AM_SELECTED:I = 0x2

.field private static final HOURS24_MODE:I = 0x3

.field private static final PM_SELECTED:I = 0x1

.field private static final TIME_PICKER_SAVED_AMPM:Ljava/lang/String; = "timer_picker_saved_ampm"

.field private static final TIME_PICKER_SAVED_BUFFER_POINTER:Ljava/lang/String; = "timer_picker_saved_buffer_pointer"

.field private static final TIME_PICKER_SAVED_INPUT:Ljava/lang/String; = "timer_picker_saved_input"


# instance fields
.field private mAmPmLabel:Landroid/widget/TextView;

.field private mAmPmState:I

.field private mAmpm:[Ljava/lang/String;

.field private final mIs24HoursMode:Z

.field private final mNoAmPmLabel:Ljava/lang/String;

.field private mSetButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/TimerSetupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/deskclock/Alarms;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/TimePicker;->mNoAmPmLabel:Ljava/lang/String;

    return-void
.end method

.method private addClickedNumber(I)V
    .locals 4
    .param p1    # I

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v3, v3, v0

    aput v3, v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x0

    aput p1, v1, v2

    :cond_1
    return-void
.end method

.method private canAddDigits()Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v0

    iget-boolean v3, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v3, :cond_2

    if-lt v0, v1, :cond_1

    const/16 v3, 0xc

    if-gt v0, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    if-ltz v0, :cond_3

    const/16 v3, 0x17

    if-gt v0, v3, :cond_3

    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_3

    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private enableSetButton()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/deskclock/TimePicker;->mSetButton:Landroid/widget/Button;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    iget-object v1, p0, Lcom/android/deskclock/TimePicker;->mSetButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    iget-boolean v3, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v0

    iget-object v3, p0, Lcom/android/deskclock/TimePicker;->mSetButton:Landroid/widget/Button;

    iget v4, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v5, 0x2

    if-lt v4, v5, :cond_3

    const/16 v4, 0x3c

    if-lt v0, v4, :cond_2

    const/16 v4, 0x5f

    if-le v0, v4, :cond_3

    :cond_2
    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/android/deskclock/TimePicker;->mSetButton:Landroid/widget/Button;

    iget v4, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    if-eqz v4, :cond_5

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method private getEnteredTime()I
    .locals 3

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    mul-int/lit16 v0, v0, 0x3e8

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x64

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private onLeftClicked()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->canAddDigits()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    :cond_0
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->canAddDigits()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    goto :goto_0
.end method

.method private onRightClicked()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->canAddDigits()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->canAddDigits()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    goto :goto_0
.end method

.method private setKeyRange(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    aget-object v2, v1, v0

    if-gt v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method private showAmPm()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/deskclock/TimePicker;->mNoAmPmLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/deskclock/TimePicker;->mAmpm:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/deskclock/TimePicker;->mAmpm:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmLabel:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private updateKeypad()V
    .locals 0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->showAmPm()V

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateLeftRightButtons()V

    invoke-virtual {p0}, Lcom/android/deskclock/TimePicker;->updateTime()V

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateNumericKeys()V

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->enableSetButton()V

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateDeleteButton()V

    return-void
.end method

.method private updateLeftRightButtons()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v1

    iget-boolean v2, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->canAddDigits()Z

    move-result v0

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0xc

    if-le v1, v2, :cond_1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_2

    :cond_1
    if-eqz v1, :cond_2

    iget v2, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateNumericKeys()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, -0x1

    const/16 v3, 0x9

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v1, :cond_39

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_5

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-eq v1, v4, :cond_2

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v1, v7, :cond_3

    :cond_2
    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v1, v6, :cond_4

    invoke-direct {p0, v5}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_5
    if-ne v0, v6, :cond_9

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v1, v7, :cond_7

    :cond_6
    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_7
    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v1, v6, :cond_8

    invoke-direct {p0, v5}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_8
    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_9
    if-ne v0, v7, :cond_d

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-eq v1, v7, :cond_a

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v1, v6, :cond_b

    :cond_a
    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_b
    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-nez v1, :cond_c

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_c
    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_d
    if-gt v0, v5, :cond_e

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_e
    if-gt v0, v3, :cond_f

    invoke-direct {p0, v5}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_f
    const/16 v1, 0xa

    if-lt v0, v1, :cond_10

    const/16 v1, 0xf

    if-gt v0, v1, :cond_10

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_10
    const/16 v1, 0x10

    if-lt v0, v1, :cond_11

    const/16 v1, 0x13

    if-gt v0, v1, :cond_11

    invoke-direct {p0, v5}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto :goto_0

    :cond_11
    const/16 v1, 0x14

    if-lt v0, v1, :cond_12

    const/16 v1, 0x19

    if-gt v0, v1, :cond_12

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_12
    const/16 v1, 0x1a

    if-lt v0, v1, :cond_13

    const/16 v1, 0x1d

    if-gt v0, v1, :cond_13

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_13
    const/16 v1, 0x1e

    if-lt v0, v1, :cond_14

    const/16 v1, 0x23

    if-gt v0, v1, :cond_14

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_14
    const/16 v1, 0x24

    if-lt v0, v1, :cond_15

    const/16 v1, 0x27

    if-gt v0, v1, :cond_15

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_15
    const/16 v1, 0x28

    if-lt v0, v1, :cond_16

    const/16 v1, 0x2d

    if-gt v0, v1, :cond_16

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_16
    const/16 v1, 0x2e

    if-lt v0, v1, :cond_17

    const/16 v1, 0x31

    if-gt v0, v1, :cond_17

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_17
    const/16 v1, 0x32

    if-lt v0, v1, :cond_18

    const/16 v1, 0x37

    if-gt v0, v1, :cond_18

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_18
    const/16 v1, 0x38

    if-lt v0, v1, :cond_19

    const/16 v1, 0x3b

    if-gt v0, v1, :cond_19

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_19
    const/16 v1, 0x3c

    if-lt v0, v1, :cond_1a

    const/16 v1, 0x41

    if-gt v0, v1, :cond_1a

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1a
    const/16 v1, 0x46

    if-lt v0, v1, :cond_1b

    const/16 v1, 0x4b

    if-gt v0, v1, :cond_1b

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1b
    const/16 v1, 0x50

    if-lt v0, v1, :cond_1c

    const/16 v1, 0x55

    if-gt v0, v1, :cond_1c

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1c
    const/16 v1, 0x5a

    if-lt v0, v1, :cond_1d

    const/16 v1, 0x5f

    if-gt v0, v1, :cond_1d

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1d
    const/16 v1, 0x64

    if-lt v0, v1, :cond_1e

    const/16 v1, 0x69

    if-gt v0, v1, :cond_1e

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1e
    const/16 v1, 0x6a

    if-lt v0, v1, :cond_1f

    const/16 v1, 0x6d

    if-gt v0, v1, :cond_1f

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_1f
    const/16 v1, 0x6e

    if-lt v0, v1, :cond_20

    const/16 v1, 0x73

    if-gt v0, v1, :cond_20

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_20
    const/16 v1, 0x74

    if-lt v0, v1, :cond_21

    const/16 v1, 0x77

    if-gt v0, v1, :cond_21

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_21
    const/16 v1, 0x78

    if-lt v0, v1, :cond_22

    const/16 v1, 0x7d

    if-gt v0, v1, :cond_22

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_22
    const/16 v1, 0x7e

    if-lt v0, v1, :cond_23

    const/16 v1, 0x81

    if-gt v0, v1, :cond_23

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_23
    const/16 v1, 0x82

    if-lt v0, v1, :cond_24

    const/16 v1, 0x87

    if-gt v0, v1, :cond_24

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_24
    const/16 v1, 0x88

    if-lt v0, v1, :cond_25

    const/16 v1, 0x8b

    if-gt v0, v1, :cond_25

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_25
    const/16 v1, 0x8c

    if-lt v0, v1, :cond_26

    const/16 v1, 0x91

    if-gt v0, v1, :cond_26

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_26
    const/16 v1, 0x92

    if-lt v0, v1, :cond_27

    const/16 v1, 0x95

    if-gt v0, v1, :cond_27

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_27
    const/16 v1, 0x96

    if-lt v0, v1, :cond_28

    const/16 v1, 0x9b

    if-gt v0, v1, :cond_28

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_28
    const/16 v1, 0x9c

    if-lt v0, v1, :cond_29

    const/16 v1, 0x9f

    if-gt v0, v1, :cond_29

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_29
    const/16 v1, 0xa0

    if-lt v0, v1, :cond_2a

    const/16 v1, 0xa5

    if-gt v0, v1, :cond_2a

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2a
    const/16 v1, 0xa6

    if-lt v0, v1, :cond_2b

    const/16 v1, 0xa9

    if-gt v0, v1, :cond_2b

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2b
    const/16 v1, 0xaa

    if-lt v0, v1, :cond_2c

    const/16 v1, 0xaf

    if-gt v0, v1, :cond_2c

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2c
    const/16 v1, 0xb0

    if-lt v0, v1, :cond_2d

    const/16 v1, 0xb3

    if-gt v0, v1, :cond_2d

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2d
    const/16 v1, 0xb4

    if-lt v0, v1, :cond_2e

    const/16 v1, 0xb9

    if-gt v0, v1, :cond_2e

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2e
    const/16 v1, 0xba

    if-lt v0, v1, :cond_2f

    const/16 v1, 0xbd

    if-gt v0, v1, :cond_2f

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_2f
    const/16 v1, 0xbe

    if-lt v0, v1, :cond_30

    const/16 v1, 0xc3

    if-gt v0, v1, :cond_30

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_30
    const/16 v1, 0xc4

    if-lt v0, v1, :cond_31

    const/16 v1, 0xc7

    if-gt v0, v1, :cond_31

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_31
    const/16 v1, 0xc8

    if-lt v0, v1, :cond_32

    const/16 v1, 0xcd

    if-gt v0, v1, :cond_32

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_32
    const/16 v1, 0xce

    if-lt v0, v1, :cond_33

    const/16 v1, 0xd1

    if-gt v0, v1, :cond_33

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_33
    const/16 v1, 0xd2

    if-lt v0, v1, :cond_34

    const/16 v1, 0xd7

    if-gt v0, v1, :cond_34

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_34
    const/16 v1, 0xd8

    if-lt v0, v1, :cond_35

    const/16 v1, 0xdb

    if-gt v0, v1, :cond_35

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_35
    const/16 v1, 0xdc

    if-lt v0, v1, :cond_36

    const/16 v1, 0xe1

    if-gt v0, v1, :cond_36

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_36
    const/16 v1, 0xe2

    if-lt v0, v1, :cond_37

    const/16 v1, 0xe5

    if-gt v0, v1, :cond_37

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_37
    const/16 v1, 0xe6

    if-lt v0, v1, :cond_38

    const/16 v1, 0xeb

    if-gt v0, v1, :cond_38

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_38
    const/16 v1, 0xec

    if-lt v0, v1, :cond_0

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_39
    iget v1, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    if-eqz v1, :cond_3a

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_3a
    if-nez v0, :cond_3b

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_3b
    if-gt v0, v3, :cond_3c

    invoke-direct {p0, v5}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_3c
    const/16 v1, 0x5f

    if-gt v0, v1, :cond_3d

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_3d
    const/16 v1, 0x64

    if-lt v0, v1, :cond_3e

    const/16 v1, 0x69

    if-gt v0, v1, :cond_3e

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_3e
    const/16 v1, 0x6a

    if-lt v0, v1, :cond_3f

    const/16 v1, 0x6d

    if-gt v0, v1, :cond_3f

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_3f
    const/16 v1, 0x6e

    if-lt v0, v1, :cond_40

    const/16 v1, 0x73

    if-gt v0, v1, :cond_40

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_40
    const/16 v1, 0x74

    if-lt v0, v1, :cond_41

    const/16 v1, 0x77

    if-gt v0, v1, :cond_41

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_41
    const/16 v1, 0x78

    if-lt v0, v1, :cond_42

    const/16 v1, 0x7d

    if-gt v0, v1, :cond_42

    invoke-direct {p0, v3}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0

    :cond_42
    const/16 v1, 0x7e

    if-lt v0, v1, :cond_0

    invoke-direct {p0, v4}, Lcom/android/deskclock/TimePicker;->setKeyRange(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected doOnClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    const/high16 v2, 0x7f0e0000

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/deskclock/TimePicker;->addClickedNumber(I)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateKeypad()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_4

    iget-boolean v2, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    if-eqz v2, :cond_2

    iput v5, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ltz v2, :cond_0

    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget-object v3, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    aput v5, v2, v3

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    if-ne p1, v2, :cond_5

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->onLeftClicked()V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    if-ne p1, v2, :cond_0

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->onRightClicked()V

    goto :goto_0
.end method

.method public getHours()I
    .locals 6

    const/4 v2, 0x0

    const/16 v1, 0xc

    iget-object v3, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    mul-int/lit8 v3, v3, 0xa

    iget-object v4, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    add-int v0, v3, v4

    if-ne v0, v1, :cond_0

    iget v3, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    iget v3, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    :goto_0
    add-int/2addr v1, v0

    :goto_1
    :pswitch_0
    return v1

    :pswitch_1
    move v1, v2

    goto :goto_1

    :pswitch_2
    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getLayoutId()I
    .locals 1

    const v0, 0x7f040027

    return v0
.end method

.method public getMinutes()I
    .locals 3

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-int/lit8 v0, v0, 0xa

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/deskclock/TimePicker;->doOnClick(Landroid/view/View;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/deskclock/TimerSetupView;->onFinishInflate()V

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/deskclock/TimePicker;->mAmpm:[Ljava/lang/String;

    iget-boolean v1, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    const v2, 0x7f0d0080

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    const v2, 0x7f0d0081

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0e007b

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/deskclock/TimePicker;->mAmPmLabel:Landroid/widget/TextView;

    iput v4, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateKeypad()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/deskclock/TimePicker;->mAmpm:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/deskclock/TimePicker;->mAmpm:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_0

    iput v0, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->reset()V

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateKeypad()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    instance-of v1, p1, Lcom/android/deskclock/TimePicker$SavedState;

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/deskclock/TimePicker$SavedState;

    invoke-virtual {v0}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v1, v0, Lcom/android/deskclock/TimePicker$SavedState;->mInputPointer:I

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iget-object v1, v0, Lcom/android/deskclock/TimePicker$SavedState;->mInput:[I

    iput-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    :cond_1
    iget v1, v0, Lcom/android/deskclock/TimePicker$SavedState;->mAmPmState:I

    iput v1, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->updateKeypad()V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/android/deskclock/TimePicker$SavedState;

    invoke-direct {v1, v0}, Lcom/android/deskclock/TimePicker$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iput-object v2, v1, Lcom/android/deskclock/TimePicker$SavedState;->mInput:[I

    iget v2, p0, Lcom/android/deskclock/TimePicker;->mAmPmState:I

    iput v2, v1, Lcom/android/deskclock/TimePicker$SavedState;->mAmPmState:I

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iput v2, v1, Lcom/android/deskclock/TimePicker$SavedState;->mInputPointer:I

    return-object v1
.end method

.method public setSetButton(Landroid/widget/Button;)V
    .locals 0
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/deskclock/TimePicker;->mSetButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->enableSetButton()V

    return-void
.end method

.method protected updateTime()V
    .locals 14

    const/16 v10, 0x9

    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v5, -0x1

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/android/deskclock/TimePicker;->getEnteredTime()I

    move-result v8

    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-le v0, v5, :cond_7

    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget v9, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    aget v6, v0, v9

    iget-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v0, :cond_0

    if-lt v6, v11, :cond_0

    if-le v6, v10, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v0, :cond_2

    if-lt v6, v12, :cond_2

    if-gt v6, v10, :cond_2

    :cond_1
    const/4 v1, -0x2

    :cond_2
    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-lez v0, :cond_5

    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ge v0, v11, :cond_5

    const/4 v0, -0x2

    if-eq v1, v0, :cond_5

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget v9, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    aget v0, v0, v9

    mul-int/lit8 v0, v0, 0xa

    iget-object v9, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget v10, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    add-int/lit8 v10, v10, -0x1

    aget v9, v9, v10

    add-int v7, v0, v9

    iget-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x18

    if-lt v7, v0, :cond_3

    const/16 v0, 0x19

    if-le v7, v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/android/deskclock/TimePicker;->mIs24HoursMode:Z

    if-nez v0, :cond_5

    const/16 v0, 0xd

    if-lt v7, v0, :cond_5

    const/16 v0, 0xf

    if-gt v7, v0, :cond_5

    :cond_4
    const/4 v1, -0x2

    :cond_5
    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ne v0, v11, :cond_6

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v1, v0, v11

    :cond_6
    :goto_0
    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ge v0, v12, :cond_8

    move v2, v5

    :goto_1
    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ge v0, v13, :cond_9

    move v3, v5

    :goto_2
    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-gez v0, :cond_a

    move v4, v5

    :goto_3
    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mEnteredTime:Lcom/android/deskclock/timer/TimerView;

    invoke-virtual/range {v0 .. v5}, Lcom/android/deskclock/timer/TimerView;->setTime(IIIII)V

    return-void

    :cond_7
    const/4 v1, -0x1

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v2, v0, v12

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v3, v0, v13

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v9, 0x0

    aget v4, v0, v9

    goto :goto_3
.end method
