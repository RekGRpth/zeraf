.class final Lcom/android/deskclock/Alarm$DaysOfWeek;
.super Ljava/lang/Object;
.source "Alarm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/Alarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DaysOfWeek"
.end annotation


# static fields
.field private static DAY_MAP:[I

.field private static DAY_TO_BIT_MASK:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDays:I

.field private mRepeatPreferenceExtension:Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x7

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_MAP:[I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_TO_BIT_MASK:Ljava/util/HashMap;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_MAP:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_TO_BIT_MASK:Ljava/util/HashMap;

    sget-object v2, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_MAP:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x1
    .end array-data
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    return-void
.end method

.method private isSet(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toString(Landroid/content/Context;ZZ)Ljava/lang/String;
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # Z

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    if-nez v11, :cond_1

    if-eqz p2, :cond_0

    const v11, 0x7f0d002c

    invoke-virtual {p1, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_0
    return-object v11

    :cond_0
    const-string v11, ""

    goto :goto_0

    :cond_1
    iget v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    const/16 v12, 0x7f

    if-ne v11, v12, :cond_2

    const v11, 0x7f0d002b

    invoke-virtual {p1, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :cond_2
    iget-object v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mRepeatPreferenceExtension:Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;

    if-nez v11, :cond_3

    const-class v11, Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    new-array v12, v12, [Landroid/content/pm/Signature;

    invoke-static {p1, v11, v12}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v9

    const/4 v7, 0x0

    invoke-virtual {v9}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v0

    :goto_1
    if-ge v7, v0, :cond_3

    invoke-virtual {v9, v7}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v8

    :try_start_0
    invoke-virtual {v8}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;

    if-eqz v6, :cond_4

    iput-object v6, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mRepeatPreferenceExtension:Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    iget-object v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mRepeatPreferenceExtension:Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mRepeatPreferenceExtension:Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;

    invoke-interface {v11}, Lcom/mediatek/deskclock/ext/IRepeatPreferenceExtension;->shouldUseMTKRepeatPref()Z

    move-result v11

    if-eqz v11, :cond_5

    iget v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    const/16 v12, 0x1f

    if-ne v11, v12, :cond_5

    const v11, 0x7f0d0005

    invoke-virtual {p1, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    iget v3, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    :goto_2
    if-lez v3, :cond_7

    and-int/lit8 v11, v3, 0x1

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    add-int/lit8 v1, v1, 0x1

    :cond_6
    shr-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    if-nez p3, :cond_8

    const/4 v11, 0x1

    if-gt v1, v11, :cond_a

    :cond_8
    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v2

    :goto_3
    const/4 v7, 0x0

    :goto_4
    const/4 v11, 0x7

    if-ge v7, v11, :cond_b

    iget v11, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    const/4 v12, 0x1

    shl-int/2addr v12, v7

    and-int/2addr v11, v12

    if-eqz v11, :cond_9

    sget-object v11, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_MAP:[I

    aget v11, v11, v7

    aget-object v11, v2, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_9

    const v11, 0x7f0d002d

    invoke-virtual {p1, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_a
    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0
.end method


# virtual methods
.method public getBooleanArray()[Z
    .locals 4

    const/4 v3, 0x7

    new-array v1, v3, [Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/android/deskclock/Alarm$DaysOfWeek;->isSet(I)Z

    move-result v2

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getCoded()I
    .locals 1

    iget v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    return v0
.end method

.method public getNextAlarm(Ljava/util/Calendar;)I
    .locals 5
    .param p1    # Ljava/util/Calendar;

    const/4 v4, 0x7

    iget v3, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    if-nez v3, :cond_1

    const/4 v1, -0x1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    rem-int/lit8 v2, v3, 0x7

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    add-int v3, v2, v1

    rem-int/lit8 v0, v3, 0x7

    invoke-direct {p0, v0}, Lcom/android/deskclock/Alarm$DaysOfWeek;->isSet(I)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getSetDays()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_1

    invoke-direct {p0, v0}, Lcom/android/deskclock/Alarm$DaysOfWeek;->isSet(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_MAP:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public isRepeatSet()Z
    .locals 1

    iget v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    shl-int/2addr v1, p1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    goto :goto_0
.end method

.method public set(Lcom/android/deskclock/Alarm$DaysOfWeek;)V
    .locals 1
    .param p1    # Lcom/android/deskclock/Alarm$DaysOfWeek;

    iget v0, p1, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    iput v0, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    return-void
.end method

.method public setDayOfWeek(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    sget-object v1, Lcom/android/deskclock/Alarm$DaysOfWeek;->DAY_TO_BIT_MASK:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/android/deskclock/Alarm$DaysOfWeek;->set(IZ)V

    return-void
.end method

.method public toAccessibilityString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/deskclock/Alarm$DaysOfWeek;->toString(Landroid/content/Context;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DaysOfWeek{mDays="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/deskclock/Alarm$DaysOfWeek;->mDays:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/deskclock/Alarm$DaysOfWeek;->toString(Landroid/content/Context;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
