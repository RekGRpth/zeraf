.class public Lcom/android/deskclock/AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmReceiver.java"


# static fields
.field static final ALARM_PHONE_LISTENER:Ljava/lang/String; = "com.android.deskclock.ALARM_PHONE_LISTENER"

.field private static final STALE_WINDOW:I = 0x1b7740

.field private static final VIBRATE_LENGTH:I = 0x3e8


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentCallState:I

.field private mICMCCSpecialSpecExtension:Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

.field private mTelephonyService:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/AlarmReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/AlarmReceiver;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/AlarmReceiver;->handleIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private buildNotification(Landroid/content/Context;ZLcom/android/deskclock/Alarm;)Landroid/app/Notification;
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # Lcom/android/deskclock/Alarm;

    new-instance v8, Landroid/content/Intent;

    const-class v13, Lcom/android/deskclock/AlarmAlert;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v13, "intent.extra.alarm"

    move-object/from16 v0, p3

    invoke-virtual {v8, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/android/deskclock/Alarm;->id:I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v8, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    new-instance v12, Landroid/content/Intent;

    const-string v13, "com.android.deskclock.ALARM_SNOOZE"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v13, "intent.extra.alarm"

    move-object/from16 v0, p3

    invoke-virtual {v12, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/android/deskclock/Alarm;->id:I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v12, v14}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    new-instance v5, Landroid/content/Intent;

    const-string v13, "com.android.deskclock.ALARM_DISMISS"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v13, "intent.extra.alarm"

    move-object/from16 v0, p3

    invoke-virtual {v5, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/android/deskclock/Alarm;->id:I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v5, v14}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    move-object/from16 v0, p3

    iget-wide v13, v0, Lcom/android/deskclock/Alarm;->time:J

    invoke-virtual {v4, v13, v14}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/android/deskclock/Alarms;->formatTime(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/deskclock/Alarm;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v13, Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v13

    const v14, 0x7f020075

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v13

    const-wide/16 v14, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    if-nez v13, :cond_0

    if-eqz p2, :cond_1

    const v13, 0x7f020075

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0d0022

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14, v11}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v13

    const v14, 0x1080038

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0d0020

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15, v9}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_0
    :goto_0
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    iput-object v10, v7, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    return-object v7

    :cond_1
    const v13, 0x1080038

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0d0020

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v13, v14, v9}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method private getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;
    .locals 1
    .param p1    # Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method private handleIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 28
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v24, "alarm_killed"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_0

    const-string v24, "alarm_change_notification"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    :cond_0
    const-string v24, "snoozed"

    const/16 v25, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_1

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Lcom/android/deskclock/Alarm;

    const-string v25, "alarm_killed_timeout"

    const/16 v26, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/android/deskclock/AlarmReceiver;->updateNotification(Landroid/content/Context;Lcom/android/deskclock/Alarm;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v24, "cancel_snooze"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    const/4 v4, 0x0

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    :cond_3
    if-eqz v4, :cond_4

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-static {v0, v1}, Lcom/android/deskclock/Alarms;->disableSnoozeAlert(Landroid/content/Context;I)V

    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    :goto_1
    new-instance v24, Landroid/content/Intent;

    const-string v25, "com.android.deskclock.ALARM_SNOOZE_CANCELLED"

    invoke-direct/range {v24 .. v25}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v24, "Unable to parse Alarm from intent."

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    const/16 v24, -0x1

    const-wide/16 v25, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    move-wide/from16 v2, v25

    invoke-static {v0, v1, v2, v3}, Lcom/android/deskclock/Alarms;->saveSnoozeAlert(Landroid/content/Context;IJ)V

    goto :goto_1

    :cond_5
    const-string v24, "disable_or_enable_snooze_notification"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    const-string v24, "enable"

    const/16 v25, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    const-string v24, "alarmID"

    const/16 v25, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v24, "alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v4}, Lcom/android/deskclock/AlarmReceiver;->buildNotification(Landroid/content/Context;ZLcom/android/deskclock/Alarm;)Landroid/app/Notification;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v14, v2}, Lcom/android/deskclock/AlarmReceiver;->sendNotification(Landroid/content/Context;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_6
    const-string v24, "com.android.deskclock.ALARM_ALERT"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    const/4 v4, 0x0

    const-string v24, "setNextAlert"

    const/16 v25, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_d

    const-string v24, "intent.extra.alarm_raw"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v15

    const/16 v24, 0x0

    array-length v0, v9

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v15, v9, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v24, Lcom/android/deskclock/Alarm;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, v24

    invoke-interface {v0, v15}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    :cond_7
    if-nez v4, :cond_8

    const-string v24, "Failed to parse the alarm from the intent"

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_8
    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-static {v0, v1}, Lcom/android/deskclock/Alarms;->disableSnoozeAlert(Landroid/content/Context;I)V

    iget-object v0, v4, Lcom/android/deskclock/Alarm;->daysOfWeek:Lcom/android/deskclock/Alarm$DaysOfWeek;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/deskclock/Alarm$DaysOfWeek;->isRepeatSet()Z

    move-result v24

    if-nez v24, :cond_c

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/android/deskclock/Alarms;->enableAlarm(Landroid/content/Context;IZ)V

    :cond_9
    :goto_2
    :try_start_0
    const-string v24, "phone"

    invoke-static/range {v24 .. v24}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/deskclock/AlarmReceiver;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v24, v0

    if-eqz v24, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/android/internal/telephony/ITelephony;->getPreciseCallState()I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v24, v0

    if-eqz v24, :cond_b

    new-instance v19, Landroid/content/Intent;

    const-string v24, "com.android.deskclock.ALARM_PHONE_LISTENER"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mICMCCSpecialSpecExtension:Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

    move-object/from16 v24, v0

    if-nez v24, :cond_a

    const-class v24, Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Landroid/content/pm/Signature;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v22

    const/4 v13, 0x0

    invoke-virtual/range {v22 .. v22}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v8

    :goto_3
    if-ge v13, v8, :cond_a

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v21

    :try_start_1
    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

    if-eqz v11, :cond_e

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/deskclock/AlarmReceiver;->mICMCCSpecialSpecExtension:Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;
    :try_end_1
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mICMCCSpecialSpecExtension:Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

    move-object/from16 v24, v0

    if-eqz v24, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/AlarmReceiver;->mICMCCSpecialSpecExtension:Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/mediatek/deskclock/ext/ICMCCSpecialSpecExtension;->isCMCCSpecialSpec()Z

    move-result v24

    if-eqz v24, :cond_f

    const-string v24, "CMCC special spec : do not vibrate when in call state "

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_b
    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Received alarm set for id="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    iget-wide v0, v4, Lcom/android/deskclock/Alarm;->time:J

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Lcom/android/deskclock/Log;->formatTime(J)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    iget-wide v0, v4, Lcom/android/deskclock/Alarm;->time:J

    move-wide/from16 v24, v0

    const-wide/32 v26, 0x1b7740

    add-long v24, v24, v26

    cmp-long v24, v17, v24

    if-lez v24, :cond_10

    const-string v24, "Ignoring stale alarm"

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_d
    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/Alarm;

    goto/16 :goto_2

    :catch_0
    move-exception v10

    :try_start_3
    const-string v24, "can not create plugin object!"

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->e(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_e
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    :cond_f
    const-string v24, "vibrator"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/os/Vibrator;

    if-eqz v23, :cond_b

    const-string v24, "vibrator starts,and vibrates:1000 ms"

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    const-wide/16 v24, 0x3e8

    invoke-virtual/range {v23 .. v25}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_4

    :catch_1
    move-exception v10

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Catch exception when getPreciseCallState: ex = "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_10
    invoke-static/range {p1 .. p1}, Lcom/android/deskclock/AlarmAlertWakeLock;->acquireCpuWakeLock(Landroid/content/Context;)V

    new-instance v7, Landroid/content/Intent;

    const-string v24, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    move-object/from16 v0, v24

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-class v6, Lcom/android/deskclock/AlarmAlert;

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/android/deskclock/AlarmReceiver;->buildNotification(Landroid/content/Context;ZLcom/android/deskclock/Alarm;)Landroid/app/Notification;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v24, v0

    if-eqz v24, :cond_11

    const/16 v24, 0x64

    move/from16 v0, v24

    move-object/from16 v1, v16

    iput v0, v1, Landroid/app/Notification;->ledOffMS:I

    const/16 v24, 0xc8

    move/from16 v0, v24

    move-object/from16 v1, v16

    iput v0, v1, Landroid/app/Notification;->ledOnMS:I

    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v24, v0

    if-nez v24, :cond_12

    new-instance v5, Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v24, 0x10040000

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v24, v0

    const/high16 v25, 0x8000000

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v5, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    :cond_12
    invoke-static {}, Lcom/android/deskclock/Alarms;->bootFromPoweroffAlarm()Z

    move-result v24

    if-nez v24, :cond_13

    iget v0, v4, Lcom/android/deskclock/Alarm;->id:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move-object/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/android/deskclock/AlarmReceiver;->sendNotification(Landroid/content/Context;ILandroid/app/Notification;)V

    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/AlarmReceiver;->mCurrentCallState:I

    move/from16 v24, v0

    if-nez v24, :cond_1

    new-instance v20, Landroid/content/Intent;

    const-string v24, "com.android.deskclock.ALARM_ALERT"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v24, "intent.extra.alarm"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method

.method private sendNotification(Landroid/content/Context;ILandroid/app/Notification;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/app/Notification;

    invoke-direct {p0, p1}, Lcom/android/deskclock/AlarmReceiver;->getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual {v0, p2, p3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private updateNotification(Landroid/content/Context;Lcom/android/deskclock/Alarm;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/deskclock/Alarm;
    .param p3    # I

    const/4 v8, 0x0

    invoke-direct {p0, p1}, Lcom/android/deskclock/AlarmReceiver;->getNotificationManager(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v3

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/deskclock/AlarmClock;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "intent.extra.alarm"

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-static {p1, v5, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p2, p1}, Lcom/android/deskclock/Alarm;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/Notification;

    const v5, 0x7f020075

    iget-wide v6, p2, Lcom/android/deskclock/Alarm;->time:J

    invoke-direct {v2, v5, v1, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v5, 0x7f0d0021

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, p1, v1, v5, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v5, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v2, Landroid/app/Notification;->flags:I

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    iget v5, p2, Lcom/android/deskclock/Alarm;->id:I

    invoke-virtual {v3, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/android/deskclock/AlarmReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v4

    invoke-static {p1}, Lcom/android/deskclock/AlarmAlertWakeLock;->createPartialWakeLock(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    new-instance v0, Lcom/android/deskclock/AlarmReceiver$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/deskclock/AlarmReceiver$1;-><init>(Lcom/android/deskclock/AlarmReceiver;Landroid/content/Context;Landroid/content/Intent;Landroid/content/BroadcastReceiver$PendingResult;Landroid/os/PowerManager$WakeLock;)V

    invoke-static {v0}, Lcom/android/deskclock/AsyncHandler;->post(Ljava/lang/Runnable;)V

    return-void
.end method
