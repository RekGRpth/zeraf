.class public Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "StopwatchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/stopwatch/StopwatchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LapsListAdapter"
.end annotation


# instance fields
.field private final mBackgroundColor:I

.field private final mFormats:[Ljava/lang/String;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mLapFormat:Ljava/lang/String;

.field private final mLapFormatSet:[Ljava/lang/String;

.field private mLapIndex:I

.field mLaps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;",
            ">;"
        }
    .end annotation
.end field

.field private final mThresholds:[J

.field private mTotalIndex:I

.field final synthetic this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;


# direct methods
.method public constructor <init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mThresholds:[J

    iput v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    iput v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mBackgroundColor:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mFormats:[Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapFormatSet:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateLapFormat()V

    return-void

    :array_0
    .array-data 8
        0x927c0
        0x36ee80
        0x2255100
        0x15752a00
        0xd693a400L
    .end array-data
.end method

.method static synthetic access$700(Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateLapFormat()V

    return-void
.end method

.method private resetTimeFormats()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    iput v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    return-void
.end method

.method private updateLapFormat()V
    .locals 3

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapFormatSet:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapFormat:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addLap(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)V
    .locals 2
    .param p1    # Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public clearLaps()V
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateLapFormat()V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->resetTimeFormats()V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getLapTimes()[J
    .locals 5

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v2, [J

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-wide v3, v3, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    aput-wide v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p1, v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    if-eqz p2, :cond_2

    move-object v1, p2

    :goto_1
    const v4, 0x7f0e004e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, 0x7f0e004f

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v4, 0x7f0e0050

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-wide v4, v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    iget-object v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mFormats:[Ljava/lang/String;

    iget v7, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    aget-object v6, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/deskclock/stopwatch/Stopwatches;->formatTimeText(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-wide v4, v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    iget-object v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mFormats:[Ljava/lang/String;

    iget v7, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    aget-object v6, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/deskclock/stopwatch/Stopwatches;->formatTimeText(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapFormat:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mBackgroundColor:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f04001a

    invoke-virtual {v4, v5, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1
.end method

.method public setLapTimes([J)V
    .locals 11
    .param p1    # [J

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    array-length v7, p1

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_2

    iget-object v10, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;

    aget-wide v2, p1, v6

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;JJ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const-wide/16 v8, 0x0

    add-int/lit8 v6, v7, -0x1

    :goto_2
    if-ltz v6, :cond_3

    aget-wide v0, p1, v6

    add-long/2addr v8, v0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iput-wide v8, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLaps:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    invoke-virtual {p0, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateTimeFormats(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)Z

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateLapFormat()V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public updateTimeFormats(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)Z
    .locals 5
    .param p1    # Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mThresholds:[J

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-wide v1, p1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mThresholds:[J

    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    aget-wide v3, v3, v4

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mLapIndex:I

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mThresholds:[J

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-wide v1, p1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mThresholds:[J

    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    aget-wide v3, v3, v4

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->mTotalIndex:I

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    return v0
.end method
