.class public Lcom/android/deskclock/TimerSetupView;
.super Landroid/widget/LinearLayout;
.source "TimerSetupView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected mDelete:Landroid/widget/ImageButton;

.field protected mEnteredTime:Lcom/android/deskclock/timer/TimerView;

.field protected mInput:[I

.field protected mInputPointer:I

.field protected mInputSize:I

.field protected mLeft:Landroid/widget/Button;

.field protected final mNumbers:[Landroid/widget/Button;

.field protected mRight:Landroid/widget/Button;

.field protected mStart:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/TimerSetupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x5

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    const/16 v1, 0xa

    new-array v1, v1, [Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iput-object p1, p0, Lcom/android/deskclock/TimerSetupView;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->getLayoutId()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method


# virtual methods
.method protected doOnClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    const/high16 v2, 0x7f0e0000

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    :goto_1
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v4, v4, v0

    aput v4, v2, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v5

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateTime()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_0

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ltz v2, :cond_0

    const/4 v0, 0x0

    :goto_2
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget-object v3, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    iget v3, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    aput v5, v2, v3

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateTime()V

    goto :goto_0
.end method

.method protected getLayoutId()I
    .locals 1

    const v0, 0x7f040028

    return v0
.end method

.method public getTime()I
    .locals 3

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v1, 0x4

    aget v0, v0, v1

    mul-int/lit16 v0, v0, 0xe10

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    mul-int/lit16 v1, v1, 0x258

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x3c

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/deskclock/TimerSetupView;->doOnClick(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateStartButton()V

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateDeleteButton()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 13

    const/4 v12, 0x1

    const v11, 0x7f0e0071

    const v10, 0x7f0e0070

    const v8, 0x7f0e006f

    const/4 v9, 0x0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v5, 0x7f0e007d

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v5, 0x7f0e007e

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0e007f

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v5, 0x7f0e0080

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0e0075

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/android/deskclock/timer/TimerView;

    iput-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mEnteredTime:Lcom/android/deskclock/timer/TimerView;

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mEnteredTime:Lcom/android/deskclock/timer/TimerView;

    invoke-virtual {v5, v9, v9, v9, v12}, Landroid/view/View;->setPadding(IIII)V

    const v5, 0x7f0e007c

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v12

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x2

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x3

    invoke-virtual {v1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x4

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x5

    invoke-virtual {v2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x6

    invoke-virtual {v2, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/4 v7, 0x7

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/16 v7, 0x8

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    const/16 v7, 0x9

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v7

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    aput-object v5, v6, v9

    invoke-virtual {v4, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {p0, v9}, Lcom/android/deskclock/TimerSetupView;->setLeftRightEnabled(Z)V

    const/4 v0, 0x0

    :goto_0
    const/16 v5, 0xa

    if-ge v0, v5, :cond_0

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    aget-object v5, v5, v0

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    aget-object v5, v5, v0

    const-string v6, "%d"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mNumbers:[Landroid/widget/Button;

    aget-object v5, v5, v0

    const/high16 v6, 0x7f0e0000

    new-instance v7, Ljava/lang/Integer;

    invoke-direct {v7, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateTime()V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->reset()V

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateStartButton()V

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateDeleteButton()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerStartButton(Landroid/widget/Button;)V
    .locals 0
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/deskclock/TimerSetupView;->mStart:Landroid/widget/Button;

    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateTime()V

    return-void
.end method

.method public restoreEntryState(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    if-eqz v1, :cond_2

    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    array-length v3, v1

    if-ne v2, v3, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/deskclock/TimerSetupView;->mInputSize:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v3, v1, v0

    aput v3, v2, v0

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    aget v2, v2, v0

    if-eqz v2, :cond_0

    iput v0, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/deskclock/TimerSetupView;->updateTime()V

    :cond_2
    return-void
.end method

.method public saveEntryState(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-void
.end method

.method protected setLeftRightEnabled(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mLeft:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mRight:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public updateDeleteButton()V
    .locals 3

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mDelete:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateStartButton()V
    .locals 3

    iget v1, p0, Lcom/android/deskclock/TimerSetupView;->mInputPointer:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mStart:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/TimerSetupView;->mStart:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateTime()V
    .locals 8

    iget-object v0, p0, Lcom/android/deskclock/TimerSetupView;->mEnteredTime:Lcom/android/deskclock/timer/TimerView;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    iget-object v4, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-int/lit8 v5, v5, 0xa

    iget-object v6, p0, Lcom/android/deskclock/TimerSetupView;->mInput:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/2addr v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/deskclock/timer/TimerView;->setTime(IIIII)V

    return-void
.end method
