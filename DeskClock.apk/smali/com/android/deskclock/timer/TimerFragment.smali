.class public Lcom/android/deskclock/timer/TimerFragment;
.super Lcom/android/deskclock/DeskClockFragment;
.source "TimerFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/timer/TimerFragment$TimesUpListAdapter;,
        Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;,
        Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;,
        Lcom/android/deskclock/timer/TimerFragment$ClickAction;
    }
.end annotation


# static fields
.field private static final KEY_ENTRY_STATE:Ljava/lang/String; = "entry_state"

.field private static final KEY_SETUP_SELECTED:Ljava/lang/String; = "_setup_selected"

.field private static final TAG:Ljava/lang/String; = "TimerFragment"


# instance fields
.field private mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

.field private mAddTimer:Landroid/widget/ImageButton;

.field private mCancel:Landroid/widget/Button;

.field private final mClockTick:Ljava/lang/Runnable;

.field private mLastVisibleView:Landroid/view/View;

.field private mNewTimerPage:Landroid/view/View;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSeperator:Landroid/view/View;

.field private mStart:Landroid/widget/Button;

.field private mTicking:Z

.field private mTimerFooter:Landroid/view/View;

.field private mTimerSetup:Lcom/android/deskclock/TimerSetupView;

.field private mTimersList:Landroid/widget/ListView;

.field private mTimersListPage:Landroid/view/View;

.field private mViewState:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/deskclock/DeskClockFragment;-><init>()V

    iput-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTicking:Z

    iput-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    new-instance v0, Lcom/android/deskclock/timer/TimerFragment$1;

    invoke-direct {v0, p0}, Lcom/android/deskclock/timer/TimerFragment$1;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mClockTick:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/timer/TimerFragment;Lcom/android/deskclock/timer/TimerFragment$ClickAction;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;
    .param p1    # Lcom/android/deskclock/timer/TimerFragment$ClickAction;

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->onClickHelper(Lcom/android/deskclock/timer/TimerFragment$ClickAction;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/deskclock/timer/TimerFragment;Lcom/android/deskclock/timer/TimerObj;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->setTimerButtons(Lcom/android/deskclock/timer/TimerObj;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/deskclock/timer/TimerFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->gotoSetupView()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/deskclock/timer/TimerFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/deskclock/timer/TimerFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/deskclock/timer/TimerFragment;Lcom/android/deskclock/timer/TimerObj;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->onLabelPressed(Lcom/android/deskclock/timer/TimerObj;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/deskclock/timer/TimerFragment;)Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/deskclock/timer/TimerFragment;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/deskclock/timer/TimerFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mClockTick:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/deskclock/timer/TimerFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/deskclock/timer/TimerFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->gotoTimersView()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/deskclock/timer/TimerFragment;)Lcom/android/deskclock/TimerSetupView;
    .locals 1
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/deskclock/timer/TimerFragment;Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/timer/TimerFragment;
    .param p1    # Lcom/android/deskclock/timer/TimerObj;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    return-void
.end method

.method private cancelTimerNotification(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private gotoSetupView()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0e008a

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->stopClockTicks()V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mCancel:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mSeperator:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    invoke-virtual {v1}, Lcom/android/deskclock/TimerSetupView;->updateStartButton()V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    invoke-virtual {v1}, Lcom/android/deskclock/TimerSetupView;->updateDeleteButton()V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    iput-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    sget-object v2, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v1, 0x7d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/android/deskclock/timer/TimerFragment$5;

    invoke-direct {v1, p0}, Lcom/android/deskclock/timer/TimerFragment$5;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mSeperator:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mCancel:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private gotoTimersView()V
    .locals 4

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0e0086

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    :goto_0
    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->startClockTicks()V

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    iput-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    sget-object v2, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v1, 0x7d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/android/deskclock/timer/TimerFragment$6;

    invoke-direct {v1, p0}, Lcom/android/deskclock/timer/TimerFragment$6;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private onClickHelper(Lcom/android/deskclock/timer/TimerFragment$ClickAction;)V
    .locals 5
    .param p1    # Lcom/android/deskclock/timer/TimerFragment$ClickAction;

    iget v2, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mAction:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mTimer:Lcom/android/deskclock/timer/TimerObj;

    iget v2, v1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    iget v2, v1, Lcom/android/deskclock/timer/TimerObj;->mTimerId:I

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->cancelTimerNotification(I)V

    :cond_0
    iget-object v2, v1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/android/deskclock/timer/TimerFragment$7;

    invoke-direct {v2, p0, v1}, Lcom/android/deskclock/timer/TimerFragment$7;-><init>(Lcom/android/deskclock/timer/TimerFragment;Lcom/android/deskclock/timer/TimerObj;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mTimer:Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->onPlusOneButtonPressed(Lcom/android/deskclock/timer/TimerObj;)V

    iget-object v2, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mTimer:Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->setTimerButtons(Lcom/android/deskclock/timer/TimerObj;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mTimer:Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->onStopButtonPressed(Lcom/android/deskclock/timer/TimerObj;)V

    iget-object v2, p1, Lcom/android/deskclock/timer/TimerFragment$ClickAction;->mTimer:Lcom/android/deskclock/timer/TimerObj;

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->setTimerButtons(Lcom/android/deskclock/timer/TimerObj;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private onLabelPressed(Lcom/android/deskclock/timer/TimerObj;)V
    .locals 5
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "label_dialog"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iget-object v3, p1, Lcom/android/deskclock/timer/TimerObj;->mLabel:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/android/deskclock/LabelDialogFragment;->newInstance(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;Ljava/lang/String;)Lcom/android/deskclock/LabelDialogFragment;

    move-result-object v1

    const-string v3, "label_dialog"

    invoke-virtual {v1, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private onPlusOneButtonPressed(Lcom/android/deskclock/timer/TimerObj;)V
    .locals 9
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    const-wide/32 v2, 0xea60

    const/4 v5, 0x1

    const/4 v8, 0x0

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1, v2, v3}, Lcom/android/deskclock/timer/TimerObj;->addTime(J)V

    invoke-virtual {p1, v8}, Lcom/android/deskclock/timer/TimerObj;->updateTimeLeft(Z)J

    move-result-wide v6

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0, v6, v7, v8}, Lcom/android/deskclock/timer/TimerListItem;->setTime(JZ)V

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0, v6, v7}, Lcom/android/deskclock/timer/TimerListItem;->setLength(J)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    const-string v0, "timer_update"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iput v5, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mStartTime:J

    iput-wide v2, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iput-wide v2, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    iget-wide v1, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    invoke-virtual {v0, v1, v2, v8}, Lcom/android/deskclock/timer/TimerListItem;->setTime(JZ)V

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    iget-wide v1, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v3, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    invoke-virtual/range {v0 .. v5}, Lcom/android/deskclock/timer/TimerListItem;->set(JJZ)V

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->start()V

    const-string v0, "timer_reset"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    const-string v0, "start_timer"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->updateTimesUpMode(Lcom/android/deskclock/timer/TimerObj;)V

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mTimerId:I

    invoke-direct {p0, v0}, Lcom/android/deskclock/timer/TimerFragment;->cancelTimerNotification(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    iput v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    iget-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mSetupLength:J

    iput-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iput-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->stop()V

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    iget-wide v1, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    invoke-virtual {v0, v1, v2, v8}, Lcom/android/deskclock/timer/TimerListItem;->setTime(JZ)V

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    iget-wide v1, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v3, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/android/deskclock/timer/TimerListItem;->set(JJZ)V

    const-string v0, "timer_reset"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onStopButtonPressed(Lcom/android/deskclock/timer/TimerObj;)V
    .locals 6
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    const/4 v1, 0x1

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, 0x2

    iput v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->pause()V

    invoke-virtual {p1, v1}, Lcom/android/deskclock/timer/TimerObj;->updateTimeLeft(Z)J

    const-string v0, "timer_stop"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iput v1, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v4, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    iput-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mStartTime:J

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->start()V

    const-string v0, "start_timer"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    iput v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->done()V

    :cond_0
    const-string v0, "timer_done"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mTimerId:I

    invoke-direct {p0, v0}, Lcom/android/deskclock/timer/TimerFragment;->cancelTimerNotification(I)V

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->updateTimesUpMode(Lcom/android/deskclock/timer/TimerObj;)V

    goto :goto_0

    :pswitch_4
    iput v1, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v4, p1, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    iput-wide v0, p1, Lcom/android/deskclock/timer/TimerObj;->mStartTime:J

    iget-object v0, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    check-cast v0, Lcom/android/deskclock/timer/TimerListItem;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerListItem;->start()V

    const-string v0, "start_timer"

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerFragment;->updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private saveViewState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v1, "_setup_selected"

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    const-string v1, "entry_state"

    invoke-virtual {v0, p1, v1}, Lcom/android/deskclock/TimerSetupView;->saveEntryState(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTimerButtons(Lcom/android/deskclock/timer/TimerObj;)V
    .locals 12
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    const v11, 0x7f0d006d

    const v10, 0x7f0d006a

    const v9, 0x7f0b0011

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    const v6, 0x7f0e0095

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iget-object v5, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    const v6, 0x7f0e0075

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/timer/CountingTimerView;

    iget-object v5, p1, Lcom/android/deskclock/timer/TimerObj;->mView:Landroid/view/View;

    const v6, 0x7f0e0096

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v5, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0x7f0d006c

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v5, 0x7f020058

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v8}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0x7f0d006e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v5, 0x7f02005b

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v8}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0x7f020058

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v8}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0x7f0d006e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v5, 0x7f02005b

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v7}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    goto/16 :goto_0

    :pswitch_4
    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v8}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private startClockTicks()V
    .locals 4

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mClockTick:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTicking:Z

    return-void
.end method

.method private stopClockTicks()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTicking:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mClockTick:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTicking:Z

    :cond_0
    return-void
.end method

.method private updateTimersState(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/deskclock/timer/TimerObj;
    .param p2    # Ljava/lang/String;

    const-string v1, "delete_timer"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p1, v1}, Lcom/android/deskclock/timer/TimerObj;->writeToSharedPref(Landroid/content/SharedPreferences;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "timer.intent.extra"

    iget v2, p1, Lcom/android/deskclock/timer/TimerObj;->mTimerId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private updateTimesUpMode(Lcom/android/deskclock/timer/TimerObj;)V
    .locals 2
    .param p1    # Lcom/android/deskclock/timer/TimerObj;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0, p1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->removeTimer(Lcom/android/deskclock/timer/TimerObj;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    invoke-interface {v0}, Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;->onEmptyList()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    invoke-interface {v0}, Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;->onListChanged()V

    goto :goto_0
.end method


# virtual methods
.method createAdapter(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;-><init>(Lcom/android/deskclock/timer/TimerFragment;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/deskclock/timer/TimerFragment$TimesUpListAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/deskclock/timer/TimerFragment$TimesUpListAdapter;-><init>(Lcom/android/deskclock/timer/TimerFragment;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/timer/TimerFragment$ClickAction;

    invoke-direct {p0, v0}, Lcom/android/deskclock/timer/TimerFragment;->onClickHelper(Lcom/android/deskclock/timer/TimerFragment$ClickAction;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v11, 0x7f0b0008

    const/4 v8, 0x0

    const v7, 0x7f04002a

    invoke-virtual {p1, v7, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v7, "times_up"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "times_up"

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const v7, 0x7f0e0087

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    instance-of v7, v7, Lcom/android/deskclock/DeskClock;

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f09004e

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v7, 0x7f040009

    iget-object v9, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {p1, v7, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v7, v7

    sub-float/2addr v7, v1

    float-to-int v7, v7

    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v3, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v11}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {v7, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    const v7, 0x7f04000a

    iget-object v9, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {p1, v7, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v7, v7

    sub-float/2addr v7, v1

    float-to-int v7, v7

    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {v7, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :goto_1
    const v7, 0x7f0e008a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    const v7, 0x7f0e0086

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersListPage:Landroid/view/View;

    const v7, 0x7f0e008b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/deskclock/TimerSetupView;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    const v7, 0x7f0e008d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mSeperator:Landroid/view/View;

    const v7, 0x7f0e008c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mCancel:Landroid/widget/Button;

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mCancel:Landroid/widget/Button;

    new-instance v9, Lcom/android/deskclock/timer/TimerFragment$2;

    invoke-direct {v9, p0}, Lcom/android/deskclock/timer/TimerFragment$2;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v7, 0x7f0e008e

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mStart:Landroid/widget/Button;

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mStart:Landroid/widget/Button;

    new-instance v9, Lcom/android/deskclock/timer/TimerFragment$3;

    invoke-direct {v9, p0}, Lcom/android/deskclock/timer/TimerFragment$3;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    iget-object v9, p0, Lcom/android/deskclock/timer/TimerFragment;->mStart:Landroid/widget/Button;

    invoke-virtual {v7, v9}, Lcom/android/deskclock/TimerSetupView;->registerStartButton(Landroid/widget/Button;)V

    const v7, 0x7f0e0089

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mAddTimer:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mAddTimer:Landroid/widget/ImageButton;

    new-instance v9, Lcom/android/deskclock/timer/TimerFragment$4;

    invoke-direct {v9, p0}, Lcom/android/deskclock/timer/TimerFragment$4;-><init>(Lcom/android/deskclock/timer/TimerFragment;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v7, 0x7f0e0088

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerFooter:Landroid/view/View;

    iget-object v9, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerFooter:Landroid/view/View;

    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mOnEmptyListListener:Lcom/android/deskclock/timer/TimerFragment$OnEmptyListListener;

    if-nez v7, :cond_2

    move v7, v8

    :goto_2
    invoke-virtual {v9, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    iput-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v6

    :catch_0
    move-exception v2

    const-string v7, "TimerFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " must implement OnEmptyListListener"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    iget-object v7, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    :cond_2
    const/16 v7, 0x8

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/android/deskclock/timer/TimerFragment;->saveViewState(Landroid/os/Bundle;)V

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onPageChanged(I)V
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->sort()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/deskclock/DeskClock;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/DeskClock;

    invoke-virtual {v0, p0}, Lcom/android/deskclock/DeskClock;->unregisterPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->stopClockTicks()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->saveGlobalState()V

    :cond_1
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onResume()V
    .locals 13

    const/4 v12, 0x0

    const/16 v11, 0x8

    const/4 v10, -0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    instance-of v5, v5, Lcom/android/deskclock/DeskClock;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/android/deskclock/DeskClock;

    invoke-virtual {v5, p0}, Lcom/android/deskclock/DeskClock;->registerPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v5, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v5, v6}, Lcom/android/deskclock/timer/TimerFragment;->createAdapter(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v12}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "from_notification"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "timer_notif_time"

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v7

    invoke-interface {v5, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "timer_notif_id"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v10, :cond_1

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v5, v5, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->mTimers:Ljava/util/ArrayList;

    invoke-static {v5, v4}, Lcom/android/deskclock/timer/Timers;->findTimer(Ljava/util/ArrayList;I)Lcom/android/deskclock/timer/TimerObj;

    move-result-object v3

    iget-wide v5, v3, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v7, v3, Lcom/android/deskclock/timer/TimerObj;->mStartTime:J

    sub-long v7, v1, v7

    sub-long/2addr v5, v7

    iput-wide v5, v3, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    invoke-direct {p0, v4}, Lcom/android/deskclock/timer/TimerFragment;->cancelTimerNotification(I)V

    :cond_1
    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "from_notification"

    invoke-interface {v0, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "from_alert"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "from_alert"

    invoke-interface {v0, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mCancel:Landroid/widget/Button;

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mSeperator:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iput-object v12, p0, Lcom/android/deskclock/timer/TimerFragment;->mLastVisibleView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/deskclock/timer/TimerFragment;->setPage()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v0, p1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mNewTimerPage:Landroid/view/View;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/deskclock/timer/TimerFragment;->saveViewState(Landroid/os/Bundle;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v8, -0x1

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "from_notification"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "timer_notif_id"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "timer_notif_time"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    const-string v5, "from_notification"

    invoke-interface {p1, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "timer_notif_time"

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v6

    invoke-interface {p1, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v5, "timer_notif_id"

    invoke-interface {p1, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v5, v6}, Lcom/android/deskclock/timer/TimerFragment;->createAdapter(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v10}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->onRestoreInstanceState(Landroid/os/Bundle;)V

    if-eq v4, v8, :cond_1

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v5, v5, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->mTimers:Ljava/util/ArrayList;

    invoke-static {v5, v4}, Lcom/android/deskclock/timer/Timers;->findTimer(Ljava/util/ArrayList;I)Lcom/android/deskclock/timer/TimerObj;

    move-result-object v3

    iget-wide v5, v3, Lcom/android/deskclock/timer/TimerObj;->mOriginalLength:J

    iget-wide v7, v3, Lcom/android/deskclock/timer/TimerObj;->mStartTime:J

    sub-long v7, v1, v7

    sub-long/2addr v5, v7

    iput-wide v5, v3, Lcom/android/deskclock/timer/TimerObj;->mTimeLeft:J

    invoke-direct {p0, v4}, Lcom/android/deskclock/timer/TimerFragment;->cancelTimerNotification(I)V

    :cond_1
    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "from_notification"

    invoke-interface {v0, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    const-string v5, "from_alert"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "from_alert"

    invoke-interface {p1, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "from_alert"

    invoke-interface {v0, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v5, v6}, Lcom/android/deskclock/timer/TimerFragment;->createAdapter(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v10}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_3
    return-void
.end method

.method public restartAdapter()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v0, v1}, Lcom/android/deskclock/timer/TimerFragment;->createAdapter(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public setLabel(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/deskclock/timer/TimerObj;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    iget v2, p1, Lcom/android/deskclock/timer/TimerObj;->mTimerId:I

    invoke-virtual {v1, v2}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->findTimerPositionById(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/timer/TimerObj;

    iput-object p2, v0, Lcom/android/deskclock/timer/TimerObj;->mLabel:Ljava/lang/String;

    iget v0, p1, Lcom/android/deskclock/timer/TimerObj;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/deskclock/timer/TimerReceiver;->showExpiredAlarmNotification(Landroid/content/Context;Lcom/android/deskclock/timer/TimerObj;)V

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimersList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    return-void
.end method

.method public setPage()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    const-string v2, "_setup_selected"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mTimerSetup:Lcom/android/deskclock/TimerSetupView;

    iget-object v2, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    const-string v3, "entry_state"

    invoke-virtual {v1, v2, v3}, Lcom/android/deskclock/TimerSetupView;->restoreEntryState(Landroid/os/Bundle;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mViewState:Landroid/os/Bundle;

    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->gotoSetupView()V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v1}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/deskclock/timer/TimerFragment;->gotoTimersView()V

    goto :goto_1
.end method

.method public stopAllTimesUpTimers()V
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_1

    move v1, v3

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_2

    iget-object v5, p0, Lcom/android/deskclock/timer/TimerFragment;->mAdapter:Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;

    invoke-virtual {v5, v4}, Lcom/android/deskclock/timer/TimerFragment$TimersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/deskclock/timer/TimerObj;

    iget v5, v2, Lcom/android/deskclock/timer/TimerObj;->mState:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    invoke-direct {p0, v2}, Lcom/android/deskclock/timer/TimerFragment;->onStopButtonPressed(Lcom/android/deskclock/timer/TimerObj;)V

    goto :goto_0

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/android/deskclock/timer/TimerFragment;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "from_alert"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    return-void
.end method
