.class Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;
.super Ljava/lang/Object;
.source "CountingTimerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/timer/CountingTimerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UnsignedTime"
.end annotation


# instance fields
.field protected mEm:F

.field protected mLabel:Ljava/lang/String;

.field private mLabelWidth:F

.field protected mPaint:Landroid/graphics/Paint;

.field private mWidest:Ljava/lang/String;

.field protected mWidth:F

.field final synthetic this$0:Lcom/android/deskclock/timer/CountingTimerView;


# direct methods
.method public constructor <init>(Lcom/android/deskclock/timer/CountingTimerView;Landroid/graphics/Paint;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Landroid/graphics/Paint;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->this$0:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    iput v4, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    iput-object p2, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    iput-object p3, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Locale digits missing - using English"

    invoke-static {v4}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    const-string p4, "0123456789"

    :cond_0
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v3, v4, [F

    iget-object v4, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, p4, v3}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    move-result v2

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_0
    if-ge v0, v2, :cond_2

    aget v4, v3, v0

    aget v5, v3, v1

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    move v1, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    aget v4, v3, v1

    iput v4, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p4, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidest:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/deskclock/timer/CountingTimerView;Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->this$0:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    iget-object v0, p2, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    iget v0, p2, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    iget v0, p2, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    iget-object v0, p2, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidest:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidest:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public calcTotalWidth(Ljava/lang/String;)F
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->updateWidth(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    iget v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    add-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->resetWidth()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;Ljava/lang/String;FFF)F
    .locals 6
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Ljava/lang/String;
    .param p3    # F
    .param p4    # F
    .param p5    # F

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->drawTime(Landroid/graphics/Canvas;Ljava/lang/String;IFF)F

    move-result p3

    iget-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->this$0:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-static {v1}, Lcom/android/deskclock/timer/CountingTimerView;->access$000(Lcom/android/deskclock/timer/CountingTimerView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, p3, p5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->getLabelWidth()F

    move-result v0

    add-float/2addr v0, p3

    return v0
.end method

.method protected drawTime(Landroid/graphics/Canvas;Ljava/lang/String;IFF)F
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # F
    .param p5    # F

    iget v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    const/high16 v2, 0x40000000

    div-float v0, v1, v2

    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p3, v1, :cond_0

    add-float/2addr p4, v0

    add-int/lit8 v1, p3, 0x1

    invoke-virtual {p2, p3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, p4, p5, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-float/2addr p4, v0

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    return p4
.end method

.method public getLabelWidth()F
    .locals 1

    iget v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    iget v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    return v0
.end method

.method protected resetWidth()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    return-void
.end method

.method protected updateWidth(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidest:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    iget-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabelWidth:F

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mEm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mWidth:F

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->this$0:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-static {v0}, Lcom/android/deskclock/timer/CountingTimerView;->access$000(Lcom/android/deskclock/timer/CountingTimerView;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lcom/android/deskclock/timer/CountingTimerView$UnsignedTime;->mLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method
