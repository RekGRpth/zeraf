.class public final Lcom/android/deskclock/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final actionbar_tab_padding:I = 0x7f090017

.field public static final actionbar_title_font_size:I = 0x7f090018

.field public static final alarm_alert_clock_padding_left:I = 0x7f09005f

.field public static final alarm_alert_display_width:I = 0x7f090043

.field public static final alarm_label_padding:I = 0x7f090011

.field public static final alarm_label_size:I = 0x7f090028

.field public static final alert_dialog_title_height:I = 0x7f090012

.field public static final ampm_margin_top:I = 0x7f09000e

.field public static final ampm_text_size:I = 0x7f090004

.field public static final analog_clock_diameter:I = 0x7f090014

.field public static final analog_clock_margin:I = 0x7f09003b

.field public static final big_font_size:I = 0x7f09001c

.field public static final body_font_padding:I = 0x7f090024

.field public static final body_font_size:I = 0x7f090023

.field public static final button_font_size:I = 0x7f090025

.field public static final button_footer_height:I = 0x7f09004c

.field public static final button_footer_height_neg:I = 0x7f09004d

.field public static final circle_margin:I = 0x7f090039

.field public static final circle_margin_top:I = 0x7f090038

.field public static final circletimer_circle_size:I = 0x7f09001a

.field public static final circletimer_diamond_size:I = 0x7f090019

.field public static final circletimer_marker_size:I = 0x7f09001b

.field public static final cities_list_item_height:I = 0x7f090037

.field public static final city_name_font_size:I = 0x7f090033

.field public static final city_time_font_size:I = 0x7f090034

.field public static final date_text_size:I = 0x7f090005

.field public static final def_digital_widget_height:I = 0x7f09005b

.field public static final def_digital_widget_width:I = 0x7f09005a

.field public static final delete_button_padding:I = 0x7f090048

.field public static final dialog_button_font_size:I = 0x7f090027

.field public static final dialpad_font_size:I = 0x7f090031

.field public static final dialpad_font_size_ampm:I = 0x7f090032

.field public static final digital_margin_bottom:I = 0x7f09000b

.field public static final digital_widget_list_margin_top:I = 0x7f09005d

.field public static final digital_widget_list_min_height:I = 0x7f09005c

.field public static final font_margin_adjust:I = 0x7f09000f

.field public static final glowpadview_glow_radius:I = 0x7f090051

.field public static final glowpadview_inner_radius:I = 0x7f090053

.field public static final glowpadview_margin_bottom:I = 0x7f090054

.field public static final glowpadview_margin_right:I = 0x7f090055

.field public static final glowpadview_outerring_diameter:I = 0x7f09004f

.field public static final glowpadview_snap_margin:I = 0x7f090052

.field public static final glowpadview_target_placement_radius:I = 0x7f090050

.field public static final header_font_size:I = 0x7f090022

.field public static final indicator_margin_top:I = 0x7f09005e

.field public static final label_font_size:I = 0x7f090021

.field public static final label_margin_big:I = 0x7f09002f

.field public static final label_margin_small:I = 0x7f090030

.field public static final lap_button_padding:I = 0x7f090049

.field public static final laps_margin:I = 0x7f09003a

.field public static final medium_font_padding:I = 0x7f090029

.field public static final medium_font_size:I = 0x7f09001f

.field public static final medium_space_bottom:I = 0x7f09002c

.field public static final medium_space_top:I = 0x7f09002b

.field public static final min_digital_widget_height:I = 0x7f090057

.field public static final min_digital_widget_resize_height:I = 0x7f090059

.field public static final min_digital_widget_resize_width:I = 0x7f090058

.field public static final min_digital_widget_width:I = 0x7f090056

.field public static final min_lock:I = 0x7f090002

.field public static final min_swipe:I = 0x7f090000

.field public static final min_vert:I = 0x7f090001

.field public static final next_alarm_margin_top:I = 0x7f090010

.field public static final next_alarm_text_size:I = 0x7f090006

.field public static final notification_large_icon_height:I = 0x7f09003f

.field public static final notification_large_icon_width:I = 0x7f09003e

.field public static final notification_subtext_size:I = 0x7f090042

.field public static final notification_text_size:I = 0x7f090040

.field public static final notification_title_text_size:I = 0x7f090041

.field public static final plusone_reset_button_padding:I = 0x7f090047

.field public static final popup_window_width:I = 0x7f090013

.field public static final screensaver_margin:I = 0x7f09000d

.field public static final share_button_padding:I = 0x7f09004a

.field public static final small_font_size:I = 0x7f09001e

.field public static final small_space:I = 0x7f09002a

.field public static final style_label_space:I = 0x7f09002d

.field public static final sw_item_space:I = 0x7f09002e

.field public static final sw_right_margin:I = 0x7f09003d

.field public static final tablet_dialpad_font_size:I = 0x7f090035

.field public static final tablet_dialpad_font_size_ampm:I = 0x7f090036

.field public static final time_margin_bottom:I = 0x7f090009

.field public static final time_margin_left:I = 0x7f090008

.field public static final time_margin_right:I = 0x7f090007

.field public static final time_margin_top:I = 0x7f09000a

.field public static final time_text_size:I = 0x7f090003

.field public static final timer_button_extra_offset:I = 0x7f09004b

.field public static final timer_circle_diameter:I = 0x7f090015

.field public static final timer_circle_margin:I = 0x7f09003c

.field public static final timer_circle_width:I = 0x7f090016

.field public static final timer_divider_height:I = 0x7f09004e

.field public static final timer_label_font_size:I = 0x7f090026

.field public static final timer_padding_left:I = 0x7f09000c

.field public static final toast_bar_bottom_margin_in_conversation:I = 0x7f090046

.field public static final widget_big_font_size:I = 0x7f09001d

.field public static final widget_medium_font_size:I = 0x7f090020

.field public static final world_clock_analog_size:I = 0x7f090044

.field public static final world_clock_margin:I = 0x7f090045


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
