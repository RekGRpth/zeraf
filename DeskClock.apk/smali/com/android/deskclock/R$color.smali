.class public final Lcom/android/deskclock/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_button_text_color:I = 0x7f0b0006

.field public static final alarm_selected_color:I = 0x7f0b0015

.field public static final alarm_whiteish:I = 0x7f0b000f

.field public static final ampm_off:I = 0x7f0b0000

.field public static final ampm_on:I = 0x7f0b0001

.field public static final ampm_text_color:I = 0x7f0b0003

.field public static final black:I = 0x7f0b0007

.field public static final blackish:I = 0x7f0b0008

.field public static final blackish_trans:I = 0x7f0b0009

.field public static final clock_blue:I = 0x7f0b0013

.field public static final clock_gray:I = 0x7f0b0012

.field public static final clock_red:I = 0x7f0b0010

.field public static final clock_white:I = 0x7f0b0011

.field public static final grey:I = 0x7f0b000c

.field public static final notification_bg:I = 0x7f0b0014

.field public static final red:I = 0x7f0b000b

.field public static final screen_saver_color:I = 0x7f0b0004

.field public static final screen_saver_dim_color:I = 0x7f0b0005

.field public static final time_text_color:I = 0x7f0b0002

.field public static final transparent:I = 0x7f0b000d

.field public static final transparent_white:I = 0x7f0b000e

.field public static final white:I = 0x7f0b000a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
