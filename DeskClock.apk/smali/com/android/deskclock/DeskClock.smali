.class public Lcom/android/deskclock/DeskClock;
.super Landroid/app/Activity;
.source "DeskClock.java"

# interfaces
.implements Lcom/android/deskclock/LabelDialogFragment$TimerLabelDialogHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/DeskClock$OnTapListener;,
        Lcom/android/deskclock/DeskClock$TabsAdapter;
    }
.end annotation


# static fields
.field public static final CLOCK_TAB_INDEX:I = 0x1

.field private static final DEBUG:Z = false

.field private static final KEY_CLOCK_STATE:Ljava/lang/String; = "clock_state"

.field private static final KEY_SELECTED_TAB:Ljava/lang/String; = "selected_tab"

.field private static final LOG_TAG:Ljava/lang/String; = "DeskClock"

.field public static final SELECT_TAB_INTENT_EXTRA:Ljava/lang/String; = "deskclock.select.tab"

.field public static final STOPWATCH_TAB_INDEX:I = 0x2

.field public static final TIMER_TAB_INDEX:I


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mClockTab:Landroid/app/ActionBar$Tab;

.field private mSelectedTab:I

.field private mStopwatchTab:Landroid/app/ActionBar$Tab;

.field private mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

.field private mTimerTab:Landroid/app/ActionBar$Tab;

.field private mViewPager:Lvedroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private createTabs(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mTimerTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTimerTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f020079

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTimerTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0d0050

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mTimerTab:Landroid/app/ActionBar$Tab;

    const-class v2, Lcom/android/deskclock/timer/TimerFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/deskclock/DeskClock$TabsAdapter;->addTab(Landroid/app/ActionBar$Tab;Ljava/lang/Class;I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mClockTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mClockTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f020024

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mClockTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0d0051

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mClockTab:Landroid/app/ActionBar$Tab;

    const-class v2, Lcom/android/deskclock/ClockFragment;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/deskclock/DeskClock$TabsAdapter;->addTab(Landroid/app/ActionBar$Tab;Ljava/lang/Class;I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mStopwatchTab:Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mStopwatchTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f020078

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setIcon(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mStopwatchTab:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0d0052

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mStopwatchTab:Landroid/app/ActionBar$Tab;

    const-class v2, Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/deskclock/DeskClock$TabsAdapter;->addTab(Landroid/app/ActionBar$Tab;Ljava/lang/Class;I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    invoke-virtual {v0, p1}, Lcom/android/deskclock/DeskClock$TabsAdapter;->notifySelectedPage(I)V

    :cond_0
    return-void
.end method

.method private initViews()V
    .locals 2

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0}, Lvedroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    const v1, 0x7f0e0038

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    new-instance v0, Lcom/android/deskclock/DeskClock$TabsAdapter;

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0, p0, v1}, Lcom/android/deskclock/DeskClock$TabsAdapter;-><init>(Lcom/android/deskclock/DeskClock;Landroid/app/Activity;Lvedroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    iget v0, p0, Lcom/android/deskclock/DeskClock;->mSelectedTab:I

    invoke-direct {p0, v0}, Lcom/android/deskclock/DeskClock;->createTabs(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    iget v1, p0, Lcom/android/deskclock/DeskClock;->mSelectedTab:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    return-void
.end method

.method private setHomeTimeZone()V
    .locals 6

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "home_time_zone"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "home_time_zone"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v3, "DeskClock"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting home time zone to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showMenu(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v2, Landroid/widget/PopupMenu;

    invoke-direct {v2, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-instance v3, Lcom/android/deskclock/DeskClock$1;

    invoke-direct {v3, p0}, Lcom/android/deskclock/DeskClock$1;-><init>(Lcom/android/deskclock/DeskClock;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    const v3, 0x7f110004

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v3, 0x7f0e00a8

    invoke-interface {v1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/android/deskclock/Utils;->prepareHelpMenuItem(Landroid/content/Context;Landroid/view/MenuItem;)V

    :cond_0
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method


# virtual methods
.method public clockButtonsOnClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/deskclock/AlarmClock;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/deskclock/DeskClock;->showMenu(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0033
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v3, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iput v4, p0, Lcom/android/deskclock/DeskClock;->mSelectedTab:I

    if-eqz p1, :cond_0

    const-string v2, "selected_tab"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/deskclock/DeskClock;->mSelectedTab:I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "deskclock.select.tab"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v3, :cond_1

    iput v1, p0, Lcom/android/deskclock/DeskClock;->mSelectedTab:I

    :cond_1
    invoke-direct {p0}, Lcom/android/deskclock/DeskClock;->initViews()V

    invoke-direct {p0}, Lcom/android/deskclock/DeskClock;->setHomeTimeZone()V

    return-void
.end method

.method public onDialogLabelSet(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/deskclock/timer/TimerObj;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/android/deskclock/timer/TimerFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/deskclock/timer/TimerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/android/deskclock/timer/TimerFragment;->setLabel(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    const-string v1, "deskclock.select.tab"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/android/deskclock/stopwatch/StopwatchService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "show_notification"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "notif_app_open"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {p0}, Lcom/android/deskclock/Utils;->showInUseNotifications(Landroid/content/Context;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 6

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/android/deskclock/stopwatch/StopwatchService;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "kill_notification"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "notif_app_open"

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "notif_in_use_cancel"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_tab"

    iget-object v1, p0, Lcom/android/deskclock/DeskClock;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public registerPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V
    .locals 1
    .param p1    # Lcom/android/deskclock/DeskClockFragment;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    invoke-virtual {v0, p1}, Lcom/android/deskclock/DeskClock$TabsAdapter;->registerPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    :cond_0
    return-void
.end method

.method public unregisterPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V
    .locals 1
    .param p1    # Lcom/android/deskclock/DeskClockFragment;

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/DeskClock;->mTabsAdapter:Lcom/android/deskclock/DeskClock$TabsAdapter;

    invoke-virtual {v0, p1}, Lcom/android/deskclock/DeskClock$TabsAdapter;->unregisterPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    :cond_0
    return-void
.end method
