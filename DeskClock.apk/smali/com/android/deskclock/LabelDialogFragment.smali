.class public Lcom/android/deskclock/LabelDialogFragment;
.super Landroid/app/DialogFragment;
.source "LabelDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/LabelDialogFragment$TimerLabelDialogHandler;,
        Lcom/android/deskclock/LabelDialogFragment$AlarmLabelDialogHandler;
    }
.end annotation


# static fields
.field private static final KEY_ALARM:Ljava/lang/String; = "alarm"

.field private static final KEY_LABEL:Ljava/lang/String; = "label"

.field private static final KEY_TAG:Ljava/lang/String; = "tag"

.field private static final KEY_TIMER:Ljava/lang/String; = "timer"


# instance fields
.field private mLabelBox:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/LabelDialogFragment;Lcom/android/deskclock/Alarm;Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/LabelDialogFragment;
    .param p1    # Lcom/android/deskclock/Alarm;
    .param p2    # Lcom/android/deskclock/timer/TimerObj;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/deskclock/LabelDialogFragment;->set(Lcom/android/deskclock/Alarm;Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    return-void
.end method

.method public static newInstance(Lcom/android/deskclock/Alarm;Ljava/lang/String;)Lcom/android/deskclock/LabelDialogFragment;
    .locals 3
    .param p0    # Lcom/android/deskclock/Alarm;
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/android/deskclock/LabelDialogFragment;

    invoke-direct {v1}, Lcom/android/deskclock/LabelDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "label"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "alarm"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static newInstance(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;Ljava/lang/String;)Lcom/android/deskclock/LabelDialogFragment;
    .locals 3
    .param p0    # Lcom/android/deskclock/timer/TimerObj;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Lcom/android/deskclock/LabelDialogFragment;

    invoke-direct {v1}, Lcom/android/deskclock/LabelDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "label"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "timer"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "tag"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private set(Lcom/android/deskclock/Alarm;Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/deskclock/Alarm;
    .param p2    # Lcom/android/deskclock/timer/TimerObj;
    .param p3    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/deskclock/LabelDialogFragment;->mLabelBox:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    const-string v1, ""

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v2, v0, Lcom/android/deskclock/LabelDialogFragment$AlarmLabelDialogHandler;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/deskclock/AlarmClock;

    invoke-virtual {v2, p1, v1}, Lcom/android/deskclock/AlarmClock;->onDialogLabelSet(Lcom/android/deskclock/Alarm;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/DialogFragment;->dismiss()V

    return-void

    :cond_1
    instance-of v2, v0, Lcom/android/deskclock/LabelDialogFragment$TimerLabelDialogHandler;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/deskclock/DeskClock;

    invoke-virtual {v2, p2, v1, p3}, Lcom/android/deskclock/DeskClock;->onDialogLabelSet(Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "Error! Activities that use LabelDialogFragment must implement AlarmLabelDialogHandler or TimerLabelDialogHandler"

    invoke-static {v2}, Lcom/android/deskclock/Log;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/DialogFragment;->setStyle(II)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v8, "label"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "alarm"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/Alarm;

    const-string v8, "timer"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/android/deskclock/timer/TimerObj;

    const-string v8, "tag"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v8, 0x7f040019

    const/4 v9, 0x0

    invoke-virtual {p1, v8, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0e004b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/android/deskclock/LabelDialogFragment;->mLabelBox:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/android/deskclock/LabelDialogFragment;->mLabelBox:Landroid/widget/EditText;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/deskclock/LabelDialogFragment;->mLabelBox:Landroid/widget/EditText;

    new-instance v9, Lcom/android/deskclock/LabelDialogFragment$1;

    invoke-direct {v9, p0, v0, v6, v5}, Lcom/android/deskclock/LabelDialogFragment$1;-><init>(Lcom/android/deskclock/LabelDialogFragment;Lcom/android/deskclock/Alarm;Lcom/android/deskclock/timer/TimerObj;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v8, 0x7f0e004c

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    new-instance v8, Lcom/android/deskclock/LabelDialogFragment$2;

    invoke-direct {v8, p0}, Lcom/android/deskclock/LabelDialogFragment$2;-><init>(Lcom/android/deskclock/LabelDialogFragment;)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f0e004d

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    new-instance v8, Lcom/android/deskclock/LabelDialogFragment$3;

    invoke-direct {v8, p0, v6, v0, v5}, Lcom/android/deskclock/LabelDialogFragment$3;-><init>(Lcom/android/deskclock/LabelDialogFragment;Lcom/android/deskclock/timer/TimerObj;Lcom/android/deskclock/Alarm;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v7
.end method
