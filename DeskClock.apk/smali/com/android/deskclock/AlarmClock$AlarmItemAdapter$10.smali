.class Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;
.super Ljava/lang/Object;
.source "AlarmClock.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->bindExpandArea(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;Lcom/android/deskclock/Alarm;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

.field final synthetic val$alarm:Lcom/android/deskclock/Alarm;

.field final synthetic val$buttonIndex:I

.field final synthetic val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;


# direct methods
.method constructor <init>(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;ILcom/android/deskclock/Alarm;)V
    .locals 0

    iput-object p1, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    iput-object p2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iput p3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    iput-object p4, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    invoke-static {v2, p1}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$1000(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget-object v2, v2, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;->dayButtons:[Landroid/widget/ToggleButton;

    iget v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->toggle()V

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget-object v2, v2, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;->dayButtons:[Landroid/widget/ToggleButton;

    iget v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    invoke-static {v2}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$1800(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;)[I

    move-result-object v2

    iget v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    aget v1, v2, v3

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    iget-object v2, v2, Lcom/android/deskclock/Alarm;->daysOfWeek:Lcom/android/deskclock/Alarm$DaysOfWeek;

    invoke-virtual {v2, v1, v0}, Lcom/android/deskclock/Alarm$DaysOfWeek;->setDayOfWeek(IZ)V

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    iget-object v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget v4, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    invoke-static {v2, v3, v4}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$2000(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;I)V

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    iget-object v2, v2, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->this$0:Lcom/android/deskclock/AlarmClock;

    iget-object v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    invoke-static {v2, v3, v5}, Lcom/android/deskclock/AlarmClock;->access$1200(Lcom/android/deskclock/AlarmClock;Lcom/android/deskclock/Alarm;Z)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    iget-object v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget v4, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$buttonIndex:I

    invoke-static {v2, v3, v4}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$2100(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;I)V

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    iget-object v2, v2, Lcom/android/deskclock/Alarm;->daysOfWeek:Lcom/android/deskclock/Alarm$DaysOfWeek;

    invoke-virtual {v2}, Lcom/android/deskclock/Alarm$DaysOfWeek;->getCoded()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget-object v2, v2, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;->repeatDays:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$itemHolder:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;

    iget-object v2, v2, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$ItemHolder;->repeat:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    invoke-static {v3}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$2200(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    invoke-static {v2}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$1600(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    iget v3, v3, Lcom/android/deskclock/Alarm;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->this$1:Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;

    invoke-static {v2}, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;->access$1700(Lcom/android/deskclock/AlarmClock$AlarmItemAdapter;)Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/deskclock/AlarmClock$AlarmItemAdapter$10;->val$alarm:Lcom/android/deskclock/Alarm;

    iget v4, v4, Lcom/android/deskclock/Alarm;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method
