.class public Lcom/android/deskclock/AlarmTimePickerDialogFragment;
.super Landroid/app/DialogFragment;
.source "AlarmTimePickerDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/AlarmTimePickerDialogFragment$AlarmTimePickerDialogHandler;
    }
.end annotation


# static fields
.field private static final KEY_ALARM:Ljava/lang/String; = "alarm"


# instance fields
.field private mCancel:Landroid/widget/Button;

.field private mPicker:Lcom/android/deskclock/TimePicker;

.field private mSet:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/AlarmTimePickerDialogFragment;)Lcom/android/deskclock/TimePicker;
    .locals 1
    .param p0    # Lcom/android/deskclock/AlarmTimePickerDialogFragment;

    iget-object v0, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mPicker:Lcom/android/deskclock/TimePicker;

    return-object v0
.end method

.method public static newInstance(Lcom/android/deskclock/Alarm;)Lcom/android/deskclock/AlarmTimePickerDialogFragment;
    .locals 3
    .param p0    # Lcom/android/deskclock/Alarm;

    new-instance v1, Lcom/android/deskclock/AlarmTimePickerDialogFragment;

    invoke-direct {v1}, Lcom/android/deskclock/AlarmTimePickerDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "alarm"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/DialogFragment;->setStyle(II)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "alarm"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/Alarm;

    const v2, 0x7f040026

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0e0074

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mSet:Landroid/widget/Button;

    const v2, 0x7f0e0073

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mCancel:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mCancel:Landroid/widget/Button;

    new-instance v3, Lcom/android/deskclock/AlarmTimePickerDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/android/deskclock/AlarmTimePickerDialogFragment$1;-><init>(Lcom/android/deskclock/AlarmTimePickerDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0e0072

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/deskclock/TimePicker;

    iput-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mPicker:Lcom/android/deskclock/TimePicker;

    iget-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mPicker:Lcom/android/deskclock/TimePicker;

    iget-object v3, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mSet:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Lcom/android/deskclock/TimePicker;->setSetButton(Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/android/deskclock/AlarmTimePickerDialogFragment;->mSet:Landroid/widget/Button;

    new-instance v3, Lcom/android/deskclock/AlarmTimePickerDialogFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/android/deskclock/AlarmTimePickerDialogFragment$2;-><init>(Lcom/android/deskclock/AlarmTimePickerDialogFragment;Lcom/android/deskclock/Alarm;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
