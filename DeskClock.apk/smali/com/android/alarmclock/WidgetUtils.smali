.class public Lcom/android/alarmclock/WidgetUtils;
.super Ljava/lang/Object;
.source "WidgetUtils.java"


# static fields
.field static final TAG:Ljava/lang/String; = "WidgetUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHeightScaleRatio(Landroid/content/Context;Landroid/os/Bundle;I)F
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/os/Bundle;
    .param p2    # I

    const/high16 v4, 0x3f800000

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    if-nez p1, :cond_0

    invoke-virtual {v3, p2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    const-string v5, "appWidgetMinHeight"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return v4

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    int-to-float v5, v0

    const v6, 0x7f09005b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    div-float v1, v5, v6

    cmpl-float v5, v1, v4

    if-lez v5, :cond_3

    move v1, v4

    :cond_3
    move v4, v1

    goto :goto_0
.end method

.method public static getScaleRatio(Landroid/content/Context;Landroid/os/Bundle;I)F
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/os/Bundle;
    .param p2    # I

    const/high16 v4, 0x3f800000

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    if-nez p1, :cond_0

    invoke-virtual {v3, p2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    const-string v5, "appWidgetMinWidth"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return v4

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    int-to-float v5, v0

    const v6, 0x7f09005a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    div-float v1, v5, v6

    cmpl-float v5, v1, v4

    if-lez v5, :cond_3

    move v1, v4

    :cond_3
    move v4, v1

    goto :goto_0
.end method

.method public static setClockSize(Landroid/content/Context;Landroid/widget/RemoteViews;F)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # F

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const v1, 0x7f0e0049

    mul-float v2, v0, p2

    invoke-virtual {p1, v1, v3, v2}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    const v1, 0x7f0e004a

    mul-float v2, v0, p2

    invoke-virtual {p1, v1, v3, v2}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    return-void
.end method

.method public static showList(Landroid/content/Context;IF)Z
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # F

    const/4 v3, 0x1

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-string v4, "appWidgetMinHeight"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    if-gt v0, v1, :cond_0

    const/4 v3, 0x0

    goto :goto_0
.end method
