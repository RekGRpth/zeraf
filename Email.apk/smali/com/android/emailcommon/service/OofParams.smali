.class public Lcom/android/emailcommon/service/OofParams;
.super Ljava/lang/Object;
.source "OofParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/emailcommon/service/OofParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mEndTimeInMillis:J

.field public mIsExternal:I

.field public mOofState:I

.field public mReplyMessage:Ljava/lang/String;

.field public mStartTimeInMillis:J

.field public mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/emailcommon/service/OofParams$1;

    invoke-direct {v0}, Lcom/android/emailcommon/service/OofParams$1;-><init>()V

    sput-object v0, Lcom/android/emailcommon/service/OofParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIJJILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # J
    .param p5    # J
    .param p7    # I
    .param p8    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    iput p1, p0, Lcom/android/emailcommon/service/OofParams;->mStatus:I

    iput p2, p0, Lcom/android/emailcommon/service/OofParams;->mOofState:I

    iput-wide p3, p0, Lcom/android/emailcommon/service/OofParams;->mStartTimeInMillis:J

    iput-wide p5, p0, Lcom/android/emailcommon/service/OofParams;->mEndTimeInMillis:J

    iput p7, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    iput-object p8, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/OofParams;->mStatus:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/OofParams;->mOofState:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mStartTimeInMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mEndTimeInMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getEndTimeInMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mEndTimeInMillis:J

    return-wide v0
.end method

.method public getIsExternal()I
    .locals 1

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    return v0
.end method

.method public getOofState()I
    .locals 1

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mOofState:I

    return v0
.end method

.method public getReplyMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimeInMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mStartTimeInMillis:J

    return-wide v0
.end method

.method public getmStatus()I
    .locals 1

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mStatus:I

    return v0
.end method

.method public setEndTimeInMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/emailcommon/service/OofParams;->mEndTimeInMillis:J

    return-void
.end method

.method public setIsExternal(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    return-void
.end method

.method public setOofState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/emailcommon/service/OofParams;->mOofState:I

    return-void
.end method

.method public setReplyMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    return-void
.end method

.method public setStartTimeInMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/emailcommon/service/OofParams;->mStartTimeInMillis:J

    return-void
.end method

.method public setmStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/emailcommon/service/OofParams;->mStatus:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mOofState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mStartTimeInMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/emailcommon/service/OofParams;->mEndTimeInMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/android/emailcommon/service/OofParams;->mReplyMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/emailcommon/service/OofParams;->mIsExternal:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
