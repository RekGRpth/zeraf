.class public Lcom/android/email/Email;
.super Landroid/app/Application;
.source "Email.java"


# static fields
.field public static final ATTACHMENT_MAX_NUMBER:I = 0x64

.field public static DEBUG:Z = false

.field public static DEBUG_EXCHANGE:Z = false

.field public static DEBUG_EXCHANGE_FILE:Z = false

.field public static DEBUG_EXCHANGE_VERBOSE:Z = false

.field public static final EDITVIEW_MAX_LENGTH_1:I = 0x100

.field public static final EDITVIEW_MAX_LENGTH_2:I = 0x3e8

.field public static final EDITVIEW_MAX_LENGTH_3:I = 0x1388

.field public static final EDITVIEW_MAX_LENGTH_4:I = 0x2710

.field public static final RECIPIENT_MAX_NUMBER:I = 0xfa

.field public static final TAG:Ljava/lang/String; = "SpendTime"

.field public static final VISIBLE_LIMIT_DEFAULT:I = 0x19

.field public static final VISIBLE_LIMIT_INCREMENT:I = 0x19

.field private static sAccountsChangedNotification:Z

.field public static sDebugInhibitGraphicsAcceleration:Z

.field private static sMessageDecodeErrorString:Ljava/lang/String;

.field private static sSetupAccountFinished:Z

.field private static sStartComposeAfterSetupAccount:Z

.field private static sStartComposeIntent:Landroid/content/Intent;

.field private static sUiThread:Ljava/lang/Thread;


# instance fields
.field private mTriedSetEncryption:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/email/Email;->sDebugInhibitGraphicsAcceleration:Z

    sput-boolean v0, Lcom/android/email/Email;->sAccountsChangedNotification:Z

    sput-boolean v0, Lcom/android/email/Email;->sStartComposeAfterSetupAccount:Z

    sput-boolean v0, Lcom/android/email/Email;->sSetupAccountFinished:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/android/email/Email;->sStartComposeIntent:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/Email;->mTriedSetEncryption:Z

    return-void
.end method

.method public static enableStrictMode(Z)V
    .locals 0
    .param p0    # Z

    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->enableStrictMode(Z)V

    return-void
.end method

.method public static getMessageDecodeErrorString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/Email;->sMessageDecodeErrorString:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/email/Email;->sMessageDecodeErrorString:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static declared-synchronized getNotifyUiAccountsChanged()Z
    .locals 2

    const-class v0, Lcom/android/email/Email;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/android/email/Email;->sAccountsChangedNotification:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getSetupAccountFinishedFlag()Z
    .locals 1

    sget-boolean v0, Lcom/android/email/Email;->sSetupAccountFinished:Z

    return v0
.end method

.method public static getStartComposeAfterSetupAccountFlag()Z
    .locals 1

    sget-boolean v0, Lcom/android/email/Email;->sStartComposeAfterSetupAccount:Z

    return v0
.end method

.method public static getStartComposeIntent()Landroid/content/Intent;
    .locals 1

    sget-object v0, Lcom/android/email/Email;->sStartComposeIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "Email"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static declared-synchronized setNotifyUiAccountsChanged(Z)V
    .locals 2
    .param p0    # Z

    const-class v0, Lcom/android/email/Email;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/android/email/Email;->sAccountsChangedNotification:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static setServicesEnabled(Landroid/content/Context;Z)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance v1, Landroid/content/ComponentName;

    const-class v4, Lcom/android/email/service/MailService;

    invoke-direct {v1, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-static {p0}, Lcom/android/email/service/MailService;->actionReschedule(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/email/service/MailService;->actionCancelClearOldAttachment(Landroid/content/Context;)V

    :cond_0
    new-instance v4, Landroid/content/ComponentName;

    const-class v1, Lcom/android/email/activity/ShortcutPicker;

    invoke-direct {v4, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v0, v4, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v4, Landroid/content/ComponentName;

    const-class v1, Lcom/android/email/service/MailService;

    invoke-direct {v4, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v0, v4, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v1, Landroid/content/ComponentName;

    const-class v4, Lcom/android/email/service/AttachmentDownloadService;

    invoke-direct {v1, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_1

    move v3, v2

    :cond_1
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    if-eqz p1, :cond_2

    new-instance v1, Landroid/content/ComponentName;

    const-class v3, Lcom/android/email/service/MailService;

    invoke-direct {v1, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    if-ne v1, v2, :cond_2

    invoke-static {p0}, Lcom/android/email/service/MailService;->actionReschedule(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/email/service/MailService;->actionRescheduleClearOldAttachment(Landroid/content/Context;)V

    :cond_2
    new-instance v1, Landroid/content/ComponentName;

    const-class v3, Lcom/android/email/widget/WidgetConfiguration;

    invoke-direct {v1, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/email/service/AttachmentDownloadService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, p0, v1}, Lcom/android/email/Email;->startOrStopService(ZLandroid/content/Context;Landroid/content/Intent;)V

    invoke-static {p0}, Lcom/android/email/NotificationController;->getInstance(Landroid/content/Context;)Lcom/android/email/NotificationController;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/email/NotificationController;->watchForMessages(Z)V

    return-void

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method public static setServicesEnabledAsync(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    new-instance v0, Lcom/android/email/Email$1;

    invoke-direct {v0, p0}, Lcom/android/email/Email$1;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public static setServicesEnabledSync(Landroid/content/Context;)Z
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/Account;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v7, 0x1

    :goto_0
    invoke-static {p0, v7}, Lcom/android/email/Email;->setServicesEnabled(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v7

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static setSetupAccountFinishedFlag(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/email/Email;->sSetupAccountFinished:Z

    return-void
.end method

.method public static setStartComposeAfterSetupAccountFlag(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/email/Email;->sStartComposeAfterSetupAccount:Z

    return-void
.end method

.method public static setStartComposeIntent(Landroid/content/Intent;)V
    .locals 0
    .param p0    # Landroid/content/Intent;

    sput-object p0, Lcom/android/email/Email;->sStartComposeIntent:Landroid/content/Intent;

    return-void
.end method

.method private static startOrStopService(ZLandroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Z
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    if-eqz p0, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public static updateLoggingFlags(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/email/Preferences;->getEnableDebugLogging()Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v4}, Lcom/android/email/Preferences;->getEnableExchangeLogging()Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v5, 0x2

    :goto_1
    invoke-virtual {v4}, Lcom/android/email/Preferences;->getEnableExchangeFileLogging()Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v3, 0x4

    :goto_2
    invoke-virtual {v4}, Lcom/android/email/Preferences;->getEnableStrictMode()Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v2, 0x8

    :goto_3
    or-int v6, v1, v5

    or-int/2addr v6, v3

    or-int v0, v6, v2

    invoke-static {p0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/android/email/Controller;->serviceLogging(I)V

    return-void

    :cond_0
    move v1, v6

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_1

    :cond_2
    move v3, v6

    goto :goto_2

    :cond_3
    move v2, v6

    goto :goto_3
.end method

.method private updateUnreadMail(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/android/email/Email$2;

    invoke-direct {v0, p0, p1}, Lcom/android/email/Email$2;-><init>(Lcom/android/email/Email;Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public static warnIfUiThread()V
    .locals 4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/android/email/Email;->sUiThread:Ljava/lang/Thread;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Email"

    const-string v1, "Method called on the UI thread"

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "STACK TRACE"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public isTriedSetEncryption()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/Email;->mTriedSetEncryption:Z

    return v0
.end method

.method public onCreate()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "SpendTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EmailApp onCreate() start time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sput-object v3, Lcom/android/email/Email;->sUiThread:Ljava/lang/Thread;

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v4, "eng"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v6}, Landroid/app/FragmentManager;->enableDebugLogging(Z)V

    invoke-virtual {v0, v6}, Lcom/android/email/Preferences;->setEnableDebugLogging(Z)V

    invoke-virtual {v0, v7}, Lcom/android/email/Preferences;->setEnableExchangeLogging(Z)V

    invoke-virtual {v0, v7}, Lcom/android/email/Preferences;->setEnableExchangeFileLogging(Z)V

    invoke-virtual {v0, v6}, Lcom/android/email/Preferences;->setEnableStrictMode(Z)V

    :cond_0
    invoke-virtual {v0, v6}, Lcom/android/email/Preferences;->setInhibitGraphicsAcceleration(Z)V

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getEnableDebugLogging()Z

    move-result v3

    sput-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getEnableDebugLogging()Z

    move-result v3

    sput-boolean v3, Lcom/android/email/Email;->DEBUG_EXCHANGE:Z

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getEnableExchangeLogging()Z

    move-result v3

    sput-boolean v3, Lcom/android/email/Email;->DEBUG_EXCHANGE_VERBOSE:Z

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getEnableExchangeFileLogging()Z

    move-result v3

    sput-boolean v3, Lcom/android/email/Email;->DEBUG_EXCHANGE_FILE:Z

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getInhibitGraphicsAcceleration()Z

    move-result v3

    sput-boolean v3, Lcom/android/email/Email;->sDebugInhibitGraphicsAcceleration:Z

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getEnableStrictMode()Z

    move-result v3

    invoke-static {v3}, Lcom/android/email/Email;->enableStrictMode(Z)V

    invoke-static {p0}, Lcom/android/emailcommon/TempDirectory;->setTempDirectory(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    invoke-static {p0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/email/Controller;->resetVisibleLimits()V

    invoke-static {p0}, Lcom/android/email/Email;->updateLoggingFlags(Landroid/content/Context;)V

    const v3, 0x7f080107

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/android/email/Email;->sMessageDecodeErrorString:Ljava/lang/String;

    invoke-static {p0}, Lcom/android/email/Email;->setServicesEnabledAsync(Landroid/content/Context;)V

    const-string v3, "updateAllEmailWidgets"

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/email/widget/WidgetManager;->getInstance()Lcom/android/email/widget/WidgetManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/android/email/widget/WidgetManager;->updateAllEmailWidgets(Landroid/content/Context;)V

    invoke-direct {p0, p0}, Lcom/android/email/Email;->updateUnreadMail(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/mediatek/email/emailvip/VipMemberCache;->init(Landroid/content/Context;)V

    const-string v3, "SpendTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EmailApp onCreate() end time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "SpendTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EmailApp onCreate() spend time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTriedSetEncryptionFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/Email;->mTriedSetEncryption:Z

    return-void
.end method
