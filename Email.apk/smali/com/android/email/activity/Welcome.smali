.class public Lcom/android/email/activity/Welcome;
.super Landroid/app/Activity;
.source "Welcome.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/Welcome$MyConnectionAlertDialog;
    }
.end annotation


# static fields
.field private static final EXTRA_DEBUG_PANE_MODE:Ljava/lang/String; = "DEBUG_PANE_MODE"

.field private static final EXTRA_FROM_ACCOUNT_MANAGER:Ljava/lang/String; = "FROM_ACCOUNT_MANAGER"

.field private static final MAILBOX_FINDER_EXECUTE_DELAY:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "Welcome"

.field public static final VIEW_MAILBOX_INTENT_URL_PATH:Ljava/lang/String; = "/view/mailbox"


# instance fields
.field private mAccountId:J

.field private mAccountUuid:Ljava/lang/String;

.field private mContext:Lcom/android/email/activity/Welcome;

.field private mFindInboxAndFinish:Z

.field private mInboxFinder:Lcom/android/email/activity/MailboxFinder;

.field private mIsResumed:Z

.field private final mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

.field private mMailboxFinderDelayTime:I

.field private mMailboxId:J

.field private mMessageId:J

.field private mNeedResolveAccount:Z

.field private mNeedStartEmailActivity:Z

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mWaitingForSyncView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    iput-boolean v1, p0, Lcom/android/email/activity/Welcome;->mFindInboxAndFinish:Z

    iput v1, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderDelayTime:I

    new-instance v0, Lcom/android/email/activity/Welcome$1;

    invoke-direct {v0, p0}, Lcom/android/email/activity/Welcome$1;-><init>(Lcom/android/email/activity/Welcome;)V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

    return-void
.end method

.method static synthetic access$002(Lcom/android/email/activity/Welcome;Lcom/android/email/activity/MailboxFinder;)Lcom/android/email/activity/MailboxFinder;
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Lcom/android/email/activity/MailboxFinder;

    iput-object p1, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    return-object p1
.end method

.method static synthetic access$1002(Lcom/android/email/activity/Welcome;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/Welcome;->mNeedResolveAccount:Z

    return p1
.end method

.method static synthetic access$102(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    return-wide p1
.end method

.method static synthetic access$1102(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    return-wide p1
.end method

.method static synthetic access$202(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    return-wide p1
.end method

.method static synthetic access$302(Lcom/android/email/activity/Welcome;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/email/activity/Welcome;)Lcom/android/email/activity/Welcome;
    .locals 1
    .param p0    # Lcom/android/email/activity/Welcome;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mContext:Lcom/android/email/activity/Welcome;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/email/activity/Welcome;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/Welcome;

    iget-boolean v0, p0, Lcom/android/email/activity/Welcome;->mIsResumed:Z

    return v0
.end method

.method static synthetic access$600(Lcom/android/email/activity/Welcome;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->resolveAccount()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/email/activity/Welcome;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startEmailActivity()V

    return-void
.end method

.method static synthetic access$802(Lcom/android/email/activity/Welcome;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/Welcome;->mNeedStartEmailActivity:Z

    return p1
.end method

.method public static actionOpenAccountInbox(Landroid/app/Activity;J)V
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # J

    invoke-static {p0, p1, p2}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static actionStart(Landroid/app/Activity;)V
    .locals 2
    .param p0    # Landroid/app/Activity;

    const-class v1, Lcom/android/email/activity/Welcome;

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static actionSyncInboxAndFinish(Landroid/app/Activity;J)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # J

    invoke-static {p0, p1, p2}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "FROM_ACCOUNT_MANAGER"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static createAccountShortcutIntent(Landroid/content/Context;Ljava/lang/String;J)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountUuid(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-static {v0, p2, p3}, Lcom/android/emailcommon/utility/IntentUtilities;->setMailboxId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountId(Landroid/net/Uri$Builder;J)V

    invoke-static {v0, p3, p4}, Lcom/android/emailcommon/utility/IntentUtilities;->setMailboxId(Landroid/net/Uri$Builder;J)V

    invoke-static {v0, p5, p6}, Lcom/android/emailcommon/utility/IntentUtilities;->setMessageId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private static getDebugPaneMode(Landroid/content/Intent;)I
    .locals 3
    .param p0    # Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "DEBUG_PANE_MODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const-string v2, "2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private resolveAccount()V
    .locals 6

    const-wide/16 v4, -0x1

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Welcome"

    const-string v2, "There\'s no account configured , start setup new account"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/email/activity/setup/AccountSetupBasics;->actionNewAccount(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-object v3, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/android/email/activity/Welcome;->resolveAccountId(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {v1, v2}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-nez v1, :cond_2

    const-string v1, "Welcome"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resolveAccountId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t have inbox start inbox finder."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startInboxLookup()V

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    cmp-long v1, v1, v4

    if-nez v1, :cond_2

    const-string v1, "Welcome"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resolveAccountId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a NormalAccount, start setup new account"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/email/activity/setup/AccountSetupBasics;->actionNewAccount(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const-string v1, "Welcome"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resolveAccountId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startEmailActivity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startEmailActivity()V

    goto/16 :goto_0
.end method

.method static resolveAccountId(Landroid/content/Context;JLjava/lang/String;)J
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const-wide/16 v6, -0x1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0, p3}, Lcom/android/emailcommon/provider/Account;->getAccountIdFromUuid(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    cmp-long v4, v0, v6

    if-eqz v4, :cond_6

    const-string v4, "Welcome"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " inputAccountId is  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " the given account is valid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-wide v0

    :cond_0
    cmp-long v4, p1, v6

    if-eqz v4, :cond_3

    const-wide/high16 v4, 0x1000000000000000L

    cmp-long v4, p1, v4

    if-eqz v4, :cond_1

    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/Account;->isValidId(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move-wide v0, p1

    goto :goto_0

    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/email/Preferences;->getLastUsedAccountId()J

    move-result-wide v2

    cmp-long v4, v2, v6

    if-eqz v4, :cond_4

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/provider/Account;->isValidId(Landroid/content/Context;J)Z

    move-result v4

    if-nez v4, :cond_4

    const-wide/16 v2, -0x1

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    :cond_4
    cmp-long v4, v2, v6

    if-nez v4, :cond_5

    invoke-static {p0}, Lcom/android/emailcommon/provider/Account;->getDefaultAccountId(Landroid/content/Context;)J

    move-result-wide v0

    :goto_2
    goto :goto_0

    :cond_5
    move-wide v0, v2

    goto :goto_2

    :cond_6
    const v4, 0x7f0801ca

    invoke-static {p0, v4}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    const-string v4, "Welcome"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " inputAccountId is  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not found,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " return the default account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/emailcommon/provider/Account;->getDefaultAccountId(Landroid/content/Context;)J

    move-result-wide v0

    goto :goto_1
.end method

.method private startEmailActivity()V
    .locals 9

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v3, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    iget-wide v5, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/android/email/activity/EmailActivity;->createOpenMessageIntent(Landroid/app/Activity;JJJ)Landroid/content/Intent;

    move-result-object v8

    :goto_0
    invoke-virtual {p0, v8}, Lcom/android/email/activity/Welcome;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/emailcommon/Logging;->LOG_PERFORMANCE:Z

    if-eqz v0, :cond_1

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Exchange Download Speed] Welcome:StartEmailActivity:ShowMessageList ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v2, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    invoke-static {p0, v0, v1, v2, v3}, Lcom/android/email/activity/EmailActivity;->createOpenMailboxIntent(Landroid/app/Activity;JJ)Landroid/content/Intent;

    move-result-object v8

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mContext:Lcom/android/email/activity/Welcome;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mContext:Lcom/android/email/activity/Welcome;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-wide/high16 v1, 0x1000000000000000L

    const-wide/16 v3, -0x2

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/android/email/activity/EmailActivity;->createLocalSearchIntent(Landroid/app/Activity;JJLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {p0, v0, v1}, Lcom/android/email/activity/EmailActivity;->createOpenAccountIntent(Landroid/app/Activity;J)Landroid/content/Intent;

    move-result-object v8

    goto :goto_0
.end method

.method private startInboxLookup()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v4, 0x0

    const-string v0, "Email"

    const-string v1, "Inbox not found.  Starting mailbox finder..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->stopInboxLookup()V

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v7

    new-instance v0, Lcom/android/email/activity/MailboxFinder;

    iget-wide v2, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-object v5, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

    iget v6, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderDelayTime:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/activity/MailboxFinder;-><init>(Landroid/content/Context;JILcom/android/email/activity/MailboxFinder$Callback;I)V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    iput v4, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderDelayTime:I

    invoke-virtual {v7}, Lcom/android/email/Preferences;->getLowStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f08000b

    invoke-static {p0, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const v0, 0x7f08000c

    invoke-static {p0, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "checkIsLowStorage canceled startInboxLookup due to low storage"

    invoke-static {v0}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxFinder;->startLookup()V

    iget-boolean v0, p0, Lcom/android/email/activity/Welcome;->mFindInboxAndFinish:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    iput-boolean v4, p0, Lcom/android/email/activity/Welcome;->mFindInboxAndFinish:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mWaitingForSyncView:Landroid/view/View;

    if-nez v0, :cond_2

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040059

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mWaitingForSyncView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mWaitingForSyncView:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method private stopInboxLookup()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxFinder;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Performance test][Email] MessageList FPS test start time ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/android/email/activity/ActivityHelper;->debugSetWindowFlags(Landroid/app/Activity;)V

    invoke-static {p0}, Lcom/android/email/service/EmailServiceUtils;->startExchangeService(Landroid/content/Context;)V

    iput-object p0, p0, Lcom/android/email/activity/Welcome;->mContext:Lcom/android/email/activity/Welcome;

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->getAccountIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->getMailboxIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->getMessageIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->getAccountUuidFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/email/activity/Welcome;->getDebugPaneMode(Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Lcom/android/email/activity/UiUtilities;->setDebugPaneMode(I)V

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "FROM_ACCOUNT_MANAGER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/email/activity/Welcome;->mFindInboxAndFinish:Z

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->resolveAccount()V

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/android/email/Email;->setNotifyUiAccountsChanged(Z)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0120

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {p0, v0, v1}, Lcom/android/email/activity/setup/AccountSettings;->actionSettings(Landroid/app/Activity;J)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/Welcome;->mIsResumed:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/Welcome;->mIsResumed:Z

    iget-boolean v0, p0, Lcom/android/email/activity/Welcome;->mNeedStartEmailActivity:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startEmailActivity()V

    iput-boolean v3, p0, Lcom/android/email/activity/Welcome;->mNeedStartEmailActivity:Z

    :cond_1
    iget-boolean v0, p0, Lcom/android/email/activity/Welcome;->mNeedResolveAccount:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x1388

    iput v0, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderDelayTime:I

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->resolveAccount()V

    iput-boolean v3, p0, Lcom/android/email/activity/Welcome;->mNeedResolveAccount:Z

    :cond_2
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v1, 0x2000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p0, v2, v2}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-super {p0, p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    return-void
.end method
