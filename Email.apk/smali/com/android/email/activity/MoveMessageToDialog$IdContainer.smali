.class Lcom/android/email/activity/MoveMessageToDialog$IdContainer;
.super Ljava/lang/Object;
.source "MoveMessageToDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MoveMessageToDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IdContainer"
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mMailboxId:J


# direct methods
.method private constructor <init>(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;->mAccountId:J

    iput-wide p3, p0, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;->mMailboxId:J

    return-void
.end method

.method synthetic constructor <init>(JJLcom/android/email/activity/MoveMessageToDialog$1;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Lcom/android/email/activity/MoveMessageToDialog$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;-><init>(JJ)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/email/activity/MoveMessageToDialog$IdContainer;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog$IdContainer;

    iget-wide v0, p0, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;->mAccountId:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/email/activity/MoveMessageToDialog$IdContainer;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MoveMessageToDialog$IdContainer;

    iget-wide v0, p0, Lcom/android/email/activity/MoveMessageToDialog$IdContainer;->mMailboxId:J

    return-wide v0
.end method
