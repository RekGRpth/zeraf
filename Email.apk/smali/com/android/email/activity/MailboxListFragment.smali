.class public Lcom/android/email/activity/MailboxListFragment;
.super Landroid/app/ListFragment;
.source "MailboxListFragment.java"

# interfaces
.implements Landroid/view/View$OnDragListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MailboxListFragment$LoadVipFolderMessageCountTask;,
        Lcom/android/email/activity/MailboxListFragment$MailboxListLoaderCallbacks;,
        Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;,
        Lcom/android/email/activity/MailboxListFragment$EmptyCallback;,
        Lcom/android/email/activity/MailboxListFragment$Callback;
    }
.end annotation


# static fields
.field private static final ARG_ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field private static final ARG_ENABLE_HIGHLIGHT:Ljava/lang/String; = "enablehighlight"

.field private static final ARG_INITIAL_CURRENT_MAILBOX_ID:Ljava/lang/String; = "initialParentMailboxId"

.field private static final BUNDLE_KEY_HIGHLIGHTED_MAILBOX_ID:Ljava/lang/String; = "MailboxListFragment.state.selected_mailbox_id"

.field private static final BUNDLE_KEY_PARENT_MAILBOX_ID:Ljava/lang/String; = "MailboxListFragment.state.parent_mailbox_id"

.field private static final BUNDLE_LIST_STATE:Ljava/lang/String; = "MailboxListFragment.state.listState"

.field private static final BUNDLE_VIP_MAIL_COUNT:Ljava/lang/String; = "MailboxListFragment.state.VipMailCount"

.field private static final DEBUG_DRAG_DROP:Z = false

.field private static final MAILBOX_LOADER_ID:I = 0x1

.field private static final NO_DROP_TARGET:I = -0x1

.field private static final SCROLL_SPEED:I = 0x4

.field private static final SCROLL_ZONE_SIZE:I = 0x40

.field private static final TAG:Ljava/lang/String; = "MailboxListFragment"

.field private static final sTouchFrame:Landroid/graphics/Rect;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

.field private mDragInProgress:Z

.field private mDragItemHeight:I

.field private mDragItemMailboxId:J

.field private mDropTargetId:I

.field private mDropTargetView:Lcom/android/email/activity/MailboxListItem;

.field private mHighlightedMailboxId:J

.field private mImmutableAccountId:Ljava/lang/Long;

.field private mImmutableEnableHighlight:Z

.field private mImmutableInitialCurrentMailboxId:J

.field private mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

.field private final mMailboxesAdapterCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

.field private mNextHighlightedMailboxId:J

.field private mParentDetermined:Z

.field private mParentMailboxId:J

.field private mRefreshManager:Lcom/android/email/RefreshManager;

.field private mSavedListState:Landroid/os/Parcelable;

.field private mTargetScrolling:Z

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mVipMailCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/email/activity/MailboxListFragment;->sTouchFrame:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    sget-object v0, Lcom/android/email/activity/MailboxListFragment$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MailboxListFragment$Callback;

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

    iput-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    iput-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    iput v1, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    iput v1, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/email/activity/MailboxListFragment;->mVipMailCount:I

    new-instance v0, Lcom/android/email/activity/MailboxListFragment$1;

    invoke-direct {v0, p0}, Lcom/android/email/activity/MailboxListFragment$1;-><init>(Lcom/android/email/activity/MailboxListFragment;)V

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mMailboxesAdapterCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/MailboxListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/email/activity/MailboxListFragment;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/android/email/activity/MailboxListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MailboxListFragment;->mParentDetermined:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/email/activity/MailboxListFragment;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/android/email/activity/MailboxListFragment;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/android/email/activity/MailboxListFragment;Z)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/activity/MailboxListFragment;->updateHighlightedMailbox(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1302(Lcom/android/email/activity/MailboxListFragment;I)I
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # I

    iput p1, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    return p1
.end method

.method static synthetic access$1402(Lcom/android/email/activity/MailboxListFragment;Lcom/android/email/activity/MailboxListItem;)Lcom/android/email/activity/MailboxListItem;
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # Lcom/android/email/activity/MailboxListItem;

    iput-object p1, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/android/email/activity/MailboxListFragment;I)I
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # I

    iput p1, p0, Lcom/android/email/activity/MailboxListFragment;->mVipMailCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/email/activity/MailboxListFragment;JJ)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # J
    .param p3    # J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/email/activity/MailboxListFragment;->startLoading(JJ)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/email/activity/MailboxListFragment;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/android/email/activity/MailboxListFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->isRoot()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/email/activity/MailboxListFragment;)Lcom/android/email/activity/MailboxFragmentAdapter;
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/email/activity/MailboxListFragment;)Landroid/os/Parcelable;
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mSavedListState:Landroid/os/Parcelable;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/email/activity/MailboxListFragment;Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 0
    .param p0    # Lcom/android/email/activity/MailboxListFragment;
    .param p1    # Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/android/email/activity/MailboxListFragment;->mSavedListState:Landroid/os/Parcelable;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/email/activity/MailboxListFragment;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/email/activity/MailboxListFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/android/email/activity/MailboxListFragment;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private initializeArgCache()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableAccountId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableAccountId:Ljava/lang/Long;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initialParentMailboxId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableInitialCurrentMailboxId:J

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "enablehighlight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableEnableHighlight:Z

    goto :goto_0
.end method

.method private isRoot()Z
    .locals 4

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewCreated()Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(JJZ)Lcom/android/email/activity/MailboxListFragment;
    .locals 3
    .param p0    # J
    .param p2    # J
    .param p4    # Z

    new-instance v1, Lcom/android/email/activity/MailboxListFragment;

    invoke-direct {v1}, Lcom/android/email/activity/MailboxListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "accountId"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "initialParentMailboxId"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "enablehighlight"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onDragEnded()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/email/activity/MailboxFragmentAdapter;->enableUpdates(Z)V

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->updateChildViews()V

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->stopScrolling()V

    :cond_0
    return-void
.end method

.method private onDragExited()V
    .locals 4

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    iget-boolean v1, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    iget-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/email/activity/MailboxListItem;->setDropTargetBackground(ZJ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->stopScrolling()V

    return-void
.end method

.method private onDragLocation(Landroid/view/DragEvent;)V
    .locals 19
    .param p1    # Landroid/view/DragEvent;

    invoke-virtual/range {p0 .. p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    if-gtz v15, :cond_1

    const-string v15, "MailboxListFragment"

    const-string v16, "drag item height is not set"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    move-result v15

    float-to-int v8, v15

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    move-result v15

    float-to-int v9, v15

    invoke-static {v5, v8, v9}, Lcom/android/email/activity/MailboxListFragment;->pointToIndex(Landroid/widget/ListView;II)I

    move-result v14

    move v13, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    if-eq v13, v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    move-wide/from16 v17, v0

    invoke-virtual/range {v15 .. v18}, Lcom/android/email/activity/MailboxListItem;->setDropTargetBackground(ZJ)V

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    :cond_2
    invoke-virtual {v5, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_5

    const/4 v6, 0x0

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lt v14, v2, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/MailboxListFragment;->onDragExited()V

    goto :goto_0

    :cond_3
    const-string v15, "MailboxListFragment"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "null view; idx: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", cnt: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    :cond_4
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v15

    add-int/lit8 v15, v15, -0x40

    sub-int v10, v9, v15

    if-lez v10, :cond_9

    const/4 v11, 0x1

    :goto_2
    const/16 v15, 0x40

    if-le v15, v9, :cond_a

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    if-nez v15, :cond_b

    if-eqz v11, :cond_b

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getCount()I

    move-result v15

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    move-result v16

    sub-int v4, v15, v16

    add-int/lit8 v15, v4, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    move/from16 v16, v0

    mul-int v7, v15, v16

    mul-int/lit8 v15, v7, 0x4

    invoke-virtual {v5, v7, v15}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    goto/16 :goto_0

    :cond_5
    instance-of v15, v3, Lcom/android/email/activity/MailboxListItem;

    if-nez v15, :cond_6

    const/4 v6, 0x0

    const/4 v13, -0x1

    goto :goto_1

    :cond_6
    move-object v6, v3

    check-cast v6, Lcom/android/email/activity/MailboxListItem;

    iget-object v15, v6, Lcom/android/email/activity/MailboxListItem;->mMailboxType:Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const/16 v16, 0x6

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    invoke-virtual {v6}, Lcom/android/email/activity/MailboxListItem;->setDropTrashBackground()V

    goto :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    move-wide v0, v15

    invoke-virtual {v6, v0, v1}, Lcom/android/email/activity/MailboxListItem;->isDropTarget(J)Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-virtual {v6}, Lcom/android/email/activity/MailboxListItem;->setDropActiveBackground()V

    goto :goto_1

    :cond_8
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v6, v15, v0, v1}, Lcom/android/email/activity/MailboxListItem;->setDropTargetBackground(ZJ)V

    const/4 v13, -0x1

    goto :goto_1

    :cond_9
    const/4 v11, 0x0

    goto :goto_2

    :cond_a
    const/4 v12, 0x0

    goto :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    if-nez v15, :cond_c

    if-eqz v12, :cond_c

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    move/from16 v16, v0

    mul-int v7, v15, v16

    neg-int v15, v7

    mul-int/lit8 v16, v7, 0x4

    move/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    goto/16 :goto_0

    :cond_c
    if-nez v12, :cond_0

    if-nez v11, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/MailboxListFragment;->stopScrolling()V

    goto/16 :goto_0
.end method

.method private onDragStarted(Landroid/view/DragEvent;)Z
    .locals 9
    .param p1    # Landroid/view/DragEvent;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v1, v2}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v3

    const-string v7, "vnd.android.cursor.item/email-message"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-wide/16 v7, -0x1

    iput-wide v7, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    const/16 v7, 0x2d

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    add-int/lit8 v7, v0, 0x1

    :try_start_0
    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iput-boolean v5, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    invoke-static {v6}, Lcom/android/email/activity/MailboxFragmentAdapter;->enableUpdates(Z)V

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->updateChildViews()V

    :goto_2
    return v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_2

    :catch_0
    move-exception v7

    goto :goto_1
.end method

.method private onDrop(Landroid/view/DragEvent;)Z
    .locals 12
    .param p1    # Landroid/view/DragEvent;

    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->stopScrolling()V

    iget v10, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetId:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_0

    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_0
    iget-object v10, p0, Lcom/android/email/activity/MailboxListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v10}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    new-array v6, v2, [J

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iget-object v10, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    iget-object v10, v10, Lcom/android/email/activity/MailboxListItem;->mMailboxType:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x6

    if-ne v10, v11, :cond_2

    invoke-static {v6}, Lcom/android/emailcommon/utility/Utility;->toLongSet([J)Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/android/email/Controller;->deleteMessages(Ljava/util/Set;)V

    goto :goto_0

    :cond_2
    iget-object v10, p0, Lcom/android/email/activity/MailboxListFragment;->mDropTargetView:Lcom/android/email/activity/MailboxListItem;

    iget-wide v10, v10, Lcom/android/email/activity/MailboxListItem;->mMailboxId:J

    invoke-virtual {v1, v6, v10, v11}, Lcom/android/email/Controller;->moveMessages([JJ)Lcom/android/emailcommon/utility/EmailAsyncTask;

    goto :goto_0
.end method

.method private static pointToIndex(Landroid/widget/ListView;II)I
    .locals 4
    .param p0    # Landroid/widget/ListView;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_1

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/email/activity/MailboxListFragment;->sTouchFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    sget-object v3, Lcom/android/email/activity/MailboxListFragment;->sTouchFrame:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " restoreInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "MailboxListFragment.state.parent_mailbox_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    const-string v0, "MailboxListFragment.state.selected_mailbox_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    const-string v0, "MailboxListFragment.state.listState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mSavedListState:Landroid/os/Parcelable;

    const-string v0, "MailboxListFragment.state.VipMailCount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/email/activity/MailboxListFragment;->mVipMailCount:I

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    iget v1, p0, Lcom/android/email/activity/MailboxListFragment;->mVipMailCount:I

    invoke-virtual {v0, v1}, Lcom/android/email/activity/MailboxFragmentAdapter;->updateVipMessageCount(I)V

    return-void
.end method

.method private setInitialParentAndHighlight()V
    .locals 10

    const-wide/16 v8, -0x1

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getInitialCurrentMailboxId()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v4

    const-wide/high16 v6, 0x1000000000000000L

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    iput-wide v8, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    :goto_0
    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getEnableHighlight()Z

    move-result v4

    if-eqz v4, :cond_0

    iput-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v5

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_2

    const-wide/16 v4, -0x7

    cmp-long v4, v2, v4

    if-nez v4, :cond_3

    :cond_2
    iput-wide v8, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    goto :goto_0

    :cond_3
    iput-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    goto :goto_0
.end method

.method private startLoading(JJ)V
    .locals 7
    .param p1    # J
    .param p3    # J

    const/4 v6, 0x0

    const/4 v5, 0x1

    sget-boolean v2, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " startLoading  parent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " highlighted="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    cmp-long v2, v2, p1

    if-eqz v2, :cond_1

    invoke-virtual {v0, v5}, Landroid/app/LoaderManager;->destroyLoader(I)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/app/ListFragment;->setListShown(Z)V

    const/4 v1, 0x1

    :cond_1
    iput-wide p1, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getEnableHighlight()Z

    move-result v2

    if-eqz v2, :cond_2

    iput-wide p3, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    :cond_2
    new-instance v2, Lcom/android/email/activity/MailboxListFragment$MailboxListLoaderCallbacks;

    invoke-direct {v2, p0, v6}, Lcom/android/email/activity/MailboxListFragment$MailboxListLoaderCallbacks;-><init>(Lcom/android/email/activity/MailboxListFragment;Lcom/android/email/activity/MailboxListFragment$1;)V

    invoke-virtual {v0, v5, v6, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/email/activity/MailboxListFragment;->mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

    invoke-interface {v2}, Lcom/android/email/activity/MailboxListFragment$Callback;->onParentMailboxChanged()V

    :cond_3
    return-void
.end method

.method private stopScrolling()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/email/activity/MailboxListFragment;->mTargetScrolling:Z

    invoke-virtual {v0, v2, v2}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    :cond_0
    return-void
.end method

.method private updateChildViews()V
    .locals 8

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    iget v5, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    if-gez v5, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemHeight:I

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v5, v0, Lcom/android/email/activity/MailboxListItem;

    if-nez v5, :cond_1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move-object v2, v0

    check-cast v2, Lcom/android/email/activity/MailboxListItem;

    iget-boolean v5, p0, Lcom/android/email/activity/MailboxListFragment;->mDragInProgress:Z

    iget-wide v6, p0, Lcom/android/email/activity/MailboxListFragment;->mDragItemMailboxId:J

    invoke-virtual {v2, v5, v6, v7}, Lcom/android/email/activity/MailboxListItem;->setDropTargetBackground(ZJ)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private updateHighlightedMailbox(Z)Z
    .locals 11
    .param p1    # Z

    const-wide/16 v9, -0x1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getEnableHighlight()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->isViewCreated()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move v1, v4

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    const/4 v1, 0x0

    iget-wide v5, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_4

    invoke-virtual {v3}, Landroid/widget/AbsListView;->clearChoices()V

    const/4 v1, 0x1

    :cond_3
    :goto_1
    if-nez v1, :cond_1

    iput-wide v9, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_3

    iget-object v5, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v5, v2}, Lcom/android/email/activity/MailboxFragmentAdapter;->getId(I)J

    move-result-wide v5

    iget-wide v7, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_5

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v3, v2}, Lcom/android/emailcommon/utility/Utility;->listViewSmoothScrollToPosition(Landroid/app/Activity;Landroid/widget/ListView;I)V

    goto :goto_1
.end method


# virtual methods
.method public canNavigateUp()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/email/activity/MailboxListFragment;->mParentDetermined:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->isRoot()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAccountId()J
    .locals 2

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->initializeArgCache()V

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableAccountId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEnableHighlight()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->initializeArgCache()V

    iget-boolean v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableEnableHighlight:Z

    return v0
.end method

.method public getInitialCurrentMailboxId()J
    .locals 2

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->initializeArgCache()V

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mImmutableInitialCurrentMailboxId:J

    return-wide v0
.end method

.method public getSelectedMailboxId()J
    .locals 4

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    goto :goto_0
.end method

.method public navigateUp()Z
    .locals 11

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->isRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v10, Lcom/android/email/activity/MailboxListFragment$2;

    invoke-direct {v10, p0}, Lcom/android/email/activity/MailboxListFragment$2;-><init>(Lcom/android/email/activity/MailboxListFragment;)V

    new-instance v0, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MailboxListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getEnableHighlight()Z

    move-result v5

    iget-wide v6, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    iget-wide v8, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    invoke-direct/range {v0 .. v10}, Lcom/android/email/activity/MailboxListFragment$FindParentMailboxTask;-><init>(Landroid/content/Context;Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;JZJJLcom/android/email/activity/MailboxListFragment$FindParentMailboxTask$ResultCallback;)V

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancelPreviousAndExecuteParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onActivityCreated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    iget-wide v1, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    iget-wide v3, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/email/activity/MailboxListFragment;->startLoading(JJ)V

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->installFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onAttach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    new-instance v0, Lcom/android/email/activity/MailboxFragmentAdapter;

    iget-object v1, p0, Lcom/android/email/activity/MailboxListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/email/activity/MailboxListFragment;->mMailboxesAdapterCallback:Lcom/android/email/activity/MailboxFragmentAdapter$Callback;

    invoke-direct {v0, v1, v2}, Lcom/android/email/activity/MailboxFragmentAdapter;-><init>(Landroid/content/Context;Lcom/android/email/activity/MailboxFragmentAdapter$Callback;)V

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->setInitialParentAndHighlight()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/email/activity/MailboxListFragment;->restoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreateView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v0, 0x7f040036

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onDestroy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v1, p0, Lcom/android/email/activity/MailboxListFragment;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/email/activity/FragmentInstallable;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/email/activity/FragmentInstallable;

    invoke-interface {v0, p0}, Lcom/android/email/activity/FragmentInstallable;->onRemoveFragment(Landroid/app/Fragment;)V

    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroyView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->uninstallFragment(Landroid/app/Fragment;)V

    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDetach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    return-void
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/DragEvent;

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    invoke-direct {p0, p2}, Lcom/android/email/activity/MailboxListFragment;->onDragStarted(Landroid/view/DragEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->onDragExited()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->onDragEnded()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p2}, Lcom/android/email/activity/MailboxListFragment;->onDragLocation(Landroid/view/DragEvent;)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p2}, Lcom/android/email/activity/MailboxListFragment;->onDrop(Landroid/view/DragEvent;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getId(I)J

    move-result-wide v3

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->isAccountRow(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

    invoke-interface {v0, v3, v4}, Lcom/android/email/activity/MailboxListFragment$Callback;->onAccountSelected(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->isMailboxRow(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0, p3}, Lcom/android/email/activity/MailboxFragmentAdapter;->getAccountId(I)J

    move-result-wide v1

    const/4 v5, 0x0

    instance-of v0, p2, Lcom/android/email/activity/MailboxListItem;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/android/email/activity/MailboxListItem;

    invoke-virtual {p2}, Lcom/android/email/activity/MailboxListItem;->isNavigable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v6, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    cmp-long v0, v3, v6

    if-eqz v0, :cond_2

    invoke-direct {p0, v3, v4, v3, v4}, Lcom/android/email/activity/MailboxListFragment;->startLoading(JJ)V

    const/4 v5, 0x1

    :cond_2
    const-wide/high16 v6, 0x1000000000000000L

    cmp-long v0, v1, v6

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v1

    :cond_3
    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

    invoke-interface/range {v0 .. v5}, Lcom/android/email/activity/MailboxListFragment$Callback;->onMailboxSelected(JJZ)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mSavedListState:Landroid/os/Parcelable;

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 5

    sget-boolean v2, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onResume"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getAccountId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/email/activity/MailboxListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {v2, v0, v1}, Lcom/android/email/RefreshManager;->isMailboxListStale(J)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/MailboxListFragment;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {v2, v0, v1}, Lcom/android/email/RefreshManager;->refreshMailboxList(J)Z

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "MailboxListFragment.state.parent_mailbox_id"

    iget-wide v1, p0, Lcom/android/email/activity/MailboxListFragment;->mParentMailboxId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "MailboxListFragment.state.selected_mailbox_id"

    iget-wide v1, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-direct {p0}, Lcom/android/email/activity/MailboxListFragment;->isViewCreated()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MailboxListFragment.state.listState"

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    const-string v0, "MailboxListFragment.state.VipMailCount"

    iget v1, p0, Lcom/android/email/activity/MailboxListFragment;->mVipMailCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onStart()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    return-void
.end method

.method public setCallback(Lcom/android/email/activity/MailboxListFragment$Callback;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MailboxListFragment$Callback;

    if-nez p1, :cond_0

    sget-object p1, Lcom/android/email/activity/MailboxListFragment$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MailboxListFragment$Callback;

    :cond_0
    iput-object p1, p0, Lcom/android/email/activity/MailboxListFragment;->mCallback:Lcom/android/email/activity/MailboxListFragment$Callback;

    return-void
.end method

.method public setHighlightedMailbox(J)V
    .locals 3
    .param p1    # J

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setHighlightedMailbox  mailbox="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/MailboxListFragment;->getEnableHighlight()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-wide v0, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MailboxListFragment;->mListAdapter:Lcom/android/email/activity/MailboxFragmentAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_3

    iput-wide p1, p0, Lcom/android/email/activity/MailboxListFragment;->mNextHighlightedMailboxId:J

    goto :goto_0

    :cond_3
    iput-wide p1, p0, Lcom/android/email/activity/MailboxListFragment;->mHighlightedMailboxId:J

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/email/activity/MailboxListFragment;->updateHighlightedMailbox(Z)Z

    goto :goto_0
.end method
