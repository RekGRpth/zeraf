.class public Lcom/android/email/activity/ActionBarController;
.super Ljava/lang/Object;
.source "ActionBarController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/ActionBarController$TabListener;,
        Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;,
        Lcom/android/email/activity/ActionBarController$DropDownListener;,
        Lcom/android/email/activity/ActionBarController$Callback;,
        Lcom/android/email/activity/ActionBarController$SearchContext;
    }
.end annotation


# static fields
.field private static final ACTION_BAR_MASK:I = 0x3a

.field private static final BUNDLE_KEY_ACTION_BAR_SELECTED_FIELD:Ljava/lang/String; = "ActionBarController.ACTION_BAR_SELECTED_TAB"

.field private static final BUNDLE_KEY_FILTER:Ljava/lang/String; = "ActionBarController.FILTER_STRING"

.field private static final BUNDLE_KEY_MODE:Ljava/lang/String; = "ActionBarController.BUNDLE_KEY_MODE"

.field private static final BUNDLE_SEARCH_HINT:Ljava/lang/String; = "ActionBarController.BUNDLE_SEARCH_HINT"

.field private static final CUSTOM_ACTION_BAR_OPTIONS:I = 0x12

.field private static final DISPLAY_TITLE_MULTIPLE_LINES:I = 0x20

.field private static final INDEX_SENDER:I = 0x1

.field private static final LOADER_ID_ACCOUNT_LIST:I = 0xc8

.field private static final LOADER_ID_ACCOUNT_TITLE:I = 0xc9

.field private static final MODE_LOCAL_SEARCH:I = 0x1

.field private static final MODE_NORMAL:I = 0x0

.field private static final MODE_REMOTE_SEARCH:I = 0x2

.field public static final TAG:Ljava/lang/String; = "ActionBarController"

.field private static final TITLE_MODE_SPINNER_ENABLED:I = 0x10

.field private static sActionBarSelectedTab:Ljava/lang/String;


# instance fields
.field private final mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

.field private final mAccountSpinner:Landroid/view/View;

.field private final mAccountSpinnerContainer:Landroid/view/ViewGroup;

.field private final mAccountSpinnerCountView:Landroid/widget/TextView;

.field private final mAccountSpinnerDefaultBackground:Landroid/graphics/drawable/Drawable;

.field private final mAccountSpinnerLine1View:Landroid/widget/TextView;

.field private final mAccountSpinnerLine2View:Landroid/widget/TextView;

.field private final mAccountSyncIntervalView:Landroid/widget/ImageView;

.field private final mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

.field private final mActionBar:Landroid/app/ActionBar;

.field private final mActionBarCustomView:Landroid/view/ViewGroup;

.field private mActionBarSelectedTab:Ljava/lang/String;

.field private final mAllFoldersLabel:Ljava/lang/String;

.field private mBackFromMessageView:Z

.field public final mCallback:Lcom/android/email/activity/ActionBarController$Callback;

.field private final mContext:Landroid/content/Context;

.field private mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

.field private final mDefaultLayoutParams:Landroid/app/ActionBar$LayoutParams;

.field private final mDelayedOperations:Lcom/android/emailcommon/utility/DelayedOperations;

.field private mFieldList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Ljava/lang/String;

.field private mIsTitleUpdated:Z

.field private mLastAccountIdForDirtyCheck:J

.field private mLastMailboxIdForDirtyCheck:J

.field private final mLoaderManager:Landroid/app/LoaderManager;

.field private final mMailSwitcher:Landroid/widget/LinearLayout;

.field private final mOnQueryText:Landroid/widget/SearchView$OnQueryTextListener;

.field private final mRefreshRunnable:Ljava/lang/Runnable;

.field private mRemoteSearchContainer:Landroid/view/View;

.field private mRemoteSearchTitle:Landroid/widget/TextView;

.field private mRestoredSelectedTab:Ljava/lang/String;

.field private mSearchContainer:Landroid/view/View;

.field private mSearchMode:I

.field private mSearchResultCountView:Landroid/widget/TextView;

.field private mSearchView:Landroid/widget/SearchView;

.field private mShouldClearSearchFocus:Z

.field private final mSwitchNewerView:Landroid/widget/ImageButton;

.field private final mSwitchOlderView:Landroid/widget/ImageButton;

.field private mTabChanged:Z

.field private mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

.field private mTitleMode:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/LoaderManager;Landroid/app/ActionBar;Lcom/android/email/activity/ActionBarController$Callback;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/app/LoaderManager;
    .param p3    # Landroid/app/ActionBar;
    .param p4    # Lcom/android/email/activity/ActionBarController$Callback;

    const-wide/16 v0, -0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/email/activity/ActionBarController;->mLastAccountIdForDirtyCheck:J

    iput-wide v0, p0, Lcom/android/email/activity/ActionBarController;->mLastMailboxIdForDirtyCheck:J

    iput v2, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    iput-boolean v2, p0, Lcom/android/email/activity/ActionBarController;->mIsTitleUpdated:Z

    iput-boolean v2, p0, Lcom/android/email/activity/ActionBarController;->mShouldClearSearchFocus:Z

    iput-boolean v2, p0, Lcom/android/email/activity/ActionBarController;->mBackFromMessageView:Z

    new-instance v0, Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-direct {v0, p0}, Lcom/android/email/activity/ActionBarController$TabListener;-><init>(Lcom/android/email/activity/ActionBarController;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    iput-boolean v2, p0, Lcom/android/email/activity/ActionBarController;->mTabChanged:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/email/activity/ActionBarController$4;

    invoke-direct {v0, p0}, Lcom/android/email/activity/ActionBarController$4;-><init>(Lcom/android/email/activity/ActionBarController;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mRefreshRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/email/activity/ActionBarController$7;

    invoke-direct {v0, p0}, Lcom/android/email/activity/ActionBarController$7;-><init>(Lcom/android/email/activity/ActionBarController;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mOnQueryText:Landroid/widget/SearchView$OnQueryTextListener;

    iput-object p1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/email/activity/ActionBarController;->mLoaderManager:Landroid/app/LoaderManager;

    iput-object p3, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    iput-object p4, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    new-instance v0, Lcom/android/emailcommon/utility/DelayedOperations;

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/emailcommon/utility/DelayedOperations;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mDelayedOperations:Lcom/android/emailcommon/utility/DelayedOperations;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAllFoldersLabel:Ljava/lang/String;

    new-instance v0, Lcom/android/email/activity/AccountSelectorAdapter;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/email/activity/AccountSelectorAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->enterCustomActionBarMode()V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f040018

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mDefaultLayoutParams:Landroid/app/ActionBar$LayoutParams;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0056

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0060

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerDefaultBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0061

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f0062

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f005e

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerCountView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f005f

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f005b

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getViewOrNull(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mMailSwitcher:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f005c

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getViewOrNull(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    const v1, 0x7f0f005d

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getViewOrNull(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/email/activity/ActionBarController$1;

    invoke-direct {v1, p0}, Lcom/android/email/activity/ActionBarController$1;-><init>(Lcom/android/email/activity/ActionBarController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/email/activity/ActionBarController$2;

    invoke-direct {v1, p0}, Lcom/android/email/activity/ActionBarController$2;-><init>(Lcom/android/email/activity/ActionBarController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    new-instance v0, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;-><init>(Lcom/android/email/activity/ActionBarController;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    new-instance v1, Lcom/android/email/activity/ActionBarController$3;

    invoke-direct {v1, p0}, Lcom/android/email/activity/ActionBarController$3;-><init>(Lcom/android/email/activity/ActionBarController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/ActionBarController;)Lcom/android/email/activity/AccountSelectorAdapter;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/email/activity/ActionBarController;)Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/email/activity/ActionBarController;Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;)Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;
    .param p1    # Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    iput-object p1, p0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/email/activity/ActionBarController;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->updateTitle()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/email/activity/ActionBarController;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/email/activity/ActionBarController;I)V
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/ActionBarController;->onAccountSpinnerItemClicked(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/email/activity/ActionBarController;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/email/activity/ActionBarController;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/email/activity/ActionBarController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/email/activity/ActionBarController;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-boolean v0, p0, Lcom/android/email/activity/ActionBarController;->mTabChanged:Z

    return v0
.end method

.method static synthetic access$402(Lcom/android/email/activity/ActionBarController;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/ActionBarController;->mTabChanged:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/email/activity/ActionBarController;)Landroid/widget/SearchView;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/email/activity/ActionBarController;)Landroid/widget/SearchView$OnQueryTextListener;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mOnQueryText:Landroid/widget/SearchView$OnQueryTextListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/email/activity/ActionBarController;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->refreshInernal()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/email/activity/ActionBarController;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/email/activity/ActionBarController;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/ActionBarController;

    iget-boolean v0, p0, Lcom/android/email/activity/ActionBarController;->mIsTitleUpdated:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/email/activity/ActionBarController;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/ActionBarController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/ActionBarController;->mIsTitleUpdated:Z

    return p1
.end method

.method private enterCustomActionBarMode()V
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x12

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method private enterMultiLineTitleCustomActionBarMode()V
    .locals 2

    const/16 v1, 0x3a

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method private enterSingleLineTitleActionBarMode()V
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0xa

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method private initRemoteSearchViews()V
    .locals 4

    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerCountView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0, v3}, Lcom/android/email/activity/ActionBarController;->setMailSwitcherVisible(Z)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    const v2, 0x7f0f0057

    invoke-static {v1, v2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    const v2, 0x7f0f0058

    invoke-static {v1, v2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private initSearchFieldsUI()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    const v1, 0x7f0f0059

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->useListMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->initSpinner()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->initTabs()V

    goto :goto_0
.end method

.method private initSearchViews()V
    .locals 3

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->useListMode()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    :goto_0
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    const v2, 0x7f0f005a

    invoke-static {v1, v2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mOnQueryText:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->onActionViewExpanded()V

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->initSearchFieldsUI()V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    goto :goto_0
.end method

.method private initSpinner()V
    .locals 9

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    const v7, 0x7f080032

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    const-string v8, "ALL"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v7, 0x7f080030

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    const-string v8, "FROM"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v7, 0x7f08002f

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    const-string v8, "TO"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v7, 0x7f08002e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    const-string v8, "SUBJECT"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v7, 0x7f080031

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    const-string v8, "BODY"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    const v8, 0x1090009

    invoke-direct {v0, v7, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    new-instance v8, Lcom/android/email/activity/ActionBarController$DropDownListener;

    invoke-direct {v8, p0, v4}, Lcom/android/email/activity/ActionBarController$DropDownListener;-><init>(Lcom/android/email/activity/ActionBarController;Ljava/util/ArrayList;)V

    invoke-virtual {v7, v0, v8}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mRestoredSelectedTab:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mRestoredSelectedTab:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mFieldList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v7, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    sget-object v7, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v6, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private initTabs()V
    .locals 8

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f080032

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-string v5, "ALL"

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v2, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f080030

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-string v5, "FROM"

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f08002f

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-string v5, "TO"

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f08002e

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-string v5, "SUBJECT"

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f080031

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mTabListener:Lcom/android/email/activity/ActionBarController$TabListener;

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-string v5, "BODY"

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mRestoredSelectedTab:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRestoredSelectedTab:Ljava/lang/String;

    :goto_0
    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getTabCount()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v0}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_3

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    sget-object v5, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v1, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private loadAccountMailboxInfo(JJ)V
    .locals 9
    .param p1    # J
    .param p3    # J

    const/4 v8, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/ActionBarController;->mIsTitleUpdated:Z

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mLoaderManager:Landroid/app/LoaderManager;

    const/16 v7, 0xc9

    new-instance v0, Lcom/android/email/activity/ActionBarController$5;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/ActionBarController$5;-><init>(Lcom/android/email/activity/ActionBarController;JJ)V

    invoke-virtual {v6, v7, v8, v0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    iget-object v6, p0, Lcom/android/email/activity/ActionBarController;->mLoaderManager:Landroid/app/LoaderManager;

    const/16 v7, 0xc8

    new-instance v0, Lcom/android/email/activity/ActionBarController$6;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/ActionBarController$6;-><init>(Lcom/android/email/activity/ActionBarController;JJ)V

    invoke-virtual {v6, v7, v8, v0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method private onAccountSpinnerItemClicked(I)V
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-virtual {v2, p1}, Lcom/android/email/activity/AccountSelectorAdapter;->getAccountId(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-virtual {v2, p1}, Lcom/android/email/activity/AccountSelectorAdapter;->isAccountItem(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v2, v0, v1}, Lcom/android/email/activity/ActionBarController$Callback;->onAccountSelected(J)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-virtual {v2, p1}, Lcom/android/email/activity/AccountSelectorAdapter;->isMailboxItem(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    iget-object v3, p0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    invoke-virtual {v3, p1}, Lcom/android/email/activity/AccountSelectorAdapter;->getId(I)J

    move-result-wide v3

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/android/email/activity/ActionBarController$Callback;->onMailboxSelected(JJ)V

    goto :goto_0
.end method

.method private refreshInernal()V
    .locals 8

    const/4 v6, 0x4

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v7}, Lcom/android/email/activity/ActionBarController$Callback;->shouldShowUp()Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_0
    const/4 v4, 0x1

    :goto_0
    iget-object v7, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    if-eqz v4, :cond_1

    move v5, v6

    :cond_1
    invoke-virtual {v7, v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v5}, Lcom/android/email/activity/ActionBarController$Callback;->getUIAccountId()J

    move-result-wide v0

    iget-object v5, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v5}, Lcom/android/email/activity/ActionBarController$Callback;->getMailboxId()J

    move-result-wide v2

    iget-wide v5, p0, Lcom/android/email/activity/ActionBarController;->mLastAccountIdForDirtyCheck:J

    cmp-long v5, v5, v0

    if-nez v5, :cond_2

    iget-wide v5, p0, Lcom/android/email/activity/ActionBarController;->mLastMailboxIdForDirtyCheck:J

    cmp-long v5, v5, v2

    if-eqz v5, :cond_3

    :cond_2
    iput-wide v0, p0, Lcom/android/email/activity/ActionBarController;->mLastAccountIdForDirtyCheck:J

    iput-wide v2, p0, Lcom/android/email/activity/ActionBarController;->mLastMailboxIdForDirtyCheck:J

    const-wide/16 v5, -0x1

    cmp-long v5, v0, v5

    if-eqz v5, :cond_3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/email/activity/ActionBarController;->loadAccountMailboxInfo(JJ)V

    :cond_3
    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->updateTitle()V

    return-void

    :cond_4
    move v4, v5

    goto :goto_0
.end method

.method private setMailSwitcherVisible(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    invoke-static {v0, v1}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    invoke-static {v0, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    invoke-static {v0, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private setSpinnerEnabled(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setClickable(Z)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerDefaultBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method private shouldShowRemoteSearchTitle()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isRemoteSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowSearchBar()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateTitle()V
    .locals 23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountsSelectorAdapter:Lcom/android/email/activity/AccountSelectorAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->enterCustomActionBarMode()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    if-nez v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mDefaultLayoutParams:Landroid/app/ActionBar$LayoutParams;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountCount()I

    move-result v19

    if-nez v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getLookUpTitleOnly()Z

    move-result v19

    if-nez v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/email/activity/ActionBarController$Callback;->onNoAccountsFound()V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountId()J

    move-result-wide v19

    const-wide/16 v21, -0x1

    cmp-long v19, v19, v21

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->accountExists()Z

    move-result v19

    if-nez v19, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->exitSearchMode()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/emailcommon/provider/Account;->getDefaultAccountId(Landroid/content/Context;)J

    move-result-wide v5

    const-wide/16 v19, -0x1

    cmp-long v19, v5, v19

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v6}, Lcom/android/email/activity/ActionBarController$Callback;->onAccountSelected(J)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/email/activity/ActionBarController$Callback;->getTitleMode()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->shouldShowRemoteSearchTitle()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->initRemoteSearchViews()V

    goto/16 :goto_0

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->shouldShowSearchBar()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->initSearchViews()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/email/activity/ActionBarController;->mShouldClearSearchFocus:Z

    move/from16 v19, v0

    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/SearchView;->clearFocus()V

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mShouldClearSearchFocus:Z

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/email/activity/ActionBarController;->mBackFromMessageView:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->useListMode()Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mTabChanged:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mOnQueryText:Landroid/widget/SearchView$OnQueryTextListener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mTabChanged:Z

    :cond_7
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mBackFromMessageView:Z

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/app/ActionBar;->setNavigationMode(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mSearchContainer:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-static/range {v19 .. v20}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchContainer:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-static/range {v19 .. v20}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    move/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerCountView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/email/activity/ActionBarController;->setMailSwitcherVisible(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v19

    if-eqz v19, :cond_9

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mBackFromMessageView:Z

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/email/activity/ActionBarController;->mShouldClearSearchFocus:Z

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->enterMultiLineTitleCustomActionBarMode()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mMailSwitcher:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v19

    const/high16 v20, -0x80000000

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mMailSwitcher:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mMailSwitcher:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    :goto_1
    new-instance v10, Landroid/app/ActionBar$LayoutParams;

    const/16 v19, -0x1

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v9, v0, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBarCustomView:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/android/email/activity/ActionBarController$Callback;->getMessageSubject()Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_a

    const-string v16, ""

    :cond_a
    new-instance v17, Landroid/text/SpannableString;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v19, Landroid/text/style/TextAppearanceSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0c0015

    invoke-direct/range {v19 .. v21}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x10

    if-eqz v19, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->shouldEnableSpinner()Z

    move-result v19

    if-eqz v19, :cond_11

    const/4 v15, 0x1

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/email/activity/ActionBarController;->setSpinnerEnabled(Z)V

    goto/16 :goto_0

    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_d

    invoke-direct/range {p0 .. p0}, Lcom/android/email/activity/ActionBarController;->enterSingleLineTitleActionBarMode()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAllFoldersLabel:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountDisplayName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/email/activity/ActionBarController;->mTitleMode:I

    move/from16 v19, v0

    const/16 v20, 0x12

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getMailboxDisplayName()Ljava/lang/String;

    move-result-object v14

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->setSingleLine()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountDisplayName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountId()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getMailboxId()J

    move-result-wide v12

    invoke-static {v5, v6}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v19

    if-eqz v19, :cond_e

    const-wide/16 v19, 0x0

    cmp-long v19, v12, v19

    if-lez v19, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v5, v6}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v12, v13}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v11

    sget-object v19, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v20, "eng"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    if-eqz v7, :cond_e

    iget v0, v7, Lcom/android/emailcommon/provider/Account;->mSyncInterval:I

    move/from16 v19, v0

    const/16 v20, -0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    if-eqz v11, :cond_e

    iget v0, v11, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    move/from16 v19, v0

    if-nez v19, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, v11, Lcom/android/emailcommon/provider/Mailbox;->mSyncInterval:I

    move/from16 v19, v0

    packed-switch v19, :pswitch_data_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f020058

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_e
    :goto_6
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/email/activity/ActionBarController;->setMailSwitcherVisible(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerCountView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerCountView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getMailboxMessageCount()I

    move-result v21

    const/16 v22, 0x1

    invoke-static/range {v20 .. v22}, Lcom/android/email/activity/UiUtilities;->getMessageCountForUi(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_f
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine1View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSpinnerLine2View:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mCursor:Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/email/activity/AccountSelectorAdapter$CursorWithExtras;->getAccountDisplayName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f020056

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/activity/ActionBarController;->mAccountSyncIntervalView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f020057

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    :cond_11
    const/4 v15, 0x0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private useListMode()Z
    .locals 2

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public enterLocalSearchMode(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->initSearchViews()V

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    iput-boolean v1, p0, Lcom/android/email/activity/ActionBarController;->mShouldClearSearchFocus:Z

    :goto_1
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v1}, Lcom/android/email/activity/ActionBarController$Callback;->getSearchHint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->refresh()V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v0}, Lcom/android/email/activity/ActionBarController$Callback;->onSearchStarted()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_1
.end method

.method public enterRemoteSearchMode(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->initRemoteSearchViews()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mRemoteSearchTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    const v2, 0x7f08002c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/android/email/activity/ActionBarController;->mFilter:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    return-void

    :cond_0
    const-string v0, "ActionBarController"

    const-string v1, "enterRemoteSearchMode initialQueryTerm is null "

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public exitSearchMode()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isRemoteSearchMode()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->isLocalSearchMode()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->refresh()V

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v1}, Lcom/android/email/activity/ActionBarController$Callback;->onSearchExit()V

    goto :goto_0
.end method

.method public getQueryTermIfSearchBody()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    :goto_0
    const-string v2, "BODY"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ALL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1

    :cond_2
    sget-object v2, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v0, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    goto :goto_0
.end method

.method public isLocalSearchMode()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoteSearchMode()Z
    .locals 2

    iget v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->refresh()V

    return-void
.end method

.method public onActivityDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mAccountDropdown:Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    sput-object v0, Lcom/android/email/activity/ActionBarController;->sActionBarSelectedTab:Ljava/lang/String;

    return-void
.end method

.method public onBackPressed(Z)Z
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/email/activity/ActionBarController;->shouldShowSearchBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController;->exitSearchMode()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const-string v1, "ActionBarController.BUNDLE_KEY_MODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "ActionBarController.ACTION_BAR_SELECTED_TAB"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRestoredSelectedTab:Ljava/lang/String;

    const-string v1, "ActionBarController.FILTER_STRING"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/ActionBarController;->mFilter:Ljava/lang/String;

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/email/activity/ActionBarController;->enterLocalSearchMode(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mFilter:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/email/activity/ActionBarController;->enterRemoteSearchMode(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v1}, Lcom/android/email/activity/ActionBarController$Callback;->getSearchHint()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ActionBarController.BUNDLE_SEARCH_HINT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/email/activity/ActionBarController;->setSearchHint(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mDelayedOperations:Lcom/android/emailcommon/utility/DelayedOperations;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/DelayedOperations;->removeCallbacks()V

    const-string v0, "ActionBarController.BUNDLE_KEY_MODE"

    iget v1, p0, Lcom/android/email/activity/ActionBarController;->mSearchMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "ActionBarController.BUNDLE_SEARCH_HINT"

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mCallback:Lcom/android/email/activity/ActionBarController$Callback;

    invoke-interface {v1}, Lcom/android/email/activity/ActionBarController$Callback;->getSearchHint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ActionBarController.ACTION_BAR_SELECTED_TAB"

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mActionBarSelectedTab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ActionBarController.FILTER_STRING"

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mFilter:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public refresh()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mDelayedOperations:Lcom/android/emailcommon/utility/DelayedOperations;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/DelayedOperations;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mDelayedOperations:Lcom/android/emailcommon/utility/DelayedOperations;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/DelayedOperations;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSearchHint(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setSwitchNewerEnable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchNewerView:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setSwitchOlderEnable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSwitchOlderView:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public updateSearchCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController;->mSearchResultCountView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/android/email/activity/UiUtilities;->getMessageCountForUi(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
