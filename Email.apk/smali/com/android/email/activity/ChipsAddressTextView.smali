.class Lcom/android/email/activity/ChipsAddressTextView;
.super Lcom/android/ex/chips/MTKRecipientEditTextView;
.source "ChipsAddressTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;
    }
.end annotation


# static fields
.field private static final ADD_POST_DELAY:J = 0x12cL

.field public static final AUTO_SEARCH_THRESHOLD_LENGTH:I = 0x1

.field private static final DELETE_KEY_POST_DELAY:J = 0x1f4L


# instance fields
.field private final mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;-><init>(Lcom/android/email/activity/ChipsAddressTextView$1;)V

    iput-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    iget-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    invoke-super {p0, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    return-void
.end method


# virtual methods
.method public enoughToFilter()Z
    .locals 4

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v2, 0x2

    if-le v0, v2, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v0, -0x2

    invoke-interface {v1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_0

    add-int/lit8 v2, v0, -0x2

    invoke-interface {v1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not perfom GAL search for current text is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->enoughToFilter()Z

    move-result v2

    goto :goto_0
.end method

.method public setGalSearchDelayer()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/email/activity/ChipsAddressTextView$1;

    invoke-direct {v1, p0}, Lcom/android/email/activity/ChipsAddressTextView$1;-><init>(Lcom/android/email/activity/ChipsAddressTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->setDelayer(Landroid/widget/Filter$Delayer;)V

    :cond_0
    return-void
.end method

.method public setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V
    .locals 1
    .param p1    # Landroid/widget/AutoCompleteTextView$Validator;

    iget-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    return-void
.end method
