.class public Lcom/android/email/activity/setup/DropdownAddressFilter;
.super Lcom/android/email/activity/setup/DropdownAccountsFilter;
.source "DropdownAddressFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/android/email/activity/setup/DropdownAccountsFilter",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter",
            "<TT;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/email/activity/setup/DropdownAccountsFilter;-><init>(Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;)V

    return-void
.end method


# virtual methods
.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/widget/Filter$FilterResults;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mFilterString:Ljava/lang/String;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mObjects:Ljava/util/List;

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mReferenceAdapter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    iget-object v1, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mObjects:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->setObjects(Ljava/util/List;)V

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mReferenceAdapter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/setup/DropdownAccountsFilter;->mReferenceAdapter:Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    goto :goto_1
.end method
