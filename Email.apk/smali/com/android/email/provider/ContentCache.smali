.class public final Lcom/android/email/provider/ContentCache;
.super Ljava/lang/Object;
.source "ContentCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/provider/ContentCache$Statistics;,
        Lcom/android/email/provider/ContentCache$CacheCounter;,
        Lcom/android/email/provider/ContentCache$CachedCursor;,
        Lcom/android/email/provider/ContentCache$CacheToken;,
        Lcom/android/email/provider/ContentCache$TokenList;,
        Lcom/android/email/provider/ContentCache$CounterMap;
    }
.end annotation


# static fields
.field private static final DEBUG_CACHE:Z = false

.field private static final DEBUG_NOT_CACHEABLE:Z = false

.field private static final DEBUG_STATISTICS:Z = false

.field private static final DEBUG_TOKENS:Z = false

.field private static final READ_CACHE_ENABLED:Z = true

.field static final sActiveCursors:Lcom/android/email/provider/ContentCache$CounterMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/email/provider/ContentCache$CounterMap",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private static final sContentCaches:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/email/provider/ContentCache;",
            ">;"
        }
    .end annotation
.end field

.field private static sLockCache:Z

.field private static sNotCacheable:I

.field private static final sNotCacheableMap:Lcom/android/email/provider/ContentCache$CounterMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/email/provider/ContentCache$CounterMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBaseProjection:[Ljava/lang/String;

.field private final mLockMap:Lcom/android/email/provider/ContentCache$CounterMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/email/provider/ContentCache$CounterMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLogTag:Ljava/lang/String;

.field private final mLruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mStats:Lcom/android/email/provider/ContentCache$Statistics;

.field mTokenList:Lcom/android/email/provider/ContentCache$TokenList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput v0, Lcom/android/email/provider/ContentCache;->sNotCacheable:I

    new-instance v0, Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-direct {v0}, Lcom/android/email/provider/ContentCache$CounterMap;-><init>()V

    sput-object v0, Lcom/android/email/provider/ContentCache;->sNotCacheableMap:Lcom/android/email/provider/ContentCache$CounterMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/email/provider/ContentCache;->sContentCaches:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/email/provider/ContentCache$CounterMap;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Lcom/android/email/provider/ContentCache$CounterMap;-><init>(I)V

    sput-object v0, Lcom/android/email/provider/ContentCache;->sActiveCursors:Lcom/android/email/provider/ContentCache$CounterMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/email/provider/ContentCache$CounterMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/android/email/provider/ContentCache$CounterMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/email/provider/ContentCache;->mLockMap:Lcom/android/email/provider/ContentCache$CounterMap;

    iput-object p1, p0, Lcom/android/email/provider/ContentCache;->mName:Ljava/lang/String;

    new-instance v0, Lcom/android/email/provider/ContentCache$1;

    invoke-direct {v0, p0, p3}, Lcom/android/email/provider/ContentCache$1;-><init>(Lcom/android/email/provider/ContentCache;I)V

    iput-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    iput-object p2, p0, Lcom/android/email/provider/ContentCache;->mBaseProjection:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContentCache-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/provider/ContentCache;->mLogTag:Ljava/lang/String;

    sget-object v0, Lcom/android/email/provider/ContentCache;->sContentCaches:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/email/provider/ContentCache$TokenList;

    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/android/email/provider/ContentCache$TokenList;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    new-instance v0, Lcom/android/email/provider/ContentCache$Statistics;

    invoke-direct {v0, p0}, Lcom/android/email/provider/ContentCache$Statistics;-><init>(Lcom/android/email/provider/ContentCache;)V

    iput-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/provider/ContentCache;)Landroid/util/LruCache;
    .locals 1
    .param p0    # Lcom/android/email/provider/ContentCache;

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/email/provider/ContentCache;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/provider/ContentCache;

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/email/provider/ContentCache;)Lcom/android/email/provider/ContentCache$Statistics;
    .locals 1
    .param p0    # Lcom/android/email/provider/ContentCache;

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    return-object v0
.end method

.method private static dumpNotCacheableQueries()V
    .locals 12

    sget-object v9, Lcom/android/email/provider/ContentCache;->sNotCacheableMap:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v9}, Lcom/android/email/provider/ContentCache$CounterMap;->size()I

    move-result v8

    new-array v1, v8, [Lcom/android/email/provider/ContentCache$CacheCounter;

    const/4 v4, 0x0

    sget-object v9, Lcom/android/email/provider/ContentCache;->sNotCacheableMap:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v9}, Lcom/android/email/provider/ContentCache$CounterMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    add-int/lit8 v5, v4, 0x1

    new-instance v11, Lcom/android/email/provider/ContentCache$CacheCounter;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-direct {v11, v9, v10}, Lcom/android/email/provider/ContentCache$CacheCounter;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v11, v1, v4

    move v4, v5

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    move-object v0, v1

    array-length v7, v0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_1

    aget-object v2, v0, v6

    const-string v9, "NotCacheable"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v2, Lcom/android/email/provider/ContentCache$CacheCounter;->count:Ljava/lang/Integer;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v2, Lcom/android/email/provider/ContentCache$CacheCounter;->uri:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private dumpOnCount(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$808(Lcom/android/email/provider/ContentCache$Statistics;)I

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$800(Lcom/android/email/provider/ContentCache$Statistics;)I

    move-result v0

    rem-int/2addr v0, p1

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/email/provider/ContentCache;->dumpStats()V

    :cond_0
    return-void
.end method

.method public static dumpStats()V
    .locals 5

    new-instance v2, Lcom/android/email/provider/ContentCache$Statistics;

    const-string v3, "Totals"

    invoke-direct {v2, v3}, Lcom/android/email/provider/ContentCache$Statistics;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/android/email/provider/ContentCache;->sContentCaches:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/provider/ContentCache;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/email/provider/ContentCache;->mName:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-virtual {v4}, Lcom/android/email/provider/ContentCache$Statistics;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2, v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$1500(Lcom/android/email/provider/ContentCache$Statistics;Lcom/android/email/provider/ContentCache;)V

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/android/email/provider/ContentCache$Statistics;->access$1600(Lcom/android/email/provider/ContentCache$Statistics;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/email/provider/ContentCache$Statistics;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getCachedCursorImpl(Ljava/lang/String;)Lcom/android/email/provider/ContentCache$CachedCursor;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/email/provider/ContentCache;->get(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v1}, Lcom/android/email/provider/ContentCache$Statistics;->access$308(Lcom/android/email/provider/ContentCache$Statistics;)I

    new-instance v1, Lcom/android/email/provider/ContentCache$CachedCursor;

    invoke-direct {v1, v0, p0, p1}, Lcom/android/email/provider/ContentCache$CachedCursor;-><init>(Landroid/database/Cursor;Lcom/android/email/provider/ContentCache;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v1}, Lcom/android/email/provider/ContentCache$Statistics;->access$408(Lcom/android/email/provider/ContentCache$Statistics;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getMatrixCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/email/provider/ContentCache;->getMatrixCursor(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method

.method private getMatrixCursor(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Landroid/database/MatrixCursor;
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;

    invoke-virtual/range {p0 .. p1}, Lcom/android/email/provider/ContentCache;->get(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_8

    new-instance v9, Landroid/database/MatrixCursor;

    const/4 v14, 0x1

    move-object/from16 v0, p2

    invoke-direct {v9, v0, v14}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-nez v14, :cond_0

    :goto_0
    return-object v9

    :cond_0
    move-object/from16 v0, p2

    array-length v14, v0

    new-array v10, v14, [Ljava/lang/Object;

    if-eqz p3, :cond_1

    new-instance v13, Landroid/content/ContentValues;

    move-object/from16 v0, p3

    invoke-direct {v13, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 p3, v13

    :cond_1
    const/4 v5, 0x0

    move-object/from16 v1, p2

    array-length v8, v1

    const/4 v7, 0x0

    move v6, v5

    :goto_1
    if-ge v7, v8, :cond_6

    aget-object v3, v1, v7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_2

    iget-object v14, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v14}, Lcom/android/email/provider/ContentCache$Statistics;->access$508(Lcom/android/email/provider/ContentCache$Statistics;)I

    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    instance-of v14, v11, Ljava/lang/Boolean;

    if-eqz v14, :cond_4

    sget-object v14, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v11, v14, :cond_3

    const-string v12, "1"

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :goto_3
    add-int/lit8 v5, v6, 0x1

    aput-object v12, v10, v6

    add-int/lit8 v7, v7, 0x1

    move v6, v5

    goto :goto_1

    :cond_3
    const-string v12, "0"

    goto :goto_2

    :cond_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto :goto_2

    :cond_5
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_3

    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual/range {p3 .. p3}, Landroid/content/ContentValues;->size()I

    move-result v14

    if-eqz v14, :cond_7

    const/4 v9, 0x0

    goto :goto_0

    :cond_7
    invoke-virtual {v9, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    iget-object v14, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v14}, Lcom/android/email/provider/ContentCache$Statistics;->access$308(Lcom/android/email/provider/ContentCache$Statistics;)I

    goto :goto_0

    :cond_8
    iget-object v14, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v14}, Lcom/android/email/provider/ContentCache$Statistics;->access$408(Lcom/android/email/provider/ContentCache$Statistics;)I

    const/4 v9, 0x0

    goto :goto_0
.end method

.method public static invalidateAllCaches()V
    .locals 3

    sget-object v2, Lcom/android/email/provider/ContentCache;->sContentCaches:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/provider/ContentCache;

    invoke-virtual {v0}, Lcom/android/email/provider/ContentCache;->invalidate()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static declared-synchronized notCacheable(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/String;

    const-class v0, Lcom/android/email/provider/ContentCache;

    monitor-enter v0

    monitor-exit v0

    return-void
.end method

.method public static setLockCacheForTest(Z)V
    .locals 1
    .param p0    # Z

    sput-boolean p0, Lcom/android/email/provider/ContentCache;->sLockCache:Z

    sget-boolean v0, Lcom/android/email/provider/ContentCache;->sLockCache:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/email/provider/ContentCache;->invalidateAllCaches()V

    :cond_0
    return-void
.end method

.method private unlockImpl(Ljava/lang/String;Landroid/content/ContentValues;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/android/email/provider/ContentCache;->get(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_0

    :cond_0
    if-eqz p2, :cond_5

    sget-boolean v2, Lcom/android/email/provider/ContentCache;->sLockCache:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mBaseProjection:[Ljava/lang/String;

    invoke-direct {p0, p1, v2, p2}, Lcom/android/email/provider/ContentCache;->getMatrixCursor(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Landroid/database/MatrixCursor;

    move-result-object v1

    if-eqz v1, :cond_4

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_1

    :cond_1
    invoke-virtual {v1}, Landroid/database/AbstractCursor;->moveToFirst()Z

    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    sget-object v2, Lcom/android/email/provider/ContentCache;->sActiveCursors:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v2, v0}, Lcom/android/email/provider/ContentCache$CounterMap;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    if-eqz p3, :cond_3

    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mLockMap:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v2, p1}, Lcom/android/email/provider/ContentCache$CounterMap;->subtract(Ljava/lang/Object;)I

    :cond_3
    return-void

    :cond_4
    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method get(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method public declared-synchronized getCacheToken(Ljava/lang/String;)Lcom/android/email/provider/ContentCache$CacheToken;
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v1, p1}, Lcom/android/email/provider/ContentCache$TokenList;->add(Ljava/lang/String;)Lcom/android/email/provider/ContentCache$CacheToken;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mLockMap:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v1, p1}, Lcom/android/email/provider/ContentCache$CounterMap;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/email/provider/ContentCache$CacheToken;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getCachedCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    :cond_0
    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mBaseProjection:[Ljava/lang/String;

    if-ne p2, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/email/provider/ContentCache;->getCachedCursorImpl(Ljava/lang/String;)Lcom/android/email/provider/ContentCache$CachedCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/android/email/provider/ContentCache;->getMatrixCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/MatrixCursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mBaseProjection:[Ljava/lang/String;

    return-object v0
.end method

.method protected getSnapshot()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized invalidate()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/email/provider/ContentCache;->invalidate(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized invalidate(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$708(Lcom/android/email/provider/ContentCache$Statistics;)I

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v0}, Lcom/android/email/provider/ContentCache$TokenList;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized lock(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mLockMap:Lcom/android/email/provider/ContentCache$CounterMap;

    invoke-virtual {v1, p1}, Lcom/android/email/provider/ContentCache$CounterMap;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v1, p1}, Lcom/android/email/provider/ContentCache$TokenList;->invalidateTokens(Ljava/lang/String;)I

    move-result v0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public putCursor(Landroid/database/Cursor;Ljava/lang/String;[Ljava/lang/String;Lcom/android/email/provider/ContentCache$CacheToken;)Landroid/database/Cursor;
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Lcom/android/email/provider/ContentCache$CacheToken;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/email/provider/ContentCache;->putCursorImpl(Landroid/database/Cursor;Ljava/lang/String;[Ljava/lang/String;Lcom/android/email/provider/ContentCache$CacheToken;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized putCursorImpl(Landroid/database/Cursor;Ljava/lang/String;[Ljava/lang/String;Lcom/android/email/provider/ContentCache$CacheToken;)Landroid/database/Cursor;
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Lcom/android/email/provider/ContentCache$CacheToken;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p4}, Lcom/android/email/provider/ContentCache$CacheToken;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    :cond_0
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v1}, Lcom/android/email/provider/ContentCache$Statistics;->access$208(Lcom/android/email/provider/ContentCache$Statistics;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v1, p4}, Lcom/android/email/provider/ContentCache$TokenList;->remove(Lcom/android/email/provider/ContentCache$CacheToken;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-object p1

    :cond_1
    if-eqz p1, :cond_4

    :try_start_2
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mBaseProjection:[Ljava/lang/String;

    invoke-static {p3, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/android/email/provider/ContentCache;->sLockCache:Z

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_2

    :cond_2
    invoke-virtual {p0, p2}, Lcom/android/email/provider/ContentCache;->get(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p2, v1, v2}, Lcom/android/email/provider/ContentCache;->unlockImpl(Ljava/lang/String;Landroid/content/ContentValues;Z)V

    :cond_3
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v1, p2, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/android/email/provider/ContentCache$CachedCursor;

    invoke-direct {v1, p1, p0, p2}, Lcom/android/email/provider/ContentCache$CachedCursor;-><init>(Landroid/database/Cursor;Lcom/android/email/provider/ContentCache;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v2, p4}, Lcom/android/email/provider/ContentCache$TokenList;->remove(Lcom/android/email/provider/ContentCache$CacheToken;)Z

    move-object p1, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v1, p4}, Lcom/android/email/provider/ContentCache$TokenList;->remove(Lcom/android/email/provider/ContentCache$CacheToken;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lcom/android/email/provider/ContentCache;->mTokenList:Lcom/android/email/provider/ContentCache$TokenList;

    invoke-virtual {v2, p4}, Lcom/android/email/provider/ContentCache$TokenList;->remove(Lcom/android/email/provider/ContentCache$CacheToken;)Z

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method recordQueryTime(Landroid/database/Cursor;J)V
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # J

    instance-of v0, p1, Lcom/android/email/provider/ContentCache$CachedCursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0, p2, p3}, Lcom/android/email/provider/ContentCache$Statistics;->access$914(Lcom/android/email/provider/ContentCache$Statistics;J)J

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$1008(Lcom/android/email/provider/ContentCache$Statistics;)J

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0, p2, p3}, Lcom/android/email/provider/ContentCache$Statistics;->access$1114(Lcom/android/email/provider/ContentCache$Statistics;J)J

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mStats:Lcom/android/email/provider/ContentCache$Statistics;

    invoke-static {v0}, Lcom/android/email/provider/ContentCache$Statistics;->access$1208(Lcom/android/email/provider/ContentCache$Statistics;)J

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/provider/ContentCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    return v0
.end method

.method public declared-synchronized unlock(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0, v1}, Lcom/android/email/provider/ContentCache;->unlockImpl(Ljava/lang/String;Landroid/content/ContentValues;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unlock(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/android/email/provider/ContentCache;->unlockImpl(Ljava/lang/String;Landroid/content/ContentValues;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
