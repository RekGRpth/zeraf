.class public Lcom/mediatek/email/emailvip/utils/MessageViewUtils;
.super Ljava/lang/Object;
.source "MessageViewUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;,
        Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;,
        Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;,
        Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;
    }
.end annotation


# static fields
.field private static final CONTACT_STATUS_STATE_LOADED:I = 0x2

.field private static final CONTACT_STATUS_STATE_UNLOADED:I = 0x0

.field private static final CONTACT_STATUS_STATE_UNLOADED_TRIGGERED:I = 0x1

.field private static mIsTwoPaneUi:Z

.field private static mWindowWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$600()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->mIsTwoPaneUi:Z

    return v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->mWindowWidth:I

    return v0
.end method

.method public static getAllAddresses(Lcom/android/emailcommon/provider/EmailContent$Message;)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;
    .locals 7
    .param p0    # Lcom/android/emailcommon/provider/EmailContent$Message;

    const/4 v6, 0x0

    if-nez p0, :cond_0

    new-array v0, v6, [Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    :goto_0
    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->unpack(Ljava/lang/String;I)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v3

    iget-object v4, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->unpack(Ljava/lang/String;I)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v2

    iget-object v4, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->unpack(Ljava/lang/String;I)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v1

    array-length v4, v3

    array-length v5, v2

    add-int/2addr v4, v5

    array-length v5, v1

    add-int/2addr v4, v5

    new-array v0, v4, [Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    array-length v4, v3

    invoke-static {v3, v6, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v3

    array-length v5, v2

    invoke-static {v2, v6, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v3

    array-length v5, v2

    add-int/2addr v4, v5

    array-length v5, v1

    invoke-static {v1, v6, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static getPopupWindow(Landroid/content/Context;)Landroid/widget/PopupWindow;
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04004f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->useTwoPane(Landroid/content/Context;)Z

    move-result v3

    sput-boolean v3, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->mIsTwoPaneUi:Z

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    sput v3, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->mWindowWidth:I

    new-instance v0, Landroid/widget/PopupWindow;

    sget v3, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->mWindowWidth:I

    const v4, 0x7f0a0037

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    const v3, 0x7f02001a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    return-object v0
.end method

.method public static getSpannableString(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;Z)Landroid/text/SpannableString;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;
    .param p2    # Z

    const/4 v6, 0x1

    if-nez p1, :cond_0

    new-instance v3, Landroid/text/SpannableString;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p1}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v3, Landroid/text/SpannableString;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-boolean v4, p1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    if-nez v4, :cond_2

    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    const v2, 0x7f02002c

    :goto_1
    new-instance v0, Landroid/text/style/ImageSpan;

    invoke-direct {v0, p0, v2, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v3, v0, v4, v6, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_3
    const v2, 0x7f02002b

    goto :goto_1
.end method

.method public static removeLastComma(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 3
    .param p0    # Landroid/text/SpannableStringBuilder;

    if-nez p0, :cond_1

    new-instance p0, Landroid/text/SpannableStringBuilder;

    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    add-int/lit8 v1, v0, -0x2

    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x2c

    if-ne v1, v2, :cond_0

    add-int/lit8 v1, v0, -0x2

    invoke-virtual {p0, v1, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static setAddressView(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;)V
    .locals 5
    .param p0    # Landroid/widget/TextView;
    .param p1    # Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    int-to-float v3, v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v2, v3, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static setDetailsLayout(Lcom/android/email/activity/MessageViewFragmentBase;[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;[Landroid/widget/LinearLayout;II)V
    .locals 14
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # [Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;
    .param p2    # [Landroid/widget/LinearLayout;
    .param p3    # I
    .param p4    # I

    array-length v10, p1

    if-nez v10, :cond_1

    :cond_0
    return-void

    :cond_1
    array-length v10, p1

    move/from16 v0, p4

    if-le v0, v10, :cond_2

    array-length v0, p1

    move/from16 p4, v0

    :cond_2
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0035

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v2}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x106000c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    new-instance v4, Landroid/text/style/ImageSpan;

    const v10, 0x7f02002b

    const/4 v11, 0x1

    invoke-direct {v4, v2, v10, v11}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/4 v8, 0x0

    new-instance v5, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-static {v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getPopupWindow(Landroid/content/Context;)Landroid/widget/PopupWindow;

    move-result-object v10

    invoke-direct {v5, p0, v10}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Landroid/widget/PopupWindow;)V

    const/4 v9, 0x0

    const/4 v7, 0x0

    move/from16 v3, p3

    :goto_0
    move/from16 v0, p4

    if-ge v3, v0, :cond_0

    new-instance v9, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;

    invoke-direct {v9, v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;-><init>(Landroid/content/Context;)V

    aget-object v10, p1, v3

    invoke-virtual {v9, v10}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->setAdress(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    const v10, 0x7f02000d

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v9, v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->setPadding(I)V

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v9}, Landroid/widget/TextView;->setSingleLine()V

    aget-object v10, p1, v3

    invoke-virtual {v10}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    aget-object v10, p1, v3

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->setVip(Z)V

    new-instance v8, Landroid/text/SpannableString;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, p1, v3

    invoke-virtual {v11}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/16 v12, 0x21

    invoke-virtual {v8, v4, v10, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v9, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v10, p1, v3

    iget v10, v10, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mParentLayoutFlag:I

    add-int/lit8 v10, v10, -0x1

    aget-object v7, p2, v10

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, -0x2

    invoke-direct {v10, v11, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v10, Landroid/view/View;

    invoke-direct {v10, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v11, Landroid/view/ViewGroup$LayoutParams;

    const/4 v12, -0x1

    const/16 v13, 0x9

    invoke-direct {v11, v12, v13}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v10, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget-object v10, p1, v3

    invoke-virtual {v10}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static setRowVisibility(Ljava/lang/String;Landroid/view/View;I)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static updateAddressesView(Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 13
    .param p0    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TextView;

    const/16 v7, 0xa

    if-eqz p0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v10, Landroid/text/SpannableStringBuilder;

    invoke-direct {v10}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getAllAddresses(Lcom/android/emailcommon/provider/EmailContent$Message;)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v0

    array-length v6, v0

    if-nez v6, :cond_2

    const-string v12, ""

    invoke-virtual {p2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-ge v6, v7, :cond_3

    move v7, v6

    :cond_3
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_5

    aget-object v11, v0, v5

    invoke-virtual {v11}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v12

    invoke-virtual {v11, v12}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->setVip(Z)V

    iget v12, v11, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mParentLayoutFlag:I

    packed-switch v12, :pswitch_data_0

    :cond_4
    :goto_2
    const/4 v12, 0x0

    invoke-static {p1, v11, v12}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getSpannableString(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;Z)Landroid/text/SpannableString;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v12, ", "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :pswitch_0
    if-nez v3, :cond_4

    const v12, 0x7f0800d9

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Utility;->appendBold(Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v3, 0x1

    goto :goto_2

    :pswitch_1
    if-nez v2, :cond_4

    invoke-static {v10}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->removeLastComma(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v12, 0x7f0800da

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Utility;->appendBold(Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x1

    goto :goto_2

    :pswitch_2
    if-nez v1, :cond_4

    invoke-static {v10}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->removeLastComma(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v12, 0x7f0800db

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Utility;->appendBold(Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    invoke-static {v10}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->removeLastComma(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v12

    if-eqz v12, :cond_6

    invoke-static {p2, v9}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setAddressView(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;)V

    goto/16 :goto_0

    :cond_6
    instance-of v12, p2, Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;

    if-eqz v12, :cond_7

    move-object v4, p2

    check-cast v4, Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;

    new-instance v12, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$1;

    invoke-direct {v12, v4, v9}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$1;-><init>(Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v4, v12}, Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;->setOnDrawnListener(Lcom/mediatek/email/emailvip/utils/EllipsizeTextView$OnDrawnListener;)V

    goto/16 :goto_0

    :cond_7
    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p2, v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static updateDetailsExpanded(Landroid/content/Context;Landroid/view/View;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const v3, 0x7f0f00bf

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const v3, 0x7f0f00c1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v3, 0x7f0f00c3

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {p0, v2, p2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateVipTextView(Landroid/content/Context;Landroid/widget/LinearLayout;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    invoke-static {p0, v1, p2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateVipTextView(Landroid/content/Context;Landroid/widget/LinearLayout;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    invoke-static {p0, v0, p2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateVipTextView(Landroid/content/Context;Landroid/widget/LinearLayout;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    goto :goto_0
.end method

.method public static updateVipInformation(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/view/View;Landroid/widget/TextView;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/widget/TextView;
    .param p4    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    if-nez p4, :cond_0

    invoke-static {p1, p0, p3}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateAddressesView(Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Context;Landroid/widget/TextView;)V

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateDetailsExpanded(Landroid/content/Context;Landroid/view/View;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p2, p4}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateDetailsExpanded(Landroid/content/Context;Landroid/view/View;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    goto :goto_0
.end method

.method public static updateVipTextView(Landroid/content/Context;Landroid/widget/LinearLayout;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_2

    instance-of v7, v5, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;

    if-eqz v7, :cond_2

    move-object v6, v5

    check-cast v6, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;

    invoke-virtual {v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->getAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v4

    if-nez p2, :cond_4

    invoke-virtual {v4}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v3

    iget-boolean v7, v4, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    if-eq v7, v3, :cond_2

    if-eqz v3, :cond_3

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->setVip(Z)V

    :goto_1
    invoke-static {p0, v4, v8}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getSpannableString(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;Z)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v4, v8}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->setVip(Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v4}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p2, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    invoke-virtual {v4, v7}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->setVip(Z)V

    invoke-static {p0, v4, v8}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getSpannableString(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;Z)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
