.class public Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;
.super Ljava/lang/Object;
.source "MessageViewUtils.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/emailvip/utils/MessageViewUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VipViewPhotoLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/android/email/activity/ContactStatusLoader$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final BUNDLE_EMAIL_ADDRESS:Ljava/lang/String; = "email"


# instance fields
.field private final mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;


# direct methods
.method public constructor <init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    return-void
.end method

.method public static createArguments(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "email"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/email/activity/ContactStatusLoader$Result;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/email/activity/ContactStatusLoader;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    const-string v2, "email"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/email/activity/ContactStatusLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Lcom/android/email/activity/ContactStatusLoader$Result;)V
    .locals 3
    .param p2    # Lcom/android/email/activity/ContactStatusLoader$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/email/activity/ContactStatusLoader$Result;",
            ">;",
            "Lcom/android/email/activity/ContactStatusLoader$Result;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    if-ne v1, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    const/4 v2, 0x2

    iput v2, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v2, p2, Lcom/android/email/activity/ContactStatusLoader$Result;->mLookupUri:Landroid/net/Uri;

    iput-object v2, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mQuickContactLookupUri:Landroid/net/Uri;

    iget-object v1, p2, Lcom/android/email/activity/ContactStatusLoader$Result;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    iget-object v2, p2, Lcom/android/email/activity/ContactStatusLoader$Result;->mPhoto:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->mVipViewOnClickListener:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->onClickBadge()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/android/email/activity/ContactStatusLoader$Result;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->onLoadFinished(Landroid/content/Loader;Lcom/android/email/activity/ContactStatusLoader$Result;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Lcom/android/email/activity/ContactStatusLoader$Result;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
