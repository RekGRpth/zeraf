.class public Lcom/mediatek/email/emailvip/activity/VipAddressTextView;
.super Lcom/android/ex/chips/MTKRecipientEditTextView;
.source "VipAddressTextView.java"


# instance fields
.field private mListFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->mListFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    return-void
.end method

.method private static getAddresses(Landroid/widget/TextView;)[Lcom/android/emailcommon/mail/Address;
    .locals 3
    .param p0    # Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;Z)[Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-super/range {p0 .. p5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    invoke-static {p0}, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->getAddresses(Landroid/widget/TextView;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->mListFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->mListFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    invoke-virtual {v1, v0}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->onAddVip([Lcom/android/emailcommon/mail/Address;)V

    const-string v1, ""

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setTargetFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V
    .locals 0
    .param p1    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->mListFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    return-void
.end method
