.class Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;
.super Lcom/mediatek/email/emailvip/activity/ListPhotoManager;
.source "ListPhotoManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;,
        Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;,
        Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;
    }
.end annotation


# static fields
.field private static final BITMAP_CACHE_SIZE:I = 0x1b0000

.field private static final COLUMNS:[Ljava/lang/String;

.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String;

.field private static final FADE_TRANSITION_DURATION:I = 0xc8

.field private static final HOLDER_CACHE_SIZE:I = 0x1e8480

.field private static final LARGE_RAM_THRESHOLD:I = 0x28000000

.field private static final LOADER_THREAD_NAME:Ljava/lang/String; = "ContactPhotoLoader"

.field private static final MESSAGE_PHOTOS_LOADED:I = 0x2

.field private static final MESSAGE_REQUEST_LOADING:I = 0x1


# instance fields
.field private final mBitmapCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Object;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmapHolderCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Object;",
            "Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mBitmapHolderCacheAllUnfresh:Z

.field private final mBitmapHolderCacheRedZoneBytes:I

.field private final mContext:Landroid/content/Context;

.field private final mFreshCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

.field private mLoadingRequested:Z

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private final mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/widget/ImageView;",
            "Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;",
            ">;"
        }
    .end annotation
.end field

.field private final mStaleCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "data15"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mStaleCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mFreshCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/mediatek/email/emailvip/utils/MemoryUtils;->getTotalMemorySize()J

    move-result-wide v3

    const-wide/32 v5, 0x28000000

    cmp-long v3, v3, v5

    if-ltz v3, :cond_0

    const/high16 v1, 0x3f800000

    :goto_0
    const/high16 v3, 0x49d80000

    mul-float/2addr v3, v1

    float-to-int v0, v3

    new-instance v3, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$1;

    invoke-direct {v3, p0, v0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$1;-><init>(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;I)V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    const v3, 0x49f42400

    mul-float/2addr v3, v1

    float-to-int v2, v3

    new-instance v3, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$2;

    invoke-direct {v3, p0, v2}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$2;-><init>(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;I)V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    int-to-double v3, v2

    const-wide/high16 v5, 0x3fe8000000000000L

    mul-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheRedZoneBytes:I

    const-string v3, "ListPhotoManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cache adj: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/high16 v1, 0x3f000000

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;)Landroid/util/LruCache;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;

    iget v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheRedZoneBytes:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;
    .param p1    # Ljava/util/Set;
    .param p2    # Ljava/util/Set;
    .param p3    # Ljava/util/Set;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->obtainPhotoIdsAndUrisToLoad(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;Ljava/lang/Object;[BZI)V
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;
    .param p1    # Ljava/lang/Object;
    .param p2    # [B
    .param p3    # Z
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BZI)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static btk(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit16 v1, p0, 0x3ff

    div-int/lit16 v1, v1, 0x400

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "K"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private cacheBitmap(Ljava/lang/Object;[BZI)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # [B
    .param p3    # Z
    .param p4    # I

    new-instance v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    if-nez p2, :cond_1

    const/4 v1, -0x1

    :goto_0
    invoke-direct {v0, p2, v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;-><init>([BI)V

    if-nez p3, :cond_0

    invoke-static {v0, p4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->inflateBitmap(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;I)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    return-void

    :cond_1
    invoke-static {p2}, Lcom/mediatek/email/emailvip/utils/BitmapUtil;->getSmallerExtentFromBytes([B)I

    move-result v1

    goto :goto_0
.end method

.method private dumpStats()V
    .locals 0

    return-void
.end method

.method private static inflateBitmap(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;I)V
    .locals 4
    .param p0    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;
    .param p1    # I

    iget v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->originalSmallerExtent:I

    invoke-static {v3, p1}, Lcom/mediatek/email/emailvip/utils/BitmapUtil;->findOptimalSampleSize(II)I

    move-result v2

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->decodedSampleSize:I

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    :cond_2
    :try_start_0
    invoke-static {v1, v2}, Lcom/mediatek/email/emailvip/utils/BitmapUtil;->decodeBitmapFromBytes([BI)Landroid/graphics/Bitmap;

    move-result-object v0

    iput v2, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->decodedSampleSize:I

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmap:Landroid/graphics/Bitmap;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private loadCachedPhoto(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;Z)Z
    .locals 10
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;
    .param p3    # Z

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {p2}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    if-nez v2, :cond_0

    move v6, v7

    :goto_0
    return v6

    :cond_0
    iget-object v6, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    if-nez v6, :cond_1

    iget-boolean v6, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    goto :goto_0

    :cond_1
    iget-object v6, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    if-nez v6, :cond_2

    move-object v0, v8

    :goto_1
    if-nez v0, :cond_3

    move v6, v7

    goto :goto_0

    :cond_2
    iget-object v6, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    invoke-virtual {v6}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    move-object v0, v6

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz p3, :cond_6

    if-eqz v4, :cond_6

    const/4 v6, 0x2

    new-array v3, v6, [Landroid/graphics/drawable/Drawable;

    instance-of v6, v4, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v6, :cond_5

    move-object v5, v4

    check-cast v5, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v7

    :goto_2
    const/4 v6, 0x1

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v9, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-direct {v7, v9, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v7, v3, v6

    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v6, 0xc8

    invoke-virtual {v1, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :goto_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v6

    iget-object v7, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v7}, Landroid/util/LruCache;->maxSize()I

    move-result v7

    div-int/lit8 v7, v7, 0x6

    if-ge v6, v7, :cond_4

    iget-object v6, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {p2}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iput-object v8, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmap:Landroid/graphics/Bitmap;

    iget-boolean v6, v2, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    goto :goto_0

    :cond_5
    aput-object v4, v3, v7

    goto :goto_2

    :cond_6
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3
.end method

.method private loadPhotoByIdOrUri(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->loadCachedPhoto(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->requestLoading()V

    goto :goto_0
.end method

.method private obtainPhotoIdsAndUrisToLoad(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    invoke-interface {p2}, Ljava/util/Set;->clear()V

    invoke-interface {p3}, Ljava/util/Set;->clear()V

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getRequestedExtent()I

    move-result v4

    invoke-static {v0, v4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->inflateBitmap(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;I)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v4, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    if-nez v4, :cond_0

    :cond_3
    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->isUriRequest()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->access$000(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_6

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    return-void
.end method

.method private processLoadedImages()V
    .locals 5

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v1, v4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->loadCachedPhoto(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->softenCache()V

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->requestLoading()V

    :cond_2
    return-void
.end method

.method private requestLoading()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoadingRequested:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoadingRequested:Z

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private static final safeDiv(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    div-int v0, p0, p1

    goto :goto_0
.end method

.method private softenCache()V
    .locals 3

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public cacheBitmap(Landroid/net/Uri;Landroid/graphics/Bitmap;[B)V
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # [B

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {p1, v2, v5}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->createFromUri(Landroid/net/Uri;IZ)Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    move-result-object v1

    new-instance v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    invoke-direct {v0, p3, v2}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;-><init>([BI)V

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, p2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->bitmapRef:Ljava/lang/ref/Reference;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v5, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    return-void
.end method

.method public ensureLoaderThread()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;-><init>(Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoadingRequested:Z

    iget-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->ensureLoaderThread()V

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;->requestLoading()V

    goto :goto_0

    :pswitch_1
    iget-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->processLoadedImages()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public loadPhoto(Landroid/widget/ImageView;Landroid/net/Uri;IZ)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # Z

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    invoke-static {p2, p3, p4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->createFromUri(Landroid/net/Uri;IZ)Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->loadPhotoByIdOrUri(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;)V

    goto :goto_0
.end method

.method public loadThumbnail(Landroid/widget/ImageView;JZ)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # J
    .param p4    # Z

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    invoke-static {p2, p3, p4}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;->createFromThumbnailId(JZ)Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->loadPhotoByIdOrUri(Landroid/widget/ImageView;Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$Request;)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x3c

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->clear()V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPaused:Z

    return-void
.end method

.method public preloadPhotosInBackground()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->ensureLoaderThread()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mLoaderThread:Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$LoaderThread;->requestPreloading()V

    return-void
.end method

.method public refreshCache()V
    .locals 3

    iget-boolean v2, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    if-eqz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    goto :goto_0
.end method

.method public removePhoto(Landroid/widget/ImageView;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public resume()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPaused:Z

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/ContactPhotoManagerImpl;->requestLoading()V

    :cond_0
    return-void
.end method
