.class public Lcom/mediatek/email/emailvip/activity/VipListAdapter;
.super Landroid/widget/CursorAdapter;
.source "VipListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;,
        Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;,
        Lcom/mediatek/email/emailvip/activity/VipListAdapter$VipFragmentLoader;
    }
.end annotation


# static fields
.field private static final COLUMN_PHOTO_ID:I = 0x0

.field private static final COLUMN_PRESENCE:I = 0x1

.field private static final NULL_PHOTO_ID:J = -0x1L

.field static final PROJECTION_PHOTO_ID_PRESENCE:[Ljava/lang/String;

.field public static final TAG:Ljava/lang/String; = "VipListAdapter"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mIsScrolling:Z

.field private mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_presence"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->PROJECTION_PHOTO_ID_PRESENCE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mIsScrolling:Z

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mActivity:Landroid/app/Activity;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mInflater:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;-><init>(Lcom/mediatek/email/emailvip/activity/VipListAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->startLoading()V

    return-void
.end method

.method static createMailboxesLoader(Landroid/content/Context;J)Landroid/content/Loader;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$VipFragmentLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$VipFragmentLoader;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v7, 0x0

    move-object v3, p1

    check-cast v3, Lcom/mediatek/email/emailvip/activity/VipListItem;

    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/activity/VipListItem;->resetViews()V

    iget-object v6, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v6}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setTargetActivity(Landroid/app/Activity;)V

    iget-object v6, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    invoke-virtual {v3, v6}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setVipFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    const/4 v6, 0x3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    iget-boolean v6, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mIsScrolling:Z

    if-nez v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v8, v0, v6}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->getAvatarInfo(Ljava/lang/String;Z)Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v6, v2, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mLookUpUri:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v2, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mLookUpUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setQuickContactLookupUri(Landroid/net/Uri;)V

    :cond_0
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setVipId(J)V

    invoke-virtual {v3, v5}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setVipName(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/mediatek/email/emailvip/activity/VipListItem;->setVipEmailAddress(Ljava/lang/String;)V

    const v6, 0x7f0f0093

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    iget-wide v6, v2, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mPhotoId:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    iget-wide v6, v2, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mPhotoId:J

    invoke-virtual {v3, v1, v6, v7}, Lcom/mediatek/email/emailvip/activity/VipListItem;->loadContactAvatar(Landroid/widget/ImageView;J)V

    :cond_1
    return-void

    :cond_2
    move v6, v7

    goto :goto_0
.end method

.method getPosition(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, -0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040033

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V
    .locals 0
    .param p1    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    return-void
.end method

.method public setScrollingState(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mIsScrolling:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mIsScrolling:Z

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    iput-boolean p1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mIsScrolling:Z

    return-void
.end method

.method public stopLoadingAvatars()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->stopLoading()V

    return-void
.end method

.method public updateAvatar(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->mAvatarLoader:Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;

    invoke-virtual {v0, p1}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->updateAvatar(Ljava/lang/String;)V

    return-void
.end method
