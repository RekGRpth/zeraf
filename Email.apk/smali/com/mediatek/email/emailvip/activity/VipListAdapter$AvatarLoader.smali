.class Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;
.super Ljava/lang/Object;
.source "VipListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/emailvip/activity/VipListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AvatarLoader"
.end annotation


# static fields
.field private static final AVATAR_FETCHING_LENGTH:I = 0xa

.field private static final UI_UPDATE_FREQUNCY:I = 0x3


# instance fields
.field mAvatarMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;",
            ">;"
        }
    .end annotation
.end field

.field mFetchingAvatars:[Ljava/lang/String;

.field mFetchingIndex:I

.field mLoaderContext:Landroid/content/Context;

.field mLoadingThread:Ljava/lang/Thread;

.field mStop:Z

.field final synthetic this$0:Lcom/mediatek/email/emailvip/activity/VipListAdapter;


# direct methods
.method constructor <init>(Lcom/mediatek/email/emailvip/activity/VipListAdapter;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->this$0:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingIndex:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z

    iput-object p2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoaderContext:Landroid/content/Context;

    return-void
.end method

.method private loadAvatarInfo(Ljava/lang/String;)V
    .locals 13
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v7, 0x0

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;

    move-object v7, v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_0

    const-wide/16 v11, -0x1

    const/4 v10, 0x0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoaderContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->PROJECTION_PHOTO_ID_PRESENCE:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_2

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoaderContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2}, Landroid/provider/ContactsContract$Data;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v10

    :cond_2
    new-instance v8, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->this$0:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-direct {v8, v1}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;-><init>(Lcom/mediatek/email/emailvip/activity/VipListAdapter;)V

    iput-wide v11, v8, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mPhotoId:J

    if-nez v10, :cond_3

    :goto_1
    iput-object v4, v8, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;->mLookUpUri:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :cond_3
    :try_start_4
    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v4

    goto :goto_1

    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private updateUi()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoaderContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader$1;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader$1;-><init>(Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public getAvatarInfo(Ljava/lang/String;Z)Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;

    move-object v1, v0

    monitor-exit v3

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingIndex:I

    aput-object p1, v2, v4

    iget v2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingIndex:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0xa

    iput v2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingIndex:I

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    monitor-exit v3

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2
.end method

.method public run()V
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z

    if-nez v4, :cond_3

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    monitor-enter v5

    const/4 v3, 0x0

    :goto_1
    const/16 v4, 0xa

    if-ge v3, v4, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    aget-object v0, v4, v3

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v6, v4, v3

    :cond_1
    if-nez v0, :cond_2

    iget-boolean v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_2

    if-nez v2, :cond_2

    :try_start_1
    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_2
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-boolean v4, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z

    if-eqz v4, :cond_5

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    const-string v4, "VipListAdapter"

    const-string v6, "AvatarLoader loading thread be interrupted"

    invoke-static {v4, v6}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    :cond_5
    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->loadAvatarInfo(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x3

    if-ne v2, v4, :cond_0

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->updateUi()V

    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    if-lez v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->updateUi()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startLoading()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoadingThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoadingThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoadingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public stopLoading()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mStop:Z

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mFetchingAvatars:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mLoadingThread:Ljava/lang/Thread;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public updateAvatar(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->mAvatarMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarLoader;->getAvatarInfo(Ljava/lang/String;Z)Lcom/mediatek/email/emailvip/activity/VipListAdapter$AvatarInfo;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
