.class public Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;
.super Lcom/android/email/activity/SafeFragment;
.source "OofGetWaitingFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;,
        Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;
    }
.end annotation


# static fields
.field public static final BUNDLE_KEY:Ljava/lang/String; = "OofGetWaitingFragment.accountId"

.field private static final DEFAULT_ID:J = -0x1L

.field private static final STATE_START:I = -0x1

.field public static final TAG:Ljava/lang/String; = "OofGetWaitingFragment"

.field private static mAccountId:J


# instance fields
.field private mAccount:Lcom/android/emailcommon/provider/Account;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field public mAttached:Z

.field private mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

.field private mContext:Landroid/content/Context;

.field private mNeedRecoverDialog:Z

.field mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

.field private mPaused:Z

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccountId:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/SafeFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;I)V
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->reportProgress(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)Lcom/android/emailcommon/provider/Account;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)V
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->onCheckingDialogCancel()V

    return-void
.end method

.method private finish()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "Kill current fragment if not already killed (finish)"

    invoke-static {v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getId()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(II)V

    :cond_1
    return-void
.end method

.method public static newInstance(JLandroid/app/Fragment;)Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;
    .locals 2
    .param p0    # J
    .param p2    # Landroid/app/Fragment;

    new-instance v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-direct {v0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    sput-wide p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccountId:J

    return-object v0
.end method

.method private onCheckingDialogCancel()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->cancelTaskInterrupt(Lcom/android/emailcommon/utility/EmailAsyncTask;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    const-string v0, "Email"

    const-string v1, "onCheckingDialogCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->finish()V

    return-void
.end method

.method private recoverAndDismissCheckingDialog()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "WaitingSaveOofDialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    :cond_0
    return-void
.end method

.method private reportProgress(I)V
    .locals 3
    .param p1    # I

    const v2, 0x7f080012

    iput p1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    iget-boolean v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAttached:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mPaused:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->recoverAndDismissCheckingDialog()V

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->startOofActivity()V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0, v0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->setupWaitingDialog(Landroid/app/FragmentManager;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/email/activity/UiUtilities;->isWifiOnly(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f080014

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->showAlertDialog(Landroid/app/FragmentManager;II)V

    goto :goto_0

    :cond_1
    const v1, 0x7f080013

    invoke-direct {p0, v0, v2, v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->showAlertDialog(Landroid/app/FragmentManager;II)V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f080058

    const v2, 0x7f080059

    invoke-direct {p0, v0, v1, v2}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->showAlertDialog(Landroid/app/FragmentManager;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setupWaitingDialog(Landroid/app/FragmentManager;)V
    .locals 3
    .param p1    # Landroid/app/FragmentManager;

    const-string v0, "WaitingSaveOofDialog"

    invoke-virtual {p1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;->newInstance(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mCheckingDialog:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$WaitingSaveOofDialog;

    const-string v2, "WaitingSaveOofDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_0
    return-void
.end method

.method private showAlertDialog(Landroid/app/FragmentManager;II)V
    .locals 3
    .param p1    # Landroid/app/FragmentManager;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->recoverAndDismissCheckingDialog()V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080073

    new-instance v2, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$1;

    invoke-direct {v2, p0, p1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$1;-><init>(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;Landroid/app/FragmentManager;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$2;-><init>(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;Landroid/app/FragmentManager;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private startOofActivity()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v1, v2, v3}, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->createIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAttached:Z

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;-><init>(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ### onActivityCreated  #### "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Fragment;->setRetainInstance(Z)V

    if-eqz p1, :cond_0

    const-string v0, "OofGetWaitingFragment.accountId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccountId:J

    :cond_0
    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mContext:Landroid/content/Context;

    sget-wide v1, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccountId:J

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->cancelTaskInterrupt(Lcom/android/emailcommon/utility/EmailAsyncTask;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mOofCheckTask:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAttached:Z

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mPaused:Z

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAlertDialog:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/email/activity/SafeFragment;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mPaused:Z

    iget v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mState:I

    invoke-direct {p0, v0}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->reportProgress(I)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "OofGetWaitingFragment.accountId"

    sget-wide v1, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAccountId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
