.class public final Lcom/android/calculator2/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calculator2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final advanced:I = 0x7f090028

.field public static final app_name:I = 0x7f090001

.field public static final basic:I = 0x7f090027

.field public static final clear:I = 0x7f09000e

.field public static final clearDesc:I = 0x7f090038

.field public static final clear_history:I = 0x7f090029

.field public static final cos:I = 0x7f090017

.field public static final cosDesc:I = 0x7f09003b

.field public static final cos_mathematical_value:I = 0x7f090018

.field public static final del:I = 0x7f09000d

.field public static final delDesc:I = 0x7f090037

.field public static final digit0:I = 0x7f090003

.field public static final digit1:I = 0x7f090004

.field public static final digit2:I = 0x7f090005

.field public static final digit3:I = 0x7f090006

.field public static final digit4:I = 0x7f090007

.field public static final digit5:I = 0x7f090008

.field public static final digit6:I = 0x7f090009

.field public static final digit7:I = 0x7f09000a

.field public static final digit8:I = 0x7f09000b

.field public static final digit9:I = 0x7f09000c

.field public static final div:I = 0x7f09000f

.field public static final divDesc:I = 0x7f090031

.field public static final dot:I = 0x7f090013

.field public static final dotDesc:I = 0x7f090035

.field public static final e:I = 0x7f09001c

.field public static final eDesc:I = 0x7f09003c

.field public static final e_mathematical_value:I = 0x7f09001d

.field public static final equal:I = 0x7f090014

.field public static final equalDesc:I = 0x7f09003e

.field public static final error:I = 0x7f090002

.field public static final factorial:I = 0x7f090026

.field public static final factorialDesc:I = 0x7f09002d

.field public static final leftParen:I = 0x7f090022

.field public static final leftParenDesc:I = 0x7f09002b

.field public static final lg:I = 0x7f090020

.field public static final lgDesc:I = 0x7f09002f

.field public static final lg_mathematical_value:I = 0x7f090021

.field public static final ln:I = 0x7f09001e

.field public static final lnDesc:I = 0x7f09002e

.field public static final ln_mathematical_value:I = 0x7f09001f

.field public static final minus:I = 0x7f090012

.field public static final minusDesc:I = 0x7f090034

.field public static final mtk_clear:I = 0x7f090000

.field public static final mul:I = 0x7f090010

.field public static final mulDesc:I = 0x7f090032

.field public static final pi:I = 0x7f09001b

.field public static final piDesc:I = 0x7f09003d

.field public static final plus:I = 0x7f090011

.field public static final plusDesc:I = 0x7f090033

.field public static final power:I = 0x7f090025

.field public static final powerDesc:I = 0x7f090036

.field public static final rightParen:I = 0x7f090023

.field public static final rightParenDesc:I = 0x7f09002c

.field public static final sin:I = 0x7f090015

.field public static final sinDesc:I = 0x7f09003a

.field public static final sin_mathematical_value:I = 0x7f090016

.field public static final sqrt:I = 0x7f090024

.field public static final sqrtDesc:I = 0x7f090030

.field public static final tan:I = 0x7f090019

.field public static final tanDesc:I = 0x7f090039

.field public static final tan_mathematical_value:I = 0x7f09001a

.field public static final text_copied_toast:I = 0x7f09002a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
