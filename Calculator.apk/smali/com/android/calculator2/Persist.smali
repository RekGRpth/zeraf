.class Lcom/android/calculator2/Persist;
.super Ljava/lang/Object;
.source "Persist.java"


# static fields
.field private static final FILE_NAME:Ljava/lang/String; = "calculator.data"

.field private static final LAST_VERSION:I = 0x2


# instance fields
.field history:Lcom/android/calculator2/History;

.field private mContext:Landroid/content/Context;

.field private mDeleteMode:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/calculator2/History;

    invoke-direct {v0}, Lcom/android/calculator2/History;-><init>()V

    iput-object v0, p0, Lcom/android/calculator2/Persist;->history:Lcom/android/calculator2/History;

    iput-object p1, p0, Lcom/android/calculator2/Persist;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getDeleteMode()I
    .locals 1

    iget v0, p0, Lcom/android/calculator2/Persist;->mDeleteMode:I

    return v0
.end method

.method public load()V
    .locals 7

    const/4 v6, 0x2

    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    iget-object v4, p0, Lcom/android/calculator2/Persist;->mContext:Landroid/content/Context;

    const-string v5, "calculator.data"

    invoke-virtual {v4, v5}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v4

    const/16 v5, 0x2000

    invoke-direct {v2, v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    iput v4, p0, Lcom/android/calculator2/Persist;->mDeleteMode:I

    :cond_0
    new-instance v4, Lcom/android/calculator2/History;

    invoke-direct {v4, v3, v1}, Lcom/android/calculator2/History;-><init>(ILjava/io/DataInput;)V

    iput-object v4, p0, Lcom/android/calculator2/Persist;->history:Lcom/android/calculator2/History;

    invoke-virtual {v1}, Ljava/io/FilterInputStream;->close()V

    :goto_0
    return-void

    :cond_1
    if-le v3, v6, :cond_0

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "data version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; expected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calculator2/Calculator;->log(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calculator2/Calculator;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public save()V
    .locals 6

    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcom/android/calculator2/Persist;->mContext:Landroid/content/Context;

    const-string v4, "calculator.data"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v1, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v3, p0, Lcom/android/calculator2/Persist;->mDeleteMode:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/calculator2/Persist;->history:Lcom/android/calculator2/History;

    invoke-virtual {v3, v2}, Lcom/android/calculator2/History;->write(Ljava/io/DataOutput;)V

    invoke-virtual {v2}, Ljava/io/FilterOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calculator2/Calculator;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDeleteMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calculator2/Persist;->mDeleteMode:I

    return-void
.end method
