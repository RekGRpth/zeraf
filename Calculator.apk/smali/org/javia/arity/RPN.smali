.class Lorg/javia/arity/RPN;
.super Lorg/javia/arity/TokenConsumer;
.source "RPN.java"


# instance fields
.field consumer:Lorg/javia/arity/TokenConsumer;

.field exception:Lorg/javia/arity/SyntaxException;

.field prevTokenId:I

.field stack:Ljava/util/Stack;


# direct methods
.method constructor <init>(Lorg/javia/arity/SyntaxException;)V
    .locals 1

    invoke-direct {p0}, Lorg/javia/arity/TokenConsumer;-><init>()V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    const/4 v0, 0x0

    iput v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    iput-object p1, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    return-void
.end method

.method static final isOperand(I)Z
    .locals 1

    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xe

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0x11

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private popHigher(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/javia/arity/SyntaxException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    iget v1, v0, Lorg/javia/arity/Token;->priority:I

    if-lt v1, p1, :cond_0

    iget-object v1, p0, Lorg/javia/arity/RPN;->consumer:Lorg/javia/arity/TokenConsumer;

    invoke-virtual {v1, v0}, Lorg/javia/arity/TokenConsumer;->push(Lorg/javia/arity/Token;)V

    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private top()Lorg/javia/arity/Token;
    .locals 1

    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/javia/arity/Token;

    goto :goto_0
.end method


# virtual methods
.method push(Lorg/javia/arity/Token;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/javia/arity/SyntaxException;
        }
    .end annotation

    const/16 v3, 0xb

    const/4 v0, 0x1

    iget v1, p1, Lorg/javia/arity/Token;->priority:I

    iget v2, p1, Lorg/javia/arity/Token;->id:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget v3, p1, Lorg/javia/arity/Token;->assoc:I

    if-ne v3, v0, :cond_c

    iget v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    invoke-static {v0}, Lorg/javia/arity/RPN;->isOperand(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/javia/arity/Lexer;->TOK_MUL:Lorg/javia/arity/Token;

    invoke-virtual {p0, v0}, Lorg/javia/arity/RPN;->push(Lorg/javia/arity/Token;)V

    :cond_0
    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    iget v0, p1, Lorg/javia/arity/Token;->id:I

    iput v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    :cond_2
    return-void

    :pswitch_1
    iget v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    invoke-static {v0}, Lorg/javia/arity/RPN;->isOperand(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lorg/javia/arity/Lexer;->TOK_MUL:Lorg/javia/arity/Token;

    invoke-virtual {p0, v0}, Lorg/javia/arity/RPN;->push(Lorg/javia/arity/Token;)V

    :cond_3
    iget-object v0, p0, Lorg/javia/arity/RPN;->consumer:Lorg/javia/arity/TokenConsumer;

    invoke-virtual {v0, p1}, Lorg/javia/arity/TokenConsumer;->push(Lorg/javia/arity/Token;)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    if-ne v0, v3, :cond_6

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v0

    iget v2, v0, Lorg/javia/arity/Token;->arity:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lorg/javia/arity/Token;->arity:I

    :cond_4
    invoke-direct {p0, v1}, Lorg/javia/arity/RPN;->popHigher(I)V

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lorg/javia/arity/Token;->id:I

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lorg/javia/arity/RPN;->consumer:Lorg/javia/arity/TokenConsumer;

    invoke-virtual {v1, v0}, Lorg/javia/arity/TokenConsumer;->push(Lorg/javia/arity/Token;)V

    :cond_5
    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_6
    iget v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    invoke-static {v0}, Lorg/javia/arity/RPN;->isOperand(I)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    const-string v1, "unexpected ) or END"

    iget v2, p1, Lorg/javia/arity/Token;->position:I

    invoke-virtual {v0, v1, v2}, Lorg/javia/arity/SyntaxException;->set(Ljava/lang/String;I)Lorg/javia/arity/SyntaxException;

    move-result-object v0

    throw v0

    :cond_7
    sget-object v1, Lorg/javia/arity/Lexer;->TOK_LPAREN:Lorg/javia/arity/Token;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    const-string v1, "expected LPAREN or CALL"

    iget v2, p1, Lorg/javia/arity/Token;->position:I

    invoke-virtual {v0, v1, v2}, Lorg/javia/arity/SyntaxException;->set(Ljava/lang/String;I)Lorg/javia/arity/SyntaxException;

    move-result-object v0

    throw v0

    :pswitch_3
    iget v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    invoke-static {v0}, Lorg/javia/arity/RPN;->isOperand(I)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    const-string v1, "misplaced COMMA"

    iget v2, p1, Lorg/javia/arity/Token;->position:I

    invoke-virtual {v0, v1, v2}, Lorg/javia/arity/SyntaxException;->set(Ljava/lang/String;I)Lorg/javia/arity/SyntaxException;

    move-result-object v0

    throw v0

    :cond_8
    invoke-direct {p0, v1}, Lorg/javia/arity/RPN;->popHigher(I)V

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v0

    if-eqz v0, :cond_9

    iget v1, v0, Lorg/javia/arity/Token;->id:I

    if-eq v1, v3, :cond_a

    :cond_9
    iget-object v0, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    const-string v1, "COMMA not inside CALL"

    iget v2, p1, Lorg/javia/arity/Token;->position:I

    invoke-virtual {v0, v1, v2}, Lorg/javia/arity/SyntaxException;->set(Ljava/lang/String;I)Lorg/javia/arity/SyntaxException;

    move-result-object v0

    throw v0

    :cond_a
    iget v1, v0, Lorg/javia/arity/Token;->arity:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lorg/javia/arity/Token;->arity:I

    goto/16 :goto_0

    :pswitch_4
    sget-object v0, Lorg/javia/arity/Lexer;->TOK_RPAREN:Lorg/javia/arity/Token;

    iget v1, p1, Lorg/javia/arity/Token;->position:I

    iput v1, v0, Lorg/javia/arity/Token;->position:I

    :cond_b
    invoke-virtual {p0, v0}, Lorg/javia/arity/RPN;->push(Lorg/javia/arity/Token;)V

    invoke-direct {p0}, Lorg/javia/arity/RPN;->top()Lorg/javia/arity/Token;

    move-result-object v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    :cond_c
    iget v3, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    invoke-static {v3}, Lorg/javia/arity/RPN;->isOperand(I)Z

    move-result v3

    if-nez v3, :cond_e

    const/4 v1, 0x2

    if-ne v2, v1, :cond_d

    sget-object p1, Lorg/javia/arity/Lexer;->TOK_UMIN:Lorg/javia/arity/Token;

    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_d
    if-eq v2, v0, :cond_2

    iget-object v0, p0, Lorg/javia/arity/RPN;->exception:Lorg/javia/arity/SyntaxException;

    const-string v1, "operator without operand"

    iget v2, p1, Lorg/javia/arity/Token;->position:I

    invoke-virtual {v0, v1, v2}, Lorg/javia/arity/SyntaxException;->set(Ljava/lang/String;I)Lorg/javia/arity/SyntaxException;

    move-result-object v0

    throw v0

    :cond_e
    iget v2, p1, Lorg/javia/arity/Token;->assoc:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_f

    :goto_1
    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lorg/javia/arity/RPN;->popHigher(I)V

    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method setConsumer(Lorg/javia/arity/TokenConsumer;)V
    .locals 0

    iput-object p1, p0, Lorg/javia/arity/RPN;->consumer:Lorg/javia/arity/TokenConsumer;

    return-void
.end method

.method start()V
    .locals 1

    iget-object v0, p0, Lorg/javia/arity/RPN;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/javia/arity/RPN;->prevTokenId:I

    iget-object v0, p0, Lorg/javia/arity/RPN;->consumer:Lorg/javia/arity/TokenConsumer;

    invoke-virtual {v0}, Lorg/javia/arity/TokenConsumer;->start()V

    return-void
.end method
