.class public Lcom/google/android/talk/util/Markup$Smileys;
.super Ljava/lang/Object;
.source "Markup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/util/Markup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Smileys"
.end annotation


# static fields
.field public static ANGEL:I

.field public static COOL:I

.field public static CRYING:I

.field public static EMBARRASSED:I

.field public static FOOT_IN_MOUTH:I

.field public static HAPPY:I

.field public static KISSING:I

.field public static LAUGHING:I

.field public static LIPS_ARE_SEALED:I

.field public static MONEY_MOUTH:I

.field public static SAD:I

.field public static SURPRISED:I

.field public static TONGUE_STICKING_OUT:I

.field public static UNDECIDED:I

.field public static WINKING:I

.field public static WTF:I

.field public static YELLING:I

.field private static final sIconIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/talk/util/Markup$Smileys;->sIconIds:[I

    const/4 v0, 0x0

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->HAPPY:I

    const/4 v0, 0x1

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->SAD:I

    const/4 v0, 0x2

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->WINKING:I

    const/4 v0, 0x3

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->TONGUE_STICKING_OUT:I

    const/4 v0, 0x4

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->SURPRISED:I

    const/4 v0, 0x5

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->KISSING:I

    const/4 v0, 0x6

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->YELLING:I

    const/4 v0, 0x7

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->COOL:I

    const/16 v0, 0x8

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->MONEY_MOUTH:I

    const/16 v0, 0x9

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->FOOT_IN_MOUTH:I

    const/16 v0, 0xa

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->EMBARRASSED:I

    const/16 v0, 0xb

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->ANGEL:I

    const/16 v0, 0xc

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->UNDECIDED:I

    const/16 v0, 0xd

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->CRYING:I

    const/16 v0, 0xe

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->LIPS_ARE_SEALED:I

    const/16 v0, 0xf

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->LAUGHING:I

    const/16 v0, 0x10

    sput v0, Lcom/google/android/talk/util/Markup$Smileys;->WTF:I

    return-void

    :array_0
    .array-data 4
        0x7f02002a
        0x7f020032
        0x7f020037
        0x7f020035
        0x7f020034
        0x7f02002c
        0x7f020039
        0x7f020026
        0x7f020030
        0x7f020029
        0x7f020028
        0x7f020025
        0x7f020036
        0x7f020027
        0x7f02002e
        0x7f02002d
        0x7f020038
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSmileyResource(I)I
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/google/android/talk/util/Markup$Smileys;->sIconIds:[I

    aget v0, v0, p0

    return v0
.end method
