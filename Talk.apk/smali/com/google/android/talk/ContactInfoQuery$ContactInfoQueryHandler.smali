.class final Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "ContactInfoQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/ContactInfoQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ContactInfoQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/talk/ContactInfoQuery;


# direct methods
.method public constructor <init>(Lcom/google/android/talk/ContactInfoQuery;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCallback:Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$000(Lcom/google/android/talk/ContactInfoQuery;)Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallbackErrorHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCallback:Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$000(Lcom/google/android/talk/ContactInfoQuery;)Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallbackErrorHandler;

    iget-object v1, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoSelectionArgs:[Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/talk/ContactInfoQuery;->access$100(Lcom/google/android/talk/ContactInfoQuery;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, v1, p3}, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallbackErrorHandler;->onContactInfoLoadFailed(Ljava/lang/String;Landroid/database/Cursor;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    invoke-virtual {v0}, Lcom/google/android/talk/ContactInfoQuery;->cleanupContactInfoCursor()V

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # setter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCursor:Landroid/database/Cursor;
    invoke-static {v0, p3}, Lcom/google/android/talk/ContactInfoQuery;->access$202(Lcom/google/android/talk/ContactInfoQuery;Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContentObserver:Landroid/database/ContentObserver;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$300(Lcom/google/android/talk/ContactInfoQuery;)Landroid/database/ContentObserver;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$200(Lcom/google/android/talk/ContactInfoQuery;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContentObserver:Landroid/database/ContentObserver;
    invoke-static {v1}, Lcom/google/android/talk/ContactInfoQuery;->access$300(Lcom/google/android/talk/ContactInfoQuery;)Landroid/database/ContentObserver;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/talk/ContactInfoQuery;->mHaveContactInfo:Z
    invoke-static {v0, v1}, Lcom/google/android/talk/ContactInfoQuery;->access$402(Lcom/google/android/talk/ContactInfoQuery;Z)Z

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mLogLevel:I
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$500(Lcom/google/android/talk/ContactInfoQuery;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryComplete: cursor.getCount(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/talk/ContactInfoQuery;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/talk/ContactInfoQuery;->access$600(Lcom/google/android/talk/ContactInfoQuery;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCallback:Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$000(Lcom/google/android/talk/ContactInfoQuery;)Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryHandler;->this$0:Lcom/google/android/talk/ContactInfoQuery;

    # getter for: Lcom/google/android/talk/ContactInfoQuery;->mContactInfoCallback:Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;
    invoke-static {v0}, Lcom/google/android/talk/ContactInfoQuery;->access$000(Lcom/google/android/talk/ContactInfoQuery;)Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;->onContactInfoLoaded()V

    goto :goto_0
.end method
