.class Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;
.super Landroid/view/ViewGroup;
.source "AnimatedAdapter2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/AnimatedAdapter2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatingItemView"
.end annotation


# instance fields
.field private mDeleted:Z

.field private mForcedHeight:I

.field private mLayoutCount:I

.field final synthetic this$0:Lcom/google/android/talk/AnimatedAdapter2;


# direct methods
.method public constructor <init>(Lcom/google/android/talk/AnimatedAdapter2;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->this$0:Lcom/google/android/talk/AnimatedAdapter2;

    invoke-direct {p0, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;)I
    .locals 1
    .param p0    # Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    iget v0, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;I)I
    .locals 0
    .param p0    # Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I

    return p1
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sub-int v1, p4, p2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    iget-boolean v1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mDeleted:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->scrollTo(II)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->getChildCount()I

    move-result v7

    if-nez v7, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    const/4 v4, 0x0

    if-eqz v5, :cond_0

    move v4, v6

    :cond_0
    iget-object v7, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->this$0:Lcom/google/android/talk/AnimatedAdapter2;

    # getter for: Lcom/google/android/talk/AnimatedAdapter2;->mAnimatedScale:F
    invoke-static {v7}, Lcom/google/android/talk/AnimatedAdapter2;->access$000(Lcom/google/android/talk/AnimatedAdapter2;)F

    move-result v7

    iget v8, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mForcedHeight:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {p0, v4, v7}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_1
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mForcedHeight:I

    int-to-float v7, v1

    iget-object v8, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->this$0:Lcom/google/android/talk/AnimatedAdapter2;

    # getter for: Lcom/google/android/talk/AnimatedAdapter2;->mAnimatedScale:F
    invoke-static {v8}, Lcom/google/android/talk/AnimatedAdapter2;->access$000(Lcom/google/android/talk/AnimatedAdapter2;)F

    move-result v8

    mul-float/2addr v7, v8

    float-to-int v2, v7

    iget-boolean v7, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mDeleted:Z

    if-eqz v7, :cond_2

    move v3, v2

    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0, v7, v3}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_2
    sub-int v3, v1, v2

    goto :goto_1
.end method

.method public setForcedHeight(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    iput p1, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mForcedHeight:I

    iput-boolean p2, p0, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mDeleted:Z

    return-void
.end method
