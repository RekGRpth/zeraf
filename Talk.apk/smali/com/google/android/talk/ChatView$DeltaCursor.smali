.class Lcom/google/android/talk/ChatView$DeltaCursor;
.super Ljava/lang/Object;
.source "ChatView.java"

# interfaces
.implements Landroid/database/Cursor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/ChatView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeltaCursor"
.end annotation


# instance fields
.field private mColumnNames:[Ljava/lang/String;

.field private mCursor:Landroid/database/Cursor;

.field private mDateColumn:I

.field private mDeltaColumn:I

.field final synthetic this$0:Lcom/google/android/talk/ChatView;


# direct methods
.method constructor <init>(Lcom/google/android/talk/ChatView;Landroid/database/Cursor;)V
    .locals 6
    .param p2    # Landroid/database/Cursor;

    const/4 v3, -0x1

    iput-object p1, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->this$0:Lcom/google/android/talk/ChatView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDateColumn:I

    iput v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    iput-object p2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    add-int/lit8 v3, v2, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mColumnNames:[Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mColumnNames:[Ljava/lang/String;

    aget-object v4, v0, v1

    aput-object v4, v3, v1

    iget-object v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mColumnNames:[Ljava/lang/String;

    aget-object v3, v3, v1

    const-string v4, "date"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput v1, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDateColumn:I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    iget-object v3, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mColumnNames:[Ljava/lang/String;

    iget v4, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    const-string v5, "delta"

    aput-object v5, v3, v4

    return-void
.end method

.method private checkPosition()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, -0x1

    if-eq v2, v1, :cond_0

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v2, Landroid/database/CursorIndexOutOfBoundsException;

    invoke-direct {v2, v1, v0}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(II)V

    throw v2

    :cond_1
    return-void
.end method

.method private getDeltaValue()J
    .locals 7

    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v0, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    iget v6, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDateColumn:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :goto_0
    sub-long v5, v3, v1

    return-wide v5

    :cond_0
    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    add-int/lit8 v6, v0, 0x1

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    iget v6, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDateColumn:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    iget v6, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDateColumn:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 7
    .param p1    # I
    .param p2    # Landroid/database/CharArrayBuffer;

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v0, p2, Landroid/database/CharArrayBuffer;->data:[C

    if-eqz v0, :cond_0

    array-length v5, v0

    if-ge v5, v1, :cond_1

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    iput-object v5, p2, Landroid/database/CharArrayBuffer;->data:[C

    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    iput v5, p2, Landroid/database/CharArrayBuffer;->sizeCopied:I

    :goto_1
    return-void

    :cond_1
    invoke-virtual {v2, v6, v1, v0, v6}, Ljava/lang/String;->getChars(II[CI)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    goto :goto_1
.end method

.method public deactivate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "delta"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const-string v0, "delta"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    const-string v0, "delta"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    long-to-double v0, v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    long-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public getShort(I)S
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->getDeltaValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    return v0
.end method

.method public isAfterLast()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    return v0
.end method

.method public isBeforeFirst()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isFirst()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isFirst()Z

    move-result v0

    return v0
.end method

.method public isLast()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/talk/ChatView$DeltaCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mDeltaColumn:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    goto :goto_0
.end method

.method public move(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->move(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPrevious()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1    # Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public requery()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1    # Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/talk/ChatView$DeltaCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method
