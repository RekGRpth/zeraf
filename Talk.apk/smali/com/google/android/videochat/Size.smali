.class public Lcom/google/android/videochat/Size;
.super Ljava/lang/Object;
.source "Size.java"


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/videochat/Size;->width:I

    iput p2, p0, Lcom/google/android/videochat/Size;->height:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videochat/Size;)V
    .locals 1
    .param p1    # Lcom/google/android/videochat/Size;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lcom/google/android/videochat/Size;->width:I

    iput v0, p0, Lcom/google/android/videochat/Size;->width:I

    iget v0, p1, Lcom/google/android/videochat/Size;->height:I

    iput v0, p0, Lcom/google/android/videochat/Size;->height:I

    return-void
.end method

.method public static newWithScaleDown(Lcom/google/android/videochat/Size;F)Lcom/google/android/videochat/Size;
    .locals 3
    .param p0    # Lcom/google/android/videochat/Size;
    .param p1    # F

    iget v2, p0, Lcom/google/android/videochat/Size;->width:I

    int-to-float v2, v2

    div-float/2addr v2, p1

    float-to-int v1, v2

    iget v2, p0, Lcom/google/android/videochat/Size;->height:I

    int-to-float v2, v2

    div-float/2addr v2, p1

    float-to-int v0, v2

    add-int/lit8 v2, v1, 0x2

    and-int/lit8 v1, v2, -0x4

    add-int/lit8 v2, v0, 0x2

    and-int/lit8 v0, v2, -0x4

    new-instance v2, Lcom/google/android/videochat/Size;

    invoke-direct {v2, v1, v0}, Lcom/google/android/videochat/Size;-><init>(II)V

    return-object v2
.end method

.method public static scaleToMax(Lcom/google/android/videochat/Size;Lcom/google/android/videochat/Size;)Lcom/google/android/videochat/Size;
    .locals 8
    .param p0    # Lcom/google/android/videochat/Size;
    .param p1    # Lcom/google/android/videochat/Size;

    const-wide/high16 v6, 0x3ff0000000000000L

    const/high16 v3, 0x3f800000

    iget v4, p0, Lcom/google/android/videochat/Size;->width:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/videochat/Size;->height:I

    int-to-float v5, v5

    div-float v0, v4, v5

    iget v4, p1, Lcom/google/android/videochat/Size;->width:I

    int-to-float v4, v4

    iget v5, p1, Lcom/google/android/videochat/Size;->height:I

    int-to-float v5, v5

    div-float v1, v4, v5

    float-to-double v4, v0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    float-to-double v4, v1

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_1

    :cond_0
    float-to-double v4, v0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_4

    float-to-double v4, v1

    cmpg-double v4, v4, v6

    if-gez v4, :cond_4

    :cond_1
    const/high16 v4, 0x3f800000

    div-float v1, v4, v1

    new-instance v2, Lcom/google/android/videochat/Size;

    iget v4, p1, Lcom/google/android/videochat/Size;->height:I

    iget v5, p1, Lcom/google/android/videochat/Size;->width:I

    invoke-direct {v2, v4, v5}, Lcom/google/android/videochat/Size;-><init>(II)V

    :goto_0
    cmpl-float v4, v0, v1

    if-lez v4, :cond_5

    iget v4, p0, Lcom/google/android/videochat/Size;->width:I

    iget v5, v2, Lcom/google/android/videochat/Size;->width:I

    if-le v4, v5, :cond_2

    iget v4, p0, Lcom/google/android/videochat/Size;->width:I

    int-to-float v4, v4

    iget v5, v2, Lcom/google/android/videochat/Size;->width:I

    int-to-float v5, v5

    div-float v3, v4, v5

    :cond_2
    :goto_1
    float-to-double v4, v3

    cmpl-double v4, v4, v6

    if-lez v4, :cond_3

    invoke-static {p0, v3}, Lcom/google/android/videochat/Size;->newWithScaleDown(Lcom/google/android/videochat/Size;F)Lcom/google/android/videochat/Size;

    move-result-object p0

    :cond_3
    return-object p0

    :cond_4
    new-instance v2, Lcom/google/android/videochat/Size;

    iget v4, p1, Lcom/google/android/videochat/Size;->width:I

    iget v5, p1, Lcom/google/android/videochat/Size;->height:I

    invoke-direct {v2, v4, v5}, Lcom/google/android/videochat/Size;-><init>(II)V

    goto :goto_0

    :cond_5
    iget v4, p0, Lcom/google/android/videochat/Size;->height:I

    iget v5, v2, Lcom/google/android/videochat/Size;->height:I

    if-le v4, v5, :cond_2

    iget v4, p0, Lcom/google/android/videochat/Size;->height:I

    int-to-float v4, v4

    iget v5, v2, Lcom/google/android/videochat/Size;->height:I

    int-to-float v5, v5

    div-float v3, v4, v5

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/videochat/Size;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/videochat/Size;

    iget v2, p0, Lcom/google/android/videochat/Size;->width:I

    iget v3, v0, Lcom/google/android/videochat/Size;->width:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/videochat/Size;->height:I

    iget v3, v0, Lcom/google/android/videochat/Size;->height:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getEncodedDimensions()I
    .locals 2

    iget v0, p0, Lcom/google/android/videochat/Size;->width:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/google/android/videochat/Size;->height:I

    or-int/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/videochat/Size;->width:I

    mul-int/lit16 v0, v0, 0x7fc9

    iget v1, p0, Lcom/google/android/videochat/Size;->height:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/videochat/Size;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videochat/Size;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
