.class public Lcom/google/android/videochat/VideoViewRequest;
.super Ljava/lang/Object;
.source "VideoViewRequest.java"


# instance fields
.field public final frameRate:I

.field public final height:I

.field public final renderer:Lcom/google/android/videochat/Renderer;

.field public final ssrc:I

.field public final width:I


# direct methods
.method public constructor <init>(ILcom/google/android/videochat/Renderer;III)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/videochat/Renderer;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/videochat/VideoViewRequest;->ssrc:I

    iput-object p2, p0, Lcom/google/android/videochat/VideoViewRequest;->renderer:Lcom/google/android/videochat/Renderer;

    iput p3, p0, Lcom/google/android/videochat/VideoViewRequest;->width:I

    iput p4, p0, Lcom/google/android/videochat/VideoViewRequest;->height:I

    iput p5, p0, Lcom/google/android/videochat/VideoViewRequest;->frameRate:I

    return-void
.end method
