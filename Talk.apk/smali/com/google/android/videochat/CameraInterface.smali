.class public Lcom/google/android/videochat/CameraInterface;
.super Ljava/lang/Object;
.source "CameraInterface.java"


# instance fields
.field private mCameraManager:Lcom/google/android/videochat/CameraManager;


# direct methods
.method constructor <init>(Lcom/google/android/videochat/CameraManager;)V
    .locals 0
    .param p1    # Lcom/google/android/videochat/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    return-void
.end method


# virtual methods
.method public arePreview3ALocksSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/videochat/CameraManager;->arePreview3ALocksSupported()Z

    move-result v0

    return v0
.end method

.method public getCurrentCameraId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/videochat/CameraManager;->getCurrentCameraId()I

    move-result v0

    return v0
.end method

.method public setPreview3ALocks(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videochat/CameraManager;->setPreview3ALocks(Z)V

    return-void
.end method

.method public suspendCamera()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/videochat/CameraManager;->suspendCamera()V

    return-void
.end method

.method public useCamera(Lcom/google/android/videochat/CameraSpecification;)V
    .locals 1
    .param p1    # Lcom/google/android/videochat/CameraSpecification;

    iget-object v0, p0, Lcom/google/android/videochat/CameraInterface;->mCameraManager:Lcom/google/android/videochat/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videochat/CameraManager;->useCamera(Lcom/google/android/videochat/CameraSpecification;)V

    return-void
.end method
