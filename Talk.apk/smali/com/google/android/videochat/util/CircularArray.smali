.class public Lcom/google/android/videochat/util/CircularArray;
.super Ljava/lang/Object;
.source "CircularArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mHasWrapped:Z

.field mList:[Ljava/lang/Object;

.field private mMaxCount:I

.field private mNextWriter:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    invoke-virtual {p0}, Lcom/google/android/videochat/util/CircularArray;->clear()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/videochat/util/CircularArray;->mList:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    aput-object p1, v0, v1

    iget v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    iget v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    iget v1, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videochat/util/CircularArray;->mHasWrapped:Z

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    iput-boolean v0, p0, Lcom/google/android/videochat/util/CircularArray;->mHasWrapped:Z

    iget v0, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/videochat/util/CircularArray;->mList:[Ljava/lang/Object;

    return-void
.end method

.method public count()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/videochat/util/CircularArray;->mHasWrapped:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/videochat/util/CircularArray;->mHasWrapped:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/videochat/util/CircularArray;->mNextWriter:I

    add-int v0, p1, v1

    iget v1, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/videochat/util/CircularArray;->mMaxCount:I

    sub-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/util/CircularArray;->mList:[Ljava/lang/Object;

    aget-object v1, v1, v0

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/videochat/util/CircularArray;->mList:[Ljava/lang/Object;

    aget-object v1, v1, p1

    goto :goto_0
.end method
