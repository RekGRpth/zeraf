.class public Landroid/support/place/connector/Endpoint;
.super Ljava/lang/Object;
.source "Endpoint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/place/connector/Endpoint$1;,
        Landroid/support/place/connector/Endpoint$PushEventErrorHandler;,
        Landroid/support/place/connector/Endpoint$Callback;
    }
.end annotation


# static fields
.field private static final sEmptyState:Landroid/support/place/rpc/RpcData;


# instance fields
.field private final mActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mBroker:Landroid/support/place/connector/Broker;

.field mCallback:Landroid/support/place/connector/Endpoint$Callback;

.field private mHandler:Landroid/os/Handler;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/place/rpc/EndpointInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMethods:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    sput-object v0, Landroid/support/place/connector/Endpoint;->sEmptyState:Landroid/support/place/rpc/RpcData;

    return-void
.end method

.method public constructor <init>(Landroid/support/place/connector/Broker;)V
    .locals 2
    .param p1    # Landroid/support/place/connector/Broker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/Endpoint;->mActions:Ljava/util/List;

    new-instance v0, Landroid/support/place/connector/Endpoint$Callback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/place/connector/Endpoint$Callback;-><init>(Landroid/support/place/connector/Endpoint;Landroid/support/place/connector/Endpoint$1;)V

    iput-object v0, p0, Landroid/support/place/connector/Endpoint;->mCallback:Landroid/support/place/connector/Endpoint$Callback;

    iput-object p1, p0, Landroid/support/place/connector/Endpoint;->mBroker:Landroid/support/place/connector/Broker;

    return-void
.end method

.method static synthetic access$100(Landroid/support/place/connector/Endpoint;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/support/place/connector/Endpoint;

    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/place/connector/Endpoint;Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;Z)[B
    .locals 1
    .param p0    # Landroid/support/place/connector/Endpoint;
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;
    .param p5    # Z

    invoke-direct/range {p0 .. p5}, Landroid/support/place/connector/Endpoint;->dispatchProcess(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;Z)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/place/connector/Endpoint;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/support/place/connector/Endpoint;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/support/place/connector/Endpoint;->unregisterListener(Ljava/lang/String;)V

    return-void
.end method

.method private dispatchProcess(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;Z)[B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;
    .param p5    # Z

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Endpoint;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Endpoint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invoke failed method=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-nez p5, :cond_0

    const/4 v1, 0x3

    iput v1, p4, Landroid/support/place/rpc/RpcError;->status:I

    const-string v1, ""

    iput-object v1, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/support/place/rpc/RpcError;->appendStackTrace(Ljava/lang/Throwable;)V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerListener(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 7
    .param p1    # Landroid/support/place/rpc/EndpointInfo;

    iget-object v1, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/support/place/connector/Endpoint;->getState()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->ser()[B

    move-result-object v3

    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mBroker:Landroid/support/place/connector/Broker;

    const-string v2, "onConnected"

    const/4 v4, 0x0

    new-instance v5, Landroid/support/place/connector/Endpoint$PushEventErrorHandler;

    const-string v1, "onConnected"

    invoke-direct {v5, p0, v1, p1}, Landroid/support/place/connector/Endpoint$PushEventErrorHandler;-><init>(Landroid/support/place/connector/Endpoint;Ljava/lang/String;Landroid/support/place/rpc/EndpointInfo;)V

    const/4 v6, 0x1

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private registerListener([B)[B
    .locals 3
    .param p1    # [B

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    new-instance v0, Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "listener"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/place/rpc/EndpointInfo;-><init>(Landroid/support/place/rpc/RpcData;)V

    invoke-direct {p0, v0}, Landroid/support/place/connector/Endpoint;->registerListener(Landroid/support/place/rpc/EndpointInfo;)V

    const/4 v2, 0x0

    return-object v2
.end method

.method private setupMethodCacheLocked()V
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/Endpoint;->mMethods:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    :goto_0
    array-length v5, v1

    move v4, v2

    move v3, v2

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v6, v1, v4

    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-class v0, Landroid/support/place/rpc/Rpc;

    invoke-virtual {v6, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/Rpc;

    if-eqz v0, :cond_0

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0}, Landroid/support/place/rpc/Rpc;->value()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v0, v2

    :goto_2
    iget-object v7, p0, Landroid/support/place/connector/Endpoint;->mMethods:Ljava/util/HashMap;

    invoke-virtual {v7, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v2}, Landroid/support/place/connector/Endpoint;->addAction(Ljava/lang/String;)V

    :cond_0
    move v0, v3

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "Endpoint"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/support/place/rpc/Rpc;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    if-nez v3, :cond_3

    const-string v0, "Endpoint"

    const-string v1, "No @Rpc methods found: check the proguard configuration"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private unregisterListener(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v2}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    monitor-exit v3

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private unregisterListener([B)[B
    .locals 3
    .param p1    # [B

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "listener"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/place/connector/Endpoint;->unregisterListener(Ljava/lang/String;)V

    const/4 v2, 0x0

    return-object v2
.end method


# virtual methods
.method public addAction(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/Endpoint;->mActions:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBroker()Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mBroker:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method public getIEndpoint()Landroid/support/place/rpc/IEndpointStub;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mCallback:Landroid/support/place/connector/Endpoint$Callback;

    return-object v0
.end method

.method public getInternalState([BLandroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;
    .locals 1
    .param p1    # [B
    .param p2    # Landroid/support/place/rpc/RpcContext;
    .annotation runtime Landroid/support/place/rpc/Rpc;
    .end annotation

    sget-object v0, Landroid/support/place/connector/Endpoint;->sEmptyState:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method public getState()Landroid/support/place/rpc/RpcData;
    .locals 1

    sget-object v0, Landroid/support/place/connector/Endpoint;->sEmptyState:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    return-void
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;

    const/4 v6, 0x0

    const-string v5, "registerListener"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, p2}, Landroid/support/place/connector/Endpoint;->registerListener([B)[B

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v5, "unregisterListener"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, p2}, Landroid/support/place/connector/Endpoint;->unregisterListener([B)[B

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v5, "getInternalState"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0, p2, p3}, Landroid/support/place/connector/Endpoint;->getInternalState([BLandroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v5, 0x3

    :try_start_0
    iput v5, p4, Landroid/support/place/rpc/RpcError;->status:I

    iget-object v7, p0, Landroid/support/place/connector/Endpoint;->mActions:Ljava/util/List;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v5, p0, Landroid/support/place/connector/Endpoint;->mMethods:Ljava/util/HashMap;

    if-nez v5, :cond_3

    invoke-direct {p0}, Landroid/support/place/connector/Endpoint;->setupMethodCacheLocked()V

    :cond_3
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v5, p0, Landroid/support/place/connector/Endpoint;->mMethods:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/reflect/Method;

    if-nez v4, :cond_4

    const-string v5, "Endpoint"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown action: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown action: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v1, v6

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_0
    move-exception v2

    const-string v5, "Endpoint"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "invoke failed method=\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown action (IllegalAccessException): "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;

    :goto_1
    move-object v1, v6

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x2

    :try_start_5
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v5, v7

    const/4 v7, 0x1

    aput-object p3, v5, v7

    invoke-virtual {v4, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    move-object v0, v5

    check-cast v0, [B

    move-object v1, v0

    const/4 v5, 0x0

    iput v5, p4, Landroid/support/place/rpc/RpcError;->status:I
    :try_end_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v5, v3, Ljava/lang/SecurityException;

    if-eqz v5, :cond_5

    const-string v5, "Endpoint"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Security exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x4

    iput v5, p4, Landroid/support/place/rpc/RpcError;->status:I

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string v5, "Endpoint"

    const-string v7, "invoke failed"

    invoke-static {v5, v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invocation failed: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;

    invoke-virtual {p4, v3}, Landroid/support/place/rpc/RpcError;->appendStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v2

    const-string v5, "Endpoint"

    const-string v7, "invoke failed "

    invoke-static {v5, v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, ""

    iput-object v5, p4, Landroid/support/place/rpc/RpcError;->logs:Ljava/lang/String;

    invoke-virtual {p4, v2}, Landroid/support/place/rpc/RpcError;->appendStackTrace(Ljava/lang/Throwable;)V

    move-object v1, v6

    goto/16 :goto_0
.end method

.method public pushEvent(Ljava/lang/String;[B)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    iget-object v2, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mListeners:Ljava/util/ArrayList;

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/support/place/rpc/EndpointInfo;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    array-length v7, v9

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_0

    aget-object v1, v9, v8

    iget-object v0, p0, Landroid/support/place/connector/Endpoint;->mBroker:Landroid/support/place/connector/Broker;

    const/4 v4, 0x0

    new-instance v5, Landroid/support/place/connector/Endpoint$PushEventErrorHandler;

    invoke-direct {v5, p0, p1, v1}, Landroid/support/place/connector/Endpoint$PushEventErrorHandler;-><init>(Landroid/support/place/connector/Endpoint;Ljava/lang/String;Landroid/support/place/rpc/EndpointInfo;)V

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void
.end method

.method public setCustomHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Landroid/support/place/connector/Endpoint;->mHandler:Landroid/os/Handler;

    return-void
.end method
