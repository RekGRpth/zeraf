.class Landroid/support/place/connector/EventListener$Registration$1;
.super Ljava/lang/Object;
.source "EventListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/EventListener$Registration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/support/place/connector/EventListener$Registration;


# direct methods
.method constructor <init>(Landroid/support/place/connector/EventListener$Registration;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    iget-object v6, v0, Landroid/support/place/connector/EventListener$Registration;->this$0:Landroid/support/place/connector/EventListener;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    # getter for: Landroid/support/place/connector/EventListener$Registration;->mRunning:Z
    invoke-static {v0}, Landroid/support/place/connector/EventListener$Registration;->access$200(Landroid/support/place/connector/EventListener$Registration;)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    iget-object v0, v0, Landroid/support/place/connector/EventListener$Registration;->this$0:Landroid/support/place/connector/EventListener;

    invoke-virtual {v0}, Landroid/support/place/connector/EventListener;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    iget-object v0, v0, Landroid/support/place/connector/EventListener$Registration;->this$0:Landroid/support/place/connector/EventListener;

    invoke-virtual {v0}, Landroid/support/place/connector/EventListener;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    # getter for: Landroid/support/place/connector/EventListener$Registration;->mEventSource:Landroid/support/place/rpc/EndpointInfo;
    invoke-static {v1}, Landroid/support/place/connector/EventListener$Registration;->access$300(Landroid/support/place/connector/EventListener$Registration;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    const-string v2, "registerListener"

    iget-object v3, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    iget-object v3, v3, Landroid/support/place/connector/EventListener$Registration;->this$0:Landroid/support/place/connector/EventListener;

    # getter for: Landroid/support/place/connector/EventListener;->mRegisterPayload:[B
    invoke-static {v3}, Landroid/support/place/connector/EventListener;->access$400(Landroid/support/place/connector/EventListener;)[B

    move-result-object v3

    iget-object v4, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    # getter for: Landroid/support/place/connector/EventListener$Registration;->mResultHandler:Landroid/support/place/rpc/RpcResultHandler;
    invoke-static {v4}, Landroid/support/place/connector/EventListener$Registration;->access$500(Landroid/support/place/connector/EventListener$Registration;)Landroid/support/place/rpc/RpcResultHandler;

    move-result-object v4

    iget-object v5, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    # getter for: Landroid/support/place/connector/EventListener$Registration;->mErrorHandler:Landroid/support/place/rpc/RpcErrorHandler;
    invoke-static {v5}, Landroid/support/place/connector/EventListener$Registration;->access$600(Landroid/support/place/connector/EventListener$Registration;)Landroid/support/place/rpc/RpcErrorHandler;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    :goto_1
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Landroid/support/place/connector/EventListener$Registration$1;->this$1:Landroid/support/place/connector/EventListener$Registration;

    # invokes: Landroid/support/place/connector/EventListener$Registration;->handleError()V
    invoke-static {v0}, Landroid/support/place/connector/EventListener$Registration;->access$700(Landroid/support/place/connector/EventListener$Registration;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
