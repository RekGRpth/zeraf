.class public Landroid/support/place/connector/coordinator/Coordinator;
.super Ljava/lang/Object;
.source "Coordinator.java"


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/rpc/EndpointInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/connector/coordinator/Coordinator;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/connector/coordinator/Coordinator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method
