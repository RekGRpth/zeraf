.class public Landroid/support/place/connector/ConnectorRegistryRpc$Listener;
.super Landroid/support/place/connector/EventListener$Listener;
.source "ConnectorRegistryRpc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/ConnectorRegistryRpc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Listener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/EventListener$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;Landroid/support/place/rpc/RpcContext;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/ConnectorInfo;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    return-void
.end method

.method public onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;Landroid/support/place/rpc/RpcContext;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/ConnectorInfo;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    return-void
.end method
