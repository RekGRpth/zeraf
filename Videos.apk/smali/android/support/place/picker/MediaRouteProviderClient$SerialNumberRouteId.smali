.class public Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;
.super Landroid/support/place/picker/MediaRouteProviderClient$RouteId;
.source "MediaRouteProviderClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/picker/MediaRouteProviderClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SerialNumberRouteId"
.end annotation


# instance fields
.field private mSerialNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/place/picker/MediaRouteProviderClient$RouteId;-><init>(I)V

    iput-object p1, p0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;->mSerialNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getRouteIdValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method
