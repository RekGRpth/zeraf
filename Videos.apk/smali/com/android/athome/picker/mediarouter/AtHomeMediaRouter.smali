.class public Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
.super Ljava/lang/Object;
.source "AtHomeMediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;,
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ProcessTgsStateRunnable;,
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AddConnectorWorkflow;,
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;,
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;,
        Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;
    }
.end annotation


# static fields
.field private static sAtHomeGroupCategory:Ljava/lang/Object;

.field private static sAtHomeReceiverCategory:Ljava/lang/Object;

.field static final sRouters:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mApplicationId:Ljava/lang/String;

.field private mAssigningRxToGroup:Z

.field private mAtHomeReceivers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private mAutoSelectRouteId:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

.field private mAvailableRxs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;",
            "Landroid/support/place/rpc/EndpointInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mBroker:Landroid/support/place/connector/Broker;

.field private mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

.field private mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

.field private mCanSendVolumeAdjustmentRpc:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mConnectorType:Ljava/lang/String;

.field private mCreatingGroup:Z

.field private final mGroupCreatingLock:Ljava/lang/Object;

.field private mGroupingListener:Landroid/support/place/music/TungstenGroupingService$Listener;

.field private mGroupingService:Landroid/support/place/music/TungstenGroupingService;

.field private mHandler:Landroid/os/Handler;

.field private mIgnoreTgsVolumeUpdates:Z

.field private mListener:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

.field private mMediaButtonReceiver:Landroid/content/ComponentName;

.field private mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;

.field private mMediaRouter:Ljava/lang/Object;

.field private mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

.field private mMediaRouterUpdateLock:Ljava/lang/Object;

.field private mMusicTxConnectors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mQueuedGroupRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mReceivers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteControlClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovingFromGroup:Z

.field private mResumeTgsVolumeUpdates:Ljava/lang/Runnable;

.field private mRetryCheckTgsVersion:Ljava/lang/Runnable;

.field private mRxStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/athome/picker/UserRouteState;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedOutput:Ljava/lang/Object;

.field private mSendVolumeAdjustmentRpc:Ljava/lang/Runnable;

.field private mStarted:Z

.field private mTgsBeingChecked:Landroid/support/place/connector/ConnectorInfo;

.field private mUpdatingMediaRouter:Z

.field private mVolumeCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

.field private mVolumeControl:Ljava/lang/Object;

.field private mVolumeHardkeyDelta:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sRouters:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;Landroid/support/place/api/broker/BrokerManager;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/support/place/api/broker/BrokerManager;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeHardkeyDelta:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCanSendVolumeAdjustmentRpc:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAvailableRxs:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    iput-boolean v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    iput-boolean v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z

    iput-boolean v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mQueuedGroupRequests:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterUpdateLock:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAutoSelectRouteId:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$1;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$1;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRetryCheckTgsVersion:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$2;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$2;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupCreatingLock:Ljava/lang/Object;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mConnectorType:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$4;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$4;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mListener:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$5;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$5;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mResumeTgsVolumeUpdates:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$11;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$11;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSendVolumeAdjustmentRpc:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$12;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$12;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingListener:Landroid/support/place/music/TungstenGroupingService$Listener;

    iput-object p2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    iput-object p3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAppContext:Landroid/content/Context;

    sget-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    const-string v1, "Nexus Q"

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    const-string v1, "Nexus Q"

    invoke-static {v0, v1, v3}, Lcom/android/athome/picker/media/MediaRouterCompat;->createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    :cond_1
    new-instance v0, Landroid/support/place/picker/MediaRouteProviderClient;

    invoke-direct {v0, p1}, Landroid/support/place/picker/MediaRouteProviderClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;

    invoke-static {p1}, Lcom/android/athome/picker/media/VolumeControlCompat;->newVolumeControl(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeControl:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/connector/ConnectorInfo;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mTgsBeingChecked:Landroid/support/place/connector/ConnectorInfo;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/connector/ConnectorInfo;)Landroid/support/place/connector/ConnectorInfo;
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mTgsBeingChecked:Landroid/support/place/connector/ConnectorInfo;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->checkTgsVersion(Landroid/support/place/connector/ConnectorInfo;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/util/List;Ljava/lang/Object;Z)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/util/List;
    .param p2    # Ljava/lang/Object;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->createNewTgsGroup(Ljava/util/List;Ljava/lang/Object;Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/picker/MediaRouteProviderClient;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildRemoteControlClient()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->updateGroupMemberShip(Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;I)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->stopTgsVolumeUpdates(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/music/TungstenGroupingService;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TungstenGroupingService;)Landroid/support/place/music/TungstenGroupingService;
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/music/TungstenGroupingService;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/connector/Broker;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/connector/Broker;)Landroid/support/place/connector/Broker;
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/connector/Broker;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->onTgsLost()V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mConnectorType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addConnector(Landroid/support/place/connector/ConnectorInfo;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mIgnoreTgsVolumeUpdates:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mIgnoreTgsVolumeUpdates:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/api/broker/BrokerManager;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/connector/ConnectorInfo;)Lcom/android/athome/picker/UserRouteState;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRxServiceState(Landroid/support/place/connector/ConnectorInfo;)Lcom/android/athome/picker/UserRouteState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # F
    .param p6    # Z

    invoke-direct/range {p0 .. p6}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildUserRoute(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAutoSelectRouteId:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;)Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAutoSelectRouteId:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TungstenGroupingService;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/music/TungstenGroupingService;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setGroupingService(Landroid/support/place/music/TungstenGroupingService;)V

    return-void
.end method

.method static synthetic access$3200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRetryCheckTgsVersion:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->processRxVolumes(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCanSendVolumeAdjustmentRpc:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeHardkeyDelta:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;I)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->adjustGroupVolume(I)V

    return-void
.end method

.method static synthetic access$3800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->useGroupingService()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TgsState;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/music/TgsState;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->processGroupAndReceiverList(Landroid/support/place/music/TgsState;)V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TgsState;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/music/TgsState;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addRxServiceConnector(Landroid/support/place/music/TgsState;)V

    return-void
.end method

.method static synthetic access$4100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Landroid/support/place/music/TgsGroup;
    .param p2    # Ljava/util/ArrayList;
    .param p3    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addRouteGroup(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$4302(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z

    return p1
.end method

.method static synthetic access$4402(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getLatestTgsState()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setReceiverVolume(Ljava/lang/Object;I)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    return v0
.end method

.method static synthetic access$900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    return-object v0
.end method

.method private addConnector(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 3
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AddConnectorWorkflow;

    invoke-direct {v1, p0, v0, p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AddConnectorWorkflow;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/String;Landroid/support/place/connector/ConnectorInfo;)V

    invoke-virtual {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AddConnectorWorkflow;->run()V

    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addRouteGroup(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/support/place/music/TgsGroup;
    .param p3    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/place/music/TgsGroup;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getMaxVolume(Ljava/lang/Object;)F

    move-result v1

    invoke-direct {p0, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->isGroupMuted(Ljava/lang/Object;)Z

    move-result v2

    invoke-direct {p0, p3, v0, v1, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setRouteState(Ljava/lang/Object;Ljava/lang/String;FZ)V

    const/4 v0, 0x1

    invoke-direct {p0, p3, v0, p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->logRouteGroup(Ljava/lang/Object;ZLjava/util/List;)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-direct {v2, p0, p3, p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/util/List;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getTx()Landroid/support/place/music/TgsTxService;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/place/music/TgsTxService;->getAppConnector()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private addRxService(Landroid/support/place/music/TgsService;)V
    .locals 5
    .param p1    # Landroid/support/place/music/TgsService;

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAvailableRxs:Ljava/util/HashMap;

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private addRxServiceConnector(Landroid/support/place/music/TgsState;)V
    .locals 7
    .param p1    # Landroid/support/place/music/TgsState;

    const-string v4, "AtHomeMediaRouter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getRxServiceConnector tgsState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAvailableRxs:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getGroups()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsGroup;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addRxService(Landroid/support/place/music/TgsService;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getAvailableRxs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addRxService(Landroid/support/place/music/TgsService;)V

    goto :goto_1
.end method

.method private declared-synchronized addTgsGroup(Landroid/support/place/music/TgsGroup;)V
    .locals 14

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_4

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v12}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v10, v9

    move-object v11, v1

    :goto_0
    if-ge v10, v12, :cond_3

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/support/place/music/TgsService;

    move-object v8, v0

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/UserRouteState;

    const/4 v3, 0x1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    if-nez v1, :cond_0

    const/4 v6, 0x0

    :goto_1
    if-nez v1, :cond_1

    move v7, v9

    :goto_2
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildUserRoute(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v1

    if-nez v10, :cond_2

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    :goto_3
    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move-object v11, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v6

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/android/athome/picker/UserRouteState;->getMute()Z

    move-result v7

    goto :goto_2

    :cond_2
    invoke-static {v11, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->addRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v11

    goto :goto_3

    :cond_3
    if-eqz v11, :cond_4

    invoke-direct {p0, p1, v13, v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addRouteGroup(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized addUserRoute(Landroid/support/place/music/TgsService;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    if-nez v0, :cond_0

    const/4 v5, 0x0

    :goto_0
    if-nez v0, :cond_1

    const/4 v6, 0x0

    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildUserRoute(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/android/athome/picker/media/MediaRouterCompat;->addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Added user route:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v5

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getMute()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized adjustGroupVolume(I)V
    .locals 4
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSendVolumeAdjustmentRpc:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS is not available yet. Not adjusting Rx volumes."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSendVolumeAdjustmentRpc:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCanSendVolumeAdjustmentRpc:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;

    invoke-static {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$8;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$8;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/place/music/TungstenGroupingService;->adjustGroupVolume(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private buildRemoteControlClient()Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->useGroupingService()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeControl:Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaButtonReceiver:Landroid/content/ComponentName;

    invoke-static {v2, v3}, Lcom/android/athome/picker/media/VolumeControlCompat;->newRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeControl:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaButtonReceiver:Landroid/content/ComponentName;

    invoke-static {v1, v2, v0}, Lcom/android/athome/picker/media/VolumeControlCompat;->registerRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private buildUserRoute(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # F
    .param p6    # Z

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    if-eqz p2, :cond_1

    sget-object v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p3}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-static {v1, p4}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    const/16 v2, 0x64

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolumeMax(Ljava/lang/Object;I)V

    const/high16 v2, 0x42c80000

    mul-float/2addr v2, p5

    float-to-int v2, v2

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolume(Ljava/lang/Object;I)V

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildRemoteControlClient()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    invoke-static {v1, v4}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolumeHandling(Ljava/lang/Object;I)V

    invoke-static {v1, v4}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setPlaybackType(Ljava/lang/Object;I)V

    invoke-direct {p0, v1, p1, p5, p6}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setRouteState(Ljava/lang/Object;Ljava/lang/String;FZ)V

    return-object v1

    :cond_1
    sget-object v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;

    goto :goto_0
.end method

.method private checkTgsVersion(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 4
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mTgsBeingChecked:Landroid/support/place/connector/ConnectorInfo;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    if-nez v1, :cond_0

    const-string v1, "AtHomeMediaRouter"

    const-string v2, "Broker not ready."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRetryCheckTgsVersion:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    new-instance v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$6;

    invoke-direct {v1, p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$6;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Landroid/support/place/music/TungstenGroupingService;)V

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$7;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$7;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->versionCheck(IILandroid/support/place/music/TungstenGroupingService$OnVersionCheck;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method

.method private clearAllRoutes()V
    .locals 8

    const-string v5, "AtHomeMediaRouter"

    const-string v6, "Clear all routes."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterUpdateLock:Ljava/lang/Object;

    monitor-enter v6

    const/4 v5, 0x1

    :try_start_0
    iput-boolean v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    sget-object v5, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-static {v4, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V

    invoke-static {v4, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRouteAt(Ljava/lang/Object;I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    invoke-static {v5, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private createNewTgsGroup(Ljava/util/List;Ljava/lang/Object;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS is not available. Not creating group."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupCreatingLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating group "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$13;

    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$13;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/Object;)V

    new-instance v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$14;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$14;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    invoke-virtual {v7, v2, p1, v0, v1}, Landroid/support/place/music/TungstenGroupingService;->createGroup(Ljava/lang/String;Ljava/util/List;Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_1
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static forApplication(Landroid/content/Context;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
    .locals 6
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v4, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sRouters:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Media Router must be initialized before using AtHomeMediaRouter."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->getInstance(Landroid/content/Context;)Landroid/support/place/api/broker/BrokerManager;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Broker Manager must be initialized before using AtHomeMediaRouter."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    new-instance v3, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {v3, v0, v2, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;-><init>(Landroid/content/Context;Ljava/lang/Object;Landroid/support/place/api/broker/BrokerManager;)V

    sget-object v4, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sRouters:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v3

    :cond_2
    sget-object v4, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sRouters:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    move-object v3, v4

    goto :goto_0
.end method

.method private getLatestTgsState()V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    new-instance v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$16;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$16;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$17;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$17;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->getGroupState(Landroid/support/place/music/TungstenGroupingService$OnGetGroupState;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method

.method private getMaxVolume(Ljava/lang/Object;)F
    .locals 4

    const/high16 v1, 0x7fc00000

    const/4 v0, 0x0

    :goto_0
    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-static {p1, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteMute(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteVolume(Ljava/lang/Object;)F

    move-result v1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteVolume(Ljava/lang/Object;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1

    :cond_2
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :cond_3
    return v1
.end method

.method public static getRouteId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/Object;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getTag(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/UserRouteState;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRouteMute(Ljava/lang/Object;)Z
    .locals 2
    .param p0    # Ljava/lang/Object;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getTag(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/UserRouteState;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getMute()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRouteVolume(Ljava/lang/Object;)F
    .locals 2
    .param p0    # Ljava/lang/Object;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getTag(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/UserRouteState;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRxServiceEndpoint(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;
    .locals 3
    .param p1    # Landroid/support/place/rpc/EndpointInfo;

    new-instance v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$DeviceId;-><init>(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAvailableRxs:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAvailableRxs:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/rpc/EndpointInfo;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRxServiceState(Landroid/support/place/connector/ConnectorInfo;)Lcom/android/athome/picker/UserRouteState;
    .locals 3
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRxServiceEndpoint(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/UserRouteState;

    goto :goto_0
.end method

.method private getUserRoute(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v3}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private isGroupMuted(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-static {p1, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteMute(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private logRouteGroup(Ljava/lang/Object;ZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Z",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "add route group to cache: Name= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connector size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update route group in cache: Name= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connector size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onTgsLost()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "OnTgsLost:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenGroupingService;->stopListening()V

    iput-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->clearAllRoutes()V

    iput-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mTgsBeingChecked:Landroid/support/place/connector/ConnectorInfo;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRetryCheckTgsVersion:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getConnectedPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    const-class v1, Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->getConnectorsWithType(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mListener:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v1, v0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;)V

    :cond_1
    return-void
.end method

.method private processGroupAndReceiverList(Landroid/support/place/music/TgsState;)V
    .locals 9

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processGroupAndReceiverList tgsState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCreatingGroup:Z

    if-eqz v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Creating group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z

    if-eqz v0, :cond_2

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Assigning rx to group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z

    if-eqz v0, :cond_3

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Removing rx from group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterUpdateLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Before updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getGroups()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsGroup;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->updateTgsGroup(Landroid/support/place/music/TgsGroup;)V

    :goto_2
    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    :try_start_1
    invoke-direct {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addTgsGroup(Landroid/support/place/music/TgsGroup;)V

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getRoute()Ljava/lang/Object;

    move-result-object v6

    const-string v1, "AtHomeMediaRouter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "routeGroup to be removed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_4
    if-ltz v1, :cond_8

    invoke-static {v6, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRouteAt(Ljava/lang/Object;I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    const-string v1, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remove stale group:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_9
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-static {v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_5
    if-ltz v0, :cond_a

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRouteAt(Ljava/lang/Object;I)V

    const-string v5, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Removed free pool rx: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    :cond_b
    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getAvailableRxs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->addUserRoute(Landroid/support/place/music/TgsService;)V

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "After updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method private processRxVolumes(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/music/TgsRxVolume;",
            ">;)V"
        }
    .end annotation

    const-string v5, "AtHomeMediaRouter"

    const-string v6, "processRxVolumes:"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/place/music/TgsRxVolume;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v4, Lcom/android/athome/picker/UserRouteState;

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v5}, Lcom/android/athome/picker/UserRouteState;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v6

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    :goto_1
    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->useGroupingService()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getUserRoute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v2, v4}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setVolumeState(Ljava/lang/Object;Lcom/android/athome/picker/UserRouteState;)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mConnectorType:Ljava/lang/String;

    if-nez v5, :cond_2

    new-instance v4, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v6

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/android/athome/picker/UserRouteState;

    const/4 v5, 0x0

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v6

    invoke-virtual {v3}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Lcom/android/athome/picker/UserRouteState;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getRoute()Ljava/lang/Object;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setVolumeState(Ljava/lang/Object;Lcom/android/athome/picker/UserRouteState;)V

    goto/16 :goto_0

    :cond_4
    return-void
.end method

.method private removeRemoteControlClient(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeControl:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaButtonReceiver:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/VolumeControlCompat;->unregisterRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private requestVolumes()V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    new-instance v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$9;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$9;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$10;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$10;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->getRxVolumes(Landroid/support/place/music/TungstenGroupingService$OnGetRxVolumes;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method private setGroupingService(Landroid/support/place/music/TungstenGroupingService;)V
    .locals 2
    .param p1    # Landroid/support/place/music/TungstenGroupingService;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenGroupingService;->stopListening()V

    :cond_0
    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingListener:Landroid/support/place/music/TungstenGroupingService$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/music/TungstenGroupingService;->startListening(Landroid/support/place/music/TungstenGroupingService$Listener;)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mVolumeHardkeyDelta:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->adjustGroupVolume(I)V

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->requestVolumes()V

    return-void
.end method

.method private setReceiverVolume(Ljava/lang/Object;I)V
    .locals 9

    const/high16 v8, 0x42c80000

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "AtHomeMediaRouter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RouteId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " GroupId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " volume: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getConnectors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    new-instance v6, Lcom/android/athome/picker/UserRouteState;

    int-to-float v0, p2

    div-float v7, v0, v8

    if-nez p2, :cond_3

    move v0, v1

    :goto_1
    invoke-direct {v6, v3, v7, v0}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    invoke-virtual {v4, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sendReceiverVolumeRpc(Landroid/support/place/rpc/EndpointInfo;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->useGroupingService()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getConnectors()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRxServiceEndpoint(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/android/athome/picker/UserRouteState;

    int-to-float v7, p2

    div-float/2addr v7, v8

    if-nez p2, :cond_5

    :goto_2
    invoke-direct {v6, v3, v7, v1}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sendReceiverVolumeRpc(Landroid/support/place/rpc/EndpointInfo;)V

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method private setRouteState(Ljava/lang/Object;Ljava/lang/String;FZ)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/String;
    .param p3    # F
    .param p4    # Z

    new-instance v0, Lcom/android/athome/picker/UserRouteState;

    invoke-direct {v0, p2, p3, p4}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    invoke-static {p1, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->setTag(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method private setVolumeState(Ljava/lang/Object;Lcom/android/athome/picker/UserRouteState;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/android/athome/picker/UserRouteState;

    if-eqz p1, :cond_0

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVolumeState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setTag(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v0

    const/high16 v1, 0x42c80000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {p1, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolume(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method private stopTgsVolumeUpdates(I)V
    .locals 4
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mIgnoreTgsVolumeUpdates:Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mResumeTgsVolumeUpdates:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mResumeTgsVolumeUpdates:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private updateGroupMemberShip(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 6
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Z

    invoke-static {p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "AtHomeMediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UpdateGroupMembership: routeId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " groupId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;

    invoke-direct {v2, p0, v1, v0, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private declared-synchronized updateTgsGroup(Landroid/support/place/music/TgsGroup;)V
    .locals 15

    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_8

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getRoute()Ljava/lang/Object;

    move-result-object v12

    invoke-static {v12}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v2

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-static {v12, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v13, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v10, :cond_5

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/support/place/music/TgsService;

    move-object v8, v0

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/UserRouteState;

    const/4 v3, 0x1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    if-nez v1, :cond_3

    const/4 v6, 0x0

    :goto_3
    if-nez v1, :cond_4

    const/4 v7, 0x0

    :goto_4
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildUserRoute(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->addRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Added route: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to existing group: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_3
    :try_start_1
    invoke-virtual {v1}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v6

    goto :goto_3

    :cond_4
    invoke-virtual {v1}, Lcom/android/athome/picker/UserRouteState;->getMute()Z

    move-result v7

    goto :goto_4

    :cond_5
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v14, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_7
    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-direct {v3, p0, v12, v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/util/List;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v12}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getMaxVolume(Ljava/lang/Object;)F

    move-result v2

    invoke-direct {p0, v12}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->isGroupMuted(Ljava/lang/Object;)Z

    move-result v3

    invoke-direct {p0, v12, v1, v2, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setRouteState(Ljava/lang/Object;Ljava/lang/String;FZ)V

    const/4 v1, 0x0

    invoke-direct {p0, v12, v1, v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->logRouteGroup(Ljava/lang/Object;ZLjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    monitor-exit p0

    return-void
.end method

.method private useGroupingService()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mConnectorType:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getReceiver(Ljava/lang/Object;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;
    .locals 3
    .param p1    # Ljava/lang/Object;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getTag(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/UserRouteState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected removeConnector(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 7
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "AtHomeMediaRouter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeConnector: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    invoke-virtual {v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getRoute()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    invoke-static {v3, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRxServiceEndpoint(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {v2}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method sendReceiverVolumeRpc(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 7

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRxStates:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/UserRouteState;

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volume: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mute: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getMute()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "volume"

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getVolume()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v2, "mute"

    invoke-virtual {v0}, Lcom/android/athome/picker/UserRouteState;->getMute()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Broker not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    const-string v2, "setMasterVolume"

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->ser()[B

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$15;

    invoke-direct {v5, p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$15;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    const/4 v6, 0x1

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    goto :goto_0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;

    return-void
.end method

.method public setConnectorType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mConnectorType:Ljava/lang/String;

    return-void
.end method

.method public start()V
    .locals 4

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mListener:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, v2}, Landroid/support/place/api/broker/BrokerManager;->startListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    const/high16 v2, 0x800000

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

    invoke-static {v1, v2, v3}, Lcom/android/athome/picker/media/MediaRouterCompat;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/place/picker/MediaRouteProviderClient;->getRouteIdForApplication(Ljava/lang/String;)Landroid/support/place/picker/MediaRouteProviderClient$RouteId;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAutoSelectRouteId:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->onTgsLost()V

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBrokerManager:Landroid/support/place/api/broker/BrokerManager;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mListener:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->stopListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mBroker:Landroid/support/place/connector/Broker;

    invoke-direct {p0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->clearAllRoutes()V

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouter:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method
