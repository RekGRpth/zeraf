.class Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;
.source "AtHomeMediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnRouteAdded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hashCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRouteGrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V
    .locals 9
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    const/4 v8, 0x0

    const-string v5, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OnRouteGrouped: routeInfo="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " routeGroup="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z
    invoke-static {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "AtHomeMediaRouter"

    const-string v6, "Updating media router. Ignore updates."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->updateGroupMemberShip(Ljava/lang/Object;Ljava/lang/Object;Z)V
    invoke-static {v5, p2, p3, v8}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0

    :cond_2
    invoke-static {p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v3

    const/4 v5, 0x1

    if-le v3, v5, :cond_0

    const-string v5, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RouteCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " creating new group."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-static {p3, v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v5}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->createNewTgsGroup(Ljava/util/List;Ljava/lang/Object;Z)V
    invoke-static {v5, v4, p3, v8}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/util/List;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OnRouteRemoved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hashCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;
    invoke-static {}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$400()Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_0

    invoke-static {p2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAtHomeReceivers:Ljava/util/Map;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMusicTxConnectors:Ljava/util/Map;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    const-string v11, "AtHomeMediaRouter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "OnRouteSelected: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z

    move-result v11

    if-eqz v11, :cond_1

    const-string v11, "AtHomeMediaRouter"

    const-string v12, "Updating media router. Ignore updates."

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p3 .. p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeGroupCategory:Ljava/lang/Object;
    invoke-static {}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$400()Ljava/lang/Object;

    move-result-object v12

    if-ne v11, v12, :cond_6

    invoke-static/range {p3 .. p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {v8}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    invoke-static {v8}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v7

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_2

    invoke-static {v8, v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v6

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mReceivers:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v11}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    const/4 v12, 0x1

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->createNewTgsGroup(Ljava/util/List;Ljava/lang/Object;Z)V
    invoke-static {v11, v10, v8, v12}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/util/List;Ljava/lang/Object;Z)V

    :cond_3
    :goto_2
    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    move-object/from16 v0, p3

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;
    invoke-static {v11, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1502(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    move-object/from16 v0, p3

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V
    invoke-static {v11, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)V

    :cond_4
    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->buildRemoteControlClient()Ljava/lang/Object;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemoteControlClients:Ljava/util/Map;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1600(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/util/Map;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v11, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v11, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setVolumeHandling(Ljava/lang/Object;I)V

    const/4 v11, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;->setPlaybackType(Ljava/lang/Object;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    move-result-object v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    move-result-object v11

    invoke-interface {v11, v8}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;->onGroupSelected(Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    invoke-static/range {p3 .. p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->sAtHomeReceiverCategory:Ljava/lang/Object;
    invoke-static {}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$300()Ljava/lang/Object;

    move-result-object v12

    if-ne v11, v12, :cond_3

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getReceiver(Ljava/lang/Object;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    move-result-object v5

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getConnectors()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getConnectors()Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v11}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getDeviceSerialNumber()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    new-instance v9, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    invoke-virtual {v5}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getDeviceSerialNumber()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;
    invoke-static {v11}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1400(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/picker/MediaRouteProviderClient;

    move-result-object v11

    iget-object v12, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;
    invoke-static {v12}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12, v9}, Landroid/support/place/picker/MediaRouteProviderClient;->setRouteIdForApplication(Ljava/lang/String;Landroid/support/place/picker/MediaRouteProviderClient$RouteId;)V

    goto/16 :goto_2
.end method

.method public onRouteUngrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;

    const-string v1, "AtHomeMediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnRouteUnGrouped: routeInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " routeGroup="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mUpdatingMediaRouter:Z
    invoke-static {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$800(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AtHomeMediaRouter"

    const-string v2, "Updating media router. Ignore updates."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    const/4 v2, 0x1

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->updateGroupMemberShip(Ljava/lang/Object;Ljava/lang/Object;Z)V
    invoke-static {v1, p2, p3, v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1900(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OnRouteUnSelected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p3, :cond_2

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    const/4 v3, 0x0

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mSelectedOutput:Ljava/lang/Object;
    invoke-static {v2, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1502(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mStarted:Z
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1200(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mMediaRouteProviderClient:Landroid/support/place/picker/MediaRouteProviderClient;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1400(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/picker/MediaRouteProviderClient;

    move-result-object v2

    iget-object v3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mApplicationId:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1300(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/place/picker/MediaRouteProviderClient;->deleteRouteIdForApplication(Ljava/lang/String;)V

    :cond_0
    invoke-static {p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getRouteId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mCallback:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;
    invoke-static {v2}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1100(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$Callback;->onGroupUnselected(Ljava/lang/Object;)V

    :cond_1
    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$3;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V
    invoke-static {v2, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$1700(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;)V

    :cond_2
    return-void
.end method
