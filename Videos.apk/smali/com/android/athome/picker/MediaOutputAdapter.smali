.class public Lcom/android/athome/picker/MediaOutputAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MediaOutputAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;,
        Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;,
        Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/athome/picker/MediaOutput;",
        ">;"
    }
.end annotation


# instance fields
.field private mFirstVisibleIndex:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMode:I

.field private mOutputType:I

.field private mSelectedGroup:Lcom/android/athome/picker/MediaOutputGroup;

.field private mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

.field private mView:Landroid/widget/AdapterView;

.field private mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput p3, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mOutputType:I

    return-void
.end method

.method static synthetic access$002(Lcom/android/athome/picker/MediaOutputAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mFirstVisibleIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/android/athome/picker/MediaOutputAdapter;)Landroid/widget/AdapterView;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mView:Landroid/widget/AdapterView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/athome/picker/MediaOutputAdapter;)Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    return-object v0
.end method

.method private createGroupingView(ILandroid/view/View;)Landroid/view/View;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/View;

    const/16 v12, 0x8

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v9, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v10, Lcom/android/athome/picker/R$layout;->list_item_output_grouping:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    sget v9, Lcom/android/athome/picker/R$id;->receiver_name:I

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v9, Lcom/android/athome/picker/R$id;->icon_now_playing_bar:I

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    sget v9, Lcom/android/athome/picker/R$id;->receiver_current_status:I

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    sget v9, Lcom/android/athome/picker/R$id;->list_item_checkbox:I

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    new-instance v9, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;

    invoke-direct {v9, v3, v4, v6, v5}, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;-><init>(Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    invoke-virtual {p2, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;

    move-object v8, p2

    move v7, p1

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getStatus()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->nowPlaying:Landroid/widget/ImageView;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->status:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->status:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getStatus()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v9, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedGroup:Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedGroup:Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v9, v2}, Lcom/android/athome/picker/MediaOutputGroup;->contains(Lcom/android/athome/picker/MediaOutput;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x1

    :cond_2
    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v9, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->checkBox:Landroid/widget/CheckBox;

    new-instance v10, Lcom/android/athome/picker/MediaOutputAdapter$3;

    invoke-direct {v10, p0, v8, v7}, Lcom/android/athome/picker/MediaOutputAdapter$3;-><init>(Lcom/android/athome/picker/MediaOutputAdapter;Landroid/view/View;I)V

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2

    :cond_3
    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->nowPlaying:Landroid/widget/ImageView;

    invoke-virtual {v9, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, v0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->status:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private createOutputSelectionView(ILandroid/view/View;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;

    if-nez p2, :cond_0

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lcom/android/athome/picker/R$layout;->list_item_output_receiver:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v3, p2

    move v2, p1

    new-instance v4, Lcom/android/athome/picker/MediaOutputAdapter$4;

    invoke-direct {v4, p0, v3, v2}, Lcom/android/athome/picker/MediaOutputAdapter$4;-><init>(Lcom/android/athome/picker/MediaOutputAdapter;Landroid/view/View;I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    sget v4, Lcom/android/athome/picker/R$id;->receiver_name:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v4, v1}, Lcom/android/athome/picker/MediaOutput;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget v4, Lcom/android/athome/picker/R$drawable;->list_selected_holo_dark:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    const-string v4, "MediaOutputAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Output id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2
.end method

.method private createVolumeControlView(ILandroid/view/View;)Landroid/view/View;
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/View;

    const/4 v4, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lcom/android/athome/picker/R$layout;->list_item_volume_details:I

    invoke-virtual {v0, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    sget v0, Lcom/android/athome/picker/R$id;->name:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v0, Lcom/android/athome/picker/R$id;->icon_volume:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    sget v0, Lcom/android/athome/picker/R$id;->volume_slider:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    new-instance v0, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;-><init>(Landroid/widget/ImageView;Landroid/widget/SeekBar;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;

    move-object v10, p2

    move v9, p1

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v4

    iget-object v5, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getMax()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LevelListDrawable;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x65

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    new-instance v4, Lcom/android/athome/picker/MediaOutputAdapter$1;

    invoke-direct {v4, p0, v7, v1}, Lcom/android/athome/picker/MediaOutputAdapter$1;-><init>(Lcom/android/athome/picker/MediaOutputAdapter;Lcom/android/athome/picker/MediaOutput;Landroid/graphics/drawable/LevelListDrawable;)V

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v2, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeIcon:Landroid/widget/ImageView;

    new-instance v4, Lcom/android/athome/picker/MediaOutputAdapter$2;

    invoke-direct {v4, p0, v1, v2, v7}, Lcom/android/athome/picker/MediaOutputAdapter$2;-><init>(Lcom/android/athome/picker/MediaOutputAdapter;Landroid/graphics/drawable/LevelListDrawable;Landroid/widget/SeekBar;Lcom/android/athome/picker/MediaOutput;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, v6, Lcom/android/athome/picker/MediaOutputAdapter$ViewHolder;->volumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public add(Lcom/android/athome/picker/MediaOutput;)V
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v0

    iget v1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mOutputType:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t add Output of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to adapter of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mOutputType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->add(Lcom/android/athome/picker/MediaOutput;)V

    return-void
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/athome/picker/MediaOutputAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "MediaOutputAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported view mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputAdapter;->createOutputSelectionView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputAdapter;->createGroupingView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputAdapter;->createVolumeControlView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public setMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputAdapter;->notifyDataSetInvalidated()V

    return-void
.end method

.method public setSelectedGroup(Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 2
    .param p1    # Lcom/android/athome/picker/MediaOutputGroup;

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedGroup:Lcom/android/athome/picker/MediaOutputGroup;

    iget v0, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputAdapter;->notifyDataSetInvalidated()V

    :cond_0
    return-void
.end method

.method public setSelectedOutput(Lcom/android/athome/picker/MediaOutput;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputAdapter;->notifyDataSetInvalidated()V

    return-void
.end method

.method public setView(Landroid/widget/AdapterView;)V
    .locals 0
    .param p1    # Landroid/widget/AdapterView;

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mView:Landroid/widget/AdapterView;

    return-void
.end method

.method public setVolumeSliderListener(Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    return-void
.end method
