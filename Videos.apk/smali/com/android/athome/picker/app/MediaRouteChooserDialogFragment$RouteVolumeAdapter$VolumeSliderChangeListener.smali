.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;
.super Ljava/lang/Object;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeSliderChangeListener"
.end annotation


# instance fields
.field private mIcon:Landroid/graphics/drawable/LevelListDrawable;

.field private mRoute:Ljava/lang/Object;

.field final synthetic this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;


# direct methods
.method private constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    .param p2    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mRoute:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mIcon:Landroid/graphics/drawable/LevelListDrawable;

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->changePerRouteVolume(Ljava/lang/Object;Landroid/graphics/drawable/LevelListDrawable;I)V
    invoke-static {v0, v1, v2, p2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$1600(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Ljava/lang/Object;Landroid/graphics/drawable/LevelListDrawable;I)V

    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iget-object v0, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mRoute:Ljava/lang/Object;

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1402(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iget-object v0, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1402(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->this$1:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mRoute:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mIcon:Landroid/graphics/drawable/LevelListDrawable;

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->updatePerRouteVolume(Ljava/lang/Object;Landroid/widget/SeekBar;Landroid/graphics/drawable/LevelListDrawable;)V
    invoke-static {v0, v1, p1, v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$1500(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Ljava/lang/Object;Landroid/widget/SeekBar;Landroid/graphics/drawable/LevelListDrawable;)V

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/LevelListDrawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/LevelListDrawable;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mIcon:Landroid/graphics/drawable/LevelListDrawable;

    return-void
.end method

.method public setRoute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->mRoute:Ljava/lang/Object;

    return-void
.end method
