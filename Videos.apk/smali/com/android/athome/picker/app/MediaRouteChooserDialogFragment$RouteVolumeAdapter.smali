.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
.super Landroid/widget/BaseAdapter;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RouteVolumeAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;
    }
.end annotation


# instance fields
.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Ljava/lang/Object;Landroid/widget/SeekBar;Landroid/graphics/drawable/LevelListDrawable;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/widget/SeekBar;
    .param p3    # Landroid/graphics/drawable/LevelListDrawable;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->updatePerRouteVolume(Ljava/lang/Object;Landroid/widget/SeekBar;Landroid/graphics/drawable/LevelListDrawable;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Ljava/lang/Object;Landroid/graphics/drawable/LevelListDrawable;I)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/graphics/drawable/LevelListDrawable;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->changePerRouteVolume(Ljava/lang/Object;Landroid/graphics/drawable/LevelListDrawable;I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V

    return-void
.end method

.method private bindItemToView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LevelListDrawable;

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolume(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->slider:Landroid/widget/SeekBar;

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolume(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->volumeSliderListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->volumeSliderListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    invoke-virtual {v2, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->setRoute(Ljava/lang/Object;)V

    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->volumeSliderListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    invoke-virtual {v2, v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;->setIcon(Landroid/graphics/drawable/LevelListDrawable;)V

    :cond_0
    iget-object v2, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->slider:Landroid/widget/SeekBar;

    iget-object v3, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->volumeSliderListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method private changePerRouteVolume(Ljava/lang/Object;Landroid/graphics/drawable/LevelListDrawable;I)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/graphics/drawable/LevelListDrawable;
    .param p3    # I

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1700(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeHandling(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result p3

    invoke-static {p1, p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->requestSetVolume(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private dispatchPerRouteVolumeControlDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1202(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private update()V
    .locals 5

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->enablePerRouteVolumeControl()Z
    invoke-static {v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1800(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->dispatchPerRouteVolumeControlDone()V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v3, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v4}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-static {v2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private updatePerRouteVolume(Ljava/lang/Object;Landroid/widget/SeekBar;Landroid/graphics/drawable/LevelListDrawable;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/widget/SeekBar;
    .param p3    # Landroid/graphics/drawable/LevelListDrawable;

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1702(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeHandling(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    invoke-virtual {p2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1702(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    invoke-virtual {p2, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/SeekBar;->setMax(I)V

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolume(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->getItemViewType(I)I

    move-result v1

    if-nez p2, :cond_1

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$600(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/LayoutInflater;

    move-result-object v2

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->VOLUME_ITEM_LAYOUTS:[I
    invoke-static {}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1000()[I

    move-result-object v3

    aget v3, v3, v1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    invoke-direct {v0, v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;)V

    iput p1, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    sget v2, Lcom/android/athome/picker/R$id;->icon:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    sget v2, Lcom/android/athome/picker/R$id;->slider:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->slider:Landroid/widget/SeekBar;

    new-instance v2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    invoke-direct {v2, p0, v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;)V

    iput-object v2, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->volumeSliderListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter$VolumeSliderChangeListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    if-nez v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->bindItemToView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    iput p1, v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p0, p3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->dispatchPerRouteVolumeControlDone()V

    :cond_0
    return-void
.end method
