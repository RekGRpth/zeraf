.class public Lcom/android/athome/picker/UserRouteState;
.super Ljava/lang/Object;
.source "UserRouteState.java"


# instance fields
.field private mId:Ljava/lang/String;

.field private mIsMuted:Z

.field private mSteps:I

.field private mVolume:F


# direct methods
.method public constructor <init>(Ljava/lang/String;FZ)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .param p3    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZI)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FZI)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .param p3    # Z
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/athome/picker/UserRouteState;->mId:Ljava/lang/String;

    iput p2, p0, Lcom/android/athome/picker/UserRouteState;->mVolume:F

    iput-boolean p3, p0, Lcom/android/athome/picker/UserRouteState;->mIsMuted:Z

    iput p4, p0, Lcom/android/athome/picker/UserRouteState;->mSteps:I

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/UserRouteState;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/UserRouteState;->mIsMuted:Z

    return v0
.end method

.method public getVolume()F
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/UserRouteState;->mVolume:F

    return v0
.end method
