.class public final Lcom/android/athome/picker/MediaOutputGroup;
.super Lcom/android/athome/picker/MediaOutput;
.source "MediaOutputGroup.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/MediaOutputGroup$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/athome/picker/MediaOutputGroup;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/MediaOutputGroup$1;

    invoke-direct {v0}, Lcom/android/athome/picker/MediaOutputGroup$1;-><init>()V

    sput-object v0, Lcom/android/athome/picker/MediaOutputGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    new-instance v2, Lcom/android/athome/picker/MediaOutputGroup$Builder;

    new-instance v3, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {v3, p1}, Lcom/android/athome/picker/MediaOutput;-><init>(Landroid/os/Parcel;)V

    invoke-direct {v2, v3}, Lcom/android/athome/picker/MediaOutputGroup$Builder;-><init>(Lcom/android/athome/picker/MediaOutput;)V

    invoke-direct {p0, v2}, Lcom/android/athome/picker/MediaOutputGroup;-><init>(Lcom/android/athome/picker/MediaOutputGroup$Builder;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    new-instance v3, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {v3, p1}, Lcom/android/athome/picker/MediaOutput;-><init>(Landroid/os/Parcel;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/athome/picker/MediaOutputGroup$Builder;)V
    .locals 8
    .param p1    # Lcom/android/athome/picker/MediaOutputGroup$Builder;

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mId:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$000(Lcom/android/athome/picker/MediaOutputGroup$Builder;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mType:I
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$100(Lcom/android/athome/picker/MediaOutputGroup$Builder;)I

    move-result v2

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mName:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$200(Lcom/android/athome/picker/MediaOutputGroup$Builder;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mStatus:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$300(Lcom/android/athome/picker/MediaOutputGroup$Builder;)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mVolume:F
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$400(Lcom/android/athome/picker/MediaOutputGroup$Builder;)F

    move-result v5

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mIsMuted:Z
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$500(Lcom/android/athome/picker/MediaOutputGroup$Builder;)Z

    move-result v6

    # getter for: Lcom/android/athome/picker/MediaOutputGroup$Builder;->mItems:Ljava/util/List;
    invoke-static {p1}, Lcom/android/athome/picker/MediaOutputGroup$Builder;->access$600(Lcom/android/athome/picker/MediaOutputGroup$Builder;)Ljava/util/List;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/athome/picker/MediaOutputGroup;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZLjava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZLjava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # F
    .param p6    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "FZ",
            "Ljava/util/List",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p6}, Lcom/android/athome/picker/MediaOutput;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZ)V

    if-nez p7, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    iput-object p7, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    goto :goto_0
.end method

.method private setGroupMuted(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, p1}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setGroupVolume(F)V
    .locals 3
    .param p1    # F

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, p1}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    instance-of v0, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t add a group to an existing group."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->isGroupable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t add "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "to a group of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public contains(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getGroupMaxVolume()F
    .locals 4

    const/high16 v2, 0x7fc00000

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_0

    :cond_2
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v2, 0x0

    :cond_3
    return v2
.end method

.method public getIsMuted()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->isGroupMuted()Z

    move-result v0

    return v0
.end method

.method public getMediaOutputs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getVolume()F
    .locals 1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getGroupMaxVolume()F

    move-result v0

    return v0
.end method

.method public isGroupMuted()Z
    .locals 4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :cond_1
    return v2
.end method

.method public remove(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setIsMuted(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->setGroupMuted(Z)V

    return-void
.end method

.method public setVolume(F)V
    .locals 0
    .param p1    # F

    invoke-super {p0, p1}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->setGroupVolume(F)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/android/athome/picker/MediaOutput;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "groupSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mItems:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/athome/picker/MediaOutput;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, p1, p2}, Lcom/android/athome/picker/MediaOutput;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
