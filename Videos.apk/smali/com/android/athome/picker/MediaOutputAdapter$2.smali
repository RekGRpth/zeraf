.class Lcom/android/athome/picker/MediaOutputAdapter$2;
.super Ljava/lang/Object;
.source "MediaOutputAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/athome/picker/MediaOutputAdapter;->createVolumeControlView(ILandroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/MediaOutputAdapter;

.field final synthetic val$item:Lcom/android/athome/picker/MediaOutput;

.field final synthetic val$volumeIcon:Landroid/graphics/drawable/LevelListDrawable;

.field final synthetic val$volumeSlider:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/MediaOutputAdapter;Landroid/graphics/drawable/LevelListDrawable;Landroid/widget/SeekBar;Lcom/android/athome/picker/MediaOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->this$0:Lcom/android/athome/picker/MediaOutputAdapter;

    iput-object p2, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeIcon:Landroid/graphics/drawable/LevelListDrawable;

    iput-object p3, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeSlider:Landroid/widget/SeekBar;

    iput-object p4, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$item:Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/16 v3, 0x65

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->this$0:Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->this$0:Lcom/android/athome/picker/MediaOutputAdapter;

    # getter for: Lcom/android/athome/picker/MediaOutputAdapter;->mView:Landroid/widget/AdapterView;
    invoke-static {v5}, Lcom/android/athome/picker/MediaOutputAdapter;->access$100(Lcom/android/athome/picker/MediaOutputAdapter;)Landroid/widget/AdapterView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v5

    # setter for: Lcom/android/athome/picker/MediaOutputAdapter;->mFirstVisibleIndex:I
    invoke-static {v4, v5}, Lcom/android/athome/picker/MediaOutputAdapter;->access$002(Lcom/android/athome/picker/MediaOutputAdapter;I)I

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeIcon:Landroid/graphics/drawable/LevelListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/LevelListDrawable;->getLevel()I

    move-result v4

    if-ne v4, v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeIcon:Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v4, v3}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeSlider:Landroid/widget/SeekBar;

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->this$0:Lcom/android/athome/picker/MediaOutputAdapter;

    # getter for: Lcom/android/athome/picker/MediaOutputAdapter;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;
    invoke-static {v1}, Lcom/android/athome/picker/MediaOutputAdapter;->access$200(Lcom/android/athome/picker/MediaOutputAdapter;)Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->this$0:Lcom/android/athome/picker/MediaOutputAdapter;

    # getter for: Lcom/android/athome/picker/MediaOutputAdapter;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;
    invoke-static {v1}, Lcom/android/athome/picker/MediaOutputAdapter;->access$200(Lcom/android/athome/picker/MediaOutputAdapter;)Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$item:Lcom/android/athome/picker/MediaOutput;

    invoke-interface {v1, v2, v0}, Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;->onMuteChange(Lcom/android/athome/picker/MediaOutput;Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputAdapter$2;->val$volumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method
