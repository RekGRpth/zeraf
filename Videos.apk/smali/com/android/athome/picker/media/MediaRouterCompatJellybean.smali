.class Lcom/android/athome/picker/media/MediaRouterCompatJellybean;
.super Ljava/lang/Object;
.source "MediaRouterCompatJellybean.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/MediaRouterCompatJellybean$VolumeCallbackShim;,
        Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static RouteCategory_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteCategory;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static RouteCategory_getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p0    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    check-cast p0, Landroid/media/MediaRouter$RouteCategory;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/media/MediaRouter$RouteCategory;->getRoutes(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p1
.end method

.method public static RouteCategory_isGroupable(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteCategory;->isGroupable()Z

    move-result v0

    return v0
.end method

.method public static RouteGroup_addRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteGroup;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteGroup;->addRoute(Landroid/media/MediaRouter$RouteInfo;)V

    return-void
.end method

.method public static RouteGroup_getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$RouteGroup;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteGroup;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static RouteGroup_getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteGroup;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    move-result v0

    return v0
.end method

.method public static RouteGroup_removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteGroup;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteGroup;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    return-void
.end method

.method public static RouteGroup_removeRouteAt(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$RouteGroup;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteGroup;->removeRoute(I)V

    return-void
.end method

.method public static RouteInfo_getCategory(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getGroup(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getIconDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getPlaybackType(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v0

    return v0
.end method

.method public static RouteInfo_getStatus(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getSupportedTypes(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    return v0
.end method

.method public static RouteInfo_getTag(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static RouteInfo_getVolume(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v0

    return v0
.end method

.method public static RouteInfo_getVolumeHandling(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v0

    return v0
.end method

.method public static RouteInfo_getVolumeMax(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v0

    return v0
.end method

.method public static RouteInfo_requestSetVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    return-void
.end method

.method public static RouteInfo_requestUpdateVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    return-void
.end method

.method public static RouteInfo_setTag(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public static UserRouteInfo_setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static UserRouteInfo_setPlaybackType(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackType(I)V

    return-void
.end method

.method public static UserRouteInfo_setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast p1, Landroid/media/RemoteControlClient;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setRemoteControlClient(Landroid/media/RemoteControlClient;)V

    return-void
.end method

.method public static UserRouteInfo_setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setStatus(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static UserRouteInfo_setVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolume(I)V

    return-void
.end method

.method public static UserRouteInfo_setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$VolumeCallbackShim;

    invoke-direct {v0, p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$VolumeCallbackShim;-><init>(Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V

    :cond_0
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V

    return-void
.end method

.method public static UserRouteInfo_setVolumeHandling(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeHandling(I)V

    return-void
.end method

.method public static UserRouteInfo_setVolumeMax(Ljava/lang/Object;I)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeMax(I)V

    return-void
.end method

.method public static addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    iget-object v1, p2, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->mCallbackShim:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v0, p2, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->mCallbackShim:Ljava/lang/Object;

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;

    :goto_0
    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0, p1, v0}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    return-void

    :cond_0
    new-instance v0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;

    invoke-direct {v0, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;-><init>(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    iput-object v0, p2, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->mCallbackShim:Ljava/lang/Object;

    goto :goto_0
.end method

.method public static addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    return-void
.end method

.method public static createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0, p1, p2}, Landroid/media/MediaRouter;->createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public static createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static forApplication(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p0    # Landroid/content/Context;

    const-string v0, "media_router"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->getCategoryAt(I)Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public static getCategoryCount(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0}, Landroid/media/MediaRouter;->getCategoryCount()I

    move-result v0

    return v0
.end method

.method public static getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v0

    return v0
.end method

.method public static getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    check-cast p0, Landroid/media/MediaRouter;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static isRouteCateory(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    instance-of v0, p0, Landroid/media/MediaRouter$RouteCategory;

    return v0
.end method

.method public static isRouteGroup(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    instance-of v0, p0, Landroid/media/MediaRouter$RouteGroup;

    return v0
.end method

.method public static isRouteInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    instance-of v0, p0, Landroid/media/MediaRouter$RouteInfo;

    return v0
.end method

.method public static removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    check-cast p0, Landroid/media/MediaRouter;

    iget-object v0, p1, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->mCallbackShim:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$Callback;

    invoke-virtual {p0, v0}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->mCallbackShim:Ljava/lang/Object;

    return-void
.end method

.method public static removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    return-void
.end method

.method public static selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    check-cast p0, Landroid/media/MediaRouter;

    check-cast p2, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1, p2}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    return-void
.end method
