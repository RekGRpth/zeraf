.class Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;
.source "MediaRouteButtonFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouteButtonFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaRouteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;


# direct methods
.method private constructor <init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;Lcom/android/athome/picker/media/MediaRouteButtonFallback$1;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouteButtonFallback;
    .param p2    # Lcom/android/athome/picker/media/MediaRouteButtonFallback$1;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;-><init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;)V

    return-void
.end method


# virtual methods
.method public onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteCount()V

    return-void
.end method

.method public onRouteGrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteCount()V

    return-void
.end method

.method public onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteCount()V

    return-void
.end method

.method public onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRemoteIndicator()V

    return-void
.end method

.method public onRouteUngrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteCount()V

    return-void
.end method

.method public onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;->this$0:Lcom/android/athome/picker/media/MediaRouteButtonFallback;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRemoteIndicator()V

    return-void
.end method
