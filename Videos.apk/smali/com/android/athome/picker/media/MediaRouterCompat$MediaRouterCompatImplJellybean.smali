.class Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplJellybean;
.super Ljava/lang/Object;
.source "MediaRouterCompat.java"

# interfaces
.implements Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaRouterCompatImplJellybean"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public RouteCategory_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteCategory_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteCategory_getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteCategory_getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public RouteCategory_isGroupable(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteCategory_isGroupable(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public RouteGroup_addRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteGroup_addRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public RouteGroup_getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteGroup_getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public RouteGroup_getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteGroup_getRouteCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteGroup_removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteGroup_removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public RouteGroup_removeRouteAt(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteGroup_removeRouteAt(Ljava/lang/Object;I)V

    return-void
.end method

.method public RouteInfo_getCategory(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getGroup(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getIconDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getIconDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getPlaybackType(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getPlaybackType(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteInfo_getStatus(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getStatus(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getSupportedTypes(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getSupportedTypes(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteInfo_getTag(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getTag(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getVolume(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getVolume(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteInfo_getVolumeHandling(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getVolumeHandling(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteInfo_getVolumeMax(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_getVolumeMax(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public RouteInfo_requestSetVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_requestSetVolume(Ljava/lang/Object;I)V

    return-void
.end method

.method public RouteInfo_requestUpdateVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_requestUpdateVolume(Ljava/lang/Object;I)V

    return-void
.end method

.method public RouteInfo_setTag(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->RouteInfo_setTag(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public UserRouteInfo_setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public UserRouteInfo_setPlaybackType(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setPlaybackType(Ljava/lang/Object;I)V

    return-void
.end method

.method public UserRouteInfo_setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public UserRouteInfo_setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public UserRouteInfo_setVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setVolume(Ljava/lang/Object;I)V

    return-void
.end method

.method public UserRouteInfo_setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V

    return-void
.end method

.method public UserRouteInfo_setVolumeHandling(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setVolumeHandling(Ljava/lang/Object;I)V

    return-void
.end method

.method public UserRouteInfo_setVolumeMax(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->UserRouteInfo_setVolumeMax(Ljava/lang/Object;I)V

    return-void
.end method

.method public addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-static {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Z

    invoke-static {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public forApplication(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getCategoryCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->getCategoryCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->getRouteCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isRouteCategory(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->isRouteCateory(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRouteGroup(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRouteInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->isRouteInfo(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    invoke-static {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterCompatJellybean;->selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method
