.class Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;
.super Landroid/media/MediaRouter$Callback;
.source "MediaRouterCompatJellybean.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterCompatJellybean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CallbackShim"
.end annotation


# instance fields
.field private mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-direct {p0}, Landroid/media/MediaRouter$Callback;-><init>()V

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    return-void
.end method


# virtual methods
.method public onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteGrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;
    .param p3    # Landroid/media/MediaRouter$RouteGroup;
    .param p4    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteGrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    return-void
.end method

.method public onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # I
    .param p3    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method public onRouteUngrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;
    .param p3    # Landroid/media/MediaRouter$RouteGroup;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteUngrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # I
    .param p3    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method public onRouteVolumeChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterCompatJellybean$CallbackShim;->mAppCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteVolumeChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
