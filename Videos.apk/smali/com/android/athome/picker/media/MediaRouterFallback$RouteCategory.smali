.class public Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;
.super Ljava/lang/Object;
.source "MediaRouterFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RouteCategory"
.end annotation


# instance fields
.field final mGroupable:Z

.field mName:Ljava/lang/CharSequence;

.field mTypes:I

.field final synthetic this$0:Lcom/android/athome/picker/media/MediaRouterFallback;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/MediaRouterFallback;Ljava/lang/CharSequence;IZ)V
    .locals 0
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # Z

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mName:Ljava/lang/CharSequence;

    iput p3, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mTypes:I

    iput-boolean p4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mGroupable:Z

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getRoutes(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v3}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v3, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v2

    iget-object v3, v2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    if-ne v3, p0, :cond_0

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    return-object p1
.end method

.method public getSupportedTypes()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mTypes:I

    return v0
.end method

.method public isGroupable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mGroupable:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RouteCategory{ name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mName:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mTypes:I

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->typesToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " groupable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->mGroupable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " routes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    # getter for: Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->access$200(Lcom/android/athome/picker/media/MediaRouterFallback;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
