.class public Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;
.super Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;
.source "MediaRouterCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserRouteInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;-><init>()V

    return-void
.end method

.method public static setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static setPlaybackType(Ljava/lang/Object;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setPlaybackType(Ljava/lang/Object;I)V

    return-void
.end method

.method public static setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static setVolume(Ljava/lang/Object;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setVolume(Ljava/lang/Object;I)V

    return-void
.end method

.method public static setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V

    return-void
.end method

.method public static setVolumeHandling(Ljava/lang/Object;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setVolumeHandling(Ljava/lang/Object;I)V

    return-void
.end method

.method public static setVolumeMax(Ljava/lang/Object;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->UserRouteInfo_setVolumeMax(Ljava/lang/Object;I)V

    return-void
.end method
