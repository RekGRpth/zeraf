.class public Lcom/android/athome/picker/media/MediaRouteButtonFallback;
.super Landroid/view/View;
.source "MediaRouteButtonFallback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;
    }
.end annotation


# static fields
.field private static final ACTIVATED_STATE_SET:[I


# instance fields
.field private mAttachedToWindow:Z

.field private mCheatSheetEnabled:Z

.field private mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

.field private mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

.field private mMinHeight:I

.field private mMinWidth:I

.field private mRemoteActive:Z

.field private mRemoteIndicator:Landroid/graphics/drawable/Drawable;

.field private mRouteTypes:I

.field private mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

.field private final mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

.field private mToggleMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->ACTIVATED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    sget v0, Lcom/android/athome/picker/R$attr;->mediaRouteButtonStyleFallback:I

    invoke-direct {p0, p1, p2, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;-><init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;Lcom/android/athome/picker/media/MediaRouteButtonFallback$1;)V

    iput-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback;

    iput-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    sget-object v2, Lcom/android/athome/picker/R$styleable;->MediaRouteButton:[I

    invoke-virtual {p1, p2, v2, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setRemoteIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mMinWidth:I

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mMinHeight:I

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v4}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setClickable(Z)V

    invoke-virtual {p0, v4}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setLongClickable(Z)V

    invoke-virtual {p0, v4}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setCheatSheetEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setRouteTypes(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/android/athome/picker/media/MediaRouteButtonFallback;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .locals 0
    .param p0    # Lcom/android/athome/picker/media/MediaRouteButtonFallback;
    .param p1    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    return-object p1
.end method

.method private getActivity()Landroid/app/Activity;
    .locals 3

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_0

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The MediaRouteButton\'s Context is not an Activity."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method private setRemoteIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->refreshDrawableState()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private updateRouteInfo()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRemoteIndicator()V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteCount()V

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->invalidate()V

    :cond_0
    return-void
.end method

.method public getRouteTypes()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mAttachedToWindow:Z

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    invoke-virtual {v0, v1, v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->addCallback(ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteInfo()V

    :cond_0
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1    # I

    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteActive:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->ACTIVATED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mAttachedToWindow:Z

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v9

    sub-int v6, v8, v9

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sub-int v8, v6, v5

    sub-int/2addr v8, v4

    div-int/lit8 v8, v8, 0x2

    add-int v2, v5, v8

    sub-int v8, v0, v7

    sub-int/2addr v8, v1

    div-int/lit8 v8, v8, 0x2

    add-int v3, v7, v8

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    add-int v9, v2, v4

    add-int v10, v3, v1

    invoke-virtual {v8, v2, v3, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    iget v10, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mMinWidth:I

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    :goto_0
    invoke-static {v10, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v8, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mMinHeight:I

    iget-object v10, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_0

    iget-object v9, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    :cond_0
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    sparse-switch v6, :sswitch_data_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v9

    add-int v5, v8, v9

    :goto_1
    sparse-switch v1, :sswitch_data_1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v8

    add-int/2addr v8, v3

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v9

    add-int v0, v8, v9

    :goto_2
    invoke-virtual {p0, v5, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setMeasuredDimension(II)V

    return-void

    :cond_1
    move v8, v9

    goto :goto_0

    :sswitch_0
    move v5, v7

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_1

    :sswitch_2
    move v0, v2

    goto :goto_2

    :sswitch_3
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v8

    add-int/2addr v8, v3

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public performClick()Z
    .locals 7

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->playSoundEffect(I)V

    :cond_0
    iget-boolean v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mToggleMode:Z

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteActive:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    iget-object v6, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v6}, Lcom/android/athome/picker/media/MediaRouterFallback;->getSystemAudioRoute()Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4, v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v4

    iget v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4}, Lcom/android/athome/picker/media/MediaRouterFallback;->getSystemAudioRoute()Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v4

    if-eq v3, v4, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    invoke-virtual {v4, v5, v3}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->showDialog()V

    goto :goto_0
.end method

.method public performLongClick()Z
    .locals 14

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    move-result v11

    if-eqz v11, :cond_0

    :goto_0
    return v9

    :cond_0
    iget-boolean v11, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mCheatSheetEnabled:Z

    if-nez v11, :cond_1

    move v9, v10

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v9, v10

    goto :goto_0

    :cond_2
    const/4 v11, 0x2

    new-array v6, v11, [I

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v6}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getLocationOnScreen([I)V

    invoke-virtual {p0, v3}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getHeight()I

    move-result v4

    aget v11, v6, v9

    div-int/lit8 v12, v4, 0x2

    add-int v5, v11, v12

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v11

    if-ge v5, v11, :cond_3

    const/16 v11, 0x30

    aget v12, v6, v10

    sub-int v12, v7, v12

    div-int/lit8 v13, v8, 0x2

    sub-int/2addr v12, v13

    invoke-virtual {v0, v11, v12, v4}, Landroid/widget/Toast;->setGravity(III)V

    :goto_1
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v10}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->performHapticFeedback(I)Z

    goto :goto_0

    :cond_3
    const/16 v11, 0x51

    invoke-virtual {v0, v11, v10, v4}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method

.method setCheatSheetEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mCheatSheetEnabled:Z

    return-void
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setRouteTypes(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mAttachedToWindow:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    :cond_2
    iput p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mAttachedToWindow:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->updateRouteInfo()V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouterCallback:Lcom/android/athome/picker/media/MediaRouteButtonFallback$MediaRouteCallback;

    invoke-virtual {v0, p1, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->addCallback(ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public showDialog()V
    .locals 4

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    instance-of v2, v0, Lvedroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_1

    check-cast v0, Lvedroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    if-nez v2, :cond_0

    const-string v2, "android:MediaRouteChooserDialogFragment"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iput-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    :cond_0
    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    if-eqz v2, :cond_2

    const-string v2, "MediaRouteButtonFallback"

    const-string v3, "showDialog(): Already showing!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v2, "MediaRouteButtonFallback"

    const-string v3, "fragments not supported by the activity."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;-><init>()V

    iput-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    new-instance v3, Lcom/android/athome/picker/media/MediaRouteButtonFallback$1;

    invoke-direct {v3, p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback$1;-><init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;)V

    invoke-virtual {v2, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->setLauncherListener(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;)V

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    invoke-virtual {v2, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->setRouteTypes(I)V

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mDialogFragment:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const-string v3, "android:MediaRouteChooserDialogFragment"

    invoke-virtual {v2, v1, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method updateRemoteIndicator()V
    .locals 3

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    iget v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getSelectedRoute(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getSystemAudioRoute()Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v2

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteActive:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteActive:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->refreshDrawableState()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateRouteCount()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouter:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4, v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v4

    iget v7, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    and-int/2addr v4, v7

    if-eqz v4, :cond_0

    instance-of v4, v3, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {v3}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteCount()I

    move-result v4

    add-int/2addr v1, v4

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    move v4, v5

    :goto_2
    invoke-virtual {p0, v4}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setEnabled(Z)V

    const/4 v4, 0x2

    if-ne v1, v4, :cond_4

    iget v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRouteTypes:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    :goto_3
    iput-boolean v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mToggleMode:Z

    return-void

    :cond_3
    move v4, v6

    goto :goto_2

    :cond_4
    move v5, v6

    goto :goto_3
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
