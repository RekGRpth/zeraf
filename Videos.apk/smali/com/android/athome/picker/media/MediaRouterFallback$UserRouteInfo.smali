.class public Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;
.super Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
.source "MediaRouterFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UserRouteInfo"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/media/MediaRouterFallback;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V
    .locals 1
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V

    const/high16 v0, 0x800000

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mSupportedTypes:I

    return-void
.end method


# virtual methods
.method public requestSetVolume(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeHandling:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    if-nez v0, :cond_1

    const-string v0, "MediaRouterFallback"

    const-string v1, "Cannot requestSetVolume on user route - no volume callback set"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    iget-object v0, v0, Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;->vcb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    invoke-virtual {v0, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;->onVolumeSetRequest(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public requestUpdateVolume(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeHandling:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    if-nez v0, :cond_1

    const-string v0, "MediaRouterFallback"

    const-string v1, "Cannot requestUpdateVolume on user route - no volume callback set"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    iget-object v0, v0, Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;->vcb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    invoke-virtual {v0, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;->onVolumeUpdateRequest(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mName:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->routeUpdated()V

    return-void
.end method

.method public setPlaybackType(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mPlaybackType:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mPlaybackType:I

    :cond_0
    return-void
.end method

.method public setStatus(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setStatusInt(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVolume(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->getVolumeMax()I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolume:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolume:I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->memberVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_0
    return-void
.end method

.method public setVolumeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V
    .locals 1
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    invoke-direct {v0, p1, p0}, Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;-><init>(Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

    return-void
.end method

.method public setVolumeHandling(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeHandling:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeHandling:I

    :cond_0
    return-void
.end method

.method public setVolumeMax(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeMax:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->mVolumeMax:I

    :cond_0
    return-void
.end method
