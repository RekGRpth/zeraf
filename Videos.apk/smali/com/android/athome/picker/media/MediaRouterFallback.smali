.class public Lcom/android/athome/picker/media/MediaRouterFallback;
.super Ljava/lang/Object;
.source "MediaRouterFallback.java"

# interfaces
.implements Lcom/android/athome/picker/MediaOutputSelector$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;,
        Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;,
        Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;,
        Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;,
        Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;,
        Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    }
.end annotation


# static fields
.field static final sRouters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/android/athome/picker/media/MediaRouterFallback;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

.field private final mOutputCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;"
        }
    .end annotation
.end field

.field private mRoutePicker:Lcom/android/athome/picker/MediaOutputSelector;

.field private final mRoutes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

.field private final mSystemCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/MediaRouterFallback;->sRouters:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mAppContext:Landroid/content/Context;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mAppContext:Landroid/content/Context;

    sget v2, Lcom/android/athome/picker/R$string;->speakers_title:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Ljava/lang/CharSequence;IZ)V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSystemCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->createDefaultRoutes()V

    new-instance v0, Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {v0}, Lcom/android/athome/picker/MediaOutputSelector;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutePicker:Lcom/android/athome/picker/MediaOutputSelector;

    return-void
.end method

.method static synthetic access$000(Lcom/android/athome/picker/media/MediaRouterFallback;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/android/athome/picker/media/MediaRouterFallback;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/athome/picker/media/MediaRouterFallback;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/athome/picker/media/MediaRouterFallback;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    return-object v0
.end method

.method private changeVolume(Lcom/android/athome/picker/MediaOutput;)V
    .locals 2
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->createState(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/UserRouteState;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private createDefaultRoutes()V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSystemCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-direct {v0, p0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mAppContext:Landroid/content/Context;

    sget v2, Lcom/android/athome/picker/R$string;->default_audio_route_name:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mName:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iput v3, v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iput v3, v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolumeHandling:I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method private createState(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/UserRouteState;
    .locals 4
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/athome/picker/UserRouteState;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v2

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/athome/picker/UserRouteState;-><init>(Ljava/lang/String;FZ)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static forApplication(Landroid/content/Context;)Lcom/android/athome/picker/media/MediaRouterFallback;
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/android/athome/picker/media/MediaRouterFallback;->sRouters:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-direct {v1, v0}, Lcom/android/athome/picker/media/MediaRouterFallback;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/android/athome/picker/media/MediaRouterFallback;->sRouters:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    sget-object v2, Lcom/android/athome/picker/media/MediaRouterFallback;->sRouters:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback;

    move-object v1, v2

    goto :goto_0
.end method

.method private getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static typesToString(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    const-string v1, "ROUTE_TYPE_LIVE_AUDIO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/high16 v1, 0x800000

    and-int/2addr v1, p0

    if-eqz v1, :cond_1

    const-string v1, "ROUTE_TYPE_USER "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public addCallback(ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget-object v3, v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    if-ne v3, p2, :cond_0

    iget v3, v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    and-int/2addr v3, p1

    iput v3, v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v4, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    invoke-direct {v4, p2, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;-><init>(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;I)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v0

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->isGroupable()Z

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    if-nez v3, :cond_2

    new-instance v1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteAdded(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    invoke-virtual {v1, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    move-object p1, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v3

    invoke-virtual {p0, v3, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteAdded(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    goto :goto_0
.end method

.method public addUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method public createRouteCategory(Ljava/lang/CharSequence;Z)Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    const/high16 v1, 0x800000

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Ljava/lang/CharSequence;IZ)V

    return-object v0
.end method

.method public createUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;
    .locals 1
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-direct {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V

    return-object v0
.end method

.method dispatchRouteAdded(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteGrouped(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;I)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;
    .param p3    # I

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1, p2, p3}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteGrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteRemoved(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteSelected(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    and-int/2addr v2, p1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteUngrouped(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteUngrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteUnselected(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    and-int/2addr v2, p1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1, p2}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->type:I

    iget v3, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    invoke-virtual {v2, p0, p1}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;->onRouteVolumeChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getCategoryAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    return-object v0
.end method

.method public getCategoryCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    return-object v0
.end method

.method public getRouteCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedRoute(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    return-object v0
.end method

.method public getSystemAudioRoute()Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    return-object v0
.end method

.method public onOutputAdded(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 6
    .param p1    # Lcom/android/athome/picker/MediaOutput;
    .param p2    # Lcom/android/athome/picker/MediaOutputGroup;

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getGroup()Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    const-string v3, "MediaRouterFallback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Add route: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to group: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2, v1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_1
    return-void
.end method

.method public onOutputRemoved(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 5
    .param p1    # Lcom/android/athome/picker/MediaOutput;
    .param p2    # Lcom/android/athome/picker/MediaOutputGroup;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    const-string v2, "MediaRouterFallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove route: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from group: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_0
    return-void
.end method

.method public onOutputSelected(Lcom/android/athome/picker/MediaOutput;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteInfo(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    const-string v1, "MediaRouterFallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected route: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_0
    return-void
.end method

.method public onVolumeChanged(Lcom/android/athome/picker/MediaOutput;)V
    .locals 6
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    const-string v3, "MediaRouterFallback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OnVolumeChanged: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    instance-of v3, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v3, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0, v2}, Lcom/android/athome/picker/media/MediaRouterFallback;->changeVolume(Lcom/android/athome/picker/MediaOutput;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->changeVolume(Lcom/android/athome/picker/MediaOutput;)V

    :cond_1
    return-void
.end method

.method public removeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 5
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;

    iget-object v2, v2, Lcom/android/athome/picker/media/MediaRouterFallback$CallbackInfo;->cb:Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "MediaRouterFallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeCallback("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): callback not registered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 7
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeRouteFromPicker(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v4

    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {v5}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v0

    if-ne v4, v0, :cond_4

    const/4 v2, 0x1

    :cond_0
    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    if-ne p1, v5, :cond_1

    const v5, 0x800001

    iget-object v6, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mDefaultAudio:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p0, v5, v6}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_1
    if-nez v2, :cond_2

    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteRemoved(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method removeRouteFromPicker(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 2
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutePicker:Lcom/android/athome/picker/MediaOutputSelector;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mRoutePicker:Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mOutputCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/MediaOutputSelector;->onOutputRemoved(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    return-void
.end method

.method public removeUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method public selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    if-ne v0, p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v0

    and-int/2addr v0, p1

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p0, v0, v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteUnselected(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_2
    iput-object p2, p0, Lcom/android/athome/picker/media/MediaRouterFallback;->mSelectedRoute:Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v0

    and-int/2addr v0, p1

    invoke-virtual {p0, v0, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteSelected(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    goto :goto_0
.end method

.method updateRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method
