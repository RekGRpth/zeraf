.class final Lcom/widevine/drm/internal/h;
.super Ljava/lang/Thread;


# instance fields
.field private synthetic a:Lcom/widevine/drm/internal/g;


# direct methods
.method synthetic constructor <init>(Lcom/widevine/drm/internal/g;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drm/internal/h;-><init>(Lcom/widevine/drm/internal/g;B)V

    return-void
.end method

.method private constructor <init>(Lcom/widevine/drm/internal/g;B)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v1}, Lcom/widevine/drm/internal/g;->a(Lcom/widevine/drm/internal/g;)Z

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v1, v1, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_0
    .catch Lcom/widevine/drm/internal/ad; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x5

    :try_start_1
    invoke-static {v1, v2}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/widevine/drm/internal/ad; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v1, v1, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    iget-object v2, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v2, v2, Lcom/widevine/drm/internal/g;->g:Ljava/lang/String;

    sget-object v3, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    invoke-virtual {v3}, Lcom/widevine/drm/internal/x;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v4}, Lcom/widevine/drm/internal/g;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2
    .catch Lcom/widevine/drm/internal/ad; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v1}, Lcom/widevine/drm/internal/g;->b(Lcom/widevine/drm/internal/g;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v3, v3, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    sget-object v4, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/e;

    invoke-virtual {v3, v4}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Lcom/widevine/drm/internal/e;)V

    :try_start_3
    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v3, v3, Lcom/widevine/drm/internal/g;->a:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v3, v3, Lcom/widevine/drm/internal/g;->a:Ljava/net/ServerSocket;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    const/high16 v3, 0x10000

    new-array v8, v3, [B

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v3}, Lcom/widevine/drm/internal/g;->c()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v5, 0x1

    :cond_2
    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v3}, Lcom/widevine/drm/internal/g;->c(Lcom/widevine/drm/internal/g;)Ljava/io/RandomAccessFile;

    move-result-object v3

    const-wide/16 v9, 0x0

    invoke-virtual {v3, v9, v10}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v3}, Lcom/widevine/drm/internal/g;->c(Lcom/widevine/drm/internal/g;)Ljava/io/RandomAccessFile;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v3

    :goto_2
    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v9, v9, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v9}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    move-result v9

    if-eqz v9, :cond_3

    const-wide/16 v9, 0x5

    :try_start_5
    invoke-static {v9, v10}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    goto :goto_2

    :catch_1
    move-exception v9

    goto :goto_2

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    const/4 v3, 0x1

    iget-object v4, v1, Lcom/widevine/drm/internal/ad;->a:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1}, Lcom/widevine/drm/internal/ad;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/widevine/drm/internal/g;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    :try_start_6
    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v9, v8, v3}, Lcom/widevine/drm/internal/g;->b([BI)I

    move v3, v4

    :goto_3
    iget-object v4, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v4}, Lcom/widevine/drm/internal/g;->b(Lcom/widevine/drm/internal/g;)Z

    move-result v4

    if-eqz v4, :cond_e

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v3}, Lcom/widevine/drm/internal/g;->c()Z

    move-result v3

    if-nez v3, :cond_12

    add-int/lit8 v4, v5, 0x1

    const/4 v9, 0x5

    if-le v5, v9, :cond_4

    iget-object v5, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    const/4 v9, 0x1

    sget-object v10, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v11, "serror (42)"

    invoke-virtual {v5, v9, v10, v11}, Lcom/widevine/drm/internal/g;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_4
    move v5, v4

    move v4, v3

    :goto_4
    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {v6, v8}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v9, -0x1

    if-eq v3, v9, :cond_e

    if-lez v3, :cond_5

    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v9, v8, v3}, Lcom/widevine/drm/internal/g;->a(Lcom/widevine/drm/internal/g;[BI)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const/4 v2, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v7, v9, v10, v11}, Ljava/io/OutputStream;->write([BII)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HTTP media player response:\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_5
    if-eqz v2, :cond_6

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-boolean v3, v3, Lcom/widevine/drm/internal/g;->d:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v3}, Lcom/widevine/drm/internal/g;->c(Lcom/widevine/drm/internal/g;)Ljava/io/RandomAccessFile;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v3

    if-gez v3, :cond_8

    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v9}, Lcom/widevine/drm/internal/g;->a()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v11, v9, v11

    if-lez v11, :cond_7

    iget-object v11, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v11}, Lcom/widevine/drm/internal/g;->c(Lcom/widevine/drm/internal/g;)Ljava/io/RandomAccessFile;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v11

    cmp-long v9, v11, v9

    if-gez v9, :cond_7

    new-instance v9, Lcom/widevine/drm/internal/b;

    sget-object v10, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    sget-object v11, Lcom/widevine/drmapi/android/WVStatus;->OutOfRange:Lcom/widevine/drmapi/android/WVStatus;

    invoke-direct {v9, v10, v11}, Lcom/widevine/drm/internal/b;-><init>(Lcom/widevine/drm/internal/x;Lcom/widevine/drmapi/android/WVStatus;)V

    iget-object v10, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v10, v10, Lcom/widevine/drm/internal/g;->g:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/widevine/drm/internal/b;->a(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/widevine/drm/internal/b;->a()V

    :goto_5
    if-lez v3, :cond_6

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    :cond_6
    :try_start_7
    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget v3, v3, Lcom/widevine/drm/internal/g;->h:I

    const/16 v9, 0xa

    if-lt v3, v9, :cond_d

    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->c()Lcom/widevine/drm/internal/e;

    move-result-object v3

    sget-object v9, Lcom/widevine/drm/internal/e;->c:Lcom/widevine/drm/internal/e;

    if-eq v3, v9, :cond_d

    add-int/lit8 v0, v0, 0x1

    const/16 v3, 0xa

    if-ge v0, v3, :cond_d

    const-wide/16 v9, 0x32

    invoke-static {v9, v10}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    move v3, v4

    goto/16 :goto_3

    :cond_7
    const/4 v3, 0x0

    :try_start_8
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    const/4 v2, 0x0

    move v13, v2

    move v2, v3

    move v3, v13

    goto :goto_5

    :cond_8
    :goto_6
    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v9, v9, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v9}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    move-result v9

    if-eqz v9, :cond_9

    const-wide/16 v9, 0x5

    :try_start_9
    invoke-static {v9, v10}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_6

    :catch_3
    move-exception v9

    goto :goto_6

    :cond_9
    :try_start_a
    iget-object v9, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-virtual {v9, v8, v3}, Lcom/widevine/drm/internal/g;->b([BI)I

    move-result v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decrypt1: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_5

    :catch_4
    move-exception v2

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    :goto_7
    iget-object v2, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    const/4 v3, 0x1

    sget-object v4, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v5, "Mediaplayer connection timeout"

    invoke-virtual {v2, v3, v4, v5}, Lcom/widevine/drm/internal/g;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    move-object v13, v0

    move v0, v1

    move-object v1, v13

    :goto_8
    if-eqz v1, :cond_1

    :try_start_b
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/widevine/drm/internal/p;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    :try_start_c
    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->c()Lcom/widevine/drm/internal/e;

    move-result-object v3

    sget-object v9, Lcom/widevine/drm/internal/e;->c:Lcom/widevine/drm/internal/e;

    if-ne v3, v9, :cond_c

    :goto_9
    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v3, v3, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v3}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_c
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    move-result v3

    if-eqz v3, :cond_b

    const-wide/16 v9, 0x5

    :try_start_d
    invoke-static {v9, v10}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    goto :goto_9

    :catch_6
    move-exception v3

    goto :goto_9

    :cond_b
    :try_start_e
    iget-object v3, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Lcom/widevine/drm/internal/g;->b([BI)I

    move-result v3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decrypt2: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_e
    .catch Ljava/net/SocketTimeoutException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_5

    :catch_7
    move-exception v2

    goto :goto_8

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_d
    const-wide/16 v9, 0xa

    :try_start_f
    invoke-static {v9, v10}, Lcom/widevine/drm/internal/h;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    move v3, v4

    goto/16 :goto_3

    :catch_8
    move-exception v3

    move v3, v4

    goto/16 :goto_3

    :cond_e
    :try_start_10
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_10
    .catch Ljava/net/SocketTimeoutException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_7

    goto :goto_8

    :cond_f
    iget-object v0, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-boolean v0, v0, Lcom/widevine/drm/internal/g;->f:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v0, v0, Lcom/widevine/drm/internal/g;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v1, v1, Lcom/widevine/drm/internal/g;->g:Ljava/lang/String;

    sget-object v2, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    invoke-virtual {v2}, Lcom/widevine/drm/internal/x;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Ljava/lang/String;I)I

    :cond_10
    iget-object v0, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-boolean v0, v0, Lcom/widevine/drm/internal/g;->e:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/widevine/drm/internal/g;->e:Z

    iget-object v0, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    iget-object v1, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v1}, Lcom/widevine/drm/internal/g;->d(Lcom/widevine/drm/internal/g;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v1

    iget-object v2, p0, Lcom/widevine/drm/internal/h;->a:Lcom/widevine/drm/internal/g;

    invoke-static {v2}, Lcom/widevine/drm/internal/g;->e(Lcom/widevine/drm/internal/g;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/widevine/drm/internal/g;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_11
    return-void

    :catch_9
    move-exception v2

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    goto/16 :goto_7

    :cond_12
    move v4, v3

    goto/16 :goto_4
.end method
