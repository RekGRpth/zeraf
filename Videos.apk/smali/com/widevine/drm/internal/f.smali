.class public final Lcom/widevine/drm/internal/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:J

.field private d:J

.field private e:J


# direct methods
.method constructor <init>(Ljava/lang/String;ZJJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/widevine/drm/internal/f;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/widevine/drm/internal/f;->b:Z

    iput-wide p3, p0, Lcom/widevine/drm/internal/f;->c:J

    iput-wide p5, p0, Lcom/widevine/drm/internal/f;->d:J

    iput-wide p7, p0, Lcom/widevine/drm/internal/f;->e:J

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/widevine/drm/internal/f;->b:Z

    return v0
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcom/widevine/drm/internal/f;->c:J

    return-wide v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/widevine/drm/internal/f;->d:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcom/widevine/drm/internal/f;->e:J

    return-wide v0
.end method
