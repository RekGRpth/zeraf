.class public Lcom/widevine/drm/internal/HTTPDecrypter;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/widevine/drm/internal/HTTPDecrypter;

.field private static b:Lcom/widevine/drm/internal/e;

.field private static c:Ljava/lang/String;

.field private static d:Z

.field private static e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/widevine/drm/internal/f;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "WVphoneAPI"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sput-object v1, Lcom/widevine/drm/internal/HTTPDecrypter;->a:Lcom/widevine/drm/internal/HTTPDecrypter;

    sget-object v0, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/e;

    sput-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->b:Lcom/widevine/drm/internal/e;

    const-string v0, ""

    sput-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->c:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z

    sput-object v1, Lcom/widevine/drm/internal/HTTPDecrypter;->f:Landroid/net/ConnectivityManager;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/widevine/drm/internal/p;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->e:Ljava/util/Queue;

    return-void
.end method

.method public static a(Lcom/widevine/drmapi/android/WVStatus;)I
    .locals 3

    const/16 v0, 0xb

    sget-object v1, Lcom/widevine/drm/internal/d;->a:[I

    invoke-virtual {p0}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_8
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_a
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_b
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_c
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_d
    const/16 v0, 0xd

    goto :goto_0

    :pswitch_e
    const/16 v0, 0xe

    goto :goto_0

    :pswitch_f
    const/16 v0, 0xf

    goto :goto_0

    :pswitch_10
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_11
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_12
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_13
    const/16 v0, 0x13

    goto :goto_0

    :pswitch_14
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_15
    const/16 v0, 0x15

    goto :goto_0

    :pswitch_16
    const/16 v0, 0x16

    goto :goto_0

    :pswitch_17
    const/16 v0, 0x17

    goto :goto_0

    :pswitch_18
    const/16 v0, 0x18

    goto :goto_0

    :pswitch_19
    const/16 v0, 0x19

    goto :goto_0

    :pswitch_1a
    const/16 v0, 0x1a

    goto :goto_0

    :pswitch_1b
    const/16 v0, 0x1b

    goto :goto_0

    :pswitch_1c
    const/16 v0, 0x1c

    goto :goto_0

    :pswitch_1d
    const/16 v0, 0x1d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public static a()Lcom/widevine/drm/internal/HTTPDecrypter;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->a:Lcom/widevine/drm/internal/HTTPDecrypter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-direct {v0}, Lcom/widevine/drm/internal/HTTPDecrypter;-><init>()V

    sput-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->a:Lcom/widevine/drm/internal/HTTPDecrypter;

    :cond_0
    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->a:Lcom/widevine/drm/internal/HTTPDecrypter;

    return-object v0
.end method

.method public static a(I)Lcom/widevine/drmapi/android/WVStatus;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotLicensedByRegion:Lcom/widevine/drmapi/android/WVStatus;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AlreadyInitialized:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->BadMedia:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotLicensed:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->LicenseDenied:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->LicenseExpired:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AssetExpired:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotLicensedByRegion:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_c
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->LicenseRequestLimitReached:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_d
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->BadURL:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_e
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->FileNotPresent:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_f
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotRegistered:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_10
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AlreadyRegistered:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_11
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotPlaying:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_12
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AlreadyPlaying:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_13
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->FileSystemError:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_14
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->AssetDBWasCorrupted:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_15
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->ClockTamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_16
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_17
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_18
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OutOfMemoryError:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_19
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_1a
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->PendingServerNotification:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_1b
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->HardwareIDAbsent:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_1c
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OutOfRange:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_1d
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->HeartbeatError:Lcom/widevine/drmapi/android/WVStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    sput-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->f:Landroid/net/ConnectivityManager;

    return-void
.end method

.method public static c()Lcom/widevine/drm/internal/e;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->b:Lcom/widevine/drm/internal/e;

    return-object v0
.end method

.method private native cs(Ljava/lang/String;I)I
.end method

.method public static d()V
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    return-void
.end method

.method private native d(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;
.end method

.method public static e()Lcom/widevine/drm/internal/f;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/widevine/drm/internal/f;

    return-object v0
.end method

.method private native glem()Ljava/lang/String;
.end method

.method private native gms([I)V
.end method

.method private native hhe(IILjava/lang/String;)V
.end method

.method private native hhr(ILjava/lang/String;)I
.end method

.method public static isOnline()Z
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native os(Ljava/lang/String;I)I
.end method

.method private native sr([I)I
.end method

.method private native uc(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method


# virtual methods
.method public final declared-synchronized a(ILjava/lang/String;)I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/HTTPDecrypter;->hhr(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/HTTPDecrypter;->cs(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(IILjava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/widevine/drm/internal/HTTPDecrypter;->hhe(IILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/widevine/drm/internal/e;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/widevine/drm/internal/HTTPDecrypter;->b:Lcom/widevine/drm/internal/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/ad;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z

    sput-object p3, Lcom/widevine/drm/internal/HTTPDecrypter;->c:Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/HTTPDecrypter;->os(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v0, v1, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    sput-boolean v1, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z

    :cond_1
    new-instance v1, Lcom/widevine/drm/internal/ad;

    invoke-direct {p0}, Lcom/widevine/drm/internal/HTTPDecrypter;->glem()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    sput-boolean v0, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/ad;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p15}, Lcom/widevine/drm/internal/HTTPDecrypter;->uc(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v0, v1, :cond_0

    new-instance v1, Lcom/widevine/drm/internal/ad;

    invoke-direct {p0}, Lcom/widevine/drm/internal/HTTPDecrypter;->glem()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a([I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/HTTPDecrypter;->gms([I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/nio/ByteBuffer;ILjava/lang/String;)[Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/widevine/drm/internal/HTTPDecrypter;->c:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x6a

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/widevine/drm/internal/HTTPDecrypter;->d(Ljava/nio/ByteBuffer;I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b([I)I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/HTTPDecrypter;->sr([I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/widevine/drm/internal/HTTPDecrypter;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public contentEncryptedCallback(Z)V
    .locals 0

    return-void
.end method

.method public native gin(I)I
.end method

.method public httpRequestCallback(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/widevine/drm/internal/k;

    invoke-direct {v0, p1, p2, p3}, Lcom/widevine/drm/internal/k;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public native ir()Z
.end method

.method public native queryLicense(JJJ)I
.end method

.method public native queryLicense(Ljava/lang/String;)I
.end method

.method public native queryLicenses()I
.end method

.method public native queryRegisteredAsset(Ljava/lang/String;)I
.end method

.method public native queryRegisteredAssets()I
.end method

.method public native refreshLicense(JJJ)I
.end method

.method public native registerAsset(Ljava/lang/String;)I
.end method

.method public registeredAssetInfoCallback(ILjava/lang/String;ZIII)V
    .locals 9

    new-instance v0, Lcom/widevine/drm/internal/f;

    int-to-long v3, p4

    int-to-long v5, p5

    int-to-long v7, p6

    move-object v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v8}, Lcom/widevine/drm/internal/f;-><init>(Ljava/lang/String;ZJJJ)V

    sget-object v1, Lcom/widevine/drm/internal/HTTPDecrypter;->e:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/widevine/drm/internal/b;

    invoke-static {p1}, Lcom/widevine/drm/internal/x;->a(I)Lcom/widevine/drm/internal/x;

    move-result-object v1

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->SystemCallError:Lcom/widevine/drmapi/android/WVStatus;

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/b;-><init>(Lcom/widevine/drm/internal/x;Lcom/widevine/drmapi/android/WVStatus;)V

    if-eqz p3, :cond_0

    int-to-long v1, p4

    int-to-long v3, p5

    int-to-long v5, p6

    invoke-virtual/range {v0 .. v6}, Lcom/widevine/drm/internal/b;->b(JJJ)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/widevine/drm/internal/b;->a(Ljava/lang/String;)V

    const-string v1, "Unable to queue asset for now online (hd:raic)"

    invoke-virtual {v0, v1}, Lcom/widevine/drm/internal/b;->b(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/widevine/drm/internal/b;->a()V

    :cond_1
    return-void
.end method

.method public reportEventCallback(IILjava/lang/String;ZIZJJJZJJJJLjava/lang/String;)V
    .locals 8

    new-instance v1, Lcom/widevine/drm/internal/b;

    invoke-static {p1}, Lcom/widevine/drm/internal/x;->a(I)Lcom/widevine/drm/internal/x;

    move-result-object v2

    invoke-static {p2}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(I)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/b;-><init>(Lcom/widevine/drm/internal/x;Lcom/widevine/drmapi/android/WVStatus;)V

    if-eqz p13, :cond_0

    move-wide/from16 v2, p16

    move-wide/from16 v4, p18

    move-wide/from16 v6, p20

    invoke-virtual/range {v1 .. v7}, Lcom/widevine/drm/internal/b;->a(JJJ)V

    :cond_0
    if-eqz p6, :cond_1

    move-wide v2, p7

    move-wide/from16 v4, p9

    move-wide/from16 v6, p11

    invoke-virtual/range {v1 .. v7}, Lcom/widevine/drm/internal/b;->b(JJJ)V

    :cond_1
    invoke-virtual {v1, p3}, Lcom/widevine/drm/internal/b;->a(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Lcom/widevine/drm/internal/b;->a(Z)V

    invoke-virtual {v1, p5}, Lcom/widevine/drm/internal/b;->a(I)V

    move-object/from16 v0, p22

    invoke-virtual {v1, v0}, Lcom/widevine/drm/internal/b;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/widevine/drm/internal/b;->a()V

    return-void
.end method

.method public native secureRetrieve()Ljava/lang/String;
.end method

.method public native secureStore(Ljava/lang/String;)I
.end method

.method public native unregisterAsset(Ljava/lang/String;)I
.end method
