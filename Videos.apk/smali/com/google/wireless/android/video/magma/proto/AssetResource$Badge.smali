.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Badge"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;


# instance fields
.field private audio51_:Z

.field private eastwood_:Z

.field private hasAudio51:Z

.field private hasEastwood:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51_:Z

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51_:Z

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$6502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasAudio51:Z

    return p1
.end method

.method static synthetic access$6602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51_:Z

    return p1
.end method

.method static synthetic access$6702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasEastwood:Z

    return p1
.end method

.method static synthetic access$6802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->access$6300()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAudio51()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51_:Z

    return v0
.end method

.method public getEastwood()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasAudio51()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getAudio51()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasEastwood()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getEastwood()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasAudio51()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasAudio51:Z

    return v0
.end method

.method public hasEastwood()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasEastwood:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasAudio51()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getAudio51()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hasEastwood()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getEastwood()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    return-void
.end method
