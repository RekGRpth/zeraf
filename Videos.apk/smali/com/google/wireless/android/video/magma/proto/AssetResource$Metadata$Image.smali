.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;


# instance fields
.field private hasHeight:Z

.field private hasResizable:Z

.field private hasType:Z

.field private hasUrl:Z

.field private hasWidth:Z

.field private height_:I

.field private memoizedSerializedSize:I

.field private resizable_:Z

.field private type_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

.field private url_:Ljava/lang/String;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url_:Ljava/lang/String;

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width_:I

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url_:Ljava/lang/String;

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width_:I

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable_:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->TYPE_LOGO:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->access$100()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height_:I

    return v0
.end method

.method public getResizable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getResizable()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width_:I

    return v0
.end method

.method public hasHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight:Z

    return v0
.end method

.method public hasResizable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl:Z

    return v0
.end method

.method public hasWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getWidth()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getResizable()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_4
    return-void
.end method
