.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$4400()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setIsPreferred(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseStatus(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalLongTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalShortTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalExpirationTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseToken(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getIsPreferred()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setIsPreferred(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseStatus()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseStatus(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseTimestampSec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalLongTimerSec()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalLongTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalShortTimerSec()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalShortTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalExpirationTimestampSec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setRentalExpirationTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->setPurchaseToken(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    goto :goto_0
.end method

.method public setIsPreferred(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$4602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred_:Z
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$4702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    return-object p0
.end method

.method public setPurchaseStatus(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-object p0
.end method

.method public setPurchaseTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;J)J

    return-object p0
.end method

.method public setPurchaseToken(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$6002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$6102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setPurchaseType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$4802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$4902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    return-object p0
.end method

.method public setRentalExpirationTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;J)J

    return-object p0
.end method

.method public setRentalLongTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec_:I
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;I)I

    return-object p0
.end method

.method public setRentalShortTimerSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec_:I
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->access$5702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;I)I

    return-object p0
.end method
