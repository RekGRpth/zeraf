.class public final Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResourceId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setMid(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setMid(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$302(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$402(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$502(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$602(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$702(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->access$802(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-object p0
.end method
