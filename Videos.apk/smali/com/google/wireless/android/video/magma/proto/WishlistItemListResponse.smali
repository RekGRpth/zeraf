.class public final Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "WishlistItemListResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$1;,
        Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;


# instance fields
.field private memoizedSerializedSize:I

.field private resourceIds_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation
.end field

.field private resource_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItem;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->create()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->access$100()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->newBuilder()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    # invokes: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->buildParsed()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->access$000(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getResourceIds(I)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method public getResourceIdsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getResourceIdsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;

    return-object v0
.end method

.method public getResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceIdsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v5, 0x1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_2

    :cond_2
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    iput v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceIdsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method
