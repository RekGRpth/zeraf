.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Playback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;


# instance fields
.field private device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

.field private hasDevice:Z

.field private hasPositionMsec:Z

.field private hasStartTimestampMsec:Z

.field private hasStopTimestampMsec:Z

.field private memoizedSerializedSize:I

.field private positionMsec_:J

.field private startTimestampMsec_:J

.field private stopTimestampMsec_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec_:J

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/VideoResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec_:J

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec_:J

    return-wide p1
.end method

.method static synthetic access$1202(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J

    return-wide p1
.end method

.method static synthetic access$1402(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec_:J

    return-wide p1
.end method

.method static synthetic access$1602(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->access$800()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-object v0
.end method

.method public getPositionMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStartTimestampMsec()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getStartTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec_:J

    return-wide v0
.end method

.method public getStopTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J

    return-wide v0
.end method

.method public hasDevice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice:Z

    return v0
.end method

.method public hasPositionMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec:Z

    return v0
.end method

.method public hasStartTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec:Z

    return v0
.end method

.method public hasStopTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStartTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    return-void
.end method
