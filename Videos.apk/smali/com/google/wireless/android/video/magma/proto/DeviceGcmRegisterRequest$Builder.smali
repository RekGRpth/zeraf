.class public final Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DeviceGcmRegisterRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;",
        "Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->create()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;-><init>(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->build()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->create()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getResourceId()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->getResourceId()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    return-object v0
.end method

.method public hasResourceId()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasResourceId()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->setRegistrationId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->hasResourceId()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->getResourceId()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->setResourceId(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasRegistrationId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->getRegistrationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->setRegistrationId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->getResourceId()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->mergeResourceId(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    goto :goto_0
.end method

.method public mergeResourceId(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    # getter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$600(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    # getter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$600(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->newBuilder(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$602(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$502(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$602(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    goto :goto_0
.end method

.method public setRegistrationId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasRegistrationId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$302(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$402(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setResourceId(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$502(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->result:Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    # setter for: Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->access$602(Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-object p0
.end method
