.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Format"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;


# instance fields
.field private hasUrl:Z

.field private hasValue:Z

.field private memoizedSerializedSize:I

.field private url_:Ljava/lang/String;

.field private value_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/VideoResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$302(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasValue:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasUrl:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;->access$100()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->getValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value_:I

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasUrl:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasValue:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->getValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    return-void
.end method
