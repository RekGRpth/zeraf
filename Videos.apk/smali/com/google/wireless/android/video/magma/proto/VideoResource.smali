.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/VideoResource$1;,
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;,
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;,
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource;


# instance fields
.field private format_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;",
            ">;"
        }
    .end annotation
.end field

.field private hasPlayback:Z

.field private hasResourceId:Z

.field private hasViewerLastStarted:Z

.field private hasViewerStarted:Z

.field private memoizedSerializedSize:I

.field private pinnedOnDevice_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;",
            ">;"
        }
    .end annotation
.end field

.field private playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

.field private resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field private viewerLastStarted_:J

.field private viewerStarted_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted_:J

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/VideoResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted_:J

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object p1
.end method

.method static synthetic access$2702(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted:Z

    return p1
.end method

.method static synthetic access$2802(Lcom/google/wireless/android/video/magma/proto/VideoResource;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted_:J

    return-wide p1
.end method

.method static synthetic access$2902(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted:Z

    return p1
.end method

.method static synthetic access$3002(Lcom/google/wireless/android/video/magma/proto/VideoResource;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->access$1900()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    # invokes: Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildParsed()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->access$1800(Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getFormatList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;

    return-object v0
.end method

.method public getPinnedOnDeviceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;

    return-object v0
.end method

.method public getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object v0
.end method

.method public getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getFormatList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPinnedOnDeviceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerStarted()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerLastStarted()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getViewerLastStarted()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted_:J

    return-wide v0
.end method

.method public getViewerStarted()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted_:J

    return-wide v0
.end method

.method public hasPlayback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback:Z

    return v0
.end method

.method public hasResourceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId:Z

    return v0
.end method

.method public hasViewerLastStarted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted:Z

    return v0
.end method

.method public hasViewerStarted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getFormatList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPinnedOnDeviceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerStarted()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerLastStarted()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_5
    return-void
.end method
