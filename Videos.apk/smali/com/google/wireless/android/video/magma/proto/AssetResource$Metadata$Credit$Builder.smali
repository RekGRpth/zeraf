.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1400()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->setName(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->setRole(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->setName(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getRole()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->setRole(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->access$1602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->access$1702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setRole(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->access$1802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->access$1902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-object p0
.end method
