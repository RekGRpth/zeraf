.class public final enum Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
.super Ljava/lang/Enum;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum ACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum INACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum PENDING:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum RECENTLY_CANCELLED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum RECENTLY_EXPIRED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field public static final enum RECENTLY_REJECTED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->INACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->PENDING:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->ACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "RECENTLY_CANCELLED"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_CANCELLED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "RECENTLY_REJECTED"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_REJECTED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const-string v1, "RECENTLY_EXPIRED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_EXPIRED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->INACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->PENDING:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->ACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_CANCELLED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_REJECTED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_EXPIRED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status$1;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status$1;-><init>()V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->index:I

    iput p4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->INACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->PENDING:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->ACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_CANCELLED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_REJECTED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->RECENTLY_EXPIRED:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    .locals 1

    const-class v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    invoke-virtual {v0}, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->value:I

    return v0
.end method
