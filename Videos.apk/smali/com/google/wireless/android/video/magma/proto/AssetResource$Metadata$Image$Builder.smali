.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setUrl(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setWidth(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setHeight(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setResizable(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setUrl(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setWidth(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setHeight(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getResizable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setResizable(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    goto :goto_0
.end method

.method public setHeight(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasHeight:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height_:I
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;I)I

    return-object p0
.end method

.method public setResizable(Z)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasResizable:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable_:Z
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$1002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    return-object p0
.end method

.method public setType(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$1102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$1202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasUrl:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setWidth(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->hasWidth:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width_:I
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->access$602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;I)I

    return-object p0
.end method
