.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$800()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;-><init>(Lcom/google/wireless/android/video/magma/proto/VideoResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->build()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object v0
.end method

.method public clearStopTimestampMsec()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x0

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1202(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const-wide/16 v1, 0x0

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J
    invoke-static {v0, v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1302(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    return-object v0
.end method

.method public getPositionMsec()J
    .locals 2

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStartTimestampMsec()J
    .locals 2

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStartTimestampMsec()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStopTimestampMsec()J
    .locals 2

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasDevice()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice()Z

    move-result v0

    return v0
.end method

.method public hasStartTimestampMsec()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec()Z

    move-result v0

    return v0
.end method

.method public hasStopTimestampMsec()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1700(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1700(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->newBuilder(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1702(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1602(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1702(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->hasDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStartTimestampMsec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDevice()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    goto :goto_0
.end method

.method public setDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasDevice:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1602(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device_:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1702(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    return-object p0
.end method

.method public setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasPositionMsec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1402(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1502(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J

    return-object p0
.end method

.method public setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStartTimestampMsec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1002(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1102(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J

    return-object p0
.end method

.method public setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hasStopTimestampMsec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1202(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->access$1302(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;J)J

    return-object p0
.end method
