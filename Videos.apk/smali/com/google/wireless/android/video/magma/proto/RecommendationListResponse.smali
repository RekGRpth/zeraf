.class public final Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$1;,
        Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;


# instance fields
.field private memoizedSerializedSize:I

.field private resource_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/Recommendation;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$300(Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->create()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->access$100()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->getResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/16 v4, 0x3ef

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->getResourceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_0
    return-void
.end method
