.class public final enum Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
.super Ljava/lang/Enum;
.source "AssetResourceId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum CONTAINER:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum EDITORIAL:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum EPISODE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum MUSIC_ALBUM:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum MUSIC_ARTIST:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum MUSIC_SONG:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum SEASON:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field public static final enum SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x7

    const/4 v8, 0x6

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "MUSIC_ALBUM"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ALBUM:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "MUSIC_ARTIST"

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ARTIST:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "MUSIC_SONG"

    invoke-direct {v0, v1, v5, v5, v7}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_SONG:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "MOVIE"

    invoke-direct {v0, v1, v6, v6, v8}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "CONTAINER"

    invoke-direct {v0, v1, v7, v7, v9}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->CONTAINER:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "EDITORIAL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EDITORIAL:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "SHOW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "SEASON"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SEASON:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const-string v1, "EPISODE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EPISODE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ALBUM:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ARTIST:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_SONG:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->CONTAINER:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EDITORIAL:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SEASON:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v1, v0, v9

    const/16 v1, 0x8

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EPISODE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type$1;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type$1;-><init>()V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->index:I

    iput p4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ALBUM:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ARTIST:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_SONG:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->CONTAINER:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EDITORIAL:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SEASON:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->EPISODE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .locals 1

    const-class v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-virtual {v0}, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->value:I

    return v0
.end method
