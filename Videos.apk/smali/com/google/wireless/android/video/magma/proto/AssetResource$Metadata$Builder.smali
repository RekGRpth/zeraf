.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2700()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object v0
.end method


# virtual methods
.method public addContentRatings(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addCredits(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addImages(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setTitle(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setDescription(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setReleaseDateTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setDurationSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addImages(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setSequenceNumber(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addCredits(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_9
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addContentRatings(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setTitle(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setDescription(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getReleaseDateTimestampSec()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setReleaseDateTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDurationSec()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setDurationSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_5
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_6
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_7
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_8
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getSequenceNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setSequenceNumber(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_a
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_b
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_c
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;

    :cond_d
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setDurationSec(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec_:I
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$4002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;I)I

    return-object p0
.end method

.method public setReleaseDateTimestampSec(J)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;J)J

    return-object p0
.end method

.method public setSequenceNumber(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$4102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$4202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->access$3402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
