.class public final Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidClientInfo"
.end annotation


# instance fields
.field private androidId_:J

.field private applicationBuild_:Ljava/lang/String;

.field private cachedSize:I

.field private hasAndroidId:Z

.field private hasApplicationBuild:Z

.field private hasLoggingId:Z

.field private hasModel:Z

.field private hasOsBuild:Z

.field private hasProduct:Z

.field private hasSdkVersion:Z

.field private loggingId_:Ljava/lang/String;

.field private model_:Ljava/lang/String;

.field private osBuild_:Ljava/lang/String;

.field private product_:Ljava/lang/String;

.field private sdkVersion_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAndroidId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId_:J

    return-wide v0
.end method

.method public getApplicationBuild()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->cachedSize:I

    return v0
.end method

.method public getLoggingId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId_:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model_:Ljava/lang/String;

    return-object v0
.end method

.method public getOsBuild()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild_:Ljava/lang/String;

    return-object v0
.end method

.method public getProduct()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product_:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasAndroidId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getAndroidId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasLoggingId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getLoggingId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasSdkVersion()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getSdkVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasModel()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasProduct()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getProduct()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasOsBuild()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getOsBuild()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasApplicationBuild()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getApplicationBuild()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->cachedSize:I

    return v0
.end method

.method public hasAndroidId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasAndroidId:Z

    return v0
.end method

.method public hasApplicationBuild()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasApplicationBuild:Z

    return v0
.end method

.method public hasLoggingId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasLoggingId:Z

    return v0
.end method

.method public hasModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasModel:Z

    return v0
.end method

.method public hasOsBuild()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasOsBuild:Z

    return v0
.end method

.method public hasProduct()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasProduct:Z

    return v0
.end method

.method public hasSdkVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasSdkVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setAndroidId(J)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setLoggingId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setSdkVersion(I)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setModel(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setProduct(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setOsBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setApplicationBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public setAndroidId(J)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasAndroidId:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId_:J

    return-object p0
.end method

.method public setApplicationBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasApplicationBuild:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild_:Ljava/lang/String;

    return-object p0
.end method

.method public setLoggingId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasLoggingId:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId_:Ljava/lang/String;

    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasModel:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model_:Ljava/lang/String;

    return-object p0
.end method

.method public setOsBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasOsBuild:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild_:Ljava/lang/String;

    return-object p0
.end method

.method public setProduct(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasProduct:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product_:Ljava/lang/String;

    return-object p0
.end method

.method public setSdkVersion(I)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasSdkVersion:Z

    iput p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasAndroidId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getAndroidId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasLoggingId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getLoggingId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasSdkVersion()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getSdkVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasModel()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasProduct()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getProduct()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasOsBuild()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getOsBuild()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hasApplicationBuild()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->getApplicationBuild()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    return-void
.end method
