.class Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;
.super Ljava/io/InputStream;
.source "RollingFileStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/RollingFileStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiFileInputStream"
.end annotation


# instance fields
.field private mFileInputStream:Ljava/io/FileInputStream;

.field private final mFileReadingFrom:Ljava/io/File;

.field final synthetic this$0:Lcom/google/android/play/analytics/RollingFileStream;


# direct methods
.method public constructor <init>(Lcom/google/android/play/analytics/RollingFileStream;Ljava/io/File;)V
    .locals 1
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->this$0:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p2, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileReadingFrom:Ljava/io/File;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileInputStream:Ljava/io/FileInputStream;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use MultipleFileStream.closeInputStream to close this stream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close(Z)V
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->this$0:Lcom/google/android/play/analytics/RollingFileStream;

    # getter for: Lcom/google/android/play/analytics/RollingFileStream;->mInputStream:Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;
    invoke-static {v0}, Lcom/google/android/play/analytics/RollingFileStream;->access$500(Lcom/google/android/play/analytics/RollingFileStream;)Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    iput-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileInputStream:Ljava/io/FileInputStream;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileReadingFrom:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->this$0:Lcom/google/android/play/analytics/RollingFileStream;

    # getter for: Lcom/google/android/play/analytics/RollingFileStream;->mAvailableFilesToWrite:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/google/android/play/analytics/RollingFileStream;->access$300(Lcom/google/android/play/analytics/RollingFileStream;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileReadingFrom:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->this$0:Lcom/google/android/play/analytics/RollingFileStream;

    # setter for: Lcom/google/android/play/analytics/RollingFileStream;->mInputStream:Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;
    invoke-static {v0, v2}, Lcom/google/android/play/analytics/RollingFileStream;->access$502(Lcom/google/android/play/analytics/RollingFileStream;Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;)Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;

    return-void
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "use read(byte[]).  It\'s more efficient"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream$MultiFileInputStream;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    return v0
.end method
