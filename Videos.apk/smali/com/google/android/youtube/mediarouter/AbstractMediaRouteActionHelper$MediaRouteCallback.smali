.class Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;
.source "AbstractMediaRouteActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaRouteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;->this$0:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;
    .param p2    # Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;-><init>(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;)V

    return-void
.end method


# virtual methods
.method public onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;->this$0:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;

    # invokes: Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->updateRouteCount()V
    invoke-static {v0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->access$100(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;)V

    return-void
.end method

.method public onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;->this$0:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;

    # invokes: Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->updateRouteCount()V
    invoke-static {v0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->access$100(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;)V

    return-void
.end method
