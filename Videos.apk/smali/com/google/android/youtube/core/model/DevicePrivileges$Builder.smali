.class public final Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;
.super Ljava/lang/Object;
.source "DevicePrivileges.java"

# interfaces
.implements Lcom/google/android/youtube/core/model/ModelBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/DevicePrivileges;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/model/ModelBuilder",
        "<",
        "Lcom/google/android/youtube/core/model/DevicePrivileges;",
        ">;"
    }
.end annotation


# instance fields
.field private design:Ljava/lang/String;

.field private formats:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private make:Ljava/lang/String;

.field private model:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/core/model/DevicePrivileges;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/DevicePrivileges;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->make:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->model:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->design:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->formats:Ljava/util/HashSet;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/DevicePrivileges;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->build()Lcom/google/android/youtube/core/model/DevicePrivileges;

    move-result-object v0

    return-object v0
.end method

.method public device(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->make:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->model:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->design:Ljava/lang/String;

    return-object p0
.end method

.method public formats(Ljava/util/HashSet;)Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->formats:Ljava/util/HashSet;

    return-object p0
.end method
