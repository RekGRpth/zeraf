.class public final Lcom/google/android/youtube/core/model/StreamExtra;
.super Ljava/lang/Object;
.source "StreamExtra.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/StreamExtra$Builder;
    }
.end annotation


# static fields
.field public static final NO_EXTRA:Lcom/google/android/youtube/core/model/StreamExtra;


# instance fields
.field public final lastModified:Ljava/lang/String;

.field public final sizeInBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/model/StreamExtra;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/model/StreamExtra;-><init>(JLjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/model/StreamExtra;->NO_EXTRA:Lcom/google/android/youtube/core/model/StreamExtra;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    iput-object p3, p0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/StreamExtra;->buildUpon()Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/StreamExtra$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;-><init>()V

    iget-wide v1, p0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes(J)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified(Ljava/lang/String;)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/youtube/core/model/StreamExtra;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v3, p0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    iget-wide v5, v0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/youtube/core/model/StreamExtra;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[sizeInBytes:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastModified:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
