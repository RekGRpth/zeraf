.class public final Lcom/google/android/youtube/core/model/Season;
.super Ljava/lang/Object;
.source "Season.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/Season$Builder;
    }
.end annotation


# instance fields
.field public final clipsUri:Landroid/net/Uri;

.field public final credits:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final episodesUri:Landroid/net/Uri;

.field public final finalEpisodeCount:Ljava/lang/Integer;

.field public final firstReleased:Ljava/util/Date;

.field public final hqPosterUri:Landroid/net/Uri;

.field public final posterUri:Landroid/net/Uri;

.field public final selfUri:Landroid/net/Uri;

.field public final showUri:Landroid/net/Uri;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/Integer;Ljava/util/Date;Ljava/util/Map;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # Landroid/net/Uri;
    .param p5    # Landroid/net/Uri;
    .param p6    # Landroid/net/Uri;
    .param p7    # Ljava/lang/Integer;
    .param p8    # Ljava/util/Date;
    .param p10    # Landroid/net/Uri;
    .param p11    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Season;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/Season;->description:Ljava/lang/String;

    const-string v4, "showUri cannot be null"

    invoke-static {p3, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    iput-object v4, p0, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    const-string v4, "selfUri cannot be null"

    invoke-static {p4, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    iput-object v4, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/Season;->clipsUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/Season;->episodesUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/youtube/core/model/Season;->finalEpisodeCount:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/google/android/youtube/core/model/Season;->firstReleased:Ljava/util/Date;

    const-string v4, "credits cannot be null"

    invoke-static {p9, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p9}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/core/model/Season;->credits:Ljava/util/Map;

    :goto_0
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Season;->posterUri:Landroid/net/Uri;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Season;->hqPosterUri:Landroid/net/Uri;

    return-void

    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/core/model/Season;->credits:Ljava/util/Map;

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Season;->buildUpon()Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/Season$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/model/Season$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Season$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->description(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->showUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->selfUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->clipsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->clipsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->episodesUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->episodesUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->finalEpisodeCount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->finalEpisodeCount(Ljava/lang/Integer;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->firstReleased:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->firstReleased(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->credits:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->credits(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->posterUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->posterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->hqPosterUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Season$Builder;->hqPosterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Season$Builder;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    if-ne p1, p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    instance-of v1, p1, Lcom/google/android/youtube/core/model/Season;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/Season;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Season[title = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "selfUri = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showUri = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
