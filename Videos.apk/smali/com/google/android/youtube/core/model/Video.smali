.class public final Lcom/google/android/youtube/core/model/Video;
.super Ljava/lang/Object;
.source "Video.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/Video$1;,
        Lcom/google/android/youtube/core/model/Video$Builder;,
        Lcom/google/android/youtube/core/model/Video$MediaType;,
        Lcom/google/android/youtube/core/model/Video$Trailer;,
        Lcom/google/android/youtube/core/model/Video$Episode;,
        Lcom/google/android/youtube/core/model/Video$Movie;,
        Lcom/google/android/youtube/core/model/Video$Pro;,
        Lcom/google/android/youtube/core/model/Video$Genre;,
        Lcom/google/android/youtube/core/model/Video$ThreeDSource;,
        Lcom/google/android/youtube/core/model/Video$Privacy;,
        Lcom/google/android/youtube/core/model/Video$State;
    }
.end annotation


# static fields
.field private static DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;


# instance fields
.field public final accessControl:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final adultContent:Z

.field public final captionTracksUri:Landroid/net/Uri;

.field public final categoryLabel:Ljava/lang/String;

.field public final categoryTerm:Ljava/lang/String;

.field public final claimed:Z

.field public final commentsUri:Landroid/net/Uri;

.field public final contentRatings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/Rating;",
            ">;"
        }
    .end annotation
.end field

.field public final defaultThumbnailUri:Landroid/net/Uri;

.field public final description:Ljava/lang/String;

.field public final dislikesCount:I

.field public final duration:I

.field public final editUri:Landroid/net/Uri;

.field public final embedAllowed:Z

.field public final episode:Lcom/google/android/youtube/core/model/Video$Episode;

.field public final favoriteCount:I

.field public final hqThumbnailUri:Landroid/net/Uri;

.field public final id:Ljava/lang/String;

.field public final is3d:Z

.field public final isHd:Z

.field public final likesCount:I

.field public final liveEventUri:Landroid/net/Uri;

.field public final location:Ljava/lang/String;

.field public final moderatedAutoplay:Z

.field public final monetize:Z

.field public final monetizeExceptionCountries:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final movie:Lcom/google/android/youtube/core/model/Video$Movie;

.field public final mqThumbnailUri:Landroid/net/Uri;

.field public final owner:Ljava/lang/String;

.field public final ownerDisplayName:Ljava/lang/String;

.field public final ownerUri:Landroid/net/Uri;

.field public final pricing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/PricingStructure;",
            ">;"
        }
    .end annotation
.end field

.field public final privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

.field public final pro:Lcom/google/android/youtube/core/model/Video$Pro;

.field public final publishedDate:Ljava/util/Date;

.field public final rateUri:Landroid/net/Uri;

.field public final relatedUri:Landroid/net/Uri;

.field public final restrictedCountries:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final sdThumbnailUri:Landroid/net/Uri;

.field public final showSubtitlesAlways:Z

.field public final showSubtitlesByDefault:Z

.field public final state:Lcom/google/android/youtube/core/model/Video$State;

.field public final streams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation
.end field

.field public final synopsis:Ljava/lang/String;

.field public final tags:Ljava/lang/String;

.field public final threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

.field public final thumbnailUri:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final title:Ljava/lang/String;

.field public final trailer:Lcom/google/android/youtube/core/model/Video$Trailer;

.field public final uploadedDate:Ljava/util/Date;

.field public final viewCount:I

.field public final watchUri:Landroid/net/Uri;

.field public final where:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "yt:cc_default_lang=([a-zA-Z]{2})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/model/Video;->DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IIIIILjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Lcom/google/android/youtube/core/model/Video$State;Ljava/util/Set;ZZLjava/util/Set;ZLcom/google/android/youtube/core/model/Video$Pro;Ljava/util/List;Landroid/net/Uri;ZLcom/google/android/youtube/core/model/Video$ThreeDSource;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # Landroid/net/Uri;
    .param p5    # Landroid/net/Uri;
    .param p6    # Landroid/net/Uri;
    .param p7    # Landroid/net/Uri;
    .param p8    # Landroid/net/Uri;
    .param p9    # Landroid/net/Uri;
    .param p10    # Landroid/net/Uri;
    .param p11    # Landroid/net/Uri;
    .param p12    # Landroid/net/Uri;
    .param p13    # Landroid/net/Uri;
    .param p14    # Ljava/lang/String;
    .param p15    # I
    .param p16    # I
    .param p17    # I
    .param p18    # I
    .param p19    # I
    .param p20    # Ljava/lang/String;
    .param p21    # Landroid/net/Uri;
    .param p22    # Ljava/util/Date;
    .param p23    # Ljava/util/Date;
    .param p24    # Ljava/lang/String;
    .param p25    # Ljava/lang/String;
    .param p26    # Ljava/lang/String;
    .param p27    # Ljava/lang/String;
    .param p28    # Ljava/lang/String;
    .param p29    # Lcom/google/android/youtube/core/model/Video$Privacy;
    .param p31    # Ljava/lang/String;
    .param p32    # Ljava/lang/String;
    .param p33    # Z
    .param p35    # Lcom/google/android/youtube/core/model/Video$State;
    .param p37    # Z
    .param p38    # Z
    .param p40    # Z
    .param p41    # Lcom/google/android/youtube/core/model/Video$Pro;
    .param p43    # Landroid/net/Uri;
    .param p44    # Z
    .param p45    # Lcom/google/android/youtube/core/model/Video$ThreeDSource;
    .param p46    # Ljava/lang/String;
    .param p47    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "IIIII",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video$Privacy;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/Rating;",
            ">;",
            "Lcom/google/android/youtube/core/model/Video$State;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/android/youtube/core/model/Video$Pro;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/PricingStructure;",
            ">;",
            "Landroid/net/Uri;",
            "Z",
            "Lcom/google/android/youtube/core/model/Video$ThreeDSource;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "youTubeId can\'t be empty"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const-string v1, "streams can\'t be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/Video;->watchUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/Video;->thumbnailUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/youtube/core/model/Video;->sdThumbnailUri:Landroid/net/Uri;

    iput-object p9, p0, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iput-object p10, p0, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iput-object p11, p0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iput-object p12, p0, Lcom/google/android/youtube/core/model/Video;->rateUri:Landroid/net/Uri;

    iput-object p13, p0, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/youtube/core/model/Video;->duration:I

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/youtube/core/model/Video;->favoriteCount:I

    move/from16 v0, p18

    iput v0, p0, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    move/from16 v0, p19

    iput v0, p0, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->ownerUri:Landroid/net/Uri;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->publishedDate:Ljava/util/Date;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->categoryTerm:Ljava/lang/String;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->synopsis:Ljava/lang/String;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->accessControl:Ljava/util/Map;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->location:Ljava/lang/String;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->where:Ljava/lang/String;

    move/from16 v0, p33

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->adultContent:Z

    const-string v1, "contentRatings can\'t be null"

    move-object/from16 v0, p34

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->contentRatings:Ljava/util/List;

    const-string v1, "state can\'t be null"

    move-object/from16 v0, p35

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video$State;

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    const-string v1, "restrictedCountries can\'t be null"

    move-object/from16 v0, p36

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->restrictedCountries:Ljava/util/Set;

    move/from16 v0, p37

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    move/from16 v0, p38

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->monetize:Z

    move/from16 v0, p40

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->moderatedAutoplay:Z

    if-nez p39, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->monetizeExceptionCountries:Ljava/util/Set;

    if-nez p44, :cond_2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-direct {p0, p2, v1}, Lcom/google/android/youtube/core/model/Video;->hasQuality(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/Video;->isHd:Z

    if-eqz p26, :cond_3

    const-string v1, "yt:cc=alwayson"

    move-object/from16 v0, p26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    if-nez v1, :cond_0

    if-eqz p26, :cond_4

    const-string v1, "yt:cc=on"

    move-object/from16 v0, p26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/Video;->showSubtitlesByDefault:Z

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->pro:Lcom/google/android/youtube/core/model/Video$Pro;

    move-object/from16 v0, p41

    instance-of v1, v0, Lcom/google/android/youtube/core/model/Video$Movie;

    if-eqz v1, :cond_5

    move-object/from16 v1, p41

    check-cast v1, Lcom/google/android/youtube/core/model/Video$Movie;

    :goto_4
    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    move-object/from16 v0, p41

    instance-of v1, v0, Lcom/google/android/youtube/core/model/Video$Trailer;

    if-eqz v1, :cond_6

    move-object/from16 v1, p41

    check-cast v1, Lcom/google/android/youtube/core/model/Video$Trailer;

    :goto_5
    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->trailer:Lcom/google/android/youtube/core/model/Video$Trailer;

    move-object/from16 v0, p41

    instance-of v1, v0, Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v1, :cond_7

    check-cast p41, Lcom/google/android/youtube/core/model/Video$Episode;

    :goto_6
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    const-string v1, "pricing can\'t be null"

    move-object/from16 v0, p42

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/Video;->pricing:Ljava/util/List;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->liveEventUri:Landroid/net/Uri;

    move/from16 v0, p44

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    invoke-static/range {p46 .. p46}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :goto_7
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    move/from16 v0, p47

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->embedAllowed:Z

    return-void

    :cond_1
    invoke-static/range {p39 .. p39}, Lcom/google/android/youtube/core/utils/Util;->toLowerInvariant(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    const/16 p41, 0x0

    goto :goto_6

    :cond_8
    move-object/from16 p20, p46

    goto :goto_7
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0    # Ljava/util/List;

    invoke-static {p0}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .param p0    # Ljava/util/Map;

    invoke-static {p0}, Lcom/google/android/youtube/core/model/Video;->unmodifiable(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private hasQuality(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Z
    .locals 3
    .param p2    # Lcom/google/android/youtube/core/model/Stream$Quality;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;",
            "Lcom/google/android/youtube/core/model/Stream$Quality;",
            ")Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, v1, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v2, p2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static unmodifiable(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-ne p0, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    goto :goto_0
.end method

.method private static unmodifiable(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    if-ne p0, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0
.end method

.method private static unmodifiable(Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    if-ne p0, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    goto :goto_0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->buildUpon()Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/Video$Builder;
    .locals 6

    new-instance v3, Lcom/google/android/youtube/core/model/Video$Builder;

    invoke-direct {v3}, Lcom/google/android/youtube/core/model/Video$Builder;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    new-instance v4, Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->streams(Ljava/util/Set;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->watchUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->watchUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->thumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->defaultThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->mqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->sdThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->editUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->commentsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->relatedUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->rateUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->rateUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->captionTracksUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->duration(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->viewCount(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/youtube/core/model/Video;->favoriteCount:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->favoriteCount(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->likesCount(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->dislikesCount(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->owner(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->ownerUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->ownerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->publishedDate:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->publishedDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->categoryTerm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->categoryTerm(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->categoryLabel(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->tags(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->description(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->synopsis:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->synopsis(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->privacy(Lcom/google/android/youtube/core/model/Video$Privacy;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->accessControl:Ljava/util/Map;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->accessControl(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->location:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->location(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->where:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->where(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/Video;->adultContent:Z

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->adultContent(Z)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->contentRatings:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->contentRatings(Ljava/util/List;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->state(Lcom/google/android/youtube/core/model/Video$State;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->restrictedCountries:Ljava/util/Set;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->restrictedCountries(Ljava/util/Set;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->claimed(Z)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/Video;->monetize:Z

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->monetize(Z)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->monetizeExceptionCountries:Ljava/util/Set;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->monetizeExceptionCountries(Ljava/util/Set;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/Video;->moderatedAutoplay:Z

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->moderatedAutoplay(Z)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->pricing:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->pricing(Ljava/util/List;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->liveEventUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->liveEventUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->is3d(Z)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->threeDSource(Lcom/google/android/youtube/core/model/Video$ThreeDSource;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->ownerDisplayName(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isMovie()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isMovie()Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v1, Lcom/google/android/youtube/core/model/Video$MediaType;->MOVIE:Lcom/google/android/youtube/core/model/Video$MediaType;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Video$Builder;->mediaType(Lcom/google/android/youtube/core/model/Video$MediaType;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->releaseMediums:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->releaseMediums(Ljava/util/List;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->genres:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->genres(Ljava/util/List;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->credits:Ljava/util/Map;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->credits(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->releaseDate:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->releaseDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->availabilityStart:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->availabilityStart(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->availabilityEnd:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->availabilityEnd(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->posterUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->posterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/youtube/core/model/Video$Pro;->trailersUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->trailersUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isEpisode()Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v3, Lcom/google/android/youtube/core/model/Video$MediaType;->EPISODE:Lcom/google/android/youtube/core/model/Video$MediaType;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/model/Video$Builder;->mediaType(Lcom/google/android/youtube/core/model/Video$MediaType;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Episode;->showTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->showTitle(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Episode;->showUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->showUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Episode;->seasonTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->seasonTitle(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Episode;->seasonUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->seasonUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Episode;->number:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->episodeNumber(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    :cond_1
    :goto_2
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isTrailer()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Video;->trailer:Lcom/google/android/youtube/core/model/Video$Trailer;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isEpisode()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isTrailer()Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v1, Lcom/google/android/youtube/core/model/Video$MediaType;->TRAILER:Lcom/google/android/youtube/core/model/Video$MediaType;

    goto/16 :goto_1

    :cond_6
    sget-object v1, Lcom/google/android/youtube/core/model/Video$MediaType;->EPISODE:Lcom/google/android/youtube/core/model/Video$MediaType;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isTrailer()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/youtube/core/model/Video$MediaType;->TRAILER:Lcom/google/android/youtube/core/model/Video$MediaType;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/model/Video$Builder;->mediaType(Lcom/google/android/youtube/core/model/Video$MediaType;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Video;->trailer:Lcom/google/android/youtube/core/model/Video$Trailer;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video$Trailer;->trailerForUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Video$Builder;->trailerForUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Video$Builder;

    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/youtube/core/model/Video;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getDefaultSubtitleLanguageCode()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/youtube/core/model/Video;->DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEpisode()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovie()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrailer()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->trailer:Lcom/google/android/youtube/core/model/Video$Trailer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Video[id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
