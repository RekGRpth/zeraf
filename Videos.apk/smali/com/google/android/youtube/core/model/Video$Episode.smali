.class public final Lcom/google/android/youtube/core/model/Video$Episode;
.super Lcom/google/android/youtube/core/model/Video$Pro;
.source "Video.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/Video;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Episode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/Video$Episode$EpisodeGenre;
    }
.end annotation


# instance fields
.field public final number:Ljava/lang/String;

.field public final seasonTitle:Ljava/lang/String;

.field public final seasonUri:Landroid/net/Uri;

.field public final showTitle:Ljava/lang/String;

.field public final showUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Date;Ljava/util/Date;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/util/Date;
    .param p5    # Ljava/util/Date;
    .param p6    # Ljava/util/Date;
    .param p7    # Landroid/net/Uri;
    .param p8    # Landroid/net/Uri;
    .param p9    # Ljava/lang/String;
    .param p10    # Landroid/net/Uri;
    .param p11    # Ljava/lang/String;
    .param p12    # Landroid/net/Uri;
    .param p13    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/Video$Genre;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct/range {p0 .. p8}, Lcom/google/android/youtube/core/model/Video$Pro;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Date;Ljava/util/Date;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;)V

    iput-object p9, p0, Lcom/google/android/youtube/core/model/Video$Episode;->showTitle:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/youtube/core/model/Video$Episode;->showUri:Landroid/net/Uri;

    iput-object p11, p0, Lcom/google/android/youtube/core/model/Video$Episode;->seasonTitle:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/youtube/core/model/Video$Episode;->seasonUri:Landroid/net/Uri;

    iput-object p13, p0, Lcom/google/android/youtube/core/model/Video$Episode;->number:Ljava/lang/String;

    return-void
.end method
