.class public Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;
.super Ljava/lang/Object;
.source "SubtitleWindowTextTimeline.java"

# interfaces
.implements Lcom/google/android/youtube/core/model/ModelBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/model/ModelBuilder",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;",
        ">;"
    }
.end annotation


# instance fields
.field private final endTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final startTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addAppendedLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->addLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p2, v0, :cond_0

    const-string v0, "subtitles are not given in non-decreasing start time order"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->build()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    move-result-object v0

    return-object v0
.end method
