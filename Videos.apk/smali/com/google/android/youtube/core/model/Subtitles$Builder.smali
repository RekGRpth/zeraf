.class public final Lcom/google/android/youtube/core/model/Subtitles$Builder;
.super Ljava/lang/Object;
.source "Subtitles.java"

# interfaces
.implements Lcom/google/android/youtube/core/model/ModelBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/Subtitles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/model/ModelBuilder",
        "<",
        "Lcom/google/android/youtube/core/model/Subtitles;",
        ">;"
    }
.end annotation


# instance fields
.field private final windowBuilders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    return-void
.end method

.method private getOrCreateWindowBuilder(I)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public addAppendedLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->getOrCreateWindowBuilder(I)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->addAppendedLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    return-object p0
.end method

.method public addLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->getOrCreateWindowBuilder(I)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->addLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    return-object p0
.end method

.method public addSettingsToWindow(IILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/Subtitles$Builder;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->getOrCreateWindowBuilder(I)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->addSettings(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/core/model/Subtitles;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles$Builder;->windowBuilders:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->build()Lcom/google/android/youtube/core/model/SubtitleWindow;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/google/android/youtube/core/model/Subtitles;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/youtube/core/model/Subtitles;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/model/Subtitles$1;)V

    return-object v2
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->build()Lcom/google/android/youtube/core/model/Subtitles;

    move-result-object v0

    return-object v0
.end method
