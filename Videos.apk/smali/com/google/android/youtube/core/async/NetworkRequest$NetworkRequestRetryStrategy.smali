.class public abstract Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;
.super Ljava/lang/Object;
.source "NetworkRequest.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/async/NetworkRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "NetworkRequestRetryStrategy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
        "<TR;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canRetry(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)Z
    .locals 1
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")Z"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x191

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;->canRetry(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public extractUserAuth(Lcom/google/android/youtube/core/async/NetworkRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object v0
.end method

.method public bridge synthetic extractUserAuth(Ljava/lang/Object;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;->extractUserAuth(Lcom/google/android/youtube/core/async/NetworkRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method
