.class public final Lcom/google/android/youtube/core/async/Timestamped;
.super Ljava/lang/Object;
.source "Timestamped.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final element:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final timestamp:J


# direct methods
.method public constructor <init>(Ljava/lang/Object;J)V
    .locals 0
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    iput-wide p2, p0, Lcom/google/android/youtube/core/async/Timestamped;->timestamp:J

    return-void
.end method
