.class public Lcom/google/android/youtube/core/async/NetworkRequest;
.super Ljava/lang/Object;
.source "NetworkRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;
    }
.end annotation


# static fields
.field public static final RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final content:[B

.field private volatile hashCode:I

.field public final headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final uri:Landroid/net/Uri;

.field public final userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/async/NetworkRequest$1;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/NetworkRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/async/NetworkRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p4    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[B)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uri can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez p3, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    return-void

    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method protected static cloneContent(Lcom/google/android/youtube/core/async/NetworkRequest;)[B
    .locals 4
    .param p0    # Lcom/google/android/youtube/core/async/NetworkRequest;

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    array-length v1, v2

    new-array v0, v1, [B

    iget-object v2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    return-object v0
.end method

.method public static create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/NetworkRequest;
    .locals 2
    .param p0    # Landroid/net/Uri;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-direct {v0, p0, v1, v1, v1}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    iget-object v4, v0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v4, v0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/accounts/UserAuth;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->hashCode:I

    if-nez v0, :cond_0

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v0, v1, v3

    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    if-nez v3, :cond_2

    :goto_1
    add-int v0, v1, v2

    iput v0, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->hashCode:I

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/UserAuth;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{uri=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "userAuth=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "headers=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "content=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method
