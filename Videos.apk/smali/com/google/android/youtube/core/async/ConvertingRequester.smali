.class public Lcom/google/android/youtube/core/async/ConvertingRequester;
.super Ljava/lang/Object;
.source "ConvertingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;TS;>;"
        }
    .end annotation
.end field

.field private final requester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TS;TF;>;"
        }
    .end annotation
.end field

.field private final responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<TF;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TS;TF;>;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;TS;>;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<TF;TE;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "requester may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;TS;>;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<TF;TE;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/async/ConvertingRequester;)Lcom/google/android/youtube/core/converter/ResponseConverter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/ConvertingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/async/ConvertingRequester;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/ConvertingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TS;TE;>;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;TS;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    const/4 v1, 0x0

    const-string v0, "requestConverter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/youtube/core/async/ConvertingRequester;

    move-object v0, v1

    check-cast v0, Lcom/google/android/youtube/core/converter/ResponseConverter;

    invoke-direct {v2, p0, p1, v0, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v2
.end method

.method public static create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TS;TF;>;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;TS;>;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<TF;TE;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    const-string v0, "requestConverter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "responseConverter may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/ConvertingRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TF;>;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<TF;TE;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    const/4 v1, 0x0

    const-string v0, "responseConverter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/youtube/core/async/ConvertingRequester;

    move-object v0, v1

    check-cast v0, Lcom/google/android/youtube/core/converter/RequestConverter;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;Ljava/util/concurrent/Executor;)V

    return-object v2
.end method


# virtual methods
.method protected doRequest(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TS;TF;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "subclasses should override doRequest"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method protected onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Exception;)V
    .locals 0
    .param p4    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    invoke-interface {p3, p1, p4}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    :try_start_0
    iget-object v4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    invoke-interface {v4, p1}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;-><init>(Lcom/google/android/youtube/core/async/ConvertingRequester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->doRequest(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    move-object v3, p1

    move-object v0, v3

    goto :goto_0

    :catch_0
    move-exception v2

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4, p2, v2}, Lcom/google/android/youtube/core/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Exception;)V

    goto :goto_1
.end method
