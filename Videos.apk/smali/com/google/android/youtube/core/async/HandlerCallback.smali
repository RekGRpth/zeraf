.class public final Lcom/google/android/youtube/core/async/HandlerCallback;
.super Lcom/google/android/youtube/core/async/ThreadingCallback;
.source "HandlerCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/youtube/core/async/ThreadingCallback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final thread:Ljava/lang/Thread;


# direct methods
.method private constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/async/ThreadingCallback;-><init>(Lcom/google/android/youtube/core/async/Callback;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/HandlerCallback;->handler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/HandlerCallback;->thread:Ljava/lang/Thread;

    return-void
.end method

.method public static create(Landroid/os/Handler;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/HandlerCallback;
    .locals 1
    .param p0    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Handler;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/HandlerCallback",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/HandlerCallback;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/async/HandlerCallback;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method protected post(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/HandlerCallback;->thread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/HandlerCallback;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
