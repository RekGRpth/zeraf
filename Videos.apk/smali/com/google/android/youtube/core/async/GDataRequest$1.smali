.class final Lcom/google/android/youtube/core/async/GDataRequest$1;
.super Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;
.source "GDataRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/async/GDataRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy",
        "<",
        "Lcom/google/android/youtube/core/async/GDataRequest;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public canRetry(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .param p2    # Ljava/lang/Exception;

    instance-of v1, p2, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "operation needs a full YouTube account"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;->canRetry(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)Z

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic canRetry(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/GDataRequest$1;->canRetry(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/GDataRequest$1;->canRetry(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public recreateRequest(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 6
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    const-string v0, "newUserAuth can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->uri:Landroid/net/Uri;

    # invokes: Lcom/google/android/youtube/core/async/GDataRequest;->upgradeToHttps(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->access$000(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/youtube/core/async/GDataRequest;->headers:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/NetworkRequest;->cloneContent(Lcom/google/android/youtube/core/async/NetworkRequest;)[B

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[BLcom/google/android/youtube/core/async/GDataRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/GDataRequest$1;->recreateRequest(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method
