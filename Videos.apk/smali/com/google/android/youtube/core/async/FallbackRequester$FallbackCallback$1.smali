.class Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;
.super Ljava/lang/Object;
.source "FallbackRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;

.field final synthetic val$firstException:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;->val$firstException:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;

    # getter for: Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0}, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;->access$100(Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;->val$firstException:Ljava/lang/Exception;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;

    # getter for: Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0}, Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;->access$100(Lcom/google/android/youtube/core/async/FallbackRequester$FallbackCallback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
