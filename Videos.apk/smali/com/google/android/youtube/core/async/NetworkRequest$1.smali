.class final Lcom/google/android/youtube/core/async/NetworkRequest$1;
.super Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;
.source "NetworkRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/async/NetworkRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy",
        "<",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/NetworkRequest$NetworkRequestRetryStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public recreateRequest(Lcom/google/android/youtube/core/async/NetworkRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/NetworkRequest;
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    const-string v0, "newUserAuth can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/NetworkRequest;->cloneContent(Lcom/google/android/youtube/core/async/NetworkRequest;)[B

    move-result-object v3

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public bridge synthetic recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/NetworkRequest$1;->recreateRequest(Lcom/google/android/youtube/core/async/NetworkRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/NetworkRequest;

    move-result-object v0

    return-object v0
.end method
