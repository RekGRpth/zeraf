.class public Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;
.super Ljava/lang/Object;
.source "DefaultDeviceAuthorizer.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Lcom/google/android/youtube/core/async/DeviceAuthorizer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/DeviceAuthorizer;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "Lcom/google/android/youtube/core/model/DeviceAuth;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile authorizingDevice:Z

.field private volatile deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

.field private final deviceRegistrationBlocker:Landroid/os/ConditionVariable;

.field private final deviceRegistrationClient:Lcom/google/android/youtube/core/client/DeviceRegistrationClient;

.field private volatile error:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/DeviceRegistrationClient;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/client/DeviceRegistrationClient;
    .param p2    # Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/DeviceRegistrationClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationClient:Lcom/google/android/youtube/core/client/DeviceRegistrationClient;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->preferences:Landroid/content/SharedPreferences;

    invoke-static {p2}, Lcom/google/android/youtube/core/model/DeviceAuth;->load(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/core/model/DeviceAuth;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationBlocker:Landroid/os/ConditionVariable;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getHeaderValue(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/model/DeviceAuth;->getXGDataDeviceHeaderValue(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->authorizingDevice:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->authorizingDevice:Z

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationBlocker:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationClient:Lcom/google/android/youtube/core/client/DeviceRegistrationClient;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/core/client/DeviceRegistrationClient;->registerDevice(Lcom/google/android/youtube/core/async/Callback;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->error:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->error:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    throw v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationBlocker:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/model/DeviceAuth;->getXGDataDeviceHeaderValue(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/Exception;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    new-instance v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->error:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationBlocker:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->authorizingDevice:Z

    const-string v0, "device registration failed"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;Lcom/google/android/youtube/core/model/DeviceAuth;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/core/model/DeviceAuth;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->error:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceAuth:Lcom/google/android/youtube/core/model/DeviceAuth;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->preferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/model/DeviceAuth;->save(Lcom/google/android/youtube/core/model/DeviceAuth;Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->deviceRegistrationBlocker:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->authorizingDevice:Z

    const-string v0, "device registered"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Lcom/google/android/youtube/core/model/DeviceAuth;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;->onResponse(Landroid/net/Uri;Lcom/google/android/youtube/core/model/DeviceAuth;)V

    return-void
.end method
