.class Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;
.super Ljava/lang/Object;
.source "SubtitleDialogHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->showTrackSelector(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

.field final synthetic val$adapter:Landroid/widget/ArrayAdapter;

.field final synthetic val$listener:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;Landroid/widget/ArrayAdapter;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;->this$0:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;->val$adapter:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;->val$listener:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;->val$adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;->val$listener:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;->onTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
