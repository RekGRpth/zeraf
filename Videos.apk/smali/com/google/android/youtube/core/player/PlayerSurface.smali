.class public interface abstract Lcom/google/android/youtube/core/player/PlayerSurface;
.super Ljava/lang/Object;
.source "PlayerSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;,
        Lcom/google/android/youtube/core/player/PlayerSurface$Listener;
    }
.end annotation


# virtual methods
.method public abstract attachMediaPlayer(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
.end method

.method public abstract closeShutter()V
.end method

.method public abstract getVerticalLetterboxFraction()F
.end method

.method public abstract getVideoDisplayHeight()I
.end method

.method public abstract getVideoDisplayWidth()I
.end method

.method public abstract isSurfaceCreated()Z
.end method

.method public abstract openShutter()V
.end method

.method public abstract recreateSurface()V
.end method

.method public abstract release()V
.end method

.method public abstract setListener(Lcom/google/android/youtube/core/player/PlayerSurface$Listener;)V
.end method

.method public abstract setOnDisplayParametersChangedListener(Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;)V
.end method

.method public abstract setVideoSize(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
.end method

.method public abstract setZoom(I)V
.end method

.method public abstract zoomSupported()Z
.end method
