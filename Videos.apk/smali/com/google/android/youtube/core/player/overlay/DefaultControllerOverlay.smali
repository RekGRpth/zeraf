.class public Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "DefaultControllerOverlay.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;
.implements Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;,
        Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    }
.end annotation


# instance fields
.field private autoHide:Z

.field private final background:Landroid/view/View;

.field private final ccButton:Landroid/widget/ImageButton;

.field private controlsHidden:Z

.field private final density:F

.field private final errorView:Landroid/widget/TextView;

.field private final fadeDurationFast:I

.field private final fadeDurationSlow:I

.field private forceShowButtonsWhenNotFullscreen:Z

.field private fullscreen:Z

.field private final fullscreenButton:Landroid/widget/ImageButton;

.field private final handler:Landroid/os/Handler;

.field private hasCC:Z

.field private hasFullscreen:Z

.field private hasNext:Z

.field private hasPrevious:Z

.field protected final hideAnimation:Landroid/view/animation/Animation;

.field private hideOnTap:Z

.field private final hqButton:Landroid/widget/ImageButton;

.field private lastDownX:I

.field private lastDownY:I

.field private listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

.field private final loadingView:Landroid/widget/LinearLayout;

.field private lockedInFullscreen:Z

.field private final mediaKeyHelper:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

.field private minimal:Z

.field private final nextButton:Landroid/widget/ImageView;

.field private final playPauseReplayView:Landroid/widget/ImageView;

.field private final previousButton:Landroid/widget/ImageView;

.field private showFullscreenInPortrait:Z

.field private state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

.field private style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private final subtitleDialogHelper:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

.field private supportsQualityToggle:Z

.field private final timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/Context;

    const/4 v6, -0x2

    const v8, 0x7f04002a

    const/4 v7, -0x1

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    sget-object v5, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    iput v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v5, Landroid/view/View;

    invoke-direct {v5, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    const v6, 0x7f02001c

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-direct {v5, p1, p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v3, Landroid/widget/ProgressBar;

    invoke-direct {v3, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v9}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v8, p0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    const v6, 0x7f020092

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    const v6, 0x7f0a00e2

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v8, p0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    const v6, 0x7f02008f

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    const v6, 0x7f0a00de

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v8, p0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    const v6, 0x7f020090

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    const v6, 0x7f0a00dc

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-boolean v9, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasFullscreen:Z

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    const v6, 0x7f02006e

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    const v6, 0x7f0a00df

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    const/16 v6, 0x11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    const/high16 v6, -0x34000000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    const v6, 0x7f02000a

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    const v6, 0x7f0a00e4

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    const/4 v6, 0x3

    iget v7, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    invoke-direct {p0, v6, v7}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->px(IF)I

    move-result v6

    const/16 v7, 0xc

    iget v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    invoke-direct {p0, v7, v8}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->px(IF)I

    move-result v7

    invoke-virtual {v5, v10, v6, v7, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    const v6, 0x7f020006

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    const v6, 0x7f0a00e5

    invoke-virtual {p1, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    const/16 v6, 0xc

    iget v7, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    invoke-direct {p0, v6, v7}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->px(IF)I

    move-result v6

    const/4 v7, 0x3

    iget v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    invoke-direct {p0, v7, v8}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->px(IF)I

    move-result v7

    invoke-virtual {v5, v6, v7, v10, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v5, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

    new-instance v6, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;)V

    invoke-direct {v5, v6}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;-><init>(Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

    const/high16 v5, 0x7f050000

    invoke-static {p1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideAnimation:Landroid/view/animation/Animation;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fadeDurationFast:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    sget-object v5, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-boolean v9, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->autoHide:Z

    iput-boolean v9, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideOnTap:Z

    new-instance v5, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

    invoke-direct {v5, p1}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->subtitleDialogHelper:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

    invoke-virtual {p0, v10}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setClipToPadding(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasCC:Z

    return v0
.end method

.method private isDpadDirectionalKey(I)Z
    .locals 1
    .param p1    # I

    const/16 v0, 0x14

    if-eq p1, v0, :cond_0

    const/16 v0, 0x15

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-eq p1, v0, :cond_0

    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutCenteredView(Landroid/view/View;II)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v2, v1, 0x2

    sub-int v2, p2, v2

    div-int/lit8 v3, v0, 0x2

    sub-int v3, p3, v3

    div-int/lit8 v4, v1, 0x2

    add-int/2addr v4, p2

    div-int/lit8 v5, v0, 0x2

    add-int/2addr v5, p3

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method private maybeLayoutBottomView(Landroid/view/View;II)I
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p3, v1

    add-int v2, p2, v0

    invoke-virtual {p1, p2, v1, v2, p3}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeStartHiding()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->autoHide:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x9c4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method

.method private onNext()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasNext:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onNext()V

    :cond_0
    return-void
.end method

.method private onPrevious()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasPrevious:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPrevious()V

    :cond_0
    return-void
.end method

.method private onToggleFullscreen()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onToggleFullscreen(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private px(IF)I
    .locals 2

    int-to-float v0, p1

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private setVisible(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private startHideAnimation(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private updateMinimal()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->minimalWhenNotFullscreen:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v3, v3, v3, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowTimes(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberHeight()I

    move-result v1

    const/16 v4, 0x18

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->density:F

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->px(IF)I

    move-result v4

    sub-int/2addr v1, v4

    div-int/lit8 v0, v1, 0x2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v0, v3, v0, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowTimes(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    goto :goto_1
.end method

.method private updateViews()V
    .locals 7

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    const v1, 0x7f02006e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0a00df

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_0
    move v1, v2

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_8

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-ne v5, v0, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v3, v6, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    if-ne v5, v3, :cond_7

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-eqz v3, :cond_7

    :cond_2
    move v3, v4

    :goto_3
    invoke-direct {p0, v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    const v1, 0x7f02006c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0a00e0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0a00e1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_3

    :cond_8
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v3, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v1, v3, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_9
    move v1, v4

    :goto_4
    invoke-direct {p0, p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    :goto_5
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v1, :cond_18

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasNext:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasPrevious:Z

    if-eqz v1, :cond_18

    :cond_b
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_18

    :goto_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    if-eqz v4, :cond_19

    const v0, 0x7f020008

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    if-eqz v4, :cond_c

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasNext:Z

    if-eqz v0, :cond_1a

    const v0, 0x7f020068

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasPrevious:Z

    if-eqz v0, :cond_1b

    const v0, 0x7f020072

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_c
    return-void

    :cond_d
    move v1, v2

    goto :goto_4

    :cond_e
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-direct {p0, v1, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    invoke-direct {p0, v1, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->showButtonsWhenNotFullscreen:Z

    if-nez v1, :cond_f

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->forceShowButtonsWhenNotFullscreen:Z

    if-eqz v1, :cond_11

    :cond_f
    move v1, v4

    :goto_a
    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_12

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->supportsQualityToggle:Z

    if-eqz v3, :cond_12

    if-eqz v1, :cond_12

    move v3, v4

    :goto_b
    invoke-direct {p0, v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v3, :cond_13

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_13

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasCC:Z

    if-eqz v3, :cond_13

    if-eqz v1, :cond_13

    move v3, v4

    :goto_c
    invoke-direct {p0, v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v5, :cond_14

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasFullscreen:Z

    if-eqz v5, :cond_14

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showFullscreenInPortrait:Z

    if-eqz v1, :cond_14

    :cond_10
    move v1, v4

    :goto_d
    invoke-direct {p0, v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    if-ne v0, v1, :cond_15

    move v1, v4

    :goto_e
    invoke-direct {p0, v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    if-ne v0, v1, :cond_16

    move v1, v4

    :goto_f
    invoke-direct {p0, v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_17

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v1, :cond_17

    move v1, v4

    :goto_10
    invoke-direct {p0, v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisibility(I)V

    goto/16 :goto_5

    :cond_11
    move v1, v2

    goto :goto_a

    :cond_12
    move v3, v2

    goto :goto_b

    :cond_13
    move v3, v2

    goto :goto_c

    :cond_14
    move v1, v2

    goto :goto_d

    :cond_15
    move v1, v2

    goto :goto_e

    :cond_16
    move v1, v2

    goto :goto_f

    :cond_17
    move v1, v2

    goto :goto_10

    :cond_18
    move v4, v2

    goto/16 :goto_6

    :cond_19
    const v0, 0x7f02008e

    goto/16 :goto_7

    :cond_1a
    const v0, 0x7f020069

    goto/16 :goto_8

    :cond_1b
    const v0, 0x7f020073

    goto/16 :goto_9
.end method


# virtual methods
.method protected cancelHiding()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getDefaultTimeBarHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getDefaultVisibleHeight()I

    move-result v0

    return v0
.end method

.method public getFullscreenButton()Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    :goto_0
    return v0

    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public hideControls()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onHidden()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    :cond_1
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onHQ()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onCC()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onToggleFullscreen()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onNext()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onPrevious()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onReplay()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPause()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->isMediaKey(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v3, v4, :cond_3

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->isDpadDirectionalKey(I)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onRetry()V

    :goto_1
    return v2

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    move v2, v1

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v6, p5, p3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v7

    sub-int v2, v6, v7

    sub-int v6, p4, p2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v7

    sub-int v3, v6, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v6

    add-int v5, v6, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v4

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    move-result v6

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    move-result v6

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    move-result v6

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v6, v6, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    move-result v6

    add-int/2addr v4, v6

    :goto_0
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v7

    const/4 v8, 0x0

    sub-int v9, p5, p3

    invoke-virtual {v6, v7, v8, v3, v9}, Landroid/widget/TextView;->layout(IIII)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v6

    div-int/lit8 v7, v3, 0x2

    add-int v0, v6, v7

    sub-int v6, p5, p3

    div-int/lit8 v1, v6, 0x2

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->layoutCenteredView(Landroid/view/View;II)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->layoutCenteredView(Landroid/view/View;II)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v6}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    invoke-direct {p0, v6, v7, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->layoutCenteredView(Landroid/view/View;II)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v7

    add-int/2addr v7, v3

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    invoke-direct {p0, v6, v7, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->layoutCenteredView(Landroid/view/View;II)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v6

    sub-int v4, p4, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v7}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v4, v7

    invoke-direct {p0, v6, v7, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeLayoutBottomView(Landroid/view/View;II)I

    move-result v6

    sub-int/2addr v4, v6

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getTop()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLeft()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getRight()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getTop()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getRight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x0

    const/16 v12, 0x8

    const/high16 v11, 0x40000000

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v8, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {p0, v8, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v8, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v8, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    invoke-static {v9, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v6

    invoke-static {v9, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v9

    sub-int v7, v8, v9

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v8

    sub-int v8, v2, v8

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v9

    sub-int v3, v8, v9

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    move v5, v7

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v8}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberHeight()I

    move-result v8

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v8

    if-eq v8, v12, :cond_0

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v8, v1, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v5, v8

    :cond_0
    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v8

    if-eq v8, v12, :cond_1

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v8, v1, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v5, v8

    :cond_1
    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v8

    if-eq v8, v12, :cond_2

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v8, v1, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v5, v8

    :cond_2
    iget-boolean v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-eqz v8, :cond_3

    div-int/lit8 v8, v3, 0x30

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_0
    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v4}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->measure(II)V

    iget-boolean v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v8}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getMeasuredHeight()I

    move-result v8

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_1
    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, v6, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setMeasuredDimension(II)V

    return-void

    :cond_3
    const/high16 v8, -0x80000000

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_0

    :cond_4
    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v8}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getDefaultVisibleHeight()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    div-int/lit8 v8, v8, 0x2

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1
.end method

.method public onScrubbingEnd(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onSeekTo(I)V

    return-void
.end method

.method public onScrubbingStart()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onScrubbingStart()V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/16 v3, 0xa

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    int-to-float v1, p1

    const v2, 0x3e2aaaab

    mul-float/2addr v1, v2

    float-to-int v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_2

    move v4, v2

    :goto_1
    iput v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_3

    move v4, v3

    :goto_2
    iput v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownY:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v6, :cond_0

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownX:I

    sub-int v0, v4, v2

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownY:I

    sub-int v1, v4, v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    if-eqz v4, :cond_6

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x4b

    if-le v4, v5, :cond_5

    if-lez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onNext()V

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownX:I

    goto :goto_1

    :cond_3
    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lastDownY:I

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onPrevious()V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v5, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onRetry()V

    goto :goto_0

    :cond_6
    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    goto :goto_0

    :cond_7
    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideOnTap:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->subtitleDialogHelper:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->reset()V

    return-void
.end method

.method public resetTime()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->resetTime()V

    return-void
.end method

.method public setAutoHide(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->autoHide:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    goto :goto_0
.end method

.method public setCcEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3
    .param p1    # Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lockedInFullscreen:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_2

    const v0, 0x7f0a00dd

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateMinimal()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v0, 0x7f0a00dc

    goto :goto_1
.end method

.method public setHQ(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    if-eqz p1, :cond_0

    const v0, 0x7f0a00e3

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f0a00e2

    goto :goto_0
.end method

.method public setHQisHD(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const v0, 0x7f020091

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void

    :cond_0
    const v0, 0x7f020092

    goto :goto_0
.end method

.method public setHasCc(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasCC:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setHasNext(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasNext:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasPrevious:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setHideOnTap(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideOnTap:Z

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    return-void
.end method

.method public setLoading()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    goto :goto_0
.end method

.method public setLoadingView(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/widget/LinearLayout$LayoutParams;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->loadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setLockedInFullscreen(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->lockedInFullscreen:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreen:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setFullscreen(Z)V

    return-void
.end method

.method public setPlaying()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    goto :goto_0
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setEnabled(Z)V

    return-void
.end method

.method public setShowButtonsWhenNotFullscreen(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->forceShowButtonsWhenNotFullscreen:Z

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasFullscreen:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setShowFullscreenInPortrait(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showFullscreenInPortrait:Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateMinimal()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->progressColor:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setProgressColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsBuffered:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowBuffered(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->supportsQualityToggle:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    return-void
.end method

.method public setTimes(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setTime(III)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->setTimes(III)V

    return-void
.end method

.method public showControls()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->controlsHidden:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onShown()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    return-void
.end method

.method public showEnded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setBufferedPercent(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    goto :goto_0
.end method

.method public showErrorMessage(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public showErrorMessage(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/DisplayUtil;->hasTouchScreen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0042

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->errorView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    return-void

    :cond_0
    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0043

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v1, ""

    goto :goto_2
.end method

.method public showPaused()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->updateViews()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    goto :goto_0
.end method

.method public showTrackSelector(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->subtitleDialogHelper:Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->showTrackSelector(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V

    return-void
.end method

.method protected startHiding(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideAnimation:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fadeDurationFast:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->minimal:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->background:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fullscreenButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->playPauseReplayView:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->nextButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->previousButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->startHideAnimation(Landroid/view/View;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    int-to-long v0, v0

    goto :goto_0
.end method
