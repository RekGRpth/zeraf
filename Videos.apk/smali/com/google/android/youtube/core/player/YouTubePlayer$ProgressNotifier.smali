.class Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
.super Landroid/os/HandlerThread;
.source "YouTubePlayer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/YouTubePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressNotifier"
.end annotation


# instance fields
.field private final bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final duration:Ljava/util/concurrent/atomic/AtomicInteger;

.field private handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const-string v0, "YouTubePlayer.ProgressNotifier"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    move v4, v5

    :goto_0
    return v4

    :pswitch_0
    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v5}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-eqz v2, :cond_2

    :try_start_0
    invoke-interface {v2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getCurrentPosition()I

    move-result v3

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getDuration()I

    move-result v0

    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    if-lez v3, :cond_1

    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    if-le v3, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I
    invoke-static {v5, v6}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3002(Lcom/google/android/youtube/core/player/YouTubePlayer;I)I

    :cond_0
    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v6, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyProgress(III)V
    invoke-static {v5, v3, v6, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2700(Lcom/google/android/youtube/core/player/YouTubePlayer;III)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    const/4 v6, 0x1

    const-wide/16 v7, 0x3e8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "Error calling mediaPlayer"

    invoke-static {v5, v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->stopNotifying()V

    goto :goto_0

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Looper;->quit()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public quit()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    return-void
.end method

.method public startNotifying()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public stopNotifying()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method
