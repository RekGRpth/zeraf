.class public Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
.super Ljava/lang/Object;
.source "SubtitlesOverlayHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Lcom/google/android/youtube/core/utils/WorkScheduler$Client;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleTrack;",
        "Lcom/google/android/youtube/core/model/Subtitles;",
        ">;",
        "Lcom/google/android/youtube/core/utils/WorkScheduler$Client;"
    }
.end annotation


# instance fields
.field private cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation
.end field

.field private currentEventTimeIndex:I

.field private currentSubtitleEventTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

.field private final listener:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private final subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

.field private final uiHandler:Landroid/os/Handler;

.field private final workScheduler:Lcom/google/android/youtube/core/utils/WorkScheduler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
    .param p3    # Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;
    .param p4    # Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->uiHandler:Landroid/os/Handler;

    const-string v0, "subtitlesOverlay cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/SubtitlesClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->listener:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;

    new-instance v0, Lcom/google/android/youtube/core/utils/WorkScheduler;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/WorkScheduler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->workScheduler:Lcom/google/android/youtube/core/utils/WorkScheduler;

    return-void
.end method

.method private disableSubtitles()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->listener:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;->onSubtitleDisabled()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    :cond_0
    return-void
.end method


# virtual methods
.method public doWork(I)I
    .locals 3
    .param p1    # I

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;->update(Ljava/util/List;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    iget v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    iget v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public freezeSubtitles()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->workScheduler:Lcom/google/android/youtube/core/utils/WorkScheduler;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/WorkScheduler;->unschedule(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;)V

    return-void
.end method

.method public hideSubtitles()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->workScheduler:Lcom/google/android/youtube/core/utils/WorkScheduler;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/WorkScheduler;->unschedule(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;->hide()V

    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/model/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "error retrieving subtitle"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->disableSubtitles()V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->onError(Lcom/google/android/youtube/core/model/SubtitleTrack;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/model/Subtitles;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;
    .param p2    # Lcom/google/android/youtube/core/model/Subtitles;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/Subtitles;->getEventTimes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->listener:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;->onSubtitleEnabled()V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    check-cast p2, Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->onResponse(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/model/Subtitles;)V

    return-void
.end method

.method public reset()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->disableSubtitles()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_0
    return-void
.end method

.method public setFontSizeFromPreferences(Landroid/content/SharedPreferences;)V
    .locals 5
    .param p1    # Landroid/content/SharedPreferences;

    const-string v2, "subtitles_size"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x4

    if-le v0, v2, :cond_0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x2

    :goto_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "subtitles_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;->setFontSizeLevel(I)V

    return-void

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x12 -> :sswitch_1
        0x19 -> :sswitch_2
        0x23 -> :sswitch_3
    .end sparse-switch
.end method

.method public setSelectedTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->disableSubtitles()V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->uiHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->cancelableSubtitlesCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/HandlerCallback;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/SubtitlesClient;->requestSubtitles(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method public syncSubtitles(I)V
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitles:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;->update(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v1, v0, 0x1

    :goto_0
    iput v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    iget v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->workScheduler:Lcom/google/android/youtube/core/utils/WorkScheduler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentSubtitleEventTimes:Ljava/util/List;

    iget v3, p0, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->currentEventTimeIndex:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, p0, p1, v1}, Lcom/google/android/youtube/core/utils/WorkScheduler;->schedule(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    xor-int/lit8 v1, v0, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    goto :goto_1
.end method
