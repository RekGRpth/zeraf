.class public Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;
.super Landroid/view/ViewGroup;
.source "DefaultSubtitlesOverlay.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;


# instance fields
.field private fontSizeLevel:I

.field private final windowSnapshots:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;",
            ">;"
        }
    .end annotation
.end field

.field private final windowTextViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->fontSizeLevel:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->hide()V

    return-void
.end method

.method private createTextView(Ljava/lang/Object;Ljava/lang/CharSequence;)Landroid/widget/TextView;
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;

    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 v1, -0x78000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->maybeSetSoftwareLayerType(Landroid/widget/TextView;)V

    return-object v0
.end method

.method private layoutWindowTextView(Landroid/widget/TextView;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;IIII)V
    .locals 9
    .param p1    # Landroid/widget/TextView;
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget v0, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorPoint:I

    iget v7, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v7, p5

    div-int/lit8 v1, v7, 0x64

    iget v7, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v7, p6

    div-int/lit8 v2, v7, 0x64

    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_0

    move v5, v1

    :goto_0
    and-int/lit8 v7, v0, 0x8

    if-eqz v7, :cond_3

    move v6, v2

    :goto_1
    add-int/2addr v5, p3

    add-int/2addr v6, p4

    add-int v7, v5, v4

    add-int v8, v6, v3

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/widget/TextView;->layout(IIII)V

    return-void

    :cond_0
    and-int/lit8 v7, v0, 0x2

    if-eqz v7, :cond_1

    div-int/lit8 v7, v4, 0x2

    sub-int v5, v1, v7

    goto :goto_0

    :cond_1
    and-int/lit8 v7, v0, 0x4

    if-eqz v7, :cond_2

    sub-int v5, v1, v4

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    :cond_3
    and-int/lit8 v7, v0, 0x10

    if-eqz v7, :cond_4

    div-int/lit8 v7, v3, 0x2

    sub-int v6, v2, v7

    goto :goto_1

    :cond_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    sub-int v6, v2, v3

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private maybeSetSoftwareLayerType(Landroid/widget/TextView;)V
    .locals 2
    .param p1    # Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-gt v1, v0, :cond_0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setSoftwareLayerTypeV11(Landroid/widget/TextView;)V

    :cond_0
    return-void
.end method

.method private measureWindowTextView(Landroid/widget/TextView;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;II)V
    .locals 7
    .param p1    # Landroid/widget/TextView;
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
    .param p3    # I
    .param p4    # I

    const/high16 v6, -0x80000000

    iget v0, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorPoint:I

    iget v5, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v5, p3

    div-int/lit8 v1, v5, 0x64

    iget v5, p2, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v5, p4

    div-int/lit8 v2, v5, 0x64

    and-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_0

    sub-int v4, p3, v1

    :goto_0
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    sub-int v3, p4, v2

    :goto_1
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Landroid/widget/TextView;->measure(II)V

    return-void

    :cond_0
    and-int/lit8 v5, v0, 0x2

    if-eqz v5, :cond_1

    sub-int v5, p3, v1

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    mul-int/lit8 v4, v5, 0x2

    goto :goto_0

    :cond_1
    and-int/lit8 v5, v0, 0x4

    if-eqz v5, :cond_2

    move v4, v1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_4

    sub-int v5, p4, v2

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    mul-int/lit8 v3, v5, 0x2

    goto :goto_1

    :cond_4
    and-int/lit8 v5, v0, 0x20

    if-eqz v5, :cond_5

    move v3, v2

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setSoftwareLayerTypeV11(Landroid/widget/TextView;)V
    .locals 2
    .param p1    # Landroid/widget/TextView;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method private updateSubtitlesTextSize()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const v4, 0x3ce66666

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v3, v4, v5

    const/high16 v4, 0x41500000

    cmpg-float v4, v3, v4

    if-gez v4, :cond_0

    const/high16 v3, 0x41500000

    :cond_0
    const-wide v4, 0x3ff6666660000000L

    iget v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->fontSizeLevel:I

    add-int/lit8 v6, v6, -0x2

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    float-to-int v4, v3

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    iget v6, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v5, v6

    float-to-int v5, v5

    if-ge v4, v5, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->removeView(Landroid/view/View;)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->createTextView(Ljava/lang/Object;Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v4, v1, v2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    :cond_1
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public hide()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v5, p4, p2

    sub-int v6, p5, p3

    mul-int/lit8 v0, v5, 0xf

    div-int/lit8 v0, v0, 0x64

    div-int/lit8 v3, v0, 0x2

    mul-int/lit8 v0, v6, 0xf

    div-int/lit8 v0, v0, 0x64

    div-int/lit8 v4, v0, 0x2

    mul-int/lit8 v0, v5, 0x55

    div-int/lit8 v5, v0, 0x64

    mul-int/lit8 v0, v6, 0x55

    div-int/lit8 v6, v0, 0x64

    const/4 v7, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v8, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->layoutWindowTextView(Landroid/widget/TextView;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;IIII)V

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setMeasuredDimension(II)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->updateSubtitlesTextSize()V

    mul-int/lit8 v4, v3, 0x55

    div-int/lit8 v3, v4, 0x64

    mul-int/lit8 v4, v0, 0x55

    div-int/lit8 v0, v4, 0x64

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-direct {p0, v2, v4, v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->measureWindowTextView(Landroid/widget/TextView;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;II)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setFontSizeLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->fontSizeLevel:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->requestLayout()V

    return-void
.end method

.method public update(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;

    iget v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    iget-boolean v6, v6, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->visible:Z

    if-nez v6, :cond_3

    :cond_1
    if-eqz v4, :cond_2

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    if-nez v4, :cond_4

    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    iget-object v7, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->createTextView(Ljava/lang/Object;Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->addView(Landroid/view/View;)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    iget v7, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v6, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    :cond_4
    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v6, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->removeView(Landroid/view/View;)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowSnapshots:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->windowTextViews:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    :cond_7
    invoke-virtual {p0, v8}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    return-void
.end method
