.class public final Lcom/google/android/youtube/core/player/YouTubePlayer;
.super Ljava/lang/Object;
.source "YouTubePlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;,
        Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;,
        Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;,
        Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;
    }
.end annotation


# static fields
.field public static final ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

.field public static final ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

.field public static final ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

.field public static final ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

.field public static final ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

.field public static final ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

.field public static final ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

.field public static final ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

.field private static final FATAL_ERROR_CODES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile buffering:Z

.field private final context:Landroid/content/Context;

.field private disconnectAtHighWaterMark:Z

.field private enableVirtualSurroundSound:Z

.field private final internalListener:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

.field private final listeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private volatile live:Z

.field private final mediaPlayerFactory:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;

.field private final mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/youtube/core/player/MediaPlayerInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

.field private final openShutterRunnable:Ljava/lang/Runnable;

.field private pendingStream:Lcom/google/android/youtube/core/model/Stream;

.field private final playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

.field private volatile playing:Z

.field private volatile prepared:Z

.field private final progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

.field private readyToPlayOnPrepared:Z

.field private volatile retries:I

.field private volatile retryingInit:Z

.field private volatile retryingPlay:Z

.field private source:Lcom/google/android/youtube/core/model/Stream;

.field private surfaceCreated:Z

.field private final uiHandler:Landroid/os/Handler;

.field private volatile videoSizeKnown:Z

.field private virtualizerInitialized:Z

.field private final virtualizerRef:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/media/audiofx/Virtualizer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v4, 0x7fffffff

    const/16 v3, -0xbb8

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0xf

    if-le v1, v2, :cond_1

    const/16 v1, -0xbb6

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    const/16 v1, -0xbb5

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    const/16 v1, -0xbb4

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    const/16 v1, -0xbb3

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    const/16 v1, -0xbb2

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    const/16 v1, -0xbb1

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    const/16 v1, -0xbb0

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    sput v3, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    :goto_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/16 v1, -0x3e81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    if-eq v1, v4, :cond_0

    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    sget v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->FATAL_ERROR_CODES:Ljava/util/Set;

    return-void

    :cond_1
    sput v3, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    const/16 v1, -0xbb9

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    const/16 v1, -0xbba

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    const/16 v1, -0xbbb

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    const/16 v1, -0xbbc

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    const/16 v1, -0xbbd

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    sput v4, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    const/16 v1, -0xbbe

    sput v1, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/PlayerSurface;Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/core/player/PlayerSurface;
    .param p3    # Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;

    const-string v0, "playerSurface cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/PlayerSurface;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    const-string v0, "mediaPlayerFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerFactory:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerRef:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/player/YouTubePlayer$1;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->internalListener:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->start()V

    new-instance v0, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->uiHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/core/player/YouTubePlayer$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer$1;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/player/PlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->openShutterRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->internalListener:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->setListener(Lcom/google/android/youtube/core/player/PlayerSurface$Listener;)V

    invoke-interface {p2}, Lcom/google/android/youtube/core/player/PlayerSurface;->isSurfaceCreated()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->surfaceCreated:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/model/Stream;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->load(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/PlayerSurface;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isWidevineOnGoogleTV()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/youtube/core/player/YouTubePlayer;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/youtube/core/player/YouTubePlayer;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->uiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifySeekEnd(I)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/youtube/core/player/YouTubePlayer;III)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyProgress(III)V

    return-void
.end method

.method static synthetic access$2802(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    return p1
.end method

.method static synthetic access$2900()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->FATAL_ERROR_CODES:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideoInternal()V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/youtube/core/player/YouTubePlayer;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I

    return v0
.end method

.method static synthetic access$3002(Lcom/google/android/youtube/core/player/YouTubePlayer;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I

    return p1
.end method

.method static synthetic access$3008(Lcom/google/android/youtube/core/player/YouTubePlayer;)I
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I

    return v0
.end method

.method static synthetic access$3100(Lcom/google/android/youtube/core/player/YouTubePlayer;II)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyRecreatedMediaPlayerDueToError(II)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/youtube/core/player/YouTubePlayer;II)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # Landroid/net/Uri;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPrepare(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPlayVideo()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPauseVideo()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/core/player/YouTubePlayer;IZ)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingSeekTo(IZ)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->surfaceCreated:Z

    return p1
.end method

.method private blockingPauseVideo()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    :try_start_0
    invoke-interface {v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->pause()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Error calling mediaPlayer"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private blockingPlayVideo()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->start()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->uiHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->openShutterRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    :cond_2
    :goto_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Error calling mediaPlayer"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->start()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->uiHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->openShutterRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    if-nez v2, :cond_4

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->startNotifying()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private blockingPrepare(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {v2, p1}, Lcom/google/android/youtube/core/player/PlayerSurface;->attachMediaPlayer(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->disconnectAtHighWaterMark:Z

    if-eqz v2, :cond_1

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->canDisconnectAtHighWaterMark()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "x-disconnect-at-highwatermark"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;

    invoke-interface {p1, v2, p2, v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    :goto_0
    invoke-interface {p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->prepareAsync()V

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setScreenOnWhilePlaying(Z)V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V

    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;

    invoke-interface {p1, v2, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Media Player error preparing video"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v2, "Media Player error preparing video"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "Error calling mediaPlayer"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Media Player null pointer preparing video "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private blockingSeekTo(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifySeekTo(I)V

    invoke-interface {v1, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->seekTo(I)V

    if-eqz p2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    if-nez v2, :cond_0

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->start()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->delayedPause(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Error calling mediaPlayer"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private blockingStopVideoInternal()V
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->releaseVirtualizerV9()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->stopNotifying()V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z

    if-nez v1, :cond_1

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    :cond_1
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->release()V

    :cond_2
    return-void
.end method

.method private initVirtualizerV9(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)Landroid/media/audiofx/Virtualizer;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Landroid/media/audiofx/Virtualizer;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getAudioSessionIdV9()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/media/audiofx/Virtualizer;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerInitialized:Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "Failed to initialize virtual surround sound"

    invoke-static {v3, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private isWidevineOnGoogleTV()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    if-eqz v0, :cond_0

    const-string v0, "video/wvm"

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private load(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->stopVideo()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/PlayerSurface;->closeShutter()V

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->surfaceCreated:Z

    if-nez v2, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerFactory:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    invoke-interface {v2, p1, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;->newMediaPlayer(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setAudioStreamType(I)V

    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->enableVirtualSurroundSound:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setVirtualSurroundSoundEnabledV9(Z)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->internalListener:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->prepare(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Factory failed to create a MediaPlayer for the stream"

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private notifyError(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyError(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v0, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyListeners(I)V
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyProgress(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v2, p1, p2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyRecreatedMediaPlayerDueToError(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifySeekEnd(I)V
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifySeekTo(I)V
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private readyToPlay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlayOnPrepared:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseVirtualizerV9()V
    .locals 3

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerInitialized:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerRef:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->release()V

    :cond_0
    return-void
.end method

.method private setBuffering(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->buffering:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->buffering:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->buffering:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x7

    goto :goto_0
.end method


# virtual methods
.method public addListener(Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public blockingStopVideo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->blockingStopVideo()V

    return-void
.end method

.method public getCurrentPositionMillis()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getCurrentPosition()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto :goto_0
.end method

.method public getStream()Lcom/google/android/youtube/core/model/Stream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadLiveVideo(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlayOnPrepared:Z

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->load(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method public loadVideo(Lcom/google/android/youtube/core/model/Stream;I)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/model/Stream;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    const-string v0, "application/x-mpegURL"

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlayOnPrepared:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlayOnPrepared:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isWidevineOnGoogleTV()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlayOnPrepared:Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->load(Lcom/google/android/youtube/core/model/Stream;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public pauseVideo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->pauseVideo()V

    return-void
.end method

.method public playVideo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->playVideo()V

    return-void
.end method

.method public release(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->quit()Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->quit()Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->release()V

    :cond_0
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->seekTo(IZ)V

    return-void
.end method

.method public seekTo(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$200(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->seekTo(IZ)V

    :cond_0
    return-void
.end method

.method public setVirtualSurroundSoundEnabledV9(Z)V
    .locals 5
    .param p1    # Z

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->enableVirtualSurroundSound:Z

    if-eq v3, p1, :cond_1

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->enableVirtualSurroundSound:Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerInitialized:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->virtualizerRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/audiofx/Virtualizer;

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/media/audiofx/Virtualizer;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v0

    iget-object v3, v0, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "3db6f600-0e41-11e2-af49-0002a5d5c51b"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, p1}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    :cond_1
    return-void

    :cond_2
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->initVirtualizerV9(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)Landroid/media/audiofx/Virtualizer;

    move-result-object v2

    goto :goto_0
.end method

.method public stopVideo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerThread:Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->stopVideo()V

    return-void
.end method
