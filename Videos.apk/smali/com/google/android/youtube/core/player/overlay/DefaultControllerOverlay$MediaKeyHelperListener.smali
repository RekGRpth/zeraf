.class final Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;
.super Ljava/lang/Object;
.source "DefaultControllerOverlay.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MediaKeyHelperListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .param p2    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public onCC()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hasCC:Z
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$500(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onCC()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPause()V

    :cond_0
    return-void
.end method

.method public onPlay()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPlay()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onReplay()V

    goto :goto_0
.end method

.method public onScrubbingEnd(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onScrubbingEnd(I)V

    return-void
.end method

.method public onScrubbingStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onScrubbingStart()V

    return-void
.end method

.method public onTogglePlayPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPause()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->state:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$400(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onReplay()V

    goto :goto_0
.end method

.method public onUpdateScrubberTime(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/youtube/core/player/overlay/TimeBar;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$300(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubberTime(I)V

    return-void
.end method
