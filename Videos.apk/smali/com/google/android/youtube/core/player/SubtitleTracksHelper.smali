.class public Lcom/google/android/youtube/core/player/SubtitleTracksHelper;
.super Ljava/lang/Object;
.source "SubtitleTracksHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleTrack;",
        ">;>;"
    }
.end annotation


# instance fields
.field private automaticLanguageCode:Ljava/lang/String;

.field private cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private final disableSubtitlesText:Ljava/lang/String;

.field private final listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

.field private notifyDefaultTrack:Z

.field private notifyTracks:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private showSubtitlesAlways:Z

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private trackSelectionEnabled:Z

.field private tracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private tracksRequestInFlight:Z

.field private final uiHandler:Landroid/os/Handler;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;
    .param p4    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->uiHandler:Landroid/os/Handler;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/SubtitlesClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    iput-object p5, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->disableSubtitlesText:Ljava/lang/String;

    return-void
.end method

.method private disableTrackSelection()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->trackSelectionEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->trackSelectionEnabled:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;->onTrackSelectionDisabled()V

    :cond_0
    return-void
.end method

.method private enableTrackSelection()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->trackSelectionEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->trackSelectionEnabled:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;->onTrackSelectionEnabled()V

    :cond_0
    return-void
.end method

.method private getAutomaticLanguageCode(ZLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "subtitles_language_code"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v0, p2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private internalRequestSubtitleTracks()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracksRequestInFlight:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracksRequestInFlight:Z

    invoke-static {p0}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->uiHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/HandlerCallback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/SubtitlesClient;->requestSubtitleTracks(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method private notifyTracks()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->showSubtitlesAlways:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->disableSubtitlesText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->disableSubtitlesText:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object v0, v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;->onSubtitleTracksResponse(Ljava/util/List;)V

    return-void
.end method

.method private onError()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracksRequestInFlight:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->disableTrackSelection()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;->onSubtitleTracksError()V

    :cond_0
    return-void
.end method


# virtual methods
.method public init(Ljava/lang/String;Landroid/net/Uri;ZZLjava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->reset()V

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->videoId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->showSubtitlesAlways:Z

    invoke-direct {p0, p3, p5}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->getAutomaticLanguageCode(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->automaticLanguageCode:Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->enableTrackSelection()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->automaticLanguageCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyDefaultTrack:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->internalRequestSubtitleTracks()V

    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->onError()V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->onResponse(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracksRequestInFlight:Z

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "SubtitleTrack response was empty"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->onError()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->showSubtitlesAlways:Z

    if-eqz v4, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->enableTrackSelection()V

    :cond_3
    iput-object p2, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyDefaultTrack:Z

    if-eqz v4, :cond_8

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyDefaultTrack:Z

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v4, v2, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->automaticLanguageCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v3, v2

    goto :goto_1

    :cond_5
    if-nez v1, :cond_4

    const-string v4, "en"

    iget-object v5, v2, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v1, v2

    goto :goto_1

    :cond_6
    if-nez v3, :cond_7

    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->showSubtitlesAlways:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/model/SubtitleTrack;

    :cond_7
    if-eqz v3, :cond_8

    iget-object v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->listener:Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;

    invoke-interface {v4, v3}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;->onDefaultSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    :cond_8
    iget-boolean v4, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks:Z

    if-eqz v4, :cond_0

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks()V

    goto :goto_0
.end method

.method public requestSubtitleTracks()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->videoId:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "call init() first"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->internalRequestSubtitleTracks()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->videoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracks:Ljava/util/List;

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyTracks:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->notifyDefaultTrack:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->tracksRequestInFlight:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->disableTrackSelection()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->cancelableTracksCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_0
    return-void
.end method

.method public setLanguagePreference(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method
