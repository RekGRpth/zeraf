.class public Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;
.super Ljava/lang/Object;
.source "MediaKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;
    }
.end annotation


# instance fields
.field private currentTime:I

.field private keyInitiatingScrubbing:I

.field private final listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

.field private scrubbingTime:I

.field private totalTime:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    const/high16 v1, -0x80000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->currentTime:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->totalTime:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    return-void
.end method

.method private static isHandledMediaKey(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMediaKey(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x5a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0x59

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x82

    if-eq p0, v0, :cond_0

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v4, 0x59

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    if-eq p1, v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-eq p1, v4, :cond_1

    const/16 v0, 0x5a

    if-ne p1, v0, :cond_5

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->currentTime:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->totalTime:I

    if-eq v0, v3, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->currentTime:I

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onScrubbingStart()V

    :cond_2
    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-ne p1, v4, :cond_4

    const/16 v0, -0x4e20

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->totalTime:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onUpdateScrubberTime(I)V

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v0, 0x4e20

    goto :goto_1

    :cond_5
    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->isHandledMediaKey(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    sparse-switch p1, :sswitch_data_0

    :goto_2
    move v0, v1

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onTogglePlayPause()V

    goto :goto_2

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onPlay()V

    goto :goto_2

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onPause()V

    goto :goto_2

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onCC()V

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_2
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
        0xaf -> :sswitch_3
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    const/16 v2, 0x59

    if-eq p1, v2, :cond_0

    const/16 v2, 0x5a

    if-ne p1, v2, :cond_2

    :cond_0
    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    if-eq v2, v4, :cond_1

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->listener:Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;->onScrubbingEnd(I)V

    iput v4, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->scrubbingTime:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->keyInitiatingScrubbing:I

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->isHandledMediaKey(I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public setTimes(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->currentTime:I

    iput p2, p0, Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;->totalTime:I

    return-void
.end method
