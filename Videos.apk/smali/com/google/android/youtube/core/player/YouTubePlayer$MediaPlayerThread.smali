.class Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;
.super Landroid/os/HandlerThread;
.source "YouTubePlayer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/YouTubePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaPlayerThread"
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const-string v0, "YouTubePlayer.MediaPlayerThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized blockingStopVideo()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideoInternal()V
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$300(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public delayedPause(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x7

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public declared-synchronized handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v6, 0x7

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    iget v4, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v4, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return v3

    :pswitch_0
    :try_start_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    move-object v0, v3

    check-cast v0, [Ljava/lang/Object;

    move-object v1, v0

    iget-object v6, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    check-cast v3, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v4, 0x1

    aget-object v4, v1, v4

    check-cast v4, Landroid/net/Uri;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPrepare(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V
    invoke-static {v6, v3, v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$400(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V

    move v3, v5

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPlayVideo()V
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$500(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    move v3, v5

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingPauseVideo()V
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$600(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    move v3, v5

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget v6, p1, Landroid/os/Message;->arg1:I

    iget v7, p1, Landroid/os/Message;->arg2:I

    if-eqz v7, :cond_0

    move v3, v5

    :cond_0
    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingSeekTo(IZ)V
    invoke-static {v4, v6, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$700(Lcom/google/android/youtube/core/player/YouTubePlayer;IZ)V

    move v3, v5

    goto :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideoInternal()V
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$300(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v5

    goto :goto_0

    :pswitch_5
    :try_start_2
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->mediaPlayerRef:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v3}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->pause()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    move v3, v5

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_3
    const-string v3, "Error delayed-pausing media player"

    invoke-static {v3, v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :pswitch_6
    :try_start_4
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideoInternal()V
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$300(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v3, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public pauseVideo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public playVideo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public prepare(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/net/Uri;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # Landroid/net/Uri;

    const/4 v3, 0x1

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    aput-object p2, v0, v3

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public quit()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public seekTo(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v3, 0x4

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v3, p1, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    return-void
.end method

.method public stopVideo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerThread;->handler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
