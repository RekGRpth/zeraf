.class public Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
.super Landroid/view/ViewGroup;
.source "DefaultPlayerSurface.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/youtube/core/player/PlayerSurface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;
    }
.end annotation


# instance fields
.field private listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

.field private onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

.field private final openShutterRunnable:Ljava/lang/Runnable;

.field private final shutterView:Landroid/view/View;

.field private surfaceCreated:Z

.field private final surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I

.field private videoHeight:I

.field private videoWidth:I

.field private zoom:I

.field private zoomSupported:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v1, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$1;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayWidth:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoomSupported:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoom:I

    return v0
.end method

.method private releaseV14()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method


# virtual methods
.method public attachMediaPlayer(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDisplay(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public closeShutter()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public getHorizontalLetterboxFraction()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v0, v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    return v0
.end method

.method public getVerticalLetterboxFraction()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v0, v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    return v0
.end method

.method public getVideoDisplayHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayHeight:I

    return v0
.end method

.method public getVideoDisplayWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayWidth:I

    return v0
.end method

.method public isSurfaceCreated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceCreated:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->measure(II)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->setMeasuredDimension(II)V

    return-void
.end method

.method public openShutter()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public recreateSurface()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public release()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->releaseV14()V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/PlayerSurface$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    return-void
.end method

.method public setOnDisplayParametersChangedListener(Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    return-void
.end method

.method public setVideoSize(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoomSupported:Z

    if-nez v1, :cond_0

    const/4 v1, 0x2

    :try_start_0
    invoke-interface {p1, v1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setVideoScalingModeV16(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoomSupported:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;->onZoomSupportedChanged(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput p2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I

    iput p3, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->requestLayout()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set video scaling mode"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setZoom(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoom:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoom:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->requestLayout()V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface$Listener;->surfaceChanged()V

    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceCreated:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface$Listener;->surfaceCreated()V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->surfaceCreated:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->listener:Lcom/google/android/youtube/core/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface$Listener;->surfaceDestroyed()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->closeShutter()V

    return-void
.end method

.method public zoomSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoomSupported:Z

    return v0
.end method
