.class Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;
.super Ljava/lang/Object;
.source "DefaultPlayerSurface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

.field final synthetic val$this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->val$this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget-object v0, v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$100(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget-object v0, v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$100(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v1, v1, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;->this$1:Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v2, v2, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;->onLetterboxChanged(FF)V

    :cond_0
    return-void
.end method
