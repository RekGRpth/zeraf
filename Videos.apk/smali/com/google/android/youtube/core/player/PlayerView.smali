.class public final Lcom/google/android/youtube/core/player/PlayerView;
.super Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;
.source "PlayerView.java"


# instance fields
.field private final playerSurface:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/PlayerView;->playerSurface:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/PlayerView;->playerSurface:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/PlayerView;->setVideoView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/PlayerView;->playerSurface:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    return-object v0
.end method
