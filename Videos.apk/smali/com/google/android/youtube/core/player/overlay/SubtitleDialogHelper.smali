.class public Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;
.super Ljava/lang/Object;
.source "SubtitleDialogHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private subtitleDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    return-void
.end method

.method public showTrackSelector(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V
    .locals 5
    .param p2    # Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;",
            "Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;",
            ")V"
        }
    .end annotation

    const-string v3, "listener cannot be null"

    invoke-static {p2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->reset()V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->context:Landroid/content/Context;

    const v4, 0x1090011

    invoke-direct {v0, v3, v4, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v2, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$1;-><init>(Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;Landroid/widget/ArrayAdapter;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$2;-><init>(Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->context:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0108

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper;->subtitleDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    return-void
.end method
