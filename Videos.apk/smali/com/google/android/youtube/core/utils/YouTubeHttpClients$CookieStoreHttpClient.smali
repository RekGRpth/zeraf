.class final Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "YouTubeHttpClients.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/utils/YouTubeHttpClients;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CookieStoreHttpClient"
.end annotation


# instance fields
.field private final cookieStore:Lorg/apache/http/client/CookieStore;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/CookieStore;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .param p1    # Lorg/apache/http/client/CookieStore;
    .param p2    # Lorg/apache/http/conn/ClientConnectionManager;
    .param p3    # Lorg/apache/http/params/HttpParams;

    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;->cookieStore:Lorg/apache/http/client/CookieStore;

    return-void
.end method


# virtual methods
.method protected createCookieStore()Lorg/apache/http/client/CookieStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;->cookieStore:Lorg/apache/http/client/CookieStore;

    return-object v0
.end method
