.class Lcom/google/android/youtube/core/utils/WorkScheduler$1;
.super Landroid/os/Handler;
.source "WorkScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/utils/WorkScheduler;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/utils/WorkScheduler;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/utils/WorkScheduler;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/google/android/youtube/core/utils/WorkScheduler$1;->this$0:Lcom/google/android/youtube/core/utils/WorkScheduler;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    if-nez v4, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/utils/WorkScheduler$Client;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    int-to-long v6, v2

    sub-long/2addr v4, v6

    long-to-int v1, v4

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/utils/WorkScheduler$Client;->doWork(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/utils/WorkScheduler$1;->this$0:Lcom/google/android/youtube/core/utils/WorkScheduler;

    # invokes: Lcom/google/android/youtube/core/utils/WorkScheduler;->scheduleNext(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V
    invoke-static {v4, v0, v2, v3}, Lcom/google/android/youtube/core/utils/WorkScheduler;->access$000(Lcom/google/android/youtube/core/utils/WorkScheduler;Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V

    :cond_0
    return-void
.end method
