.class public Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
.super Lcom/google/android/youtube/core/adapter/Outline;
.source "ListAdapterOutline.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/ListAdapter;",
        ">",
        "Lcom/google/android/youtube/core/adapter/Outline;"
    }
.end annotation


# instance fields
.field private final adapter:Landroid/widget/ListAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final observer:Landroid/database/DataSetObserver;

.field private final viewTypes:[Lcom/google/android/youtube/core/adapter/Outline$ViewType;


# direct methods
.method public varargs constructor <init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V
    .locals 6
    .param p2    # Z
    .param p3    # [Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z[",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/Outline;-><init>()V

    const-string v1, "adapter cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListAdapter;

    iput-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    if-eqz p3, :cond_0

    array-length v1, p3

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    new-array p3, v1, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_2

    new-instance v1, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Auto "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    aput-object v1, p3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v1, p3

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v4

    if-ne v1, v4, :cond_3

    move v1, v2

    :goto_1
    const-string v4, "viewTypes array size must match adapter\'s view type count"

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_2
    const-string v1, "viewTypes must not contain null"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    :cond_2
    iput-object p3, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->viewTypes:[Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    if-eqz p2, :cond_5

    new-instance v1, Lcom/google/android/youtube/core/adapter/ListAdapterOutline$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline$1;-><init>(Lcom/google/android/youtube/core/adapter/ListAdapterOutline;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->observer:Landroid/database/DataSetObserver;

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->observer:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :goto_3
    return-void

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    iput-object v5, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->observer:Landroid/database/DataSetObserver;

    goto :goto_3
.end method


# virtual methods
.method public final getAdapter()Landroid/widget/ListAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method protected getAllViewTypes(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->viewTypes:[Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->viewTypes:[Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aget-object v1, v1, v0

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-gez v0, :cond_0

    sget-object v1, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->IGNORE_VIEW_TYPE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->viewTypes:[Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aget-object v1, v1, v0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->adapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method protected onDataSetChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->notifyOutlineChanged()V

    return-void
.end method

.method protected onDataSetInvalidated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->notifyOutlineChanged()V

    return-void
.end method
