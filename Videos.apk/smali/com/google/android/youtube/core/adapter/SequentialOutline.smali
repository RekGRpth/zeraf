.class public Lcom/google/android/youtube/core/adapter/SequentialOutline;
.super Lcom/google/android/youtube/core/adapter/Outline;
.source "SequentialOutline.java"

# interfaces
.implements Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;


# instance fields
.field private final components:[Lcom/google/android/youtube/core/adapter/Outline;

.field private final endPositions:[I

.field private final hasStableIds:Z


# direct methods
.method public varargs constructor <init>([Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 8
    .param p1    # [Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/Outline;-><init>()V

    const-string v5, "components cannot be null"

    invoke-static {p1, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/android/youtube/core/adapter/Outline;

    iput-object v5, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    array-length v5, p1

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    invoke-direct {p0, v7, v6}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->computeEndPositionsFrom(IZ)V

    const/4 v0, 0x1

    move-object v1, p1

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v1, v3

    invoke-virtual {v2, p0}, Lcom/google/android/youtube/core/adapter/Outline;->setOnOutlineChangedListener(Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;)V

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/youtube/core/adapter/Outline;->hasStableIds()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v6

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->hasStableIds:Z

    return-void
.end method

.method private componentIndexOf(I)I
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_1
    xor-int/lit8 v1, v0, -0x1

    goto :goto_0
.end method

.method private computeEndPositionsFrom(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    move v1, p1

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v3, v1

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->getVisibleCount()I

    move-result v3

    add-int/2addr v2, v3

    if-nez p2, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    aget v3, v3, v1

    if-eq v2, v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    aput v2, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private startPositionOf(I)I
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->endPositions:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected getAllViewTypes(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getAllViewTypes(Ljava/util/Set;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1    # I

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->componentIndexOf(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/adapter/Outline;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1    # I

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->hasStableIds:Z

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->componentIndexOf(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/adapter/Outline;->getItemId(I)J

    move-result-wide v2

    :goto_1
    return-wide v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    int-to-long v2, p1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->componentIndexOf(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2, p2, p3}, Lcom/google/android/youtube/core/adapter/Outline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .locals 4
    .param p1    # I

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->componentIndexOf(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/adapter/Outline;->getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->hasStableIds:Z

    return v0
.end method

.method public isEnabled(I)Z
    .locals 4
    .param p1    # I

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "position out of bounds"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->componentIndexOf(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v0, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->startPositionOf(I)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/adapter/Outline;->isEnabled(I)Z

    move-result v2

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onOutlineChanged(Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/SequentialOutline;->components:[Lcom/google/android/youtube/core/adapter/Outline;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->computeEndPositionsFrom(IZ)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/SequentialOutline;->notifyOutlineChanged()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
