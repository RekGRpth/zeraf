.class public Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;
.super Lcom/google/android/youtube/core/client/BaseClient;
.source "DefaultDeviceRegistrationClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/client/DeviceRegistrationClient;


# instance fields
.field private final developerKey:Ljava/lang/String;

.field private final deviceRegistrationRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/DeviceAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final serial:Ljava/lang/String;

.field private final uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/UriRewriter;[B[BLjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Lcom/google/android/youtube/core/utils/UriRewriter;
    .param p4    # [B
    .param p5    # [B
    .param p6    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/BaseClient;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p4, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v2, "developerKey cannot be null or empty"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->developerKey:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "serial cannot be null or empty"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->serial:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    invoke-direct {p0, p5}, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->createDeviceRegistrationRequester([B)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->deviceRegistrationRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private createDeviceRegistrationRequester([B)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/DeviceAuth;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v1, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public registerDevice(Lcom/google/android/youtube/core/async/Callback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/DeviceAuth;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    if-nez v2, :cond_0

    const-string v0, "https://www.google.com/youtube/accounts/registerDevice"

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?developer=%s&serialNumber=%s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->developerKey:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->serial:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->deviceRegistrationRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v2, v1, p1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v3, "https://www.google.com/youtube/accounts/registerDevice"

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
