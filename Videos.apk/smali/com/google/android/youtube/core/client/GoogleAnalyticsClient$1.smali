.class Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;
.super Ljava/lang/Object;
.source "GoogleAnalyticsClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->trackEvent(Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$label:Ljava/lang/String;

.field final synthetic val$value:I


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->this$0:Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$action:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$label:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$value:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->this$0:Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    # getter for: Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;
    invoke-static {v0}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->access$100(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->this$0:Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    # getter for: Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->access$000(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$action:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$label:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;->val$value:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
