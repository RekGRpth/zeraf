.class public final Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;
.super Ljava/lang/Object;
.source "DefaultVideoStats2Client.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Lcom/google/android/youtube/core/client/VideoStats2Client;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/youtube/core/client/VideoStats2Client;"
    }
.end annotation


# static fields
.field private static final VSS2_BASE_URI:Landroid/net/Uri;


# instance fields
.field private final account:Ljava/lang/String;

.field private final adformat:Ljava/lang/String;

.field private final autoplay:Z

.field private currentPlaybackPosition:J

.field private final cvssClient:Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;

.field private final delay:I

.field private delayedPingSent:Z

.field private finalPingSent:Z

.field private initialPingSent:Z

.field private isPlaying:Z

.field private isRecordingPlaybackSegment:Z

.field private final length:Ljava/lang/String;

.field private final pingRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final playbackSegmentEndTimes:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final playbackSegmentStartTimes:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final scriptedPlayback:Z

.field private final sessionStartTimestamp:J

.field private final statParams:Lcom/google/android/youtube/core/client/StatParams;

.field private volatile throttled:Z

.field private final videoId:Ljava/lang/String;

.field private final vssPlaybackId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://s.youtube.com/api/stats"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->VSS2_BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/core/client/StatParams;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # Z
    .param p9    # Z
    .param p10    # Ljava/lang/String;
    .param p11    # Ljava/lang/String;
    .param p12    # Ljava/lang/String;
    .param p13    # J
    .param p15    # Lcom/google/android/youtube/core/client/StatParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/google/android/youtube/core/client/StatParams;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->createAuthDecoratingRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->pingRequester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p4, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->account:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->videoId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->length:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delay:I

    iput-boolean p8, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->autoplay:Z

    iput-boolean p9, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->scriptedPlayback:Z

    iput-object p10, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->adformat:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->vssPlaybackId:Ljava/lang/String;

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sessionStartTimestamp:J

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->statParams:Lcom/google/android/youtube/core/client/StatParams;

    new-instance v2, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;

    iget-object v3, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->pingRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p12

    invoke-direct {v2, v3, p5, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;-><init>(Lcom/google/android/youtube/core/async/Requester;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->cvssClient:Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentStartTimes:Ljava/util/LinkedList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentEndTimes:Ljava/util/LinkedList;

    return-void
.end method

.method static synthetic access$200(J)Ljava/lang/String;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addCommonVssParameters(Landroid/net/Uri$Builder;)V
    .locals 3
    .param p1    # Landroid/net/Uri$Builder;

    const-string v0, "cpn"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->vssPlaybackId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "docid"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ver"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "len"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->length:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->statParams:Lcom/google/android/youtube/core/client/StatParams;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/client/StatParams;->appendStatsParams(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->adformat:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "el"

    const-string v1, "adunit"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "adformat"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->adformat:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_0
    return-void

    :cond_0
    const-string v0, "el"

    const-string v1, "detailpage"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method private addPlaybackParameters(Landroid/net/Uri$Builder;)V
    .locals 8
    .param p1    # Landroid/net/Uri$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sessionStartTimestamp:J

    sub-long v0, v4, v6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    invoke-static {v4, v5}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v2

    const-string v4, "rt"

    invoke-virtual {p1, v4, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "cmt"

    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "delay"

    iget v6, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delay:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-boolean v4, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->autoplay:Z

    if-eqz v4, :cond_0

    const-string v4, "autoplay"

    const-string v5, "1"

    invoke-virtual {p1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-boolean v4, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->scriptedPlayback:Z

    if-eqz v4, :cond_1

    const-string v4, "splay"

    const-string v5, "1"

    invoke-virtual {p1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    return-void
.end method

.method private addWatchTimeParameters(Landroid/net/Uri$Builder;)V
    .locals 4
    .param p1    # Landroid/net/Uri$Builder;

    const-string v1, "state"

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isPlaying:Z

    if-eqz v0, :cond_0

    const-string v0, "playing"

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentEndTimes:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "st"

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "et"

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_1
    return-void

    :cond_0
    const-string v0, "paused"

    goto :goto_0

    :cond_1
    const-string v0, "st"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentStartTimes:Ljava/util/LinkedList;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->flushListToString(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "et"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentEndTimes:Ljava/util/LinkedList;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->flushListToString(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1
.end method

.method private static createAuthDecoratingRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p0    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;

    sget-object v1, Lcom/google/android/youtube/core/async/NetworkRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v1

    return-object v1
.end method

.method private finishPlaybackSegment()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isRecordingPlaybackSegment:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isRecordingPlaybackSegment:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentEndTimes:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private flushListToString(Ljava/util/LinkedList;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static millisToSecondsString(J)Ljava/lang/String;
    .locals 8
    .param p0    # J

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private sendDelayedPing()V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delayedPingSent:Z

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->VSS2_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "delayplay"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendPlaybackPing(Landroid/net/Uri$Builder;)V

    return-void
.end method

.method private sendInitialPing()V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->initialPingSent:Z

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->VSS2_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "playback"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendPlaybackPing(Landroid/net/Uri$Builder;)V

    return-void
.end method

.method private sendPing(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finalPingSent:Z

    if-eqz v0, :cond_1

    const-string v0, "Final ping for this playback has already been sent - Ignoring request"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->throttled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Pinging "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->pingRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/NetworkRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/NetworkRequest;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method private sendPlaybackPing(Landroid/net/Uri$Builder;)V
    .locals 1
    .param p1    # Landroid/net/Uri$Builder;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->addCommonVssParameters(Landroid/net/Uri$Builder;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->addPlaybackParameters(Landroid/net/Uri$Builder;)V

    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendPing(Landroid/net/Uri;)V

    return-void
.end method

.method private sendWatchTimePing(Z)V
    .locals 3
    .param p1    # Z

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->VSS2_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "watchtime"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->addCommonVssParameters(Landroid/net/Uri$Builder;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->addWatchTimeParameters(Landroid/net/Uri$Builder;)V

    if-eqz p1, :cond_0

    const-string v1, "final"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendPing(Landroid/net/Uri;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finalPingSent:Z

    or-int/2addr v1, p1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finalPingSent:Z

    return-void
.end method

.method private startPlaybackSegment()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isRecordingPlaybackSegment:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isRecordingPlaybackSegment:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->playbackSegmentStartTimes:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Ljava/lang/Exception;

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->throttled:Z

    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onPlaybackDestroyed()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finishPlaybackSegment()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->initialPingSent:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendWatchTimePing(Z)V

    :cond_0
    return-void
.end method

.method public onPlaybackEnded()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isPlaying:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delayedPingSent:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delay:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendDelayedPing()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finishPlaybackSegment()V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendWatchTimePing(Z)V

    return-void
.end method

.method public onPlaybackPaused()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isPlaying:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finishPlaybackSegment()V

    return-void
.end method

.method public onPlaybackPlay()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isPlaying:Z

    return-void
.end method

.method public onPlaybackProgress(J)V
    .locals 2
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->initialPingSent:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendInitialPing()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delayedPingSent:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delay:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->delay:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendDelayedPing()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isPlaying:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->isRecordingPlaybackSegment:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->startPlaybackSegment()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->cvssClient:Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->maybeSendCvssPing(J)V

    return-void
.end method

.method public onPlaybackSeek(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finishPlaybackSegment()V

    iput-wide p1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->currentPlaybackPosition:J

    return-void
.end method

.method public onPlaybackSuspended()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->finishPlaybackSegment()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->initialPingSent:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->sendWatchTimePing(Z)V

    :cond_0
    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Void;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Ljava/lang/Void;

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Void;)V

    return-void
.end method
