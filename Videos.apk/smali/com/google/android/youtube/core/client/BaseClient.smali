.class public abstract Lcom/google/android/youtube/core/client/BaseClient;
.super Ljava/lang/Object;
.source "BaseClient.java"


# instance fields
.field protected final cachePath:Ljava/lang/String;

.field protected final executor:Ljava/util/concurrent/Executor;

.field protected final httpClient:Lorg/apache/http/client/HttpClient;

.field protected final uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

.field protected final xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lorg/apache/http/client/HttpClient;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->httpClient:Lorg/apache/http/client/HttpClient;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/BaseClient;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/BaseClient;->cachePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->httpClient:Lorg/apache/http/client/HttpClient;

    const-string v0, "xmlParser cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/XmlParser;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->cachePath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/AsyncRequester",
            "<TR;TE;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/client/BaseClient;->executor:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method protected newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/youtube/core/async/HttpRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/HttpRequester;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/BaseClient;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    return-object v0
.end method
