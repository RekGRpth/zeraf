.class public interface abstract Lcom/google/android/youtube/core/client/VideoStats2Client;
.super Ljava/lang/Object;
.source "VideoStats2Client.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;
    }
.end annotation


# virtual methods
.method public abstract onPlaybackDestroyed()V
.end method

.method public abstract onPlaybackEnded()V
.end method

.method public abstract onPlaybackPaused()V
.end method

.method public abstract onPlaybackPlay()V
.end method

.method public abstract onPlaybackProgress(J)V
.end method

.method public abstract onPlaybackSeek(J)V
.end method

.method public abstract onPlaybackSuspended()V
.end method
