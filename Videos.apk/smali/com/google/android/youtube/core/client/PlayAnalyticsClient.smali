.class public Lcom/google/android/youtube/core/client/PlayAnalyticsClient;
.super Ljava/lang/Object;
.source "PlayAnalyticsClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/client/AnalyticsClient;
.implements Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final constantExtras:[Ljava/lang/Object;

.field private final context:Landroid/content/Context;

.field private final eventLogger:Lcom/google/android/play/analytics/EventLogger;

.field private final inSample:Z

.field private final signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/accounts/SignInManager;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p7    # Lcom/google/android/youtube/videos/accounts/SignInManager;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->context:Landroid/content/Context;

    iput-object p6, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object p7, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    invoke-static {p1, p4}, Lcom/google/android/youtube/core/utils/AnalyticsHelper;->inSample(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->inSample:Z

    new-instance v0, Lcom/google/android/play/analytics/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/AnalyticsHelper;->getLoggingId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/play/analytics/EventLogger$LogSource;->VIDEO:Lcom/google/android/play/analytics/EventLogger$LogSource;

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->eventLogger:Lcom/google/android/play/analytics/EventLogger;

    invoke-virtual {p7, p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->setListener(Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->updateUploadAccount()V

    new-instance v6, Ljava/util/ArrayList;

    const/16 v0, 0x8

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-string v0, "ver"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "appSuffix"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x1

    if-le p4, v0, :cond_1

    const-string v0, "sampleRatio"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "experiment"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    return-void
.end method

.method private declared-synchronized updateUploadAccount()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->eventLogger:Lcom/google/android/play/analytics/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/EventLogger;->setUploadAccount(Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onSignedInAccountChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->updateUploadAccount()V

    return-void
.end method

.method public varargs trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->inSample:Z

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    array-length v1, p2

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->eventLogger:Lcom/google/android/play/analytics/EventLogger;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    array-length v1, p2

    iget-object v2, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    array-length v2, v2

    add-int/2addr v1, v2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->constantExtras:[Ljava/lang/Object;

    array-length v1, v1

    array-length v2, p2

    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;->eventLogger:Lcom/google/android/play/analytics/EventLogger;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
