.class final Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;
.super Ljava/lang/Object;
.source "DefaultVideoStats2Client.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CvssClient"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final CVSS_BASE_URI:Landroid/net/Uri;


# instance fields
.field private cvssPingCounter:I

.field private final cvssPlaybackId:Ljava/lang/String;

.field private final requester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://s2.youtube.com/s?ns=yt"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->CVSS_BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->requester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPlaybackId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->videoId:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    return-void
.end method

.method private sendPing(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Pinging "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->requester:Lcom/google/android/youtube/core/async/Requester;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/NetworkRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/NetworkRequest;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method


# virtual methods
.method public maybeSendCvssPing(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPlaybackId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const-wide/16 v0, 0x4e20

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    const-wide/16 v0, 0x7530

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->sendCvssPing(J)V

    :cond_2
    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Void;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .param p2    # Ljava/lang/Void;

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Void;)V

    return-void
.end method

.method public sendCvssPing(J)V
    .locals 4
    .param p1    # J

    # invokes: Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->millisToSecondsString(J)Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;->access$200(J)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->CVSS_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "yttk"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ps"

    const-string v3, "android"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ctp"

    iget v3, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPingCounter:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "docid"

    iget-object v3, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "plid"

    iget-object v3, p0, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->cvssPlaybackId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "st"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "et"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "el"

    const-string v3, "detailpage"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client$CvssClient;->sendPing(Landroid/net/Uri;)V

    return-void
.end method
