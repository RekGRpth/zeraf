.class Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;
.super Ljava/lang/Object;
.source "AuthDecoratingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->request(Lcom/google/android/youtube/core/async/NetworkRequest;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/client/AuthDecoratingRequester;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$originalRequest:Lcom/google/android/youtube/core/async/NetworkRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/client/AuthDecoratingRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/NetworkRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->this$0:Lcom/google/android/youtube/core/client/AuthDecoratingRequester;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$originalRequest:Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$originalRequest:Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->onError(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->val$originalRequest:Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;->onResponse(Lcom/google/android/youtube/core/async/NetworkRequest;Ljava/lang/Object;)V

    return-void
.end method
