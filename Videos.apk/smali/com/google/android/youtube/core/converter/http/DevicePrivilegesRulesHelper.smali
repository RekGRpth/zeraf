.class public Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper;
.super Ljava/lang/Object;
.source "DevicePrivilegesRulesHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addDevicePriviledgesListRules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/converter/Rules$Builder;

    const-string v0, "/feed"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$1;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$1;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    const-string v0, "/feed/entry"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$2;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$2;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    const-string v0, "/feed"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper;->addInternalRules(Lcom/google/android/youtube/core/converter/Rules$Builder;Ljava/lang/String;)V

    return-void
.end method

.method private static addInternalRules(Lcom/google/android/youtube/core/converter/Rules$Builder;Ljava/lang/String;)V
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/converter/Rules$Builder;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry/yt:device"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$4;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$4;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry/yt:privilege"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$5;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$5;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    return-void
.end method
