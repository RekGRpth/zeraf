.class public abstract Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "XmlResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final parser:Lcom/google/android/youtube/core/converter/XmlParser;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    const-string v0, "the parser can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/XmlParser;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;->parser:Lcom/google/android/youtube/core/converter/XmlParser;

    return-void
.end method


# virtual methods
.method protected convertResponseContent(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;->parser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;->getRules()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/google/android/youtube/core/converter/XmlParser;->parse(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/Rules;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ModelBuilder;

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/youtube/core/model/ModelBuilder;->build()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected abstract getRules()Lcom/google/android/youtube/core/converter/Rules;
.end method
