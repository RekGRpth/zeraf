.class public Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;
.super Ljava/lang/Object;
.source "NetworkRequestConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<TR;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public final contentType:Ljava/lang/String;

.field public final deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

.field public final method:Lcom/google/android/youtube/core/converter/http/HttpMethod;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/http/HttpMethod;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    const-string v0, "method can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->contentType:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/converter/http/HttpMethod;
    .param p2    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "deviceAuthorizer can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    const-string v0, "method can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->contentType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->convertRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 9
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v6}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->supportsPayload()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Content not allowed [method="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->createHttpRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v4

    const-string v6, "Accept-Encoding"

    const-string v7, "gzip"

    invoke-interface {v4, v6, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->headers:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v4, v6, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    if-eqz v6, :cond_2

    :try_start_0
    const-string v6, "X-GData-Device"

    iget-object v7, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    iget-object v8, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-interface {v7, v8}, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->getHeaderValue(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    iget-object v6, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-eqz v6, :cond_3

    sget-object v6, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter$1;->$SwitchMap$com$google$android$youtube$videos$accounts$UserAuth$AuthType:[I

    iget-object v7, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v7, v7, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v7, v7, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Unsupported authorization method"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    :catch_0
    move-exception v0

    new-instance v6, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v6, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    :pswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GoogleLogin auth=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v7, v7, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "Authorization"

    invoke-interface {v4, v6, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    iget-object v6, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    if-eqz v6, :cond_4

    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    iget-object v6, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->content:[B

    invoke-direct {v1, v6}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    iget-object v6, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->contentType:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    move-object v6, v4

    check-cast v6, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    invoke-virtual {v6, v1}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_4
    return-object v4

    :pswitch_1
    const-string v6, "Authorization"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bearer "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v8, v8, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected createHttpRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
