.class final Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper$2;
.super Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;
.source "DevicePrivilegesRulesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper;->addDevicePriviledgesListRules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-class v0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/Stack;->poll(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;->build()Lcom/google/android/youtube/core/model/DevicePrivileges;

    move-result-object v1

    const-class v0, Lcom/google/android/youtube/core/model/ListBuilder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ListBuilder;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ListBuilder;->add(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/ListBuilder;

    return-void
.end method

.method public start(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p2    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/DevicePrivileges$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/Stack;->offer(Ljava/lang/Object;)Z

    return-void
.end method
