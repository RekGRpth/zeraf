.class public Lcom/google/android/youtube/core/converter/http/SeasonResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;
.source "SeasonResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/GDataResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/Season;",
        ">;"
    }
.end annotation


# instance fields
.field private final rules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/Rules$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;-><init>()V

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/converter/http/SeasonRulesHelper;->addSeasonRules(Lcom/google/android/youtube/core/converter/Rules$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/SeasonResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method


# virtual methods
.method protected getRules()Lcom/google/android/youtube/core/converter/Rules;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/SeasonResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-object v0
.end method
