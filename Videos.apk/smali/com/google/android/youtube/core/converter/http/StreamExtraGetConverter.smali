.class public Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "StreamExtraGetConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/StreamExtra;",
        ">;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Landroid/net/Uri;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    return-void
.end method

.method private getContentLength(Lorg/apache/http/Header;)J
    .locals 7
    .param p1    # Lorg/apache/http/Header;

    const-wide/16 v3, 0x0

    if-nez p1, :cond_0

    const-string v5, "Missing content range header"

    invoke-static {v5}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :goto_0
    return-wide v3

    :cond_0
    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v2, v5, 0x1

    if-lez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_1

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "Error parsing content range header"

    invoke-static {v5, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid content range header "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getLastModified(Lorg/apache/http/Header;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/http/Header;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->convertRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1    # Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "widevine"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_0
    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    const-string v1, "Range"

    const-string v2, "bytes=0-1"

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/core/model/StreamExtra;
    .locals 3
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;-><init>()V

    const-string v1, "Content-Range"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->getContentLength(Lorg/apache/http/Header;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes(J)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v0

    const-string v1, "Last-Modified"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->getLastModified(Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified(Ljava/lang/String;)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->build()Lcom/google/android/youtube/core/model/StreamExtra;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/core/model/StreamExtra;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/core/model/StreamExtra;

    move-result-object v0

    return-object v0
.end method
