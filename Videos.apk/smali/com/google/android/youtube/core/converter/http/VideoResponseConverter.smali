.class public Lcom/google/android/youtube/core/converter/http/VideoResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;
.source "VideoResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/GDataResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/Video;",
        ">;"
    }
.end annotation


# instance fields
.field private final rules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;Z)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/Rules$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;-><init>()V

    const-string v1, ""

    invoke-static {v0, v1, p2}, Lcom/google/android/youtube/core/converter/http/VideoRulesHelper;->addVideoRules(Lcom/google/android/youtube/core/converter/Rules$Builder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/VideoResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method


# virtual methods
.method protected getRules()Lcom/google/android/youtube/core/converter/Rules;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/VideoResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-object v0
.end method
