.class public final Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;
.super Ljava/lang/Object;
.source "BytesToBitmapResponseConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final cropToWidescreen:Z

.field private final desiredWidth:I

.field private final preferredConfig:Landroid/graphics/Bitmap$Config;

.field private final purgeable:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(IZZLandroid/graphics/Bitmap$Config;)V

    return-void
.end method

.method public constructor <init>(IZZLandroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p1    # I
    .param p2    # Z
    .param p3    # Z
    .param p4    # Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "desiredWidth must be > 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->desiredWidth:I

    iput-boolean p2, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->cropToWidescreen:Z

    iput-boolean p3, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->purgeable:Z

    iput-object p4, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->preferredConfig:Landroid/graphics/Bitmap$Config;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->desiredWidth:I

    iput-boolean v0, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->cropToWidescreen:Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->purgeable:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->preferredConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method private static calculateScale(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x1

    :goto_0
    shr-int/lit8 p1, p1, 0x1

    if-lt p1, p0, :cond_0

    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private decode([B)Landroid/graphics/Bitmap;
    .locals 11
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v10, 0x0

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v8, p1

    invoke-static {p1, v10, v8, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v8, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gez v8, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v8, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->desiredWidth:I

    if-nez v8, :cond_2

    move v6, v7

    :goto_1
    iget-boolean v8, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->cropToWidescreen:Z

    if-nez v8, :cond_3

    iget-boolean v8, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->purgeable:Z

    if-nez v8, :cond_3

    if-ne v6, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->preferredConfig:Landroid/graphics/Bitmap$Config;

    if-nez v7, :cond_3

    array-length v7, p1

    invoke-static {p1, v10, v7, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget v8, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->desiredWidth:I

    iget v9, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v8, v9}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->calculateScale(II)I

    move-result v6

    goto :goto_1

    :cond_3
    iput v6, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iget-object v7, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->preferredConfig:Landroid/graphics/Bitmap$Config;

    iput-object v7, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iget-boolean v7, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->purgeable:Z

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    array-length v7, p1

    invoke-static {p1, v10, v7, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-boolean v7, p0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->cropToWidescreen:Z

    if-eqz v7, :cond_0

    if-eqz v0, :cond_0

    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v7, v7

    const/high16 v8, 0x3f100000

    mul-float v3, v7, v8

    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v7, v7

    sub-float/2addr v7, v3

    const/high16 v8, 0x40000000

    div-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v1, v7

    if-lez v1, :cond_0

    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/lit8 v8, v1, 0x2

    sub-int v5, v7, v8

    if-lez v5, :cond_0

    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v10, v1, v7, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v2

    goto :goto_0
.end method


# virtual methods
.method public convertResponse([B)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->decode([B)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v2, "failed to decode bitmap"

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->convertResponse([B)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
