.class public final Lcom/google/android/youtube/core/converter/XmlParser;
.super Ljava/lang/Object;
.source "XmlParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/converter/XmlParser$1;,
        Lcom/google/android/youtube/core/converter/XmlParser$EmptyAttributes;,
        Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;,
        Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;,
        Lcom/google/android/youtube/core/converter/XmlParser$Rule;
    }
.end annotation


# static fields
.field private static final EMPTY_ATTRS:Lorg/xml/sax/Attributes;

.field private static final PREFIXES_ONLY_FEATURES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final factory:Ljavax/xml/parsers/SAXParserFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v1, Lcom/google/android/youtube/core/converter/XmlParser$EmptyAttributes;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/XmlParser$EmptyAttributes;-><init>(Lcom/google/android/youtube/core/converter/XmlParser$1;)V

    sput-object v1, Lcom/google/android/youtube/core/converter/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "http://xml.org/sax/features/namespaces"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "http://xml.org/sax/features/namespace-prefixes"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/core/converter/XmlParser;->PREFIXES_ONLY_FEATURES:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v3, "features can\'t be null"

    invoke-static {p1, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/converter/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v3, v4}, Ljavax/xml/parsers/SAXParserFactory;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :catch_2
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "SAX initilization error"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    return-void
.end method

.method static synthetic access$100()Lorg/xml/sax/Attributes;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/converter/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;

    return-object v0
.end method

.method public static createPrefixesOnlyParser()Lcom/google/android/youtube/core/converter/XmlParser;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/converter/XmlParser;

    sget-object v1, Lcom/google/android/youtube/core/converter/XmlParser;->PREFIXES_ONLY_FEATURES:Ljava/util/Map;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/XmlParser;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/Rules;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lcom/google/android/youtube/core/converter/Rules;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const-string v3, "input can\'t be null"

    invoke-static {p1, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "rules can\'t be null"

    invoke-static {p2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/youtube/core/converter/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    monitor-enter v4
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v3, p0, Lcom/google/android/youtube/core/converter/XmlParser;->factory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v1, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;

    invoke-direct {v1, p2}, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;-><init>(Lcom/google/android/youtube/core/converter/Rules;)V

    invoke-interface {v2, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    new-instance v3, Lorg/xml/sax/InputSource;

    invoke-direct {v3, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v2, v3}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    iget-object v3, v1, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/android/youtube/core/converter/InvalidFormatException;

    const-string v4, "XML is well-formed but invalid"

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/converter/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    new-instance v3, Lcom/google/android/youtube/core/converter/ParserException;

    invoke-direct {v3, v0}, Lcom/google/android/youtube/core/converter/ParserException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    new-instance v3, Lcom/google/android/youtube/core/converter/ParserException;

    invoke-direct {v3, v0}, Lcom/google/android/youtube/core/converter/ParserException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    :try_start_5
    iget-object v3, v1, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->result:Ljava/lang/Object;
    :try_end_5
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_5 .. :try_end_5} :catch_1

    return-object v3
.end method
