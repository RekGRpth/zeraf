.class public Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;
.super Lcom/google/android/youtube/core/cache/PersistentCache;
.source "TimestampedProtosPersistentCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E::",
        "Lcom/google/protobuf/MessageLite;",
        ">",
        "Lcom/google/android/youtube/core/cache/PersistentCache",
        "<TK;",
        "Lcom/google/android/youtube/core/async/Timestamped",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field private final parser:Lcom/google/android/youtube/videos/ProtoParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ProtoParser",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/ProtoParser;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/ProtoParser",
            "<TE;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/cache/PersistentCache;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "parser cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ProtoParser;

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->parser:Lcom/google/android/youtube/videos/ProtoParser;

    return-void
.end method


# virtual methods
.method protected readElement(Ljava/io/BufferedInputStream;)Lcom/google/android/youtube/core/async/Timestamped;
    .locals 5
    .param p1    # Ljava/io/BufferedInputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedInputStream;",
            ")",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->parser:Lcom/google/android/youtube/videos/ProtoParser;

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/videos/ProtoParser;->parseFrom(Ljava/io/InputStream;)Lcom/google/protobuf/MessageLite;

    move-result-object v1

    new-instance v4, Lcom/google/android/youtube/core/async/Timestamped;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    return-object v4
.end method

.method protected bridge synthetic readElement(Ljava/io/BufferedInputStream;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/BufferedInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->readElement(Ljava/io/BufferedInputStream;)Lcom/google/android/youtube/core/async/Timestamped;

    move-result-object v0

    return-object v0
.end method

.method protected writeElement(Lcom/google/android/youtube/core/async/Timestamped;Ljava/io/BufferedOutputStream;)V
    .locals 3
    .param p2    # Ljava/io/BufferedOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;",
            "Ljava/io/BufferedOutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-wide v1, p1, Lcom/google/android/youtube/core/async/Timestamped;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v1, p1, Lcom/google/android/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-interface {v1, v0}, Lcom/google/protobuf/MessageLite;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v1
.end method

.method protected bridge synthetic writeElement(Ljava/lang/Object;Ljava/io/BufferedOutputStream;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/BufferedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/core/async/Timestamped;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->writeElement(Lcom/google/android/youtube/core/async/Timestamped;Ljava/io/BufferedOutputStream;)V

    return-void
.end method
