.class public final Lcom/google/android/youtube/core/cache/AsyncCache;
.super Ljava/lang/Object;
.source "AsyncCache.java"

# interfaces
.implements Lcom/google/android/youtube/core/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/cache/Cache",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final pending:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;TE;>;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TK;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/cache/Cache;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TK;TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->executor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->target:Lcom/google/android/youtube/core/cache/Cache;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/cache/AsyncCache;)Lcom/google/android/youtube/core/cache/Cache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/cache/AsyncCache;

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->target:Lcom/google/android/youtube/core/cache/Cache;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/cache/AsyncCache;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/cache/AsyncCache;

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->target:Lcom/google/android/youtube/core/cache/Cache;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/core/cache/Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/core/cache/AsyncCache$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/core/cache/AsyncCache$1;-><init>(Lcom/google/android/youtube/core/cache/AsyncCache;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
