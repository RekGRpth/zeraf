.class Lcom/google/android/youtube/core/cache/AsyncCache$1;
.super Ljava/lang/Object;
.source "AsyncCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/cache/AsyncCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/cache/AsyncCache;

.field final synthetic val$element:Ljava/lang/Object;

.field final synthetic val$key:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/cache/AsyncCache;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->this$0:Lcom/google/android/youtube/core/cache/AsyncCache;

    iput-object p2, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->val$key:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->val$element:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->this$0:Lcom/google/android/youtube/core/cache/AsyncCache;

    # getter for: Lcom/google/android/youtube/core/cache/AsyncCache;->target:Lcom/google/android/youtube/core/cache/Cache;
    invoke-static {v0}, Lcom/google/android/youtube/core/cache/AsyncCache;->access$000(Lcom/google/android/youtube/core/cache/AsyncCache;)Lcom/google/android/youtube/core/cache/Cache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->val$key:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->val$element:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->this$0:Lcom/google/android/youtube/core/cache/AsyncCache;

    # getter for: Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/youtube/core/cache/AsyncCache;->access$100(Lcom/google/android/youtube/core/cache/AsyncCache;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->this$0:Lcom/google/android/youtube/core/cache/AsyncCache;

    # getter for: Lcom/google/android/youtube/core/cache/AsyncCache;->pending:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/youtube/core/cache/AsyncCache;->access$100(Lcom/google/android/youtube/core/cache/AsyncCache;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/cache/AsyncCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
