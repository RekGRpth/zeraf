.class Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;
.super Ljava/lang/Object;
.source "VideosApplication.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/VideosApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchCollapseListenerV14"
.end annotation


# instance fields
.field private final searchItem:Landroid/view/MenuItem;


# direct methods
.method private constructor <init>(Landroid/view/MenuItem;)V
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "searchItem cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;->searchItem:Landroid/view/MenuItem;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/MenuItem;Lcom/google/android/youtube/videos/VideosApplication$1;)V
    .locals 0
    .param p1    # Landroid/view/MenuItem;
    .param p2    # Lcom/google/android/youtube/videos/VideosApplication$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;-><init>(Landroid/view/MenuItem;)V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;->searchItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionClick(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;->searchItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
