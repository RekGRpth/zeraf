.class Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;
.super Ljava/lang/Object;
.source "DrmManagerV8.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getLicense(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    invoke-static {v2, v0, v3, v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v2, v2, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-direct {v4, v5, v6}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v2, v2, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v3, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v2, v3, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method
