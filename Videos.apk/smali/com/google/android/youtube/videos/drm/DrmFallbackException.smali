.class public Lcom/google/android/youtube/videos/drm/DrmFallbackException;
.super Lcom/google/android/youtube/videos/drm/DrmException;
.source "DrmFallbackException.java"


# instance fields
.field public final fallbackDrmLevel:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;II)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmException$DrmError;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "fallbackLevel must be positive"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput p3, p0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
