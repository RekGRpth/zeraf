.class public Lcom/google/android/youtube/videos/drm/DrmRequestSigner;
.super Ljava/lang/Object;
.source "DrmRequestSigner.java"


# instance fields
.field private final deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    return-void
.end method


# virtual methods
.method public sign(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "/feeds/api/videos/"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;->deviceAuthorizer:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    invoke-interface {v3, v2}, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->getHeaderValue(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "utf-8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const/16 v4, 0xb

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
