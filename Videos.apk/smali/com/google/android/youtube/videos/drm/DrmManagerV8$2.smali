.class Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;
.super Ljava/lang/Object;
.source "DrmManagerV8.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$credentials:Ljava/util/HashMap;

.field final synthetic val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/util/HashMap;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$credentials:Ljava/util/HashMap;

    iput-object p3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$200(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Ljava/lang/Object;

    move-result-object v13

    monitor-enter v13

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Lcom/widevine/drmapi/android/WVPlayback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$credentials:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/widevine/drmapi/android/WVPlayback;->setCredentials(Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v12

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v12, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine setCredentials failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v3, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I
    invoke-static {v4, v12}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    monitor-exit v13

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v1, v1, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Lcom/widevine/drmapi/android/WVPlayback;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/widevine/drmapi/android/WVPlayback;->registerAsset(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v12

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v12, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine registerAsset failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v3, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I
    invoke-static {v4, v12}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    monitor-exit v13

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;
    invoke-static {}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$500()[Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v1

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    invoke-static {v0, v7, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$600(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v9, v0, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    if-eqz v9, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Lcom/widevine/drmapi/android/WVPlayback;

    move-result-object v0

    iget-wide v1, v9, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->systemId:J

    iget-wide v3, v9, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->assetId:J

    iget-wide v5, v9, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-virtual/range {v0 .. v6}, Lcom/widevine/drmapi/android/WVPlayback;->requestLicense(JJJ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v12

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v12, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine requestLicense failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v3, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I
    invoke-static {v4, v12}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    monitor-exit v13

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Lcom/widevine/drmapi/android/WVPlayback;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/widevine/drmapi/android/WVPlayback;->requestLicense(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v12

    goto :goto_1

    :cond_3
    if-eqz v9, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;
    invoke-static {}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$700()[Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v1

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForIdentifiers(Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    invoke-static {v0, v9, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$800(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v11

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v1, v1, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getSuccess(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z
    invoke-static {v2, v11}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z

    move-result v2

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getLicense(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    invoke-static {v0, v7, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v10

    :goto_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getError(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/youtube/videos/drm/DrmException;
    invoke-static {v0, v11}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$1100(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/youtube/videos/drm/DrmException;

    move-result-object v8

    iget-object v0, v8, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v0, v1, v8}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_4
    monitor-exit v13

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;
    invoke-static {}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$700()[Lcom/widevine/drmapi/android/WVEvent;

    move-result-object v1

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    invoke-static {v0, v7, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$600(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v11

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v1, v11, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getSuccess(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z
    invoke-static {v2, v11}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z

    move-result v2

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$1000(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v10

    goto :goto_3

    :cond_6
    if-eqz v10, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v0, :cond_8

    iget-boolean v0, v10, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v0, v1, v8}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v0, v1, v10}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v0, v1, :cond_b

    iget-object v0, v8, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-eq v0, v1, :cond_a

    iget-object v0, v8, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v0, v1, :cond_b

    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v0, v1, v8}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method
