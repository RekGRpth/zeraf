.class public Lcom/google/android/youtube/videos/drm/DrmRequest;
.super Ljava/lang/Object;
.source "DrmRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;
    }
.end annotation


# instance fields
.field public final appLevelDrm:Z

.field public final ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

.field public final playbackId:Ljava/lang/String;

.field public final robotToken:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field public final stream:Lcom/google/android/youtube/core/model/Stream;

.field public final type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

.field public final userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p6    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/Stream;",
            "Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "stream cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    const-string v0, "videoId cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne p2, v0, :cond_0

    :goto_0
    iput-object p5, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_1

    const-string v0, "ids cannot be null for offline stream"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    :goto_1
    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iput-object p4, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    iput-boolean p8, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->appLevelDrm:Z

    return-void

    :cond_0
    const-string v0, "userAuth cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-object p5, v0

    goto :goto_0

    :cond_1
    iput-object p6, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    goto :goto_1
.end method

.method public static createOfflineRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Lcom/google/android/youtube/core/model/Stream;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p4    # Z

    const/4 v4, 0x0

    const-string v0, "userAuth must not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    const-string v1, "stream must be offline"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, v4

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createOfflineRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Lcom/google/android/youtube/core/model/Stream;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p3    # Z

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    const-string v1, "stream must be offline"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, p2

    move-object v7, v4

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createOfflineSyncRequest(Ljava/io/File;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Ljava/io/File;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p4    # Z

    const/4 v4, 0x0

    new-instance v1, Lcom/google/android/youtube/core/model/Stream;

    invoke-static {p0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/model/Stream;-><init>(Landroid/net/Uri;)V

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, v4

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createPinRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Lcom/google/android/youtube/core/model/Stream;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Z

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, v4

    move-object v7, v4

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createRobotStreamingRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Lcom/google/android/youtube/core/model/Stream;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/Stream;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;Z)",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->STREAMING:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createStreamingRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Lcom/google/android/youtube/core/model/Stream;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p4    # Z

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->STREAMING:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v7, v6

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public static createUnpinRequest(Ljava/io/File;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p0    # Ljava/io/File;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p4    # Z

    const/4 v4, 0x0

    new-instance v1, Lcom/google/android/youtube/core/model/Stream;

    invoke-static {p0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/model/Stream;-><init>(Landroid/net/Uri;)V

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, v4

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method


# virtual methods
.method public copyWithAuth(Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 9
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-object v7, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->appLevelDrm:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmRequest;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/drm/DrmRequest;->appLevelDrm:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
