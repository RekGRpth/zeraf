.class Lcom/google/android/youtube/videos/drm/DrmManagerV12;
.super Lcom/google/android/youtube/videos/drm/DrmManager;
.source "DrmManagerV12.java"

# interfaces
.implements Landroid/drm/DrmManagerClient$OnErrorListener;
.implements Landroid/drm/DrmManagerClient$OnEventListener;
.implements Landroid/drm/DrmManagerClient$OnInfoListener;
.implements Lcom/google/android/youtube/videos/drm/DrmManager$Listener;


# instance fields
.field private final appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final deviceId:Ljava/lang/String;

.field private final drmManager:Landroid/drm/DrmManagerClient;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final executor:Ljava/util/concurrent/Executor;

.field private volatile frameworkDrmLevel:I

.field private final gservicesId:J

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private final requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p4    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p5    # Lcom/google/android/youtube/videos/Config;
    .param p6    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0, p4}, Lcom/google/android/youtube/videos/drm/DrmManager;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)V

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->executor:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;

    iput-object p6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object p7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v0, "android_id"

    invoke-static {v7, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->deviceId:Ljava/lang/String;

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->gservicesId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->gservicesId:J

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->setListener(Lcom/google/android/youtube/videos/drm/DrmManager$Listener;)V

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;-><init>(Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnInfoListener(Landroid/drm/DrmManagerClient$OnInfoListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p0}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->rootedFrameworkLevelDrmEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->initFrameworkDrmLevel(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getLicense(Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Landroid/drm/DrmManagerClient;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;)Landroid/content/ContentValues;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->shouldRefreshLicense(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/drm/DrmInfo;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # Landroid/drm/DrmInfo;
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/core/utils/NetworkStatus;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;)Lcom/google/android/youtube/videos/drm/DrmException;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getError(Landroid/net/Uri;)Lcom/google/android/youtube/videos/drm/DrmException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/videos/Config;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/youtube/videos/drm/DrmManagerV12;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    return-object v0
.end method

.method private eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/drm/DrmEvent;

    invoke-virtual {p1}, Landroid/drm/DrmEvent;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "ALL_RIGHTS_REMOVED"

    goto :goto_0

    :sswitch_1
    const-string v0, "DRM_INFO_PROCESSED"

    goto :goto_0

    :sswitch_2
    const-string v0, "ACCOUNT_ALREADY_REGISTERED"

    goto :goto_0

    :sswitch_3
    const-string v0, "ALREADY_REGISTERED_BY_ANOTHER_ACCOUNT"

    goto :goto_0

    :sswitch_4
    const-string v0, "REMOVE_RIGHTS"

    goto :goto_0

    :sswitch_5
    const-string v0, "RIGHTS_INSTALLED"

    goto :goto_0

    :sswitch_6
    const-string v0, "WAIT_FOR_RIGHTS"

    goto :goto_0

    :sswitch_7
    const-string v0, "NO_INTERNET_CONNECTION"

    goto :goto_0

    :sswitch_8
    const-string v0, "NOT_SUPPORTED"

    goto :goto_0

    :sswitch_9
    const-string v0, "OUT_OF_MEMORY"

    goto :goto_0

    :sswitch_a
    const-string v0, "PROCESS_DRM_INFO_FAILED"

    goto :goto_0

    :sswitch_b
    const-string v0, "REMOVE_ALL_RIGHTS_FAILED"

    goto :goto_0

    :sswitch_c
    const-string v0, "RIGHTS_NOT_INSTALLED"

    goto :goto_0

    :sswitch_d
    const-string v0, "RIGHTS_RENEWAL_NOT_ALLOWED"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_5
        0x4 -> :sswitch_6
        0x5 -> :sswitch_2
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x7d1 -> :sswitch_c
        0x7d2 -> :sswitch_d
        0x7d3 -> :sswitch_8
        0x7d4 -> :sswitch_9
        0x7d5 -> :sswitch_7
        0x7d6 -> :sswitch_a
        0x7d7 -> :sswitch_b
    .end sparse-switch
.end method

.method private getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;
    .locals 3
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private getError(Landroid/net/Uri;)Lcom/google/android/youtube/videos/drm/DrmException;
    .locals 6
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    :goto_0
    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    invoke-direct {v4, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    return-object v4

    :cond_0
    const-string v4, "WVLastErrorKey"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WVLastErrorKey is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    const-string v4, "ok"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getRemainingTime(Landroid/content/ContentValues;)I

    move-result v4

    if-nez v4, :cond_1

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->LICENSE_EXPIRED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->parseErrorCode(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    move-result-object v1

    goto :goto_0
.end method

.method private getLicense(Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p3    # Z

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v9

    if-eqz v9, :cond_0

    const-string v2, "WVLicenseTypeKey"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No license for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    const-string v2, "WVLicenseTypeKey"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    and-int/lit8 v2, v10, 0x1

    if-eqz v2, :cond_2

    move v3, v4

    :goto_1
    and-int/lit8 v2, v10, 0x2

    if-eqz v2, :cond_3

    :goto_2
    if-nez v10, :cond_4

    const-string v1, "License mode is 0"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v4, v1

    goto :goto_2

    :cond_4
    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getSecondsSinceActivation(Landroid/content/ContentValues;)I

    move-result v6

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getRemainingTime(Landroid/content/ContentValues;)I

    move-result v5

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/drm/DrmResponse;-><init>(JZZIILcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)V

    goto :goto_0
.end method

.method private getRemainingTime(Landroid/content/ContentValues;)I
    .locals 2
    .param p1    # Landroid/content/ContentValues;

    const-string v1, "license_expiry_time"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSecondsSinceActivation(Landroid/content/ContentValues;)I
    .locals 2
    .param p1    # Landroid/content/ContentValues;

    const-string v1, "license_start_time"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initFrameworkDrmLevel(Z)V
    .locals 8
    .param p1    # Z

    const/4 v7, 0x3

    new-instance v1, Landroid/drm/DrmInfoRequest;

    const/4 v5, 0x1

    const-string v6, "video/wvm"

    invoke-direct {v1, v5, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    const-string v5, "WVPortalKey"

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDrmFrameworkFailed()V

    const/4 v5, -0x2

    iput v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    :goto_0
    return-void

    :cond_0
    const-string v5, "WVDrmInfoRequestStatusKey"

    invoke-virtual {v0, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v5, v4}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDrmFrameworkInit(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iput v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0

    :cond_1
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-nez v5, :cond_2

    iput v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const-wide/16 v5, 0x2

    cmp-long v5, v2, v5

    if-nez v5, :cond_3

    const/4 v5, 0x2

    iput v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0

    :cond_3
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    goto :goto_0
.end method

.method private invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V
    .locals 10
    .param p1    # Landroid/drm/DrmInfo;
    .param p2    # Z
    .param p3    # Z

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    const-string v1, "WVKeyIDKey"

    invoke-virtual {p1, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v4, "WVAssetIDKey"

    invoke-virtual {p1, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v7, "WVSystemIDKey"

    invoke-virtual {p1, v7}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    const-string v1, "VideosRequest"

    invoke-virtual {p1, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/videos/drm/DrmRequest;

    const-string v1, "VideosCallback"

    invoke-virtual {p1, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/core/async/Callback;

    if-eqz v6, :cond_0

    if-nez v5, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request or callback missing from DrmEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, v6, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    iget-object v9, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;

    move-object v2, p0

    move v4, p2

    move v7, p3

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;ZLcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmRequest;ZLcom/google/android/youtube/videos/drm/DrmManager$Identifiers;)V

    invoke-interface {v9, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private logDrmInfoRequest(Landroid/drm/DrmInfoRequest;)V
    .locals 4
    .param p1    # Landroid/drm/DrmInfoRequest;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "infoType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->getInfoType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mimeType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->keyIterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v1}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private shouldRefreshLicense(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)Z
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManager;->getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->EMM_DECODE_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->INVALID_KEYBOX_SYSTEM_ID:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->KEY_VERIFICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x2c -> :sswitch_1
        0x31 -> :sswitch_2
        0x198 -> :sswitch_3
    .end sparse-switch
.end method

.method public getDrmLevel()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    if-gez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getDrmLevel()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V
    .locals 3
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmErrorEvent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getUniqueId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmErrorEvent;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    const-string v1, "drm_info_object"

    invoke-virtual {p2, v1}, Landroid/drm/DrmErrorEvent;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/drm/DrmInfo;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DrmInfo missing from DrmEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V
    .locals 4
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmEvent;

    const/4 v3, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getUniqueId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/drm/DrmEvent;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    const-string v1, "drm_info_object"

    invoke-virtual {p2, v1}, Landroid/drm/DrmEvent;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/drm/DrmInfo;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, v3, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DrmInfo missing from DrmEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onHeartbeatError(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->notifyHeartbeatError(Ljava/lang/String;I)V

    return-void
.end method

.method public onInfo(Landroid/drm/DrmManagerClient;Landroid/drm/DrmInfoEvent;)V
    .locals 2
    .param p1    # Landroid/drm/DrmManagerClient;
    .param p2    # Landroid/drm/DrmInfoEvent;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DRM info "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->eventType(Landroid/drm/DrmEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/drm/DrmInfoEvent;->getUniqueId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/drm/DrmInfoEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    return-void
.end method

.method public onPlaybackStopped(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V

    return-void
.end method

.method public request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    iget-boolean v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->appLevelDrm:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method protected requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x3

    new-instance v2, Landroid/drm/DrmInfoRequest;

    const-string v0, "video/wvm"

    invoke-direct {v2, v5, v0}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    const-string v0, "WVPortalKey"

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v1, "server_uri"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v1, "WVDRMServerKey"

    new-instance v3, Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v4, "server_uri"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v1, v3}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    const-string v0, "WVAssetURIKey"

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "WVDeviceIDKey"

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "WVLicenseTypeKey"

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v0, :cond_7

    const-string v0, "3"

    :goto_1
    invoke-virtual {v2, v1, v0}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    if-eqz v0, :cond_0

    const-string v0, "WVKeyIDKey"

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v3, v1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "WVAssetIDKey"

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v3, v1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "WVSystemIDKey"

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v3, v1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "WVStreamIDKey"

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "v=2"

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&videoid="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&aid="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->gservicesId:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "&root="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I

    if-eq v0, v5, :cond_8

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    if-ne v0, v1, :cond_9

    const-string v0, "&clientauth="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v1, v1, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_3
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/drm/DrmResponse;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "&time_since_started="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/google/android/youtube/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v1, "robot_token"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "&robottoken="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v3, "robot_token"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    const/16 v3, 0xb

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v0, v1, :cond_5

    const-string v0, "&unpin=true"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&sign="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    iget-object v3, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v3, v8}, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;->sign(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    const-string v0, "WVCAUserDataKey"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->logDrmInfoRequest(Landroid/drm/DrmInfoRequest;)V

    iget-object v9, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->executor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/drm/DrmInfoRequest;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmResponse;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_4
    return-void

    :cond_6
    const-string v0, "WVDRMServerKey"

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->wvDrmServerUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "1"

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_9
    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    if-ne v0, v1, :cond_2

    const-string v0, "&oauth="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v1, v1, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :catch_0
    move-exception v6

    invoke-interface {p3, p1, v6}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_4
.end method
