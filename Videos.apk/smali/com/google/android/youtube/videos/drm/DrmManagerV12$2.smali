.class Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;
.super Ljava/lang/Object;
.source "DrmManagerV12.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV12;->requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$drmInfoRequest:Landroid/drm/DrmInfoRequest;

.field final synthetic val$existingLicense:Lcom/google/android/youtube/videos/drm/DrmResponse;

.field final synthetic val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/drm/DrmInfoRequest;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$drmInfoRequest:Landroid/drm/DrmInfoRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iput-object p4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$existingLicense:Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Landroid/drm/DrmManagerClient;

    move-result-object v5

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Landroid/drm/DrmManagerClient;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$drmInfoRequest:Landroid/drm/DrmInfoRequest;

    invoke-virtual {v4, v6}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v6, v6, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v6, v6, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getConstraints(Landroid/net/Uri;)Landroid/content/ContentValues;
    invoke-static {v4, v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$200(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;)Landroid/content/ContentValues;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v4, "WVLastErrorKey"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    const-string v6, "WVLastErrorKey"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->parseErrorCode(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    invoke-virtual {v6, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    move-result-object v6

    invoke-direct {v4, v6, v3}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    throw v4

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    :try_start_3
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v6, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    const/4 v7, -0x3

    invoke-direct {v4, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    throw v4

    :cond_1
    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v6, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    const/4 v7, -0x3

    invoke-direct {v4, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    throw v4

    :cond_2
    const-string v4, "VideosRequest"

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {v1, v4, v6}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v4, "VideosCallback"

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v1, v4, v6}, Landroid/drm/DrmInfo;->put(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->val$existingLicense:Lcom/google/android/youtube/videos/drm/DrmResponse;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->shouldRefreshLicense(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)Z
    invoke-static {v4, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "No need to refresh, using cached license"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    const/4 v6, 0x0

    const/4 v7, 0x0

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V
    invoke-static {v4, v1, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/drm/DrmInfo;ZZ)V

    :cond_3
    :goto_1
    monitor-exit v5

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$500(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "Network unavailable, using cached license"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    const/4 v6, 0x0

    const/4 v7, 0x0

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V
    invoke-static {v4, v1, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/drm/DrmInfo;ZZ)V

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$2;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Landroid/drm/DrmManagerClient;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lcom/google/android/youtube/videos/drm/DrmException;

    sget-object v6, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    const/4 v7, -0x4

    invoke-direct {v4, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method
