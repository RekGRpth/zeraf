.class Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;
.super Ljava/lang/Object;
.source "DrmManagerV12.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV12;->invokePendingCallback(Landroid/drm/DrmInfo;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

.field final synthetic val$assetUri:Landroid/net/Uri;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

.field final synthetic val$refreshed:Z

.field final synthetic val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

.field final synthetic val$requested:Z


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;ZLcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmRequest;ZLcom/google/android/youtube/videos/drm/DrmManager$Identifiers;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$assetUri:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$requested:Z

    iput-object p4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iput-boolean p6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$refreshed:Z

    iput-object p7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$assetUri:Landroid/net/Uri;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getError(Landroid/net/Uri;)Lcom/google/android/youtube/videos/drm/DrmException;
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$600(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;)Lcom/google/android/youtube/videos/drm/DrmException;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->drmManager:Landroid/drm/DrmManagerClient;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Landroid/drm/DrmManagerClient;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$assetUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$requested:Z

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$refreshed:Z

    if-eqz v4, :cond_4

    :cond_1
    if-nez v3, :cond_4

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$assetUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-boolean v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$refreshed:Z

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getLicense(Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    invoke-static {v4, v5, v6, v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$000(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v4, :cond_3

    iget-boolean v4, v2, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "License is valid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v4, v5, :cond_6

    iget-object v4, v1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-eq v4, v5, :cond_5

    iget-object v4, v1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v4, v5, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->config:Lcom/google/android/youtube/videos/Config;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$700(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->fallbackDrmErrorCodes()Ljava/util/List;

    move-result-object v4

    iget v5, v1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I
    invoke-static {v5}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$800(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)I

    move-result v5

    iget v6, v1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-virtual {v4, v5, v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->setFrameworkDrmFallbackInfo(II)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    const/4 v5, -0x1

    # setter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->frameworkDrmLevel:I
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$802(Lcom/google/android/youtube/videos/drm/DrmManagerV12;I)I

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->appLevelDrmManager:Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    invoke-static {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV12;)Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getDrmLevel()I

    move-result v0

    if-lez v0, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Falling back to application level drm: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    new-instance v6, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    iget-object v7, v1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget v8, v1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-direct {v6, v7, v8, v0}, Lcom/google/android/youtube/videos/drm/DrmFallbackException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;II)V

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$3;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-interface {v4, v5, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method
