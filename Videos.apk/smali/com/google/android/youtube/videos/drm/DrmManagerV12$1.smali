.class Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;
.super Ljava/lang/Object;
.source "DrmManagerV12.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV12;->requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/drm/DrmRequest;->stream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    const/4 v3, 0x0

    const/4 v4, 0x0

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV12;->getLicense(Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    invoke-static {v2, v0, v3, v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->access$000(Lcom/google/android/youtube/videos/drm/DrmManagerV12;Landroid/net/Uri;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->val$request:Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV12$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;->requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
