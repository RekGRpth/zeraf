.class Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DrmRequestRetryStrategy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
        "<",
        "Lcom/google/android/youtube/videos/drm/DrmRequest;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManager$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public canRetry(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)Z
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Ljava/lang/Exception;

    instance-of v1, p2, Lcom/google/android/youtube/videos/drm/DrmException;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v1, v0, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;->canRetry(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public extractUserAuth(Lcom/google/android/youtube/videos/drm/DrmRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object v0
.end method

.method public bridge synthetic extractUserAuth(Ljava/lang/Object;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;->extractUserAuth(Lcom/google/android/youtube/videos/drm/DrmRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method

.method public recreateRequest(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p1, p2}, Lcom/google/android/youtube/videos/drm/DrmRequest;->copyWithAuth(Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;->recreateRequest(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0
.end method
