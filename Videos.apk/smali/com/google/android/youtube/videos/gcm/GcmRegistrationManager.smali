.class public Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
.super Ljava/lang/Object;
.source "GcmRegistrationManager.java"


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final enabled:Z

.field private final gcmRegisterCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmUnregisterCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final networkExecutor:Ljava/util/concurrent/Executor;

.field private final registerWithServerRunnable:Ljava/lang/Runnable;

.field private registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private final unregisterWithServerRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->enabled:Z

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    new-instance v0, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;-><init>(I)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->networkExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;-><init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->gcmRegisterCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$2;-><init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->gcmUnregisterCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$3;

    invoke-direct {v0, p0, p3, p4}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$3;-><init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registerWithServerRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;

    invoke-direct {v0, p0, p3, p5}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;-><init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->unregisterWithServerRunnable:Ljava/lang/Runnable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_gcm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->setRegistrationId(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->clearRegistrationId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getShouldRegister(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getRegistrationId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->gcmRegisterCallback:Lcom/google/android/youtube/core/async/Callback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->clearShouldRegister(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->gcmUnregisterCallback:Lcom/google/android/youtube/core/async/Callback;

    return-object v0
.end method

.method private anyPendingRegistration(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getShouldRegister(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getRegistrationId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private clearLatestRegistrationId()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastest_registration_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private clearRegistrationId(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private clearShouldRegister(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private getLatestRegistrationId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "lastest_registration_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRegistrationId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getShouldRegister(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_should_register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private registerWithGcm(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkDevice(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkManifest(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "859429584213"

    aput-object v3, v1, v2

    invoke-static {p1, v1}, Lcom/google/android/gcm/GCMRegistrar;->register(Landroid/content/Context;[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registerWithServer(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLatestRegistrationId(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastest_registration_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private setRegistrationId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_registration_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private setShouldRegister(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_should_register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method


# virtual methods
.method public ensureRegistered(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->enabled:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->setShouldRegister(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->anyPendingRegistration(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getLatestRegistrationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registerWithGcm(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerWithServer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->setLatestRegistrationId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->networkExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registerWithServerRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public unregisterWithServer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->networkExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->unregisterWithServerRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->clearLatestRegistrationId()V

    return-void
.end method
