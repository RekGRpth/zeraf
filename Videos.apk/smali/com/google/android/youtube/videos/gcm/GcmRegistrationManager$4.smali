.class Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;
.super Ljava/lang/Object;
.source "GcmRegistrationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;-><init>(Landroid/content/Context;ZLcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

.field final synthetic val$accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field final synthetic val$gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->val$accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object p3, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->val$gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v6, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    # getter for: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->registrationIdRefeference:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v6}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$200(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->val$accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    # invokes: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->clearShouldRegister(Ljava/lang/String;)V
    invoke-static {v6, v1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$600(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    # invokes: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->getRegistrationId(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v6, v1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$400(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->val$gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v7, Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$4;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    # getter for: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->gcmUnregisterCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v8}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$700(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
