.class public Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;
.super Ljava/lang/Object;
.source "RecommendationsRequest.java"

# interfaces
.implements Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultFactory"
.end annotation


# instance fields
.field private resources:Landroid/content/res/Resources;

.field private telephonyManager:Landroid/telephony/TelephonyManager;

.field private videosConfig:Lcom/google/android/youtube/videos/Config;


# direct methods
.method public constructor <init>(Landroid/telephony/TelephonyManager;Landroid/content/res/Resources;Lcom/google/android/youtube/videos/Config;)V
    .locals 0
    .param p1    # Landroid/telephony/TelephonyManager;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Lcom/google/android/youtube/videos/Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->telephonyManager:Landroid/telephony/TelephonyManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->resources:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->videosConfig:Lcom/google/android/youtube/videos/Config;

    return-void
.end method

.method private create(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .param p4    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    new-instance v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v6, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->videosConfig:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/api/RecommendationsRequest;-><init>(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Lcom/google/android/youtube/videos/api/RecommendationsRequest$1;)V

    return-object v0
.end method


# virtual methods
.method public createForMovies(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MOVIE:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-virtual {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public createForShows(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;->create(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method
