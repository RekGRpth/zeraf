.class public Lcom/google/android/youtube/videos/api/AssetListConverter;
.super Ljava/lang/Object;
.source "AssetListConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/api/AssetsRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Landroid/net/Uri;

.field private final converter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "asset"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetListConverter;->baseUri:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetListConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/api/AssetsRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/AssetListConverter;->convertRequest(Lcom/google/android/youtube/videos/api/AssetsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/videos/api/AssetsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/api/AssetsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/AssetListConverter;->baseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "cr"

    iget-object v4, p1, Lcom/google/android/youtube/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "m"

    iget-boolean v4, p1, Lcom/google/android/youtube/videos/api/AssetsRequest;->includeMetadata:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "i"

    iget-boolean v4, p1, Lcom/google/android/youtube/videos/api/AssetsRequest;->includeImages:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/youtube/videos/api/AssetsRequest;->encodedIds:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/AssetListConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    new-instance v3, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-direct {v3, v1, v5, v5, v5}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v2
.end method

.method public convertResponse([B)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->parseFrom([B)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/AssetListConverter;->convertResponse([B)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    move-result-object v0

    return-object v0
.end method
