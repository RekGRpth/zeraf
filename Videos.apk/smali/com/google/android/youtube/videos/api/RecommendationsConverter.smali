.class public Lcom/google/android/youtube/videos/api/RecommendationsConverter;
.super Ljava/lang/Object;
.source "RecommendationsConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Landroid/net/Uri;

.field private final converter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->baseUri:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-void
.end method

.method private buildCategoryFromType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildQueryFromAssetId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->convertRequest(Lcom/google/android/youtube/videos/api/RecommendationsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/videos/api/RecommendationsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/api/RecommendationsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->baseUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "recommendation"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "max"

    iget v3, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->max:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "cat"

    iget-object v3, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->buildCategoryFromType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v1, :cond_0

    const-string v1, "q"

    iget-object v2, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->buildQueryFromAssetId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v1, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "mcc_mnc"

    iget-object v2, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    iget-object v1, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    if-eqz v1, :cond_2

    const-string v1, "lr"

    iget-object v2, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    iget-object v1, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, "cr"

    iget-object v2, p1, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    new-instance v2, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v3, v4, v4, v4}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v1
.end method

.method public convertResponse([B)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->newBuilder()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->build()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/RecommendationsConverter;->convertResponse([B)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    move-result-object v0

    return-object v0
.end method
