.class public Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.super Lcom/google/android/youtube/videos/api/Request;
.source "RecommendationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/api/RecommendationsRequest$1;,
        Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;,
        Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
    }
.end annotation


# instance fields
.field public final country:Ljava/lang/String;

.field public final locale:Ljava/util/Locale;

.field public final max:I

.field public final mccMnc:Ljava/lang/String;

.field public final queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public final type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;


# direct methods
.method private constructor <init>(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .param p4    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/util/Locale;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/Request;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->max:I

    iput-object p3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    iput-object p4, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iput-object p5, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    iput-object p7, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Lcom/google/android/youtube/videos/api/RecommendationsRequest$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .param p4    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/util/Locale;
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/youtube/videos/api/RecommendationsRequest$1;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/youtube/videos/api/RecommendationsRequest;-><init>(Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->max:I

    iget v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->max:I

    if-eq v3, v4, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areProtosEqual(Lcom/google/protobuf/MessageLite;Lcom/google/protobuf/MessageLite;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    iget-object v4, v0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    const/4 v3, 0x0

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->max:I

    add-int v1, v2, v4

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v4}, Lcom/google/android/youtube/core/utils/Util;->protoHashCode(Lcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int v1, v2, v4

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    if-nez v4, :cond_4

    :goto_4
    add-int v1, v2, v3

    return v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/google/android/youtube/videos/api/RecommendationsRequest;->type:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->hashCode()I

    move-result v3

    goto :goto_4
.end method
