.class public final Lcom/google/android/youtube/videos/api/AssetResourceUtil;
.super Ljava/lang/Object;
.source "AssetResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/api/AssetResourceUtil$1;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deviation(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;FF)D
    .locals 11
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .param p1    # F
    .param p2    # F

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getWidth()I

    move-result v7

    int-to-float v4, v7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getHeight()I

    move-result v7

    int-to-float v3, v7

    cmpg-float v7, v4, v9

    if-lez v7, :cond_0

    cmpg-float v7, v3, v9

    if-gtz v7, :cond_1

    :cond_0
    const-wide/high16 v7, 0x7ff0000000000000L

    :goto_0
    return-wide v7

    :cond_1
    div-float v7, p1, v4

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->log(D)D

    move-result-wide v7

    double-to-float v6, v7

    div-float v7, p2, v3

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->log(D)D

    move-result-wide v7

    double-to-float v2, v7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getResizable()Z

    move-result v7

    if-eqz v7, :cond_2

    cmpg-float v7, v6, v9

    if-gez v7, :cond_2

    cmpg-float v7, v2, v9

    if-gez v7, :cond_2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float/2addr v6, v0

    sub-float/2addr v2, v0

    :cond_2
    sub-float v7, v6, v2

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    add-float v5, v7, v8

    float-to-double v7, v5

    const/high16 v9, 0x42480000

    mul-float/2addr v9, v1

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->exp(D)D

    move-result-wide v9

    mul-double/2addr v7, v9

    goto :goto_0
.end method

.method public static findBestImage(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;II)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 10
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImagesCount()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v7, 0x0

    const-wide/high16 v5, 0x7ff0000000000000L

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_5

    invoke-virtual {p0, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImages(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v8

    if-eq p1, v8, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez v7, :cond_4

    move-object v7, v0

    int-to-float v8, p2

    int-to-float v9, p3

    invoke-static {v7, v8, v9}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->deviation(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;FF)D

    move-result-wide v5

    goto :goto_2

    :cond_4
    int-to-float v8, p2

    int-to-float v9, p3

    invoke-static {v0, v8, v9}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->deviation(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;FF)D

    move-result-wide v1

    cmpg-double v8, v1, v5

    if-gez v8, :cond_2

    move-wide v5, v1

    move-object v7, v0

    goto :goto_2

    :cond_5
    move-object v0, v7

    goto :goto_0
.end method

.method public static idFromAlbumId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "albumId cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sj:album:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromMovieId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "videoId cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:movie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromMovieMid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "mid cannot be empty"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m:movie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "seasonId cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:season:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromShowId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "showId cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yt:show:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromShowMid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "mid cannot be empty"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m:show:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromSongId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "songId cannot be empty"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sj:song:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static idFromTypeAndId(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/String;

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p1}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromAlbumId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-static {p1}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromSongId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static itemTypeToInteger(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)I
    .locals 2
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    sget-object v0, Lcom/google/android/youtube/videos/api/AssetResourceUtil$1;->$SwitchMap$com$google$wireless$android$video$magma$proto$AssetResourceId$Type:[I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
