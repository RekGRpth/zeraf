.class final Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;
.super Ljava/lang/Object;
.source "SimpleAuthenticatedHttpRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RetryingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/youtube/videos/api/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private retried:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

.field private final userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/api/Request;)V
    .locals 0
    .param p3    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "TR;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object p4, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/api/Request;Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;
    .param p3    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p4    # Lcom/google/android/youtube/videos/api/Request;
    .param p5    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;-><init>(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/api/Request;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->onError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Exception;)V
    .locals 6
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2    # Ljava/lang/Exception;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->retried:Z

    if-nez v2, :cond_0

    const/16 v2, 0x191

    invoke-static {p2, v2}, Lcom/google/android/youtube/core/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    invoke-interface {v2, v3, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->retried:Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    # getter for: Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->access$100(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->invalidateToken(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    iget-object v3, v3, Lcom/google/android/youtube/videos/api/Request;->account:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->access$200(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Could not fetch userAuth"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    # getter for: Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->target:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->access$500(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->this$0:Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    # getter for: Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;
    invoke-static {v2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->access$300(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/core/converter/RequestConverter;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    invoke-interface {v2, v5}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/client/methods/HttpUriRequest;

    # invokes: Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->addCredentials(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v4, v2, v1}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->access$400(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-interface {v3, v2, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->onResponse(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Object;)V

    return-void
.end method

.method public onResponse(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;->originalRequest:Lcom/google/android/youtube/videos/api/Request;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
