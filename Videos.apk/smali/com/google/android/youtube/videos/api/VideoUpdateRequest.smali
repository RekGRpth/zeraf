.class public Lcom/google/android/youtube/videos/api/VideoUpdateRequest;
.super Lcom/google/android/youtube/videos/api/Request;
.source "VideoUpdateRequest.java"


# instance fields
.field public final playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/Request;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/youtube/videos/api/VideoUpdateRequest;->videoId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/api/VideoUpdateRequest;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-void
.end method
