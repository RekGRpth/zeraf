.class public Lcom/google/android/youtube/videos/api/GcmRegisterConverter;
.super Ljava/lang/Object;
.source "GcmRegisterConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Landroid/net/Uri;

.field private final converter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final gservicesId:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;J)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->baseUri:Landroid/net/Uri;

    iput-wide p2, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->gservicesId:J

    new-instance v0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->PUT:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->convertRequest(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/api/GcmRegisterRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    new-instance v1, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->baseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "device"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->gservicesId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "gcm"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->newBuilder()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;->registrationId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->setRegistrationId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest$Builder;->build()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->toByteArray()[B

    move-result-object v3

    invoke-direct {v1, v2, v5, v5, v3}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;->convertResponse([B)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public convertResponse([B)Ljava/lang/Void;
    .locals 1
    .param p1    # [B

    const/4 v0, 0x0

    return-object v0
.end method
