.class public Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;
.super Lcom/google/android/youtube/videos/api/Request;
.source "RemoveFromWishlistRequest.java"


# instance fields
.field public final assetResourceId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/Request;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;->assetResourceId:Ljava/lang/String;

    return-void
.end method
