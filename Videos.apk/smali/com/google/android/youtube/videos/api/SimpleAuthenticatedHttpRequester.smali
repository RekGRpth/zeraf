.class public final Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
.super Ljava/lang/Object;
.source "SimpleAuthenticatedHttpRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$1;,
        Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/youtube/videos/api/Request;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object p3, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    iput-object p2, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/core/converter/RequestConverter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->addCredentials(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method private addCredentials(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    sget-object v1, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$1;->$SwitchMap$com$google$android$youtube$videos$accounts$UserAuth$AuthType:[I

    iget-object v2, p2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unsupported authorization method"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleLogin auth=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Authorization"

    invoke-interface {p1, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :pswitch_1
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static create(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/youtube/videos/api/Request;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "TE;>;)",
            "Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)V

    return-object v0
.end method

.method private getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/api/Request;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/youtube/videos/api/Request;->account:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Could not fetch userAuth"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->requestConverter:Lcom/google/android/youtube/core/converter/RequestConverter;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->addCredentials(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$RetryingCallback;-><init>(Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/api/Request;Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester$1;)V

    invoke-interface {v7, v8, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-interface {p2, p1, v6}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/api/Request;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->request(Lcom/google/android/youtube/videos/api/Request;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
