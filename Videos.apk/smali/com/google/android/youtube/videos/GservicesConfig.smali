.class final Lcom/google/android/youtube/videos/GservicesConfig;
.super Ljava/lang/Object;
.source "GservicesConfig.java"

# interfaces
.implements Lcom/google/android/youtube/videos/Config;


# static fields
.field private static final BASE_API_URI:Landroid/net/Uri;

.field private static final BASE_KNOWLEDGE_URI:Landroid/net/Uri;


# instance fields
.field private final context:Landroid/content/Context;

.field private final packageManager:Landroid/content/pm/PackageManager;

.field private final resolver:Landroid/content/ContentResolver;

.field private final uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://www.googleapis.com/android_video/v1/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/GservicesConfig;->BASE_API_URI:Landroid/net/Uri;

    const-string v0, "https://play.google.com/video/downloads/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/GservicesConfig;->BASE_KNOWLEDGE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/youtube/core/utils/UriRewriter;Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Lcom/google/android/youtube/core/utils/UriRewriter;
    .param p4    # Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/google/android/youtube/videos/GservicesConfig;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    iput-object p4, p0, Lcom/google/android/youtube/videos/GservicesConfig;->packageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private getBoolean(Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getInt(Ljava/lang/String;I)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getLong(Ljava/lang/String;J)J
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private isTungsten()Z
    .locals 2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "phantasm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abrFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "abrFormats"

    const-string v2, "15,16,32,34,35"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public allowDownloads()Z
    .locals 2

    const-string v1, "allow_downloads"

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->packageManager:Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public atHomeHdmiDisconnectTimeoutMillis()J
    .locals 3

    const-string v0, "at_home_hdmi_disconnect_timeout_millis"

    const-wide/16 v1, 0x1388

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public atHomeIdleTimeoutMillis()J
    .locals 3

    const-string v0, "at_home_idle_timeout_millis"

    const-wide/32 v1, 0x36ee80

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public atHomeRobotTokenRequestUri()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "at_home_robot_token_request_uri"

    const-string v2, "https://play.google.com/video/license/GetRobotToken"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public atHomeVolumeStepPercent()I
    .locals 2

    const-string v0, "at_home_volume_step_percent"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public baseApiUri()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    sget-object v1, Lcom/google/android/youtube/videos/GservicesConfig;->BASE_API_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public baseKnowledgeUri()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    sget-object v1, Lcom/google/android/youtube/videos/GservicesConfig;->BASE_KNOWLEDGE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bufferingEventsBeforeQualityDrop()I
    .locals 2

    const-string v0, "bufferingEventsBeforeQualityDrop"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public bufferingSettleMillis()J
    .locals 3

    const-string v0, "bufferingSettleMillis"

    const-wide/16 v1, 0x1388

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bufferingWindowMillis()J
    .locals 3

    const-string v0, "bufferingWindowMillis"

    const-wide/32 v1, 0xea60

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public defaultWelcomeVertical()Ljava/lang/String;
    .locals 2

    const-string v0, "default_welcome_vertical"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deviceCountry()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    const-string v2, "country_override"

    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    const-string v3, "device_country"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->toUpperInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method public downloadFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "downloadFormats"

    const-string v2, "13,8,14,11,3,12,20"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public fallbackDrmErrorCodes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "fallbackDrmErrorCodes"

    const-string v2, "49,108"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public feedbackProductId()I
    .locals 2

    const-string v0, "feedback_product_id"

    const v1, 0x11077

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public feedbackSubmitUrlTemplate()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "https://www.google.com/tools/feedback/submit?at={$GF_TOKEN}"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public feedbackTokenUrl()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "https://www.google.com/tools/feedback/preview?productId={$PRODUCT_ID}"

    const-string v2, "{$PRODUCT_ID}"

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/GservicesConfig;->feedbackProductId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public fieldProvisionedFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "fieldProvisionedFormats"

    const-string v2, "15,14,11,3,12,20"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public forceAppLevelDrm()Z
    .locals 2

    const-string v0, "force_app_level_drm"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public forceMirrorMode()Z
    .locals 2

    const-string v0, "force_mirror_mode"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public gcmRegistrationEnabled()Z
    .locals 2

    const-string v0, "gcm_registration_enabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public gdataVersion()Lcom/google/android/youtube/core/async/GDataRequest$Version;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequest$Version;->V_2:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    return-object v0
.end method

.method public getAnalyticsCategorySuffix()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_category_suffix"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsEnabled()Z
    .locals 2

    const-string v0, "analytics_enabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAnalyticsPropertyId()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_property_id"

    const-string v1, "UA-20803990-2"

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsSampleRatio()I
    .locals 2

    const-string v0, "analytics_sample_ratio"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getAnalyticsUpdateSecs()I
    .locals 2

    const-string v0, "analytics_update_secs"

    const/16 v1, 0x1e

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getExperimentId()Ljava/lang/String;
    .locals 2

    const-string v0, "experiment_id"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public gmsCoreAvailable()Z
    .locals 2

    const-string v1, "gms_core_available"

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public gservicesId()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->resolver:Landroid/content/ContentResolver;

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public hdStreamingFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "hdFormats"

    const-string v2, "16,8"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public isAtHomeDevice()Z
    .locals 2

    const-string v0, "at_home_device"

    invoke-direct {p0}, Lcom/google/android/youtube/videos/GservicesConfig;->isTungsten()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public knowledgeEnabled()Z
    .locals 3

    const/4 v0, 0x0

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/GservicesConfig;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/DisplayUtil;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "knowledge_enabled_tablets"

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    const-string v1, "knowledge_enabled_phones"

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public knowledgeFeedbackTypeId()I
    .locals 2

    const-string v0, "knowledge_feedback_type_id"

    const v1, 0x17898

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeRecheckDataAfterMillis()J
    .locals 3

    const-string v0, "knowledge_recheck_data_after_millis"

    const-wide/32 v1, 0x5265c00

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public knowledgeShowAllCards()Z
    .locals 2

    const-string v0, "knowledge_show_all_cards"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public knowledgeShowRecentActorsWithinMillis()I
    .locals 2

    const-string v0, "knowledge_show_recent_actors_within_millis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public knowledgeWelcomeBrowseUri()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "knowledge_welcome_browse_uri"

    const-string v2, ""

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public knowledgeWelcomeGraphic()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "knowledge_welcome_graphic"

    const-string v2, ""

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public localVerticalEnabled()Z
    .locals 2

    const-string v1, "local_vertical_enabled"

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->packageManager:Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lqFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "lqFormats"

    const-string v2, "20,12,3,11,15,14"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public maxConcurrentLicenseTasks()I
    .locals 2

    const-string v0, "maxConcurrentLicenseTasks"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxConcurrentOrBackedOffPinningTasks()I
    .locals 2

    const-string v0, "maxConcurrentPinningTasks"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxConcurrentUpdateUserdataTasks()I
    .locals 2

    const-string v0, "maxConcurrentUpdateUserdataTasks"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxLicenseTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "maxLicenseTaskRetryDelayMillis"

    const v1, 0x36ee80

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxNewContentNotificationDelayMillis()J
    .locals 3

    const-string v0, "max_new_content_notification_delay_millis"

    const-wide/32 v1, 0x240c8400

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public maxPinningTaskRetries()I
    .locals 2

    const-string v0, "maxPinningTaskRetries"

    const/16 v1, 0x14

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxPinningTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "maxPinningTaskRetryDelayMillis"

    const v1, 0xea60

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxResumeTimeValidityMillis()J
    .locals 3

    const-string v0, "max_resume_time_validity_millis"

    const-wide/32 v1, 0x14997000

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public maxUpdateUserdataTaskRetries()I
    .locals 2

    const-string v0, "maxUpdateUserdataTaskRetries"

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public maxUpdateUserdataTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "maxUpdateUserdataTaskRetryDelayMillis"

    const v1, 0x36ee80

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minLicenseTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "minLicenseTaskRetryDelayMillis"

    const/16 v1, 0x7d0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minPinningTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "minPinningTaskRetryDelayMillis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minUpdateUserdataTaskRetryDelayMillis()I
    .locals 2

    const-string v0, "minUpdateUserdataTaskRetryDelayMillis"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public minimumVersion()I
    .locals 2

    const-string v0, "minimum_version"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public mobileDownloadsEnabled()Z
    .locals 2

    const-string v0, "mobileDownloadsEnabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public mobileStreamingEnabled()Z
    .locals 2

    const-string v0, "mobileStreamingEnabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public moviesVerticalEnabled()Z
    .locals 2

    const-string v0, "movies_vertical_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public moviesWelcomeBrowseUri()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "movies_welcome_browse_uri"

    const-string v2, "https://play.google.com/store/movies"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public moviesWelcomeGraphic()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "movies_welcome_graphic"

    const-string v2, ""

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public moviesWelcomeIsFree()Z
    .locals 2

    const-string v0, "movies_welcome_is_free"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public needsSystemUpdate()Z
    .locals 2

    const-string v0, "needs_system_update"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ratingScheme()Ljava/lang/String;
    .locals 6

    const-string v4, "ratingSchemes"

    const-string v5, "AU,urn:acb,CA,urn:chvrs,DE,urn:fsk,ES,urn:icaa,FR,urn:fmoc,GB,urn:bbfc,IN,urn:cbfc,JP,urn:eirin,KR,urn:kmrb,NZ,urn:oflc,US,urn:mpaa,BR,urn:djctq,RU,urn:russia,MX,urn:rtc"

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/GservicesConfig;->deviceCountry()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v4, v1, 0x1

    aget-object v4, v2, v4

    :goto_1
    return-object v4

    :cond_0
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_1
    const-string v4, "defaultRatingScheme"

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public refreshLicensesOlderThanMillis()J
    .locals 3

    const-string v0, "transferServicePingInterval"

    const-wide/32 v1, 0x240c8400

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncSeasonEpisodesAfterMillis()J
    .locals 3

    const-string v0, "resync_season_episodes_after_millis"

    const-wide/32 v1, 0x44aa200

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public resyncVideoAfterMillis()J
    .locals 3

    const-string v0, "resync_video_metadata_after_millis"

    const-wide/32 v1, 0x240c8400

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public rootedAppLevelDrmEnabled()Z
    .locals 2

    const-string v0, "rootedAppLevelDrmEnabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public rootedFrameworkLevelDrmEnabled()Z
    .locals 2

    const-string v0, "rootedFrameworkLevelDrmEnabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public sdStreamingFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v1, "sdFormats"

    const-string v2, "15,14,11,3,12,20"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public showsVerticalEnabled()Z
    .locals 2

    const-string v0, "shows_vertical_enabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public showsWelcomeBrowseUri()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "shows_welcome_browse_uri"

    const-string v2, "https://play.google.com/store/movies/category/TV"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public showsWelcomeGraphic()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "shows_welcome_graphic"

    const-string v2, ""

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public showsWelcomeIsFree()Z
    .locals 2

    const-string v0, "shows_welcome_is_free"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public surroundSoundFormats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-le v1, v2, :cond_0

    const-string v1, "surroundSoundFormats"

    const-string v2, ""

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public transferServicePingIntervalMillis()J
    .locals 3

    const-string v0, "transferServicePingInterval"

    const-wide/32 v1, 0x55d4a80

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public useGDataMovieSuggestions()Z
    .locals 2

    const-string v0, "use_gdata_movie_suggestions"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useGDataRelatedMovieSuggestions()Z
    .locals 2

    const-string v0, "use_gdata_related_movie_suggestions"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useGDataShowSuggestions()Z
    .locals 2

    const-string v0, "use_gdata_show_suggestions"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public usePlayAnalytics()Z
    .locals 2

    const-string v0, "use_play_analytics"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useSslForDownloads()Z
    .locals 2

    const-string v0, "use_ssl_for_downloads"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public wvDrmServerUri()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/GservicesConfig;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v1, "wv_drm_server_uri"

    const-string v2, "https://play.google.com/video/license/GetEMMs.cgi"

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wvPortalName()Ljava/lang/String;
    .locals 2

    const-string v0, "wv_portal_name"

    const-string v1, "YouTube"

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/GservicesConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
