.class Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;
.super Ljava/lang/Object;
.source "DefaultAccountManagerWrapper.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuthTokenCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

.field final synthetic this$0:Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->this$0:Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    const/4 v7, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    const-string v4, "intent"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iput-object v7, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    :goto_1
    return-void

    :cond_0
    :try_start_1
    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "authtoken"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    new-instance v5, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v6, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->this$0:Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;

    iget-object v6, v6, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    invoke-direct {v5, v0, v6, v1}, Lcom/google/android/youtube/videos/accounts/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v7, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    :cond_1
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "got null authToken for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    new-instance v6, Landroid/accounts/AuthenticatorException;

    invoke-direct {v6}, Landroid/accounts/AuthenticatorException;-><init>()V

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_4
    const-string v4, "login IOException"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5, v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iput-object v7, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    :catch_2
    move-exception v2

    :try_start_5
    const-string v4, "login AuthenticatorException"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5, v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iput-object v7, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    :catchall_0
    move-exception v4

    iput-object v7, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    throw v4
.end method
