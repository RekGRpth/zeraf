.class public Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;
.super Ljava/lang/Object;
.source "UserAuth.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/UserAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthMethod"
.end annotation


# instance fields
.field public final authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

.field public final scope:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    iput-object p2, p0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    return-void
.end method

.method public static createClientLogin(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;-><init>(Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createOAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;
    .locals 4
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "oauth2:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;-><init>(Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;Ljava/lang/String;)V

    return-object v0
.end method
