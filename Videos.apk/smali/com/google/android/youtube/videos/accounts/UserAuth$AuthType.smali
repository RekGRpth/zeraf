.class public final enum Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;
.super Ljava/lang/Enum;
.source "UserAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/UserAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AuthType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

.field public static final enum CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

.field public static final enum OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    const-string v1, "CLIENTLOGIN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    new-instance v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    const-string v1, "OAUTH2"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->$VALUES:[Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;
    .locals 1

    const-class v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->$VALUES:[Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-virtual {v0}, [Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    return-object v0
.end method
