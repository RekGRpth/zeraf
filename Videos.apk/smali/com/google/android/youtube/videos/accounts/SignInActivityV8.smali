.class public Lcom/google/android/youtube/videos/accounts/SignInActivityV8;
.super Landroid/app/Activity;
.source "SignInActivityV8.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;
    }
.end annotation


# instance fields
.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private dialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onSignInCanceled()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountSelected(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->addAccount()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountNotAdded()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/accounts/SignInActivityV8;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onSignInError(Ljava/lang/Exception;)V

    return-void
.end method

.method private addAccount()V
    .locals 2

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountNotAdded()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "adding account"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    new-instance v1, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;-><init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->addAccount(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V

    goto :goto_0
.end method

.method private getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object v5, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    move-object v2, v1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static newChooseAccountIntent(Landroid/content/Context;Landroid/accounts/Account;[Landroid/accounts/Account;)Landroid/content/Intent;
    .locals 5

    array-length v0, p2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    aget-object v3, p2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v0

    if-eqz p1, :cond_0

    aget-object v3, v2, v0

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "accounts"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "selected_account_index"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private onAccountNotAdded()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method private onAccountSelected(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method private onSignInCanceled()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method private onSignInError(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "exception"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "accounts"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v4, "selected_account_index"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Provider;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Provider;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    new-instance v0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;I)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0a0067

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$2;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$2;-><init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$1;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$1;-><init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;-><init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$4;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$4;-><init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
