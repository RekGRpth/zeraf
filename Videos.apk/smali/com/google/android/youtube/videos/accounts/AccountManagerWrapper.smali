.class public abstract Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
.super Ljava/lang/Object;
.source "AccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;,
        Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Provider;
    }
.end annotation


# static fields
.field public static final DEFAULT_AUTH_METHOD:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;


# instance fields
.field protected final accountManager:Landroid/accounts/AccountManager;

.field protected final accountType:Ljava/lang/String;

.field public final authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "youtube"

    invoke-static {v0}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->createClientLogin(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->DEFAULT_AUTH_METHOD:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    sget-object v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->DEFAULT_AUTH_METHOD:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/accounts/AccountManager;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "accountManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    const-string v0, "authMethod cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    const-string v0, "accountType cannot be empty"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    return-void
.end method

.method private onNoAccountToAuthenticate(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/accounts/AuthenticatorException;

    const-string v1, "User account \'%s\' not found."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public accountExists(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addAccount(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V
    .locals 8
    .param p1    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    move-object v4, v3

    move-object v5, p1

    move-object v6, p2

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method public blockingGetUserAuth(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Landroid/accounts/Account;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuthInternal(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    goto :goto_0
.end method

.method public blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method

.method protected abstract blockingGetUserAuthInternal(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;
.end method

.method public getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_0

    aget-object v3, v0, v1

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-object v2, v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getAccountType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method public getAccounts()[Landroid/accounts/Account;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getUserAuth(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 1
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "activity cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuthInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    return-void
.end method

.method public getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const-string v1, "activity cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "callback cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->onNoAccountToAuthenticate(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0
.end method

.method protected abstract getUserAuthInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
.end method

.method public invalidateToken(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountType:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/Intent;

    const/4 v0, 0x0

    return v0
.end method

.method public peekUserAuth(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 1
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->peekUserAuthInternal(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    return-void
.end method

.method public peekUserAuth(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const-string v1, "callback cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->onNoAccountToAuthenticate(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->peekUserAuth(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0
.end method

.method protected abstract peekUserAuthInternal(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
.end method
