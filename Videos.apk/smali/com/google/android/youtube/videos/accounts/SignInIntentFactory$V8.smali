.class Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V8;
.super Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
.source "SignInIntentFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V8;-><init>()V

    return-void
.end method


# virtual methods
.method public newChooseAccountIntent(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p3    # Landroid/accounts/Account;
    .param p4    # Landroid/os/Bundle;

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->newChooseAccountIntent(Landroid/content/Context;Landroid/accounts/Account;[Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
