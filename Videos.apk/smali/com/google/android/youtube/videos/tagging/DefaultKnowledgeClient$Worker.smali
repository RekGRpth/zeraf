.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Worker"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;"
        }
    .end annotation
.end field

.field private final imageCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

.field private final knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

.field private final localImageRequester:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

.field private final localResourcesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTagStreamRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final resourcesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tagStreamCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private tagStreamParser:Lcom/google/android/youtube/videos/tagging/TagStreamParser;

.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)V
    .locals 1
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p6    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localTagStreamRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamCallback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localResourcesRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$2;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->resourcesCallback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->imageCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2    # Ljava/util/Map;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onImageResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onImageRequestError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method private end(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->setAlwaysRequest(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    invoke-interface {v0, v1, p1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onImageRequestError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error requesting knowledge bundle image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->localImageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onImageResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private onImageResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method private onResourcesRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error requesting asset resources for knowledge request "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V

    return-void
.end method

.method private onResourcesResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterWidth:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->access$600(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I

    move-result v2

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterHeight:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->access$700(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I

    move-result v3

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterWidth:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->access$800(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I

    move-result v4

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterHeight:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->access$900(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I

    move-result v5

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->build(Ljava/util/Map;IIII)Lcom/google/android/youtube/videos/tagging/TagStream;

    move-result-object v9

    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    invoke-direct {v0, v9, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;-><init>(Lcom/google/android/youtube/videos/tagging/TagStream;Lcom/google/android/youtube/core/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    invoke-virtual {v9}, Lcom/google/android/youtube/videos/tagging/TagStream;->getUniqueImages()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v7}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->imageCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Callback;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method private onTagStreamRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    return-void
.end method

.method private onTagStreamResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localResourcesRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iget-object v3, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->userCountry:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->getFilmographyIds()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Ljava/util/List;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->resourcesCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method public start(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->localTagStreamRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
