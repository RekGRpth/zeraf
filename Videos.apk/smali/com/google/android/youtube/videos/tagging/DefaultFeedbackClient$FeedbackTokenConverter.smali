.class final Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FeedbackTokenConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
        ">;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Ljava/lang/String;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final TOKEN_EXTRACTOR:Ljava/util/regex/Pattern;


# instance fields
.field private final tokenUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\\bGF_TOKEN\\s*=\\s*\"([^\"]+)\";"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->TOKEN_EXTRACTOR:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->tokenUrl:Ljava/lang/String;

    :goto_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
    .locals 6
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    const-string v3, "X-Auto-Login"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v3

    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->TOKEN_EXTRACTOR:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v5, v4}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v4, "Couldn\'t find feedback token in response"

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    move-result-object v0

    return-object v0
.end method
