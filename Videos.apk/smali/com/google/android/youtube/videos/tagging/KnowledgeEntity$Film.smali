.class public abstract Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
.super Ljava/lang/Object;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Film"
.end annotation


# instance fields
.field public final googlePlayUrl:Ljava/lang/String;

.field public final image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

.field public final releaseDate:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->googlePlayUrl:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    return-void
.end method
