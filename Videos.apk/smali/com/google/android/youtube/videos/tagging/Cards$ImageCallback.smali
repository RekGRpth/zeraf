.class final Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;
.super Ljava/lang/Object;
.source "Cards.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/Cards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ImageCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final imageView:Landroid/widget/ImageView;

.field private final requestTime:J

.field private response:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->requestTime:J

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->onError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->response:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->response:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->response:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->requestTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method
