.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;
.super Lcom/google/android/youtube/videos/store/AbstractFileStore;
.source "DefaultKnowledgeClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PinnedFileStore"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/youtube/videos/store/AbstractFileStore",
        "<",
        "Ljava/lang/String;",
        "TS;>;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/AbstractFileStore;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public bridge synthetic generateFilepath(Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;->generateFilepath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public generateFilepath(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    const-string v3, "knowledge"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v2
.end method
