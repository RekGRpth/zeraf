.class Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;
.super Ljava/lang/Object;
.source "CardsViewAnimationHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private static final DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;


# instance fields
.field private final animationThreshold:F

.field private final cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

.field private collapseMethod:Ljava/lang/String;

.field private final collapsedHeight:I

.field private currentAnimator:Landroid/animation/ObjectAnimator;

.field private detectCollapseGesture:Z

.field private expandMethod:Ljava/lang/String;

.field private final gestureDetector:Landroid/view/GestureDetector;

.field private grabbedFromExpanded:Z

.field private final knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

.field private listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

.field private final maxAnimationDurationMillis:I

.field private maxTranslationY:F

.field private final minFlingVelocity:I

.field private pendingState:I

.field private state:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/CardsView;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .param p2    # Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v5, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Landroid/view/GestureDetector;

    const/4 v4, 0x0

    invoke-direct {v3, v0, p0, v4, v5}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, v6}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090030

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapsedHeight:I

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    new-array v4, v5, [I

    const v5, 0x1010112

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-virtual {v2, v6, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x42c80000

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->animationThreshold:F

    return-void
.end method

.method private collapse(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Landroid/animation/TimeInterpolator;
    .param p3    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/youtube/videos/tagging/CardsView;->smoothScrollTo(II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expandMethod:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapseMethod:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->isAnyCardShown()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->setTranslationY(F)V

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    return-void

    :cond_2
    const/4 v1, 0x5

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    move-object v0, p0

    move v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->startAnimation(IIFILandroid/animation/TimeInterpolator;)V

    goto :goto_0
.end method

.method private expand(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/animation/TimeInterpolator;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapseMethod:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expandMethod:Ljava/lang/String;

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x4

    move-object v0, p0

    move v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->startAnimation(IIFILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    return-void
.end method

.method private getFlingAnimationDuration(FF)I
    .locals 4
    .param p1    # F
    .param p2    # F

    const/high16 v1, 0x40400000

    div-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float v0, v1, v2

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    const/high16 v2, 0x447a0000

    mul-float/2addr v2, v0

    const/high16 v3, 0x3f000000

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method private inCardHolderBounds(FF)Z
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->getLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float v0, p1, v3

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTop()I

    move-result v3

    int-to-float v3, v3

    sub-float v3, p2, v3

    sub-float v1, v3, v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->inCardsBounds(FF)Z

    move-result v3

    return v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v2

    goto :goto_0
.end method

.method private inCardsViewColumn(F)Z
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reportCollapseProgress(F)V
    .locals 2
    .param p1    # F

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    div-float v0, p1, v1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onCardListCollapseProgress(F)V

    :cond_0
    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapseMethod:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onCardListCollapsed(Ljava/lang/String;)V

    :cond_1
    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapseMethod:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expandMethod:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onCardListExpanded(Ljava/lang/String;)V

    :cond_2
    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expandMethod:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private startAnimation(IIFILandroid/animation/TimeInterpolator;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # F
    .param p4    # I
    .param p5    # Landroid/animation/TimeInterpolator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v0

    sub-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->animationThreshold:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p3, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iput p4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->pendingState:I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/videos/tagging/CardsView;->setTranslationY(F)V

    invoke-direct {p0, p4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    goto :goto_0
.end method


# virtual methods
.method public collapse(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    sget-object v1, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    sget-object v1, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V

    return-void
.end method

.method public isExpanded()Z
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->pendingState:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1
    .param p1    # Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->reportCollapseProgress(F)V

    :cond_0
    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->isAnyCardShown()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v4, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    int-to-float v4, v4

    cmpl-float v4, p4, v4

    if-ltz v4, :cond_2

    const/4 v0, 0x1

    :cond_0
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-eq v4, v3, :cond_1

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    int-to-float v4, v4

    cmpl-float v4, p4, v4

    if-ltz v4, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v4

    sub-float v1, v2, v4

    invoke-direct {p0, v1, p4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->getFlingAnimationDuration(FF)I

    move-result v2

    sget-object v4, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    const-string v5, "swipeCards"

    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V

    move v2, v3

    :cond_2
    :goto_0
    return v2

    :cond_3
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v4, p4, v4

    if-gtz v4, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v1

    invoke-direct {p0, v1, p4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->getFlingAnimationDuration(FF)I

    move-result v2

    sget-object v4, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    const-string v5, "swipeCards"

    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V

    move v2, v3

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v4, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v3, v0

    :cond_0
    :goto_1
    return v3

    :pswitch_1
    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->inCardsViewColumn(F)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/CardsView;->getScrollY()I

    move-result v4

    if-nez v4, :cond_2

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v1, v4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->inCardHolderBounds(FF)Z

    move-result v4

    if-eqz v4, :cond_4

    move v3, v2

    goto :goto_1

    :cond_4
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v4, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    :cond_5
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v4, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->inCardsViewColumn(F)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v3, :cond_2

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    cmpl-float v3, p4, v5

    if-ltz v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    :cond_2
    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-eq v3, v2, :cond_3

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v3, v4, :cond_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v3

    sub-float v0, v3, p4

    cmpg-float v3, v0, v5

    if-gez v3, :cond_6

    const/4 v0, 0x0

    :cond_4
    :goto_1
    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v3, v2, :cond_5

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    cmpl-float v3, v0, v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    :cond_5
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->setTranslationY(F)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->reportCollapseProgress(F)V

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_4

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    goto :goto_1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v2, :cond_1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v2, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->inCardHolderBounds(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "clickCards"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    const/high16 v4, 0x40000000

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(Ljava/lang/String;)V

    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "swipeCards"

    goto :goto_1

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v2, :cond_3

    const-string v1, "swipeCards"

    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(Ljava/lang/String;)V

    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setListener(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    return-void
.end method

.method public updateMaxTranslationY()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTopInset()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v3

    add-int v1, v2, v3

    const/4 v2, 0x0

    sub-int v3, v0, v1

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapsedHeight:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->state:I

    sparse-switch v2, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->setTranslationY(F)V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->getDuration()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->getCurrentPlayTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    sget-object v3, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method
