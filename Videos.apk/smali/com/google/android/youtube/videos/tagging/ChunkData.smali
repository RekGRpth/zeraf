.class public Lcom/google/android/youtube/videos/tagging/ChunkData;
.super Ljava/lang/Object;
.source "ChunkData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;
    }
.end annotation


# instance fields
.field private final keyframeTimes:[I

.field private final keyframes:[[Lcom/google/android/youtube/videos/tagging/Tag;


# direct methods
.method constructor <init>([I[[Lcom/google/android/youtube/videos/tagging/Tag;)V
    .locals 2
    .param p1    # [I
    .param p2    # [[Lcom/google/android/youtube/videos/tagging/Tag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "keyframeTimes cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "keyframes cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "keyframeTimes and keyframes length mismatch"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/ChunkData;->keyframeTimes:[I

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/ChunkData;->keyframes:[[Lcom/google/android/youtube/videos/tagging/Tag;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getActiveTags(ILjava/util/List;I)V
    .locals 5
    .param p1    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/Tag;",
            ">;I)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/ChunkData;->keyframeTimes:[I

    invoke-static {v4, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-gez v1, :cond_1

    xor-int/lit8 v4, v1, -0x1

    add-int/lit8 v1, v4, -0x1

    if-gez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/ChunkData;->keyframes:[[Lcom/google/android/youtube/videos/tagging/Tag;

    aget-object v3, v4, v1

    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    aget-object v4, v3, v0

    invoke-virtual {v4, p1}, Lcom/google/android/youtube/videos/tagging/Tag;->getFirstTagAt(I)Lcom/google/android/youtube/videos/tagging/Tag;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/Tag;->interpolatesIn()Z

    move-result v4

    if-nez v4, :cond_2

    iget v4, v2, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    add-int/2addr v4, p3

    if-gt v4, p1, :cond_3

    :cond_2
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
