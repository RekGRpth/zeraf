.class Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

.field final synthetic val$this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->val$this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onImageRequestError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->access$500(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->onError(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onImageResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->access$400(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$3;->onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method
