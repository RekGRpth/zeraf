.class public Lcom/google/android/youtube/videos/tagging/CardsView;
.super Landroid/widget/ScrollView;
.source "CardsView.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
.implements Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;
    }
.end annotation


# static fields
.field private static final PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;


# instance fields
.field private final cardBounds:Landroid/graphics/Rect;

.field private cardDismissListener:Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

.field private final cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

.field private cardPendingPulseAnimation:Landroid/view/View;

.field private final cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private cardsShown:I

.field private final inflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/CardsView;->PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setOnDismissListener(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->addView(Landroid/view/View;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/CardsView;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->maybePulseCard()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/tagging/CardsView;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/CardsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    return-object v0
.end method

.method private insertAt(Ljava/util/List;I)V
    .locals 5
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    add-int v2, p2, v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private maybePulseCard()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x2

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardBounds:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTopInset()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getTranslationY()F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v3, v9, v4, v5, v6}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v3, v8, [Landroid/animation/Animator;

    const-string v4, "scaleX"

    new-array v5, v8, [F

    fill-array-data v5, :array_0

    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v3, v9

    const/4 v4, 0x1

    const-string v5, "scaleY"

    new-array v6, v8, [F

    fill-array-data v6, :array_1

    invoke-static {v1, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v3, 0x12c

    invoke-virtual {v0, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    sget-object v3, Lcom/google/android/youtube/videos/tagging/CardsView;->PULSE_ANIMATION_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x3f866666
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f866666
        0x3f800000
    .end array-data
.end method

.method private onCardHiddenOrRemoved(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardDismissListener:Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardDismissListener:Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;->onCardDismissed(Landroid/view/View;)V

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardDismissListener:Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;->onAllCardsDismissed()V

    :cond_0
    return-void
.end method

.method private scrollToInternal(I)V
    .locals 10
    .param p1    # I

    const/16 v9, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x0

    add-int/lit8 v2, p1, -0x1

    :goto_0
    if-ltz v2, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v9, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v7

    sub-int v4, v6, v7

    :cond_0
    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getScrollY()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    if-lt v6, v7, :cond_4

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_1

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v6, v5}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setLayoutTransitionsEnabled(Z)V

    :cond_1
    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-ne v6, v9, :cond_2

    iget v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardPendingPulseAnimation:Landroid/view/View;

    move v1, v4

    new-instance v5, Lcom/google/android/youtube/videos/tagging/CardsView$1;

    invoke-direct {v5, p0, v1, v3}, Lcom/google/android/youtube/videos/tagging/CardsView$1;-><init>(Lcom/google/android/youtube/videos/tagging/CardsView;IZ)V

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/tagging/CardsView;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_4
    move v3, v5

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    return-void
.end method

.method public dismiss(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Card not in the view hierarchy of this CardsView"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->onViewDismissed(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->dismissView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public getTopInset()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public hide(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->onCardHiddenOrRemoved(Landroid/view/View;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public inCardsBounds(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public inflate(I)Landroid/view/View;
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->inflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public insertAfter(Ljava/util/List;Landroid/view/View;)V
    .locals 4
    .param p2    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    :cond_2
    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, p1, v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->insertAt(Ljava/util/List;I)V

    const/4 v1, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->scrollTo(Landroid/view/View;)Z

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public isAnyCardShown()Z
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->maybePulseCard()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->inCardsBounds(FF)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onViewDismissed(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->onCardHiddenOrRemoved(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->hide(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public scrollTo(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    const-string v1, "card cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->scrollToInternal(I)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public scrollTo(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;)Z
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    const/4 v5, 0x0

    const-string v4, "knowledgeEntity cannot be null"

    invoke-static {p1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, -0x1

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Lcom/google/android/youtube/videos/tagging/CardTag;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/google/android/youtube/videos/tagging/CardTag;

    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v1

    :cond_0
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    move v4, v5

    :goto_1
    return v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/tagging/CardsView;->scrollToInternal(I)V

    add-int/lit8 v1, v2, 0x1

    :goto_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cards:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Lcom/google/android/youtube/videos/tagging/CardTag;

    if-eqz v4, :cond_3

    check-cast v3, Lcom/google/android/youtube/videos/tagging/CardTag;

    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v6, 0x8

    if-ne v4, v6, :cond_3

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardsShown:I

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public setCardDismissListener(Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardDismissListener:Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;

    return-void
.end method

.method public setChangeAppearingAnimatorEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setChangeAppearingAnimatorEnabled(Z)V

    return-void
.end method

.method public setTopInset(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v0, v1, p1, v1, v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setPadding(IIII)V

    return-void
.end method

.method public setTranslationY(F)V
    .locals 0
    .param p1    # F

    invoke-super {p0, p1}, Landroid/widget/ScrollView;->setTranslationY(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->maybePulseCard()V

    return-void
.end method

.method public show(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->clear()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->insertAt(Ljava/util/List;I)V

    return-void
.end method
