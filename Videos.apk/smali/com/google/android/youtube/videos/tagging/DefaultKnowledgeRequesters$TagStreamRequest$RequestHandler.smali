.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
        "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;-><init>()V

    return-void
.end method

.method private decryptResponse([BLjava/lang/String;Ljava/lang/String;)[B
    .locals 10
    .param p1    # [B
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/16 v8, 0x10

    const/16 v7, 0x8

    :try_start_0
    invoke-static {p2, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-direct {p0, v0, v8}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->trimOrPad([BI)[B

    move-result-object v5

    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "AES"

    invoke-direct {v6, v5, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    if-nez p3, :cond_0

    new-array v3, v8, [B

    :goto_0
    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    :try_start_1
    const-string v7, "AES/CBC/PKCS5Padding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    const/4 v7, 0x2

    :try_start_2
    invoke-virtual {v1, v7, v6, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_2} :catch_4

    :try_start_3
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljavax/crypto/BadPaddingException; {:try_start_3 .. :try_end_3} :catch_7

    move-result-object v7

    return-object v7

    :catch_0
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot base64-decode video ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-direct {p0, v7, v8}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->trimOrPad([BI)[B

    move-result-object v3

    goto :goto_0

    :catch_1
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_2
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_3
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_4
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_5
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_6
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_7
    move-exception v2

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method

.method private getParser(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    iget-object v2, p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->contentLanguage:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v1, "No locale in request and Content-Language not set in response."

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Server not returning Content-Language; assuming "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->content:[B

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->decryptResponse([BLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v6

    new-instance v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoItag:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parse()Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method

.method private trimOrPad([BI)[B
    .locals 3
    .param p1    # [B
    .param p2    # I

    const/4 v2, 0x0

    array-length v1, p1

    if-ne v1, p2, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-array v0, p2, [B

    array-length v1, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v0

    goto :goto_0
.end method


# virtual methods
.method public createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    iget-boolean v0, p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->isNotFound:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    invoke-direct {p0, v0, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->getParser(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method

.method public createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->access$500(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v0, p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public resolveUnderlyingRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;Ljava/lang/Exception;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagStreamParser;
    .param p3    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p2, :cond_0

    const/16 v0, 0x130

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p2

    :cond_0
    throw p3
.end method

.method public bridge synthetic resolveUnderlyingRequestError(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;->resolveUnderlyingRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;Ljava/lang/Exception;)Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    move-result-object v0

    return-object v0
.end method
