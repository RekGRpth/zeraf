.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AssetResourceBatchingRequester"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final apiAssetsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "apiRequester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "[B>;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->request(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
