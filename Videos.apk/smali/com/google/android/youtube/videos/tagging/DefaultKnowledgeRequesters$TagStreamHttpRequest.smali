.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
.super Lcom/google/android/youtube/videos/api/Request;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamHttpRequest"
.end annotation


# instance fields
.field public final ifModifiedSince:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/Request;-><init>(Ljava/lang/String;)V

    const-string v0, "url cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->ifModifiedSince:Ljava/lang/String;

    return-void
.end method
