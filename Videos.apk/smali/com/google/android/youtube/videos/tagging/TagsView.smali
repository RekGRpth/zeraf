.class public Lcom/google/android/youtube/videos/tagging/TagsView;
.super Landroid/widget/FrameLayout;
.source "TagsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;,
        Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;,
        Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;,
        Lcom/google/android/youtube/videos/tagging/TagsView$TagView;,
        Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;
    }
.end annotation


# instance fields
.field private final activatedTagColor:I

.field private activatedView:Landroid/view/View;

.field private final boundingBox:Landroid/graphics/RectF;

.field private downX:F

.field private downY:F

.field private inSpotlightMode:Z

.field private moved:Z

.field private onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

.field private final shadowColor:I

.field private final shadowOverflow:F

.field private final shadowRadius:F

.field private final shadowYOffset:F

.field private final tagColor:I

.field private final tagPaint:Landroid/graphics/Paint;

.field private final tagShapeAlpha:F

.field private final tagStrokeWidth:F

.field private final tagViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagsView$TagView;",
            ">;"
        }
    .end annotation
.end field

.field private taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final textHeight:F

.field private final textPaint:Landroid/graphics/Paint;

.field private final textYOffset:F

.field private final touchSlop:I

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/tagging/TagsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x437f0000

    div-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagShapeAlpha:F

    const v2, 0x7f08001c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagColor:I

    const v2, 0x7f08001b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedTagColor:I

    const v2, 0x7f090029

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F

    const v2, 0x7f08001d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowColor:I

    const v2, 0x7f09002a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowRadius:F

    const v2, 0x7f09002b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowRadius:F

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowOverflow:F

    const v2, 0x7f09002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textHeight:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textHeight:F

    const v3, 0x7f09002e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textYOffset:F

    const v2, 0x7f08001e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->touchSlop:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textHeight:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowRadius:F

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/TagsView;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->createChildren()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/tagging/TagsView;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagColor:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagShapeAlpha:F

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textYOffset:F

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->textHeight:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowOverflow:F

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/tagging/TagsView;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowColor:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/tagging/TagsView;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->shadowRadius:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/tagging/TagsView;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/TagsView;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedTagColor:I

    return v0
.end method

.method private clearViews()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->removeAllViews()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->inSpotlightMode:Z

    return-void
.end method

.method private createChildren()V
    .locals 21

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->clearViews()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayWidth:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayHeight:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getWidth()I

    move-result v17

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getHeight()I

    move-result v17

    if-nez v17, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    new-instance v5, Landroid/graphics/RectF;

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayWidth:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getWidth()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayHeight:I

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getHeight()I

    move-result v19

    sub-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000

    div-float v18, v18, v19

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    const/4 v10, 0x1

    const/4 v6, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v7, v15, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v0, v15, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    new-instance v14, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;-><init>(Lcom/google/android/youtube/videos/tagging/TagsView;Landroid/content/Context;)V

    const/4 v8, 0x0

    iget-object v0, v7, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_3

    new-instance v8, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v8, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;-><init>(Lcom/google/android/youtube/videos/tagging/TagsView;Landroid/content/Context;)V

    invoke-virtual {v8, v15, v14, v5}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->init(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/youtube/videos/tagging/TagsView;->updateOverlaps(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;Ljava/util/List;)V

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v14, v15, v8, v5}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->init(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v10, 0x1

    invoke-virtual {v14, v10}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setId(I)V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setFocusable(Z)V

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v7, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setContentDescription(Ljava/lang/CharSequence;)V

    move v10, v11

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_AREA:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v6, v13, -0x1

    :goto_3
    if-ltz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    add-int/lit8 v17, v13, -0x1

    sub-int v17, v17, v6

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->addView(Landroid/view/View;I)V

    invoke-virtual {v14}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->getLabelView()Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    move-result-object v8

    if-eqz v8, :cond_5

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/youtube/videos/tagging/TagsView;->addView(Landroid/view/View;)V

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibleIfNoVisibleOverlaps()V

    :cond_5
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_Y:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v12, 0x0

    const/4 v6, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/View;

    if-eqz v12, :cond_7

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getId()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setNextFocusUpId(I)V

    :cond_7
    move-object/from16 v12, v16

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_X:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v12, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    if-eqz v12, :cond_9

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->getId()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setNextFocusLeftId(I)V

    :cond_9
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->startAppearAnimation(I)V

    move-object/from16 v12, v16

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_AREA:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    invoke-static/range {v17 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method private setActivatedView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->activatedView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    :cond_1
    return-void
.end method

.method private updateOverlaps(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;Ljava/util/List;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->intersects(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->addOverlappingLabelView(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;)V

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->addOverlappingLabelView(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->clearViews()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    return-void
.end method

.method public clearSpotlight()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->inSpotlightMode:Z

    if-eqz v2, :cond_1

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->inSpotlightMode:Z

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    instance-of v2, v1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibleIfNoVisibleOverlaps()V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;->onTagClick(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setActivated(Z)V

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/tagging/TagsView$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/tagging/TagsView$1;-><init>(Lcom/google/android/youtube/videos/tagging/TagsView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v11, 0x3

    if-ne v0, v11, :cond_1

    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    :cond_0
    :goto_0
    return v10

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    const/4 v5, 0x0

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayWidth:I

    if-eqz v11, :cond_2

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayHeight:I

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayWidth:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getWidth()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v6, v1, v11

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayHeight:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v7, v2, v11

    const/4 v3, 0x0

    :goto_1
    iget-object v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v3, v11, :cond_2

    iget-object v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->getVisibility()I

    move-result v11

    if-nez v11, :cond_5

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F

    invoke-virtual {v4, v6, v7, v11}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->hitTest(FFF)Z

    move-result v11

    if-eqz v11, :cond_5

    move-object v5, v4

    :cond_2
    packed-switch v0, :pswitch_data_0

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->moved:Z

    if-nez v8, :cond_4

    iget v8, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->downX:F

    sub-float v8, v1, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->touchSlop:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-gtz v8, :cond_3

    iget v8, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->downY:F

    sub-float v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->touchSlop:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-lez v8, :cond_8

    :cond_3
    move v8, v10

    :goto_2
    iput-boolean v8, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->moved:Z

    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_0
    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    iget-object v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    if-eqz v11, :cond_0

    if-nez v5, :cond_6

    iget-boolean v11, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->moved:Z

    if-nez v11, :cond_0

    :cond_6
    invoke-virtual {p0, v9}, Lcom/google/android/youtube/videos/tagging/TagsView;->playSoundEffect(I)V

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    if-nez v5, :cond_7

    :goto_3
    invoke-interface {v9, v8}, Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;->onTagClick(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v8

    goto :goto_3

    :pswitch_1
    iput v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->downX:F

    iput v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->downY:F

    iput-boolean v9, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->moved:Z

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/tagging/TagsView;->setActivatedView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_8
    move v8, v9

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setOnTagClickListener(Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->onTagClickListener:Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;

    return-void
.end method

.method public setSpotlight(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)Z
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v2, "taggedKnowledgeEntity cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v4, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->inSpotlightMode:Z

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->tagViews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    instance-of v2, v1, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    :cond_1
    move v3, v4

    :cond_2
    return v3
.end method

.method public show(Ljava/util/List;II)V
    .locals 1
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;II)V"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->taggedKnowledgeEntities:Ljava/util/List;

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayWidth:I

    iput p3, p0, Lcom/google/android/youtube/videos/tagging/TagsView;->videoDisplayHeight:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->createChildren()V

    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method
