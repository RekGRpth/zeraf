.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;
    }
.end annotation


# instance fields
.field private final image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;)V

    const-string v0, "image cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    return-object v0
.end method


# virtual methods
.method public toFileName()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->baseFileNameWithCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iget-object v1, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->localImageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
