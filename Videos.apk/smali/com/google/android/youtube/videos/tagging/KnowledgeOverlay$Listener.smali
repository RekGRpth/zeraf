.class public interface abstract Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;
.super Ljava/lang/Object;
.source "KnowledgeOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCardDismissed(Lcom/google/android/youtube/videos/tagging/CardTag;)V
.end method

.method public abstract onCardListCollapseProgress(F)V
.end method

.method public abstract onCardListCollapsed(Ljava/lang/String;)V
.end method

.method public abstract onCardListExpanded(Ljava/lang/String;)V
.end method

.method public abstract onCardsShown(Z)V
.end method

.method public abstract onClickOutsideTags()V
.end method

.method public abstract onExpandRecentActors()V
.end method

.method public abstract onReset()V
.end method
