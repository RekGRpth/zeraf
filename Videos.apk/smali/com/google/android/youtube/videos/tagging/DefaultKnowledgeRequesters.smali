.class Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$1;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAssetResourcesRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "[B>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;)V

    return-object v0
.end method

.method public static createKnowledgeComponentRequester(Lcom/google/android/youtube/videos/store/AbstractFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;
    .locals 7
    .param p1    # Z
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;",
            "E:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "TS;>;ZJ",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TT;TS;>;",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;)",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester",
            "<TR;TE;TT;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;-><init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)V

    return-object v0
.end method

.method public static createTagStreamHttpRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;)Lcom/google/android/youtube/core/async/Requester;
    .locals 4
    .param p0    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p1    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;

    invoke-direct {v0, v3}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$1;)V

    new-instance v1, Lcom/google/android/youtube/core/async/HttpRequester;

    new-instance v2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$1;)V

    invoke-direct {v1, p1, v3, v2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->create(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    move-result-object v0

    return-object v0
.end method
