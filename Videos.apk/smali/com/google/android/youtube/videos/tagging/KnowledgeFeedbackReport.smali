.class public Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;
.super Ljava/lang/Object;
.source "KnowledgeFeedbackReport.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;


# instance fields
.field public final account:Ljava/lang/String;

.field public final incorrectlyTaggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field public final knowledgePositionMillis:I

.field private final productId:I

.field public final tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

.field private final typeId:I


# direct methods
.method public constructor <init>(IILjava/lang/String;Lcom/google/android/youtube/videos/tagging/TagStream;ILjava/util/List;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/tagging/TagStream;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/TagStream;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->productId:I

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->typeId:I

    const-string v0, "account cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->account:Ljava/lang/String;

    const-string v0, "tagStream cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TagStream;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iput p5, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    const-string v0, "incorrectlyTaggedKnowledgeEntities cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    return-void
.end method

.method private appendError(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " (sid="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->splitId:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private getTimeString()Ljava/lang/String;
    .locals 7

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    div-int/lit16 v3, v3, 0x3e8

    rem-int/lit8 v2, v3, 0x3c

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    const v4, 0xea60

    div-int/2addr v3, v4

    rem-int/lit8 v1, v3, 0x3c

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    const v4, 0x36ee80

    div-int v0, v3, v4

    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public buildMessage()Lcom/google/protobuf/MessageLite;
    .locals 9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    invoke-direct {p0, v6, v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->appendError(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V

    const/4 v3, 0x1

    :goto_0
    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_0

    const-string v6, "; "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    invoke-direct {p0, v6, v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->appendError(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Ljava/lang/StringBuilder;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->getTimeString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TF "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " @"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " f="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " itag="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream;->videoItag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ts="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->newBuilder()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescriptionTranslated(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "videoId"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "videoTitle"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "contentLanguage"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "lastModified"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->lastModified:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "videoItag"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->videoItag:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "videoTimestamp"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v8, v8, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "knowledgePositionMillis"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->knowledgePositionMillis:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "knowledgePositionString"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    const-string v8, "errors"

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    invoke-static {}, Lcom/google/protos/userfeedback/ExtensionSubmit;->newBuilder()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->productId:I

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setProductId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->typeId:I

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setTypeId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v6

    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->newBuilder()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setWebData(Lcom/google/protos/userfeedback/WebData$Builder;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setCommonData(Lcom/google/protos/userfeedback/CommonData$Builder;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->build()Lcom/google/protos/userfeedback/ExtensionSubmit;

    move-result-object v6

    return-object v6
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;->account:Ljava/lang/String;

    return-object v0
.end method
