.class public final Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
.super Ljava/lang/Object;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# instance fields
.field private final cropRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field public final localImageId:Ljava/lang/String;

.field public final source:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<[I>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->localImageId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->source:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCropRegion(ILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    int-to-float v2, v2

    const/4 v3, 0x2

    aget v3, v0, v3

    int-to-float v3, v3

    const/4 v4, 0x3

    aget v4, v0, v4

    int-to-float v4, v4

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    return-object p2
.end method

.method public getSquareCropRegionIndex()I
    .locals 6

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    if-nez v3, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->cropRegions:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v3, 0x2

    aget v3, v0, v3

    const/4 v4, 0x0

    aget v4, v0, v4

    sub-int/2addr v3, v4

    const/4 v4, 0x3

    aget v4, v0, v4

    const/4 v5, 0x1

    aget v5, v0, v5

    sub-int/2addr v4, v5

    if-eq v3, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method
