.class public final Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;
.super Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Person"
.end annotation


# instance fields
.field public final characterNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final dateOfBirth:Ljava/lang/String;

.field public final dateOfDeath:Ljava/lang/String;

.field public final filmography:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            ">;"
        }
    .end annotation
.end field

.field public final placeOfBirth:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/util/Collection;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[ILjava/util/Collection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p5    # [I
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "[I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;-><init>(ILjava/util/Collection;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[I)V

    invoke-static {p6}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    iput-object p7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    return-void
.end method
