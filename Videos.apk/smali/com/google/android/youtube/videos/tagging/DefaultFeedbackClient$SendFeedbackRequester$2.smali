.class Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->onGotAutoLoginHeaderOrToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->onError(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Void;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .param p2    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->onResponse(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Void;)V

    return-void
.end method
