.class public Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;
.super Lcom/google/android/youtube/core/client/BaseClient;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/KnowledgeClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;,
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final desiredMoviePosterHeight:I

.field private final desiredMoviePosterWidth:I

.field private final desiredShowPosterHeight:I

.field private final desiredShowPosterWidth:I

.field private final feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

.field private final filmographyResourcesPinningRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final filmographyResourcesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final imagePinningRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final imageRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final tagStreamPinningRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private final tagStreamRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;IIIIILjava/lang/String;)V
    .locals 24
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/Config;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p5    # Lorg/apache/http/client/HttpClient;
    .param p8    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I
    .param p13    # I
    .param p14    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/youtube/videos/Config;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;",
            "Lcom/google/android/youtube/videos/utils/BitmapLruCache;",
            "IIIII",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/BaseClient;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    const-string v3, "config cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->config:Lcom/google/android/youtube/videos/Config;

    const-string v3, "globalBitmapCache cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move/from16 v0, p10

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterWidth:I

    move/from16 v0, p11

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterHeight:I

    move/from16 v0, p12

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterWidth:I

    move/from16 v0, p13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterHeight:I

    invoke-interface/range {p2 .. p2}, Lcom/google/android/youtube/videos/Config;->knowledgeRecheckDataAfterMillis()J

    move-result-wide v6

    invoke-static/range {p4 .. p5}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;->createTagStreamHttpRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;

    invoke-direct {v9}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->tagStreamRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v4, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->tagStreamPinningRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-static/range {p6 .. p6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;->createAssetResourcesRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v15

    new-instance v16, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;-><init>()V

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v10, p0

    move-wide v13, v6

    invoke-direct/range {v10 .. v16}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v11, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;)V

    const/4 v12, 0x1

    move-object/from16 v10, p0

    move-wide v13, v6

    invoke-direct/range {v10 .. v16}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesPinningRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v23, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;

    move-object/from16 v0, v23

    move/from16 v1, p9

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;-><init>(I)V

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v17, p0

    move-wide/from16 v20, v6

    move-object/from16 v22, p7

    invoke-direct/range {v17 .. v23}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v18, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;-><init>(Landroid/content/Context;)V

    const/16 v19, 0x0

    move-object/from16 v17, p0

    move-wide/from16 v20, v6

    move-object/from16 v22, p7

    invoke-direct/range {v17 .. v23}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->imagePinningRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p14

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/Config;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterWidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredMoviePosterHeight:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterWidth:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->desiredShowPosterHeight:I

    return v0
.end method

.method private createAsyncKnowledgeComponentRequester(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p2    # Z
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;",
            "E:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$PinnedFileStore",
            "<TS;>;ZJ",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TT;TS;>;",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
            "<TR;TE;TT;TS;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static/range {p1 .. p6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;->createKnowledgeComponentRequester(Lcom/google/android/youtube/videos/store/AbstractFileStore;ZJLcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequester;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v1

    return-object v1
.end method

.method private requestKnowledgeBundleInternal(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Z)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p6    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->config:Lcom/google/android/youtube/videos/Config;

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->from(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/Config;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    move-result-object v7

    new-instance v6, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    iget-object v0, v7, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->userCountry:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-direct {v6, p1, v0, v1, p5}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Lcom/google/android/youtube/videos/utils/BitmapLruCache;Lcom/google/android/youtube/core/async/Requester;)V

    invoke-virtual {v6, p6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->setAlwaysRequest(Z)V

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)V

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->start(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)V

    return-void
.end method


# virtual methods
.method public getFeedbackClient()Lcom/google/android/youtube/videos/tagging/FeedbackClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    return-object v0
.end method

.method public requestKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->tagStreamRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->requestKnowledgeBundleInternal(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Z)V

    return-void
.end method

.method public requestPinnedKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->tagStreamPinningRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->filmographyResourcesPinningRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->imagePinningRequester:Lcom/google/android/youtube/core/async/Requester;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;->requestKnowledgeBundleInternal(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Z)V

    return-void
.end method
