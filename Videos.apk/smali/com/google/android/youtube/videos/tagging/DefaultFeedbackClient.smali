.class public Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/FeedbackClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;,
        Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;,
        Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;,
        Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;,
        Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;
    }
.end annotation


# instance fields
.field private final accountManager:Landroid/accounts/AccountManager;

.field private final sendFeedbackRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/Config;Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/Config;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v8, "context cannot be null"

    invoke-static {p1, v8}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "config cannot be null"

    invoke-static {p2, v8}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;->accountManager:Landroid/accounts/AccountManager;

    new-instance v0, Lorg/apache/http/impl/client/BasicCookieStore;

    invoke-direct {v0}, Lorg/apache/http/impl/client/BasicCookieStore;-><init>()V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Mozilla/5.0 (Linux; Android 4.1.1; Nexus 7 Build/JRO03D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Safari/535.19 com.google.android.videos/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createCookieStoreHttpClient(Ljava/lang/String;Lorg/apache/http/client/CookieStore;)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    new-instance v5, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;

    invoke-interface {p2}, Lcom/google/android/youtube/videos/Config;->feedbackTokenUrl()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/youtube/core/async/HttpRequester;

    invoke-direct {v6, v3, v5, v5}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    new-instance v4, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;

    invoke-interface {p2}, Lcom/google/android/youtube/videos/Config;->feedbackSubmitUrlTemplate()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/youtube/core/async/HttpRequester;

    invoke-direct {v2, v3, v4, v4}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v8, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;->accountManager:Landroid/accounts/AccountManager;

    invoke-direct {v8, v9, v0, v6, v2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;-><init>(Landroid/accounts/AccountManager;Lorg/apache/http/client/CookieStore;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V

    invoke-static {v1, v8}, Lcom/google/android/youtube/core/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;->sendFeedbackRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method


# virtual methods
.method public sendFeedback(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;->sendFeedbackRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
