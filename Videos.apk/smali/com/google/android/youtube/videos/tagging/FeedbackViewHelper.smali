.class Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;
.super Ljava/lang/Object;
.source "FeedbackViewHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;
    }
.end annotation


# instance fields
.field private final cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

.field private currentIndex:I

.field private final feedbackBox:Landroid/view/View;

.field private final feedbackView:Landroid/view/View;

.field private final incorrectlyTaggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;

.field private final noButton:Landroid/view/View;

.field private final questionView:Landroid/widget/TextView;

.field private taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

.field private final yesButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/tagging/TagsView;Lcom/google/android/youtube/videos/tagging/CardsView;Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/videos/tagging/TagsView;
    .param p4    # Lcom/google/android/youtube/videos/tagging/CardsView;
    .param p5    # Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "feedbackView cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "tagsView cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TagsView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    const-string v0, "cardsView cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/CardsView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const-string v0, "listener cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->listener:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;

    const v0, 0x7f070035

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070036

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    const v0, 0x7f070037

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070038

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    return-void
.end method

.method private askAboutCurrentTaggedKnowledgeEntity()V
    .locals 7

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0101

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v6, v6, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->setSpotlight(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    :cond_0
    return-void
.end method


# virtual methods
.method public collectFeedback(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v5, 0x96

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->askAboutCurrentTaggedKnowledgeEntity()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method public isCollectingFeedback()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->listener:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;->onFeedbackCollected(Ljava/util/List;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    if-eq p1, v0, :cond_3

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->askAboutCurrentTaggedKnowledgeEntity()V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 7

    const-wide/16 v5, 0x96

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->clearSpotlight()V

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->currentIndex:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const-string v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method public setPadding(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method
