.class public Lcom/google/android/youtube/videos/tagging/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mInitialTouchPos:F

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private mSwipeDirection:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;FF)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mMinAlpha:F

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput p3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDensityScale:F

    iput p4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mPagingTouchSlop:F

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/SwipeHelper;)Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/tagging/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SwipeHelper;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, "translationY"

    goto :goto_0
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 7
    .param p1    # Landroid/view/View;

    const/high16 v6, 0x3f800000

    const v5, 0x3e19999a

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    const v4, 0x3f266666

    mul-float v0, v4, v3

    const/high16 v2, 0x3f800000

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v1

    mul-float v4, v3, v5

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    mul-float v4, v3, v5

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v6, v4

    :cond_0
    :goto_0
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    :cond_1
    const v4, 0x3f59999a

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    mul-float v4, v3, v5

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v6, v4

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getPos(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getTranslation(Landroid/view/View;)F
    .locals 1
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public cancelOngoingDrag()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->onSnapBackCompleted(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    :cond_1
    return-void
.end method

.method public dismissChild(Landroid/view/View;F)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    cmpg-float v4, p2, v6

    if-ltz v4, :cond_1

    cmpl-float v4, p2, v6

    if-nez v4, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpg-float v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    cmpl-float v4, p2, v6

    if-nez v4, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mSwipeDirection:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v4

    neg-float v3, v4

    :goto_0
    const/16 v2, 0x96

    cmpl-float v4, p2, v6

    if-eqz v4, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x447a0000

    mul-float/2addr v4, v5

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    div-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v4, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v4, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;

    invoke-direct {v4, p0, p1, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;-><init>(Lcom/google/android/youtube/videos/tagging/SwipeHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v4, Lcom/google/android/youtube/videos/tagging/SwipeHelper$2;

    invoke-direct {v4, p0, v1, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$2;-><init>(Lcom/google/android/youtube/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    goto :goto_0

    :cond_3
    const/16 v2, 0x4b

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    sub-float v1, v2, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mPagingTouchSlop:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    goto :goto_0

    :pswitch_2
    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDragging:Z

    if-nez v14, :cond_0

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    :goto_0
    return v14

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    const/4 v14, 0x1

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mInitialTouchPos:F

    sub-float v6, v14, v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v12

    const v14, 0x3e19999a

    mul-float v9, v14, v12

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v12

    if-ltz v14, :cond_4

    const/4 v14, 0x0

    cmpl-float v14, v6, v14

    if-lez v14, :cond_3

    move v6, v9

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCanCurrViewBeDimissed:Z

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-virtual {v14, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    :cond_3
    neg-float v6, v9

    goto :goto_2

    :cond_4
    div-float v14, v6, v12

    const v15, 0x3fc90fdb

    mul-float/2addr v14, v15

    invoke-static {v14}, Landroid/util/FloatMath;->sin(F)F

    move-result v14

    mul-float v6, v9, v14

    goto :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_1

    const/high16 v14, 0x44fa0000

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDensityScale:F

    mul-float v10, v14, v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v15, 0x3e8

    invoke-virtual {v14, v15, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    const/high16 v14, 0x42c80000

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mDensityScale:F

    mul-float v8, v14, v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    float-to-double v14, v14

    const-wide v16, 0x3fe3333333333333L

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v18

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    cmpl-double v14, v14, v16

    if-lez v14, :cond_6

    const/4 v4, 0x1

    :goto_3
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v8

    if-lez v14, :cond_9

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v15

    cmpl-float v14, v14, v15

    if-lez v14, :cond_9

    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-lez v14, :cond_7

    const/4 v14, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v15

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_8

    const/4 v15, 0x1

    :goto_5
    if-ne v14, v15, :cond_9

    const/4 v5, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-eqz v14, :cond_a

    if-nez v5, :cond_5

    if-eqz v4, :cond_a

    :cond_5
    const/4 v7, 0x1

    :goto_7
    if-eqz v7, :cond_c

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v5, :cond_b

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_1

    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    :cond_7
    const/4 v14, 0x0

    goto :goto_4

    :cond_8
    const/4 v15, 0x0

    goto :goto_5

    :cond_9
    const/4 v5, 0x0

    goto :goto_6

    :cond_a
    const/4 v7, 0x0

    goto :goto_7

    :cond_b
    const/4 v13, 0x0

    goto :goto_8

    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/16 v2, 0x96

    int-to-long v3, v2

    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/google/android/youtube/videos/tagging/SwipeHelper$3;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$3;-><init>(Lcom/google/android/youtube/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v3, Lcom/google/android/youtube/videos/tagging/SwipeHelper$4;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$4;-><init>(Lcom/google/android/youtube/videos/tagging/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
