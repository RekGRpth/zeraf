.class final Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;
.super Ljava/lang/Object;
.source "RecentActorsCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/RecentActorsCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RecentActorsCardItemClickListener"
.end annotation


# instance fields
.field private final cardsToShow:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final onRecentActorsCardClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;

.field private final recentActorsCard:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Ljava/util/List;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->recentActorsCard:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->onRecentActorsCardClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->cardsToShow:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->onRecentActorsCardClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->recentActorsCard:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;->cardsToShow:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;->onRecentActorsCardClick(Landroid/view/View;Ljava/util/List;)V

    return-void
.end method
