.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
        "Landroid/graphics/Bitmap;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private final bytesConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;-><init>()V

    new-instance v0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->bytesConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    return-void
.end method


# virtual methods
.method public createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;[B)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->bytesConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->convertResponse([B)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;[B)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;[B)Landroid/net/Uri;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2    # [B

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    invoke-static {p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->access$600(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest$RequestHandler;->createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;[B)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
