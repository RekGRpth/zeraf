.class final Lcom/google/android/youtube/videos/tagging/SongCards;
.super Lcom/google/android/youtube/videos/tagging/Cards;
.source "SongCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/SongCards$SongSecondaryActionButtonHelper;,
        Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/Cards;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;
    .locals 24
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p3    # Landroid/app/Activity;
    .param p4    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const v4, 0x7f040034

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->albumArt:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    if-eqz v4, :cond_0

    const v4, 0x7f070078

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    const/4 v4, 0x4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v4, 0x7f0a00fb

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    aput-object v9, v5, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->albumArt:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1}, Lcom/google/android/youtube/videos/tagging/SongCards;->requestImage(Landroid/widget/ImageView;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Requester;)V

    :cond_0
    const v4, 0x7f070079

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/google/android/youtube/videos/tagging/SongCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v21, 0x0

    :goto_0
    const v4, 0x7f070077

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v17, 0x0

    :goto_1
    if-eqz v17, :cond_4

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/tagging/SongCards;->getAlbumId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const v4, 0x7f070062

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v4, Lcom/google/android/youtube/videos/tagging/SongCards$SongSecondaryActionButtonHelper;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    move-object/from16 v5, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v4 .. v11}, Lcom/google/android/youtube/videos/tagging/SongCards$SongSecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/SongCards$SongSecondaryActionButtonHelper;->setup()V

    if-eqz v21, :cond_1

    invoke-static/range {v23 .. v23}, Lcom/google/android/youtube/videos/tagging/SongCards;->layout(Landroid/view/View;)V

    invoke-static/range {v21 .. v21}, Lcom/google/android/youtube/videos/tagging/SongCards;->getBottom(Landroid/view/View;)I

    move-result v4

    invoke-virtual {v6}, Landroid/widget/Button;->getTop()I

    move-result v5

    if-le v4, v5, :cond_1

    const/16 v4, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v4, 0x7f070026

    const v5, 0x7f0a00fc

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    aput-object v10, v7, v9

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/google/android/youtube/videos/tagging/SongCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v18

    if-eqz v17, :cond_6

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/CardTag;->forSong(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;)Lcom/google/android/youtube/videos/tagging/CardTag;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v22

    :cond_2
    const v4, 0x7f07007a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/google/android/youtube/videos/tagging/SongCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v21

    goto/16 :goto_0

    :cond_3
    new-instance v17, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/tagging/SongCards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/youtube/videos/tagging/SongCards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    :cond_5
    new-instance v4, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;

    const-string v5, "songCard"

    move-object/from16 v0, p3

    move-object/from16 v1, v19

    move-object/from16 v2, p6

    invoke-direct {v4, v0, v1, v2, v5}, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_6
    new-instance v9, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    const-string v12, "music"

    const-string v15, "songCard"

    move-object/from16 v10, p3

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    invoke-direct/range {v9 .. v15}, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3
.end method

.method private static getAlbumId(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v4

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v5, "id"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "tid"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "/store/music/album"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "song-"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    move-object v0, v4

    goto :goto_0
.end method
