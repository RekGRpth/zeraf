.class public abstract Lcom/google/android/youtube/videos/tagging/Tag;
.super Ljava/lang/Object;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/Tag$1;,
        Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;,
        Lcom/google/android/youtube/videos/tagging/Tag$FaceRectTag;,
        Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;,
        Lcom/google/android/youtube/videos/tagging/Tag$ShapelessTag;,
        Lcom/google/android/youtube/videos/tagging/Tag$TagShape;
    }
.end annotation


# instance fields
.field public final interpolates:Z

.field private interpolatesIn:Z

.field private nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

.field public final splitId:I

.field public final timeMillis:I


# direct methods
.method private constructor <init>(IIZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/tagging/Tag;->interpolates:Z

    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/youtube/videos/tagging/Tag$1;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # Lcom/google/android/youtube/videos/tagging/Tag$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/tagging/Tag;-><init>(IIZ)V

    return-void
.end method

.method static parseFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;IZ)Lcom/google/android/youtube/videos/tagging/Tag;
    .locals 13
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v1, "Tag invalid - no split ID"

    invoke-direct {v0, v1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getCircle()I

    move-result v3

    move v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;-><init>(IIIZLcom/google/android/youtube/videos/tagging/Tag$1;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v6, Lcom/google/android/youtube/videos/tagging/Tag$FaceRectTag;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getFaceRectShape()J

    move-result-wide v9

    move v8, p1

    move v11, p2

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/google/android/youtube/videos/tagging/Tag$FaceRectTag;-><init>(IIJZLcom/google/android/youtube/videos/tagging/Tag$1;)V

    move-object v0, v6

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/youtube/videos/tagging/Tag$ShapelessTag;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v1

    invoke-direct {v0, v1, p1, p2, v5}, Lcom/google/android/youtube/videos/tagging/Tag$ShapelessTag;-><init>(IIZLcom/google/android/youtube/videos/tagging/Tag$1;)V

    goto :goto_0
.end method


# virtual methods
.method public final getFirstTagAt(I)Lcom/google/android/youtube/videos/tagging/Tag;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    if-ge p1, v1, :cond_1

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    if-eq v1, p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->interpolates:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    iget v1, v1, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    if-lt p1, v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/Tag;->getFirstTagAt(I)Lcom/google/android/youtube/videos/tagging/Tag;

    move-result-object p0

    goto :goto_0

    :cond_3
    move-object p0, v0

    goto :goto_0
.end method

.method protected final getNextTag()Lcom/google/android/youtube/videos/tagging/Tag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    return-object v0
.end method

.method public abstract getTagShape(IFFF)Lcom/google/android/youtube/videos/tagging/Tag$TagShape;
.end method

.method protected final interpolate(IFIF)F
    .locals 2
    .param p1    # I
    .param p2    # F
    .param p3    # I
    .param p4    # F

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    if-gt v0, p1, :cond_0

    if-ge p1, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "times must satisfy millis <= atMillis < endMillis"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    sub-float v0, p4, p2

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    sub-int v1, p1, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    sub-int v1, p3, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, p2

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final interpolateAngle(IFIF)F
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # I
    .param p4    # F

    const/high16 v2, 0x43340000

    const/high16 v1, 0x43b40000

    sub-float v0, p4, p2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    add-float/2addr p2, v1

    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/tagging/Tag;->interpolate(IFIF)F

    move-result v0

    rem-float/2addr v0, v1

    return v0

    :cond_1
    sub-float v0, p2, p4

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    add-float/2addr p4, v1

    goto :goto_0
.end method

.method interpolatesIn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->interpolatesIn:Z

    return v0
.end method

.method final setInterpolatesIn(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->interpolatesIn:Z

    return-void
.end method

.method final setNextTag(Lcom/google/android/youtube/videos/tagging/Tag;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/Tag;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    return-void
.end method

.method protected final shouldInterpolate(I)Z
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->interpolates:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->timeMillis:I

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/Tag;->nextTag:Lcom/google/android/youtube/videos/tagging/Tag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
