.class public final Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;
.super Ljava/lang/Object;
.source "SongCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/SongCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "GoToStoreOnClickListener"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final storeUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->storeUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->account:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->storeUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->account:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->activity:Landroid/app/Activity;

    const v2, 0x7f0a0078

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SongCards$GoToStoreOnClickListener;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForSong(Z)V

    return-void
.end method
