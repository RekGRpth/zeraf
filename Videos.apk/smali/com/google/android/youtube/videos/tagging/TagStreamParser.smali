.class final Lcom/google/android/youtube/videos/tagging/TagStreamParser;
.super Ljava/lang/Object;
.source "TagStreamParser.java"


# instance fields
.field private final actorFilmography:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            ">;"
        }
    .end annotation
.end field

.field private final characterNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentLanguage:Ljava/lang/String;

.field private final crops:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private filmographyIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private framesPerSecond:F

.field private header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

.field private final lastModified:Ljava/lang/String;

.field private parseException:Ljava/io/IOException;

.field private parsed:Z

.field private final source:[B

.field private usedReferencedFilmIndices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final videoId:Ljava/lang/String;

.field private final videoItag:Ljava/lang/String;

.field private final videoTimestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->contentLanguage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->lastModified:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoItag:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoTimestamp:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->source:[B

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    return-void
.end method

.method private buildActors(Ljava/util/List;[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Ljava/util/List;Ljava/util/Map;)V
    .locals 21
    .param p2    # [Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;[",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    const/16 v18, 0x0

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getLocalId()I

    move-result v3

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitIdList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "actor."

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v2, v5, v1}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-result-object v6

    :goto_1
    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildAppearances(Lcom/google/protobuf/ByteString;)[I

    move-result-object v7

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getCharacterCount()I

    move-result v14

    const/16 v19, 0x0

    :goto_3
    move/from16 v0, v19

    if-ge v0, v14, :cond_3

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getCharacter(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-virtual {v15}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexCount()I

    move-result v17

    const/16 v20, 0x0

    const/16 v19, 0x0

    :goto_4
    move/from16 v0, v19

    move/from16 v1, v17

    if-ge v0, v1, :cond_5

    const/4 v2, 0x3

    move/from16 v0, v20

    if-ge v0, v2, :cond_5

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndex(I)I

    move-result v16

    if-ltz v16, :cond_4

    move-object/from16 v0, p2

    array-length v2, v0

    move/from16 v0, v16

    if-ge v0, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    aget-object v5, p2, v16

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v20, v20, 0x1

    :cond_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    :cond_5
    new-instance v2, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfBirth()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfDeath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getPlaceOfBirth()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-direct/range {v2 .. v12}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;-><init>(ILjava/util/Collection;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[ILjava/util/Collection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->characterNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->actorFilmography:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method private buildAppearances(Lcom/google/protobuf/ByteString;)[I
    .locals 9
    .param p1    # Lcom/google/protobuf/ByteString;

    const/4 v7, 0x0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffsetCount()I

    move-result v5

    if-nez v5, :cond_1

    move-object v1, v7

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-array v1, v5, [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    :goto_1
    array-length v8, v1

    if-ge v3, v8, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffset(I)I

    move-result v4

    if-gez v4, :cond_2

    move-object v1, v7

    goto :goto_0

    :cond_2
    add-int/2addr v6, v4

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v8

    aput v8, v1, v3
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v1, v7

    goto :goto_0
.end method

.method private buildChunks(I)Ljava/util/Collection;
    .locals 12
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;",
            ">;"
        }
    .end annotation

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v9}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunkCount()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunk(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v0

    const/4 v4, 0x1

    :goto_0
    if-ge v4, v1, :cond_0

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v9, v4}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunk(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->toMillis(I)I

    move-result v7

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v5

    new-instance v9, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    sub-int v10, v5, v0

    invoke-direct {v9, v8, v7, v0, v10}, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;-><init>(IIII)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v8, v7

    move v0, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v9, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    const v10, 0x7fffffff

    sub-int v11, p1, v0

    invoke-direct {v9, v8, v10, v0, v11}, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;-><init>(IIII)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method private buildImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Image;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    :goto_0
    return-object v6

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getReferrerUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    :goto_1
    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getCropRegionCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getCropRegion(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    const/4 v7, 0x4

    new-array v7, v7, [I

    const/4 v8, 0x0

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getLeft()I

    move-result v9

    aput v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getTop()I

    move-result v9

    aput v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getRight()I

    move-result v9

    aput v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getBottom()I

    move-result v9

    aput v9, v7, v8

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-direct {v2, p2, v5, v4, v6}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    invoke-interface {p3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v2

    goto :goto_0
.end method

.method private buildKnowledgeEntities(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Ljava/util/Map;)Ljava/util/List;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p2    # [Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            "[",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getActorList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, p2, v0, p3}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildActors(Ljava/util/List;[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getSongList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, v0, p3}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildSongs(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method

.method private buildReferencedFilms(Ljava/util/List;Ljava/util/Set;Ljava/util/Map;IIIILjava/util/Map;)[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
    .locals 16
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;IIII",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)[",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;"
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    new-array v14, v15, [Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;

    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getMid()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v9

    :goto_2
    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getIsTv()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->TYPE_LOGO:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "poster."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p0

    move/from16 v4, p6

    move/from16 v5, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;IILjava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-result-object v8

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReleaseDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getEndDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v3 .. v9}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V

    aput-object v3, v14, v1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-object v2, v1

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    :cond_2
    sget-object v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->TYPE_POSTER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "poster."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p0

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;IILjava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-result-object v8

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReleaseDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getRuntimeMinutes()I

    move-result v6

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v3 .. v9}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V

    aput-object v3, v14, v1

    goto/16 :goto_0

    :cond_3
    return-object v14
.end method

.method private buildSongs(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Song;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x0

    const/4 v7, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getLocalId()I

    move-result v1

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "song."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, p3}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-result-object v3

    :goto_1
    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAppearance()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildAppearances(Lcom/google/protobuf/ByteString;)[I

    move-result-object v4

    :goto_2
    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getPerformer()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;-><init>(ILjava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    move-object v3, v9

    goto :goto_1

    :cond_1
    move-object v4, v9

    goto :goto_2

    :cond_2
    return-void
.end method

.method private getPosterFromAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;IILjava/lang/String;Ljava/util/Map;)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .locals 6
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;)",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v4

    invoke-static {v4, p2, p3, p4}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->findBestImage(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;II)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getResizable()Z

    move-result v4

    if-eqz v4, :cond_0

    if-lez p3, :cond_0

    if-lez p4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=p-w"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-h"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    return-object v3

    :cond_1
    invoke-interface {p6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->crops:Ljava/util/List;

    invoke-direct {v0, p5, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    invoke-interface {p6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    goto :goto_0
.end method

.method private parseActors(Ljava/util/List;ILjava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 14
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v10

    const/4 v4, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    if-ge v4, v11, :cond_9

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId()Z

    move-result v11

    if-nez v11, :cond_0

    new-instance v11, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v12, "Actor invalid - no local ID"

    invoke-direct {v11, v12}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getLocalId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    new-instance v11, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Actor invalid - duplicate local ID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitIdCount()I

    move-result v8

    if-nez v8, :cond_2

    new-instance v11, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Actor invalid - no split IDs for LID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v8, :cond_4

    invoke-virtual {v1, v5}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitId(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    new-instance v11, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Actor invalid - duplicate split ID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName()Z

    move-result v11

    if-nez v11, :cond_5

    new-instance v11, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Actor invalid - no name for LID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v11

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexCount()I

    move-result v3

    const/4 v9, 0x0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v3, :cond_8

    const/4 v11, 0x3

    if-ge v9, v11, :cond_8

    invoke-virtual {v1, v5}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndex(I)I

    move-result v2

    if-ltz v2, :cond_7

    move/from16 v0, p2

    if-ge v2, v0, :cond_7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_9
    return-object v10
.end method

.method private parseChunks(I)V
    .locals 10
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunkCount()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v8, "Header invalid - no chunks defined"

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    const/high16 v5, -0x80000000

    const/high16 v4, -0x80000000

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v7, v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunk(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart()Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] missing start"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v6

    if-gt v6, v5, :cond_2

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s start not increasing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    move v5, v6

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset()Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] missing byte offset"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v0

    if-gt v0, v4, :cond_4

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s byte offset not increasing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    if-lt v0, p1, :cond_5

    new-instance v7, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Header invalid - chunk["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s byte offset beyond file size"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_5
    move v4, v0

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method private parseImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image of \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' invalid - no url"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private parseKnowledgeEntities(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/Set;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getActorList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReferencedFilmCount()I

    move-result v4

    invoke-direct {p0, v3, v4, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseActors(Ljava/util/List;ILjava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getSongList()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseSongs(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)V

    return-object v2
.end method

.method private parseReferencedFilms(Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v5, "Referenced film invalid - no title"

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getMid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getIsTv()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromShowMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromMovieMid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method private parseSongs(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Song;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasLocalId()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v4, "Song invalid - no local ID"

    invoke-direct {v3, v4}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getLocalId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - duplicate local ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - duplicate split ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasTitle()Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/protobuf/InvalidProtocolBufferException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Song invalid - no title for LID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_5
    return-void
.end method

.method private toMillis(I)I
    .locals 2
    .param p1    # I

    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->framesPerSecond:F

    div-float/2addr v0, v1

    const/high16 v1, 0x447a0000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public build(Ljava/util/Map;IIII)Lcom/google/android/youtube/videos/tagging/TagStream;
    .locals 26
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;IIII)",
            "Lcom/google/android/youtube/videos/tagging/TagStream;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parsed:Z

    const-string v4, "Source not yet parsed"

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    const-string v4, "Source did not succeed parsing"

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    const-string v3, "assetResources cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v24

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v11

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReferencedFilmList()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v3 .. v11}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildReferencedFilms(Ljava/util/List;Ljava/util/Set;Ljava/util/Map;IIIILjava/util/Map;)[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildKnowledgeEntities(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;[Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Ljava/util/Map;)Ljava/util/List;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->source:[B

    array-length v3, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->buildChunks(I)Ljava/util/Collection;

    move-result-object v23

    new-instance v12, Lcom/google/android/youtube/videos/tagging/TagStream;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoId:Ljava/lang/String;

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->contentLanguage:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->lastModified:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoItag:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->videoTimestamp:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->source:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->framesPerSecond:F

    move/from16 v20, v0

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v22

    invoke-direct/range {v12 .. v23}, Lcom/google/android/youtube/videos/tagging/TagStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BFLjava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v12

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFilmographyIds()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parsed:Z

    const-string v1, "Source not yet parsed"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->filmographyIds:Ljava/util/List;

    return-object v0
.end method

.method public parse()Lcom/google/android/youtube/videos/tagging/TagStreamParser;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x1

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parsed:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    throw v4

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->source:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v5, "Header invalid - no FPS"

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseException:Ljava/io/IOException;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v4

    iput-boolean v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parsed:Z

    throw v4

    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFps()F

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->framesPerSecond:F

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v5, "Header invalid - no film"

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->header:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseKnowledgeEntities(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/Set;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReferencedFilmList()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->usedReferencedFilmIndices:Ljava/util/Set;

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseReferencedFilms(Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->source:[B

    array-length v4, v4

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parseChunks(I)V

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->filmographyIds:Ljava/util/List;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-boolean v6, p0, Lcom/google/android/youtube/videos/tagging/TagStreamParser;->parsed:Z

    :cond_3
    return-object p0
.end method
