.class public Lcom/google/android/youtube/videos/tagging/CardTag;
.super Ljava/lang/Object;
.source "CardTag.java"


# static fields
.field public static final RECENT_ACTORS:Lcom/google/android/youtube/videos/tagging/CardTag;


# instance fields
.field public final cardType:I

.field public final knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/tagging/CardTag;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/tagging/CardTag;-><init>(ILcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/CardTag;->RECENT_ACTORS:Lcom/google/android/youtube/videos/tagging/CardTag;

    return-void
.end method

.method private constructor <init>(ILcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/CardTag;->cardType:I

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/CardTag;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    return-void
.end method

.method public static forActor(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Lcom/google/android/youtube/videos/tagging/CardTag;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/CardTag;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/videos/tagging/CardTag;-><init>(ILcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V

    return-object v0
.end method

.method public static forMovies(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Lcom/google/android/youtube/videos/tagging/CardTag;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/CardTag;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/videos/tagging/CardTag;-><init>(ILcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V

    return-object v0
.end method

.method public static forSong(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;)Lcom/google/android/youtube/videos/tagging/CardTag;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/CardTag;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/videos/tagging/CardTag;-><init>(ILcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V

    return-object v0
.end method
