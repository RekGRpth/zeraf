.class public abstract Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.super Ljava/lang/Object;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;
    }
.end annotation


# instance fields
.field private final appearances:[I

.field public final image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

.field public final localId:I

.field public final name:Ljava/lang/String;

.field public final splitIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[I)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p4    # [I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->splitIds:Ljava/util/Set;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->appearances:[I

    return-void
.end method

.method constructor <init>(ILjava/util/Collection;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[I)V
    .locals 1
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p5    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "[I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {p2}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->splitIds:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->appearances:[I

    return-void
.end method


# virtual methods
.method public hasAppearanceData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->appearances:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastAppearance(I)I
    .locals 4
    .param p1    # I

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->hasAppearanceData()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->appearances:[I

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_2

    move v1, p1

    goto :goto_0

    :cond_2
    xor-int/lit8 v0, v0, -0x1

    and-int/lit8 v2, v0, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    move v1, p1

    goto :goto_0

    :cond_3
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->appearances:[I

    add-int/lit8 v2, v0, -0x1

    aget v1, v1, v2

    goto :goto_0
.end method
