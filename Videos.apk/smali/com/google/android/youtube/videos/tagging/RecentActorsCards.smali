.class final Lcom/google/android/youtube/videos/tagging/RecentActorsCards;
.super Lcom/google/android/youtube/videos/tagging/Cards;
.source "RecentActorsCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/Cards;-><init>()V

    return-void
.end method

.method public static create(Ljava/util/List;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;
    .locals 31
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;
    .param p4    # Landroid/app/Activity;
    .param p5    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
            ">;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v18

    move/from16 v0, v25

    new-array v15, v0, [I

    const v4, 0x7f04002d

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup;

    const v4, 0x7f070067

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v29

    invoke-virtual/range {v26 .. v26}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v22

    invoke-virtual/range {v26 .. v26}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const/16 v20, 0x0

    move/from16 v30, v29

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    const v4, 0x7f04002e

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    const v4, 0x7f070069

    iget-object v5, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-static {v13, v4, v5}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-object/from16 v0, v28

    invoke-static {v0, v3}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const v4, 0x7f07006a

    move-object/from16 v0, v19

    invoke-static {v13, v4, v0}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    :cond_0
    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    if-eqz v4, :cond_1

    const v4, 0x7f070068

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    const v5, 0x7f09003b

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move-object/from16 v0, v21

    invoke-static {v4, v0, v5}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->setImageMatrixIfSquareCropExists(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V

    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-object/from16 v0, v21

    move-object/from16 v1, p3

    invoke-static {v0, v4, v1}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->requestImage(Landroid/widget/ImageView;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Requester;)V

    :cond_1
    add-int/lit8 v29, v30, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v30

    invoke-virtual {v0, v13, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v14

    const/4 v5, 0x0

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-static/range {v3 .. v10}, Lcom/google/android/youtube/videos/tagging/ActorCards;->create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    aput v5, v15, v4

    new-instance v27, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards$RecentActorsCardItemClickListener;-><init>(Landroid/view/View;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Ljava/util/List;)V

    move-object/from16 v0, v27

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v12, :cond_3

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v12, 0x1

    :cond_3
    add-int/lit8 v20, v20, 0x1

    move/from16 v30, v29

    goto/16 :goto_0

    :cond_4
    const/16 v20, 0x0

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    const/16 v23, 0x0

    :goto_2
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_7

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_6

    :cond_5
    add-int/lit8 v23, v23, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/List;

    const/16 v24, 0x0

    :goto_3
    aget v4, v15, v23

    move/from16 v0, v24

    if-ge v0, v4, :cond_5

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    :cond_7
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    :cond_8
    sget-object v4, Lcom/google/android/youtube/videos/tagging/CardTag;->RECENT_ACTORS:Lcom/google/android/youtube/videos/tagging/CardTag;

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    return-object v26
.end method
