.class final Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;
.super Ljava/lang/Object;
.source "ActorCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/ActorCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RequestFeedbackOnClickListener"
.end annotation


# instance fields
.field private final knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

.field private final onFeedbackButtonClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;->onFeedbackButtonClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;->onFeedbackButtonClickListener:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;->onFeedbackButtonClick(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V

    return-void
.end method
