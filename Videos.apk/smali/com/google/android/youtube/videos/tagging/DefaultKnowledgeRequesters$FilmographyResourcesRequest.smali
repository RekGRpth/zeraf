.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FilmographyResourcesRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;
    }
.end annotation


# instance fields
.field public final filmographyIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;)V

    const-string v0, "filmographyIds cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->filmographyIds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public toFileName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->baseFileNameWithCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
