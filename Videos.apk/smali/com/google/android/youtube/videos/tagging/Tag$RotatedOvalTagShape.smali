.class Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;
.super Ljava/lang/Object;
.source "Tag.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/Tag$TagShape;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RotatedOvalTagShape"
.end annotation


# instance fields
.field public angle:F

.field public centerX:F

.field public centerY:F

.field public ovalA:F

.field public ovalB:F


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/tagging/Tag$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/Tag$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;-><init>()V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    const/high16 v2, -0x3fc00000

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    const/high16 v2, 0x40400000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v5, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v6, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    add-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBoundingBox(Landroid/graphics/RectF;)V
    .locals 3
    .param p1    # Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    sub-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    sub-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method public hitTest(FFF)Z
    .locals 23
    .param p1    # F
    .param p2    # F
    .param p3    # F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    move/from16 v17, v0

    sub-float v9, p1, v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    move/from16 v17, v0

    sub-float v10, p2, v17

    mul-float v17, v9, v9

    mul-float v18, v10, v10

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v17

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v19, v0

    sub-double v7, v17, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-ltz v17, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move/from16 v17, v0

    :goto_1
    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    cmpl-double v17, v7, v17

    if-lez v17, :cond_2

    const/16 v17, 0x0

    :goto_2
    return v17

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v17, v0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v17, v0

    :goto_3
    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    cmpg-double v17, v7, v17

    if-gtz v17, :cond_4

    const/16 v17, 0x1

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move/from16 v17, v0

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    move/from16 v17, v0

    sub-float v13, p1, v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    move/from16 v17, v0

    sub-float v14, v17, p2

    float-to-double v0, v14

    move-wide/from16 v17, v0

    float-to-double v0, v13

    move-wide/from16 v19, v0

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->angle:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v19

    sub-double v15, v17, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sin(D)D

    move-result-wide v19

    mul-double v3, v17, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->cos(D)D

    move-result-wide v19

    mul-double v5, v17, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    mul-double v19, v3, v3

    mul-double v21, v5, v5

    add-double v19, v19, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v19

    div-double v11, v17, v19

    cmpg-double v17, v7, v11

    if-gtz v17, :cond_5

    const/16 v17, 0x1

    goto/16 :goto_2

    :cond_5
    const/16 v17, 0x0

    goto/16 :goto_2
.end method
