.class public final Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;
.super Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Song"
.end annotation


# instance fields
.field public final albumArt:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

.field public final googlePlayUrl:Ljava/lang/String;

.field public final performer:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p4    # [I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;-><init>(ILjava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;[I)V

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;->albumArt:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    return-void
.end method
