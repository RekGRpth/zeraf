.class public final Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;
.super Ljava/lang/Object;
.source "Cards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/Cards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "StoreSearchOnClickListener"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final category:Ljava/lang/String;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final eventSource:Ljava/lang/String;

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->activity:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/youtube/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->query:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->category:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->account:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->eventSource:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->category:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->account:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->activity:Landroid/app/Activity;

    const v2, 0x7f0a0078

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;->eventSource:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForSearch(Ljava/lang/String;Z)V

    return-void
.end method
