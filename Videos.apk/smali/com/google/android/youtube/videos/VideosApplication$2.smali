.class Lcom/google/android/youtube/videos/VideosApplication$2;
.super Ljava/lang/Object;
.source "VideosApplication.java"

# interfaces
.implements Lcom/google/android/youtube/videos/store/Database$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/VideosApplication;->initInternal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/VideosApplication;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/VideosApplication;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/VideosApplication$2;->this$0:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication$2;->this$0:Lcom/google/android/youtube/videos/VideosApplication;

    # getter for: Lcom/google/android/youtube/videos/VideosApplication;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    invoke-static {v0}, Lcom/google/android/youtube/videos/VideosApplication;->access$200(Lcom/google/android/youtube/videos/VideosApplication;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication$2;->this$0:Lcom/google/android/youtube/videos/VideosApplication;

    # getter for: Lcom/google/android/youtube/videos/VideosApplication;->contentNotificationManager:Lcom/google/android/youtube/videos/ContentNotificationManager;
    invoke-static {v0}, Lcom/google/android/youtube/videos/VideosApplication;->access$100(Lcom/google/android/youtube/videos/VideosApplication;)Lcom/google/android/youtube/videos/ContentNotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager;->checkForNewEpisodes(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
