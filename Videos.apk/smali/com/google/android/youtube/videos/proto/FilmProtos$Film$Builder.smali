.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object v0
.end method


# virtual methods
.method public addActor(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addAlbum(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addDirector(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addReferencedFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSong(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReleaseDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setReleaseDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getRuntimeMinutes()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setRuntimeMinutes(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_7
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_b
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_d
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_e
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_10
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getIsTv()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setIsTv(Z)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_11
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getEndDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setEndDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_12
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;

    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setReleaseDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->addActor(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->addDirector(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->addSong(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setRuntimeMinutes(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto/16 :goto_0

    :sswitch_b
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->addReferencedFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setIsTv(Z)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->setEndDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto/16 :goto_0

    :sswitch_e
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->addAlbum(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1900(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1900(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1802(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    goto :goto_0
.end method

.method public setEndDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->endDate_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2002(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->googlePlayUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2102(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1002(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1102(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1802(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p0
.end method

.method public setIsTv(Z)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2202(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isTv_:Z
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$2302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    return-object p0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$802(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setReleaseDate(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->releaseDate_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setRuntimeMinutes(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->runtimeMinutes_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;I)I

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1202(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->title_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->access$1302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
