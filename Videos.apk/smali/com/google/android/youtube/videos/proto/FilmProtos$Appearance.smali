.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Appearance"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;


# instance fields
.field private memoizedSerializedSize:I

.field private offsetMemoizedSerializedSize:I

.field private offset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offsetMemoizedSerializedSize:I

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offsetMemoizedSerializedSize:I

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->access$3300()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 1
    .param p0    # Lcom/google/protobuf/ByteString;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->access$3200(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getOffset(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getOffsetCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOffsetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offsetMemoizedSerializedSize:I

    iput v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffsetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    iget v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offsetMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getOffsetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32NoTag(I)V

    goto :goto_0

    :cond_1
    return-void
.end method
