.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Frame;",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;-><init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    return-object v0
.end method


# virtual methods
.method public addTagIn(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2102(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTagOut(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2202(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->build()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2102(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2202(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->setOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    :cond_2
    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2102(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2202(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->setOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->addTagIn(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->addTagOut(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2302(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->offset_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->access$2402(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;I)I

    return-object p0
.end method
