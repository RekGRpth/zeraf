.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Image;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$8200()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method


# virtual methods
.method public addCropRegion(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8402(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8402(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getReferrerUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setReferrerUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setData(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_5
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8402(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/util/List;)Ljava/util/List;

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setReferrerUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->setData(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->addCropRegion(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setData(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$9102(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->data_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$9202(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8502(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->mimeType_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8602(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setReferrerUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8902(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->referrerUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$9002(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8702(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->url_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->access$8802(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
