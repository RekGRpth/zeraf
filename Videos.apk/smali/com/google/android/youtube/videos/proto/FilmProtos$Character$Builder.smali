.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Character;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$6200()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6900(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6900(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6902(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6802(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6902(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    goto :goto_0
.end method

.method public setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6802(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6902(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6402(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6502(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6602(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->access$6702(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
