.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Song;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Song;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$9400()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public hasAlbumArt()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAlbumArt(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->albumArt_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10900(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->albumArt_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10900(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->albumArt_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10902(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10802(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->albumArt_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10902(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasPerformer()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getPerformer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setPerformer(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasIsrc()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getIsrc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setIsrc(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasGooglePlayUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasLocalId()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getLocalId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->mergeAlbumArt(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAppearance()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setPerformer(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setIsrc(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->hasAlbumArt()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->getAlbumArt()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setAlbumArt(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setAlbumArt(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAlbumArt:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10802(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->albumArt_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10902(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p0
.end method

.method public setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasAppearance:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$11002(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->appearance_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$11102(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public setGooglePlayUrl(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasGooglePlayUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10402(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->googlePlayUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10502(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setIsrc(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasIsrc:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10202(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->isrc_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10302(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasLocalId:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10602(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->localId_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10702(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;I)I

    return-object p0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$9602(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$9702(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setPerformer(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasPerformer:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10002(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->performer_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$10102(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->hasTitle:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$9802(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->title_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;->access$9902(Lcom/google/android/youtube/videos/proto/FilmProtos$Song;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
