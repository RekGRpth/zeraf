.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$3700()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    return-object v0
.end method


# virtual methods
.method public addCharacter(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addFilmography(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addFilmographyIndex(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSplitId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_5
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_7
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfBirth()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setDateOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfDeath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setDateOfDeath(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getPlaceOfBirth()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setPlaceOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getLocalId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    :cond_d
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_f
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;

    :cond_10
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_11
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addCharacter(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addSplitId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addSplitId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addFilmography(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setDateOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setDateOfDeath(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setPlaceOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addFilmographyIndex(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v1

    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->addFilmographyIndex(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x62 -> :sswitch_e
        0x6a -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    goto :goto_0
.end method

.method public setAppearance(Lcom/google/protobuf/ByteString;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->appearance_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$6002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public setDateOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfBirth_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setDateOfDeath(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5302(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfDeath_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5402(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4502(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4602(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setImage(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p0
.end method

.method public setLocalId(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5702(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->localId_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5802(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;I)I

    return-object p0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4302(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4402(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4702(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$4802(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setPlaceOfBirth(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5502(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->placeOfBirth_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->access$5602(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
