.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Header;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Header"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Header;


# instance fields
.field private chunk_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;",
            ">;"
        }
    .end annotation
.end field

.field private encrypted_:Z

.field private film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

.field private fps_:F

.field private hasEncrypted:Z

.field private hasFilm:Z

.field private hasFps:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->fps_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->encrypted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->fps_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->encrypted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/youtube/videos/proto/TagProtos$Header;F)F
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # F

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->fps_:F

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->encrypted_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->access$100()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 2
    .param p0    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->access$000(Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;)Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getChunk(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    return-object v0
.end method

.method public getChunkCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getChunkList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;

    return-object v0
.end method

.method public getEncrypted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->encrypted_:Z

    return v0
.end method

.method public getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object v0
.end method

.method public getFps()F
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->fps_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFps()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunkList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getEncrypted()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    iput v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public hasEncrypted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted:Z

    return v0
.end method

.method public hasFilm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm:Z

    return v0
.end method

.method public hasFps()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFps()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getChunkList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getEncrypted()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_3
    return-void
.end method
