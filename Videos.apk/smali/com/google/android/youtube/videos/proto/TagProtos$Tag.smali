.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tag"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;


# instance fields
.field private circle_:I

.field private faceRectShape_:J

.field private hasCircle:Z

.field private hasFaceRectShape:Z

.field private hasOffCameraShape:Z

.field private hasSplitId:Z

.field private memoizedSerializedSize:I

.field private offCameraShape_:I

.field private splitId_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->splitId_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->offCameraShape_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->circle_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->faceRectShape_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->splitId_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->offCameraShape_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->circle_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->faceRectShape_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2802(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->splitId_:I

    return p1
.end method

.method static synthetic access$3002(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->offCameraShape_:I

    return p1
.end method

.method static synthetic access$3202(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle:Z

    return p1
.end method

.method static synthetic access$3302(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->circle_:I

    return p1
.end method

.method static synthetic access$3402(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape:Z

    return p1
.end method

.method static synthetic access$3502(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;J)J
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->faceRectShape_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->access$2600()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCircle()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->circle_:I

    return v0
.end method

.method public getFaceRectShape()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->faceRectShape_:J

    return-wide v0
.end method

.method public getOffCameraShape()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->offCameraShape_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getOffCameraShape()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getFaceRectShape()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getCircle()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getSplitId()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->splitId_:I

    return v0
.end method

.method public hasCircle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle:Z

    return v0
.end method

.method public hasFaceRectShape()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape:Z

    return v0
.end method

.method public hasOffCameraShape()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape:Z

    return v0
.end method

.method public hasSplitId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getOffCameraShape()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getFaceRectShape()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getCircle()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    return-void
.end method
