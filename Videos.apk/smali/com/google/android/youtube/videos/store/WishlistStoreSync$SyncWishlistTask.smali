.class Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;
.super Ljava/lang/Object;
.source "WishlistStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/WishlistStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncWishlistTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/api/WishlistRequest;",
        "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/WishlistStoreSync;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "account cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/api/WishlistRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/api/WishlistRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p1, Lcom/google/android/youtube/videos/api/WishlistRequest;->account:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/api/WishlistRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->onError(Lcom/google/android/youtube/videos/api/WishlistRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/api/WishlistRequest;Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)V
    .locals 22
    .param p1    # Lcom/google/android/youtube/videos/api/WishlistRequest;
    .param p2    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v3, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v3, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    const/4 v10, 0x0

    const/4 v14, 0x0

    :try_start_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v17, "wishlist_in_cloud"

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v17, "wishlist"

    const-string v18, "wishlist_account = ?"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v4, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v6, 0x0

    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceIdsCount()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getResourceIds(I)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->itemTypeToInteger(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)I

    move-result v8

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_0

    invoke-virtual {v11}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v7

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v17, "wishlist_in_cloud"

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v17, "wishlist"

    const-string v18, "wishlist_account = ? AND wishlist_item_id = ? AND wishlist_item_type = ? AND wishlist_item_state != 2"

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v7, v19, v20

    const/16 v20, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v9, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    if-nez v16, :cond_0

    const-string v17, "wishlist_account"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "wishlist_item_id"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "wishlist_item_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v17, "wishlist_item_state"

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v17, "wishlist"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1, v9}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    const-wide/16 v17, -0x1

    cmp-long v17, v12, v17

    if-eqz v17, :cond_0

    const/4 v10, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    const-string v17, "wishlist"

    const-string v18, "wishlist_account = ? AND NOT wishlist_in_cloud AND wishlist_item_state != 2"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v16

    if-lez v16, :cond_2

    const/4 v10, 0x1

    :cond_2
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v18

    if-eqz v10, :cond_4

    const/16 v17, 0x8

    :goto_1
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v17 .. v19}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    const/16 v17, 0x0

    goto :goto_1

    :catch_0
    move-exception v5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v5}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v18

    if-eqz v10, :cond_5

    const/16 v17, 0x8

    :goto_3
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v17 .. v19}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_5
    const/16 v17, 0x0

    goto :goto_3

    :catchall_0
    move-exception v17

    move-object/from16 v18, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v19

    if-eqz v10, :cond_7

    const/16 v17, 0x8

    :goto_4
    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v20

    invoke-virtual {v0, v15, v14, v1, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_6
    throw v18

    :cond_7
    const/16 v17, 0x0

    goto :goto_4
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/api/WishlistRequest;

    check-cast p2, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->onResponse(Lcom/google/android/youtube/videos/api/WishlistRequest;Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)V

    return-void
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$000(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/videos/api/WishlistRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/api/WishlistRequest;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
