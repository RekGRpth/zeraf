.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncPosterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final databaseEvent:I

.field private final fileStore:Lcom/google/android/youtube/videos/store/FileStore;

.field private final itemId:Ljava/lang/String;

.field private final metadataItemIdColumn:Ljava/lang/String;

.field private final metadataPosterSyncedColumn:Ljava/lang/String;

.field private final metadataTable:Ljava/lang/String;

.field private final posterItemIdColumn:Ljava/lang/String;

.field private final posterTable:Ljava/lang/String;

.field private final posterUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/net/Uri;
    .param p6    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;
    .param p11    # Ljava/lang/String;
    .param p12    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/videos/store/FileStore;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "videoId cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    const-string v0, "posterUri cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterUri:Landroid/net/Uri;

    const-string v0, "fileStore cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v0, "metadataTable cannot be empty"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataTable:Ljava/lang/String;

    const-string v0, "metadataPosterSyncedColumn cannot be empty"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataPosterSyncedColumn:Ljava/lang/String;

    const-string v0, "metadataItemIdColumn cannot be empty"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataItemIdColumn:Ljava/lang/String;

    const-string v0, "posterTable cannot be empty"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterTable:Ljava/lang/String;

    const-string v0, "posterItemIdColumn cannot be empty"

    invoke-static {p11, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterItemIdColumn:Ljava/lang/String;

    iput p12, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->databaseEvent:I

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPosterArtRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1700(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterUri:Landroid/net/Uri;

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/Exception;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "showposter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/vi/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/vi/"

    const-string v2, "/sh/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPosterArtRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1700(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    invoke-interface {v1, p1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xa

    invoke-virtual {p0, v1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->onSyncError(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;[B)V
    .locals 13
    .param p1    # Landroid/net/Uri;
    .param p2    # [B

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v7, 0x1

    iput-boolean v7, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v7, 0x0

    array-length v8, p2

    invoke-static {p2, v7, v8, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v7, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gez v7, :cond_0

    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot decode bitmap"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const/4 v4, 0x0

    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataPosterSyncedColumn:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataTable:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->metadataItemIdColumn:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = ?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v5, v7, v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_1

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterItemIdColumn:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->posterTable:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {v5, v7, v8, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    invoke-virtual {v7, v8, p2}, Lcom/google/android/youtube/videos/store/FileStore;->putBytes(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v4, 0x1

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    iget v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->databaseEvent:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v7, v5, v4, v8, v9}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    const/16 v7, 0xa

    :try_start_1
    invoke-virtual {p0, v7, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->onSyncError(ILjava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    iget v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->databaseEvent:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v7, v5, v4, v8, v9}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v8}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v8

    iget v9, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->databaseEvent:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->itemId:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v8, v5, v4, v9, v10}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v7
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->onResponse(Landroid/net/Uri;[B)V

    return-void
.end method
