.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncPurchasesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/core/async/GDataRequest;",
        "Lcom/google/android/youtube/core/model/Page",
        "<",
        "Lcom/google/android/youtube/core/model/Purchase;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final request:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final resyncSeasonEpisodes:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/core/async/GDataRequest;Z)V
    .locals 1
    .param p2    # I
    .param p4    # Lcom/google/android/youtube/core/async/GDataRequest;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "gDataRequest cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->request:Lcom/google/android/youtube/core/async/GDataRequest;

    iput-boolean p5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->resyncSeasonEpisodes:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)V
    .locals 6
    .param p2    # I
    .param p4    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Z)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p4, v0}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMyPurchasesRequest(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/core/async/GDataRequest;Z)V

    return-void
.end method

.method private doPostSyncCleanup(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->schedule()V

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->request:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->doPostSyncCleanup(Ljava/lang/String;)V

    const/16 v0, 0x1e

    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onSyncError(ILjava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onError(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .locals 13
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v6, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    const/4 v10, 0x0

    const/4 v12, 0x0

    :try_start_0
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-static {v11, v6, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->updateStoredPurchases(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v11, v10, v3, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/youtube/core/model/Purchase;

    iget-object v0, v9, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$2;->$SwitchMap$com$google$android$youtube$core$model$Purchase$ItemType:[I

    iget-object v1, v9, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Purchase$ItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v11, v10, v3, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideo(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v8}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$500(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->resyncSeasonEpisodes:Z

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncSeason(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2, v8, v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$600(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_0
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v5, p1, Lcom/google/android/youtube/core/async/GDataRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-static {v4, v5}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->resyncSeasonEpisodes:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/core/async/GDataRequest;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->doPostSyncCleanup(Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->onResponse(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method
