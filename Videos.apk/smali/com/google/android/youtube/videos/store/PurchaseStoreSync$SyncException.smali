.class public Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;
.super Ljava/lang/Exception;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncException"
.end annotation


# instance fields
.field public final errorLevel:I


# direct methods
.method public constructor <init>(ILjava/lang/Exception;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/Exception;

    const-string v0, "An error occurred during a sync."

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;->errorLevel:I

    return-void
.end method
