.class public final Lcom/google/android/youtube/videos/store/V2Duration;
.super Ljava/lang/Object;
.source "V2Duration.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x7de340a37b073a69L


# instance fields
.field public final days:F

.field private volatile hashCode:I

.field public final hours:F

.field public final minutes:F

.field public final months:F

.field public final seconds:F

.field public final weeks:F

.field public final years:F


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->years:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->months:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->weeks:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->days:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->hours:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->minutes:F

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Duration;->seconds:F

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 8

    new-instance v0, Lcom/google/android/youtube/core/model/Duration;

    iget v1, p0, Lcom/google/android/youtube/videos/store/V2Duration;->years:F

    iget v2, p0, Lcom/google/android/youtube/videos/store/V2Duration;->months:F

    iget v3, p0, Lcom/google/android/youtube/videos/store/V2Duration;->weeks:F

    iget v4, p0, Lcom/google/android/youtube/videos/store/V2Duration;->days:F

    iget v5, p0, Lcom/google/android/youtube/videos/store/V2Duration;->hours:F

    iget v6, p0, Lcom/google/android/youtube/videos/store/V2Duration;->minutes:F

    iget v7, p0, Lcom/google/android/youtube/videos/store/V2Duration;->seconds:F

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/core/model/Duration;-><init>(FFFFFFF)V

    return-object v0
.end method
