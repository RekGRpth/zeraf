.class public Lcom/google/android/youtube/videos/store/Database;
.super Ljava/lang/Object;
.source "Database.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;,
        Lcom/google/android/youtube/videos/store/Database$Listener;
    }
.end annotation


# instance fields
.field private final dbHelper:Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;

.field private final listeners:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/youtube/videos/store/Database$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Handler;
    .param p4    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;

    invoke-direct {v0, p1, p2, p4}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->dbHelper:Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;

    const-string v0, "notificationHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->notificationHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/Database;I[Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/store/Database;->notify(I[Ljava/lang/Object;)V

    return-void
.end method

.method private notify(I[Ljava/lang/Object;)V
    .locals 6
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/Database$Listener;

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    aget-object v3, p2, v5

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/store/Database$Listener;->onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/store/Database$Listener;->onPosterUpdated(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-interface {v1}, Lcom/google/android/youtube/videos/store/Database$Listener;->onPurchasesUpdated()V

    goto :goto_0

    :pswitch_3
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/store/Database$Listener;->onVideoMetadataUpdated(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    aget-object v3, p2, v5

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/store/Database$Listener;->onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    aget-object v3, p2, v5

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/store/Database$Listener;->onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    aget-object v2, p2, v4

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/store/Database$Listener;->onShowPosterUpdated(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_7
    invoke-interface {v1}, Lcom/google/android/youtube/videos/store/Database$Listener;->onWishlistUpdated()V

    goto :goto_0

    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/Database$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public beginTransaction()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/Database;->dbHelper:Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    return-object v0
.end method

.method public varargs endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Z
    .param p3    # I
    .param p4    # [Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->notificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/videos/store/Database$1;

    invoke-direct {v1, p0, p3, p4}, Lcom/google/android/youtube/videos/store/Database$1;-><init>(Lcom/google/android/youtube/videos/store/Database;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->dbHelper:Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/Database$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/Database;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
