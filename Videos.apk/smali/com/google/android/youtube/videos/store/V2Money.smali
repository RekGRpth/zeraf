.class public final Lcom/google/android/youtube/videos/store/V2Money;
.super Ljava/lang/Object;
.source "V2Money.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x592736b8a041a9c1L


# instance fields
.field public final currency:Ljava/util/Currency;

.field private volatile hashCode:I

.field public final value:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/store/V2Money;->value:I

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2Money;->currency:Ljava/util/Currency;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/model/Money;

    iget v1, p0, Lcom/google/android/youtube/videos/store/V2Money;->value:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/V2Money;->currency:Ljava/util/Currency;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/model/Money;-><init>(ILjava/util/Currency;)V

    return-object v0
.end method
