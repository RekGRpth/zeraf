.class Lcom/google/android/youtube/videos/store/V2$V2UpgradeObjectInputStream;
.super Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;
.source "V2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/V2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V2UpgradeObjectInputStream"
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/StreamCorruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;-><init>(Ljava/io/InputStream;)V

    const-class v0, Lcom/google/android/youtube/core/model/Video$Builder;

    const-class v1, Lcom/google/android/youtube/videos/store/V2Video$Builder;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/store/V2$V2UpgradeObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    const-class v1, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/store/V2$V2UpgradeObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/youtube/core/model/Money;

    const-class v1, Lcom/google/android/youtube/videos/store/V2Money;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/store/V2$V2UpgradeObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/youtube/core/model/Duration;

    const-class v1, Lcom/google/android/youtube/videos/store/V2Duration;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/store/V2$V2UpgradeObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method
