.class public Lcom/google/android/youtube/videos/store/PosterStore;
.super Ljava/lang/Object;
.source "PosterStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/PosterStore$NoStoredPosterException;
    }
.end annotation


# instance fields
.field private final bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final bitmapConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

.field private final showPosterRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

.field private final videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p3    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p4    # Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;
    .param p5    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "videoPosterFileStore cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v0, "showPosterFileStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/FileStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v0, "bitmapConverter cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->bitmapConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    const-string v0, "bitmapCache cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    new-instance v0, Lcom/google/android/youtube/videos/store/PosterStore$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/PosterStore$1;-><init>(Lcom/google/android/youtube/videos/store/PosterStore;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Lcom/google/android/youtube/videos/store/PosterStore$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/PosterStore$2;-><init>(Lcom/google/android/youtube/videos/store/PosterStore;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->showPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/PosterStore;)Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PosterStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->bitmapConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/PosterStore;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PosterStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    return-object v0
.end method

.method private getPoster(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/store/FileStore;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    invoke-interface {p4, p1, v6}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PosterStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/youtube/videos/store/PosterStore$4;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PosterStore$4;-><init>(Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private getPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/store/FileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/store/FileStore;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/store/PosterStore$3;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/youtube/videos/store/PosterStore$3;-><init>(Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private toShowPosterBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".sp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toVideoPosterBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getShowPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PosterStore;->toShowPosterBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getPoster(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public getShowPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public getShowPosterRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->showPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getVideoPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PosterStore;->toVideoPosterBitmapCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getPoster(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public getVideoPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public getVideoPosterRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method
