.class public interface abstract Lcom/google/android/youtube/videos/store/V2$DownloadsQuery;
.super Ljava/lang/Object;
.source "V2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/V2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DownloadsQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "file_path"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "network_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "bytes_total"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "bytes_transferred"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "extras"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/store/V2$DownloadsQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
