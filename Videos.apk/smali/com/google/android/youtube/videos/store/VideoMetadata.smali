.class public Lcom/google/android/youtube/videos/store/VideoMetadata;
.super Ljava/lang/Object;
.source "VideoMetadata.java"


# instance fields
.field public final actors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final directors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final durationSecs:I

.field public final id:Ljava/lang/String;

.field public final producers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final purchasedHd:Z

.field public final ratings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final relatedUri:Landroid/net/Uri;

.field public final releaseYear:I

.field public final surroundSoundPurchasedAndAvailable:Z

.field public final title:Ljava/lang/String;

.field public final writers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/net/Uri;
    .param p12    # Z
    .param p13    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->description:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->releaseYear:I

    iput p5, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->durationSecs:I

    iput-object p6, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->relatedUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->directors:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->writers:Ljava/util/List;

    iput-object p9, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->actors:Ljava/util/List;

    iput-object p10, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->producers:Ljava/util/List;

    iput-object p11, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->ratings:Ljava/util/List;

    iput-boolean p12, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->purchasedHd:Z

    iput-boolean p13, p0, Lcom/google/android/youtube/videos/store/VideoMetadata;->surroundSoundPurchasedAndAvailable:Z

    return-void
.end method
