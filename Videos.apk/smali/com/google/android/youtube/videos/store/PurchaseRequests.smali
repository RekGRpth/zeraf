.class public Lcom/google/android/youtube/videos/store/PurchaseRequests;
.super Ljava/lang/Object;
.source "PurchaseRequests.java"


# static fields
.field private static final WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

.field private static final WHERE_NOT_HIDDEN_AND_ACTIVE_EST_AND_MATCHES_QUERY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " NOT hidden AND status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " NOT hidden AND purchase_type = 2 AND status = 2 AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "show_title"

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "show_title"

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_EST_AND_MATCHES_QUERY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createActivePurchaseRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 9
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v6, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v3, " NOT hidden AND status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW AND video_id = ?"

    const-string v4, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    aput-object p1, v5, v1

    const/4 v8, -0x1

    move-object v3, p0

    move-object v7, v6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createActivePurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    const/4 v7, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v3, "status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW AND video_id = ?"

    const-string v4, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    aput-object p2, v6, v1

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createActivePurchasesRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 11
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J
    .param p5    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    invoke-static {p1}, Lcom/google/android/youtube/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    sget-object v3, Lcom/google/android/youtube/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

    const-string v4, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "% "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move v8, p2

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createActivePurchasesRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # J

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v3, "status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW"

    const-string v4, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createEstMoviesRequestForUser(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchases, (videos, video_userdata ON video_id = pinning_video_id) ON account = pinning_account AND item_type = 1 AND item_id = video_id AND episode_season_id IS NULL"

    const-string v5, " NOT hidden AND purchase_type = 2 AND status = 2"

    const-string v8, "title"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v7, v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMoviesSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # J
    .param p6    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    invoke-static {p2}, Lcom/google/android/youtube/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchases, (videos, video_userdata ON video_id = pinning_video_id) ON account = pinning_account AND item_type = 1 AND item_id = video_id AND episode_season_id IS NULL"

    sget-object v3, Lcom/google/android/youtube/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

    const-string v4, "@NOW"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "% "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, p1

    move v9, p3

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createMyEpisodesRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchases, (videos, video_userdata ON video_id = pinning_video_id), seasons ON account = pinning_account AND ((item_type = 2 AND item_id = season_id) OR (item_type = 1 AND item_id = video_id)) AND episode_season_id = season_id"

    const-string v5, "purchase_type = 2 AND status = 2 AND episode_season_id = ?"

    new-array v6, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v6, v3

    const/4 v7, 0x0

    const-string v8, "CAST(episode_number_text AS INTEGER), episode_number_text"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMySeasonsOfSameShowRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "(purchases LEFT JOIN videos ON item_type = 1 AND item_id = video_id), seasons ON (item_type = 2 AND item_id = season_id) OR season_id = episode_season_id"

    const-string v5, "purchase_type = 2 AND status = 2 AND show_id = (SELECT show_id FROM seasons WHERE season_id = ?)"

    new-array v6, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v6, v3

    const-string v8, "season_title"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMySeasonsRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "(purchases LEFT JOIN videos ON item_type = 1 AND item_id = video_id), seasons ON (item_type = 2 AND item_id = season_id) OR season_id = episode_season_id"

    const-string v5, "purchase_type = 2 AND status = 2 AND show_id = ?"

    new-array v6, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v6, v3

    const-string v8, "season_title"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMyShowsRequest(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchases, (videos, video_userdata ON video_id = pinning_video_id), seasons ON account = pinning_account AND ((item_type = 2 AND item_id = season_id) OR (item_type = 1 AND item_id = video_id)) AND episode_season_id = season_id"

    const-string v5, " NOT hidden AND purchase_type = 2 AND status = 2"

    const/4 v6, 0x0

    const-string v7, "show_id"

    const-string v8, "show_title"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createNowPlayingRequestForGlobalSearch([Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # [Ljava/lang/String;
    .param p1    # I
    .param p2    # J
    .param p4    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x1

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v3, " NOT hidden AND status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW"

    const-string v4, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    move-object v3, p0

    move-object v6, v5

    move-object v7, v5

    move v8, p1

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createNowPlayingRequestForUser(Ljava/lang/String;[Ljava/lang/String;IJ)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x1

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v3, " NOT hidden AND status = 2 AND CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END > @NOW"

    const-string v4, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "min( @NOW - purchase_timestamp, @NOW - last_watched_timestamp, ifnull(expiration_timestamp - @NOW, 9223372036854775807))"

    const-string v4, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-object v4, p1

    move-object v7, v6

    move v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createOtherEpisodesOfSeasonRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "videos, seasons ON episode_season_id = season_id"

    const-string v4, "episode_season_id = ? AND NOT EXISTS (SELECT NULL FROM purchases WHERE purchase_type = 2 AND status = 2 AND item_id = (CASE item_type WHEN 1 THEN video_id WHEN 2 THEN episode_season_id END) AND account = ?)"

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    aput-object p2, v5, v1

    const/4 v3, 0x1

    aput-object p0, v5, v3

    const/4 v6, 0x0

    const-string v7, "CAST(episode_number_text AS INTEGER), episode_number_text"

    const/4 v8, -0x1

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "((videos, video_userdata ON video_id = pinning_video_id) LEFT JOIN seasons ON episode_season_id = season_id), purchases ON account = pinning_account AND ((item_type = 2 AND item_id IS season_id) OR (item_type = 1 AND item_id = video_id))"

    const-string v5, "video_id = ?"

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    aput-object p2, v6, v1

    const/4 v7, 0x0

    const-string v8, "status <> 2"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createShowsSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # J
    .param p6    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    invoke-static {p2}, Lcom/google/android/youtube/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x1

    const-string v2, "(purchases LEFT JOIN videos ON item_type = 1 AND item_id = video_id), seasons ON (item_type = 2 AND item_id = season_id) OR season_id = episode_season_id"

    sget-object v5, Lcom/google/android/youtube/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_EST_AND_MATCHES_QUERY:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "% "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "%"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v7, 0x0

    const-string v8, "show_title"

    move-object v3, p0

    move-object v4, p1

    move v9, p3

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createVodMoviesRequestForUser(Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    const-string v0, " NOT hidden AND purchase_type = 1 AND status IN (2,3) AND @MERGED_EXPIRATION_TIMESTAMP > @NOW"

    const-string v1, "@MERGED_EXPIRATION_TIMESTAMP"

    if-eqz p4, :cond_0

    :goto_0
    invoke-virtual {v0, v1, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "@NOW"

    const-wide/32 v2, 0x5265c00

    sub-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchases, (videos, video_userdata ON video_id = pinning_video_id) ON account = pinning_account AND item_type = 1 AND item_id = video_id AND episode_season_id IS NULL"

    const/4 v9, -0x1

    move-object v3, p0

    move-object v4, p1

    move-object v7, v6

    move-object v8, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    :cond_0
    const-string p4, "CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END"

    goto :goto_0
.end method
