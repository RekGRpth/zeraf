.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;ZLcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private syncCompleted()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "initial_sync_completed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "initial_sync_completed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->syncCompleted()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Void;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->syncCompleted()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;->onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V

    return-void
.end method
