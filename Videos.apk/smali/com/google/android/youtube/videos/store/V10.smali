.class public Lcom/google/android/youtube/videos/store/V10;
.super Ljava/lang/Object;
.source "V10.java"


# static fields
.field private static final itagPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "itag/(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/store/V10;->itagPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALTER TABLE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    if-eqz p4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SET "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createPurchasesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createSeasonsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createVideosTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createPostersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createShowPostersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createVideoUserDataTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createWishlistTable(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static createPostersTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE posters (poster_video_id TEXT PRIMARY KEY)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createPurchasesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "DROP TABLE IF EXISTS purchases"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE purchases (account TEXT NOT NULL,item_id TEXT NOT NULL,item_type INT NOT NULL,item_uri TEXT,purchase_id TEXT NOT NULL,purchase_type INT NOT NULL,status INT NOT NULL,purchase_timestamp INT NOT NULL,expiration_timestamp INT,purchase_duration_seconds INT,formats TEXT NOT NULL,streamable INT NOT NULL,price_value INT NOT NULL,price_currency TEXT NOT NULL,pricing_info_uri TEXT,hidden INT NOT NULL DEFAULT 0,PRIMARY KEY (account,item_id))"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createSeasonsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE seasons (season_id TEXT NOT NULL PRIMARY KEY,season_title TEXT NOT NULL,show_id TEXT NOT NULL,show_title TEXT NOT NULL,show_description TEXT,show_poster_uri TEXT,show_poster_synced INT NOT NULL DEFAULT 0,episodes_synced INT NOT NULL DEFAULT 0,season_synced_timestamp INT NOT NULL DEFAULT 0)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createShowPostersTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE show_posters (poster_show_id TEXT PRIMARY KEY)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createVideoUserDataTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE video_userdata (pinning_account TEXT NOT NULL,pinning_video_id TEXT NOT NULL,last_playback_is_dirty INT NOT NULL DEFAULT 0,last_playback_start_timestamp INT NOT NULL DEFAULT 0,last_watched_timestamp INT NOT NULL DEFAULT 0,resume_timestamp INT NOT NULL DEFAULT 0,pinned INT NOT NULL DEFAULT 0,pinning_notification_active INT NOT NULL DEFAULT 0,pinning_status INT,pinning_status_reason INT,pinning_drm_error_code INT,is_new_notification_dismissed INT NOT NULL DEFAULT 0,license_video_format INT,license_expiration_timestamp INT,license_last_synced_timestamp INT,license_file_path_key TEXT,license_key_id INT,license_asset_id INT,license_system_id INT,license_force_sync INT,have_subtitles INT NOT NULL DEFAULT 0,download_relative_filepath TEXT,pinning_download_size INT,download_bytes_downloaded INT,download_last_modified TEXT,PRIMARY KEY (pinning_account,pinning_video_id))"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createVideosTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE videos (video_id TEXT PRIMARY KEY,title TEXT NOT NULL,description TEXT,actors TEXT,directors TEXT,writers TEXT,producers TEXT,ratings TEXT,release_year INT,publish_timestamp INT,duration_seconds INT NOT NULL,claimed INT NOT NULL,subtitle_mode INT NOT NULL,default_subtitle_language TEXT,captions_track_uri TEXT,related_uri TEXT,poster_uri TEXT,poster_synced INT NOT NULL DEFAULT 0,episode_season_id TEXT,episode_number_text TEXT,video_synced_timestamp INT NOT NULL DEFAULT 0,stream_formats TEXT)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static createWishlistTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE wishlist (wishlist_account TEXT NOT NULL,wishlist_item_id TEXT NOT NULL,wishlist_item_type TEXT NOT NULL,wishlist_item_state INT NOT NULL,wishlist_in_cloud INT NOT NULL DEFAULT 0,PRIMARY KEY (wishlist_account,wishlist_item_id,wishlist_item_type))"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static parseGDataFormat(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/google/android/youtube/videos/store/V10;->itagPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/4 v2, 0x3

    goto :goto_0

    :sswitch_1
    const/16 v2, 0xe

    goto :goto_0

    :sswitch_2
    const/16 v2, 0x8

    goto :goto_0

    :sswitch_3
    const/16 v2, 0xb

    goto :goto_0

    :sswitch_4
    const/16 v2, 0xc

    goto :goto_0

    :sswitch_5
    const/16 v2, 0xd

    goto :goto_0

    :sswitch_6
    const/16 v2, 0x14

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_2
        0x50 -> :sswitch_3
        0x51 -> :sswitch_4
        0x58 -> :sswitch_5
        0x77 -> :sswitch_6
    .end sparse-switch
.end method

.method public static upgradeFromV1(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/store/V10;->upgradePurchaseAndVideoBlobsFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeTransfersDbFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV3(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "ALTER TABLE video_userdata ADD COLUMN license_force_sync INT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE purchases ADD COLUMN purchase_duration_seconds INT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE purchases SET purchase_duration_seconds = (duration_hours * 60 * 60)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV4(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createSeasonsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createShowPostersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v0, "ALTER TABLE videos ADD COLUMN episode_season_id TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE videos ADD COLUMN episode_number_text TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE videos ADD COLUMN ratings TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE purchases ADD COLUMN hidden INT NOT NULL DEFAULT 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE videos ADD COLUMN video_synced_timestamp INT NOT NULL DEFAULT 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE seasons ADD COLUMN season_synced_timestamp INT NOT NULL DEFAULT 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV5(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV5(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "ALTER TABLE video_userdata ADD COLUMN download_last_modified TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV6(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV6(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "ALTER TABLE videos ADD COLUMN stream_formats TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE OR IGNORE videos SET video_synced_timestamp = 0 WHERE episode_season_id IS NULL"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV7(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV7(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "video_userdata"

    const-string v1, "last_playback_is_dirty"

    const-string v2, "INT NOT NULL DEFAULT 0"

    const-string v3, "0"

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/V10;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "video_userdata"

    const-string v1, "last_playback_start_timestamp"

    const-string v2, "INT NOT NULL DEFAULT 0"

    const-string v3, "last_watched_timestamp"

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/V10;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV8(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV8(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->createWishlistTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV9(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static upgradeFromV9(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "video_userdata"

    const-string v1, "is_new_notification_dismissed"

    const-string v2, "INT NOT NULL DEFAULT 0"

    const-string v3, "1"

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/V10;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "videos"

    const-string v1, "publish_timestamp"

    const-string v2, "INT"

    const-string v3, "NULL"

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/V10;->addColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static upgradePurchaseAndVideoBlobsFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/videos/store/V2;->readPurchaseBlobsFromSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Map;

    move-result-object v7

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/utils/Util;->wipeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/videos/store/V10;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Collection;

    move-object/from16 v0, p1

    invoke-static {v0, v1, v13}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->updateStoredPurchases(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v9

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    if-ge v3, v13, :cond_0

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/youtube/core/model/Purchase;

    iget-object v12, v13, Lcom/google/android/youtube/core/model/Purchase;->purchasedVideo:Lcom/google/android/youtube/core/model/Video;

    const-wide/16 v13, 0x0

    const/4 v15, 0x0

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->buildVideoContentValues(Lcom/google/android/youtube/core/model/Video;JZ)Landroid/content/ContentValues;

    move-result-object v11

    const-string v13, "videos"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "resume_time_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v12, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "last_watched_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v12, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const-wide/16 v14, 0x0

    invoke-interface {v10, v13, v14, v15}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    if-gtz v8, :cond_1

    const-wide/16 v13, 0x0

    cmp-long v13, v5, v13

    if-lez v13, :cond_2

    :cond_1
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string v13, "last_playback_start_timestamp"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v13, "last_watched_timestamp"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v13, "resume_timestamp"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v13, "video_userdata"

    const-string v14, "pinning_account = ? AND pinning_video_id = ?"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v1, v15, v16

    const/16 v16, 0x1

    iget-object v0, v12, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v11, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method private static upgradeTransfersDbFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 24
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "downloads.db"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    :try_start_0
    const-string v3, "transfers"

    sget-object v4, Lcom/google/android/youtube/videos/store/V2$DownloadsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v15

    :goto_0
    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/4 v3, 0x1

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/4 v3, 0x2

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const/4 v3, 0x3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const/4 v3, 0x4

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const/4 v3, 0x5

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/store/V2;->toBundle([B)Landroid/os/Bundle;

    move-result-object v19

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getUserFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getVideoIdFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "pinned"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const/4 v3, 0x2

    move/from16 v0, v21

    if-ne v0, v3, :cond_0

    cmp-long v3, v11, v13

    if-nez v3, :cond_0

    const-string v3, "pinning_status"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "pinning_notification_active"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "have_subtitles"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :goto_1
    const-string v3, "pinning_status_reason"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "pinning_drm_error_code"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    cmp-long v3, v11, v3

    if-lez v3, :cond_1

    const-string v3, "pinning_download_size"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_2
    const-string v3, "download_bytes_downloaded"

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v16, Ljava/io/File;

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, "download_relative_filepath"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "license_ids"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v18

    check-cast v18, Lcom/google/android/youtube/videos/DrmManager$Identifiers;

    if-eqz v18, :cond_2

    const-string v3, "license_last_synced_timestamp"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "license_force_sync"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "license_video_format"

    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/videos/store/V10;->parseGDataFormat(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "license_file_path_key"

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "license_key_id"

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/youtube/videos/DrmManager$Identifiers;->keyId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "license_asset_id"

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/youtube/videos/DrmManager$Identifiers;->assetId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "license_system_id"

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/youtube/videos/DrmManager$Identifiers;->systemId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_3
    const-string v3, "video_userdata"

    const-string v4, "pinning_account = ? AND pinning_video_id = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    const/4 v6, 0x1

    aput-object v23, v5, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v3

    :cond_0
    :try_start_3
    const-string v3, "pinning_status"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "pinning_notification_active"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    :cond_1
    const-string v3, "pinning_download_size"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_2
    const-string v3, "license_key_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "license_asset_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "license_system_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :cond_3
    :try_start_4
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void
.end method
