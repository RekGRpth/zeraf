.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncPurchasesForVideoTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field private final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;)V
    .locals 1
    .param p2    # I
    .param p4    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "userAuth cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    const-string v0, "videoId cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->videoId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 12

    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos"

    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$VideoMetadataQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "video_id = ?"

    new-array v4, v10, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->videoId:Ljava/lang/String;

    aput-object v6, v4, v11

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v9, v10

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->videoId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-static {v4, v5, v11}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMyVideoPurchasesRequest(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    if-nez v9, :cond_1

    move v5, v10

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/core/async/GDataRequest;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    return-void

    :cond_0
    move v9, v11

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move v5, v11

    goto :goto_1
.end method
