.class Lcom/google/android/youtube/videos/store/SyncService$3;
.super Lcom/google/android/youtube/videos/async/CallbackAsFuture;
.source "SyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/SyncService;->syncAllPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/async/CallbackAsFuture",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/SyncService;

.field final synthetic val$syncResult:Landroid/content/SyncResult;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/SyncService;Landroid/content/SyncResult;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/SyncService$3;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/SyncService$3;->val$syncResult:Landroid/content/SyncResult;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Exception;

    instance-of v0, p2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;

    iget v0, v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;->errorLevel:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sync completed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with failure to sync optional data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sync failed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/SyncService$3;->val$syncResult:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/SyncService$3;->onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Void;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sync completed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/SyncService$3;->onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V

    return-void
.end method
