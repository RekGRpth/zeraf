.class public Lcom/google/android/youtube/videos/store/PurchaseStore;
.super Ljava/lang/Object;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;,
        Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;,
        Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    }
.end annotation


# instance fields
.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/Database;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/videos/store/Database;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "database cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "executor cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public getDatabase()Lcom/google/android/youtube/videos/store/Database;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method public getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const-string v1, "request cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "callback cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method
