.class public Lcom/google/android/youtube/videos/store/WishlistStore;
.super Ljava/lang/Object;
.source "WishlistStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;,
        Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;
    }
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "wishlist_item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "wishlist_item_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/Database;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/videos/store/Database;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "database cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "executor cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/WishlistStore;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method


# virtual methods
.method public loadWishlist(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;-><init>(Lcom/google/android/youtube/videos/store/WishlistStore;Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
