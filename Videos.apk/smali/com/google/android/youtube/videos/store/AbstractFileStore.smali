.class public abstract Lcom/google/android/youtube/videos/store/AbstractFileStore;
.super Ljava/lang/Object;
.source "AbstractFileStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private closeIfOpen(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private closeIfOpen(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V
    .locals 1
    .param p1    # Ljava/io/FileOutputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v0
.end method


# virtual methods
.method protected createObjectInputStream(Ljava/io/InputStream;)Ljava/io/ObjectInputStream;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/ObjectInputStream;

    invoke-direct {v0, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method protected abstract generateFilepath(Ljava/lang/Object;)Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/io/File;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    const-string v8, "key may not be null"

    invoke-static {p1, v8}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->createObjectInputStream(Ljava/io/InputStream;)Ljava/io/ObjectInputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v3

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    goto :goto_0

    :catch_0
    move-exception v2

    :goto_1
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error opening cache file (maybe removed). [file="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    new-instance v8, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v8, v2}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v8

    :goto_2
    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/InputStream;)V

    throw v8

    :catchall_1
    move-exception v8

    move-object v5, v6

    goto :goto_2

    :catchall_2
    move-exception v8

    move-object v0, v1

    move-object v5, v6

    goto :goto_2

    :catch_1
    move-exception v2

    move-object v5, v6

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v0, v1

    move-object v5, v6

    goto :goto_1
.end method

.method public getLastModified(Ljava/lang/Object;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    const-string v10, "key may not be null"

    invoke-static {p1, v10}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "element may not be null"

    invoke-static {p1, v10}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-nez v9, :cond_0

    new-instance v10, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "unable to create parent directory: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x0

    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v7, Ljava/io/ObjectOutputStream;

    invoke-direct {v7, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    invoke-virtual {v7, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v7}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V

    return-void

    :catch_0
    move-exception v2

    :goto_0
    :try_start_4
    const-string v10, "Error creating file."

    invoke-static {v10, v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v10, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v10, v2}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v10

    :goto_1
    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->closeIfOpen(Ljava/io/OutputStream;)V

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->syncAndCloseIfOpen(Ljava/io/FileOutputStream;)V

    throw v10

    :catch_1
    move-exception v2

    :goto_2
    :try_start_5
    const-string v10, "Error creating file."

    invoke-static {v10, v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v10, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v10, v2}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v10

    move-object v4, v5

    goto :goto_1

    :catchall_2
    move-exception v10

    move-object v0, v1

    move-object v4, v5

    goto :goto_1

    :catchall_3
    move-exception v10

    move-object v6, v7

    move-object v0, v1

    move-object v4, v5

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v4, v5

    goto :goto_2

    :catch_3
    move-exception v2

    move-object v0, v1

    move-object v4, v5

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v6, v7

    move-object v0, v1

    move-object v4, v5

    goto :goto_2

    :catch_5
    move-exception v2

    move-object v4, v5

    goto :goto_0

    :catch_6
    move-exception v2

    move-object v0, v1

    move-object v4, v5

    goto :goto_0

    :catch_7
    move-exception v2

    move-object v6, v7

    move-object v0, v1

    move-object v4, v5

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    const-string v1, "key may not be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public removeByPrefix(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lcom/google/android/youtube/videos/store/AbstractFileStore$1;

    invoke-direct {v3, p0, v2}, Lcom/google/android/youtube/videos/store/AbstractFileStore$1;-><init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->recursivelyDelete([Ljava/io/File;)V

    goto :goto_0
.end method

.method public touch(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->generateFilepath(Ljava/lang/Object;)Ljava/io/File;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setLastModified(J)Z

    return-void
.end method
