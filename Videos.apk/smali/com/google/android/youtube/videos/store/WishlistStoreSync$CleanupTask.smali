.class Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;
.super Ljava/lang/Object;
.source "WishlistStoreSync.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/WishlistStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupTask"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final request:I

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/WishlistStoreSync;ILcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->request:I

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    const/16 v17, 0x0

    const/16 v20, 0x0

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "wishlist"

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$200()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "wishlist_account"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v12

    :goto_0
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v13

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->request:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v13}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    if-eqz v17, :cond_6

    const/16 v2, 0x8

    :goto_1
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->request:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$300(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v16

    const/4 v14, 0x0

    :goto_3
    move-object/from16 v0, v16

    array-length v2, v0

    if-ge v14, v2, :cond_2

    aget-object v2, v16, v14

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v11, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_2
    const/16 v18, 0x0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v2, "wishlist"

    const-string v3, "wishlist_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v2

    add-int v18, v18, v2

    goto :goto_4

    :cond_3
    if-lez v18, :cond_4

    const/16 v17, 0x1

    :goto_5
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    if-eqz v17, :cond_5

    const/16 v2, 0x8

    :goto_6
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->request:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    const/16 v17, 0x0

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    :catchall_1
    move-exception v2

    move-object v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v4

    if-eqz v17, :cond_8

    const/16 v2, 0x8

    :goto_7
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v4, v0, v1, v2, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v20, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;->request:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_7
    throw v3

    :cond_8
    const/4 v2, 0x0

    goto :goto_7
.end method
