.class Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;
.super Ljava/lang/Object;
.source "WishlistStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/WishlistStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWishlistTask"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final request:Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/WishlistStore;Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p2    # Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "request cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v4, 0x0

    const/4 v0, 0x0

    const-string v1, "wishlist"

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/WishlistStore;->access$000()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->access$100(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->this$0:Lcom/google/android/youtube/videos/store/WishlistStore;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStore;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/WishlistStore;->access$300(Lcom/google/android/youtube/videos/store/WishlistStore;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    # getter for: Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->access$200(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/WishlistStore$LoadWishlistTask;->request:Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    invoke-interface {v0, v1, v8}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
