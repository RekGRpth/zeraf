.class abstract Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "SyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;",
        ">;"
    }
.end annotation


# instance fields
.field protected final callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;"
        }
    .end annotation
.end field

.field protected final priority:I

.field private volatile scheduled:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final ticket:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V
    .locals 1
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->priority:I

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->ticket:I

    return-void
.end method

.method private shouldReportRequiredDataLevelError(Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Exception;

    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsDisabledInMaintenanceModeError()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/google/android/youtube/core/converter/InvalidFormatException;

    goto :goto_0
.end method


# virtual methods
.method public final compareTo(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;)I
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->priority:I

    iget v1, p1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->priority:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->priority:I

    iget v1, p1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->priority:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->ticket:I

    iget v1, p1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->ticket:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->compareTo(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;)I

    move-result v0

    return v0
.end method

.method protected abstract doSync()V
.end method

.method protected final maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->shouldReportRequiredDataLevelError(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->onSyncError(ILjava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method protected final onSyncError(ILjava/lang/Exception;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->onError(ILjava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public final run()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->scheduled:Z

    const-string v1, "sync task run without being scheduled"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->doSync()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->decrement()V

    :cond_0
    return-void
.end method

.method public final schedule()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->scheduled:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->increment()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$200(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
