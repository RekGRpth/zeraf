.class public final Lcom/google/android/youtube/videos/store/V1SubtitleTrack;
.super Ljava/lang/Object;
.source "V1SubtitleTrack.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x68ec098b6117b2cdL


# instance fields
.field public final languageCode:Ljava/lang/String;

.field public final languageName:Ljava/lang/String;

.field public final sourceLanguageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->trackName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->videoId:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 5

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->videoId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createIncomplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->languageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/V1SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v0

    goto :goto_0
.end method
