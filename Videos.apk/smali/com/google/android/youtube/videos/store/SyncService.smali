.class public Lcom/google/android/youtube/videos/store/SyncService;
.super Landroid/app/Service;
.source "SyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/SyncService$DummyContentProvider;,
        Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;
    }
.end annotation


# instance fields
.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private syncAdapter:Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/accounts/Account;)Z
    .locals 1
    .param p0    # Landroid/accounts/Account;

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/SyncService;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/SyncService;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/SyncService;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/SyncService;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/SyncService;->syncVideoPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/SyncService;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/SyncService;->syncSeasonPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/SyncService;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/store/SyncService;->syncAllPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V

    return-void
.end method

.method public static enableSyncForUninitializedAccounts(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)V
    .locals 3
    .param p0    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    aget-object v2, v0, v1

    invoke-static {v2}, Lcom/google/android/youtube/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static enableSyncIfAccountUninitialized(Landroid/accounts/Account;)Z
    .locals 2
    .param p0    # Landroid/accounts/Account;

    const/4 v0, 0x1

    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1, v0}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startSeasonSync(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/accounts/Account;
    .param p1    # Ljava/lang/String;

    const-string v1, "account must not be null"

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "seasonId must not be empty"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "season"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public static startSync(Landroid/accounts/Account;)V
    .locals 1
    .param p0    # Landroid/accounts/Account;

    const-string v0, "account must not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/youtube/videos/store/SyncService;->startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;)V

    return-void
.end method

.method public static startSync(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/accounts/Account;
    .param p1    # Ljava/lang/String;

    const-string v0, "account must not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "videoId must not be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/store/SyncService;->startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;)V

    return-void
.end method

.method private static startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/accounts/Account;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "video"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private syncAllPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Landroid/content/SyncResult;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting sync for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/youtube/videos/store/SyncService$3;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/videos/store/SyncService$3;-><init>(Lcom/google/android/youtube/videos/store/SyncService;Landroid/content/SyncResult;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;ZLcom/google/android/youtube/core/async/Callback;)V

    const-wide/32 v1, 0x1d4c0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/store/SyncService;->waitFor(Lcom/google/android/youtube/videos/async/CallbackAsFuture;J)V

    return-void
.end method

.method private syncSeasonPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/SyncResult;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting sync for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " season "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/videos/store/SyncService$2;

    invoke-direct {v0, p0, p3}, Lcom/google/android/youtube/videos/store/SyncService$2;-><init>(Lcom/google/android/youtube/videos/store/SyncService;Landroid/content/SyncResult;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForSeason(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    const-wide/32 v2, 0x1d4c0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/youtube/videos/store/SyncService;->waitFor(Lcom/google/android/youtube/videos/async/CallbackAsFuture;J)V

    return-void
.end method

.method private syncVideoPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/SyncResult;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting sync for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " video "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/videos/store/SyncService$1;

    invoke-direct {v0, p0, p3}, Lcom/google/android/youtube/videos/store/SyncService$1;-><init>(Lcom/google/android/youtube/videos/store/SyncService;Landroid/content/SyncResult;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    const-wide/32 v2, 0x1d4c0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/youtube/videos/store/SyncService;->waitFor(Lcom/google/android/youtube/videos/async/CallbackAsFuture;J)V

    return-void
.end method

.method private waitFor(Lcom/google/android/youtube/videos/async/CallbackAsFuture;J)V
    .locals 1
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/async/CallbackAsFuture",
            "<TR;TE;>;J)V"
        }
    .end annotation

    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, p3, v0}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "android.content.SyncAdapter"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/SyncService;->syncAdapter:Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/SyncService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    new-instance v1, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;-><init>(Lcom/google/android/youtube/videos/store/SyncService;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService;->syncAdapter:Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;

    return-void
.end method
