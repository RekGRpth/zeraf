.class interface abstract Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder$Query;
.super Ljava/lang/Object;
.source "VideoUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "Query"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "purchase_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "expiration_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_watched_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "poster_synced"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "show_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "season_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "show_poster_synced"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder$Query;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
