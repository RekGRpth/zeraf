.class public final Lcom/google/android/youtube/videos/store/V1Subtitle$Line;
.super Ljava/lang/Object;
.source "V1Subtitle.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/V1Subtitle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Line"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x6071770e3b23befaL


# instance fields
.field public final endTimeMillis:I

.field public final startTimeMillis:I

.field public final text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->text:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->startTimeMillis:I

    iput v1, p0, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->endTimeMillis:I

    return-void
.end method
