.class Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;
.super Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetPurchasesRunnableV16"
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->sql:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$700(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$800(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$800(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/videos/store/CancellationSignalHolder;->cancellationSignal:Landroid/os/CancellationSignal;
    :try_end_1
    .catch Landroid/os/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnableV16;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-interface {v2, v3, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
