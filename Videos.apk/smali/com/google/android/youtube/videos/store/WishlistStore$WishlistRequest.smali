.class public Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;
.super Ljava/lang/Object;
.source "WishlistStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/WishlistStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WishlistRequest"
.end annotation


# instance fields
.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "wishlist_account = ? AND wishlist_item_state != 3"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;

    return-object v0
.end method
