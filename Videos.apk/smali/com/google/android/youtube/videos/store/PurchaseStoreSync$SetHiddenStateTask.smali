.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetHiddenStateTask"
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final hidden:Z

.field private final itemId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    iput-object p4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->account:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->itemId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->whereClause:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->hidden:Z

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 12

    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "hidden"

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->hidden:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    const-string v7, "purchases"

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->whereClause:Ljava/lang/String;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->account:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->itemId:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v3, v7, v0, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-lez v7, :cond_1

    move v1, v4

    :goto_0
    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    if-eqz v1, :cond_2

    move v4, v6

    :goto_1
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v7, v3, v2, v4, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->hidden:Z

    if-eqz v4, :cond_0

    new-instance v4, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->priority:I

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->account:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_0
    return-void

    :cond_1
    move v1, v5

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :catchall_0
    move-exception v4

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    if-eqz v1, :cond_4

    :goto_2
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v7, v3, v2, v6, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->hidden:Z

    if-eqz v5, :cond_3

    new-instance v5, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->priority:I

    iget-object v8, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v9, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->account:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_3
    throw v4

    :cond_4
    move v6, v5

    goto :goto_2
.end method
