.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncSeasonMetadataTask"
.end annotation


# instance fields
.field private season:Lcom/google/android/youtube/core/model/Season;

.field private final seasonCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;"
        }
    .end annotation
.end field

.field private final seasonId:Ljava/lang/String;

.field private final showCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 1
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "seasonId cannot be empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->seasonId:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->seasonCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$2;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->showCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/core/model/Season;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;
    .param p1    # Lcom/google/android/youtube/core/model/Season;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->onSeason(Lcom/google/android/youtube/core/model/Season;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/core/model/Show;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;
    .param p1    # Lcom/google/android/youtube/core/model/Show;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->onShow(Lcom/google/android/youtube/core/model/Show;)V

    return-void
.end method

.method private onError(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V

    return-void
.end method

.method private onSeason(Lcom/google/android/youtube/core/model/Season;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/Season;

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->season:Lcom/google/android/youtube/core/model/Season;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncShowRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->showCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private onShow(Lcom/google/android/youtube/core/model/Show;)V
    .locals 13
    .param p1    # Lcom/google/android/youtube/core/model/Show;

    const/4 v12, 0x6

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->season:Lcom/google/android/youtube/core/model/Season;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->season:Lcom/google/android/youtube/core/model/Season;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->season:Lcom/google/android/youtube/core/model/Season;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->useHqThumbnails:Z
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Z

    move-result v3

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->buildSeasonContentValues(Lcom/google/android/youtube/core/model/Season;Lcom/google/android/youtube/core/model/Show;JZ)Landroid/content/ContentValues;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const/4 v7, 0x0

    :try_start_0
    const-string v0, "seasons"

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1, v9}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    new-array v1, v11, [Ljava/lang/Object;

    aput-object v4, v1, v6

    aput-object v5, v1, v10

    invoke-virtual {v0, v8, v7, v12, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->season:Lcom/google/android/youtube/core/model/Season;

    iget-object v6, v6, Lcom/google/android/youtube/core/model/Season;->episodesUri:Landroid/net/Uri;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->schedule()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->priority:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncShowPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1500(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    new-array v2, v11, [Ljava/lang/Object;

    aput-object v4, v2, v6

    aput-object v5, v2, v10

    invoke-virtual {v1, v8, v7, v12, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method protected doSync()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->seasonId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->seasonCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
