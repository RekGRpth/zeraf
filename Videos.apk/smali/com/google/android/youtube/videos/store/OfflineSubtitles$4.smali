.class Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;
.super Lcom/google/android/youtube/videos/store/AbstractFileStore;
.source "OfflineSubtitles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitlesStore()Lcom/google/android/youtube/videos/store/AbstractFileStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/store/AbstractFileStore",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleTrack;",
        "Lcom/google/android/youtube/core/model/Subtitles;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;->this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/AbstractFileStore;-><init>()V

    return-void
.end method


# virtual methods
.method protected createObjectInputStream(Ljava/io/InputStream;)Ljava/io/ObjectInputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/StreamCorruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;-><init>(Ljava/io/InputStream;)V

    const-string v1, "com.google.android.youtube.core.model.Subtitle"

    const-class v2, Lcom/google/android/youtube/videos/store/V1Subtitle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v1, "com.google.android.youtube.core.model.Subtitle$Line"

    const-class v2, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected generateFilepath(Lcom/google/android/youtube/core/model/SubtitleTrack;)Ljava/io/File;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;->this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    # invokes: Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitlesDir()Ljava/io/File;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->access$400(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".cc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;-><init>(Ljava/lang/Exception;)V

    throw v2
.end method

.method protected bridge synthetic generateFilepath(Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;->generateFilepath(Lcom/google/android/youtube/core/model/SubtitleTrack;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
