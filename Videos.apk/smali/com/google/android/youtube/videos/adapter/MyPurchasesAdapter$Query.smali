.class public abstract Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;
.super Ljava/lang/Object;
.source "MyPurchasesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Query"
.end annotation


# static fields
.field private static final BASE_VOD_PROJECTION:[Ljava/lang/String;

.field private static final EST_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "purchase_id AS _id"

    aput-object v1, v0, v3

    const-string v1, "CASE WHEN (CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END < @NOW) THEN 3 ELSE status END AS merged_status"

    aput-object v1, v0, v4

    const-string v1, "CASE purchase_type WHEN 1 THEN min(ifnull(expiration_timestamp,9223372036854775807),ifnull(license_expiration_timestamp,9223372036854775807)) ELSE ifnull(expiration_timestamp,9223372036854775807) END AS merged_timestamp"

    aput-object v1, v0, v5

    const-string v1, "video_id"

    aput-object v1, v0, v6

    const-string v1, "title"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "release_year"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pinned"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->BASE_VOD_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "purchase_id AS _id"

    aput-object v1, v0, v3

    const-string v1, "status AS merged_status"

    aput-object v1, v0, v4

    const-string v1, "9223372036854775807 AS merged_timestamp"

    aput-object v1, v0, v5

    const-string v1, "video_id"

    aput-object v1, v0, v6

    const-string v1, "title"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "release_year"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pinned"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->EST_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final createEstRequest(Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # J

    sget-object v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->EST_PROJECTION:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createEstMoviesRequestForUser(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method public static final createVodRequest(Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # J

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->BASE_VOD_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->BASE_VOD_PROJECTION:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->BASE_VOD_PROJECTION:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v0, v1, v4

    const-string v2, "@NOW"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v4, "merged_timestamp"

    const-string v5, "merged_status, purchase_timestamp DESC"

    move-object v0, p0

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createVodMoviesRequestForUser(Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method
