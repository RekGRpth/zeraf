.class public Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MovieSuggestionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final calendar:Ljava/util/Calendar;

.field private isNetworkConnected:Z

.field private final missingPoster:Landroid/graphics/Bitmap;

.field private final posterArtRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p3    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->activity:Landroid/app/Activity;

    const-string v0, "posterArtRequester cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->posterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "missingPoster cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->missingPoster:Landroid/graphics/Bitmap;

    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->calendar:Ljava/util/Calendar;

    return-void
.end method

.method private getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I
    .locals 5
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->calendar:Ljava/util/Calendar;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getReleaseDateTimestampSec()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->calendar:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    if-eqz p2, :cond_1

    move-object v3, p2

    check-cast v3, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I

    move-result v6

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDurationSec()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->setInfo(Ljava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getPosterCount()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v0, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getPoster(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->activity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->posterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v7, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->missingPoster:Landroid/graphics/Bitmap;

    invoke-static {v5, v3, v6, v2, v7}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->isNetworkConnected:Z

    if-nez v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->setDimmedStyle(Z)V

    return-object v3

    :cond_1
    new-instance v3, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;

    iget-object v5, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->activity:Landroid/app/Activity;

    invoke-direct {v3, v5}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->isNetworkConnected:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->isNetworkConnected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
