.class public Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;
.super Lcom/google/android/youtube/videos/adapter/ViewOutline;
.source "DimmableViewOutline.java"


# instance fields
.field private final defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private final dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;)V
    .locals 8
    .param p1    # Landroid/widget/FrameLayout;

    const/4 v7, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/adapter/ViewOutline;-><init>(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f01000c

    invoke-virtual {v4, v5, v2, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x2

    new-array v1, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const v6, 0x7f08000e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v5, v1, v4

    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v1, v7

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v4, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v4}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public setDimmedStyle(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
