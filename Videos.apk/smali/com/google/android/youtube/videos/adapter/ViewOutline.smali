.class public Lcom/google/android/youtube/videos/adapter/ViewOutline;
.super Lcom/google/android/youtube/core/adapter/SingleViewOutline;
.source "ViewOutline.java"


# instance fields
.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    sget-object v0, Lcom/google/android/youtube/videos/adapter/ViewOutline;->IGNORE_VIEW_TYPE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/adapter/SingleViewOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    const-string v0, "view cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/ViewOutline;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/ViewOutline;->view:Landroid/view/View;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/ViewOutline;->view:Landroid/view/View;

    return-object v0
.end method
