.class public Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;
.super Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;
.source "CollapsiblePanelHeadingOutline.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected final bodyOutline:Lcom/google/android/youtube/core/adapter/Outline;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/core/adapter/Outline;

    const v0, 0x7f04000b

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/adapter/Outline;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->bodyOutline:Lcom/google/android/youtube/core/adapter/Outline;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->bodyOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/adapter/Outline;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f020032

    :goto_0
    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    const v0, 0x7f020030

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->bodyOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->toggleVisibility()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->notifyOutlineChanged()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->onToggledBodyVisibility()V

    return-void
.end method

.method protected onToggledBodyVisibility()V
    .locals 0

    return-void
.end method
