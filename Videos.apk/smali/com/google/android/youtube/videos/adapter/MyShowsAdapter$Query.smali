.class public abstract Lcom/google/android/youtube/videos/adapter/MyShowsAdapter$Query;
.super Ljava/lang/Object;
.source "MyShowsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Query"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "show_id AS _id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "show_title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SUM(pinning_status=3)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter$Query;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
