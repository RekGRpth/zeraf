.class public Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;
.super Landroid/widget/CursorAdapter;
.source "MyPurchasesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final allowDownloads:Z

.field private isNetworkConnected:Z

.field private final missingBitmap:Landroid/graphics/Bitmap;

.field private final videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->activity:Landroid/app/Activity;

    const-string v0, "missingBitmap cannot be null"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->missingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPosterRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->allowDownloads:Z

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object/from16 v9, p1

    check-cast v9, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;

    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isExpired(Landroid/database/Cursor;)Z

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    const/4 v6, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->missingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v9, v5, v6, v0}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/16 v2, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isPinned(Landroid/database/Cursor;)Z

    move-result v3

    const/16 v2, 0xb

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v7

    const/16 v2, 0xa

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v8

    if-eqz v8, :cond_2

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v17, 0x0

    cmp-long v2, v5, v17

    if-lez v2, :cond_2

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v17, 0x64

    mul-long v5, v5, v17

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    div-long v5, v5, v17

    long-to-int v0, v5

    move/from16 v19, v0

    :goto_0
    const/16 v16, 0x0

    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->activity:Landroid/app/Activity;

    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-static {v0, v5}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v6, 0x9

    move-object/from16 v0, p3

    invoke-static {v0, v6}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static/range {v2 .. v8}, Lcom/google/android/youtube/videos/pinning/PinningStatusHelper;->getDownloadStatusText(Landroid/content/Context;ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->allowDownloads:Z

    move/from16 v20, v0

    move/from16 v17, v3

    move-object/from16 v18, v4

    invoke-virtual/range {v9 .. v20}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->setInfo(Ljava/lang/String;IIZJLjava/lang/String;ZLjava/lang/Integer;IZ)V

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v5, 0x3

    if-ne v2, v5, :cond_3

    const/16 v21, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isNetworkConnected:Z

    if-nez v2, :cond_0

    if-eqz v21, :cond_1

    :cond_0
    if-eqz v13, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v9, v2}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->setDimmedStyle(Z)V

    return-void

    :cond_2
    const/16 v19, 0x0

    goto :goto_0

    :cond_3
    const/16 v21, 0x0

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isExpired(Landroid/database/Cursor;)Z
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinned(Landroid/database/Cursor;)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isNetworkConnected:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isNetworkConnected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
