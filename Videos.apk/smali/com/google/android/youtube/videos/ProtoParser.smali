.class public Lcom/google/android/youtube/videos/ProtoParser;
.super Ljava/lang/Object;
.source "ProtoParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lcom/google/protobuf/MessageLite;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final builder:Lcom/google/protobuf/MessageLite$Builder;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/MessageLite$Builder;)V
    .locals 0
    .param p1    # Lcom/google/protobuf/MessageLite$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ProtoParser;->builder:Lcom/google/protobuf/MessageLite$Builder;

    return-void
.end method

.method public static create(Lcom/google/protobuf/GeneratedMessageLite$Builder;)Lcom/google/android/youtube/videos/ProtoParser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Lcom/google/protobuf/GeneratedMessageLite;",
            ">(",
            "Lcom/google/protobuf/GeneratedMessageLite$Builder",
            "<TS;*>;)",
            "Lcom/google/android/youtube/videos/ProtoParser",
            "<TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/ProtoParser;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/ProtoParser;-><init>(Lcom/google/protobuf/MessageLite$Builder;)V

    return-object v0
.end method


# virtual methods
.method public parseFrom(Ljava/io/InputStream;)Lcom/google/protobuf/MessageLite;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TS;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ProtoParser;->builder:Lcom/google/protobuf/MessageLite$Builder;

    invoke-interface {v0}, Lcom/google/protobuf/MessageLite$Builder;->clone()Lcom/google/protobuf/MessageLite$Builder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/protobuf/MessageLite$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/MessageLite$Builder;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/MessageLite$Builder;->build()Lcom/google/protobuf/MessageLite;

    move-result-object v0

    return-object v0
.end method
