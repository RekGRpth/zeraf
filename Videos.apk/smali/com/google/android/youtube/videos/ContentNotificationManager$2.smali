.class Lcom/google/android/youtube/videos/ContentNotificationManager$2;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/ContentNotificationManager;->getNewEpisodesCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$seasonId:Ljava/lang/String;

.field final synthetic val$showId:Ljava/lang/String;

.field final synthetic val$showTitle:Ljava/lang/String;

.field final synthetic val$videos:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$seasonId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$showId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$videos:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$showTitle:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "Could not get poster for show with id %s."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$showId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$seasonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$showId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$videos:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;->val$showTitle:Ljava/lang/String;

    move-object v4, p2

    # invokes: Lcom/google/android/youtube/videos/ContentNotificationManager;->onNewEpisodes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/videos/ContentNotificationManager;->access$100(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method
