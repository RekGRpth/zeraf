.class public interface abstract Lcom/google/android/youtube/videos/Config;
.super Ljava/lang/Object;
.source "Config.java"


# virtual methods
.method public abstract abrFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract allowDownloads()Z
.end method

.method public abstract atHomeHdmiDisconnectTimeoutMillis()J
.end method

.method public abstract atHomeIdleTimeoutMillis()J
.end method

.method public abstract atHomeRobotTokenRequestUri()Ljava/lang/String;
.end method

.method public abstract atHomeVolumeStepPercent()I
.end method

.method public abstract baseApiUri()Landroid/net/Uri;
.end method

.method public abstract baseKnowledgeUri()Landroid/net/Uri;
.end method

.method public abstract bufferingEventsBeforeQualityDrop()I
.end method

.method public abstract bufferingSettleMillis()J
.end method

.method public abstract bufferingWindowMillis()J
.end method

.method public abstract defaultWelcomeVertical()Ljava/lang/String;
.end method

.method public abstract deviceCountry()Ljava/lang/String;
.end method

.method public abstract downloadFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fallbackDrmErrorCodes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract feedbackProductId()I
.end method

.method public abstract feedbackSubmitUrlTemplate()Ljava/lang/String;
.end method

.method public abstract feedbackTokenUrl()Ljava/lang/String;
.end method

.method public abstract fieldProvisionedFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract forceAppLevelDrm()Z
.end method

.method public abstract forceMirrorMode()Z
.end method

.method public abstract gcmRegistrationEnabled()Z
.end method

.method public abstract gdataVersion()Lcom/google/android/youtube/core/async/GDataRequest$Version;
.end method

.method public abstract getAnalyticsCategorySuffix()Ljava/lang/String;
.end method

.method public abstract getAnalyticsEnabled()Z
.end method

.method public abstract getAnalyticsPropertyId()Ljava/lang/String;
.end method

.method public abstract getAnalyticsSampleRatio()I
.end method

.method public abstract getAnalyticsUpdateSecs()I
.end method

.method public abstract getExperimentId()Ljava/lang/String;
.end method

.method public abstract gmsCoreAvailable()Z
.end method

.method public abstract gservicesId()J
.end method

.method public abstract hdStreamingFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isAtHomeDevice()Z
.end method

.method public abstract knowledgeEnabled()Z
.end method

.method public abstract knowledgeFeedbackTypeId()I
.end method

.method public abstract knowledgeRecheckDataAfterMillis()J
.end method

.method public abstract knowledgeShowAllCards()Z
.end method

.method public abstract knowledgeShowRecentActorsWithinMillis()I
.end method

.method public abstract knowledgeWelcomeBrowseUri()Landroid/net/Uri;
.end method

.method public abstract knowledgeWelcomeGraphic()Landroid/net/Uri;
.end method

.method public abstract localVerticalEnabled()Z
.end method

.method public abstract lqFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract maxConcurrentLicenseTasks()I
.end method

.method public abstract maxConcurrentOrBackedOffPinningTasks()I
.end method

.method public abstract maxConcurrentUpdateUserdataTasks()I
.end method

.method public abstract maxLicenseTaskRetryDelayMillis()I
.end method

.method public abstract maxNewContentNotificationDelayMillis()J
.end method

.method public abstract maxPinningTaskRetries()I
.end method

.method public abstract maxPinningTaskRetryDelayMillis()I
.end method

.method public abstract maxResumeTimeValidityMillis()J
.end method

.method public abstract maxUpdateUserdataTaskRetries()I
.end method

.method public abstract maxUpdateUserdataTaskRetryDelayMillis()I
.end method

.method public abstract minLicenseTaskRetryDelayMillis()I
.end method

.method public abstract minPinningTaskRetryDelayMillis()I
.end method

.method public abstract minUpdateUserdataTaskRetryDelayMillis()I
.end method

.method public abstract minimumVersion()I
.end method

.method public abstract mobileDownloadsEnabled()Z
.end method

.method public abstract mobileStreamingEnabled()Z
.end method

.method public abstract moviesVerticalEnabled()Z
.end method

.method public abstract moviesWelcomeBrowseUri()Landroid/net/Uri;
.end method

.method public abstract moviesWelcomeGraphic()Landroid/net/Uri;
.end method

.method public abstract moviesWelcomeIsFree()Z
.end method

.method public abstract needsSystemUpdate()Z
.end method

.method public abstract ratingScheme()Ljava/lang/String;
.end method

.method public abstract refreshLicensesOlderThanMillis()J
.end method

.method public abstract resyncSeasonEpisodesAfterMillis()J
.end method

.method public abstract resyncVideoAfterMillis()J
.end method

.method public abstract rootedAppLevelDrmEnabled()Z
.end method

.method public abstract rootedFrameworkLevelDrmEnabled()Z
.end method

.method public abstract sdStreamingFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showsVerticalEnabled()Z
.end method

.method public abstract showsWelcomeBrowseUri()Landroid/net/Uri;
.end method

.method public abstract showsWelcomeGraphic()Landroid/net/Uri;
.end method

.method public abstract showsWelcomeIsFree()Z
.end method

.method public abstract surroundSoundFormats()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract transferServicePingIntervalMillis()J
.end method

.method public abstract useGDataMovieSuggestions()Z
.end method

.method public abstract useGDataRelatedMovieSuggestions()Z
.end method

.method public abstract useGDataShowSuggestions()Z
.end method

.method public abstract usePlayAnalytics()Z
.end method

.method public abstract useSslForDownloads()Z
.end method

.method public abstract wvDrmServerUri()Ljava/lang/String;
.end method

.method public abstract wvPortalName()Ljava/lang/String;
.end method
