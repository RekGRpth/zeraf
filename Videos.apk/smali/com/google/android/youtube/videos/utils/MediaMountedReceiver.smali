.class public Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaMountedReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final listener:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;

.field private volatile mediaMounted:Z

.field private registered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->listener:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->registered:Z

    return-void
.end method


# virtual methods
.method public isMediaMounted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->mediaMounted:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->mediaMounted:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->listener:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->mediaMounted:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;->onMediaMountedChanged(Z)V

    return-void
.end method

.method public register()V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->registered:Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->mediaMounted:Z

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->registered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->registered:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
