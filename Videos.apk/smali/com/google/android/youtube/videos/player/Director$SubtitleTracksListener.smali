.class Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleTracksListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/player/Director;
    .param p2    # Lcom/google/android/youtube/videos/player/Director$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    return-void
.end method


# virtual methods
.method public onDefaultSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3800(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/player/PlaybackHelper;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->selectTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public onSubtitleTracksError()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$1900(Lcom/google/android/youtube/videos/player/Director;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a010a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    return-void
.end method

.method public onSubtitleTracksResponse(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showTrackSelector(Ljava/util/List;)V

    return-void
.end method

.method public onTrackSelectionDisabled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHasCc(Z)V

    return-void
.end method

.method public onTrackSelectionEnabled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHasCc(Z)V

    return-void
.end method
