.class Lcom/google/android/youtube/videos/player/Director$8;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$8;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/api/VideoGetRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/api/VideoGetRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$8;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onGetVideoResourceFailed(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/player/Director;->access$1800(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/api/VideoGetRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$8;->onError(Lcom/google/android/youtube/videos/api/VideoGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/api/VideoGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/api/VideoGetRequest;
    .param p2    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$8;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onGetVideoResource(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/player/Director;->access$1700(Lcom/google/android/youtube/videos/player/Director;Lcom/google/wireless/android/video/magma/proto/VideoResource;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/api/VideoGetRequest;

    check-cast p2, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$8;->onResponse(Lcom/google/android/youtube/videos/api/VideoGetRequest;Lcom/google/wireless/android/video/magma/proto/VideoResource;)V

    return-void
.end method
