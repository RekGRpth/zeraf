.class public Lcom/google/android/youtube/videos/player/Director;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
.implements Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/Director$13;,
        Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;,
        Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;,
        Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;,
        Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;,
        Lcom/google/android/youtube/videos/player/Director$DrmCallback;,
        Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;,
        Lcom/google/android/youtube/videos/player/Director$State;,
        Lcom/google/android/youtube/videos/player/Director$Listener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final activity:Landroid/app/Activity;

.field private appLevelDrm:Z

.field private final atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

.field private cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

.field private cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<**>;"
        }
    .end annotation
.end field

.field private cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<**>;"
        }
    .end annotation
.end field

.field private captionsTrackUri:Landroid/net/Uri;

.field private claimed:Z

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

.field private controlsHiddenForDialog:Z

.field private defaultSubtitleLanguage:Ljava/lang/String;

.field private displaySupportsProtectedBuffers:Z

.field private drmIdentifiers:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private durationMillis:I

.field private final errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final handler:Landroid/os/Handler;

.field private initialDrmLevel:I

.field private knowledgeCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;"
        }
    .end annotation
.end field

.field private final knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

.field private final knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

.field private lastWatchedTimestamp:J

.field private final legacyDownloadsHaveAppLevelDrm:Z

.field private final listener:Lcom/google/android/youtube/videos/player/Director$Listener;

.field private final localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

.field private final maxResumeTimeValidityMillis:J

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private pendingShortClockActivation:Z

.field private permittedStreams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private pinnedToThisDevice:Z

.field private playWhenInitialized:Z

.field private final playerView:Lcom/google/android/youtube/core/player/PlayerView;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private final purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private purchasedFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resumeTimeMillis:I

.field private final retryRunnable:Ljava/lang/Runnable;

.field private selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

.field private shortClockActivated:Z

.field private shortClockMillis:J

.field private state:Lcom/google/android/youtube/videos/player/Director$State;

.field private final storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

.field private storedPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation
.end field

.field private final streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

.field private subtitleMode:I

.field private final subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

.field private syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field private userConfirmedShortClockActivation:Z

.field private videoGetCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoGetRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoId:Ljava/lang/String;

.field private videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/core/player/PlayerView;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;Lcom/google/android/youtube/videos/player/Director$Listener;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 27
    .param p1    # Lcom/google/android/youtube/videos/VideosApplication;
    .param p2    # Lcom/google/android/youtube/core/player/PlayerView;
    .param p3    # Landroid/app/Activity;
    .param p4    # Landroid/content/SharedPreferences;
    .param p5    # Lcom/google/android/youtube/videos/Requesters;
    .param p7    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p8    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p9    # Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;
    .param p10    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p11    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p12    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p13    # Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    .param p14    # Lcom/google/android/youtube/videos/player/Director$Listener;
    .param p15    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p16    # Lcom/google/android/youtube/videos/StreamsSelector;
    .param p17    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p18    # Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    .param p19    # Z
    .param p20    # J
    .param p22    # Ljava/lang/String;
    .param p23    # Ljava/lang/String;
    .param p24    # Ljava/lang/String;
    .param p25    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/VideosApplication;",
            "Lcom/google/android/youtube/core/player/PlayerView;",
            "Landroid/app/Activity;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/youtube/videos/Requesters;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/client/SubtitlesClient;",
            "Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;",
            "Lcom/google/android/youtube/videos/store/PurchaseStore;",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/youtube/videos/drm/DrmManager;",
            "Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;",
            "Lcom/google/android/youtube/videos/player/Director$Listener;",
            "Lcom/google/android/youtube/core/utils/NetworkStatus;",
            "Lcom/google/android/youtube/videos/StreamsSelector;",
            "Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeClient;",
            "ZJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-string v3, "playerView cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    const-string v3, "activity cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->config:Lcom/google/android/youtube/videos/Config;

    const-string v3, "listener cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/player/Director$Listener;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    const-string v3, "purchaseStore cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    const-string v3, "purchaseStoreSync cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    const-string v3, "accountManagerWrapper cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/youtube/videos/Requesters;->getPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/youtube/videos/Requesters;->getStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v3, "videoGetRequester cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v3, "preferences can\'t be null"

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/player/Director;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    const-string v3, "networkStatus cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    const-string v3, "streamsSelector cannot be null"

    move-object/from16 v0, p16

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/StreamsSelector;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    move/from16 v0, p19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/player/Director;->legacyDownloadsHaveAppLevelDrm:Z

    move-wide/from16 v0, p20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/youtube/videos/player/Director;->maxResumeTimeValidityMillis:J

    const-string v3, "account cannot be null"

    move-object/from16 v0, p24

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    const-string v3, "videoId cannot be null"

    move-object/from16 v0, p22

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    const-string v3, "eventLogger cannot be null"

    move-object/from16 v0, p25

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/videos/player/Director$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/player/Director$1;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->retryRunnable:Ljava/lang/Runnable;

    invoke-virtual/range {p3 .. p3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f040030

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v3, v5, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v26

    new-instance v18, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v3

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/view/View;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    new-instance v17, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    if-eqz p18, :cond_0

    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    new-instance v3, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    new-instance v3, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getWishlistStore()Lcom/google/android/youtube/videos/store/WishlistStore;

    move-result-object v6

    move-object/from16 v0, p3

    move-object/from16 v1, p10

    invoke-direct {v3, v0, v5, v1, v6}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/WishlistStore;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    new-instance v3, Lcom/google/android/youtube/videos/player/Director$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/videos/player/Director$2;-><init>(Lcom/google/android/youtube/videos/player/Director;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    const/4 v5, 0x0

    aput-object v17, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    aput-object v6, v3, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    sget-object v5, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-virtual {v3, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setFullscreen(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setShowFullscreen(Z)V

    new-instance v4, Landroid/os/Handler;

    invoke-virtual/range {p3 .. p3}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v4, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    new-instance v6, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v5}, Lcom/google/android/youtube/videos/player/Director$SubtitleTracksListener;-><init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$1;)V

    const v5, 0x7f0a0109

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v5, p4

    move-object/from16 v7, p8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    new-instance v5, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/player/Director;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v16, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/VideosApplication;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-object/from16 v22, v0

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p15

    move-object/from16 v10, p9

    move-object/from16 v11, p8

    move-object/from16 v12, p13

    move-object/from16 v13, p12

    move-object v14, v4

    move-object/from16 v15, p2

    move-object/from16 v21, p0

    move-object/from16 v23, p22

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    invoke-direct/range {v5 .. v25}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;Lcom/google/android/youtube/videos/drm/DrmManager;Landroid/os/Handler;Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/youtube/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    new-instance v5, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/youtube/videos/Requesters;->getRobotTokenRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v6, p3

    move-object/from16 v7, p17

    move-object/from16 v10, v18

    move-object/from16 v11, p22

    move-object/from16 v12, p23

    invoke-direct/range {v5 .. v12}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/player/Director;->initCallbacks()V

    sget-object v3, Lcom/google/android/youtube/videos/player/Director$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/Director$State;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    return-void

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    new-instance v3, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    const/4 v5, 0x0

    aput-object v17, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    aput-object v6, v3, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/player/Director;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/player/Director;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->init(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/player/Director;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onSyncedPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onSyncPurchasesError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/PurchaseStore;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/player/Director;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onStoredPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/videos/player/Director;Ljava/util/Map;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onPermittedStreamsResponse(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onPermittedStreamsError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/youtube/videos/player/Director;Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onGetVideoResource(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onGetVideoResourceFailed(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/videos/player/Director;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/youtube/videos/player/Director;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/Config;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->config:Lcom/google/android/youtube/videos/Config;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/google/android/youtube/videos/player/Director;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/Director;->userConfirmedShortClockActivation:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/google/android/youtube/videos/player/Director;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->warnAndPlayVideo(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/utils/NetworkStatus;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/youtube/videos/player/Director;ZII)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Ljava/lang/Throwable;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/youtube/videos/player/Director;)J
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-wide v0, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    return-wide v0
.end method

.method static synthetic access$3100(Lcom/google/android/youtube/videos/player/Director;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onLicensesResponse(Z)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/youtube/videos/player/Director;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onDrmFallback(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->acquireLicenses()V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->getUserAuth(Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/model/Stream;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->getKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/player/PlaybackHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/player/Director;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;
    .param p1    # Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/Director;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    return-object v0
.end method

.method private acquireLicenses()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/16 v7, 0x10

    invoke-static {v5, v6, v7}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x2

    :goto_0
    invoke-direct {v0, p0, v5}, Lcom/google/android/youtube/videos/player/Director$DrmCallback;-><init>(Lcom/google/android/youtube/videos/player/Director;I)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/player/Director;->createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v5, v2, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_1
    return-void

    :cond_0
    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/player/Director;->createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v5, v2, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v5, v3, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_1
.end method

.method private computeAndLogDrmErrorMessage(Lcom/google/android/youtube/videos/drm/DrmException;)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmException;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const v9, 0x7f0a0091

    const/4 v7, 0x1

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$13;->$SwitchMap$com$google$android$youtube$videos$drm$DrmException$DrmError:[I

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v3, v7

    move v8, v9

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    iget-object v4, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget v5, p1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitDrmError(Ljava/lang/String;ZZLcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    if-ne v8, v9, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    new-array v1, v7, [Ljava/lang/Object;

    iget v2, p1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v8, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->pinnedToThisDevice:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a008a

    move v3, v6

    move v8, v0

    goto :goto_0

    :cond_0
    const v0, 0x7f0a0089

    move v3, v7

    move v8, v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a011c

    move v3, v6

    move v8, v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a00a7

    move v3, v6

    move v8, v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a00a8

    move v3, v6

    move v8, v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a0080

    move v3, v7

    move v8, v0

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a00a6

    move v3, v6

    move v8, v0

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0a0083

    move v3, v6

    move v8, v0

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0a0092

    move v3, v7

    move v8, v0

    goto :goto_0

    :pswitch_8
    move v3, v7

    move v8, v9

    goto :goto_0

    :pswitch_9
    move v3, v7

    move v8, v9

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->drmIdentifiers:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/player/Director;->appLevelDrm:Z

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createOfflineRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->drmIdentifiers:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->appLevelDrm:Z

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createOfflineRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->appLevelDrm:Z

    invoke-static {p1, v0, p2, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createStreamingRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    goto :goto_0
.end method

.method private decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    return-object v1
.end method

.method private decorateKnowledgeCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    return-object v1
.end method

.method private getKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    invoke-static {v2, v3, v4, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->createWithCurrentLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/player/Director;->decorateKnowledgeCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->pinnedToThisDevice:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    invoke-interface {v2, v1, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeClient;->requestPinnedKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    invoke-interface {v2, v1, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeClient;->requestKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method private getUserAuth(Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;

    new-instance v0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    return-void
.end method

.method private init(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->init()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->displaySupportsProtectedBuffers:Z
    :try_end_0
    .catch Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->isSecure()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-interface {v2, v3, v6, v5, v4}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZI)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    const v4, 0x7f0a008c

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v5}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZING:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/drm/DrmManager;->getDrmLevel()I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    iget v2, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    if-gez v2, :cond_1

    sget-object v2, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitUnlockedDeviceError(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    const v4, 0x7f0a00a7

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v5}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->displaySupportsProtectedBuffers:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->alreadyPlaying()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    iget-object v3, v1, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->title:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/videos/player/Director$Listener;->onVideoTitle(Ljava/lang/String;)V

    :cond_3
    sget-object v2, Lcom/google/android/youtube/videos/player/Director$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v2, v6}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->initVideo()V

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showPaused()V

    goto :goto_1
.end method

.method private initCallbacks()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/player/Director$4;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/player/Director$4;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$5;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/videos/player/Director$5;-><init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/async/Callback;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$6;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/Director$6;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->storedPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$7;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/Director$7;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$8;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/Director$8;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->videoGetCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$9;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/Director$9;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method private initVideo()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput v3, p0, Lcom/google/android/youtube/videos/player/Director;->durationMillis:I

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/player/Director;->claimed:Z

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->captionsTrackUri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->defaultSubtitleLanguage:Ljava/lang/String;

    iput v3, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleMode:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreams:Ljava/util/Map;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->drmIdentifiers:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/player/Director;->pinnedToThisDevice:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->storedPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    goto :goto_0
.end method

.method private maybeGetKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "enable_info_cards"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->reset()V

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/model/StreamExtra;->NO_EXTRA:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/StreamExtra;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;-><init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/model/Stream;)V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/player/Director;->decorateKnowledgeCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->getKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V

    goto :goto_0
.end method

.method private onDrmFallback(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitDrmFallbackError(Ljava/lang/String;I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    const v2, 0x7f0a00a7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, v3}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->selectVideoStreams(I)V

    goto :goto_0
.end method

.method private onGetVideoResource(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 4
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/Director;->lastWatchedTimestamp:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/youtube/videos/player/Director;->resumeTimeMillis:I

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/player/Director;->lastWatchedTimestamp:J

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->onGetVideoResourceCompleted()V

    return-void
.end method

.method private onGetVideoResourceCompleted()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/Director;->lastWatchedTimestamp:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/Director;->maxResumeTimeValidityMillis:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iput v4, p0, Lcom/google/android/youtube/videos/player/Director;->resumeTimeMillis:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget v1, p0, Lcom/google/android/youtube/videos/player/Director;->resumeTimeMillis:I

    iget v2, p0, Lcom/google/android/youtube/videos/player/Director;->durationMillis:I

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setTimes(III)V

    new-instance v0, Lcom/google/android/youtube/videos/player/Director$3;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/player/Director$3;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->getUserAuth(Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V

    return-void
.end method

.method private onGetVideoResourceFailed(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->onGetVideoResourceCompleted()V

    return-void
.end method

.method private onInitializationError(Ljava/lang/Throwable;)V
    .locals 6
    .param p1    # Ljava/lang/Throwable;

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZLjava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1, v5}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    sget-object v1, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    return-void
.end method

.method private onInitializationError(ZII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZI)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-virtual {v1, p3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    return-void
.end method

.method private onLicensesResponse(Z)V
    .locals 1
    .param p1    # Z

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_OK:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockActivated:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->warnAndPlayVideo(Z)V

    :cond_0
    return-void
.end method

.method private onNoOfflineStream()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/api/VideoGetRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/videos/api/VideoGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->videoGetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private onPermittedStreamsError(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    instance-of v0, p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    check-cast p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v1, p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v1, v1, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lcom/google/android/youtube/core/player/MissingStreamException;

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    const v1, 0x7f0a0114

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private onPermittedStreamsResponse(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreams:Ljava/util/Map;

    iget v0, p0, Lcom/google/android/youtube/videos/player/Director;->initialDrmLevel:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->selectVideoStreams(I)V

    return-void
.end method

.method private onStoredPurchaseCursor(Landroid/database/Cursor;)V
    .locals 20
    .param p1    # Landroid/database/Cursor;

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/youtube/videos/player/Director;->readCursor(Landroid/database/Cursor;)V

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LAST_WATCHED_TIMESTAMP:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/youtube/videos/player/Director;->lastWatchedTimestamp:J

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->RESUME_TIMESTAMP:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/youtube/videos/player/Director;->resumeTimeMillis:I

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const/4 v2, 0x2

    move/from16 v0, v18

    if-eq v0, v2, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    :try_start_2
    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->PINNED:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x3

    new-array v2, v2, [I

    const/4 v3, 0x0

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_KEY_ID:I

    aput v4, v2, v3

    const/4 v3, 0x1

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_ASSET_ID:I

    aput v4, v2, v3

    const/4 v3, 0x2

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_SYSTEM_ID:I

    aput v4, v2, v3

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->isAnyNotNull(Landroid/database/Cursor;[I)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/player/Director;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/player/Director;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->retryRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    const v4, 0x7f0a00ac

    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_3
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x1

    :try_start_5
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/videos/player/Director;->pinnedToThisDevice:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    if-ne v2, v3, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x5

    :try_start_6
    new-array v2, v2, [I

    const/4 v3, 0x0

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    aput v4, v2, v3

    const/4 v3, 0x1

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_BYTES_DOWNLOADED:I

    aput v4, v2, v3

    const/4 v3, 0x2

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_KEY_ID:I

    aput v4, v2, v3

    const/4 v3, 0x3

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_ASSET_ID:I

    aput v4, v2, v3

    const/4 v3, 0x4

    sget v4, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_SYSTEM_ID:I

    aput v4, v2, v3

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->isAnyNull(Landroid/database/Cursor;[I)Z

    move-result v2

    if-nez v2, :cond_6

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_BYTES_DOWNLOADED:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    :cond_6
    const/4 v2, 0x0

    const/16 v3, 0xd

    const v4, 0x7f0a0094

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_7
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;)Ljava/io/File;
    :try_end_7
    .catch Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v15

    :try_start_8
    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_SIZE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_LAST_MODIFIED:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    sget v2, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_VIDEO_FORMAT:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    sget v3, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_KEY_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    sget v5, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_ASSET_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    sget v7, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_SYSTEM_ID:I

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/player/Director;->drmIdentifiers:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v15, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    new-instance v19, Lcom/google/android/youtube/core/model/VideoStreams;

    new-instance v2, Lcom/google/android/youtube/core/model/Stream$Builder;

    invoke-direct {v2}, Lcom/google/android/youtube/core/model/Stream$Builder;-><init>()V

    invoke-virtual {v2, v11}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    invoke-direct {v3}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;-><init>()V

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes(J)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified(Ljava/lang/String;)Lcom/google/android/youtube/core/model/StreamExtra$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->build()Lcom/google/android/youtube/core/model/StreamExtra;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->extra(Lcom/google/android/youtube/core/model/StreamExtra;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/youtube/core/model/Stream$Builder;->gdataFormat(I)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/core/model/Stream$Quality;->UNKNOWN:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->quality(Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    const-string v3, "video/wvm"

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->mimeType(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/google/android/youtube/core/model/VideoStreams;-><init>(Lcom/google/android/youtube/core/model/Stream;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/youtube/videos/player/Director;->legacyDownloadsHaveAppLevelDrm:Z

    invoke-static {v14, v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->isAppLevelDrmEncrypted(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_8

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/youtube/videos/player/Director;->displaySupportsProtectedBuffers:Z

    if-nez v2, :cond_8

    const/4 v2, 0x0

    const/16 v3, 0xf

    const v4, 0x7f0a008b

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catch_0
    move-exception v10

    const/4 v2, 0x0

    const/16 v3, 0xe

    const v4, 0x7f0a0095

    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_8
    :try_start_a
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/player/Director;->maybeGetKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v9}, Lcom/google/android/youtube/videos/player/Director;->onVideoStreams(Lcom/google/android/youtube/core/model/VideoStreams;Z)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private onSyncPurchasesError(Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/Exception;

    const/4 v3, 0x0

    instance-of v1, p1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    const v2, 0x7f0a00ac

    invoke-direct {p0, v1, v3, v2}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    :goto_1
    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    if-eqz v1, :cond_2

    const/16 v1, 0x9

    check-cast p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v2, p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v2, v2, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-direct {p0, v3, v1, v2}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private onSyncedPurchaseCursor(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v3, 0x2

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    const/16 v3, 0xc

    const v4, 0x7f0a0085

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->readCursor(Landroid/database/Cursor;)V

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eq v1, v3, :cond_1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x1

    const/4 v3, 0x5

    const v4, 0x7f0a0084

    :try_start_2
    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v2

    :pswitch_2
    const/4 v2, 0x0

    const/4 v3, 0x6

    const v4, 0x7f0a008f

    :try_start_3
    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    goto :goto_1

    :pswitch_3
    const/4 v2, 0x0

    const/4 v3, 0x7

    const v4, 0x7f0a0083

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_2
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_2

    const/4 v2, 0x2

    invoke-static {p1, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntegerList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->purchasedFormats:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/videos/async/StreamsRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->purchasedFormats:Ljava/util/List;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/videos/async/StreamsRequest;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private onVideoStreams(Lcom/google/android/youtube/core/model/VideoStreams;Z)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/core/model/VideoStreams;
    .param p2    # Z

    const/4 v5, 0x0

    const/4 v4, 0x1

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/player/Director;->appLevelDrm:Z

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->captionsTrackUri:Landroid/net/Uri;

    iget v3, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleMode:I

    if-eq v3, v4, :cond_0

    move v3, v4

    :goto_0
    iget v6, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_1

    :goto_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/Director;->defaultSubtitleLanguage:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->init(Ljava/lang/String;Landroid/net/Uri;ZZLjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->acquireLicenses()V

    return-void

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1
.end method

.method private playVideo()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onPlay()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/Director$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/player/Director$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director;->purchasedFormats:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget v4, p0, Lcom/google/android/youtube/videos/player/Director;->durationMillis:I

    iget v5, p0, Lcom/google/android/youtube/videos/player/Director;->resumeTimeMillis:I

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/player/Director;->appLevelDrm:Z

    iget-boolean v7, p0, Lcom/google/android/youtube/videos/player/Director;->pendingShortClockActivation:Z

    iget-wide v8, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    iget-boolean v10, p0, Lcom/google/android/youtube/videos/player/Director;->claimed:Z

    invoke-interface/range {v0 .. v10}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->load(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;Lcom/google/android/youtube/core/model/VideoStreams;IIZZJZ)V

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->play()V

    goto :goto_0
.end method

.method private readCursor(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x1

    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director;->claimed:Z

    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->readUri(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->captionsTrackUri:Landroid/net/Uri;

    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleMode:I

    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->defaultSubtitleLanguage:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    iput v1, p0, Lcom/google/android/youtube/videos/player/Director;->durationMillis:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v1, -0x1

    :goto_0
    iput-wide v1, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onVideoTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->onVideoTitle(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    goto :goto_0
.end method

.method private restartFlowFromAcquireLicenses()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    if-nez v0, :cond_0

    const-string v0, "videoStreams should not be null"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->initVideo()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->acquireLicenses()V

    goto :goto_0
.end method

.method private selectVideoStreams(I)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "drmLevel must not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "enable_surround_sound"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/Director;->permittedStreams:Ljava/util/Map;

    invoke-virtual {v3, v4, p1, v0}, Lcom/google/android/youtube/videos/StreamsSelector;->getOnlineStreams(Ljava/util/Map;IZ)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v0

    if-ne p1, v1, :cond_1

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/player/Director;->onVideoStreams(Lcom/google/android/youtube/core/model/VideoStreams;Z)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const v1, 0x7f0a0114

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V

    goto :goto_2
.end method

.method private showShortClockConfirmationDialog()V
    .locals 9

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/Director;->hideControlsForDialog()V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;

    const v2, 0x7f0a00bb

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    const-wide/32 v7, 0x36ee80

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, Lcom/google/android/youtube/videos/player/Director$12;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/videos/player/Director$12;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, Lcom/google/android/youtube/videos/player/Director$11;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/videos/player/Director$11;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/videos/player/Director$10;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/Director$10;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method

.method private warnAndPlayVideo(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    :cond_0
    iget-wide v0, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->shortClockActivated:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->pendingShortClockActivation:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->pendingShortClockActivation:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->userConfirmedShortClockActivation:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->showShortClockConfirmationDialog()V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->playVideo()V

    goto :goto_1
.end method


# virtual methods
.method public hideControlsForDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onUserInteractionExpected()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->controlsHiddenForDialog:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->controlsHiddenForDialog:Z

    return-void
.end method

.method public initAtHomePlayback(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->atHomePlayer:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->init(Z)V

    return-void
.end method

.method public initLocalPlayback(Z)V
    .locals 2
    .param p1    # Z

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setPresentationDisplayRouteV17(Landroid/media/MediaRouter$RouteInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->init(Z)V

    return-void
.end method

.method public initLocalPresentationPlaybackV17(ZLandroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Z
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setPresentationDisplayRouteV17(Landroid/media/MediaRouter$RouteInfo;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->init(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onCC()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCcClicked()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->requestSubtitleTracks()V

    return-void
.end method

.method public onDialogShown()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/Director;->hideControlsForDialog()V

    return-void
.end method

.method public onHQ()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->onHQ()V

    return-void
.end method

.method public onHidden()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;->setFocusable(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director;->controlsHiddenForDialog:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->isCardListExpanded()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setContentVisible(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onUserInteractionNotExpected()V

    :cond_2
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLicensesError(Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/Exception;

    instance-of v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->onInitializationError(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmException;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director;->computeAndLogDrmErrorMessage(Lcom/google/android/youtube/videos/drm/DrmException;)Landroid/util/Pair;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/player/Director$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v3, v1, v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onNext()V
    .locals 0

    return-void
.end method

.method public onOnlineStreamSelected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->getSelectedStream()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->maybeGetKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/Director$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/player/Director$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onPause()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->pause()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPause called in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPlay()V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$13;->$SwitchMap$com$google$android$youtube$videos$player$Director$State:[I

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/Director$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPlay called in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->playVideo()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->warnAndPlayVideo(Z)V

    goto :goto_0

    :pswitch_2
    iput-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->setKeepScreenOn(Z)V

    goto :goto_0

    :pswitch_3
    iput-boolean v2, p0, Lcom/google/android/youtube/videos/player/Director;->playWhenInitialized:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->initVideo()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPrevious()V
    .locals 0

    return-void
.end method

.method public onReplay()V
    .locals 0

    return-void
.end method

.method public onRetry()V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$13;->$SwitchMap$com$google$android$youtube$videos$player$Director$State:[I

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/Director$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/Director;->restartFlowFromAcquireLicenses()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/Director;->reset()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/Director;->init(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onScrubbingStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->onScrubbingStart()V

    return-void
.end method

.method public onSeekTo(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->onSeekTo(I)V

    return-void
.end method

.method public onShown()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setContentVisible(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->listener:Lcom/google/android/youtube/videos/player/Director$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/Director$Listener;->onUserInteractionExpected()V

    return-void
.end method

.method public onToggleFullscreen(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onSubtitleTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->setLanguagePreference(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->selectTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public release(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->release()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->release(Z)V

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableKnowledgeCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->cancel()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/Director;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->subtitleTracksHelper:Lcom/google/android/youtube/core/player/SubtitleTracksHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/SubtitleTracksHelper;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->selectedPlayer:Lcom/google/android/youtube/videos/player/PlaybackHelper;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->reset()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/Director$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->state:Lcom/google/android/youtube/videos/player/Director$State;

    return-void
.end method

.method public showControls()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    return-void
.end method
