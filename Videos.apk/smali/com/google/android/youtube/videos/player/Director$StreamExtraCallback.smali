.class Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StreamExtraCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "Lcom/google/android/youtube/core/model/StreamExtra;",
        ">;"
    }
.end annotation


# instance fields
.field private final originalStream:Lcom/google/android/youtube/core/model/Stream;

.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/model/Stream;)V
    .locals 0
    .param p2    # Lcom/google/android/youtube/core/model/Stream;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->originalStream:Lcom/google/android/youtube/core/model/Stream;

    return-void
.end method


# virtual methods
.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "Error getting stream extra, will try requesting knowledge without it"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->getSelectedStream()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->originalStream:Lcom/google/android/youtube/core/model/Stream;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->originalStream:Lcom/google/android/youtube/core/model/Stream;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->getKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/player/Director;->access$3600(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/model/Stream;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;Lcom/google/android/youtube/core/model/StreamExtra;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/core/model/StreamExtra;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->originalStream:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/model/Stream$Builder;->extra(Lcom/google/android/youtube/core/model/StreamExtra;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->localPlayer:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$3500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->getSelectedStream()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->originalStream:Lcom/google/android/youtube/core/model/Stream;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->getKnowledgeBundle(Lcom/google/android/youtube/core/model/Stream;)V
    invoke-static {v1, v0}, Lcom/google/android/youtube/videos/player/Director;->access$3600(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/model/Stream;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Lcom/google/android/youtube/core/model/StreamExtra;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$StreamExtraCallback;->onResponse(Landroid/net/Uri;Lcom/google/android/youtube/core/model/StreamExtra;)V

    return-void
.end method
