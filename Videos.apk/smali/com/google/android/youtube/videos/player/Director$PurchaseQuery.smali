.class interface abstract Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;
.super Ljava/lang/Object;
.source "Director.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PurchaseQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "purchase_duration_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "formats"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "claimed"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "default_subtitle_language"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "subtitle_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "captions_track_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
