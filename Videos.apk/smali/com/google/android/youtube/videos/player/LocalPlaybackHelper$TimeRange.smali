.class Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeRange"
.end annotation


# instance fields
.field public final lowerBound:J

.field public upperBound:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;->lowerBound:J

    iput-wide p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;->upperBound:J

    return-void
.end method
