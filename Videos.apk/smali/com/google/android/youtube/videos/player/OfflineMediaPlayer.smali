.class public final Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;
.super Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;
.source "OfflineMediaPlayer.java"

# interfaces
.implements Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;


# instance fields
.field private downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

.field private volatile duration:I

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private volatile percentDownloaded:I

.field private final totalFileSize:J

.field private volatile waitingForMoreData:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Lcom/google/android/youtube/core/utils/NetworkStatus;J)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p3    # J

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-wide p3, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->totalFileSize:J

    return-void
.end method

.method public static allowSeekTo(III)Z
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # I

    invoke-static {p1, p2}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->maxSeekPositionMillis(II)I

    move-result v0

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSeekToPercent(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->maxSeekPositionMillis(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    div-int/2addr v1, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private static maxSeekPositionMillis(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    const/16 v1, 0x64

    if-lt p1, v1, :cond_0

    :goto_0
    return p0

    :cond_0
    mul-int v1, p0, p1

    div-int/lit8 v0, v1, 0x64

    const v1, 0x1c138

    sub-int p0, v0, v1

    goto :goto_0
.end method

.method private declared-synchronized stopScheduler()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateCurrentPosition()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getCurrentPosition()I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->duration:I

    iget v3, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    mul-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x64

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->waitingForMoreData:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x1d4c0

    sub-int v2, v0, v2

    if-le v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pausing at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "% to buffer more data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->pause()V

    iput-boolean v5, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->waitingForMoreData:Z

    const/16 v2, 0x2bd

    invoke-virtual {p0, v2, v4}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifyInfo(II)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v2, 0x2bf20

    sub-int v2, v0, v2

    if-ge v1, v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer full, resuming at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->waitingForMoreData:Z

    const/16 v2, 0x2be

    invoke-virtual {p0, v2, v4}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifyInfo(II)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->start()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, -0x3e81

    invoke-virtual {p0, v5, v2}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifyError(II)Z

    goto :goto_0
.end method

.method private updateFileSize(J)V
    .locals 2
    .param p1    # J

    iget v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->duration:I

    iget v1, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getSeekToPercent(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifyBufferingUpdate(I)V

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I

    return-void
.end method

.method public onDownloadProgress(JJI)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # I

    iput p5, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->updateFileSize(J)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->updateCurrentPosition()V

    return-void
.end method

.method public onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->duration:I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->start()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public pause()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->pause()V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->waitingForMoreData:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->waitingForMoreData:Z

    const/16 v0, 0x2be

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifyInfo(II)Z

    :cond_0
    return-void
.end method

.method public prepareAsync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->getCurrentFileSize()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->updateFileSize(J)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->prepareAsync()V

    return-void
.end method

.method public release()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->stopScheduler()V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->release()V

    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getDuration()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->percentDownloaded:I

    invoke-static {p1, v0, v1}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->allowSeekTo(III)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->seekTo(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->notifySeekComplete()V

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    iget-wide v1, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->totalFileSize:J

    invoke-direct {v0, p2, v1, v2, p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;-><init>(Landroid/net/Uri;JLcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method
