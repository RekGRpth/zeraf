.class Lcom/google/android/youtube/videos/player/Director$3;
.super Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;
.source "Director.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;->onGetVideoResourceCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;-><init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$1;)V

    return-void
.end method


# virtual methods
.method public onUserAuthSet()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$900(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/Director;->access$600(Lcom/google/android/youtube/videos/player/Director;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director$3;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v3}, Lcom/google/android/youtube/videos/player/Director;->access$700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v3

    # invokes: Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/player/Director;->access$800(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
