.class interface abstract Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "StoredPurchaseQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;

.field public static final DOWNLOAD_BYTES_DOWNLOADED:I

.field public static final DOWNLOAD_LAST_MODIFIED:I

.field public static final DOWNLOAD_RELATIVE_FILEPATH:I

.field public static final DOWNLOAD_SIZE:I

.field public static final LAST_WATCHED_TIMESTAMP:I

.field public static final LICENSE_ASSET_ID:I

.field public static final LICENSE_KEY_ID:I

.field public static final LICENSE_SYSTEM_ID:I

.field public static final LICENSE_VIDEO_FORMAT:I

.field public static final PINNED:I

.field public static final RESUME_TIMESTAMP:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pinning_download_size"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "download_relative_filepath"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "license_key_id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "license_asset_id"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "license_system_id"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "resume_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "last_watched_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "pinned"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "download_bytes_downloaded"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "download_last_modified"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "license_video_format"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->COLUMNS:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_SIZE:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_RELATIVE_FILEPATH:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_KEY_ID:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_ASSET_ID:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_SYSTEM_ID:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->RESUME_TIMESTAMP:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x6

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LAST_WATCHED_TIMESTAMP:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x7

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->PINNED:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_BYTES_DOWNLOADED:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x9

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->DOWNLOAD_LAST_MODIFIED:I

    sget-object v0, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xa

    sput v0, Lcom/google/android/youtube/videos/player/Director$StoredPurchaseQuery;->LICENSE_VIDEO_FORMAT:I

    return-void
.end method
