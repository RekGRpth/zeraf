.class Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalBroadcastReceiver"
.end annotation


# instance fields
.field private registered:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p2    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->registered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pause()V

    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->registered:Z

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->registered:Z

    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->registered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->registered:Z

    :cond_0
    return-void
.end method
