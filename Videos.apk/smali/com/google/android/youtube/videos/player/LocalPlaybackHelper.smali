.class public Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/PlayerUi$Listener;
.implements Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;
.implements Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;
.implements Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;
.implements Lcom/google/android/youtube/videos/player/PlaybackHelper;
.implements Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;
.implements Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private alreadyDroppedQuality:Z

.field private volatile appLevelDrm:Z

.field private final audioManager:Landroid/media/AudioManager;

.field private buffering:Z

.field private final bufferingEvents:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;",
            ">;"
        }
    .end annotation
.end field

.field private claimed:Z

.field private currentKnowledgeTimeMillis:I

.field private currentVideoDisplayHeight:I

.field private currentVideoDisplayWidth:I

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

.field private downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private durationMillis:I

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private hq:Z

.field private hqToggleRequested:Z

.field private final internalBroadcastReceiver:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

.field private final knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

.field private final listener:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;

.field private loadedStream:Lcom/google/android/youtube/core/model/Stream;

.field private final localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

.field private final localPlayerView:Lcom/google/android/youtube/core/player/PlayerView;

.field private final localSubtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private pendingLocalShortClockActivation:Z

.field private playStartedTimestamp:J

.field private playWhenScrubbingEnds:Z

.field private playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

.field private playedFromMillis:I

.field private player:Lcom/google/android/youtube/core/player/YouTubePlayer;

.field private final playerView:Lcom/google/android/youtube/core/player/PlayerUi;

.field private final preferences:Landroid/content/SharedPreferences;

.field private remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

.field private final remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

.field private resumeTimeMillis:I

.field private scrubbing:Z

.field private selectedStream:Lcom/google/android/youtube/core/model/Stream;

.field private shortClockMillis:J

.field private state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field private statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

.field private final streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

.field private final streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

.field private trackingSessionStarted:Z

.field private final uiHandler:Landroid/os/Handler;

.field private final videoId:Ljava/lang/String;

.field private final videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

.field private videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

.field private final videosConfig:Lcom/google/android/youtube/videos/Config;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;Lcom/google/android/youtube/videos/drm/DrmManager;Landroid/os/Handler;Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/youtube/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 7
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p4    # Lcom/google/android/youtube/videos/Config;
    .param p5    # Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;
    .param p6    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p7    # Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    .param p8    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p9    # Landroid/os/Handler;
    .param p10    # Lcom/google/android/youtube/core/player/PlayerView;
    .param p11    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .param p12    # Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
    .param p13    # Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;
    .param p14    # Lcom/google/android/youtube/videos/store/Database;
    .param p15    # Ljava/util/concurrent/Executor;
    .param p16    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;
    .param p17    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .param p18    # Ljava/lang/String;
    .param p19    # Ljava/lang/String;
    .param p20    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "activity cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const-string v1, "statsClientFactory cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    const-string v1, "playerView cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/PlayerUi;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/youtube/core/player/PlayerUi;

    const-string v1, "subtitlesClient cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/SubtitlesClient;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    const-string v1, "uiHandler cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    const-string v1, "streamingStatusNotifier cannot be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    const-string v1, "controllerOverlay cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const-string v1, "videosConfig cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/Config;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videosConfig:Lcom/google/android/youtube/videos/Config;

    const-string v1, "networkStatus cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    const-string v1, "preferences cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "drmManager cannot be null"

    invoke-static {p8, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    const-string v1, "database cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v1, "localExecutor cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    const-string v1, "account cannot be null"

    move-object/from16 v0, p19

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    const-string v1, "videoId cannot be null"

    move-object/from16 v0, p18

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    const-string v1, "listener cannot be null"

    move-object/from16 v0, p16

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;

    const-string v1, "eventLogger cannot be null"

    move-object/from16 v0, p20

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlayerView:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localSubtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    new-instance v1, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    invoke-interface {p4}, Lcom/google/android/youtube/videos/Config;->mobileStreamingEnabled()Z

    move-result v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;Lcom/google/android/youtube/core/utils/NetworkStatus;Z)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    new-instance v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$1;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->audioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-eqz p17, :cond_0

    move-object/from16 v0, p17

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setListener(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;)V

    invoke-interface {p4}, Lcom/google/android/youtube/videos/Config;->knowledgeShowAllCards()Z

    move-result v1

    move-object/from16 v0, p17

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setShowAllKnowledgeCards(Z)V

    :cond_0
    move-object/from16 v0, p10

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/player/PlayerView;->setListener(Lcom/google/android/youtube/core/player/PlayerUi$Listener;)V

    const-string v1, "default_hq"

    const/4 v2, 0x0

    invoke-interface {p2, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {p3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isFastNetwork()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/GservicesUtil;->probablyLowEndDevice(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isChargeableNetwork()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->persistLocalShortClockActivation()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->updateKnowledge(I)V

    return-void
.end method

.method static synthetic access$1702(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->buffering:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/utils/NetworkStatus;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/VideoStreams;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->alreadyDroppedQuality:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->alreadyDroppedQuality:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)J
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-wide v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playStartedTimestamp:J

    return-wide v0
.end method

.method static synthetic access$2800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->changeQuality(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    return p1
.end method

.method private changeQuality(Z)V
    .locals 3
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHQ(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v2, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->play()V

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->buffering:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z

    goto :goto_0
.end method

.method private getShortClockFromNowTimestamp()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->shortClockMillis:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private persistLastPlayback()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->hasStartTimestampMsec()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->hasStopTimestampMsec()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_1
    const-string v3, "last_playback_is_dirty"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "last_playback_start_timestamp"

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->getStartTimestampMsec()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "last_watched_timestamp"

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->getStopTimestampMsec()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "resume_timestamp"

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->getPositionMsec()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$1;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$1;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private persistLocalShortClockActivation()V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->getShortClockFromNowTimestamp()J

    move-result-wide v0

    new-instance v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;JLcom/google/android/youtube/videos/pinning/DownloadKey;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private playVideoInternal()V
    .locals 10

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    invoke-virtual {v0, v9}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->setStreaming(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setLoading()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playStartedTimestamp:J

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->playVideo()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/youtube/core/player/PlayerUi;

    invoke-interface {v0, v4}, Lcom/google/android/youtube/core/player/PlayerUi;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->trackingSessionStarted:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    div-int/lit16 v3, v3, 0x3e8

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->claimed:Z

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;->createForVideo(Ljava/lang/String;Ljava/lang/String;IZZZ)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    iput-boolean v9, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->trackingSessionStarted:Z

    :cond_3
    iget-object v8, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, v8, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v8}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v8

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videosConfig:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->surroundSoundFormats()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget v2, v2, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->setVirtualSurroundSoundEnabledV9(Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    invoke-virtual {v0, v8, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->loadVideo(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_0
.end method

.method private updateKnowledge(I)V
    .locals 7
    .param p1    # I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;->knowledgeEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->appLevelDrm:Z

    if-eqz v5, :cond_3

    move v0, p1

    :goto_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v5

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->scrubbing:Z

    if-eqz v5, :cond_4

    :cond_2
    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentKnowledgeTimeMillis:I

    if-eq v0, v5, :cond_0

    iput v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentKnowledgeTimeMillis:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->prepareForPosition(I)V

    goto :goto_0

    :cond_3
    add-int/lit16 v5, p1, -0xc8

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlayerView:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/PlayerView;->getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/core/player/PlayerSurface;->getVideoDisplayWidth()I

    move-result v2

    invoke-interface {v3}, Lcom/google/android/youtube/core/player/PlayerSurface;->getVideoDisplayHeight()I

    move-result v1

    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentKnowledgeTimeMillis:I

    if-ne v0, v5, :cond_5

    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayWidth:I

    if-ne v2, v5, :cond_5

    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayHeight:I

    if-eq v1, v5, :cond_0

    :cond_5
    iput v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentKnowledgeTimeMillis:I

    iput v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayWidth:I

    iput v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayHeight:I

    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_6

    :goto_2
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v5, v0, v4, v2, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->show(IIII)V

    goto :goto_0

    :cond_6
    iget v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    sub-int v5, p1, v5

    iget-object v6, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videosConfig:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->knowledgeShowRecentActorsWithinMillis()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_2
.end method


# virtual methods
.method public getSelectedStream()Lcom/google/android/youtube/core/model/Stream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method public init()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->alreadyDroppedQuality:Z

    iput-object v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iput-object v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videosConfig:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->forceMirrorMode()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->initLocalPlaybackViewHolder()Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    :goto_0
    new-instance v2, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;->getSubtitlesOverlay()Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-direct {v2, v3, v4, p0, v5}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    new-instance v2, Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;->getPlayerView()Lcom/google/android/youtube/core/player/PlayerView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/PlayerView;->getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v4

    invoke-direct {v2, v3, v4, p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/PlayerSurface;Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    new-instance v3, Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;

    iget-object v6, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videosConfig:Lcom/google/android/youtube/videos/Config;

    invoke-direct {v5, p0, v6}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/Config;)V

    invoke-direct {v3, v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->addListener(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;->getRemoteControllerOverlay()Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->setFontSizeFromPreferences(Landroid/content/SharedPreferences;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAutoHide(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHideOnTap(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setSupportsQualityToggle(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->register()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->register()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->audioManager:Landroid/media/AudioManager;

    const/high16 v3, -0x80000000

    invoke-virtual {v2, v7, v3, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    sget-object v2, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->INITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->supportsProtectedBuffers()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    new-instance v2, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlayerView:Lcom/google/android/youtube/core/player/PlayerView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localSubtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;-><init>(Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    goto/16 :goto_0
.end method

.method public isSecure()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->isSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;Lcom/google/android/youtube/core/model/VideoStreams;IIZZJZ)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Lcom/google/android/youtube/core/model/VideoStreams;
    .param p4    # I
    .param p5    # I
    .param p6    # Z
    .param p7    # Z
    .param p8    # J
    .param p10    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/youtube/core/model/VideoStreams;",
            "IIZZJZ)V"
        }
    .end annotation

    iput-object p3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iput p4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    iput p5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    iput-wide p8, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->shortClockMillis:J

    iput-boolean p6, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->appLevelDrm:Z

    iput-boolean p10, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->claimed:Z

    if-eqz p7, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v3, v3, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;-><init>(Landroid/net/Uri;JLcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->getPercentDownloaded()I

    move-result v0

    if-lez p5, :cond_0

    invoke-static {p5, p4, v0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->allowSeekTo(III)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p5, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->start()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setSupportsQualityToggle(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHQ(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->play()V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public newMediaPlayer(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Z

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    move-object v0, v1

    :cond_0
    new-instance v1, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->appLevelDrm:Z

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/youtube/videos/drm/DrmManager;Z)V

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v2, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v3, v3, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Lcom/google/android/youtube/core/utils/NetworkStatus;J)V

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardDismissed(Lcom/google/android/youtube/videos/tagging/CardTag;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/CardTag;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget v1, p1, Lcom/google/android/youtube/videos/tagging/CardTag;->cardType:I

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCardDismissed(I)V

    return-void
.end method

.method public onCardListCollapseProgress(F)V
    .locals 3

    const/4 v0, 0x0

    const/high16 v1, 0x3fc00000

    mul-float/2addr v1, p1

    const/high16 v2, 0x3f000000

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    return-void
.end method

.method public onCardListCollapsed(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCardsCollapsed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCardListExpanded(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCardsExpanded(Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardsShown(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCardsShown(Z)V

    return-void
.end method

.method public onClickOutsideTags()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    return-void
.end method

.method public onDownloadProgress(JJI)V
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;

    invoke-direct {v1, p0, p5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onExpandRecentActors()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onExpandRecentActors()V

    return-void
.end method

.method public onHQ()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onQualityChanged(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    if-nez v0, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->changeQuality(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public onReset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAlpha(F)V

    return-void
.end method

.method public onScrubbingStart()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playWhenScrubbingEnds:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->scrubbing:Z

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->pauseVideo()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v2, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showPaused()V

    goto :goto_0
.end method

.method public onSeekTo(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->scrubbing:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v3, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playWhenScrubbingEnds:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->seekTo(IZ)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->play()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-virtual {v2, p1, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->seekTo(IZ)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->getPercentDownloaded()I

    move-result v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->getPercentDownloaded()I

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->allowSeekTo(III)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    iput p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I

    :cond_5
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    iget v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I

    invoke-static {v3, v0}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getSeekToPercent(II)I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setTimes(III)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public onStreamingAccepted(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    return-void
.end method

.method public onStreamingDeclined()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showPaused()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/youtube/core/player/PlayerUi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/PlayerUi;->setKeepScreenOn(Z)V

    return-void
.end method

.method public onSubtitleDisabled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setCcEnabled(Z)V

    return-void
.end method

.method public onSubtitleEnabled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setCcEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->syncSubtitles(I)V

    :cond_0
    return-void
.end method

.method public onUnhandledTouchEvent()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVideoInfo(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onWifiConnected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    return-void
.end method

.method public onWifiDisconnected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;->onDialogShown()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pause()V

    return-void
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->pauseVideo()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->setStreaming(Z)V

    return-void
.end method

.method public play()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    if-eq v1, v0, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$Listener;->onOnlineStreamSelected()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->onPlaybackStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showEnded()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->hideControls()V

    goto :goto_1
.end method

.method public release(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackDestroyed()V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->stop()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->downloadProgressNotifier:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->setStreaming(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->reset()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->persistLastPlayback()V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideo()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->release(Z)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;->release()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/youtube/core/player/PlayerUi;

    invoke-interface {v0, v3}, Lcom/google/android/youtube/core/player/PlayerUi;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->reset()V

    iput v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentKnowledgeTimeMillis:I

    iput v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayWidth:I

    iput v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->currentVideoDisplayHeight:I

    :cond_5
    sget-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ERROR:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v0, v1, :cond_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->persistLastPlayback()V

    goto :goto_0
.end method

.method public selectTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->setSelectedTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public setKeepScreenOn(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/youtube/core/player/PlayerUi;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/PlayerUi;->setKeepScreenOn(Z)V

    return-void
.end method

.method public setLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setLoading()V

    :cond_0
    return-void
.end method

.method protected setPlaying()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setPlaying()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setPlaying()V

    :cond_0
    return-void
.end method

.method public setPresentationDisplayRouteV17(Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter$RouteInfo;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;-><init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Landroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0
.end method

.method public showErrorMessage(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public showPaused()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showPaused()V

    :cond_0
    return-void
.end method
