.class public Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;
.super Ljava/lang/Object;
.source "LocalPlaybackViewHolder.java"

# interfaces
.implements Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrimaryScreenViewHolder"
.end annotation


# instance fields
.field private final playerView:Lcom/google/android/youtube/core/player/PlayerView;

.field private final subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/PlayerView;
    .param p2    # Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    iput-object p2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    return-void
.end method


# virtual methods
.method public getPlayerView()Lcom/google/android/youtube/core/player/PlayerView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    return-object v0
.end method

.method public getRemoteControllerOverlay()Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubtitlesOverlay()Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    return-object v0
.end method

.method public knowledgeEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public release()V
    .locals 0

    return-void
.end method
