.class Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->onDownloadProgress(JJI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

.field final synthetic val$percentDownloaded:I


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iput p2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->val$percentDownloaded:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->durationMillis:I
    invoke-static {v3}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$3;->val$percentDownloaded:I

    invoke-static {v3, v4}, Lcom/google/android/youtube/videos/player/OfflineMediaPlayer;->getSeekToPercent(II)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setTimes(III)V

    :cond_0
    return-void
.end method
