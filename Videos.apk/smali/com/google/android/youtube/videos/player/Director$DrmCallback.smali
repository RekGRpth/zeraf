.class Lcom/google/android/youtube/videos/player/Director$DrmCallback;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/drm/DrmRequest;",
        "Lcom/google/android/youtube/videos/drm/DrmResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private expectedResponses:I

.field private onErrorAlreadyInvoked:Z

.field private purchaseShortClockActivated:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/player/Director;I)V
    .locals 2
    .param p2    # I

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(Z)V

    iput p2, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->expectedResponses:I

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->purchaseShortClockActivated:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Ljava/lang/Exception;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->onErrorAlreadyInvoked:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->onErrorAlreadyInvoked:Z

    instance-of v1, p2, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iget v2, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onDrmFallback(I)V
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/player/Director;->access$3200(Lcom/google/android/youtube/videos/player/Director;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$500(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    new-instance v2, Lcom/google/android/youtube/videos/player/Director$DrmCallback$1;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/videos/player/Director$DrmCallback$1;-><init>(Lcom/google/android/youtube/videos/player/Director$DrmCallback;)V

    # invokes: Lcom/google/android/youtube/videos/player/Director;->getUserAuth(Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/player/Director;->access$3400(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/videos/player/Director;->onLicensesError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->onError(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->purchaseShortClockActivated:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3000(Lcom/google/android/youtube/videos/player/Director;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    iget v0, p2, Lcom/google/android/youtube/videos/drm/DrmResponse;->remainingSeconds:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->shortClockMillis:J
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$3000(Lcom/google/android/youtube/videos/player/Director;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->purchaseShortClockActivated:Z

    iget v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->expectedResponses:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->expectedResponses:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->purchaseShortClockActivated:Z

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onLicensesResponse(Z)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/player/Director;->access$3100(Lcom/google/android/youtube/videos/player/Director;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    check-cast p2, Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$DrmCallback;->onResponse(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)V

    return-void
.end method
