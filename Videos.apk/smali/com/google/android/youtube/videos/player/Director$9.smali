.class Lcom/google/android/youtube/videos/player/Director$9;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error getting knowledge for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$9;->onError(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;)V
    .locals 9
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No knowledge data for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got knowledge data for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$1900(Lcom/google/android/youtube/videos/player/Director;)Landroid/app/Activity;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2000(Lcom/google/android/youtube/videos/player/Director;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2100(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeClient;->getFeedbackClient()Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->config:Lcom/google/android/youtube/videos/Config;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2300(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->feedbackProductId()I

    move-result v6

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->config:Lcom/google/android/youtube/videos/Config;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2300(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->knowledgeFeedbackTypeId()I

    move-result v7

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$9;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2400(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v8

    move-object v1, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->init(Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Lcom/google/android/youtube/videos/tagging/FeedbackClient;IILcom/google/android/youtube/videos/logging/EventLogger;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$9;->onResponse(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;)V

    return-void
.end method
