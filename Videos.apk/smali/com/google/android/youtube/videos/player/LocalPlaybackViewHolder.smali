.class interface abstract Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;
.super Ljava/lang/Object;
.source "LocalPlaybackViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;,
        Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;
    }
.end annotation


# virtual methods
.method public abstract getPlayerView()Lcom/google/android/youtube/core/player/PlayerView;
.end method

.method public abstract getRemoteControllerOverlay()Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
.end method

.method public abstract getSubtitlesOverlay()Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
.end method

.method public abstract knowledgeEnabled()Z
.end method

.method public abstract release()V
.end method
