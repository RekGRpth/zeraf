.class abstract Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "DirectorAuthenticatee"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/player/Director$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/player/Director;
    .param p2    # Lcom/google/android/youtube/videos/player/Director$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;-><init>(Lcom/google/android/youtube/videos/player/Director;)V

    return-void
.end method


# virtual methods
.method public final onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # setter for: Lcom/google/android/youtube/videos/player/Director;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/player/Director;->access$502(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->onUserAuthSet()V

    return-void
.end method

.method public final onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/Director;->access$2700(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const v4, 0x7f0a00ac

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onInitializationError(ZII)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/player/Director;->access$2800(Lcom/google/android/youtube/videos/player/Director;ZII)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onInitializationError(Ljava/lang/Throwable;)V
    invoke-static {v1, p2}, Lcom/google/android/youtube/videos/player/Director;->access$2900(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onNotAuthenticated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$DirectorAuthenticatee;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$1900(Lcom/google/android/youtube/videos/player/Director;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected abstract onUserAuthSet()V
.end method
