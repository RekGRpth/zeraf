.class public Lcom/google/android/youtube/videos/PromoManager;
.super Ljava/lang/Object;
.source "PromoManager.java"


# instance fields
.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1    # Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private getLaunchCount()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "current_promo_launch_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getMillisSinceStart()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "current_promo_start_time_mills"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long v2, v0, v2

    return-wide v2
.end method

.method private resetIfPromoChanged()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "current_promo"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "knowledge"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "current_promo"

    const-string v3, "knowledge"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "current_promo_launch_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v2, "current_promo_start_time_mills"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method private wasDismissed()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "knowledge_promo_dismissed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "knowledge_promo_dismissed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public incrementLaunchCount()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/PromoManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "current_promo_launch_count"

    invoke-direct {p0}, Lcom/google/android/youtube/videos/PromoManager;->getLaunchCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public shouldShow()Z
    .locals 4

    invoke-direct {p0}, Lcom/google/android/youtube/videos/PromoManager;->resetIfPromoChanged()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/PromoManager;->wasDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/PromoManager;->getLaunchCount()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/PromoManager;->getMillisSinceStart()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
