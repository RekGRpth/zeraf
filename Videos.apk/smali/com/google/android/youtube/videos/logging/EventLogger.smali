.class public interface abstract Lcom/google/android/youtube/videos/logging/EventLogger;
.super Ljava/lang/Object;
.source "EventLogger.java"


# virtual methods
.method public abstract onAppDrmInit(II)V
.end method

.method public abstract onAppDrmInitFailed(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;II)V
.end method

.method public abstract onCardDismissed(I)V
.end method

.method public abstract onCardsCollapsed(Ljava/lang/String;)V
.end method

.method public abstract onCardsEnabledChanged(Z)V
.end method

.method public abstract onCardsExpanded(Ljava/lang/String;Z)V
.end method

.method public abstract onCardsShown(Z)V
.end method

.method public abstract onCcClicked()V
.end method

.method public abstract onDatabaseUpgrade(II)V
.end method

.method public abstract onDatabaseUpgradeError(IILjava/lang/String;)V
.end method

.method public abstract onDismissDownloadDialog(Z)V
.end method

.method public abstract onDismissDownloadErrorDialog()V
.end method

.method public abstract onDrmFrameworkFailed()V
.end method

.method public abstract onDrmFrameworkInit(Ljava/lang/String;)V
.end method

.method public abstract onExpandRecentActors()V
.end method

.method public abstract onLicenseRefreshCompleted(Ljava/lang/String;)V
.end method

.method public abstract onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onLicenseReleaseCompleted(Ljava/lang/String;)V
.end method

.method public abstract onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract onMenuItemSelected(Ljava/lang/String;)V
.end method

.method public abstract onMoreDetailsButtonClicked(Z)V
.end method

.method public abstract onMoviesCardItem(Ljava/lang/String;)V
.end method

.method public abstract onOpenedPlayStoreForEpisode(Z)V
.end method

.method public abstract onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V
.end method

.method public abstract onOpenedPlayStoreForSearch(Ljava/lang/String;Z)V
.end method

.method public abstract onOpenedPlayStoreForShow(Ljava/lang/String;Z)V
.end method

.method public abstract onOpenedPlayStoreForSong(Z)V
.end method

.method public abstract onOpenedPlayStoreForVertical(ZLjava/lang/String;Z)V
.end method

.method public abstract onPersonalWatchPageOpened(Ljava/lang/String;Z)V
.end method

.method public abstract onPinAction(Ljava/lang/String;Z)V
.end method

.method public abstract onPinClick()V
.end method

.method public abstract onPinningCompleted(Ljava/lang/String;Ljava/lang/Integer;JLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZLjava/lang/Integer;I)V
.end method

.method public abstract onPlaybackError(Ljava/lang/String;ZZIIII)V
.end method

.method public abstract onPlaybackInitDrmError(Ljava/lang/String;ZZLcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V
.end method

.method public abstract onPlaybackInitDrmFallbackError(Ljava/lang/String;I)V
.end method

.method public abstract onPlaybackInitError(Ljava/lang/String;ZZI)V
.end method

.method public abstract onPlaybackInitError(Ljava/lang/String;ZZLjava/lang/String;)V
.end method

.method public abstract onPlaybackInitUnlockedDeviceError(Ljava/lang/String;I)V
.end method

.method public abstract onPlaybackMediaServerDiedError(Ljava/lang/String;)V
.end method

.method public abstract onPremiumErrorMessageUpated(I)V
.end method

.method public abstract onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onQualityChanged(Z)V
.end method

.method public abstract onQualityDropped()V
.end method

.method public abstract onSettingsPageOpened()V
.end method

.method public abstract onShowDownloadDialog(Z)V
.end method

.method public abstract onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V
.end method

.method public abstract onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onSubtitleTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
.end method

.method public abstract onSuggestionsError(Ljava/lang/String;)V
.end method

.method public abstract onTransfersPing([II)V
.end method

.method public abstract onVerticalOpened(I)V
.end method

.method public abstract onVideoDetailsPageOpened(Ljava/lang/String;)V
.end method

.method public abstract onVideosStart(Landroid/app/Activity;)V
.end method

.method public abstract onWebSearch(Ljava/lang/String;Z)V
.end method

.method public abstract onWishlistAction(Ljava/lang/String;IZLjava/lang/String;)V
.end method

.method public abstract setReferer(Ljava/lang/String;)V
.end method
