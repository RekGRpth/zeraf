.class final Lcom/google/android/youtube/videos/logging/DefaultEventLogger;
.super Ljava/lang/Object;
.source "DefaultEventLogger.java"

# interfaces
.implements Lcom/google/android/youtube/videos/logging/EventLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;,
        Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;
    }
.end annotation


# static fields
.field private static final TRANSFERS_PING_KEYS:[Ljava/lang/String;


# instance fields
.field private final analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

.field private lastPage:Ljava/lang/String;

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "runningPinnedCount"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "runningReleaseCount"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "runningRefreshCount"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "runningUpdateCount"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "runningWishlistCount"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "activePinnedCount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "activeReleaseCount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "activeRefreshCount"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "activeUpdateCount"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "activeWishlistCount"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "canceledPinnedCount"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "canceledReleaseCount"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "canceledRefreshCount"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "canceledUpdateCount"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "canceledWishlistCount"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "failedPinnedCount"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "failedReleaseCount"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "failedRefreshCount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "failedUpdateCount"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "failedWishlistCount"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "backedOffPinnedCount"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "backedOffReleaseCount"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "backedOffRefreshCount"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "backedOffUpdateCount"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "backedOffWishlistCount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->TRANSFERS_PING_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/client/AnalyticsClient;Lcom/google/android/youtube/core/utils/NetworkStatus;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/client/AnalyticsClient;
    .param p2    # Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "analyticsClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/AnalyticsClient;

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-void
.end method

.method private onLicenseError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Throwable;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    instance-of v1, p3, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "videoId"

    aput-object v3, v2, v4

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "networkType"

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "status"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    iget-object v4, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "errorCode"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget v4, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->errorCode:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "drmLevel"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget v4, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, p1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    instance-of v1, p3, Lcom/google/android/youtube/videos/drm/DrmException;

    if-eqz v1, :cond_1

    move-object v0, p3

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "videoId"

    aput-object v3, v2, v4

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "networkType"

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "status"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    iget-object v4, v0, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "errorCode"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget v4, v0, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, p1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "videoId"

    aput-object v3, v2, v4

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "networkType"

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "exceptionClass"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, p1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "videoId"

    aput-object v3, v2, v4

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "networkType"

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-interface {v1, p1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onAppDrmInit(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "appDrmInit"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_LEVEL:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_ERROR:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onAppDrmInitFailed(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;II)V
    .locals 5
    .param p1    # Z
    .param p2    # Lcom/widevine/drmapi/android/WVStatus;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "appDrmInitFailed"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->SYNC:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->STATUS:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_ERROR:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_LEVEL:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_ERROR:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCardDismissed(I)V
    .locals 5
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "cardDismissed"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "cardType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->cardTypeToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCardsCollapsed(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "cardsCollapsed"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "method"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCardsEnabledChanged(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "cardsEnabledChanged"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "enabled"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCardsExpanded(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "cardsExpanded"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "method"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "isAfterSeek"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCardsShown(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "cardsShown"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "hasRecentActorsCard"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onCcClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "ccClicked"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDatabaseUpgrade(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "databaseUpgrade"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->OLD_VERSION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->NEW_VERSION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDatabaseUpgradeError(IILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "databaseUpgradeError"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->OLD_VERSION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->NEW_VERSION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DESCRIPTION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDismissDownloadDialog(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "dismissDownloadDialog"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "remove"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDismissDownloadErrorDialog()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "dismissDownloadErrorDialog"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDrmFrameworkFailed()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "drmFrameworkFailed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDrmFrameworkInit(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "drmFrameworkInit"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->STATUS:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onExpandRecentActors()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "expandRecentActors"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onLicenseRefreshCompleted(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "licenseRefreshCompleted"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    const-string v0, "licenseRefreshFailed"

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->onLicenseError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onLicenseReleaseCompleted(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "licenseReleaseCompleted"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    const-string v0, "licenseReleaseFailed"

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->onLicenseError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onMenuItemSelected(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "menuItemSelected"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "menuItem"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMoreDetailsButtonClicked(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "moreDetails"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "expand"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMoviesCardItem(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "moviesCardItem"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "action"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForEpisode(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playStoreOpenedForShows"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playStoreOpenedForMovies"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForSearch(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playStoreOpenedForSearch"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForShow(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playStoreOpenedForShows"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForSong(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playStoreOpenedForMusic"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onOpenedPlayStoreForVertical(ZLjava/lang/String;Z)V
    .locals 5
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    if-eqz p1, :cond_0

    const-string v0, "playStoreOpenedForMovies"

    :goto_0
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "playStoreOpenedForShows"

    goto :goto_0
.end method

.method public onPersonalWatchPageOpened(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pageView"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "personalWatch"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "lastPage"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "mimeType"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "personalWatch"

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onPinAction(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pinAction"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "pinned"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPinClick()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pinClick"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPinningCompleted(Ljava/lang/String;Ljava/lang/Integer;JLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pinningCompleted"

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "gdataFormat"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "fileSize"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "lastModified"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p5}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "checksum"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p6}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZLjava/lang/Integer;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Throwable;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/Integer;
    .param p7    # I

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x12

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "gdataFormat"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_0

    const-string v1, "pinningErrorFatalCauseClass"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p4, :cond_1

    const-string v1, "fatal"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p5, :cond_2

    const-string v1, "maxRetries"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p6, :cond_3

    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, p6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const-string v1, "pinningErrorFailedReason"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "networkType"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v2, "pinningFailed"

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackError(Ljava/lang/String;ZZIIII)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackError"

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "canRetry"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "userVisible"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "gdataFormat"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "mediaPlayerCode"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "extraErrorCode"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackInitDrmError(Ljava/lang/String;ZZLcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Lcom/google/android/youtube/videos/drm/DrmException$DrmError;
    .param p5    # I

    const/16 v5, 0xa

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackInitError"

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "canRetry"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "userVisible"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "status"

    aput-object v3, v2, v5

    const/16 v3, 0xb

    invoke-static {p4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "errorCode"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackInitDrmFallbackError(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackInitError"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DRM_LEVEL:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackInitError(Ljava/lang/String;ZZI)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackInitError"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "canRetry"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "userVisible"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackInitError(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackInitError"

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DESCRIPTION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "canRetry"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "userVisible"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackInitUnlockedDeviceError(Ljava/lang/String;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x4

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackInitError"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    sget-object v3, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DRM_LEVEL:Ljava/lang/Object;

    aput-object v3, v2, v5

    const/4 v3, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackMediaServerDiedError(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "playbackError"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "errorType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "networkType"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPremiumErrorMessageUpated(I)V
    .locals 6
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v2, "premiumMessageUpated"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "premiumErrorType"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :pswitch_0
    const-string v0, "OK"

    goto :goto_0

    :pswitch_1
    const-string v0, "ClockError"

    goto :goto_0

    :pswitch_2
    const-string v0, "Rooted"

    goto :goto_0

    :pswitch_3
    const-string v0, "SystemUpdateNeeded"

    goto :goto_0

    :pswitch_4
    const-string v0, "ApplicationUpdateNeeded"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pageView"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "premiumWatch"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "lastPage"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "seasonId"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "premiumWatch"

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onQualityChanged(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "qualityChanged"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->HQ:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onQualityDropped()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "qualityDropped"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSettingsPageOpened()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pageView"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "settings"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "lastPage"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "settings"

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onShowDownloadDialog(Z)V
    .locals 5
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "downloadDialog"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "completed"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Long;
    .param p3    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "downloadErrorDialog"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "pinningErrorFailedReason"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "fileSize"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "errorCode"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pageView"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "show"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "lastPage"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "showId"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "seasonId"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "show"

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onSubtitleTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v0, "null"

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v2, "subtitleSelected"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->LANG:Ljava/lang/Object;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onSuggestionsError(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "suggestionError"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DESCRIPTION:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onTransfersPing([II)V
    .locals 9
    .param p1    # [I
    .param p2    # I

    const/16 v8, 0x19

    const/4 v5, 0x1

    if-nez p2, :cond_1

    move v4, v5

    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v8, :cond_2

    aget v6, p1, v3

    if-eqz v6, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x2

    goto :goto_0

    :cond_2
    mul-int/lit8 v6, v4, 0x2

    new-array v0, v6, [Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    const/4 v6, 0x0

    const-string v7, "flags"

    aput-object v7, v0, v6

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    const/4 v1, 0x2

    :cond_3
    add-int/lit8 v2, v1, 0x1

    const-string v5, "networkType"

    aput-object v5, v0, v1

    add-int/lit8 v1, v2, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v5}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    const/4 v3, 0x0

    move v2, v1

    :goto_2
    if-ge v3, v8, :cond_5

    aget v5, p1, v3

    if-eqz v5, :cond_4

    add-int/lit8 v1, v2, 0x1

    sget-object v5, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->TRANSFERS_PING_KEYS:[Ljava/lang/String;

    aget-object v5, v5, v3

    aput-object v5, v0, v2

    add-int/lit8 v2, v1, 0x1

    aget v5, p1, v3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    :cond_4
    move v1, v2

    add-int/lit8 v3, v3, 0x1

    move v2, v1

    goto :goto_2

    :cond_5
    iget-object v5, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v6, "pinningTasksPing"

    invoke-interface {v5, v6, v0}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onVerticalOpened(I)V
    .locals 6
    .param p1    # I

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->verticalToString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v2, "pageView"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "pageName"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    const-string v5, "lastPage"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onVideoDetailsPageOpened(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "pageView"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "pageName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "videoDetails"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "lastPage"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "videoId"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "videoDetails"

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    return-void
.end method

.method public onVideosStart(Landroid/app/Activity;)V
    .locals 6
    .param p1    # Landroid/app/Activity;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v2, "start"

    const/16 v3, 0xc

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "orientation"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Lcom/google/android/play/analytics/EventConstants;->getOrientationValue(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "sdkInt"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "screenWidth"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "screenHeight"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "screenDensity"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "networkType"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    iget-object v5, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v5}, Lcom/google/android/youtube/core/utils/NetworkStatus;->getNetworkType()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onWebSearch(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "webSearch"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "success"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onWishlistAction(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/youtube/core/client/AnalyticsClient;

    const-string v1, "wishlist"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "itemId"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "itemType"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "isAdd"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->booleanToString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "eventSource"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Values;->objectToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/AnalyticsClient;->trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setReferer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "referer:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;->lastPage:Ljava/lang/String;

    goto :goto_0
.end method
