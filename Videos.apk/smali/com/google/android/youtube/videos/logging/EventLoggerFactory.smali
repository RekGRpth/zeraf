.class public Lcom/google/android/youtube/videos/logging/EventLoggerFactory;
.super Ljava/lang/Object;
.source "EventLoggerFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createDefaultEventLogger(Lcom/google/android/youtube/core/client/AnalyticsClient;Lcom/google/android/youtube/core/utils/NetworkStatus;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/client/AnalyticsClient;
    .param p1    # Lcom/google/android/youtube/core/utils/NetworkStatus;

    new-instance v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/logging/DefaultEventLogger;-><init>(Lcom/google/android/youtube/core/client/AnalyticsClient;Lcom/google/android/youtube/core/utils/NetworkStatus;)V

    return-object v0
.end method
