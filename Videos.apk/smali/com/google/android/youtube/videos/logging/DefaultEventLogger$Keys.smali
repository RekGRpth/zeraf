.class interface abstract Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;
.super Ljava/lang/Object;
.source "DefaultEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/logging/DefaultEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "Keys"
.end annotation


# static fields
.field public static final DESCRIPTION:Ljava/lang/Object;

.field public static final DRM_LEVEL:Ljava/lang/Object;

.field public static final HQ:Ljava/lang/Object;

.field public static final LANG:Ljava/lang/Object;

.field public static final NEW_VERSION:Ljava/lang/Object;

.field public static final OLD_VERSION:Ljava/lang/Object;

.field public static final STATUS:Ljava/lang/Object;

.field public static final SYNC:Ljava/lang/Object;

.field public static final WV_ERROR:Ljava/lang/Object;

.field public static final WV_FRAMEWORK_DRM_ERROR:Ljava/lang/Object;

.field public static final WV_FRAMEWORK_DRM_LEVEL:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "description"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DESCRIPTION:Ljava/lang/Object;

    const-string v0, "hq"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->HQ:Ljava/lang/Object;

    const-string v0, "lang"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->LANG:Ljava/lang/Object;

    const-string v0, "wvError"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_ERROR:Ljava/lang/Object;

    const-string v0, "wvFrameworkDrmLevel"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_LEVEL:Ljava/lang/Object;

    const-string v0, "wvFrameworkDrmError"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->WV_FRAMEWORK_DRM_ERROR:Ljava/lang/Object;

    const-string v0, "sync"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->SYNC:Ljava/lang/Object;

    const-string v0, "status"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->STATUS:Ljava/lang/Object;

    const-string v0, "drmLevel"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->DRM_LEVEL:Ljava/lang/Object;

    const-string v0, "oldVersion"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->OLD_VERSION:Ljava/lang/Object;

    const-string v0, "newVersion"

    sput-object v0, Lcom/google/android/youtube/videos/logging/DefaultEventLogger$Keys;->NEW_VERSION:Ljava/lang/Object;

    return-void
.end method
