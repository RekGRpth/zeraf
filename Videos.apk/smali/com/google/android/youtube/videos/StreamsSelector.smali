.class public Lcom/google/android/youtube/videos/StreamsSelector;
.super Ljava/lang/Object;
.source "StreamsSelector.java"


# static fields
.field private static final APP_LEVEL_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final APP_LEVEL_LOW_END_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final isLowEndDevice:Z

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v2, 0x14

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/videos/StreamsSelector;->APP_LEVEL_FORMATS:Ljava/util/Collection;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/videos/StreamsSelector;->APP_LEVEL_LOW_END_FORMATS:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Z)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/Config;
    .param p2    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "config cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/Config;

    iput-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/StreamsSelector;->isLowEndDevice:Z

    return-void
.end method

.method private filterStreams(Ljava/util/Map;IZ)Ljava/util/Map;
    .locals 4
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;IZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    const/4 v2, 0x1

    if-ne p2, v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/StreamsSelector;->isLowEndDevice:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/youtube/videos/StreamsSelector;->APP_LEVEL_LOW_END_FORMATS:Ljava/util/Collection;

    :goto_0
    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    :cond_0
    :goto_1
    if-nez p3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->surroundSoundFormats()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0xd

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->abrFormats()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0

    :cond_3
    sget-object v2, Lcom/google/android/youtube/videos/StreamsSelector;->APP_LEVEL_FORMATS:Ljava/util/Collection;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/youtube/videos/StreamsSelector;->APP_LEVEL_FORMATS:Ljava/util/Collection;

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->fieldProvisionedFormats()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private getHiQualityOnlineStream(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)",
            "Lcom/google/android/youtube/core/model/Stream;"
        }
    .end annotation

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->hdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/model/Stream;->getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->sdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/model/Stream;->getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    goto :goto_0
.end method

.method private getLoQualityOnlineStream(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)",
            "Lcom/google/android/youtube/core/model/Stream;"
        }
    .end annotation

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isFastNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->sdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/model/Stream;->getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->lqFormats()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/model/Stream;->getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getDownloadStream(Ljava/util/Map;IZ)Lcom/google/android/youtube/core/model/Stream;
    .locals 4
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;IZ)",
            "Lcom/google/android/youtube/core/model/Stream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/player/MissingStreamException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/StreamsSelector;->filterStreams(Ljava/util/Map;IZ)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/StreamsSelector;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->downloadFormats()Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/Stream;->getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v2, Lcom/google/android/youtube/core/player/MissingStreamException;

    const-string v3, "No valid download stream"

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-object v0
.end method

.method public getOnlineStreams(Ljava/util/Map;IZ)Lcom/google/android/youtube/core/model/VideoStreams;
    .locals 5
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;IZ)",
            "Lcom/google/android/youtube/core/model/VideoStreams;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/player/MissingStreamException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/StreamsSelector;->filterStreams(Ljava/util/Map;IZ)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/StreamsSelector;->getHiQualityOnlineStream(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/StreamsSelector;->getLoQualityOnlineStream(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    new-instance v3, Lcom/google/android/youtube/core/model/VideoStreams;

    invoke-direct {v3, v1, v2}, Lcom/google/android/youtube/core/model/VideoStreams;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    return-object v3

    :cond_1
    new-instance v3, Lcom/google/android/youtube/core/player/MissingStreamException;

    const-string v4, "No valid streams"

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
