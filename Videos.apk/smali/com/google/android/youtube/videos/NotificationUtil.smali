.class public abstract Lcom/google/android/youtube/videos/NotificationUtil;
.super Ljava/lang/Object;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/NotificationUtil$V8;,
        Lcom/google/android/youtube/videos/NotificationUtil$V11;
    }
.end annotation


# static fields
.field private static final UNITS:[Ljava/lang/String;


# instance fields
.field protected final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "k"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "M"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "G"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "T"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/NotificationUtil;->UNITS:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil;->context:Landroid/content/Context;

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/youtube/videos/NotificationUtil;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/NotificationUtil$V11;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/NotificationUtil$V11;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/NotificationUtil$V8;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/NotificationUtil$V8;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract createAtHomeTrackingNotification(Ljava/lang/String;Ljava/lang/String;IIZLandroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadCompletedNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadErrorNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadsOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method protected getDownloadProgressText(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)Ljava/lang/String;
    .locals 9
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    const/4 v4, 0x0

    iget-boolean v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    if-eqz v5, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/NotificationUtil;->context:Landroid/content/Context;

    const v5, 0x7f0a00c7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    iget-boolean v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    if-eqz v5, :cond_1

    move v1, v4

    :goto_1
    iget-wide v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    invoke-virtual {p0, v5, v6}, Lcom/google/android/youtube/videos/NotificationUtil;->getFormatUnit(J)I

    move-result v3

    iget-wide v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransfered:J

    invoke-virtual {p0, v5, v6, v3}, Lcom/google/android/youtube/videos/NotificationUtil;->getFormattedSize(JI)Ljava/lang/String;

    move-result-object v0

    iget-wide v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    invoke-virtual {p0, v5, v6, v3}, Lcom/google/android/youtube/videos/NotificationUtil;->getFormattedSize(JI)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/youtube/videos/NotificationUtil;->context:Landroid/content/Context;

    const v6, 0x7f0a00c6

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v4, 0x1

    aput-object v0, v7, v4

    const/4 v4, 0x2

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    iget-wide v5, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransfered:J

    const-wide/16 v7, 0x64

    mul-long/2addr v5, v7

    iget-wide v7, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    div-long/2addr v5, v7

    long-to-int v1, v5

    goto :goto_1
.end method

.method getFormatUnit(J)I
    .locals 3
    .param p1    # J

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/youtube/videos/NotificationUtil;->UNITS:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const-wide/16 v1, 0x200

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    const/16 v1, 0xa

    shr-long/2addr p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/youtube/videos/NotificationUtil;->UNITS:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    goto :goto_1
.end method

.method getFormattedSize(JI)Ljava/lang/String;
    .locals 8
    .param p1    # J
    .param p3    # I

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p3, :cond_0

    const-string v3, "%dB"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    long-to-double v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_1

    const-wide/high16 v3, 0x4090000000000000L

    div-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string v3, "%1.1f%sB"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v6

    sget-object v5, Lcom/google/android/youtube/videos/NotificationUtil;->UNITS:[Ljava/lang/String;

    aget-object v5, v5, p3

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
