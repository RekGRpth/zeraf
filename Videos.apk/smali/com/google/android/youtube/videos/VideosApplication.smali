.class public final Lcom/google/android/youtube/videos/VideosApplication;
.super Landroid/app/Application;
.source "VideosApplication.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Provider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;
    }
.end annotation


# static fields
.field private static final OAUTH2_SCOPES:[Ljava/lang/String;


# instance fields
.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

.field private applicationVersion:Ljava/lang/String;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private config:Lcom/google/android/youtube/videos/Config;

.field private contentNotificationManager:Lcom/google/android/youtube/videos/ContentNotificationManager;

.field private database:Lcom/google/android/youtube/videos/store/Database;

.field private downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

.field private drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private executor:Ljava/util/concurrent/Executor;

.field private gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

.field private globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private isInitialized:Z

.field private isTablet:Z

.field private knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

.field private legacyDownloadsHaveAppLevelDrm:Z

.field private localStoreExecutor:Ljava/util/concurrent/Executor;

.field private localVideoThumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

.field private networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

.field private offlineSubtitles:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

.field private posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

.field private preferences:Landroid/content/SharedPreferences;

.field private premiumStatus:I

.field private promoManager:Lcom/google/android/youtube/videos/PromoManager;

.field private purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

.field private robotAccountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

.field private streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

.field private streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

.field private uiExecutor:Ljava/util/concurrent/Executor;

.field private uiHandler:Landroid/os/Handler;

.field private versionCode:I

.field private videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

.field private wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;

.field private wishlistStoreSync:Lcom/google/android/youtube/videos/store/WishlistStoreSync;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/android_video"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/youtube"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/VideosApplication;->OAUTH2_SCOPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/VideosApplication;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->uiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/VideosApplication;)Lcom/google/android/youtube/videos/ContentNotificationManager;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->contentNotificationManager:Lcom/google/android/youtube/videos/ContentNotificationManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/VideosApplication;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    return-object v0
.end method

.method private createSearchMenuV11(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V
    .locals 4

    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    invoke-virtual {p3}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const v0, 0x7f0f0004

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0700a2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/videos/VideosApplication$SearchCollapseListenerV14;-><init>(Landroid/view/MenuItem;Lcom/google/android/youtube/videos/VideosApplication$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    :cond_0
    return-void
.end method

.method private getShouldCheckAccountsSyncStatus()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "check_accounts_sync_status"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private initInternal()V
    .locals 71

    const-string v3, "youtube"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/youtube/videos/VideosApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    new-instance v4, Lcom/google/android/youtube/core/utils/DefaultNetworkStatus;

    const-string v3, "connectivity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/VideosApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    invoke-direct {v4, v3}, Lcom/google/android/youtube/core/utils/DefaultNetworkStatus;-><init>(Landroid/net/ConnectivityManager;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    new-instance v3, Lcom/google/android/youtube/core/ErrorHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/youtube/core/ErrorHelper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/NetworkStatus;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    new-instance v3, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    invoke-direct {v3}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    new-instance v3, Lcom/google/android/youtube/videos/PromoManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-direct {v3, v4}, Lcom/google/android/youtube/videos/PromoManager;-><init>(Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->promoManager:Lcom/google/android/youtube/videos/PromoManager;

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v62

    move-object/from16 v0, v62

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    move-object/from16 v0, v62

    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v68

    invoke-virtual/range {v68 .. v68}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v59

    sget v3, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v4, 0xd

    if-lt v3, v4, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, v59

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/VideosApplication;->initIsTabletV13(Landroid/content/res/Configuration;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->isTablet:Z

    :goto_1
    new-instance v70, Lcom/google/android/youtube/core/utils/UriRewriter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v70

    invoke-direct {v0, v3}, Lcom/google/android/youtube/core/utils/UriRewriter;-><init>(Landroid/content/ContentResolver;)V

    new-instance v3, Lcom/google/android/youtube/videos/GservicesConfig;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v70

    invoke-direct {v3, v0, v4, v1, v5}, Lcom/google/android/youtube/videos/GservicesConfig;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/youtube/core/utils/UriRewriter;Landroid/content/pm/PackageManager;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-static {v3}, Lcom/google/android/youtube/videos/async/GDataRequests;->setConfig(Lcom/google/android/youtube/videos/Config;)V

    new-instance v3, Landroid/os/Handler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->uiHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/videos/VideosApplication$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/VideosApplication$1;-><init>(Lcom/google/android/youtube/videos/VideosApplication;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->uiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v3, 0x4

    const/4 v4, 0x4

    const-wide/16 v5, 0x3c

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;-><init>(I)V

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/VideosApplication;->localStoreExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Ljava/util/concurrent/ThreadPoolExecutor;

    const/16 v3, 0x8

    const/16 v4, 0x8

    const-wide/16 v5, 0x3c

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;

    const/16 v10, 0xa

    invoke-direct {v9, v10}, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;-><init>(I)V

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    const-string v3, " "

    sget-object v4, Lcom/google/android/youtube/videos/VideosApplication;->OAUTH2_SCOPES:[Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->createOAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    move-result-object v56

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->isAtHomeDevice()Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;

    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "android.athome"

    move-object/from16 v0, v56

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->robotAccountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->gmsCoreAvailable()Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;)V

    :goto_3
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    new-instance v3, Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;->create()Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/accounts/SignInManager;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getExperimentId()Ljava/lang/String;

    move-result-object v7

    new-instance v2, Lcom/google/android/youtube/core/client/DummyAnalyticsClient;

    invoke-direct {v2}, Lcom/google/android/youtube/core/client/DummyAnalyticsClient;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->usePlayAnalytics()Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v2, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsCategorySuffix()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsSampleRatio()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/VideosApplication;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/core/client/PlayAnalyticsClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/accounts/SignInManager;)V

    :cond_0
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/logging/EventLoggerFactory;->createDefaultEventLogger(Lcom/google/android/youtube/core/client/AnalyticsClient;Lcom/google/android/youtube/core/utils/NetworkStatus;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->mobileDownloadsEnabled()Z

    move-result v66

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "mobile_downloads_enabled"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "mobile_downloads_enabled"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v58

    move/from16 v0, v66

    move/from16 v1, v58

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "mobile_downloads_enabled"

    move/from16 v0, v66

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "download_policy"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "download_policy_dialog_shown"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    :cond_1
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v63, Landroid/content/ComponentName;

    const-string v3, "com.google.android.youtube.ManageNetworkUsageActivity"

    move-object/from16 v0, v63

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v64

    move-object/from16 v0, v64

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v69

    const/4 v3, 0x1

    move/from16 v0, v69

    if-eq v0, v3, :cond_2

    const-string v3, "Enabling network usage management"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, v64

    move-object/from16 v1, v63

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "legacy_downloads_have_app_level_drm"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "legacy_downloads_have_app_level_drm"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm:Z

    :goto_6
    new-instance v3, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v3}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    invoke-static {v3}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v5}, Lcom/google/android/youtube/videos/Config;->getExperimentId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/google/android/youtube/core/utils/Util;->buildUserAgent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " gzip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createDefaultHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    new-instance v13, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/VideosApplication;->uiExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    sget-object v17, Lcom/google/android/youtube/videos/K;->DEVELOPER_KEY:[B

    sget-object v18, Lcom/google/android/youtube/videos/K;->DEVELOPER_SECRET:[B

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android_id"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v16, v70

    invoke-direct/range {v13 .. v19}, Lcom/google/android/youtube/core/client/DefaultDeviceRegistrationClient;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/UriRewriter;[B[BLjava/lang/String;)V

    new-instance v16, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, v16

    invoke-direct {v0, v13, v3}, Lcom/google/android/youtube/core/async/DefaultDeviceAuthorizer;-><init>(Lcom/google/android/youtube/core/client/DeviceRegistrationClient;Landroid/content/SharedPreferences;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->isAtHomeDevice()Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->robotAccountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v17, v0

    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->forceAppLevelDrm()Z

    move-result v21

    move-object/from16 v14, p0

    invoke-static/range {v14 .. v21}, Lcom/google/android/youtube/videos/drm/DrmManager;->newDrmManager(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/logging/EventLogger;Z)Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    sget v3, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_e

    const/high16 v57, 0x2000000

    :goto_8
    new-instance v3, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move/from16 v0, v57

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->isTablet:Z

    if-nez v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v3

    if-eqz v3, :cond_f

    :cond_3
    const/16 v27, 0x1

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    const-wide/32 v4, 0x500000

    move-object/from16 v0, v22

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/youtube/core/cache/PersistentCache;->triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V

    invoke-static {}, Lcom/google/android/youtube/core/converter/XmlParser;->createPrefixesOnlyParser()Lcom/google/android/youtube/core/converter/XmlParser;

    move-result-object v24

    new-instance v17, Lcom/google/android/youtube/videos/DefaultRequesters;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v26, v0

    move-object/from16 v18, p0

    move-object/from16 v19, v16

    invoke-direct/range {v17 .. v27}, Lcom/google/android/youtube/videos/DefaultRequesters;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/utils/BitmapLruCache;Z)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    new-instance v28, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->gservicesId()J

    move-result-wide v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->baseApiUri()Landroid/net/Uri;

    move-result-object v35

    move-object/from16 v32, v15

    move-object/from16 v33, v22

    invoke-direct/range {v28 .. v35}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLjava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    new-instance v3, Lcom/google/android/youtube/videos/StreamsSelector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/GservicesUtil;->probablyLowEndDevice(Landroid/content/Context;)Z

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/StreamsSelector;-><init>(Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    new-instance v3, Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v3, v0, v4, v5, v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->offlineSubtitles:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    new-instance v3, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    sget-object v4, Lcom/google/android/youtube/videos/VideosApplication;->OAUTH2_SCOPES:[Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->knowledgeEnabled()Z

    move-result v3

    if-eqz v3, :cond_10

    const v3, 0x7f090038

    move-object/from16 v0, v68

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v38

    const v3, 0x7f09003a

    move-object/from16 v0, v68

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v39

    const v3, 0x7f09003d

    move-object/from16 v0, v68

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v37

    new-instance v28, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->getAssetsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v29, p0

    move/from16 v40, v38

    move/from16 v41, v38

    invoke-direct/range {v28 .. v42}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;IIIIILjava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    :goto_a
    new-instance v28, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->localStoreExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v30, v0

    new-instance v31, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "localvideothumbs"

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v32, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    const/4 v3, 0x1

    move-object/from16 v0, v32

    invoke-direct {v0, v3}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v33, v0

    move-object/from16 v29, p0

    invoke-direct/range {v28 .. v33}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/io/File;Lcom/google/android/youtube/core/converter/ResponseConverter;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->localVideoThumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->localVideoThumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->cleanup()V

    new-instance v3, Lcom/google/android/youtube/videos/store/Database;

    const-string v4, "purchase_store.db"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->uiHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/VideosApplication;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/google/android/youtube/videos/store/Database;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    new-instance v45, Lcom/google/android/youtube/videos/store/FileStore;

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "posters"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-direct {v0, v3}, Lcom/google/android/youtube/videos/store/FileStore;-><init>(Ljava/io/File;)V

    new-instance v46, Lcom/google/android/youtube/videos/store/FileStore;

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "show_posters"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v46

    invoke-direct {v0, v3}, Lcom/google/android/youtube/videos/store/FileStore;-><init>(Ljava/io/File;)V

    new-instance v67, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    const v3, 0x7f09001e

    move-object/from16 v0, v68

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, v67

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(IZZ)V

    new-instance v40, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncVideoRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v47

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncSeasonRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v48

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncShowRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v49

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncMyPurchasesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v51

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->getSyncEpisodesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v52

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v54, v0

    move-object/from16 v41, p0

    move/from16 v53, v27

    invoke-direct/range {v40 .. v54}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;ZLcom/google/android/youtube/videos/Config;)V

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->cleanup()V

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->localStoreExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/videos/store/PurchaseStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/Database;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    new-instance v43, Lcom/google/android/youtube/videos/store/PosterStore;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->localStoreExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-object/from16 v48, v0

    move-object/from16 v47, v67

    invoke-direct/range {v43 .. v48}, Lcom/google/android/youtube/videos/store/PosterStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    new-instance v3, Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v8

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/core/async/Requester;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->wishlistStoreSync:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    new-instance v3, Lcom/google/android/youtube/videos/store/WishlistStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->localStoreExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/videos/store/WishlistStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/Database;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/youtube/videos/NotificationUtil;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    new-instance v3, Lcom/google/android/youtube/videos/ContentNotificationManager;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/ContentNotificationManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->contentNotificationManager:Lcom/google/android/youtube/videos/ContentNotificationManager;

    new-instance v28, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    new-instance v30, Landroid/os/Handler;

    invoke-direct/range {v30 .. v30}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v31, v0

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    move-object/from16 v33, v0

    move-object/from16 v29, p0

    invoke-direct/range {v28 .. v33}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/NotificationUtil;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    new-instance v60, Lcom/google/android/youtube/videos/VideosApplication$2;

    move-object/from16 v0, v60

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/VideosApplication$2;-><init>(Lcom/google/android/youtube/videos/VideosApplication;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v0, v60

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    new-instance v4, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/VideosApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v65, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    move-object/from16 v0, v65

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-object/from16 v0, p0

    move-object/from16 v1, v65

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    new-instance v28, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->gcmRegistrationEnabled()Z

    move-result v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->getGcmRegisterSyncRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->getGcmUnregisterSyncRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v33

    move-object/from16 v29, p0

    invoke-direct/range {v28 .. v33}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;-><init>(Landroid/content/Context;ZLcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    new-instance v34, Lcom/google/android/youtube/core/client/StatParams;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getApplicationVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/DisplayUtil;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_11

    sget-object v3, Lcom/google/android/youtube/core/client/StatParams$Platform;->TABLET:Lcom/google/android/youtube/core/client/StatParams$Platform;

    :goto_b
    sget-object v6, Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;->ANDROID:Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;

    move-object/from16 v0, v34

    invoke-direct {v0, v4, v5, v3, v6}, Lcom/google/android/youtube/core/client/StatParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/client/StatParams$Platform;Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;)V

    new-instance v28, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;

    new-instance v29, Ljava/security/SecureRandom;

    invoke-direct/range {v29 .. v29}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v30

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v33, v0

    move-object/from16 v32, v16

    invoke-direct/range {v28 .. v34}, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;-><init>(Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/client/StatParams;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/VideosApplication;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getShouldCheckAccountsSyncStatus()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-static {v3}, Lcom/google/android/youtube/videos/store/SyncService;->enableSyncForUninitializedAccounts(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->setShouldCheckAccountsSyncStatus()V

    :cond_4
    return-void

    :catch_0
    move-exception v61

    const-string v3, "could not retrieve application version name"

    move-object/from16 v0, v61

    invoke-static {v3, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v3, "Unknown"

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->versionCode:I

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, v59

    iget v3, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0xf

    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    const/4 v3, 0x1

    :goto_c
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->isTablet:Z

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_c

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_8
    new-instance v3, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;

    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    move-object/from16 v0, v56

    invoke-direct {v3, v4, v0}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;)V

    goto/16 :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsPropertyId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, ""

    :goto_d
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    new-instance v2, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->getDeviceType()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsPropertyId()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsUpdateSecs()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsSampleRatio()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->getAnalyticsCategorySuffix()Ljava/lang/String;

    move-result-object v16

    move-object v8, v2

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v16}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    goto/16 :goto_4

    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_d

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "mobile_downloads_enabled"

    move/from16 v0, v66

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_5

    :cond_c
    new-instance v55, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/VideosApplication;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "IDM"

    move-object/from16 v0, v55

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "legacy_downloads_have_app_level_drm"

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm:Z

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_6

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v17, v0

    goto/16 :goto_7

    :cond_e
    const/high16 v57, 0x1000000

    goto/16 :goto_8

    :cond_f
    const/16 v27, 0x0

    goto/16 :goto_9

    :cond_10
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/VideosApplication;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    goto/16 :goto_a

    :cond_11
    sget-object v3, Lcom/google/android/youtube/core/client/StatParams$Platform;->MOBILE:Lcom/google/android/youtube/core/client/StatParams$Platform;

    goto/16 :goto_b
.end method

.method private initIsTabletV13(Landroid/content/res/Configuration;)Z
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    iget v0, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;Lcom/google/android/youtube/videos/logging/EventLogger;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p1}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const-string v1, "settings"

    invoke-interface {p2, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0a00b6

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/ExternalIntents;->getLocalizedHelpUrl(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/ExternalIntents;->viewUri(Landroid/app/Activity;Landroid/net/Uri;)V

    const-string v1, "help"

    invoke-interface {p2, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f0a00b7

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/ExternalIntents;->getLocalizedHelpUrl(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/ExternalIntents;->viewUri(Landroid/app/Activity;Landroid/net/Uri;)V

    const-string v1, "contact"

    invoke-interface {p2, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/BugReporter;->launchGoogleFeedback(Landroid/app/Activity;)V

    const-string v1, "feedback"

    invoke-interface {p2, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07009b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setShouldCheckAccountsSyncStatus()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "check_accounts_sync_status"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method


# virtual methods
.method public createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Landroid/app/Activity;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/VideosApplication;->createSearchMenuV11(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method public getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->apiRequesters:Lcom/google/android/youtube/videos/api/DefaultApiRequesters;

    return-object v0
.end method

.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    return-object v0
.end method

.method public getConfig()Lcom/google/android/youtube/videos/Config;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    return-object v0
.end method

.method public getContentNotificationManager()Lcom/google/android/youtube/videos/ContentNotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->contentNotificationManager:Lcom/google/android/youtube/videos/ContentNotificationManager;

    return-object v0
.end method

.method public getDatabase()Lcom/google/android/youtube/videos/store/Database;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method public getDownloadNotificationManager()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    return-object v0
.end method

.method public getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    return-object v0
.end method

.method public getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    return-object v0
.end method

.method public getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method public getExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public getGcmRegistrationManager()Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    return-object v0
.end method

.method public getGmsPlusOneClient()Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->gmsCoreAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->httpClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public getKnowledgeClient()Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    return-object v0
.end method

.method public getLocalVideoThumbnailStore()Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->localVideoThumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    return-object v0
.end method

.method public getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-object v0
.end method

.method public getNotificationUtil()Lcom/google/android/youtube/videos/NotificationUtil;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    return-object v0
.end method

.method public getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    return-object v0
.end method

.method public getPreferences()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getPremiumErrorMessage()Ljava/lang/CharSequence;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a00b8

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/videos/VideosApplication;->applicationVersion:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/VideosApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0031

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/VideosApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a0032

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a0033

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/youtube/core/utils/ExternalIntents;->createShopForVideosAppUri()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/VideosApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getPromoManager()Lcom/google/android/youtube/videos/PromoManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->promoManager:Lcom/google/android/youtube/videos/PromoManager;

    return-object v0
.end method

.method public getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    return-object v0
.end method

.method public getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    return-object v0
.end method

.method public getRecommendationsRequestFactory()Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
    .locals 4

    new-instance v1, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/VideosApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$DefaultFactory;-><init>(Landroid/telephony/TelephonyManager;Landroid/content/res/Resources;Lcom/google/android/youtube/videos/Config;)V

    return-object v1
.end method

.method public getRequesters()Lcom/google/android/youtube/videos/Requesters;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->requesters:Lcom/google/android/youtube/videos/DefaultRequesters;

    return-object v0
.end method

.method public getRobotAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->robotAccountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method public getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    return-object v0
.end method

.method public getStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->offlineSubtitles:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v0

    return-object v0
.end method

.method public getStreamingStatusNotifier()Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    return-object v0
.end method

.method public getStreamsSelector()Lcom/google/android/youtube/videos/StreamsSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    return-object v0
.end method

.method public getSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->offlineSubtitles:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v0

    return-object v0
.end method

.method public getVideoStats2ClientFactory()Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    return-object v0
.end method

.method public getWishlistStore()Lcom/google/android/youtube/videos/store/WishlistStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;

    return-object v0
.end method

.method public getWishlistStoreSync()Lcom/google/android/youtube/videos/store/WishlistStoreSync;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->wishlistStoreSync:Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    return-object v0
.end method

.method public hasPremiumError()Z
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final init()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->isInitialized:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->isInitialized:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/VideosApplication;->initInternal()V

    :cond_0
    return-void
.end method

.method public isTablet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->isTablet:Z

    return v0
.end method

.method public legacyDownloadsHaveAppLevelDrm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm:Z

    return v0
.end method

.method public final onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/VideosApplication;->init()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x7f0f0001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/BugReporter;->isGoogleFeedbackInstalled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f07009e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public updatePremiumErrorMessage()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/drm/DrmManager;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPremiumErrorMessageUpated(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->needsSystemUpdate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->versionCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->minimumVersion()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/videos/VideosApplication;->versionCode:I

    if-le v0, v1, :cond_2

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x1383fd63800L

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/VideosApplication;->premiumStatus:I

    goto :goto_0
.end method
