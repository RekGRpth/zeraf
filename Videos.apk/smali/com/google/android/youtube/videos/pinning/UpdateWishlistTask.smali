.class Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;
.super Lcom/google/android/youtube/videos/pinning/Task;
.source "UpdateWishlistTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask$WishlistQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/pinning/Task",
        "<",
        "Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Landroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;
    .param p2    # Landroid/os/PowerManager$WakeLock;
    .param p3    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p5    # Lcom/google/android/youtube/videos/store/Database;
    .param p6    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p9    # Landroid/net/ConnectivityManager;
    .param p10    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/youtube/videos/pinning/Task$Listener;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Landroid/net/ConnectivityManager;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p10

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/pinning/Task;-><init>(ILcom/google/android/youtube/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    const-string v0, "database cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "removeFromWishlistRequester cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "addToWishlistRequester cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method private addToWishlist()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v5, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected itemType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->failSilently()V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lcom/google/android/youtube/videos/api/AddToWishlistRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/google/android/youtube/videos/api/AddToWishlistRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v1}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v4, v3, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->onAddedToWishlist()V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v4, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v5, "request failed"

    invoke-direct {v4, v5, v2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private failSilently()V
    .locals 5

    const/4 v4, 0x0

    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v4}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;Z)V

    return-void
.end method

.method private onAddedToWishlist()V
    .locals 9

    const/4 v8, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "wishlist_item_state"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "wishlist"

    const-string v5, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ? AND wishlist_item_state = 2"

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v3, v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v3, v6, v7

    const/4 v7, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v3, v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v7, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v3, v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v3, v6, v7

    invoke-virtual {v1, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v8, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method private onRemovedFromWishlist()V
    .locals 5

    const/4 v4, 0x0

    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ? AND wishlist_item_state = 3"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v4}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;Z)V

    return-void
.end method

.method private removeFromWishlist()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v5, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/android/youtube/videos/api/AssetResourceUtil;->idFromTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected itemType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->failSilently()V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v1}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v4, v3, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->onRemovedFromWishlist()V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v4, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v5, "request failed"

    invoke-direct {v4, v5, v2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private removeItem(Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Z

    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    const-string v4, "wishlist"

    invoke-virtual {v1, v4, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    if-eqz p3, :cond_0

    :goto_0
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v0, v2, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :cond_0
    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    if-eqz p3, :cond_1

    :goto_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v5, v1, v0, v2, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v4

    :cond_1
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v7, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->failSilently()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask$WishlistQuery;->COLUMNS:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    new-array v4, v11, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    const/4 v7, 0x1

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v6, v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v6, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v11, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeFromWishlist()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-ne v9, v10, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->addToWishlist()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method protected onCompleted()V
    .locals 0

    return-void
.end method

.method protected onError(Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/Throwable;

    return-void
.end method

.method removeItem()V
    .locals 5

    const/4 v4, 0x1

    const-string v1, "wishlist_account = ? AND wishlist_item_type = ? AND wishlist_item_id = ?"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    aput-object v0, v2, v3

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2, v4}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeItem(Ljava/lang/String;[Ljava/lang/String;Z)V

    return-void
.end method
