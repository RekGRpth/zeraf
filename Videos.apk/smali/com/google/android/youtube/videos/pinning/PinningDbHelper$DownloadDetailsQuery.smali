.class interface abstract Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetailsQuery;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/PinningDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadDetailsQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "download_relative_filepath"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "license_key_id IS NOT NULL AND license_asset_id IS NOT NULL AND license_system_id IS NOT NULL"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "license_video_format"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "have_subtitles"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "download_last_modified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetailsQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
