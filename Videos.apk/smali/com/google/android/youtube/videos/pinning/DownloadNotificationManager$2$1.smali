.class Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

.field final synthetic val$completed:Ljava/util/List;

.field final synthetic val$errors:Ljava/util/List;

.field final synthetic val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$completed:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$errors:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$completed:Ljava/util/List;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateCompletedNotifications(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$500(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;->val$errors:Ljava/util/List;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateErrorNotifications(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$600(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/util/List;)V

    return-void
.end method
