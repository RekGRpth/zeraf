.class Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TransfersExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/TransfersExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkStateReceiver"
.end annotation


# instance fields
.field private volatile connected:Z

.field private volatile isWifi:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private update()Z
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;
    invoke-static {v5}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$400(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/net/ConnectivityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v5, v3, :cond_1

    move v2, v3

    :goto_1
    iget-boolean v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->connected:Z

    if-ne v5, v0, :cond_2

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->isWifi:Z

    if-ne v5, v2, :cond_2

    :goto_2
    return v4

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->connected:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->isWifi:Z

    move v4, v3

    goto :goto_2
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->connected:Z

    return v0
.end method

.method public isWifi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->isWifi:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->update()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # invokes: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pingUnlessIdle()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$300(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V

    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->update()Z

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
