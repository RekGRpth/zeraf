.class abstract Lcom/google/android/youtube/videos/pinning/LicenseTask;
.super Lcom/google/android/youtube/videos/pinning/Task;
.source "LicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/LicenseTask$1;,
        Lcom/google/android/youtube/videos/pinning/LicenseTask$LicenseInfoQuery;,
        Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;,
        Lcom/google/android/youtube/videos/pinning/LicenseTask$Refresh;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/pinning/Task",
        "<",
        "Lcom/google/android/youtube/videos/pinning/DownloadKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field protected final database:Lcom/google/android/youtube/videos/store/Database;

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private final legacyDownloadsHaveAppLevelDrm:Z


# direct methods
.method private constructor <init>(ILcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 8
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p3    # Landroid/os/PowerManager$WakeLock;
    .param p4    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p6    # Lcom/google/android/youtube/videos/store/Database;
    .param p7    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p8    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p9    # Z
    .param p10    # Landroid/net/ConnectivityManager;
    .param p11    # Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p11

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/pinning/Task;-><init>(ILcom/google/android/youtube/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    const-string v1, "drmManager cannot be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    const-string v1, "database cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v1, "accountManagerWrapper cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->legacyDownloadsHaveAppLevelDrm:Z

    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/videos/pinning/LicenseTask$1;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p3    # Landroid/os/PowerManager$WakeLock;
    .param p4    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p6    # Lcom/google/android/youtube/videos/store/Database;
    .param p7    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p8    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p9    # Z
    .param p10    # Landroid/net/ConnectivityManager;
    .param p11    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p12    # Lcom/google/android/youtube/videos/pinning/LicenseTask$1;

    invoke-direct/range {p0 .. p11}, Lcom/google/android/youtube/videos/pinning/LicenseTask;-><init>(ILcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    return-void
.end method

.method static synthetic access$100(Landroid/content/ContentValues;)V
    .locals 0
    .param p0    # Landroid/content/ContentValues;

    invoke-static {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->setNullLicense(Landroid/content/ContentValues;)V

    return-void
.end method

.method private getStoredLicenseInfo()Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/io/File;",
            "Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "video_userdata"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/LicenseTask$LicenseInfoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_account = ? AND pinning_video_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing entry in pinning table for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    const/4 v2, 0x4

    :try_start_1
    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v8, v2}, Lcom/google/android/youtube/videos/utils/DbUtils;->isAnyNull(Landroid/database/Cursor;[I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing identifers in pinning table for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    :try_start_2
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x2

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method private static setNullLicense(Landroid/content/ContentValues;)V
    .locals 1
    .param p0    # Landroid/content/ContentValues;

    const-string v0, "license_file_path_key"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_last_synced_timestamp"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_expiration_timestamp"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_video_format"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_key_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_asset_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_system_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "license_force_sync"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "download_relative_filepath"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected abstract buildRequest(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/io/File;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
.end method

.method public execute()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v8, v7}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;)Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v7, v1, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->haveLicense:Z

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v7, v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v8, v7}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->onRequestImpossible()V

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v7, v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v8, v7}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v6

    if-nez v6, :cond_3

    new-instance v7, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v8, "could not get credentials"

    invoke-direct {v7, v8}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->isCanceled()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->getStoredLicenseInfo()Landroid/util/Pair;

    move-result-object v5

    if-nez v5, :cond_4

    new-instance v7, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v8, "licenseInfo is null"

    invoke-direct {v7, v8}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->isCanceled()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->legacyDownloadsHaveAppLevelDrm:Z

    invoke-static {v7, v8}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->isAppLevelDrmEncrypted(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v7, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/io/File;

    iget-object v8, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    invoke-virtual {p0, v6, v7, v8, v0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->buildRequest(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/io/File;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v3

    new-instance v2, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v7, v3, v2}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->onResponseCompleted(Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->onResponseError(Ljava/lang/Throwable;)V

    invoke-virtual {v4}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected abstract onRequestImpossible()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onResponseCompleted(Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected abstract onResponseError(Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method
