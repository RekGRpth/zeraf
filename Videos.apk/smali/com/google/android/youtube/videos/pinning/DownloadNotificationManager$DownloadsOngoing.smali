.class public Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadsOngoing"
.end annotation


# instance fields
.field public final bytesTotal:J

.field public final bytesTotalIsIndeterminate:Z

.field public final bytesTransfered:J

.field public final count:I

.field public final singleAccount:Ljava/lang/String;

.field public final singleSeasonId:Ljava/lang/String;

.field public final singleVideoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # J
    .param p9    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleAccount:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleVideoId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleSeasonId:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransfered:J

    iput-wide p7, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    iput-boolean p9, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    return-void
.end method
