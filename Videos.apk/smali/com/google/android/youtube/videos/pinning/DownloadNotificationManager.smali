.class public Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadErrorsQuery;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompletedQuery;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoingQuery;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;,
        Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final databaseExecutor:Ljava/util/concurrent/Executor;

.field private final mainHandler:Landroid/os/Handler;

.field private notificationHandler:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/NotificationUtil;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/google/android/youtube/videos/store/Database;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Lcom/google/android/youtube/videos/NotificationUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    const-string v0, "mainHandler may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;

    const-string v0, "database may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "databaseExecutor may not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->databaseExecutor:Ljava/util/concurrent/Executor;

    const-string v0, "notificationUtil may not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/NotificationUtil;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeDownloadsOngoing()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeAllCompleted()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeAllErrors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateCompletedNotifications(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateErrorNotifications(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeCompletedFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeErrorFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V

    return-void
.end method

.method private computeAllCompleted()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos, video_userdata ON video_id = pinning_video_id"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompletedQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_status = 3 AND pinning_notification_active"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v10}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->getCompletedFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method private computeAllErrors()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos, video_userdata ON video_id = pinning_video_id"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadErrorsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_status = 4 AND pinning_notification_active"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->getErrorFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-object v10
.end method

.method private computeCompletedFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos, video_userdata ON video_id = pinning_video_id"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompletedQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_status = 3 AND pinning_notification_active AND pinning_account = ? AND pinning_video_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    const/4 v6, 0x1

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->getCompletedFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private computeDownloadsOngoing()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .locals 13

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos, video_userdata ON video_id = pinning_video_id"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoingQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_status IN (1, 2) AND pinned AND pinning_notification_active"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v4

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    new-instance v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    if-ne v2, v9, :cond_1

    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    if-ne v2, v9, :cond_2

    const/4 v5, 0x2

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v12, v5

    :goto_2
    if-ne v2, v9, :cond_3

    const/4 v4, 0x3

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    const/4 v4, 0x4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v4, 0x5

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v4, 0x6

    invoke-static {v11, v4}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v10

    move-object v4, v12

    invoke-direct/range {v1 .. v10}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move-object v4, v1

    goto :goto_0

    :cond_1
    move-object v3, v4

    goto :goto_1

    :cond_2
    move-object v12, v4

    goto :goto_2

    :cond_3
    move-object v5, v4

    goto :goto_3

    :catchall_0
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private computeErrorFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos, video_userdata ON video_id = pinning_video_id"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadErrorsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_status = 4 AND pinning_notification_active AND pinning_account = ? AND pinning_video_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    const/4 v6, 0x1

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->getErrorFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private createCompletedTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "completed_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private createErrorTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private dismissNotificationFor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->databaseExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getCompletedFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    invoke-direct {v4, v0, v3, v1, v2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method private getErrorFromCursor(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    invoke-direct {v4, v0, v3, v1, v2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method private updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    const v6, 0x7f070005

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->createCompletedTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v4, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->seasonId:Ljava/lang/String;

    invoke-static {v3, p1, p2, v4}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createCompletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v5, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->seasonId:Ljava/lang/String;

    invoke-static {v4, p1, p2, v5}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createCompletedDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v2, p3, v3, v4}, Lcom/google/android/youtube/videos/NotificationUtil;->createDownloadCompletedNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v0, v6, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0, v6}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateCompletedNotifications(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    iget-object v2, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->account:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->videoId:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    const v8, 0x7f070006

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->createErrorTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v4, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v5, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    iget-object v6, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->seasonId:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createErrorPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v5, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v6, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    iget-object v7, p3, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->seasonId:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createErrorDeletedPendingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v2, p3, v3, v4}, Lcom/google/android/youtube/videos/NotificationUtil;->createDownloadErrorNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v0, v8, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0, v8}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateErrorNotifications(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    iget-object v2, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    if-lez v1, :cond_3

    iget v1, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleAccount:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleVideoId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleSeasonId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createDownloadingOngoingPendingIntentForVideo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    const v2, 0x7f070004

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    invoke-virtual {v3, p1, v0}, Lcom/google/android/youtube/videos/NotificationUtil;->createDownloadsOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;->onOngoingNotification(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/NotificationsCallbackService;->createDownloadingOngoingPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;->onOngoingNotificationCanceled()V

    goto :goto_0
.end method


# virtual methods
.method public dismissCompletedNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public dismissErrorNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadsStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->databaseExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onDownloadsStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->databaseExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setHandler(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->notificationHandler:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->databaseExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$1;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
