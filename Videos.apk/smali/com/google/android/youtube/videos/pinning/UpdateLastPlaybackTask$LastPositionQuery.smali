.class interface abstract Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask$LastPositionQuery;
.super Ljava/lang/Object;
.source "UpdateLastPlaybackTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "LastPositionQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "last_playback_start_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "last_watched_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "resume_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask$LastPositionQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
