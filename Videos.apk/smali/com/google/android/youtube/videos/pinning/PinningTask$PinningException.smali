.class public Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
.super Lcom/google/android/youtube/videos/pinning/Task$TaskException;
.source "PinningTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/PinningTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PinningException"
.end annotation


# instance fields
.field public final drmErrorCode:Ljava/lang/Integer;

.field public final failedReason:I

.field public final fatal:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;
    .param p3    # Z
    .param p4    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->fatal:Z

    iput p4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->failedReason:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZII)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;
    .param p3    # Z
    .param p4    # I
    .param p5    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->fatal:Z

    iput p4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->failedReason:I

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->fatal:Z

    iput p3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->failedReason:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    return-void
.end method
