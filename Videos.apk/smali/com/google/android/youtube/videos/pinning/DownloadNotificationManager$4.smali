.class Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v11, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v6}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$1100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "pinning_notification_active"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v6, "video_userdata"

    const-string v7, "(pinning_account = ? AND pinning_video_id = ?) AND pinning_notification_active"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$account:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$videoId:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v2, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    move v1, v4

    :goto_0
    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v6}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$1100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v6

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$account:Ljava/lang/String;

    aput-object v8, v7, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$videoId:Ljava/lang/String;

    aput-object v5, v7, v4

    invoke-virtual {v6, v2, v1, v4, v7}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :cond_0
    move v1, v5

    goto :goto_0

    :catchall_0
    move-exception v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v7}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$1100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$account:Ljava/lang/String;

    aput-object v9, v8, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$4;->val$videoId:Ljava/lang/String;

    aput-object v5, v8, v4

    invoke-virtual {v7, v2, v1, v4, v8}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v6
.end method
