.class interface abstract Lcom/google/android/youtube/videos/pinning/PinningTask$ReservedSpaceQuery;
.super Ljava/lang/Object;
.source "PinningTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/PinningTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ReservedSpaceQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SUM(pinning_download_size) - SUM(download_bytes_downloaded)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/pinning/PinningTask$ReservedSpaceQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
