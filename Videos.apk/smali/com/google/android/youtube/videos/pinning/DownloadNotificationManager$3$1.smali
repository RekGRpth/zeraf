.class Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

.field final synthetic val$completed:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

.field final synthetic val$error:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

.field final synthetic val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$completed:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    iput-object p4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$error:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$ongoing:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$100(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$completed:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateCompletedNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$900(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->this$1:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;

    iget-object v2, v2, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;->val$error:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->updateErrorNotificationFor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$1000(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V

    return-void
.end method
