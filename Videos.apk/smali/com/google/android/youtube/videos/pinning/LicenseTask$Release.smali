.class Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;
.super Lcom/google/android/youtube/videos/pinning/LicenseTask;
.source "LicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/LicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Release"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 13
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p2    # Landroid/os/PowerManager$WakeLock;
    .param p3    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p5    # Lcom/google/android/youtube/videos/store/Database;
    .param p6    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p7    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p8    # Z
    .param p9    # Landroid/net/ConnectivityManager;
    .param p10    # Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v1, 0x1

    const/4 v12, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/videos/pinning/LicenseTask;-><init>(ILcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/videos/pinning/LicenseTask$1;)V

    return-void
.end method

.method private clearLicense()V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    # invokes: Lcom/google/android/youtube/videos/pinning/LicenseTask;->setNullLicense(Landroid/content/ContentValues;)V
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/LicenseTask;->access$100(Landroid/content/ContentValues;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    return-void
.end method


# virtual methods
.method protected buildRequest(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/io/File;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/io/File;
    .param p3    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p4    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p2, v0, p1, p3, p4}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createUnpinRequest(Ljava/io/File;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0
.end method

.method protected onCompleted()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onLicenseReleaseCompleted(Ljava/lang/String;)V

    return-void
.end method

.method protected onError(Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0, p1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected onRequestImpossible()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->clearLicense()V

    return-void
.end method

.method protected onResponseCompleted(Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->clearLicense()V

    return-void
.end method

.method protected onResponseError(Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    instance-of v1, p1, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->onRequestImpossible()V

    :goto_0
    return-void

    :cond_0
    instance-of v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;

    if-eqz v1, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v1, v0, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->KEY_VERIFICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->onRequestImpossible()V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to release "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
