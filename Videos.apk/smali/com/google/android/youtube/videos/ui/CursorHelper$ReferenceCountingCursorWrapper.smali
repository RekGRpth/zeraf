.class Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;
.super Landroid/database/CursorWrapper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReferenceCountingCursorWrapper"
.end annotation


# instance fields
.field private referenceCount:I

.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/ui/CursorHelper;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/database/CursorWrapper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized increment()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->referenceCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
