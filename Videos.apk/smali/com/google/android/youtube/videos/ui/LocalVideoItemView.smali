.class public Lcom/google/android/youtube/videos/ui/LocalVideoItemView;
.super Lcom/google/android/youtube/videos/ui/VideoItemView;
.source "LocalVideoItemView.java"


# instance fields
.field private final dateTimeTextView:Landroid/widget/TextView;

.field private final durationTextView:Landroid/widget/TextView;

.field private final titleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v4, 0x7f040018

    const/high16 v5, 0x7f0c0000

    const/high16 v6, 0x3f800000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/VideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIF)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->titleTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->dateTimeTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070044

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->durationTextView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public setInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->dateTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->durationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
