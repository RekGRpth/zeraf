.class public Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
.super Ljava/lang/Object;
.source "SuggestionsHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;,
        Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataVideoPageConverter;,
        Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TT;",
        "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private currentRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final listener:Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;

.field private final suggestionsCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
    .param p4    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p5    # Lcom/google/android/youtube/core/ErrorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TT;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            "Lcom/google/android/youtube/core/ErrorHelper;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "suggestionsRequester cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->suggestionsRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->listener:Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;

    const-string v0, "eventLogger cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ErrorHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->suggestionsCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method private static addSuggestionsV11(Landroid/widget/ArrayAdapter;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    return-void
.end method

.method private static addSuggestionsV8(Landroid/widget/ArrayAdapter;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->clear()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->addSuggestionsV11(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->addSuggestionsV8(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public init(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "request cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->suggestionsRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->suggestionsCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public isLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    if-eq p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onSuggestionsError(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->listener:Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;->onSuggestionsError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResponse(Ljava/lang/Object;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;)V
    .locals 2
    .param p2    # Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->listener:Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;

    invoke-virtual {p2}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->getResourceList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;->onSuggestionsAvailable(Ljava/util/List;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->onResponse(Ljava/lang/Object;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;)V

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->currentRequest:Ljava/lang/Object;

    return-void
.end method
