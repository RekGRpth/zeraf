.class public Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;
.super Landroid/os/Handler;
.source "FullscreenUiVisibilityHelperV11.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;
    }
.end annotation


# static fields
.field private static final HIDDEN_FULLSCREEN_FLAGS:I

.field private static final HIDE_SYSTEM_UI_FLAGS:I

.field private static final VISIBLE_FULLSCREEN_FLAGS:I


# instance fields
.field private final actionBar:Landroid/app/ActionBar;

.field private final actionBarHeight:I

.field private currentVisibilityFlags:I

.field private defaultWindowFullscreenSet:Z

.field private fullscreen:Z

.field private hasActionBar:Z

.field private insets:Landroid/graphics/Rect;

.field private final listener:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;

.field private final playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

.field private released:Z

.field private systemUiHiddenInFullscreen:Z

.field private final window:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    or-int/lit8 v1, v1, 0x2

    :cond_0
    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    or-int/lit8 v1, v1, 0x4

    or-int/lit16 v0, v0, 0x400

    or-int/lit16 v0, v0, 0x200

    or-int/lit16 v0, v0, 0x100

    :cond_1
    sput v1, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->HIDE_SYSTEM_UI_FLAGS:I

    sput v0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->VISIBLE_FULLSCREEN_FLAGS:I

    or-int v2, v0, v1

    sput v2, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->HIDDEN_FULLSCREEN_FLAGS:I

    return-void
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;)V
    .locals 5
    .param p1    # Landroid/view/Window;
    .param p2    # Landroid/app/ActionBar;
    .param p3    # Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;
    .param p4    # Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const-string v1, "window cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Window;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    const-string v1, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;)V

    iput-object p2, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBar:Landroid/app/ActionBar;

    iput-object p4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->listener:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    invoke-virtual {p3}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->currentVisibilityFlags:I

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->hasActionBar:Z

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v2, [I

    const v4, 0x10102eb

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBarHeight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_1
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iput v3, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBarHeight:I

    goto :goto_1
.end method

.method private applyDesiredFlags()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->maybeScheduleFlagApplication()V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->systemUiHiddenInFullscreen:Z

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->HIDDEN_FULLSCREEN_FLAGS:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->VISIBLE_FULLSCREEN_FLAGS:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeScheduleFlagApplication()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->removeMessages(I)V

    iget v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->currentVisibilityFlags:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->shouldApplyVisibilityFlags(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x9c4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method private shouldApplyVisibilityFlags(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->systemUiHiddenInFullscreen:Z

    and-int v1, v4, v5

    sget v4, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->HIDE_SYSTEM_UI_FLAGS:I

    and-int/2addr v4, p1

    sget v5, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->HIDE_SYSTEM_UI_FLAGS:I

    if-ne v4, v5, :cond_0

    move v0, v2

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->released:Z

    if-nez v4, :cond_1

    if-eq v1, v0, :cond_1

    :goto_1
    return v2

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private updatePaddingForInsetViews()V
    .locals 10

    const/16 v9, 0x10

    const/4 v8, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    iget-boolean v4, v3, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->isInsetView:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    if-lt v4, v9, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    if-nez v4, :cond_2

    :cond_0
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    sget v4, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    if-lt v4, v9, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_3
    iget-boolean v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->hasActionBar:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/view/Window;->hasFeature(I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBarHeight:I

    invoke-virtual {v0, v8, v4, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->applyDesiredFlags()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 2
    .param p1    # I

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->playerOverlaysLayout:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->listener:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->currentVisibilityFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->listener:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;->onNavigationShown()V

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->currentVisibilityFlags:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->maybeScheduleFlagApplication()V

    return-void
.end method

.method public onSystemWindowInsets(Landroid/graphics/Rect;)V
    .locals 1
    .param p1    # Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->insets:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->updatePaddingForInsetViews()V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->released:Z

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x400

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    if-eq v0, p1, :cond_4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->defaultWindowFullscreenSet:Z

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->applyDesiredFlags()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->updatePaddingForInsetViews()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    if-nez p1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->defaultWindowFullscreenSet:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->hasActionBar:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_1
.end method

.method public setSystemUiHidden(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->systemUiHiddenInFullscreen:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->applyDesiredFlags()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->fullscreen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->hasActionBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0
.end method
