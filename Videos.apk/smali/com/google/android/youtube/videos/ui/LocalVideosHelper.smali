.class public Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
.super Ljava/lang/Object;
.source "LocalVideosHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;
    }
.end annotation


# static fields
.field private static final bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

.field private bucketEnds:[I

.field private bucketIds:[Ljava/lang/String;

.field private bucketNames:[Ljava/lang/String;

.field private collapsedBuckets:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

.field private final listAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final logo:Landroid/graphics/Bitmap;

.field private final numColumns:I

.field private final outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

.field private queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

.field private final statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    sget-object v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Landroid/view/View;Landroid/os/Bundle;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 7
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "activity cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    const-string v2, "eventLogger cannot be null"

    invoke-static {p5, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f030000

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->logo:Landroid/graphics/Bitmap;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    sget-object v3, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/google/android/youtube/videos/ui/ViewTypes;->COLLAPSIBLE_HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aput-object v3, v2, v6

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->LOCAL_VIDEO:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-direct {v2, v1, v5}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>(Ljava/util/Set;Z)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    new-instance v2, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    const v3, 0x7f02004d

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v2, p1, v3, p2}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;-><init>(Landroid/app/Activity;Landroid/graphics/Bitmap;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    new-instance v3, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$1;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$1;-><init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    new-instance v2, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    new-array v4, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-direct {v2, v3, v5, v4}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->listAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const v2, 0x7f070046

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v2, v6}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setItemsCanFocus(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v2, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setFocusable(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v2, 0x7f070045

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1, v2, p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    if-eqz p4, :cond_0

    const-string v2, "com.google.android.videos.ui.LocalVideosHelper.COLLAPSED_BUCKETS"

    invoke-virtual {p4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.google.android.videos.ui.LocalVideosHelper.COLLAPSED_BUCKETS"

    invoke-virtual {p4, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;

    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->numColumns:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->updateOutline()V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->updateOutline()V

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;)Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
    .param p1    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)[I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[I)[I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
    .param p1    # [I

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/StatusHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/TouchAwareListView;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method private getBodyOutline(II)Lcom/google/android/youtube/core/adapter/Outline;
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x0

    if-nez p1, :cond_1

    move v8, v10

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    aget v9, v2, p1

    new-instance v1, Lcom/google/android/youtube/core/adapter/OutlineRange;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->listAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-direct {v1, v2, v8, v9}, Lcom/google/android/youtube/core/adapter/OutlineRange;-><init>(Lcom/google/android/youtube/core/adapter/Outline;II)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->inflater:Landroid/view/LayoutInflater;

    sget-object v3, Lcom/google/android/youtube/videos/ui/ViewTypes;->LOCAL_VIDEO:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    iget v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->numColumns:I

    const/4 v7, 0x0

    move v5, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v10}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->setVisible(Z)V

    :cond_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    add-int/lit8 v3, p1, -0x1

    aget v8, v2, v3

    goto :goto_0
.end method

.method private getCollapsibleHeadingOutline(ILcom/google/android/youtube/core/adapter/Outline;)Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;
    .locals 7
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/core/adapter/Outline;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;

    aget-object v6, v0, p1

    new-instance v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->inflater:Landroid/view/LayoutInflater;

    sget-object v3, Lcom/google/android/youtube/videos/ui/ViewTypes;->COLLAPSIBLE_HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;

    aget-object v4, v1, p1

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;-><init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;Lcom/google/android/youtube/core/adapter/Outline;Ljava/lang/String;)V

    return-object v0
.end method

.method private updateOutline()V
    .locals 13

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    if-nez v9, :cond_0

    move v1, v8

    :goto_0
    if-nez v1, :cond_1

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    new-instance v10, Lcom/google/android/youtube/core/adapter/SequentialOutline;

    new-array v8, v8, [Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {v10, v8}, Lcom/google/android/youtube/core/adapter/SequentialOutline;-><init>([Lcom/google/android/youtube/core/adapter/Outline;)V

    invoke-virtual {v9, v10}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V

    :goto_1
    return-void

    :cond_0
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    array-length v1, v9

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090028

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    array-length v9, v9

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, 0x1

    new-array v4, v9, [Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_2
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    array-length v9, v9

    if-ge v2, v9, :cond_3

    invoke-direct {p0, v2, v7}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->getBodyOutline(II)Lcom/google/android/youtube/core/adapter/Outline;

    move-result-object v0

    const/4 v9, 0x1

    if-ne v1, v9, :cond_2

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->inflater:Landroid/view/LayoutInflater;

    sget-object v10, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    iget-object v11, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;

    aget-object v11, v11, v8

    invoke-direct {v3, v9, v10, v11}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    :goto_3
    add-int/lit8 v6, v5, 0x1

    aput-object v3, v4, v5

    add-int/lit8 v5, v6, 0x1

    aput-object v0, v4, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->getCollapsibleHeadingOutline(ILcom/google/android/youtube/core/adapter/Outline;)Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;

    move-result-object v3

    goto :goto_3

    :cond_3
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    new-instance v9, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->inflater:Landroid/view/LayoutInflater;

    const v11, 0x7f040029

    sget-object v12, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    aput-object v9, v4, v8

    iget-object v8, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    new-instance v9, Lcom/google/android/youtube/core/adapter/SequentialOutline;

    invoke-direct {v9, v4}, Lcom/google/android/youtube/core/adapter/SequentialOutline;-><init>([Lcom/google/android/youtube/core/adapter/Outline;)V

    invoke-virtual {v8, v9}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V

    goto :goto_1
.end method


# virtual methods
.method public init()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;-><init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Lcom/google/android/youtube/videos/ui/LocalVideosHelper$1;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V
    .locals 15
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # Z
    .param p3    # I

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v9, 0x0

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    sget-object v9, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.VIEW"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8, v6}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "logo-bitmap"

    iget-object v11, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->logo:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "treat-up-as-back"

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    const/4 v7, 0x1

    :try_start_0
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v9, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v9, v6, v7}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPersonalWatchPageOpened(Ljava/lang/String;Z)V

    goto :goto_0

    :catch_0
    move-exception v2

    const/4 v7, 0x0

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;

    const v11, 0x7f0a0035

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x2

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t play local video ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onRetry()V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v1, "com.google.android.videos.ui.LocalVideosHelper.COLLAPSED_BUCKETS"

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    :cond_0
    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    return-void
.end method
