.class public Lcom/google/android/youtube/videos/ui/WelcomeOutline;
.super Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;
.source "WelcomeOutline.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/WelcomeOutline$BitmapCallback;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final bitmapRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final buttonLabel:I

.field private final clickListener:Landroid/view/View$OnClickListener;

.field private final defaultBitmap:I

.field private final welcomeBitmapUri:Landroid/net/Uri;

.field private final welcomeMessage:I

.field private final welcomeTitle:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Lcom/google/android/youtube/core/async/Requester;IIIILandroid/net/Uri;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Landroid/net/Uri;
    .param p9    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;IIII",
            "Landroid/net/Uri;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040046

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->bitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    iput p4, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeTitle:I

    iput p5, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeMessage:I

    iput p6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->buttonLabel:I

    iput p7, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->defaultBitmap:I

    iput-object p8, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeBitmapUri:Landroid/net/Uri;

    iput-object p9, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->clickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    if-ne v5, p2, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const v6, 0x7f070097

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeMessage:I

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    const v6, 0x7f070096

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeTitle:I

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    const v6, 0x7f070098

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->buttonLabel:I

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f070099

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->defaultBitmap:I

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeBitmapUri:Landroid/net/Uri;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeBitmapUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/ui/WelcomeOutline$BitmapCallback;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/ui/WelcomeOutline$BitmapCallback;-><init>(Landroid/widget/ImageView;)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->bitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->welcomeBitmapUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/youtube/videos/ui/WelcomeOutline;->activity:Landroid/app/Activity;

    invoke-static {v8, v1}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    iget-boolean v6, v1, Lcom/google/android/youtube/videos/ui/WelcomeOutline$BitmapCallback;->isComplete:Z

    if-nez v6, :cond_0

    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/google/android/youtube/videos/ui/WelcomeOutline$BitmapCallback;->shouldAnimate:Z

    goto :goto_0
.end method
