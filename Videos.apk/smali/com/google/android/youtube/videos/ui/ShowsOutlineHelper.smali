.class public Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;
.super Ljava/lang/Object;
.source "ShowsOutlineHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;


# instance fields
.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private final outline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

.field private final showSuggestions:Z

.field private final showsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final showsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final welcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ErrorHelper;Landroid/widget/ListView;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Landroid/view/View$OnClickListener;Landroid/view/View;Z)V
    .locals 19
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p3    # Landroid/widget/ListView;
    .param p4    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p5    # Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;
    .param p6    # Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;
    .param p7    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p8    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;
    .param p9    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p10    # Landroid/view/View$OnClickListener;
    .param p11    # Landroid/view/View;
    .param p12    # Z

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move/from16 v0, p12

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showSuggestions:Z

    const-string v3, "welcomeOutline cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/adapter/Outline;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->welcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    const-string v3, "showsClickListener cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v16

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v5, 0x7f0a0012

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v5, 0x7f0a0069

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p5

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p6

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const v3, 0x7f040036

    const/4 v4, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p10

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    check-cast v18, Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;-><init>(Landroid/widget/FrameLayout;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    new-instance v3, Lcom/google/android/youtube/videos/NetworkMonitor;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    new-instance v2, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    sget-object v5, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v8, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/videos/ui/ViewTypes;->SUGGESTION:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/4 v15, 0x0

    move v13, v7

    move-object/from16 v14, p9

    invoke-direct/range {v8 .. v15}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v17, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;

    const v3, 0x7f040029

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->updateVisibilities()V

    new-instance v3, Lcom/google/android/youtube/core/adapter/SequentialOutline;

    const/4 v4, 0x7

    new-array v4, v4, [Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v9, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v9, v4, v5

    const/4 v5, 0x4

    aput-object v8, v4, v5

    const/4 v5, 0x5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    aput-object v9, v4, v5

    const/4 v5, 0x6

    aput-object v17, v4, v5

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/adapter/SequentialOutline;-><init>([Lcom/google/android/youtube/core/adapter/Outline;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-void
.end method


# virtual methods
.method public getOutline()Lcom/google/android/youtube/core/adapter/Outline;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-object v0
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setDimmedStyle(Z)V

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setDimmedStyle(Z)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    return-void
.end method

.method public updateVisibilities()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move v1, v3

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v3

    :goto_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->welcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    if-nez v0, :cond_3

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->seeMoreRentalsOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->showSuggestions:Z

    if-eqz v5, :cond_4

    if-nez v1, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    :goto_3
    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setVisible(Z)V

    return-void

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3
.end method
