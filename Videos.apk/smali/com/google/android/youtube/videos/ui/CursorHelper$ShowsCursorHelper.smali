.class public final Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;
.super Lcom/google/android/youtube/videos/ui/CursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowsCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p3    # Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/CursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    return-void
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter$Query;->COLUMNS:[Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createMyShowsRequest(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method
