.class public Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;
.super Lcom/google/android/youtube/videos/ui/VideoItemView;
.source "MovieSuggestionItemView.java"


# instance fields
.field private final descriptionView:Landroid/widget/TextView;

.field private final titleView:Landroid/widget/TextView;

.field private final yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v4, 0x7f04000e

    const/high16 v5, 0x7f0c0000

    const v6, 0x3f333333

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/VideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIF)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f07002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->yearAndDurationView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->descriptionView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public setInfo(Ljava/lang/String;IILjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->getYearAndDurationText(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->yearAndDurationView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->descriptionView:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MovieSuggestionItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
