.class Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;
.super Lcom/google/android/youtube/videos/ui/StatusHelper;
.source "StatusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/StatusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewStatusHelper"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    invoke-direct {p0, p1, p3}, Lcom/google/android/youtube/videos/ui/StatusHelper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)V

    iput-object p2, p0, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/StatusHelper$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
    .param p4    # Lcom/google/android/youtube/videos/ui/StatusHelper$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)V

    return-void
.end method


# virtual methods
.method protected ensureHidden()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected ensureVisible()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;->statusView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public init()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;->ensureHidden()V

    return-void
.end method
