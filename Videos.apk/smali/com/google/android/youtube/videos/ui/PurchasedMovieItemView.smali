.class public Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;
.super Lcom/google/android/youtube/videos/ui/VideoItemView;
.source "PurchasedMovieItemView.java"


# instance fields
.field private final downloadStatusView:Landroid/widget/TextView;

.field private final downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

.field private final expirationView:Landroid/widget/TextView;

.field private final titleView:Landroid/widget/TextView;

.field private final yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v8, 0x7f07002d

    const v7, 0x7f07000f

    const v4, 0x7f04002c

    const/high16 v5, 0x7f0c0000

    const v6, 0x3f333333

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/VideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIF)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f07002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->yearAndDurationView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->expirationView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f070031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadStatusView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->detailsView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->setId(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/videos/ui/DownloadView;->setNextFocusLeftId(I)V

    invoke-virtual {p0, v8}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->setNextFocusRightId(I)V

    return-void
.end method


# virtual methods
.method public setInfo(Ljava/lang/String;IIZJLjava/lang/String;ZLjava/lang/Integer;IZ)V
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # J
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .param p9    # Ljava/lang/Integer;
    .param p10    # I
    .param p11    # Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v10, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->getYearAndDurationText(II)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->yearAndDurationView:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const/4 v4, 0x0

    if-eqz p4, :cond_4

    const v10, 0x7f0a0127

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_0
    :goto_1
    if-eqz v4, :cond_5

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->expirationView:Landroid/widget/TextView;

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->expirationView:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadStatusView:Landroid/widget/TextView;

    if-eqz p4, :cond_1

    const/16 p7, 0x0

    :cond_1
    move-object/from16 v0, p7

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez p4, :cond_2

    if-nez p11, :cond_6

    :cond_2
    const/4 v3, 0x1

    :goto_3
    iget-object v11, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    if-eqz v3, :cond_7

    const/16 v10, 0x8

    :goto_4
    invoke-virtual {v11, v10}, Lcom/google/android/youtube/videos/ui/DownloadView;->setVisibility(I)V

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v10, p1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    move/from16 v0, p8

    move-object/from16 v1, p9

    move/from16 v2, p10

    invoke-virtual {v10, v0, v1, v2}, Lcom/google/android/youtube/videos/ui/DownloadView;->setPinState(ZLjava/lang/Integer;I)V

    if-eqz v3, :cond_8

    const/4 v10, -0x1

    :goto_5
    invoke-virtual {p0, v10}, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->setNextFocusRightId(I)V

    return-void

    :cond_3
    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->yearAndDurationView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const-wide v10, 0x7fffffffffffffffL

    cmp-long v10, p5, v10

    if-eqz v10, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-wide/from16 v0, p5

    invoke-static {v0, v1, v5, v6}, Lcom/google/android/youtube/core/utils/TimeUtil;->getRemainingDays(JJ)I

    move-result v7

    const/16 v10, 0x3c

    if-gt v7, v10, :cond_0

    move-wide/from16 v0, p5

    invoke-static {v0, v1, v5, v6, v8}, Lcom/google/android/youtube/core/utils/TimeUtil;->getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/PurchasedMovieItemView;->expirationView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    :cond_7
    const/4 v10, 0x0

    goto :goto_4

    :cond_8
    const v10, 0x7f07002d

    goto :goto_5
.end method
