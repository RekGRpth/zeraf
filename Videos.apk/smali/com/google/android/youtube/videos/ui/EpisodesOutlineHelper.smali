.class public Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;
.super Ljava/lang/Object;
.source "EpisodesOutlineHelper.java"


# instance fields
.field private final otherEpisodesAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final otherEpisodesHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final outline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final purchaseColumns:I

.field private final showOtherEpisodes:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Z)V
    .locals 18
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;
    .param p3    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p4    # Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;
    .param p5    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p6    # Z

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->showOtherEpisodes:Z

    const-string v5, "activity cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "episodesAdater cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "episodesClickListener cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "otherEpisodesAdater cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "otherEpisodesClickListener cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->purchaseColumns:I

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090028

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    new-instance v16, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v5, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v6, 0x7f0a0013

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v5, v6}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    new-instance v2, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    sget-object v5, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->purchaseColumns:I

    const/4 v9, 0x0

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v5, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v6, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v9, 0x7f0a0014

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v4, v6, v9}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v5, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v6, 0x1

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p4

    invoke-direct {v5, v0, v6, v9}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    new-instance v8, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    sget-object v11, Lcom/google/android/youtube/videos/ui/ViewTypes;->SUGGESTION:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/4 v15, 0x0

    move-object v10, v4

    move v13, v7

    move-object/from16 v14, p5

    invoke-direct/range {v8 .. v15}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v17, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;

    const v5, 0x7f040029

    sget-object v6, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->updateVisibilities()V

    new-instance v5, Lcom/google/android/youtube/core/adapter/SequentialOutline;

    const/4 v6, 0x5

    new-array v6, v6, [Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v9, 0x0

    aput-object v16, v6, v9

    const/4 v9, 0x1

    aput-object v2, v6, v9

    const/4 v9, 0x2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v10, v6, v9

    const/4 v9, 0x3

    aput-object v8, v6, v9

    const/4 v9, 0x4

    aput-object v17, v6, v9

    invoke-direct {v5, v6}, Lcom/google/android/youtube/core/adapter/SequentialOutline;-><init>([Lcom/google/android/youtube/core/adapter/Outline;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-void
.end method


# virtual methods
.method public getOutline()Lcom/google/android/youtube/core/adapter/Outline;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-object v0
.end method

.method public overallPositionOfEpisode(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->purchaseColumns:I

    div-int v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public updateVisibilities()V
    .locals 2

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->showOtherEpisodes:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->otherEpisodesAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
