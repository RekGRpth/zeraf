.class public Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;
.super Ljava/lang/Object;
.source "RemoteScreenPanelHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

.field private final remoteScreenPanel:Landroid/view/View;

.field private final screenNameView:Landroid/widget/TextView;

.field private final videoThumbnailView:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p3    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->activity:Landroid/app/Activity;

    const-string v0, "posterStore cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/PosterStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    const-string v0, "remotePanel cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    const v0, 0x7f070071

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->screenNameView:Landroid/widget/TextView;

    const v0, 0x7f070072

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->videoThumbnailView:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;

    return-void
.end method


# virtual methods
.method public init(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->screenNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->activity:Landroid/app/Activity;

    invoke-static {v1, p0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "poster request failed"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->videoThumbnailView:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->setThumbnail(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->videoThumbnailView:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->setThumbnail(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setKeepScreenOn(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->remoteScreenPanel:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setKeepScreenOn(Z)V

    return-void
.end method
