.class final Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;
.super Ljava/lang/Object;
.source "MoviesHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/MoviesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SuggestionClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/MoviesHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # Z
    .param p3    # I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1000(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1100(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;
    invoke-static {v3}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1200(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1300(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v2

    const-string v3, "suggestionOnMoviesTab"

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1100(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0a0078

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method
