.class final Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;
.super Ljava/lang/Object;
.source "MoviesHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/MoviesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FeatureWelcomeButtonClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/MoviesHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1100(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->config:Lcom/google/android/youtube/videos/Config;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1400(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->knowledgeWelcomeBrowseUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;
    invoke-static {v3}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1200(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1300(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v1

    const-string v2, "featureWelcome"

    invoke-interface {v1, v4, v2, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForVertical(ZLjava/lang/String;Z)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->promoManager:Lcom/google/android/youtube/videos/PromoManager;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1500(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/PromoManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/PromoManager;->dismiss()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$1100(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a0078

    invoke-static {v1, v2, v4}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    goto :goto_0
.end method
