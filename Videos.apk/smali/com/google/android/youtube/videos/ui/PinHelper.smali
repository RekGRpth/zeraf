.class public final Lcom/google/android/youtube/videos/ui/PinHelper;
.super Ljava/lang/Object;
.source "PinHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;,
        Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;,
        Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;
    }
.end annotation


# instance fields
.field private final activity:Lvedroid/support/v4/app/FragmentActivity;

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/FragmentActivity;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-void
.end method

.method static synthetic access$000(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;

    invoke-static {p0}, Lcom/google/android/youtube/videos/ui/PinHelper;->getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    return-object v0
.end method

.method private static getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public pinVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v1, "account cannot be null or empty"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "videoId cannot be null or empty"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "download_policy_dialog_shown"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    const/4 v2, 0x1

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/youtube/videos/pinning/PinService;->requestSetPinned(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Long;
    .param p5    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V

    return-void
.end method

.method public showPinningDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/Integer;

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PinHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
