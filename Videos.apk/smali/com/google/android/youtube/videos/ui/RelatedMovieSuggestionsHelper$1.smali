.class Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;
.super Landroid/database/DataSetObserver;
.source "RelatedMovieSuggestionsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;-><init>(Landroid/app/Activity;Landroid/widget/LinearLayout;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;Lcom/google/android/youtube/core/async/Requester;Landroid/view/View;ILcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;Ljava/lang/String;IIZLcom/google/android/youtube/core/async/Requester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->updateSuggestionsHolder()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->access$000(Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;)V

    return-void
.end method

.method public onInvalidated()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->updateSuggestionsHolder()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->access$000(Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;)V

    return-void
.end method
