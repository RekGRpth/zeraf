.class public Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;
.super Landroid/widget/FrameLayout;
.source "RemoteScreenPanelThumbnailView.java"


# instance fields
.field private final backgroundView:Landroid/widget/ImageView;

.field private final backgroundViewMatrix:Landroid/graphics/Matrix;

.field private final backgroundViewMatrixValues:[F

.field private final gradientView:Landroid/view/View;

.field private moviePosterMode:Z

.field private thumbnail:Landroid/graphics/Bitmap;

.field private final thumbnailView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrixValues:[F

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    const v1, 0x7f020093

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->update()V

    return-void
.end method

.method private update()V
    .locals 4

    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->moviePosterMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    const v1, -0x7f7f80

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    sub-int v7, p4, p2

    sub-int v6, p5, p3

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->moviePosterMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v3, v7, v6}, Landroid/widget/ImageView;->layout(IIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    mul-int/2addr v0, v6

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v1, v0, v2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v3, v1, v6}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->gradientView:Landroid/view/View;

    invoke-virtual {v0, v1, v3, v7, v6}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v3, v7, v6}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrixValues:[F

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->getHeight()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setFlippedImageMatrix([FIIIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrixValues:[F

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->setValues([F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v3, 0x40000000

    if-eq v2, v3, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    mul-int/lit8 v2, v1, 0x9

    mul-int/lit8 v3, v0, 0x10

    if-le v2, v3, :cond_1

    mul-int/lit8 v2, v0, 0x10

    div-int/lit8 v1, v2, 0x9

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_1
    mul-int/lit8 v2, v1, 0x9

    div-int/lit8 v0, v2, 0x10

    goto :goto_1
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->thumbnail:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->moviePosterMode:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->update()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelThumbnailView;->invalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
