.class public Lcom/google/android/youtube/videos/ui/VideoListOutline;
.super Lcom/google/android/youtube/core/adapter/GroupingOutline;
.source "VideoListOutline.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;,
        Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    }
.end annotation


# instance fields
.field private final clickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;

.field private final innerConvertViews:[Landroid/view/View;

.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final longClickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;

.field private final positionOffset:I

.field private final spacingPixelSize:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V
    .locals 9
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p7    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;I)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p7    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;
    .param p8    # I

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/core/adapter/GroupingOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/core/adapter/Outline$ViewType;I)V

    const-string v0, "inflater cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->layoutInflater:Landroid/view/LayoutInflater;

    const-string v0, "clickListener cannot be null"

    invoke-static {p6, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->clickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;

    iput-object p7, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->longClickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;

    iput p8, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->positionOffset:I

    iput p5, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->spacingPixelSize:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->getGroupSize()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->innerConvertViews:[Landroid/view/View;

    return-void
.end method

.method private configureListeners(Landroid/view/View;I)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    const v3, 0x7f07000b

    const/4 v2, 0x1

    iget v1, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->positionOffset:I

    add-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->longClickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;

    if-eqz v1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    const v1, 0x7f07002d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->positionOffset:I

    add-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :cond_1
    return-void
.end method

.method private createLayoutParamsFor(I)Landroid/view/ViewGroup$LayoutParams;
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x3f800000

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    if-nez p1, :cond_0

    :goto_0
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    return-object v0

    :cond_0
    iget v1, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->spacingPixelSize:I

    goto :goto_0
.end method


# virtual methods
.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    move-object v2, p2

    check-cast v2, Landroid/widget/LinearLayout;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->getGroupSize()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v0, :cond_1

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->innerConvertViews:[Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    aput-object v7, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->layoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040037

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->startIndexOf(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->endIndexOf(I)I

    move-result v6

    sub-int v5, v6, v1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_2

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    add-int v7, v1, v3

    iget-object v8, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->innerConvertViews:[Landroid/view/View;

    aget-object v8, v8, v3

    invoke-virtual {v6, v7, v8, v2}, Lcom/google/android/youtube/core/adapter/Outline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->createLayoutParamsFor(I)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int v6, v1, v3

    invoke-direct {p0, v4, v6}, Lcom/google/android/youtube/videos/ui/VideoListOutline;->configureListeners(Landroid/view/View;I)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v0, :cond_3

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->innerConvertViews:[Landroid/view/View;

    const/4 v7, 0x0

    aput-object v7, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    return-object v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const v2, 0x7f07000c

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const v1, 0x7f07000b

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->clickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f07002d

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    move v2, v1

    :goto_0
    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v3, p0, v2, v1}, Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;->onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1    # Landroid/view/View;

    const v2, 0x7f07000c

    const/4 v3, 0x1

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/VideoListOutline;->longClickListener:Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, p0, v2, p1}, Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;->onItemLongClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ILandroid/view/View;)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const v1, 0x7f07000c

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f07000b

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
