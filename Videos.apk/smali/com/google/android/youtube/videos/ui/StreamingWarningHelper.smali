.class public Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
.super Landroid/content/BroadcastReceiver;
.source "StreamingWarningHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private dialog:Landroid/app/Dialog;

.field private final listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

.field private final mobileStreamingEnabled:Z

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private playing:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private registered:Z

.field private warningAccepted:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;Lcom/google/android/youtube/core/utils/NetworkStatus;Z)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;
    .param p4    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p5    # Z

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v1, "activity can\'t be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    const-string v1, "preferences can\'t be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "listener can\'t be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    const-string v1, "networkStatus cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-boolean p5, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->mobileStreamingEnabled:Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0050

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00ba

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    if-eqz p5, :cond_0

    const v1, 0x7f0a0077

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a00b9

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    return-void

    :cond_0
    const v1, 0x7f0a00bf

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private isDialogNeeded()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isMobileNetwork()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->mobileStreamingEnabled:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "warning_streaming_bandwidth"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onStreamingDeclined()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "warning_streaming_bandwidth"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->mobileStreamingEnabled:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    goto :goto_0

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onStreamingDeclined()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startWifiSettingsActivity(Landroid/app/Activity;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onPlaybackStarted()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->isDialogNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onStreamingAccepted(Z)V

    :goto_1
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->showDialog()V

    move v2, v1

    goto :goto_1
.end method

.method public onPlaybackStopped()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->registered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->isInitialStickyBroadcast()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->warningAccepted:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->isDialogNeeded()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onWifiDisconnected()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->showDialog()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->listener:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper$Listener;->onWifiConnected()V

    goto :goto_0
.end method

.method public register()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->registered:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->registered:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->registered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->registered:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->playing:Z

    return-void
.end method
