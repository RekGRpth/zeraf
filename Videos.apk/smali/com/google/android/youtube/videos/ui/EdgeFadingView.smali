.class public Lcom/google/android/youtube/videos/ui/EdgeFadingView;
.super Landroid/widget/FrameLayout;
.source "EdgeFadingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;
    }
.end annotation


# instance fields
.field private final fadingEdgeSolidBackgroundColor:I

.field private fadingEdgeVisible:Z

.field private listener:Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;

.field private maxChildHeight:I

.field private maxChildWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/16 v5, 0xff

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v4, Lcom/google/android/videos/R$styleable;->EdgeFadingView:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setFadingEdgeLength(I)V

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v2, v3, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    if-ne v4, v5, :cond_0

    :goto_0
    iput v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->fadingEdgeSolidBackgroundColor:I

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setWillNotDraw(Z)V

    return-void

    :cond_0
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    if-ne v4, v5, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0
.end method

.method private measureChildAndUpdateMax(Landroid/view/View;IILandroid/view/ViewGroup$LayoutParams;II)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/view/ViewGroup$LayoutParams;
    .param p5    # I
    .param p6    # I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->isHorizontalFadingEdgeEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->isVerticalFadingEdgeEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    iget v2, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildWidth:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, p5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildWidth:I

    iget v2, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildHeight:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, p6

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildHeight:I

    return-void

    :cond_0
    iget v2, p4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, p5, v2}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getChildMeasureSpec(III)I

    move-result v1

    goto :goto_0

    :cond_1
    iget v2, p4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p3, p6, v2}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getChildMeasureSpec(III)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method protected computeHorizontalScrollRange()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildWidth:I

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildHeight:I

    return v0
.end method

.method public getSolidColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->fadingEdgeSolidBackgroundColor:I

    return v0
.end method

.method protected measureChild(Landroid/view/View;II)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingRight()I

    move-result v1

    add-int v5, v0, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingBottom()I

    move-result v1

    add-int v6, v0, v1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->measureChildAndUpdateMax(Landroid/view/View;IILandroid/view/ViewGroup$LayoutParams;II)V

    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    iget v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    add-int v5, v0, p3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iget v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    add-int v6, v0, p5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->measureChildAndUpdateMax(Landroid/view/View;IILandroid/view/ViewGroup$LayoutParams;II)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildWidth:I

    iput v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildHeight:I

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildWidth:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getMeasuredWidth()I

    move-result v2

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->maxChildHeight:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getMeasuredHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->fadingEdgeVisible:Z

    if-eq v1, v0, :cond_2

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->fadingEdgeVisible:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->listener:Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->listener:Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;->onFadingEdgeVisibilityChanged(Z)V

    :cond_2
    return-void
.end method

.method public setHorizontalFadingEdgeEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setHorizontalFadingEdgeEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->requestLayout()V

    return-void
.end method

.method public setOnFadingEdgeVisibilityChangedListener(Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->listener:Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;

    return-void
.end method

.method public setVerticalFadingEdgeEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVerticalFadingEdgeEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->requestLayout()V

    return-void
.end method
