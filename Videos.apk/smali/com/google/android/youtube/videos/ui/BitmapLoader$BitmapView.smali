.class public interface abstract Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/BitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BitmapView"
.end annotation


# virtual methods
.method public abstract getThumbnailTag()Ljava/lang/Object;
.end method

.method public abstract setThumbnail(Landroid/graphics/Bitmap;Z)V
.end method

.method public abstract setThumbnailTag(Ljava/lang/Object;)V
.end method
