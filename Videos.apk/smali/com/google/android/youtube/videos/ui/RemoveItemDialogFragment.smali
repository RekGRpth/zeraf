.class public Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "RemoveItemDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private account:Ljava/lang/String;

.field private itemId:Ljava/lang/String;

.field private itemIsShow:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "authAccount"

    const-string v2, "account cannot be null"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "remove_item_id"

    const-string v2, "itemId cannot be null"

    invoke-static {p2, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "remove_item_title"

    const-string v2, "itemTitle cannot be null"

    invoke-static {p3, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "remove_item_is_show"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "RemoveItemDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v1

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "authAccount"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->account:Ljava/lang/String;

    const-string v4, "remove_item_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemId:Ljava/lang/String;

    const-string v4, "remove_item_is_show"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    const-string v4, "remove_item_title"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->itemIsShow:Z

    if-eqz v4, :cond_0

    const v3, 0x7f0a0039

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0037

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a003f

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a003b

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    :cond_0
    const v3, 0x7f0a0038

    goto :goto_0
.end method
