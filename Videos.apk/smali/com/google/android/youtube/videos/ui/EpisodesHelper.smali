.class public Lcom/google/android/youtube/videos/ui/EpisodesHelper;
.super Ljava/lang/Object;
.source "EpisodesHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;,
        Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodeClickListener;,
        Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;,
        Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodeClickListener;,
        Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodesCallback;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

.field private final episodesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final episodesOutlineHelper:Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private final otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

.field private final otherEpisodesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

.field private pendingEpisodesCursor:Landroid/database/Cursor;

.field private pendingOtherEpisodesCursor:Landroid/database/Cursor;

.field private pendingPosterUpdatedNotification:Z

.field private final pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private scrollPositionReported:Z

.field private seasonId:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private startDownload:Z

.field private videoIdToSelect:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/ui/PinHelper;Ljava/lang/String;ZZLcom/google/android/youtube/videos/logging/EventLogger;Z)V
    .locals 9
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p4    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p5    # Lcom/google/android/youtube/videos/ui/PinHelper;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Z
    .param p9    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p10    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iput-object p5, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iput-object p6, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->videoIdToSelect:Ljava/lang/String;

    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->startDownload:Z

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v1, Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-direct {v1, p1, p0}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    const v1, 0x7f070074

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setItemsCanFocus(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setOnTouchStateChangedListener(Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02004d

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    new-instance v1, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    move/from16 v0, p8

    invoke-direct {v1, p1, p4, v8, v0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;Z)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    new-instance v1, Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodesCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodesCallback;-><init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-direct {v1, p1, p4, v8}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    new-instance v1, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;-><init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v1, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    new-instance v4, Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodeClickListener;

    const/4 v2, 0x0

    invoke-direct {v4, p0, v2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$EpisodeClickListener;-><init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    new-instance v6, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodeClickListener;

    const/4 v2, 0x0

    invoke-direct {v6, p0, v2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodeClickListener;-><init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;)V

    move-object v2, p1

    move/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Z)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesOutlineHelper:Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

    new-instance v1, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-direct {v1}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesOutlineHelper:Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->getOutline()Lcom/google/android/youtube/core/adapter/Outline;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->outlinerAdapter:Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/EpisodesHelper;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onEpisodesNewCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/ui/EpisodesHelper;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/EpisodesHelper;
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onEpisodeClick(ZI)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/EpisodesHelper;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onOtherEpisodesNewCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/ui/EpisodesHelper;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/EpisodesHelper;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onOtherEpisodeClick(I)V

    return-void
.end method

.method private discardPendingCursors()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    :cond_1
    return-void
.end method

.method private onEpisodeClick(ZI)V
    .locals 7
    .param p1    # Z
    .param p2    # I

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v4, p2}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onPinClicked(Ljava/lang/String;Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->isAtHomeDevice()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    invoke-static {v4, v5, v3, v6}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v4, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    invoke-static {v4, v5, v3, v6}, Lcom/google/android/youtube/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1
.end method

.method private onEpisodesNewCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->processEpisodesCursor(Landroid/database/Cursor;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesOutlineHelper:Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->overallPositionOfEpisode(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setSelection(I)V

    goto :goto_0
.end method

.method private onOtherEpisodeClick(I)V
    .locals 7
    .param p1    # I

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->showId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForEpisodeDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v3, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForEpisode(Z)V

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->activity:Landroid/app/Activity;

    const v4, 0x7f0a0078

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method

.method private onOtherEpisodesNewCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesOutlineHelper:Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/EpisodesOutlineHelper;->updateVisibilities()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_1
.end method

.method private onPinClicked(Ljava/lang/String;Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showPinningDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->showErrorDialog(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPinClick()V

    goto :goto_0
.end method

.method private onPostersUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingPosterUpdatedNotification:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private processEpisodesCursor(Landroid/database/Cursor;)I
    .locals 13
    .param p1    # Landroid/database/Cursor;

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v8, -0x1

    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    const-wide/16 v3, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-ne v7, v8, :cond_1

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->videoIdToSelect:Ljava/lang/String;

    invoke-static {v9, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    :cond_1
    const/16 v9, 0x9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    cmp-long v9, v1, v3

    if-lez v9, :cond_0

    move-object v5, v0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    move-wide v3, v1

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v9, v5}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->setLastWatchedVideoId(Ljava/lang/String;)V

    if-eq v7, v8, :cond_4

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->startDownload:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->videoIdToSelect:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v11, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->startDownload:Z

    :cond_3
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->videoIdToSelect:Ljava/lang/String;

    iput-boolean v12, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->scrollPositionReported:Z

    :goto_1
    return v7

    :cond_4
    iget-boolean v9, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->scrollPositionReported:Z

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->videoIdToSelect:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    if-eq v6, v8, :cond_5

    iput-boolean v12, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->scrollPositionReported:Z

    move v7, v6

    goto :goto_1

    :cond_5
    move v7, v8

    goto :goto_1
.end method

.method private showErrorDialog(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0x8

    invoke-static {p1, v4}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x6

    invoke-static {p1, v5}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V

    return-void
.end method

.method private updateCursors()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter$Query;->PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createMyEpisodesRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter$Query;->PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createOtherEpisodesOfSeasonRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method


# virtual methods
.method public final init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->reset()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->showId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->updateCursors()V

    return-void
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->setNetworkConnected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->account:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->updateCursors()V

    :cond_0
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onPostersUpdated()V

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->seasonId:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->updateCursors()V

    :cond_0
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->setNetworkConnected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    return-void
.end method

.method public onTouchStateChanged(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onEpisodesNewCursor(Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingEpisodesCursor:Landroid/database/Cursor;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onOtherEpisodesNewCursor(Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingOtherEpisodesCursor:Landroid/database/Cursor;

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingPosterUpdatedNotification:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onPostersUpdated()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingPosterUpdatedNotification:Z

    goto :goto_0
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->episodesAdapter:Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->otherEpisodesAdapter:Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->discardPendingCursors()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->pendingPosterUpdatedNotification:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    return-void
.end method
