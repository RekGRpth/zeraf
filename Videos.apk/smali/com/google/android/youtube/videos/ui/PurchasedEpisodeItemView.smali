.class public Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;
.super Lcom/google/android/youtube/videos/ui/EpisodeItemView;
.source "PurchasedEpisodeItemView.java"


# instance fields
.field private final downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

.field private final playButtonView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v3, 0x7f07002d

    const v2, 0x7f07000e

    const v0, 0x7f04000d

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->detailsView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f07002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->playButtonView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setId(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/ui/DownloadView;->setNextFocusLeftId(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setNextFocusRightId(I)V

    return-void
.end method


# virtual methods
.method public setPinningInfo(ZZLjava/lang/Integer;I)V
    .locals 2
    .param p1    # Z
    .param p2    # Z
    .param p3    # Ljava/lang/Integer;
    .param p4    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setVisibility(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setNextFocusRightId(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/youtube/videos/ui/DownloadView;->setPinState(ZLjava/lang/Integer;I)V

    const v0, 0x7f07002d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setNextFocusDownId(I)V

    goto :goto_0
.end method

.method public setPlayButtonVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->playButtonView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->setTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    return-void
.end method
