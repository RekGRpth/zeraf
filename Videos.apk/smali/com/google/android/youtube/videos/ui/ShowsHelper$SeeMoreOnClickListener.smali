.class final Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;
.super Ljava/lang/Object;
.source "ShowsHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/ShowsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SeeMoreOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/ShowsHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/ShowsHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$700(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$800(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForShowsActivity(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/ShowsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$900(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "seeMore"

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForVertical(ZLjava/lang/String;Z)V

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$700(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a0078

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method
