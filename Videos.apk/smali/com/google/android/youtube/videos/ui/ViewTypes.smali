.class public interface abstract Lcom/google/android/youtube/videos/ui/ViewTypes;
.super Ljava/lang/Object;
.source "ViewTypes.java"


# static fields
.field public static final COLLAPSIBLE_HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final LOCAL_VIDEO:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final MOVIES_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final PURCHASE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final PURCHASE_EST:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final PURCHASE_VOD:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final SHOWS_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final SUGGESTION:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

.field public static final SUGGESTION_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "Heading"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "CollapsibleHeading"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->COLLAPSIBLE_HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "PanelSpacingFooter"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "Purchase"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "PurchaseVod"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE_VOD:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "PurchaseEst"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE_EST:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "Suggestion"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->SUGGESTION:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "SuggestionFooter"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->SUGGESTION_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "LocalVideo"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->LOCAL_VIDEO:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "ShowsWelcome"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->SHOWS_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "MoviesWelcome"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/videos/ui/ViewTypes;->MOVIES_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    return-void
.end method
