.class final Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;
.super Ljava/lang/Object;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CursorCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/CursorHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/ui/CursorHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/CursorHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;-><init>(Lcom/google/android/youtube/videos/ui/CursorHelper;)V

    return-void
.end method

.method private processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$100(Lcom/google/android/youtube/videos/ui/CursorHelper;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$102(Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    if-nez p2, :cond_3

    const/4 v0, 0x1

    :goto_1
    # setter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->haveError:Z
    invoke-static {v2, v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$202(Lcom/google/android/youtube/videos/ui/CursorHelper;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->haveError:Z
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$200(Lcom/google/android/youtube/videos/ui/CursorHelper;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$300(Lcom/google/android/youtube/videos/ui/CursorHelper;Landroid/database/Cursor;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$400(Lcom/google/android/youtube/videos/ui/CursorHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    # setter for: Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->access$402(Lcom/google/android/youtube/videos/ui/CursorHelper;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->this$0:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->updateCursors()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
