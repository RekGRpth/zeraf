.class final Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;
.super Ljava/lang/Object;
.source "PermittedStreamsRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VideoStreamsCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/youtube/core/model/Video;",
        ">;"
    }
.end annotation


# instance fields
.field private final originalCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

.field final synthetic this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p2    # Lcom/google/android/youtube/videos/async/StreamsRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    # getter for: Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->channelFormatsRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->access$000(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    new-instance v2, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    iget-object v4, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v6, p2, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;-><init>(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/util/Set;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    new-instance v2, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;-><init>(Lcom/google/android/youtube/core/model/Video$State;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
