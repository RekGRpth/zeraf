.class public Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;
.super Ljava/lang/Object;
.source "PermittedStreamsRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;,
        Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/async/StreamsRequest;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/google/android/youtube/core/model/Stream;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final abrFormats:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final channelFormatsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final videoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "videoRequester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "channelFormatsRequester cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->channelFormatsRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "abrFormats cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->abrFormats:Ljava/util/Collection;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->channelFormatsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;)Ljava/util/Collection;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->abrFormats:Ljava/util/Collection;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/async/StreamsRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p1, Lcom/google/android/youtube/videos/async/StreamsRequest;->videoId:Ljava/lang/String;

    new-instance v2, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$VideoStreamsCallback;-><init>(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->request(Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
