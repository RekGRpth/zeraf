.class public Lcom/google/android/youtube/videos/async/CallbackAsFuture;
.super Ljava/lang/Object;
.source "CallbackAsFuture.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<TE;>;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# static fields
.field private static final NULL_RESPONSE:Ljava/lang/Object;


# instance fields
.field private final resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->NULL_RESPONSE:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    return-void
.end method

.method public static create()Lcom/google/android/youtube/videos/async/CallbackAsFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/youtube/videos/async/CallbackAsFuture",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;-><init>()V

    return-object v0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    :goto_0
    const-wide/32 v0, 0x7fffffff

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 4
    .param p1    # J
    .param p3    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v2, p1, p2, p3}, Ljava/util/concurrent/ArrayBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v2}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v2

    :cond_0
    sget-object v2, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->NULL_RESPONSE:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    instance-of v2, v1, Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;

    if-eqz v2, :cond_2

    new-instance v2, Ljava/util/concurrent/ExecutionException;

    check-cast v1, Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;

    iget-object v3, v1, Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;->exception:Ljava/lang/Exception;

    invoke-direct {v2, v3}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public isCancelled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isDone()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    new-instance v1, Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;

    invoke-direct {v1, p2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture$ExceptionWrapper;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->resultQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    if-nez p2, :cond_0

    sget-object p2, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->NULL_RESPONSE:Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/concurrent/ArrayBlockingQueue;->offer(Ljava/lang/Object;)Z

    return-void
.end method
