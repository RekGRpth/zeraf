.class Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;
.super Ljava/lang/Object;
.source "StoreCachingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/async/StoreCachingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StoringCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final targetCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/youtube/videos/async/StoreCachingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/async/StoreCachingRequester;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    # getter for: Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;
    invoke-static {v1}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->access$000(Lcom/google/android/youtube/videos/async/StoreCachingRequester;)Lcom/google/android/youtube/videos/store/AbstractFileStore;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v1, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->this$0:Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    # getter for: Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;
    invoke-static {v1}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->access$000(Lcom/google/android/youtube/videos/async/StoreCachingRequester;)Lcom/google/android/youtube/videos/store/AbstractFileStore;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->remove(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
