.class final Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;
.super Ljava/lang/Object;
.source "PermittedStreamsRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ChannelFormatsCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Collection",
        "<",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final originalCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

.field final synthetic this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

.field private final videoStreams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;Lcom/google/android/youtube/videos/async/StreamsRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/util/Set;)V
    .locals 0
    .param p2    # Lcom/google/android/youtube/videos/async/StreamsRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->videoStreams:Ljava/util/Set;

    return-void
.end method

.method private rewriteStream(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->this$0:Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    # getter for: Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->abrFormats:Ljava/util/Collection;
    invoke-static {v1}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;->access$100(Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;)Ljava/util/Collection;

    move-result-object v1

    iget v2, p1, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "widevine"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->onResponse(Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->videoStreams:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/Stream;

    iget-object v3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/async/StreamsRequest;->candidates:Ljava/util/List;

    iget v4, v2, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->rewriteStream(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester$ChannelFormatsCallback;->originalRequest:Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-interface {v3, v4, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
