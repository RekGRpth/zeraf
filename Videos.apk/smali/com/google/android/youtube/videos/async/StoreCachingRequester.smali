.class public Lcom/google/android/youtube/videos/async/StoreCachingRequester;
.super Ljava/lang/Object;
.source "StoreCachingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<TR;TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "fileStore cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/AbstractFileStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "fileStore cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/AbstractFileStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/async/StoreCachingRequester;)Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/videos/store/AbstractFileStore;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/videos/async/StoreCachingRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;-><init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/videos/store/AbstractFileStore;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/videos/async/StoreCachingRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;-><init>(Lcom/google/android/youtube/videos/store/AbstractFileStore;Lcom/google/android/youtube/core/async/Requester;)V

    return-object v0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->fileStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/videos/store/AbstractFileStore;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;

    invoke-direct {v3, p0, p2}, Lcom/google/android/youtube/videos/async/StoreCachingRequester$StoringCallback;-><init>(Lcom/google/android/youtube/videos/async/StoreCachingRequester;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v2, p1, v3}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v2, Lcom/google/android/youtube/core/async/NotFoundException;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/NotFoundException;-><init>()V

    invoke-interface {p2, p1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catch Lcom/google/android/youtube/videos/store/AbstractFileStore$StoreOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
