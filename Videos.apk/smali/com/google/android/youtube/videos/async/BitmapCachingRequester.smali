.class public abstract Lcom/google/android/youtube/videos/async/BitmapCachingRequester;
.super Ljava/lang/Object;
.source "BitmapCachingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V
    .locals 1
    .param p2    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/youtube/videos/utils/BitmapLruCache;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "cache can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->cache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    const-string v0, "target can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/async/BitmapCachingRequester;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/async/BitmapCachingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->cache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    return-object v0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->toCacheKey(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->cache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;

    invoke-direct {v3, p0, v0, p2, p1}, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;-><init>(Lcom/google/android/youtube/videos/async/BitmapCachingRequester;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;)V

    invoke-interface {v2, p1, v3}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method public abstract toCacheKey(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
