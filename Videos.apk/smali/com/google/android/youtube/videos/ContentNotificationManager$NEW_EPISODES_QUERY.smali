.class interface abstract Lcom/google/android/youtube/videos/ContentNotificationManager$NEW_EPISODES_QUERY;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ContentNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "NEW_EPISODES_QUERY"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinning_account"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "show_title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/ContentNotificationManager$NEW_EPISODES_QUERY;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
