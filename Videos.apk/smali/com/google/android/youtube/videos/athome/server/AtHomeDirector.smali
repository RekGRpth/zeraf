.class public Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$3;,
        Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;,
        Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;,
        Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;,
        Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final activity:Landroid/app/Activity;

.field private volatile appLevelDrm:Z

.field private bufferingCount:I

.field private cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

.field private final cancelableCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<**>;>;"
        }
    .end annotation
.end field

.field private final controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private durationSecs:I

.field private final errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private hq:Z

.field private final idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

.field private initialDrmLevel:I

.field private latestTimeMillis:I

.field private final listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

.field private loadedStream:Lcom/google/android/youtube/core/model/Stream;

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private permittedStreams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private playWhenInitialized:Z

.field private final player:Lcom/google/android/youtube/core/player/YouTubePlayer;

.field private volatile playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

.field private purchasedFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resumeTimeMillis:I

.field private robotToken:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private selectedStream:Lcom/google/android/youtube/core/model/Stream;

.field private state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

.field private statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

.field private final statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

.field private final streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

.field private subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

.field private final subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

.field private trackingSessionStarted:Z

.field private final uiHandler:Landroid/os/Handler;

.field private userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field private video:Lcom/google/android/youtube/core/model/Video;

.field private videoCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field

.field private videoId:Ljava/lang/String;

.field private final videoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field

.field private videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/player/PlayerView;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/athome/server/IdleTimeout;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 12
    .param p1    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p3    # Lcom/google/android/youtube/core/player/PlayerView;
    .param p4    # Landroid/app/Activity;
    .param p5    # Landroid/content/SharedPreferences;
    .param p6    # Lcom/google/android/youtube/videos/Requesters;
    .param p7    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p8    # Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;
    .param p9    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p10    # Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    .param p11    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p12    # Lcom/google/android/youtube/videos/StreamsSelector;
    .param p13    # Lcom/google/android/youtube/athome/server/IdleTimeout;
    .param p14    # Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;
    .param p15    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    const-string v1, "playerView cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {p3}, Lcom/google/android/youtube/core/player/PlayerView;->getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-direct {v1, v0, v2, p0}, Lcom/google/android/youtube/core/player/YouTubePlayer;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/PlayerSurface;Lcom/google/android/youtube/core/player/YouTubePlayer$MediaPlayerFactory;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const-string v1, "accountManagerWrapper cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v1, "activity cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    const-string v1, "controllerOverlay cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    const-string v1, "statsClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    const-string v1, "requesters cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/youtube/videos/Requesters;->getPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface/range {p6 .. p6}, Lcom/google/android/youtube/videos/Requesters;->getVideoRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "drmManager cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    const-string v1, "errorHelper cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ErrorHelper;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    const-string v1, "networkStatus cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    const-string v1, "streamsSelector cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/StreamsSelector;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    const-string v1, "idleTimeout cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/athome/server/IdleTimeout;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    const-string v1, "eventLogger cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual/range {p4 .. p4}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->uiHandler:Landroid/os/Handler;

    new-instance v11, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, p4

    invoke-direct {v11, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->uiHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;)V

    move-object/from16 v0, p7

    invoke-direct {v1, v2, v11, v3, v0}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    const/4 v1, 0x1

    invoke-virtual {p3, v1}, Lcom/google/android/youtube/core/player/PlayerView;->setMakeSafeForOverscan(Z)V

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    const/4 v2, 0x0

    aput-object v11, v1, v2

    const/4 v2, 0x1

    aput-object p10, v1, v2

    invoke-virtual {p3, v1}, Lcom/google/android/youtube/core/player/PlayerView;->addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V

    const-string v1, "default_hq"

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface/range {p11 .. p11}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isFastNetwork()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static/range {p4 .. p4}, Lcom/google/android/youtube/core/utils/GservicesUtil;->probablyLowEndDevice(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface/range {p11 .. p11}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isChargeableNetwork()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setHQ(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setFullscreen(Z)V

    const/4 v1, 0x0

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setShowFullscreen(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableCallbacks:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initCallbacks()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->uiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->addListener(Landroid/os/Handler;)V

    sget-object v1, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;III)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->updatePlayerStateTimes(III)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/model/VideoStreams;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    return p1
.end method

.method static synthetic access$1704(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->bufferingCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->bufferingCount:I

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/util/Map;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onPermittedStreamsResponse(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/utils/NetworkStatus;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onLicensesResponse()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onDrmFallback(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onLicensesError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onPermittedStreamsError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/core/model/Video;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onVideo(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    return-object v0
.end method

.method private acquireLicenses()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/16 v7, 0x10

    invoke-static {v5, v6, v7}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x2

    :goto_0
    invoke-direct {v0, p0, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v5, v2, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_1
    return-void

    :cond_0
    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    if-eqz v5, :cond_2

    move-object v5, v2

    :goto_2
    invoke-virtual {v6, v5, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    if-eqz v6, :cond_3

    :goto_3
    invoke-virtual {v5, v3, v1}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_1

    :cond_2
    move-object v5, v3

    goto :goto_2

    :cond_3
    move-object v3, v2

    goto :goto_3
.end method

.method private computeAndLogDrmErrorMessage(Lcom/google/android/youtube/videos/drm/DrmException;)Landroid/util/Pair;
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmException;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const v6, 0x7f0a0091

    const/4 v3, 0x1

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$3;->$SwitchMap$com$google$android$youtube$videos$drm$DrmException$DrmError:[I

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    iget-object v4, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    iget v5, p1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitDrmError(Ljava/lang/String;ZZLcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    const v0, 0x7f0a0091

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v4, p1, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v6, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v6, 0x7f0a0089

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_2
    const v6, 0x7f0a011c

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_3
    const v6, 0x7f0a00a7

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_4
    const v6, 0x7f0a00a8

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_5
    const v6, 0x7f0a0080

    goto :goto_0

    :pswitch_6
    const v6, 0x7f0a00a6

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_7
    const v6, 0x7f0a0083

    const/4 v3, 0x0

    goto :goto_0

    :pswitch_8
    const v6, 0x7f0a0092

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private createDrmRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmRequest;
    .locals 6
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "only online stream is valid"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->robotToken:Ljava/util/Map;

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->appLevelDrm:Z

    move-object v0, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createRobotStreamingRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    return-object v1
.end method

.method private initCallbacks()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method private initInternal()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/drm/DrmManager;->getDrmLevel()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initialDrmLevel:I

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initialDrmLevel:I

    if-gez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initialDrmLevel:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitUnlockedDeviceError(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    const v1, 0x7f0a00a7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(IZ)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZING:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setLoading()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showControls()V

    new-instance v0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->peekUserAuth(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showPaused()V

    goto :goto_1
.end method

.method private notifyPlayerStateChanged()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->updatePlayerState()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    :cond_0
    return-void
.end method

.method private notifyVideoInfoChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->listener:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;->onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    :cond_0
    return-void
.end method

.method private onDrmFallback(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectVideoStreams(I)V

    return-void
.end method

.method private onInitializationError(Ljava/lang/Exception;)V
    .locals 6
    .param p1    # Ljava/lang/Exception;

    const/4 v5, 0x1

    sget-object v1, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/ErrorHelper;->getHumanizedRepresentation(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZLjava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1, v5}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private onLicensesError(Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/Exception;

    instance-of v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onInitializationError(Ljava/lang/Exception;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmException;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->computeAndLogDrmErrorMessage(Lcom/google/android/youtube/videos/drm/DrmException;)Landroid/util/Pair;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private onLicensesResponse()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_OK:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playVideo()V

    :cond_0
    return-void
.end method

.method private onPermittedStreamsError(Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Ljava/lang/Exception;

    const/4 v4, 0x0

    instance-of v0, p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    const/16 v3, 0x9

    invoke-interface {v0, v1, v2, v4, v3}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZI)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    check-cast p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v1, p1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v1, v1, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v0, v1, v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lcom/google/android/youtube/core/player/MissingStreamException;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->INITIALIZED_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    const/16 v3, 0x8

    invoke-interface {v0, v1, v2, v4, v3}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackInitError(Ljava/lang/String;ZZI)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    const v1, 0x7f0a0114

    invoke-virtual {v0, v1, v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(IZ)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onInitializationError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onPermittedStreamsResponse(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreams:Ljava/util/Map;

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initialDrmLevel:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectVideoStreams(I)V

    return-void
.end method

.method private onVideo(Lcom/google/android/youtube/core/model/Video;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/Video;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVideoInfo(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyVideoInfoChanged()V

    return-void
.end method

.method private playVideoInternal()V
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    sget-object v1, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    if-ne v0, v1, :cond_0

    iput v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->bufferingCount:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->playVideo()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->trackingSessionStarted:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->durationSecs:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    iget-boolean v6, v5, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    move v5, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;->createForVideo(Ljava/lang/String;Ljava/lang/String;IZZZ)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->trackingSessionStarted:Z

    :cond_1
    iget-object v8, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, v8, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v8}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v8

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    invoke-virtual {v0, v8, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->loadVideo(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_0
.end method

.method private selectVideoStreams(I)V
    .locals 6
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-ltz p1, :cond_2

    move v1, v2

    :goto_0
    const-string v4, "drmLevel must not be negative"

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    if-ne p1, v2, :cond_0

    move v3, v2

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->appLevelDrm:Z

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreams:Ljava/util/Map;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, p1, v4}, Lcom/google/android/youtube/videos/StreamsSelector;->getOnlineStreams(Ljava/util/Map;IZ)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    :try_end_0
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iget-object v1, v1, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iget-object v3, v3, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iget-object v4, v4, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iget-object v5, v5, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v1, v3, v4, v5, v2}, Lcom/google/android/youtube/core/model/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->setSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setSupportsQualityToggle(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setHQ(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->acquireLicenses()V

    :goto_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onPermittedStreamsError(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private updatePlayerState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iget v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    iput v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    :cond_1
    return-void
.end method

.method private updatePlayerStateTimes(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iput p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v1, v1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    sub-int/2addr v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v1, v1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    sub-int/2addr v0, v1

    const/16 v1, 0x3a98

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    if-eq v0, p3, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iput p1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iput p2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iput p3, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V

    :cond_1
    return-void
.end method


# virtual methods
.method public getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->updatePlayerState()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    return-object v0
.end method

.method public getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->title(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    iget v1, v1, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v1, v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->durationMillis(I)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->currentPositionMillis(I)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->build()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public init(Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;Ljava/util/Map;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/athome/PushRequest;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/athome/PushRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;Z)V"
        }
    .end annotation

    const/4 v2, 0x0

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->account:Ljava/lang/String;

    const-string v0, "parsedRobotToken cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->robotToken:Ljava/util/Map;

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playWhenInitialized:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->accountId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v1, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    iput v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->latestTimeMillis:I

    iput v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v1, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    iput v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V

    iget-object v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->trackingSessionStarted:Z

    :cond_0
    iget v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    iput v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    iget-object v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->purchasedFormats:Ljava/util/List;

    iget-object v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    iput-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->loadedStream:Lcom/google/android/youtube/core/model/Stream;

    iput-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    iget v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->durationSecs:I

    iget-object v0, p2, Lcom/google/android/youtube/videos/athome/PushRequest;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyVideoInfoChanged()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initInternal()V

    return-void
.end method

.method public newMediaPlayer(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Z

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;

    new-instance v1, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;

    invoke-direct {v1}, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->appLevelDrm:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/youtube/videos/drm/DrmManager;Z)V

    return-object v0
.end method

.method public onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/async/StreamsRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->purchasedFormats:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/videos/async/StreamsRequest;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->permittedStreamsCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onInitializationError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onNotAuthenticated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public pauseVideo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->pauseVideo()V

    :cond_0
    return-void
.end method

.method public playVideo()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->selectedStream:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playVideoInternal()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    goto :goto_0
.end method

.method public release(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackDestroyed()V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->reset()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->cancel()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->cancelableAuthenticatee:Lcom/google/android/youtube/core/async/CancelableAuthenticatee;

    :cond_1
    sget-object v2, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iput v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideo()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v2, v2, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iput v3, v2, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V

    :cond_2
    iput-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->video:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyVideoInfoChanged()V

    return-void
.end method

.method public seekTo(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->pauseVideo()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    sget-object v2, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->seekTo(I)V

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playVideo()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    sget-object v2, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_ENDED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showPaused()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->durationSecs:I

    if-lez v1, :cond_1

    iput p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    iget v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->durationSecs:I

    mul-int/lit16 v3, v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setTimes(III)V

    iget v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I

    iget v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->durationSecs:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-direct {p0, v1, v2, v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->updatePlayerStateTimes(III)V

    goto :goto_1
.end method

.method public setSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->setSelectedTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method
