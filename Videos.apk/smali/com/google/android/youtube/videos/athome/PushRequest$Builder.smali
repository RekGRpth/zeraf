.class public Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
.super Ljava/lang/Object;
.source "PushRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/PushRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private accountId:Ljava/lang/String;

.field private durationMillis:I

.field private purchasedFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resumeTimeMillis:I

.field private robotToken:Ljava/lang/String;

.field private subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accountId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->accountId:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/athome/PushRequest;
    .locals 8

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->accountId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->resumeTimeMillis:I

    iget v4, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->durationMillis:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->robotToken:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->purchasedFormats:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/athome/PushRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V

    return-object v0
.end method

.method public durationMillis(I)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->durationMillis:I

    return-object p0
.end method

.method public purchasedFormats(Ljava/util/List;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/youtube/videos/athome/PushRequest$Builder;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->purchasedFormats:Ljava/util/List;

    return-object p0
.end method

.method public resumeTimeMillis(I)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->resumeTimeMillis:I

    return-object p0
.end method

.method public robotToken(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->robotToken:Ljava/lang/String;

    return-object p0
.end method

.method public subtitles(Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    return-object p0
.end method

.method public videoId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->videoId:Ljava/lang/String;

    return-object p0
.end method
