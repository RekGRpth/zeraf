.class public Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;
.super Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;
.source "MediaRouteProviderV11.java"

# interfaces
.implements Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;


# instance fields
.field private mediaRouteMenuItem:Landroid/view/MenuItem;

.field private final selectableItemBackground:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const v1, 0x800003

    invoke-direct {p0, p1, v1}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;-><init>(Landroid/content/Context;I)V

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x101030e

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    iput v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->selectableItemBackground:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->init()V

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    if-eqz p3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/high16 v1, 0x7f0f0000

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f07009a

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->mediaRouteMenuItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->mediaRouteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "mediaButton obtained via getActionView() is null. This should never happen."

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->invalidate()V

    goto :goto_0

    :cond_1
    const v1, 0x800003

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouteButtonCompat;->setRouteTypes(Landroid/view/View;I)V

    iget v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->selectableItemBackground:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method protected onVisibility(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->mediaRouteMenuItem:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->mediaRouteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;->mediaRouteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public setForceHidden(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method
