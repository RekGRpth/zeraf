.class public interface abstract Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;
.super Ljava/lang/Object;
.source "AtHomeInstance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AtHomeConnectorListener"
.end annotation


# virtual methods
.method public abstract onAppVersionTooNew()V
.end method

.method public abstract onAppVersionTooOld()V
.end method

.method public abstract onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
.end method

.method public abstract onError(Landroid/support/place/rpc/RpcError;)V
.end method

.method public abstract onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
.end method

.method public abstract onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
.end method

.method public abstract onVolumeChanged(I)V
.end method
