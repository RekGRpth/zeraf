.class Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/async/StreamsRequest;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/google/android/youtube/core/model/Stream;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/async/StreamsRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/async/StreamsRequest;
    .param p2    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error loading video streams [request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/async/StreamsRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onPermittedStreamsError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/async/StreamsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;->onError(Lcom/google/android/youtube/videos/async/StreamsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/async/StreamsRequest;Ljava/util/Map;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/async/StreamsRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onPermittedStreamsResponse(Ljava/util/Map;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/util/Map;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/async/StreamsRequest;

    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;->onResponse(Lcom/google/android/youtube/videos/async/StreamsRequest;Ljava/util/Map;)V

    return-void
.end method
