.class Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;
.super Landroid/support/place/connector/EventListener;
.source "AtHomeMoviesConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Presenter"
.end annotation


# instance fields
.field private _listener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Landroid/support/place/connector/Broker;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;)V
    .locals 0
    .param p2    # Landroid/support/place/connector/Broker;
    .param p3    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->this$0:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-direct {p0, p2, p3}, Landroid/support/place/connector/EventListener;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/connector/EventListener$Listener;)V

    iput-object p3, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->_listener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    return-void
.end method


# virtual methods
.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const/4 v2, 0x0

    const-string v3, "onPlayerStateChanged"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "state"

    sget-object v4, Lcom/google/android/youtube/videos/athome/PlayerState;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v3, v4}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->_listener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    invoke-virtual {v3, v0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;Landroid/support/place/rpc/RpcContext;)V

    :goto_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const-string v3, "onVolumeChanged"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "volume"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->_listener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    invoke-virtual {v3, v0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;->onVolumeChanged(ILandroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_1
    const-string v3, "onVideoInfoChanged"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "videoInfo"

    sget-object v4, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v3, v4}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->_listener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    invoke-virtual {v3, v0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;->onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/EventListener;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
