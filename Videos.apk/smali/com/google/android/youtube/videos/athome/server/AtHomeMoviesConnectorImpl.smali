.class public Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;
.super Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;
.source "AtHomeMoviesConnectorImpl.java"

# interfaces
.implements Lcom/google/android/youtube/athome/server/ActivityBinderConnection;
.implements Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;
.implements Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;",
        "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
        "<",
        "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
        ">;",
        "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;",
        "Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;"
    }
.end annotation


# static fields
.field private static final USER_OR_GUEST_PERMISSIONS:[Ljava/lang/String;


# instance fields
.field private final account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<",
            "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
            ">;"
        }
    .end annotation
.end field

.field private binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

.field private final connectorInfo:Landroid/support/place/connector/ConnectorInfo;

.field private final volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "guest"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "user"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->USER_OR_GUEST_PERMISSIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/athome/server/ActivityBinderHandler;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/support/place/connector/Broker;",
            "Landroid/support/place/connector/PlaceInfo;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<",
            "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    iput-object p5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object p6, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->getAtHomeAccountName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->account:Ljava/lang/String;

    invoke-virtual {p3, p0}, Landroid/support/place/connector/Broker;->newConnectorInfo(Landroid/support/place/connector/Endpoint;)Landroid/support/place/connector/ConnectorInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    const-class v1, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorInfo;->setType(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getExtras()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    const-string v1, "versionInfo"

    new-instance v2, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    invoke-direct {v2}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->minApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->maxApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->buildId(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->appVersion(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->build()Lcom/google/android/youtube/athome/common/VersionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    new-instance v0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    return-void
.end method

.method private enforcePermissions(Landroid/support/place/rpc/RpcContext;)V
    .locals 3
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->USER_OR_GUEST_PERMISSIONS:[Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/place/connector/Broker;->enforceCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private getAtHomeAccountName()Ljava/lang/String;
    .locals 5

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-lt v1, v2, :cond_1

    move v1, v2

    :goto_0
    const-string v4, "there should be at least one account"

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    array-length v1, v0

    if-le v1, v2, :cond_0

    const-string v1, "there should be one and only one robot account: picking the first one"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :cond_0
    aget-object v1, v0, v3

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    return-object v1

    :cond_1
    move v1, v3

    goto :goto_0
.end method


# virtual methods
.method public getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    return-object v0
.end method

.method public getPlayerState(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PlayerState;
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRobotAccount(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->account:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoInfo(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVolume(Landroid/support/place/rpc/RpcContext;)I
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->getVolume()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Lcom/google/android/youtube/athome/server/ActivityBinder;

    check-cast p2, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;)V

    return-void
.end method

.method public onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-ne v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->removeListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V

    :cond_1
    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {p2, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->addListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V

    goto :goto_0
.end method

.method public bridge synthetic onActivityDisconnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Lcom/google/android/youtube/athome/server/ActivityBinder;

    check-cast p2, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->onActivityDisconnected(Landroid/content/ComponentName;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;)V

    return-void
.end method

.method public onActivityDisconnected(Landroid/content/ComponentName;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->removeListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->pushOnPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;->bindCurrentActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->onStop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;->unbindActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V

    invoke-super {p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->onStop()V

    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->pushOnVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    return-void
.end method

.method public onVolumeChanged(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->pushOnVolumeChanged(I)V

    return-void
.end method

.method public pause(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->pause(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public play(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->play(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public push(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/support/place/rpc/RpcContext;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushRequest;

    const-string v1, ","

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Util;->splitIntegers(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/athome/PushRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V

    invoke-virtual {p0, v0, p6}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->push2(Lcom/google/android/youtube/videos/athome/PushRequest;Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PushResponse;

    return-void
.end method

.method public push2(Lcom/google/android/youtube/videos/athome/PushRequest;Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PushResponse;
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/athome/PushRequest;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/athome/PushResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/athome/PushResponse;-><init>(Z)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->account:Ljava/lang/String;

    invoke-static {v1, v2, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;->bindActivity(Landroid/content/Intent;Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V

    :goto_1
    new-instance v0, Lcom/google/android/youtube/videos/athome/PushResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/athome/PushResponse;-><init>(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->push(Lcom/google/android/youtube/videos/athome/PushRequest;)V

    goto :goto_1
.end method

.method public seekTo(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->seekTo(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
    .param p3    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V

    :cond_0
    return-void
.end method

.method public setVolume(ILandroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->volumeHelper:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->setVolume(I)V

    return-void
.end method

.method public stop(Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcContext;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->enforcePermissions(Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->binder:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;->stop()V

    :cond_0
    return-void
.end method
