.class public Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;
.super Ljava/lang/Object;
.source "MediaRouteProviderV8.java"

# interfaces
.implements Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;


# instance fields
.field private final mediaRouteActionHelper:Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;

    const v1, 0x800003

    const v2, 0x7f070013

    invoke-virtual {p1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;-><init>(Landroid/content/Context;ILandroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;->mediaRouteActionHelper:Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;->mediaRouteActionHelper:Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->init()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;->mediaRouteActionHelper:Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->destroy()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    return-void
.end method

.method public setForceHidden(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;->mediaRouteActionHelper:Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->setForceHidden(Z)V

    return-void
.end method
