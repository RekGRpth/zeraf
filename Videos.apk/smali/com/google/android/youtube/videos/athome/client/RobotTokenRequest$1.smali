.class final Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;
.super Ljava/lang/Object;
.source "RobotTokenRequest.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
        "<",
        "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canRetry(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Ljava/lang/Exception;

    const/16 v0, 0x191

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/ErrorHelper;->isHttpException(Ljava/lang/Throwable;I)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;->canRetry(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public extractUserAuth(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object v0
.end method

.method public bridge synthetic extractUserAuth(Ljava/lang/Object;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;->extractUserAuth(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method

.method public recreateRequest(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->videoId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->robotAccountName:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    return-object v0
.end method

.method public bridge synthetic recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;->recreateRequest(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    move-result-object v0

    return-object v0
.end method
