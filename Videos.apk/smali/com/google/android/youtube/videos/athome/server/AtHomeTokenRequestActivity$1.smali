.class Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;
.super Ljava/lang/Object;
.source "AtHomeTokenRequestActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->onUserAuthReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

.field final synthetic val$googleAccountName:Ljava/lang/String;

.field final synthetic val$purchasedFormat:Ljava/lang/String;

.field final synthetic val$robotAccountName:Ljava/lang/String;

.field final synthetic val$seasonId:Ljava/lang/String;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$purchasedFormat:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$googleAccountName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$robotAccountName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$videoId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$seasonId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "AtHomeTokenRequestActivity"

    const-string v1, "Error retrieving token."

    invoke-static {v0, v1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->finish()V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->onError(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mApp:Lcom/google/android/youtube/videos/VideosApplication;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->access$000(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)Lcom/google/android/youtube/videos/VideosApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$purchasedFormat:Ljava/lang/String;

    const-string v1, "SD_FORMAT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Lcom/google/android/youtube/videos/Config;->sdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$googleAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$robotAccountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$seasonId:Ljava/lang/String;

    move-object v5, p2

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->launchActivity(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->val$purchasedFormat:Ljava/lang/String;

    const-string v1, "HD_FORMAT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Lcom/google/android/youtube/videos/Config;->sdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v7}, Lcom/google/android/youtube/videos/Config;->hdStreamingFormats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;->onResponse(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/String;)V

    return-void
.end method
