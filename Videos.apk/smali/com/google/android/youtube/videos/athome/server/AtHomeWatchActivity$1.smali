.class Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;
.super Ljava/lang/Object;
.source "AtHomeWatchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->push(Lcom/google/android/youtube/videos/athome/PushRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

.field final synthetic val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Lcom/google/android/youtube/videos/athome/PushRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$100(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;
    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$202(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Lcom/google/android/youtube/videos/athome/PushRequest;)Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parseRobotToken(Ljava/lang/String;)Ljava/util/Map;
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;
    invoke-static {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$302(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Ljava/util/Map;)Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->isValidRequest()Z
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$500(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid request: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    new-instance v0, Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v3, 0x4

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    const v5, 0x7f0a0090

    invoke-virtual {v2, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v2, v1

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$100(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->val$pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$300(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Ljava/util/Map;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    invoke-virtual {v0, v1, v2, v3, v9}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->init(Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;Ljava/util/Map;Z)V

    goto :goto_0

    :cond_1
    move v9, v4

    goto :goto_1
.end method
