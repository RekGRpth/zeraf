.class public Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;
.super Ljava/lang/Object;
.source "AtHomeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$1;,
        Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;
    }
.end annotation


# instance fields
.field private final atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private final iconView:Landroid/widget/ImageView;

.field private final moviesConnectorListener:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;

.field private registered:Z

.field private final volumeBar:Landroid/widget/ProgressBar;

.field private volumeDelta:I

.field private final volumeStep:I

.field private final volumeToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V
    .locals 7
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p3    # I

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    const-string v1, "atHomeInstance cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    if-lez p3, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "stepPercent must be strictly positive"

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    mul-int/lit8 v1, p3, 0x64

    div-int/lit8 v1, v1, 0x64

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeStep:I

    new-instance v1, Landroid/widget/Toast;

    invoke-direct {v1, p1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeToast:Landroid/widget/Toast;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040039

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeToast:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeToast:Landroid/widget/Toast;

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeToast:Landroid/widget/Toast;

    const/16 v2, 0x30

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    const v1, 0x7f07007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->iconView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v1, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;

    invoke-direct {v1, p0, v6}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$1;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->moviesConnectorListener:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;

    return-void

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private showVolumeToast(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->iconView:Landroid/widget/ImageView;

    const v1, 0x7f020026

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v2, 0x19

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->canControlPlayback()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq p1, v2, :cond_2

    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    :cond_2
    if-ne p1, v2, :cond_3

    iget v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    iget v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeStep:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    :goto_1
    const/16 v1, 0x64

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getVolume()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->showVolumeToast(I)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    iget v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeStep:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->canControlPlayback()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/16 v2, 0x19

    if-eq p1, v2, :cond_2

    const/16 v2, 0x18

    if-ne p1, v2, :cond_0

    :cond_2
    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getVolume()I

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    add-int/2addr v3, v4

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MoviesConnector: setVolume("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") called"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->setVolume(ILandroid/support/place/rpc/RpcErrorHandler;)V

    iput v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->volumeDelta:I

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public register()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->moviesConnectorListener:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->startListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->moviesConnectorListener:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper$MoviesConnectorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->stopListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->registered:Z

    :cond_0
    return-void
.end method
