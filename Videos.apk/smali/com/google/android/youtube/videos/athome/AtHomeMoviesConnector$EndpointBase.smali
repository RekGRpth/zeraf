.class public abstract Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "AtHomeMoviesConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EndpointBase"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/place/connector/Broker;
    .param p3    # Landroid/support/place/connector/PlaceInfo;

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract getPlayerState(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PlayerState;
.end method

.method public abstract getRobotAccount(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getVideoInfo(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
.end method

.method public abstract getVolume(Landroid/support/place/rpc/RpcContext;)I
.end method

.method public abstract pause(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract play(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;

    new-instance v8, Landroid/support/place/rpc/RpcData;

    invoke-direct {v8, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const/4 v9, 0x0

    const-string v0, "push"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "videoId"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "resumeTimeMillis"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v0, "durationMillis"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v0, "token"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "formats"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->push(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    :goto_0
    if-eqz v9, :cond_c

    invoke-virtual {v9}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "play"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "videoId"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->play(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_1
    const-string v0, "pause"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "videoId"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->pause(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_2
    const-string v0, "seekTo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "videoId"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "timeMillis"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->seekTo(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_3
    const-string v0, "setSubtitles"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "videoId"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "subtitles"

    sget-object v6, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v8, v0, v6}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_4
    const-string v0, "stop"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->stop(Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_5
    const-string v0, "getRobotAccount"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->getRobotAccount(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Landroid/support/place/rpc/RpcData;

    invoke-direct {v9}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "_result"

    invoke-virtual {v9, v0, v7}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "getPlayerState"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->getPlayerState(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v7

    new-instance v9, Landroid/support/place/rpc/RpcData;

    invoke-direct {v9}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "_result"

    invoke-virtual {v9, v0, v7}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "getVolume"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->getVolume(Landroid/support/place/rpc/RpcContext;)I

    move-result v7

    new-instance v9, Landroid/support/place/rpc/RpcData;

    invoke-direct {v9}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "_result"

    invoke-virtual {v9, v0, v7}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "setVolume"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "volume"

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->setVolume(ILandroid/support/place/rpc/RpcContext;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "getVideoInfo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->getVideoInfo(Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v7

    new-instance v9, Landroid/support/place/rpc/RpcData;

    invoke-direct {v9}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "_result"

    invoke-virtual {v9, v0, v7}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "push2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "request"

    sget-object v6, Lcom/google/android/youtube/videos/athome/PushRequest;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v8, v0, v6}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/PushRequest;

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->push2(Lcom/google/android/youtube/videos/athome/PushRequest;Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PushResponse;

    move-result-object v7

    new-instance v9, Landroid/support/place/rpc/RpcData;

    invoke-direct {v9}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "_result"

    invoke-virtual {v9, v0, v7}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto/16 :goto_0

    :cond_b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public abstract push(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract push2(Lcom/google/android/youtube/videos/athome/PushRequest;Landroid/support/place/rpc/RpcContext;)Lcom/google/android/youtube/videos/athome/PushResponse;
.end method

.method public pushOnPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    const-string v1, "onPlayerStateChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "videoInfo"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    const-string v1, "onVideoInfoChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnVolumeChanged(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "volume"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v1, "onVolumeChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract seekTo(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setVolume(ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract stop(Landroid/support/place/rpc/RpcContext;)V
.end method
