.class public Lcom/google/android/youtube/videos/athome/PlayerState;
.super Ljava/lang/Object;
.source "PlayerState.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<",
            "Lcom/google/android/youtube/videos/athome/PlayerState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public accountId:Ljava/lang/String;

.field public bufferedPercentage:I

.field public canRetry:Z

.field public duration:I

.field public errorMessage:Ljava/lang/String;

.field public state:I

.field public subtitles:Z

.field public time:I

.field public videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/PlayerState$1;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/PlayerState$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/athome/PlayerState;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/athome/common/SafeRpcData;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    const/4 v9, 0x0

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "state"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v3

    const-string v0, "time"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "duration"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "buffered_percentage"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v6

    const-string v0, "subtitles"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    const-string v0, "error_message"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v0, "can_retry"

    invoke-virtual {p1, v0, v9}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/athome/common/SafeRpcData;Lcom/google/android/youtube/videos/athome/PlayerState$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;
    .param p2    # Lcom/google/android/youtube/videos/athome/PlayerState$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Lcom/google/android/youtube/athome/common/SafeRpcData;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    iget v3, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget v4, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iget v5, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    iget v6, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    iget-boolean v7, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->subtitles:Z

    iget-object v8, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iget-boolean v9, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # Ljava/lang/String;
    .param p9    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iput p4, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iput p5, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    iput p6, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    iput-boolean p7, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->subtitles:Z

    iput-object p8, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PlayerState [accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bufferedPercentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subtitles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->subtitles:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/PlayerState;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "account_id"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "video_id"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "state"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "time"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "duration"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "buffered_percentage"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "subtitles"

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->subtitles:Z

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "error_message"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "can_retry"

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
