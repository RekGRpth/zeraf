.class public Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;
.super Landroid/support/place/connector/EventListener$Listener;
.source "AtHomeMoviesConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Listener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/EventListener$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;Landroid/support/place/rpc/RpcContext;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;Landroid/support/place/rpc/RpcContext;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    return-void
.end method

.method public onVolumeChanged(ILandroid/support/place/rpc/RpcContext;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/support/place/rpc/RpcContext;

    return-void
.end method
