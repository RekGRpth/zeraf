.class Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;
.super Landroid/os/AsyncTask;
.source "AtHomeTokenRequestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserAuthAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/accounts/Account;",
        "Ljava/lang/Void;",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;
    .param p2    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 3
    .param p1    # [Landroid/accounts/Account;

    const/4 v2, 0x0

    aget-object v0, p1, v2

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mApp:Lcom/google/android/youtube/videos/VideosApplication;
    invoke-static {v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->access$000(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)Lcom/google/android/youtube/videos/VideosApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->doInBackground([Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleUserAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->access$102(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->onUserAuthReceived()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->access$200(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->onPostExecute(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    return-void
.end method
