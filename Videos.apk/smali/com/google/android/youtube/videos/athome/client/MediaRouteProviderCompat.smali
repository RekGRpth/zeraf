.class public Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderCompat;
.super Ljava/lang/Object;
.source "MediaRouteProviderCompat.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/app/Activity;)Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;
    .locals 2
    .param p0    # Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderDummy;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderDummy;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV16;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV16;-><init>()V

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV11;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV8;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method
