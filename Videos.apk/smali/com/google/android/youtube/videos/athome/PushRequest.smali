.class public Lcom/google/android/youtube/videos/athome/PushRequest;
.super Ljava/lang/Object;
.source "PushRequest.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/PushRequest$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/youtube/videos/athome/PushRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<",
            "Lcom/google/android/youtube/videos/athome/PushRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final accountId:Ljava/lang/String;

.field public final durationMillis:I

.field public final purchasedFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final resumeTimeMillis:I

.field public final robotToken:Ljava/lang/String;

.field public final subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushRequest$1;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/PushRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/athome/PushRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushRequest$2;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/PushRequest$2;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/athome/PushRequest;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/google/android/youtube/athome/common/SafeRpcData;

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p1}, Landroid/support/place/rpc/RpcData;-><init>(Landroid/os/Parcel;)V

    invoke-direct {v0, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;-><init>(Landroid/support/place/rpc/RpcData;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/PushRequest;-><init>(Lcom/google/android/youtube/athome/common/SafeRpcData;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/athome/common/SafeRpcData;)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    const/4 v4, 0x0

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "resume_time_millis"

    invoke-virtual {p1, v0, v4}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v3

    const-string v0, "duration_millis"

    invoke-virtual {p1, v0, v4}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "robot_token"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "purchased_formats"

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, v0, v6}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getList(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    const-string v0, "subtitles"

    sget-object v7, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {p1, v0, v7}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v7

    check-cast v7, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/athome/PushRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->accountId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    iput p4, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    iput-object p5, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    iput-object p7, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PushRequest [accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resumeTimeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", durationMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", robotToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", purchasedFormats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subtitles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/PushRequest;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "account_id"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->accountId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "video_id"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "resume_time_millis"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "duration_millis"

    iget v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "robot_token"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "purchased_formats"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putList(Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "subtitles"

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/PushRequest;->subtitles:Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    return-void
.end method
