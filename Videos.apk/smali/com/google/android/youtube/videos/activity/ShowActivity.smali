.class public Lcom/google/android/youtube/videos/activity/ShowActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "ShowActivity.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

.field private database:Lcom/google/android/youtube/videos/store/Database;

.field private episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

.field private errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private haveSeasons:Z

.field private mainLayout:Landroid/view/View;

.field private mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

.field private pendingPurchaseRequest:Z

.field private pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

.field private purchaseCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private seasonId:Ljava/lang/String;

.field private seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

.field private showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

.field private showId:Ljava/lang/String;

.field private started:Z

.field private statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

.field private syncCompleted:Z

.field private syncException:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/activity/ShowActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onSyncSuccess()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/activity/ShowActivity;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/activity/ShowActivity;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/activity/ShowActivity;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/activity/ShowActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/activity/ShowActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/activity/ShowActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/activity/ShowActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->refreshPurchase()V

    return-void
.end method

.method public static createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity;->createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_download"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    const-string v0, "seasonId cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    const-string v0, "showId cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private onPurchaseCursor(Landroid/database/Cursor;)V
    .locals 17
    .param p1    # Landroid/database/Cursor;

    const/4 v12, 0x1

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    invoke-virtual {v13}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;->setNoSeason()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v12, :cond_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-nez v13, :cond_2

    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    const/4 v13, 0x1

    if-ne v2, v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v13, v0, v1, v14}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v13, v10, v0, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    invoke-virtual {v13, v10}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;->setSingleSeason(Landroid/view/View;)V

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->haveSeasons:Z

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->updateVisibilities()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v12, :cond_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    const/4 v11, -0x1

    const-wide/16 v8, -0x1

    const/4 v6, -0x1

    const/4 v7, 0x0

    :cond_4
    const/4 v13, 0x0

    :try_start_2
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-static {v3, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    :goto_2
    const/4 v13, -0x1

    if-ne v11, v13, :cond_5

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    move v11, v6

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;->changeCursor(Landroid/database/Cursor;)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    invoke-virtual {v13, v14, v11}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;->setMultipleSeasons(Landroid/widget/SpinnerAdapter;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v13

    if-eqz v12, :cond_6

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v13

    :cond_7
    const/4 v13, 0x4

    :try_start_3
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v13, v4, v8

    if-lez v13, :cond_8

    move-wide v8, v4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    move-object v7, v3

    :cond_8
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v13

    if-nez v13, :cond_4

    goto :goto_2
.end method

.method private onSyncError(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncCompleted:Z

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->updateVisibilities()V

    return-void
.end method

.method private onSyncSuccess()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncCompleted:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->updateVisibilities()V

    return-void
.end method

.method private refreshPurchase()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter$Query;->COLUMNS:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    const-string v3, "_id"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createMySeasonsRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter$Query;->COLUMNS:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    const-string v3, "_id"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createMySeasonsOfSameShowRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    goto :goto_1
.end method

.method private startSync()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncCompleted:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->updateVisibilities()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p0, p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    return-void
.end method

.method private updateVisibilities()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->haveSeasons:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mainLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mainLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncCompleted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->syncException:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    const v1, 0x7f0a0081

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForSeason(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;ZLcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const v3, 0x7f070074

    const/4 v13, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v0, "authAccount"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    const-string v0, "show_id"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v0, "season_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    :goto_0
    const-string v0, "video_id"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "start_download"

    invoke-virtual {v12, v0, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "invalid arguments format: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->finish()V

    :goto_1
    return-void

    :cond_1
    const-string v0, "season_id"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const v0, 0x7f040031

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;->create(Lcom/google/android/youtube/videos/activity/ShowActivity;)Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    invoke-static {p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderCompat;->create(Landroid/app/Activity;)Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mainLayout:Landroid/view/View;

    const v0, 0x7f070073

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v0, p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    new-instance v0, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    new-instance v0, Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/ui/PinHelper;-><init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    new-instance v0, Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->allowDownloads()Z

    move-result v8

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->showsVerticalEnabled()Z

    move-result v10

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/ui/PinHelper;Ljava/lang/String;ZZLcom/google/android/youtube/videos/logging/EventLogger;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v13}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_3
    new-instance v0, Lcom/google/android/youtube/videos/activity/ShowActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/ShowActivity$1;-><init>(Lcom/google/android/youtube/videos/activity/ShowActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/ShowActivity$2;-><init>(Lcom/google/android/youtube/videos/activity/ShowActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->purchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->atHomeVolumeStepPercent()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v0, "video_id"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v0, "start_download"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0, v11}, Lcom/google/android/youtube/videos/activity/ShowActivity;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->destroy()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNotAuthenticated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->finish()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showActivityCompat:Lcom/google/android/youtube/videos/activity/ShowActivityCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;Lcom/google/android/youtube/videos/logging/EventLogger;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onRetry()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->startSync()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "season_id"

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->refreshPurchase()V

    :cond_0
    return-void
.end method

.method public final onSeasonSelected(I)V
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->showId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/logging/EventLogger;->onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account does not exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->refreshPurchase()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onStart()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->startSync()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->started:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->started:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->started:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->seasonItemAdapter:Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    iput-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->episodesHelper:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onStop()V

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method onUpPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity;->account:Ljava/lang/String;

    const-string v1, "shows"

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->finish()V

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
