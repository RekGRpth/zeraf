.class Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;
.super Lcom/google/android/youtube/videos/activity/ShowActivityCompat;
.source "ShowActivityCompat.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/ShowActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# instance fields
.field private final activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

.field private final seasonSpinner:Landroid/widget/Spinner;

.field private final singleSeasonContainer:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/ShowActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    const v1, 0x7f070015

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->singleSeasonContainer:Landroid/widget/FrameLayout;

    const v1, 0x7f070016

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v1, 0x7f070010

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onBackPressed()V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onSeasonSelected(I)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public setMultipleSeasons(Landroid/widget/SpinnerAdapter;I)V
    .locals 2
    .param p1    # Landroid/widget/SpinnerAdapter;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->singleSeasonContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public setNoSeason()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->singleSeasonContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    return-void
.end method

.method public setSingleSeason(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->singleSeasonContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->singleSeasonContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V8;->seasonSpinner:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    return-void
.end method
