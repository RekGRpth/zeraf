.class abstract Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.super Ljava/lang/Object;
.source "WatchActivityCompat.java"

# interfaces
.implements Lcom/google/android/youtube/videos/player/Director$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;,
        Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V9;,
        Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;,
        Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    :goto_0
    return-object v1

    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v1, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x9

    if-lt v0, v1, :cond_2

    new-instance v1, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V9;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V9;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public initDirectorForLocalPlayback(Lcom/google/android/youtube/videos/player/Director;ZZ)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/player/Director;
    .param p2    # Z
    .param p3    # Z

    if-eqz p2, :cond_0

    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/youtube/videos/player/Director;->initLocalPlayback(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract onAtHomeConnectorChanged()V
.end method

.method public abstract onCreate()V
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onPlay()V
    .locals 0

    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    return-void
.end method
