.class abstract Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
.super Ljava/lang/Object;
.source "HomeActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;,
        Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/videos/activity/HomeActivity;)Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/VideosApplication;
    .param p1    # Lcom/google/android/youtube/videos/activity/HomeActivity;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;-><init>(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/videos/activity/HomeActivity;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;-><init>(Lcom/google/android/youtube/videos/activity/HomeActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    return-void
.end method

.method public onOptionsMenuInvalidated(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public abstract onPageSelected(I)V
.end method

.method public abstract onTabsChanged(Lvedroid/support/v4/app/FragmentPagerAdapter;I)V
.end method
