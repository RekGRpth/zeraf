.class Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;
.super Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.source "WatchActivityCompat.java"

# interfaces
.implements Landroid/app/ActionBar$OnMenuVisibilityListener;
.implements Lcom/google/android/youtube/core/utils/DockReceiver$DockListener;
.implements Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;
.implements Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private actionBar:Landroid/app/ActionBar;

.field protected final activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

.field private final dockReceiver:Lcom/google/android/youtube/core/utils/DockReceiver;

.field private fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

.field private final hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

.field private isMenuVisible:Z

.field private isUserInteractionExpected:Z

.field private isVideoPlaying:Z

.field private final window:Landroid/view/Window;

.field private zoomHelper:Lcom/google/android/youtube/videos/player/ZoomHelperV11;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    new-instance v0, Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    new-instance v0, Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/core/utils/DockReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/DockReceiver$DockListener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    return-void
.end method


# virtual methods
.method protected isConnectedToExternalDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->isHdmiPlugged()Z

    move-result v0

    return v0
.end method

.method protected lockOrientation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->getDockState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAtHomeConnectorChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateOrientation()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public onCreate()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/16 v0, 0x1c

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v0, v0}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, p0}, Landroid/app/ActionBar;->addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    const v3, 0x7f070080

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/PlayerView;

    new-instance v2, Lcom/google/android/youtube/videos/player/ZoomHelperV11;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/PlayerView;->getPlayerSurface()Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/player/PlayerSurface;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/youtube/videos/player/ZoomHelperV11;

    new-instance v2, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-direct {v2, v3, v4, v1, p0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11$Listener;)V

    iput-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->setFullscreen(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/youtube/videos/player/ZoomHelperV11;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar;->removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->release()V

    return-void
.end method

.method public onDockState(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateOrientation()V

    return-void
.end method

.method public onHdmiPluggedState(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    return-void
.end method

.method public onMenuVisibilityChanged(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->setSystemUiHidden(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNavigationShown()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->showControls()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onUpPressed()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->zoomHelper:Lcom/google/android/youtube/videos/player/ZoomHelperV11;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public onPlay()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isVideoPlaying:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->register()V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->dockReceiver:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->unregister()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->setSystemUiHidden(Z)V

    return-void
.end method

.method public onUserInteractionExpected()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->setSystemUiHidden(Z)V

    return-void
.end method

.method public onUserInteractionNotExpected()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->updateScreenBrightness()V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->fullscreenUiVisibilityHelper:Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/FullscreenUiVisibilityHelperV11;->setSystemUiHidden(Z)V

    :cond_1
    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final updateOrientation()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->lockOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->setRequestedOrientation(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final updateScreenBrightness()V
    .locals 4

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isUserInteractionExpected:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isMenuVisible:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->isConnectedToExternalDisplay()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    const v1, 0x3c23d70a

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->window:Landroid/view/Window;

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/high16 v1, -0x40800000

    goto :goto_1
.end method
