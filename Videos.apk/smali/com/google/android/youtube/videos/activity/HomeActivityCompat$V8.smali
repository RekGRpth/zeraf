.class Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;
.super Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
.source "HomeActivityCompat.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# instance fields
.field private final activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

.field private final headerTitle:Landroid/widget/TextView;

.field private final shopButton:Landroid/view/View;

.field private final tabRow:Lcom/google/android/youtube/videos/ui/TabRow;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/HomeActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    const v0, 0x7f070014

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->shopButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->shopButton:Landroid/view/View;

    new-instance v1, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8$1;-><init>(Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070012

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/TabRow;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/TabRow;->setOnTabClickListener(Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;)V

    const v0, 0x7f070011

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->headerTitle:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;)Lcom/google/android/youtube/videos/activity/HomeActivity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    return-object v0
.end method


# virtual methods
.method public onOptionsMenuInvalidated(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->shopButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TabRow;->getTabCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/ui/TabRow;->focusTab(IZ)V

    :cond_0
    return-void
.end method

.method public onTabClicked(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->selectPage(I)V

    return-void
.end method

.method public onTabsChanged(Lvedroid/support/v4/app/FragmentPagerAdapter;I)V
    .locals 8
    .param p1    # Lvedroid/support/v4/app/FragmentPagerAdapter;
    .param p2    # I

    const/16 v2, 0x8

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/ui/TabRow;->removeAllTabs()V

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getCount()I

    move-result v3

    if-le v3, v7, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/videos/ui/TabRow;->setVisibility(I)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    const v4, 0x7f040004

    const v5, 0x7f070017

    invoke-virtual {p1, v0}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/ui/TabRow;->addTab(IILjava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v3, p2, v1}, Lcom/google/android/youtube/videos/ui/TabRow;->focusTab(IZ)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->headerTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->tabRow:Lcom/google/android/youtube/videos/ui/TabRow;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/videos/ui/TabRow;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V8;->headerTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
