.class Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;
.super Lcom/google/android/youtube/videos/activity/ShowActivityCompat;
.source "ShowActivityCompat.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/ShowActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private final actionBar:Landroid/app/ActionBar;

.field private final activity:Lcom/google/android/youtube/videos/activity/ShowActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/ShowActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/ShowActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v0, v0}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method


# virtual methods
.method public onNavigationItemSelected(IJ)Z
    .locals 1
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onSeasonSelected(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->onUpPressed()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMultipleSeasons(Landroid/widget/SpinnerAdapter;I)V
    .locals 3
    .param p1    # Landroid/widget/SpinnerAdapter;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    return-void
.end method

.method public setNoSeason()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method

.method public setSingleSeason(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/16 v3, 0x8

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const v2, 0x7f07001b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f07001c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/ShowActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method
