.class Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;
.super Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;
.source "WatchActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V17"
.end annotation


# instance fields
.field private final mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

.field private final mediaRouter:Landroid/media/MediaRouter;

.field private presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    new-instance v0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateDisplays()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)Landroid/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method private supportsLiveVideo(Landroid/media/MediaRouter$RouteInfo;)Z
    .locals 2
    .param p1    # Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private supportsPresentationDisplay(Landroid/media/MediaRouter$RouteInfo;)Z
    .locals 4
    .param p1    # Landroid/media/MediaRouter$RouteInfo;

    const/4 v2, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v2, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private updateDisplays()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->supportsLiveVideo(Landroid/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->supportsPresentationDisplay(Landroid/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eq v1, v0, :cond_2

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateOrientation()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateScreenBrightness()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onPresentationDisplayRouteChanged()V

    :cond_2
    return-void
.end method


# virtual methods
.method public initDirectorForLocalPlayback(Lcom/google/android/youtube/videos/player/Director;ZZ)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/player/Director;
    .param p2    # Z
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_2

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/videos/player/Director;->initLocalPresentationPlaybackV17(ZLandroid/media/MediaRouter$RouteInfo;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->initDirectorForLocalPlayback(Lcom/google/android/youtube/videos/player/Director;ZZ)V

    goto :goto_1
.end method

.method protected isConnectedToExternalDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected lockOrientation()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->lockOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHdmiPluggedState(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateDisplays()V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    invoke-super {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V11;->onStop()V

    return-void
.end method
