.class public interface abstract Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter$Query;
.super Ljava/lang/Object;
.source "ShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Query"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "season_id AS _id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "show_title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "season_title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "show_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "(SELECT max( max(purchase_timestamp), ifnull(max(last_watched_timestamp), 0)) FROM video_userdata, videos WHERE pinning_account = account AND pinning_video_id = video_id AND episode_season_id = season_id)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/activity/ShowActivity$SeasonItemAdapter$Query;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
