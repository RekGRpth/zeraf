.class public Lcom/google/android/youtube/videos/activity/MoviesFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "MoviesFragment.java"


# instance fields
.field private moviesHelper:Lcom/google/android/youtube/videos/ui/MoviesHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 23
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/MoviesFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v22

    check-cast v22, Lcom/google/android/youtube/videos/VideosApplication;

    new-instance v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getPromoManager()Lcom/google/android/youtube/videos/PromoManager;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/MoviesFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getMoviesEstCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getMoviesVodCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;

    move-result-object v8

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v9

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v10

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v11

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v12

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v13

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-result-object v14

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getPinHelper()Lcom/google/android/youtube/videos/ui/PinHelper;

    move-result-object v15

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v16

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v17

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getRecommendationsRequestFactory()Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move-result-object v18

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v19

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->allowDownloads()Z

    move-result v20

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->useGDataMovieSuggestions()Z

    move-result v21

    move-object v6, v4

    invoke-direct/range {v1 .. v21}, Lcom/google/android/youtube/videos/ui/MoviesHelper;-><init>(Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/PromoManager;Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/ui/SyncHelper;Lcom/google/android/youtube/videos/ui/PinHelper;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/videos/api/ApiRequesters;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;ZZZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/MoviesFragment;->moviesHelper:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/MoviesFragment;->moviesHelper:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f040026

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/MoviesFragment;->moviesHelper:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/MoviesFragment;->moviesHelper:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onStop()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method
