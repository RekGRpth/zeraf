.class abstract Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;
.super Ljava/lang/Object;
.source "WatchDetailsActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V8;,
        Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V8;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V8;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method
