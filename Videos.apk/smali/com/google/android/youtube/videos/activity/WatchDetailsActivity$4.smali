.class Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;
.super Ljava/lang/Object;
.source "WatchDetailsActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->access$700(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->onThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method
