.class public Lcom/google/android/youtube/videos/activity/HomeActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "HomeActivity.java"

# interfaces
.implements Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;
    }
.end annotation


# instance fields
.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private defaultWelcomeVertical:I

.field private enabledVerticals:I

.field private eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

.field private homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

.field private mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

.field private moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

.field private moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

.field private pendingVertical:I

.field private pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

.field private premiumErrorMessage:Ljava/lang/CharSequence;

.field private purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

.field private syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

.field private typesWithContent:I

.field private typesWithFirstCursor:I

.field private viewPager:Lvedroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "selected_vertical"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    return-object v0
.end method

.method private getCurrentVertical()I
    .locals 2

    iget v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Lvedroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->getVertical(I)I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private haveContent(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private haveFirstCursors(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithFirstCursor:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    iput v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithFirstCursor:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->setVerticals(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->onTabsChanged(Lvedroid/support/v4/app/FragmentPagerAdapter;I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->init(Ljava/lang/String;ZZ)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->supportInvalidateOptionsMenu()V

    return-void
.end method

.method private initVerticals()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveContent(I)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveContent(I)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveContent(I)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v0

    if-nez v0, :cond_5

    iget v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    if-nez v4, :cond_5

    iget v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->defaultWelcomeVertical:I

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_5

    iget v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->defaultWelcomeVertical:I

    :cond_5
    if-eqz v0, :cond_6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v4, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->setVerticals(Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v4, v1, v3}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    iput v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Lvedroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->onTabsChanged(Lvedroid/support/v4/app/FragmentPagerAdapter;I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->supportInvalidateOptionsMenu()V

    return-void

    :cond_6
    move v1, v3

    goto :goto_0
.end method

.method private maybePrepareAccountForPlusOne()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getGmsPlusOneClient()Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->prepareForAccount(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private parseExternalVerticalName(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "movies"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "shows"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "local"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowPremiumMenuItem(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    if-eq p1, v1, :cond_0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public areVerticalsEnabled(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMoviesEstCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    return-object v0
.end method

.method public getMoviesVodCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    return-object v0
.end method

.method public getPinHelper()Lcom/google/android/youtube/videos/ui/PinHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    return-object v0
.end method

.method public getShowsCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    return-object v0
.end method

.method public getSyncHelper()Lcom/google/android/youtube/videos/ui/SyncHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/SyncHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04000f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-static {v0, p0}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->create(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/videos/activity/HomeActivity;)Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    invoke-static {p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderCompat;->create(Landroid/app/Activity;)Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->atHomeVolumeStepPercent()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    new-instance v0, Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/ui/PinHelper;-><init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    new-instance v0, Lcom/google/android/youtube/videos/ui/SyncHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getGcmRegistrationManager()Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/SyncHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/accounts/SignInManager;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->addListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {v0, p0, v7, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {v0, p0, v7, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {v0, p0, v7, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->moviesVerticalEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v8

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    iget v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->showsVerticalEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->shopVersionSupportsShows(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v10

    :goto_1
    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    iget v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->localVerticalEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v9, 0x4

    :cond_0
    or-int/2addr v0, v9

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->enabledVerticals:I

    invoke-interface {v6}, Lcom/google/android/youtube/videos/Config;->defaultWelcomeVertical()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->parseExternalVerticalName(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->defaultWelcomeVertical:I

    if-eqz p1, :cond_1

    const-string v0, "types_with_content"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    const-string v0, "vertical"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    :cond_1
    new-instance v0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;-><init>(Lcom/google/android/youtube/videos/activity/HomeActivity;Lvedroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v10}, Lvedroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v8}, Lvedroid/support/v4/view/ViewPager;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->fragmentPageAdapter:Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setAdapter(Lvedroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    return-void

    :cond_2
    move v0, v9

    goto :goto_0

    :cond_3
    move v0, v9

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    const v4, 0x7f0f0002

    invoke-virtual {v0, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v1

    invoke-direct {p0, v1, v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->shouldShowPremiumMenuItem(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->shouldShowPremiumMenuItem(IZ)Z

    move-result v5

    invoke-virtual {v4, p1, v0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-direct {p0, v1, v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->shouldShowPremiumMenuItem(IZ)Z

    move-result v5

    if-nez v5, :cond_1

    move v2, v3

    :cond_1
    invoke-interface {v4, p1, v0, v2}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    return v3
.end method

.method public onCursorChanged(Lcom/google/android/youtube/videos/ui/CursorHelper;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    const/4 v9, 0x7

    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveFirstCursors(I)Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_3

    move v3, v5

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    if-ne p1, v7, :cond_5

    const/4 v0, 0x4

    if-eqz v2, :cond_4

    invoke-virtual {p0, v8}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v7

    if-nez v7, :cond_4

    move v4, v5

    :goto_1
    iget v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithFirstCursor:I

    or-int/2addr v5, v0

    iput v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithFirstCursor:I

    iget v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    and-int/2addr v5, v0

    if-eqz v3, :cond_0

    move v6, v0

    :cond_0
    if-eq v5, v6, :cond_1

    iget v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    xor-int/2addr v5, v0

    iput v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->initVerticals()V

    :cond_1
    if-nez v2, :cond_2

    invoke-direct {p0, v9}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveFirstCursors(I)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->initVerticals()V

    :cond_2
    return-void

    :cond_3
    move v3, v6

    goto :goto_0

    :cond_4
    move v4, v6

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    if-ne p1, v7, :cond_7

    const/4 v0, 0x2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveContent(I)Z

    move-result v7

    if-nez v7, :cond_6

    move v4, v5

    :goto_2
    goto :goto_1

    :cond_6
    move v4, v6

    goto :goto_2

    :cond_7
    const/4 v0, 0x1

    if-eqz v2, :cond_8

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/activity/HomeActivity;->haveContent(I)Z

    move-result v7

    if-nez v7, :cond_8

    move v4, v5

    :goto_3
    goto :goto_1

    :cond_8
    move v4, v6

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->destroy()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-static {p1, p0, v2}, Lcom/google/android/youtube/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;Lcom/google/android/youtube/videos/logging/EventLogger;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->startShop()V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->onRetry()V

    :goto_1
    const v2, 0x7f0a0049

    invoke-static {p0, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2, v4, v3, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->init(Ljava/lang/String;ZZ)V

    goto :goto_1

    :pswitch_3
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v3, "switchAccount"

    invoke-interface {v2, v3}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    invoke-direct {p0, v4, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->init(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07009f
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1    # I
    .param p2    # F
    .param p3    # I

    return-void
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->supportInvalidateOptionsMenu()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Lvedroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->onPageSelected(I)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->premiumErrorMessage:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    move v1, v2

    :goto_0
    const v3, 0x7f0700a0

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v3, 0x7f07009f

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "selected_vertical"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "selected_vertical"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->parseExternalVerticalName(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    iput v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->pendingVertical:I

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getPremiumErrorMessage()Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->premiumErrorMessage:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->premiumErrorMessage:Ljava/lang/CharSequence;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->premiumErrorMessage:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/SyncHelper;->initForError(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v4, "authAccount"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v4, "selected_vertical"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->setIntent(Landroid/content/Intent;)V

    return-void

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "authAccount"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->init(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onRetry()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->init(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "types_with_content"

    iget v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->typesWithContent:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "vertical"

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->register()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesEstCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->moviesVodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->showsCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;->onStop()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sync state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for account "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->initVerticals()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->finish()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->maybePrepareAccountForPlusOne()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method selectPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Lvedroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->viewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onVerticalOpened(I)V

    return-void
.end method

.method public final startShop()V
    .locals 6

    const/4 v3, 0x1

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v1

    if-ne v1, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForMoviesActivity(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    if-ne v1, v3, :cond_2

    move v2, v3

    :goto_1
    const-string v5, "actionBar"

    invoke-interface {v4, v2, v5, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForVertical(ZLjava/lang/String;Z)V

    if-nez v0, :cond_0

    const v2, 0x7f0a0078

    invoke-static {p0, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForShowsActivity(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public supportInvalidateOptionsMenu()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getCurrentVertical()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->homeActivityCompat:Lcom/google/android/youtube/videos/activity/HomeActivityCompat;

    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->shouldShowPremiumMenuItem(IZ)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;->onOptionsMenuInvalidated(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/HomeActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->shouldShowPremiumMenuItem(IZ)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    invoke-interface {v3, v1}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->setForceHidden(Z)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method
