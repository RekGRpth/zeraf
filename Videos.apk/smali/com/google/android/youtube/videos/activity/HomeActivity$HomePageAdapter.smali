.class Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;
.super Lvedroid/support/v4/app/FragmentPagerAdapter;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomePageAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/HomeActivity;

.field private verticals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/HomeActivity;Lvedroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p2    # Lvedroid/support/v4/app/FragmentManager;

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-direct {p0, p2}, Lvedroid/support/v4/app/FragmentPagerAdapter;-><init>(Lvedroid/support/v4/app/FragmentManager;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lvedroid/support/v4/app/Fragment;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected vertical: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    new-instance v1, Lcom/google/android/youtube/videos/activity/MoviesFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/activity/MoviesFragment;-><init>()V

    :goto_0
    return-object v1

    :pswitch_2
    new-instance v1, Lcom/google/android/youtube/videos/activity/ShowsFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/activity/ShowsFragment;-><init>()V

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;-><init>()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->getVertical(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 4
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/youtube/videos/activity/MoviesFragment;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    :goto_1
    return v1

    :cond_0
    instance-of v1, p1, Lcom/google/android/youtube/videos/activity/ShowsFragment;

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    instance-of v1, p1, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected vertical: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/youtube/videos/activity/HomeActivity;

    const v2, 0x7f0a000d

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/youtube/videos/activity/HomeActivity;

    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->this$0:Lcom/google/android/youtube/videos/activity/HomeActivity;

    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getVertical(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public setVerticals(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->verticals:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/HomeActivity$HomePageAdapter;->notifyDataSetChanged()V

    return-void
.end method
