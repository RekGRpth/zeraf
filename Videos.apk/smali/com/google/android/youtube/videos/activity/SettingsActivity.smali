.class public Lcom/google/android/youtube/videos/activity/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private settingsActivityCompat:Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method configureSettings(Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;

    const/4 v9, 0x0

    invoke-interface {p1}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v7

    const-string v8, "youtube"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const v7, 0x7f060001

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->addPreferencesFromResource(I)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v3

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->mobileStreamingEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    if-nez v3, :cond_1

    :cond_0
    const-string v7, "warning_streaming_bandwidth"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->mobileDownloadsEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    if-nez v3, :cond_3

    :cond_2
    const-string v7, "download_policy"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-static {v7}, Lcom/google/android/youtube/core/utils/GservicesUtil;->isLowEndDevice(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "default_hq"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v7, "default_hq"

    invoke-interface {v5, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "default_hq"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_4
    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->surroundSoundFormats()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "enable_surround_sound"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    const-string v7, "enable_info_cards"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getKnowledgeClient()Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-result-object v7

    if-nez v7, :cond_6

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_0
    const-string v7, "gservices_id"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->gservicesId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v7, "version"

    invoke-interface {p1, v7}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/VideosApplication;->getApplicationVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_6
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;->setIntentExtras(Landroid/content/Intent;)V

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-static {p0}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;->create(Lcom/google/android/youtube/videos/activity/SettingsActivity;)Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->settingsActivityCompat:Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->settingsActivityCompat:Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onCardsEnabledChanged(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onSettingsPageOpened()V

    return-void
.end method
