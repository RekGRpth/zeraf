.class public Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuggestionRequest"
.end annotation


# instance fields
.field public final country:Ljava/lang/String;

.field public final limit:I

.field public final locale:Ljava/util/Locale;

.field public final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/util/Locale;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->query:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->limit:I

    iput-object p3, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->country:Ljava/lang/String;

    return-void
.end method
