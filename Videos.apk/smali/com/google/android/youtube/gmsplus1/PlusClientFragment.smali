.class public Lcom/google/android/youtube/gmsplus1/PlusClientFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "PlusClientFragment.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# instance fields
.field private plusClient:Lcom/google/android/gms/plus/PlusClient;

.field private shouldConnect:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getPlusClient()Lcom/google/android/gms/plus/PlusClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    return-object v0
.end method

.method public initializePlusClient(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
    .param p2    # Ljava/lang/String;

    const-string v0, "gmsPlusOneClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "accountName cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->isAdded()Z

    move-result v0

    const-string v1, "this fragment must be attached to an activity"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->resetPlusClient()V

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1, v0, p2, p0, p0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->createPlusClient(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->shouldConnect:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    :cond_0
    return-void
.end method

.method public onConnected()V
    .locals 0

    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method public onDetach()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->resetPlusClient()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDetach()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->shouldConnect:Z

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->shouldConnect:Z

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method

.method public resetPlusClient()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->shouldConnect:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    :cond_1
    return-void
.end method
