.class Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;
.super Landroid/os/AsyncTask;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTokenTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Landroid/accounts/Account;

.field private final activity:Lvedroid/support/v4/app/FragmentActivity;

.field private final authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

.field private errorCodeForDialog:I

.field private exception:Ljava/lang/Exception;

.field private intentForStartActivity:Landroid/content/Intent;

.field private final retainInstance:Z

.field final synthetic this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 6
    .param p2    # Landroid/accounts/Account;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;-><init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V
    .locals 1
    .param p2    # Lvedroid/support/v4/app/FragmentActivity;
    .param p3    # Landroid/accounts/Account;
    .param p4    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
    .param p5    # Z

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    iput-object p2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iput-object p3, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iput-boolean p5, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->retainInstance:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
    .locals 1
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 5
    .param p1    # [Ljava/lang/Void;

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->access$100(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    iget-object v4, v4, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v4, v4, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;->getConnectionStatusCode()I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_0

    :catch_2
    move-exception v1

    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    goto :goto_0
.end method

.method public onActivityResult(ILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-boolean v4, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->retainInstance:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->getUserAuth(Landroid/accounts/Account;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    new-instance v1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    iget-object v3, v3, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/youtube/videos/accounts/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    # invokes: Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->saveTask(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V
    invoke-static {v0, p0}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->access$200(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->errorCodeForDialog:I

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->show(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->this$0:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    # invokes: Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->saveTask(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V
    invoke-static {v0, p0}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->access$200(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->intentForStartActivity:Landroid/content/Intent;

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetToken error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->exception:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    const-string v0, "GetToken error: could not get token and no exception"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V

    goto :goto_0
.end method
