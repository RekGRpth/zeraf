.class public Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GmsDialogFragment"
.end annotation


# instance fields
.field private dialog:Landroid/app/Dialog;

.field private gmsAccountManagerWrapper:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static show(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;I)V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;

    invoke-direct {v0}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;-><init>()V

    iput-object p0, v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->gmsAccountManagerWrapper:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    const/16 v1, 0x388

    invoke-static {p2, p1, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-class v2, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->gmsAccountManagerWrapper:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x388

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;->dialog:Landroid/app/Dialog;

    return-object v0
.end method
