.class Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;
.super Ljava/lang/Object;
.source "GmsPlusOneClient.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountStatusCollector"
.end annotation


# instance fields
.field private final plusClient:Lcom/google/android/gms/plus/PlusClient;

.field final synthetic this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Lcom/google/android/gms/plus/PlusClient;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient;

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->access$200(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method
