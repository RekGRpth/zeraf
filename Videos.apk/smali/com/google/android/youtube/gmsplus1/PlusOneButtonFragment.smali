.class public Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;
.super Lcom/google/android/youtube/gmsplus1/PlusClientFragment;
.source "PlusOneButtonFragment.java"


# instance fields
.field private button:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/PlusClientFragment;-><init>()V

    return-void
.end method

.method private initializeButton(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    check-cast v0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->getPlusClient()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    check-cast v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->getPlusClient()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot initialize button, unknown class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public initialize(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v0, "url cannot be empty"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Fragment view is not yet created"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->initializePlusClient(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->isPlusOneEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p3}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->initializeButton(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Lvedroid/support/v4/app/FragmentTransaction;->show(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Lvedroid/support/v4/app/FragmentTransaction;->hide(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f04002b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    return-object v0
.end method

.method public reset()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->resetPlusClient()V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->button:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Lvedroid/support/v4/app/FragmentTransaction;->hide(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method
