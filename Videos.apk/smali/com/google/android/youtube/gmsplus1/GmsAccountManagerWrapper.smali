.class public Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;
.super Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
.source "GmsAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GmsDialogFragment;,
        Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;
    }
.end annotation


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private savedTask:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    const-string v0, "applicationContext cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;
    .param p1    # Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->saveTask(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V

    return-void
.end method

.method private saveTask(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->authenticatee:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->access$000(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected blockingGetUserAuthInternal(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 6
    .param p1    # Landroid/accounts/Account;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v5, v5, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/youtube/videos/accounts/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v3, "Cannot get user auth; network error"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "Cannot get user auth"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    const-string v3, "got null authToken for the selected account"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getUserAuth(Landroid/accounts/Account;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lvedroid/support/v4/app/FragmentActivity;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
    .param p4    # Z

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "activity cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;-><init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected getUserAuthInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const/4 v5, 0x0

    instance-of v0, p2, Lvedroid/support/v4/app/FragmentActivity;

    const-string v1, "activity must be a FragmentActivity"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    move-object v2, p2

    check-cast v2, Lvedroid/support/v4/app/FragmentActivity;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;-><init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;Z)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public invalidateToken(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->applicationContext:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/Intent;

    const/4 v1, 0x0

    const/16 v2, 0x388

    if-eq p2, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;->savedTask:Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->onActivityResult(ILandroid/content/Intent;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected peekUserAuthInternal(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    new-instance v0, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;-><init>(Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper;Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/gmsplus1/GmsAccountManagerWrapper$GetTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
