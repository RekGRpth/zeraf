.class public Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
.super Ljava/lang/Object;
.source "AtHomeVideoInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private currentPositionMillis:I

.field private durationMillis:I

.field private title:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iget-object v1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->title:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->currentPositionMillis:I

    iget v4, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->durationMillis:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/youtube/athome/common/AtHomeVideoInfo$1;)V

    return-object v0
.end method

.method public currentPositionMillis(I)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->currentPositionMillis:I

    return-object p0
.end method

.method public durationMillis(I)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->durationMillis:I

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;->videoId:Ljava/lang/String;

    return-object p0
.end method
