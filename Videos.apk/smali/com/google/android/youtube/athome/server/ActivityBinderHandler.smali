.class public interface abstract Lcom/google/android/youtube/athome/server/ActivityBinderHandler;
.super Ljava/lang/Object;
.source "ActivityBinderHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/youtube/athome/server/ActivityBinder;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract bindActivity(Landroid/content/Intent;Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract bindCurrentActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract setBinderActivity(Lcom/google/android/youtube/athome/server/BinderActivity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/BinderActivity",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract unbindActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation
.end method
