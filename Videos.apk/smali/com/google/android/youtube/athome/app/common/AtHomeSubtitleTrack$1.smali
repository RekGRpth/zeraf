.class final Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;
.super Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;
.source "AtHomeSubtitleTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/athome/common/SafeFlattenableCreator",
        "<",
        "Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Landroid/support/place/rpc/Flattenable;
    .locals 1
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;->createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-result-object v0

    return-object v0
.end method

.method protected createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
    .locals 2
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    new-instance v0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v0

    const-string v1, "languageCode"

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->languageCode(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v0

    const-string v1, "trackName"

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->trackName(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->build()Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-result-object v0

    return-object v0
.end method
