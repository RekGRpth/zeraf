.class public Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
.super Ljava/lang/Object;
.source "AtHomeSubtitleTrack.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;
    }
.end annotation


# static fields
.field public static RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<",
            "Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final languageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;

    invoke-direct {v0}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->videoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->trackName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "videoId"

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "languageCode"

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "trackName"

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
