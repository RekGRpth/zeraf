.class Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;
.super Ljava/lang/Object;
.source "PlusOneButtonWithPopupContentView.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/PlusOne;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mTogglePending:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mTogglePending:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-object p2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->updateView()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->refreshButtonLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # setter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z
    invoke-static {v0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$202(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->showDisabled()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$2;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # setter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z
    invoke-static {v1, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$202(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z

    throw v0
.end method
