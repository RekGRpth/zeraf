.class public final Lcom/google/protos/userfeedback/CommonData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CommonData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/userfeedback/CommonData$1;,
        Lcom/google/protos/userfeedback/CommonData$Builder;,
        Lcom/google/protos/userfeedback/CommonData$ReportType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/protos/userfeedback/CommonData;


# instance fields
.field private additionalFormContent_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificData;",
            ">;"
        }
    .end annotation
.end field

.field private descriptionTranslated_:Ljava/lang/String;

.field private description_:Ljava/lang/String;

.field private gaiaId_:J

.field private hasDescription:Z

.field private hasDescriptionTranslated:Z

.field private hasGaiaId:Z

.field private hasProductSpecificContext:Z

.field private hasProductVersion:Z

.field private hasReportType:Z

.field private hasSourceDescriptionLanguage:Z

.field private hasSpellingErrorCount:Z

.field private hasUiLanguage:Z

.field private hasUserEmail:Z

.field private memoizedSerializedSize:I

.field private productSpecificBinaryDataName_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private productSpecificBinaryData_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificBinaryData;",
            ">;"
        }
    .end annotation
.end field

.field private productSpecificContext_:Ljava/lang/String;

.field private productSpecificData_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificData;",
            ">;"
        }
    .end annotation
.end field

.field private productVersion_:Ljava/lang/String;

.field private reportType_:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field private sourceDescriptionLanguage_:Ljava/lang/String;

.field private spellingErrorCount_:I

.field private uiLanguage_:Ljava/lang/String;

.field private userEmail_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/userfeedback/CommonData;-><init>(Z)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData;->defaultInstance:Lcom/google/protos/userfeedback/CommonData;

    invoke-static {}, Lcom/google/protos/userfeedback/UserfeedbackCommon;->internalForceInit()V

    sget-object v0, Lcom/google/protos/userfeedback/CommonData;->defaultInstance:Lcom/google/protos/userfeedback/CommonData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/CommonData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/userfeedback/CommonData;->gaiaId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->descriptionTranslated_:Ljava/lang/String;

    iput v2, p0, Lcom/google/protos/userfeedback/CommonData;->spellingErrorCount_:I

    const-string v0, "en"

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->sourceDescriptionLanguage_:Ljava/lang/String;

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->uiLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->userEmail_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productVersion_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificContext_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;

    iput v2, p0, Lcom/google/protos/userfeedback/CommonData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/protos/userfeedback/CommonData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/userfeedback/CommonData$1;)V
    .locals 0
    .param p1    # Lcom/google/protos/userfeedback/CommonData$1;

    invoke-direct {p0}, Lcom/google/protos/userfeedback/CommonData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/userfeedback/CommonData;->gaiaId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->descriptionTranslated_:Ljava/lang/String;

    iput v2, p0, Lcom/google/protos/userfeedback/CommonData;->spellingErrorCount_:I

    const-string v0, "en"

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->sourceDescriptionLanguage_:Ljava/lang/String;

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->uiLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->userEmail_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productVersion_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificContext_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;

    iput v2, p0, Lcom/google/protos/userfeedback/CommonData;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->description_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->descriptionTranslated_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/protos/userfeedback/CommonData;I)I
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # I

    iput p1, p0, Lcom/google/protos/userfeedback/CommonData;->spellingErrorCount_:I

    return p1
.end method

.method static synthetic access$1502(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->sourceDescriptionLanguage_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->uiLanguage_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasUserEmail:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->userEmail_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasProductVersion:Z

    return p1
.end method

.method static synthetic access$2202(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->productVersion_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificContext_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasReportType:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/protos/userfeedback/CommonData;Lcom/google/protos/userfeedback/CommonData$ReportType;)Lcom/google/protos/userfeedback/CommonData$ReportType;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Lcom/google/protos/userfeedback/CommonData$ReportType;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->reportType_:Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/CommonData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/CommonData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/CommonData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/CommonData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasGaiaId:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/protos/userfeedback/CommonData;J)J
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/protos/userfeedback/CommonData;->gaiaId_:J

    return-wide p1
.end method

.method static synthetic access$902(Lcom/google/protos/userfeedback/CommonData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/CommonData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/CommonData;->hasDescription:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/userfeedback/CommonData;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/CommonData;->defaultInstance:Lcom/google/protos/userfeedback/CommonData;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->WEB_FEEDBACK:Lcom/google/protos/userfeedback/CommonData$ReportType;

    iput-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->reportType_:Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-void
.end method

.method public static newBuilder()Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 1

    # invokes: Lcom/google/protos/userfeedback/CommonData$Builder;->create()Lcom/google/protos/userfeedback/CommonData$Builder;
    invoke-static {}, Lcom/google/protos/userfeedback/CommonData$Builder;->access$100()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/CommonData;

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->newBuilder()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAdditionalFormContentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionTranslated()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->descriptionTranslated_:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/userfeedback/CommonData;->gaiaId_:J

    return-wide v0
.end method

.method public getProductSpecificBinaryDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificBinaryData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;

    return-object v0
.end method

.method public getProductSpecificBinaryDataNameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;

    return-object v0
.end method

.method public getProductSpecificContext()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificContext_:Ljava/lang/String;

    return-object v0
.end method

.method public getProductSpecificDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;

    return-object v0
.end method

.method public getProductVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->productVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getReportType()Lcom/google/protos/userfeedback/CommonData$ReportType;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->reportType_:Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    iget v3, p0, Lcom/google/protos/userfeedback/CommonData;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasGaiaId()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getGaiaId()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeFixed64Size(IJ)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasDescription()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasUserEmail()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getUserEmail()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getDescriptionTranslated()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getSourceDescriptionLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getUiLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataNameList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_7
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataNameList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/16 v5, 0x9

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasProductVersion()Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductVersion()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_9
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificDataList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/16 v5, 0xb

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext()Z

    move-result v5

    if-eqz v5, :cond_b

    const/16 v5, 0xf

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificContext()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_b
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount()Z

    move-result v5

    if-eqz v5, :cond_c

    const/16 v5, 0x11

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getSpellingErrorCount()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_c
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getAdditionalFormContentList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/16 v5, 0x13

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_4

    :cond_d
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasReportType()Z

    move-result v5

    if-eqz v5, :cond_e

    const/16 v5, 0x15

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getReportType()Lcom/google/protos/userfeedback/CommonData$ReportType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/protos/userfeedback/CommonData$ReportType;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_e
    iput v3, p0, Lcom/google/protos/userfeedback/CommonData;->memoizedSerializedSize:I

    move v4, v3

    goto/16 :goto_0
.end method

.method public getSourceDescriptionLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->sourceDescriptionLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpellingErrorCount()I
    .locals 1

    iget v0, p0, Lcom/google/protos/userfeedback/CommonData;->spellingErrorCount_:I

    return v0
.end method

.method public getUiLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->uiLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData;->userEmail_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasDescription:Z

    return v0
.end method

.method public hasDescriptionTranslated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated:Z

    return v0
.end method

.method public hasGaiaId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasGaiaId:Z

    return v0
.end method

.method public hasProductSpecificContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext:Z

    return v0
.end method

.method public hasProductVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasProductVersion:Z

    return v0
.end method

.method public hasReportType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasReportType:Z

    return v0
.end method

.method public hasSourceDescriptionLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage:Z

    return v0
.end method

.method public hasSpellingErrorCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount:Z

    return v0
.end method

.method public hasUiLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage:Z

    return v0
.end method

.method public hasUserEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/CommonData;->hasUserEmail:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificDataList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getAdditionalFormContentList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasGaiaId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getGaiaId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFixed64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasUserEmail()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getUserEmail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getDescriptionTranslated()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getSourceDescriptionLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getUiLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataNameList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasProductVersion()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_9
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificContext()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getSpellingErrorCount()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getAdditionalFormContentList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->hasReportType()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData;->getReportType()Lcom/google/protos/userfeedback/CommonData$ReportType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protos/userfeedback/CommonData$ReportType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_d
    return-void
.end method
