.class public final Lcom/google/protos/userfeedback/ExtensionSubmit;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ExtensionSubmit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/userfeedback/ExtensionSubmit$1;,
        Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/protos/userfeedback/ExtensionSubmit;


# instance fields
.field private bucket_:Ljava/lang/String;

.field private commonData_:Lcom/google/protos/userfeedback/CommonData;

.field private hasBucket:Z

.field private hasCommonData:Z

.field private hasProductId:Z

.field private hasTypeId:Z

.field private hasWebData:Z

.field private memoizedSerializedSize:I

.field private productId_:I

.field private productSpecificBinaryData_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificBinaryData;",
            ">;"
        }
    .end annotation
.end field

.field private typeId_:I

.field private webData_:Lcom/google/protos/userfeedback/WebData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;-><init>(Z)V

    sput-object v0, Lcom/google/protos/userfeedback/ExtensionSubmit;->defaultInstance:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-static {}, Lcom/google/protos/userfeedback/UserfeedbackExtension;->internalForceInit()V

    sget-object v0, Lcom/google/protos/userfeedback/ExtensionSubmit;->defaultInstance:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->typeId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;

    iput v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->bucket_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/userfeedback/ExtensionSubmit$1;)V
    .locals 0
    .param p1    # Lcom/google/protos/userfeedback/ExtensionSubmit$1;

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->typeId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;

    iput v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->bucket_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/protos/userfeedback/ExtensionSubmit;I)I
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # I

    iput p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productId_:I

    return p1
.end method

.method static synthetic access$1202(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->bucket_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/CommonData;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Lcom/google/protos/userfeedback/CommonData;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/WebData;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Lcom/google/protos/userfeedback/WebData;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/protos/userfeedback/ExtensionSubmit;I)I
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ExtensionSubmit;
    .param p1    # I

    iput p1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->typeId_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/userfeedback/ExtensionSubmit;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/ExtensionSubmit;->defaultInstance:Lcom/google/protos/userfeedback/ExtensionSubmit;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->getDefaultInstance()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;

    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->getDefaultInstance()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;

    return-void
.end method

.method public static newBuilder()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 1

    # invokes: Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->create()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    invoke-static {}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->access$100()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBucket()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->bucket_:Ljava/lang/String;

    return-object v0
.end method

.method public getCommonData()Lcom/google/protos/userfeedback/CommonData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;

    return-object v0
.end method

.method public getProductId()I
    .locals 1

    iget v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productId_:I

    return v0
.end method

.method public getProductSpecificBinaryDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/protos/userfeedback/ProductSpecificBinaryData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getWebData()Lcom/google/protos/userfeedback/WebData;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getTypeId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/16 v4, 0xf

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId()Z

    move-result v4

    if-eqz v4, :cond_5

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket()Z

    move-result v4

    if-eqz v4, :cond_6

    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getBucket()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    iput v2, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getTypeId()I
    .locals 1

    iget v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->typeId_:I

    return v0
.end method

.method public getWebData()Lcom/google/protos/userfeedback/WebData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;

    return-object v0
.end method

.method public hasBucket()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket:Z

    return v0
.end method

.method public hasCommonData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z

    return v0
.end method

.method public hasProductId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId:Z

    return v0
.end method

.method public hasTypeId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId:Z

    return v0
.end method

.method public hasWebData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protos/userfeedback/CommonData;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getWebData()Lcom/google/protos/userfeedback/WebData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getTypeId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductSpecificBinaryDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getBucket()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_5
    return-void
.end method
