.class public final enum Lcom/google/protos/userfeedback/CommonData$ReportType;
.super Ljava/lang/Enum;
.source "CommonData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/CommonData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReportType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/protos/userfeedback/CommonData$ReportType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_ANR_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_ANR_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_BATTERY_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_BATTERY_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_CRASH_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_RUNNING_SERVICES_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_RUNNING_SERVICES_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum ANDROID_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum CHROME:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum CHROME_OS:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum FEEDBACK_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum IE_TOOLBAR:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum IMPORT_DATA:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum IOS_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum IOS_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field public static final enum WEB_FEEDBACK:Lcom/google/protos/userfeedback/CommonData$ReportType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/protos/userfeedback/CommonData$ReportType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "WEB_FEEDBACK"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->WEB_FEEDBACK:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "IE_TOOLBAR"

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IE_TOOLBAR:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "CHROME"

    invoke-direct {v0, v1, v6, v6, v7}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "CHROME_OS"

    invoke-direct {v0, v1, v7, v7, v8}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME_OS:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_CRASH_EXTERNAL"

    invoke-direct {v0, v1, v8, v8, v9}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_BATTERY_EXTERNAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_ANR_EXTERNAL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_RUNNING_SERVICES_EXTERNAL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_CRASH_INTERNAL"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_BATTERY_INTERNAL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_ANR_INTERNAL"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_RUNNING_SERVICES_INTERNAL"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "ANDROID_USER_INITIATED_INTERNAL"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "IOS_CRASH_INTERNAL"

    const/16 v2, 0xd

    const/16 v3, 0xd

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "IOS_USER_INITIATED_INTERNAL"

    const/16 v2, 0xe

    const/16 v3, 0xe

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "FEEDBACK_EXTERNAL"

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->FEEDBACK_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    const-string v1, "IMPORT_DATA"

    const/16 v2, 0x10

    const/16 v3, 0x10

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protos/userfeedback/CommonData$ReportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IMPORT_DATA:Lcom/google/protos/userfeedback/CommonData$ReportType;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/google/protos/userfeedback/CommonData$ReportType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->WEB_FEEDBACK:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/protos/userfeedback/CommonData$ReportType;->IE_TOOLBAR:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME_OS:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->FEEDBACK_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/protos/userfeedback/CommonData$ReportType;->IMPORT_DATA:Lcom/google/protos/userfeedback/CommonData$ReportType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->$VALUES:[Lcom/google/protos/userfeedback/CommonData$ReportType;

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$ReportType$1;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/CommonData$ReportType$1;-><init>()V

    sput-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/userfeedback/CommonData$ReportType;->index:I

    iput p4, p0, Lcom/google/protos/userfeedback/CommonData$ReportType;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/protos/userfeedback/CommonData$ReportType;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->WEB_FEEDBACK:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IE_TOOLBAR:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->CHROME_OS:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_BATTERY_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_ANR_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_RUNNING_SERVICES_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_c
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->ANDROID_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_d
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_CRASH_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_e
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IOS_USER_INITIATED_INTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_f
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->FEEDBACK_EXTERNAL:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_10
    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->IMPORT_DATA:Lcom/google/protos/userfeedback/CommonData$ReportType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$ReportType;
    .locals 1

    const-class v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-object v0
.end method

.method public static values()[Lcom/google/protos/userfeedback/CommonData$ReportType;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/CommonData$ReportType;->$VALUES:[Lcom/google/protos/userfeedback/CommonData$ReportType;

    invoke-virtual {v0}, [Lcom/google/protos/userfeedback/CommonData$ReportType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/protos/userfeedback/CommonData$ReportType;->value:I

    return v0
.end method
