.class public final Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ExtensionSubmit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/ExtensionSubmit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/userfeedback/ExtensionSubmit;",
        "Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/protos/userfeedback/ExtensionSubmit;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->create()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;-><init>()V

    new-instance v1, Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit;-><init>(Lcom/google/protos/userfeedback/ExtensionSubmit$1;)V

    iput-object v1, v0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    return-object v0
.end method


# virtual methods
.method public addProductSpecificBinaryData(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->build()Lcom/google/protos/userfeedback/ExtensionSubmit;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/userfeedback/ExtensionSubmit;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->buildPartial()Lcom/google/protos/userfeedback/ExtensionSubmit;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/userfeedback/ExtensionSubmit;
    .locals 3

    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v2, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->clone()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->clone()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->clone()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->create()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->mergeFrom(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->clone()Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCommonData()Lcom/google/protos/userfeedback/CommonData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    return-object v0
.end method

.method public getWebData()Lcom/google/protos/userfeedback/WebData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getWebData()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    return-object v0
.end method

.method public hasCommonData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData()Z

    move-result v0

    return v0
.end method

.method public hasWebData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeCommonData(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/CommonData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$500(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->getDefaultInstance()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$500(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/CommonData;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protos/userfeedback/CommonData;->newBuilder(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/CommonData$Builder;->buildPartial()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$502(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData;

    :goto_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$402(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$502(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->newBuilder()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->hasCommonData()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->buildPartial()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setCommonData(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->newBuilder()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->hasWebData()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->getWebData()Lcom/google/protos/userfeedback/WebData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/WebData$Builder;->buildPartial()Lcom/google/protos/userfeedback/WebData;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setWebData(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setTypeId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->addProductSpecificBinaryData(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setProductId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setBucket(Ljava/lang/String;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x7a -> :sswitch_4
        0x88 -> :sswitch_5
        0x92 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-static {}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getDefaultInstance()Lcom/google/protos/userfeedback/ExtensionSubmit;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getCommonData()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->mergeCommonData(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getWebData()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->mergeWebData(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getTypeId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setTypeId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    :cond_4
    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$300(Lcom/google/protos/userfeedback/ExtensionSubmit;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getProductId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setProductId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->getBucket()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->setBucket(Ljava/lang/String;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;

    goto :goto_0
.end method

.method public mergeWebData(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/WebData;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v0}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$700(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->getDefaultInstance()Lcom/google/protos/userfeedback/WebData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    iget-object v1, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # getter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$700(Lcom/google/protos/userfeedback/ExtensionSubmit;)Lcom/google/protos/userfeedback/WebData;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protos/userfeedback/WebData;->newBuilder(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/WebData$Builder;->buildPartial()Lcom/google/protos/userfeedback/WebData;

    move-result-object v1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$702(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData;

    :goto_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$602(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$702(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData;

    goto :goto_0
.end method

.method public setBucket(Ljava/lang/String;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasBucket:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$1202(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->bucket_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$1302(Lcom/google/protos/userfeedback/ExtensionSubmit;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setCommonData(Lcom/google/protos/userfeedback/CommonData$Builder;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/CommonData$Builder;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$402(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData$Builder;->build()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$502(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData;

    return-object p0
.end method

.method public setCommonData(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/CommonData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasCommonData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$402(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->commonData_:Lcom/google/protos/userfeedback/CommonData;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$502(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData;

    return-object p0
.end method

.method public setProductId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasProductId:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$1002(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->productId_:I
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$1102(Lcom/google/protos/userfeedback/ExtensionSubmit;I)I

    return-object p0
.end method

.method public setTypeId(I)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasTypeId:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$802(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->typeId_:I
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$902(Lcom/google/protos/userfeedback/ExtensionSubmit;I)I

    return-object p0
.end method

.method public setWebData(Lcom/google/protos/userfeedback/WebData$Builder;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/WebData$Builder;

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$602(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/WebData$Builder;->build()Lcom/google/protos/userfeedback/WebData;

    move-result-object v1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$702(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData;

    return-object p0
.end method

.method public setWebData(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/WebData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->hasWebData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$602(Lcom/google/protos/userfeedback/ExtensionSubmit;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ExtensionSubmit$Builder;->result:Lcom/google/protos/userfeedback/ExtensionSubmit;

    # setter for: Lcom/google/protos/userfeedback/ExtensionSubmit;->webData_:Lcom/google/protos/userfeedback/WebData;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ExtensionSubmit;->access$702(Lcom/google/protos/userfeedback/ExtensionSubmit;Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData;

    return-object p0
.end method
