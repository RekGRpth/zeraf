.class public final Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ProductSpecificData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/ProductSpecificData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/userfeedback/ProductSpecificData;",
        "Lcom/google/protos/userfeedback/ProductSpecificData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/protos/userfeedback/ProductSpecificData;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;-><init>()V

    new-instance v1, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protos/userfeedback/ProductSpecificData;-><init>(Lcom/google/protos/userfeedback/ProductSpecificData$1;)V

    iput-object v1, v0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->build()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/userfeedback/ProductSpecificData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-static {v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/userfeedback/ProductSpecificData;
    .locals 3

    iget-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-virtual {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Type;->valueOf(I)Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setType(Lcom/google/protos/userfeedback/ProductSpecificData$Type;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 1
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->getDefaultInstance()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->getType()Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->setType(Lcom/google/protos/userfeedback/ProductSpecificData$Type;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    goto :goto_0
.end method

.method public setKey(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$302(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->key_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$402(Lcom/google/protos/userfeedback/ProductSpecificData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setType(Lcom/google/protos/userfeedback/ProductSpecificData$Type;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$702(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->type_:Lcom/google/protos/userfeedback/ProductSpecificData$Type;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$802(Lcom/google/protos/userfeedback/ProductSpecificData;Lcom/google/protos/userfeedback/ProductSpecificData$Type;)Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$502(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificData;->value_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificData;->access$602(Lcom/google/protos/userfeedback/ProductSpecificData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
