.class public final Lcom/google/protos/userfeedback/CommonData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CommonData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/CommonData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/userfeedback/CommonData;",
        "Lcom/google/protos/userfeedback/CommonData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/protos/userfeedback/CommonData;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData$Builder;->create()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/userfeedback/CommonData$Builder;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/CommonData$Builder;-><init>()V

    new-instance v1, Lcom/google/protos/userfeedback/CommonData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protos/userfeedback/CommonData;-><init>(Lcom/google/protos/userfeedback/CommonData$1;)V

    iput-object v1, v0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    return-object v0
.end method


# virtual methods
.method public addAdditionalFormContent(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$602(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addProductSpecificBinaryData(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$402(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addProductSpecificBinaryDataName(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$302(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData$Builder;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$502(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->build()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$502(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->build()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/userfeedback/CommonData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->buildPartial()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/userfeedback/CommonData;
    .locals 3

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    iget-object v2, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/protos/userfeedback/CommonData;->access$302(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    iget-object v2, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/protos/userfeedback/CommonData;->access$402(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    iget-object v2, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/protos/userfeedback/CommonData;->access$502(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    iget-object v2, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/protos/userfeedback/CommonData;->access$602(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->clone()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->clone()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->clone()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData$Builder;->create()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    invoke-virtual {v0, v1}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/CommonData$Builder;->clone()Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/CommonData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/CommonData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/CommonData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/protos/userfeedback/CommonData$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFixed64()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/google/protos/userfeedback/CommonData$Builder;->setGaiaId(J)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setUserEmail(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescriptionTranslated(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setSourceDescriptionLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setUiLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificBinaryDataName(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificBinaryData(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setProductVersion(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_a
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->addProductSpecificData(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setProductSpecificContext(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->setSpellingErrorCount(I)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto :goto_0

    :sswitch_d
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData;->newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificData;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/protos/userfeedback/CommonData$Builder;->addAdditionalFormContent(Lcom/google/protos/userfeedback/ProductSpecificData;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData$ReportType;->valueOf(I)Lcom/google/protos/userfeedback/CommonData$ReportType;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/protos/userfeedback/CommonData$Builder;->setReportType(Lcom/google/protos/userfeedback/CommonData$ReportType;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x7a -> :sswitch_b
        0x88 -> :sswitch_c
        0x9a -> :sswitch_d
        0xa8 -> :sswitch_e
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/userfeedback/CommonData;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/CommonData;

    invoke-static {}, Lcom/google/protos/userfeedback/CommonData;->getDefaultInstance()Lcom/google/protos/userfeedback/CommonData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasGaiaId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getGaiaId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/userfeedback/CommonData$Builder;->setGaiaId(J)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getDescriptionTranslated()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setDescriptionTranslated(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getSpellingErrorCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setSpellingErrorCount(I)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getSourceDescriptionLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setSourceDescriptionLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getUiLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setUiLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasUserEmail()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getUserEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setUserEmail(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_8
    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$302(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_9
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryDataName_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$300(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_a
    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$402(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_b
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificBinaryData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$400(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_c
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasProductVersion()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getProductVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setProductVersion(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_d
    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$502(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_e
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificData_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$500(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_f
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getProductSpecificContext()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setProductSpecificContext(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;

    :cond_10
    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$602(Lcom/google/protos/userfeedback/CommonData;Ljava/util/List;)Ljava/util/List;

    :cond_11
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/protos/userfeedback/CommonData;->additionalFormContent_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/protos/userfeedback/CommonData;->access$600(Lcom/google/protos/userfeedback/CommonData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_12
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->hasReportType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/CommonData;->getReportType()Lcom/google/protos/userfeedback/CommonData$ReportType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/CommonData$Builder;->setReportType(Lcom/google/protos/userfeedback/CommonData$ReportType;)Lcom/google/protos/userfeedback/CommonData$Builder;

    goto/16 :goto_0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasDescription:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$902(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->description_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$1002(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setDescriptionTranslated(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasDescriptionTranslated:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$1102(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->descriptionTranslated_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$1202(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setGaiaId(J)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasGaiaId:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$702(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->gaiaId_:J
    invoke-static {v0, p1, p2}, Lcom/google/protos/userfeedback/CommonData;->access$802(Lcom/google/protos/userfeedback/CommonData;J)J

    return-object p0
.end method

.method public setProductSpecificContext(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasProductSpecificContext:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$2302(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productSpecificContext_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$2402(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setProductVersion(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasProductVersion:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$2102(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->productVersion_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$2202(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setReportType(Lcom/google/protos/userfeedback/CommonData$ReportType;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Lcom/google/protos/userfeedback/CommonData$ReportType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasReportType:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$2502(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->reportType_:Lcom/google/protos/userfeedback/CommonData$ReportType;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$2602(Lcom/google/protos/userfeedback/CommonData;Lcom/google/protos/userfeedback/CommonData$ReportType;)Lcom/google/protos/userfeedback/CommonData$ReportType;

    return-object p0
.end method

.method public setSourceDescriptionLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasSourceDescriptionLanguage:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$1502(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->sourceDescriptionLanguage_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$1602(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setSpellingErrorCount(I)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasSpellingErrorCount:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$1302(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->spellingErrorCount_:I
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$1402(Lcom/google/protos/userfeedback/CommonData;I)I

    return-object p0
.end method

.method public setUiLanguage(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasUiLanguage:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$1702(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->uiLanguage_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$1802(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setUserEmail(Ljava/lang/String;)Lcom/google/protos/userfeedback/CommonData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/CommonData;->hasUserEmail:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/CommonData;->access$1902(Lcom/google/protos/userfeedback/CommonData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/CommonData$Builder;->result:Lcom/google/protos/userfeedback/CommonData;

    # setter for: Lcom/google/protos/userfeedback/CommonData;->userEmail_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/CommonData;->access$2002(Lcom/google/protos/userfeedback/CommonData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
