.class public Lcom/mediatek/vlw/VLWSeekBar;
.super Landroid/widget/SeekBar;
.source "VLWSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "VLWSeekBar"


# instance fields
.field private mAlpha:I

.field private mFromKey:Z

.field private mLeftThreshold:I

.field private mListener:Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

.field private mRightThreshold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mAlpha:I

    iput v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    iput-boolean v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mAlpha:I

    iput v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    iput-boolean v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0xff

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mAlpha:I

    iput v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    iput-boolean v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/AbsSeekBar;->drawableStateChanged()V

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-void
.end method

.method public fromKeyEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    return v0
.end method

.method public getLeftThreshold()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    return v0
.end method

.method public getOnTouchUpWithoutHandledListener()Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mListener:Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

    return-object v0
.end method

.method public getRightThreshold()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getKeyProgressIncrement()I

    move-result v1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/widget/AbsSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    sub-int/2addr v2, v1

    iget v3, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    if-lt v2, v3, :cond_0

    iget v3, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    if-gt v2, v3, :cond_0

    iput-boolean v4, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    invoke-super {p0, p1, p2}, Landroid/widget/AbsSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    add-int/2addr v2, v1

    iget v3, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    if-lt v2, v3, :cond_0

    iget v3, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    if-gt v2, v3, :cond_0

    iput-boolean v4, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    invoke-super {p0, p1, p2}, Landroid/widget/AbsSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v7, v4, v7

    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v1, v7, v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v5, v7

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    if-ge v5, v7, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v7

    int-to-float v7, v7

    mul-float v2, v3, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v7, 0x3

    if-ne v0, v7, :cond_3

    const-string v7, "VLWSeekBar"

    const-string v8, "MotionEvent.ACTION_CANCEL just return"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return v6

    :cond_1
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v7, v4, v7

    if-le v5, v7, :cond_2

    const/high16 v3, 0x3f800000

    goto :goto_0

    :cond_2
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v7, v5, v7

    int-to-float v7, v7

    int-to-float v8, v1

    div-float v3, v7, v8

    goto :goto_0

    :cond_3
    iget v7, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    int-to-float v7, v7

    cmpl-float v7, v2, v7

    if-ltz v7, :cond_4

    iget v7, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    int-to-float v7, v7

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_4

    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto :goto_1

    :cond_4
    if-ne v0, v6, :cond_0

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    iget-object v7, p0, Lcom/mediatek/vlw/VLWSeekBar;->mListener:Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/vlw/VLWSeekBar;->mListener:Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

    invoke-interface {v7, v2}, Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;->onTouchUp(F)V

    goto :goto_1
.end method

.method public resetFromKeyEvent()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vlw/VLWSeekBar;->mFromKey:Z

    return-void
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mAlpha:I

    return-void
.end method

.method public setLeftThreshold(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mLeftThreshold:I

    return-void
.end method

.method public setOnTouchUpWithoutHandledListener(Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mListener:Lcom/mediatek/vlw/VLWSeekBar$OnTouchUpWithoutHandledListener;

    return-void
.end method

.method public setRightThreshold(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VLWSeekBar;->mRightThreshold:I

    return-void
.end method
