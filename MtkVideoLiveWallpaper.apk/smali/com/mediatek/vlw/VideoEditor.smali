.class public Lcom/mediatek/vlw/VideoEditor;
.super Landroid/app/Activity;
.source "VideoEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vlw/VideoEditor$SelectVideo;
    }
.end annotation


# static fields
.field private static final ACTION_PICK_FOLDER:Ljava/lang/String; = "com.mediatek.action.PICK_VIDEO_FOLDER"

.field private static final ACTION_VIDEO_CAPTURE:Ljava/lang/String; = "android.media.action.VIDEO_CAPTURE"

.field private static final DEBUG:Z = true

.field private static final DEFAULT_MODE:I = 0x1

.field private static final DIALOG_SELECT_VIDEO:I = 0x1

.field private static final FILE:Ljava/lang/String; = "file"

.field private static final HTML_BOLD:Ljava/lang/String; = "<b>"

.field private static final LEFT_BRACKET:Ljava/lang/String; = "("

.field private static final PICK_CAMERA_REQUEST:I = 0x2

.field private static final PICK_FOLDER_REQUEST:I = 0x1

.field private static final PICK_VIDEO_REQUEST:I = 0x0

.field private static final RIGHT_BRACKET:Ljava/lang/String; = ")"

.field private static final SEPARATOR:Ljava/lang/String; = "/"

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x2

.field private static final STATE_PLAYBACK_COMPLETED:I = 0x4

.field private static final STATE_PLAYING:I = 0x1

.field private static final STATE_PREPARED:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoEditor"

.field private static final TYPE:Ljava/lang/String; = "video/*"

.field private static final VIDEO_LIVE_WALLPAPER_CLASS:Ljava/lang/String; = "com.mediatek.vlw.VideoLiveWallpaper"

.field private static final VIDEO_LIVE_WALLPAPER_PACKAGE:Ljava/lang/String; = "com.mediatek.vlw"


# instance fields
.field private mBucketId:Ljava/lang/String;

.field private final mCallback:Lcom/mediatek/vlw/VLWMediaController$Callback;

.field private mClosed:Z

.field private mCurrentPos:I

.field private mCurrentState:I

.field private mEndTime:I

.field private mFolderInfo:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mHavaUnmountedData:Z

.field private mIsOpening:Z

.field private mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

.field private mMediaController:Lcom/mediatek/vlw/VLWMediaController;

.field private mMode:I

.field private final mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private final mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPlayPause:Landroid/widget/ImageButton;

.field private mPlayer:Landroid/widget/VideoView;

.field private mPrevBucketId:Ljava/lang/String;

.field private mPrevCurrentPos:I

.field private mPrevEndTime:I

.field private mPrevStartTime:I

.field private mPrevUri:Landroid/net/Uri;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSetWallpaper:Landroid/widget/Button;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mStartTime:I

.field private mTargetState:I

.field private mUnMounted:Z

.field private mUri:Landroid/net/Uri;

.field private mUriInvalid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoTitle:Landroid/widget/TextView;

.field private mWallpaperIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    sget-object v0, Lcom/mediatek/vlw/Utils$LoopMode;->ALL:Lcom/mediatek/vlw/Utils$LoopMode;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$1;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoEditor$1;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCallback:Lcom/mediatek/vlw/VLWMediaController$Callback;

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$2;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoEditor$2;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$3;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoEditor$3;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$4;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoEditor$4;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$5;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoEditor$5;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/vlw/VideoEditor;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoEditor;->updatePausePlay(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/vlw/VideoEditor;ZZ)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZ)V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/vlw/VideoEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->startPlayback()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/vlw/VideoEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/vlw/VideoEditor;)Landroid/widget/VideoView;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/vlw/VideoEditor;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoEditor;->mIsOpening:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/vlw/VideoEditor;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoEditor;->mIsOpening:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/vlw/VideoEditor;)Lcom/mediatek/vlw/VLWMediaController;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/vlw/VideoEditor;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/mediatek/vlw/VideoEditor;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->play()V

    return-void
.end method

.method static synthetic access$1900(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    return v0
.end method

.method static synthetic access$1902(Lcom/mediatek/vlw/VideoEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/vlw/VideoEditor;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/vlw/VideoEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    return p1
.end method

.method static synthetic access$2100(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->stopPlayback()V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/vlw/VideoEditor;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/vlw/VideoEditor;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHavaUnmountedData:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/mediatek/vlw/VideoEditor;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoEditor;->mHavaUnmountedData:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/mediatek/vlw/VideoEditor;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevBucketId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->restoreMountedData()V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/vlw/VideoEditor;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUnMounted:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/vlw/VideoEditor;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoEditor;->mUnMounted:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->updateInfo()V

    return-void
.end method

.method static synthetic access$2800(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->saveUnmountedData()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/vlw/VideoEditor;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/vlw/VideoEditor;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mVideoTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/vlw/VideoEditor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/mediatek/vlw/VideoEditor;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/vlw/VideoEditor;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/vlw/VideoEditor;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/vlw/VideoEditor;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/vlw/VideoEditor;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$700(Lcom/mediatek/vlw/VideoEditor;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/vlw/VideoEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/vlw/VideoEditor;)Lcom/mediatek/vlw/Utils$LoopMode;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/vlw/VideoEditor;ZZZ)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoEditor;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZZ)V

    return-void
.end method

.method private checkThumbnailBitmap(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v4, p1, p2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    const/4 v2, 0x0

    if-nez v0, :cond_4

    const-string v6, "VideoEditor"

    const-string v7, "thumbnail bitmap == null"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    :cond_0
    :goto_1
    if-eqz v2, :cond_3

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    :cond_1
    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v6, "VideoEditor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thumbnail find unsuport video: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/mediatek/vlw/Utils;->getUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    iget v7, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    iget-object v8, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    invoke-static {v6, v7, v8, v9}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v6

    iput v6, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    iget v6, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    if-ltz v6, :cond_5

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    iget v7, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    iput-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-direct {p0, p1, v6}, Lcom/mediatek/vlw/VideoEditor;->checkThumbnailBitmap(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p2

    :cond_3
    :goto_2
    return-object p2

    :catch_0
    move-exception v1

    const-string v6, "VideoEditor"

    const-string v7, "error when release retriver"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_2
    const-string v6, "VideoEditor"

    const-string v7, "corrupt video file "

    invoke-static {v6, v7, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    const-string v6, "VideoEditor"

    const-string v7, "error when release retriver"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :try_start_4
    const-string v6, "VideoEditor"

    const-string v7, "corrupt video file "

    invoke-static {v6, v7, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    const-string v6, "VideoEditor"

    const-string v7, "error when release retriver"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_5
    move-exception v1

    :try_start_6
    const-string v6, "VideoEditor"

    const-string v7, "corrupt video file "

    invoke-static {v6, v7, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v1

    const-string v6, "VideoEditor"

    const-string v7, "error when release retriver"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_7
    move-exception v1

    :try_start_8
    const-string v6, "VideoEditor"

    const-string v7, "error: "

    invoke-static {v6, v7, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_8

    goto/16 :goto_0

    :catch_8
    move-exception v1

    const-string v6, "VideoEditor"

    const-string v7, "error when release retriver"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    :try_start_a
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_9

    :goto_3
    throw v6

    :catch_9
    move-exception v1

    const-string v7, "VideoEditor"

    const-string v8, "error when release retriver"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const-string v6, "VideoEditor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thumbnail bitmap.getWidth() = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",bitmap.getHeight()="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_0

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5
    const-string v6, "VideoEditor"

    const-string v7, "Error: No valid videos, the folder cann\'t be set as wallpaper"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private checkUri()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private clear(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    const v1, 0x927c0

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    if-nez v1, :cond_2

    const-string v1, "VideoEditor"

    const-string v2, "we lost the shared preferences"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "bucketId"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "uri"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "start"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "end"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "pos"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "VideoEditor"

    const-string v2, "clear(), reset the default state into shared_prefs"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private clear(ZZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZ)V

    return-void
.end method

.method private findLiveWallpaper()V
    .locals 15

    iget-object v12, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "com.mediatek.vlw"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "com.mediatek.vlw.VideoLiveWallpaper"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    new-instance v12, Landroid/content/Intent;

    const-string v13, "android.service.wallpaper.WallpaperService"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v13, 0x80

    invoke-virtual {v9, v12, v13}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v10, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v5, v4

    :goto_1
    if-ge v3, v7, :cond_2

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    iget-object v0, v10, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    :try_start_0
    new-instance v4, Landroid/app/WallpaperInfo;

    invoke-direct {v4, p0, v10}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v4}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    const-string v12, "com.mediatek.vlw"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    const-string v12, "com.mediatek.vlw.VideoLiveWallpaper"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    new-instance v12, Landroid/content/Intent;

    const-string v13, "android.service.wallpaper.WallpaperService"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v12, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    iget-object v12, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v12, v8, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v12, "VideoEditor"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Skipping wallpaper "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v5

    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move-object v5, v4

    goto :goto_1

    :catch_1
    move-exception v2

    const-string v12, "VideoEditor"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Skipping wallpaper "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v5

    goto :goto_2

    :cond_2
    move-object v4, v5

    goto/16 :goto_0
.end method

.method private isInPlaybackState()Z
    .locals 2

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadSettings()V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080016

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    const-string v3, "VideoEditor"

    const-string v4, "has no SharedPreferences, use default"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iput v7, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    const v3, 0x927c0

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    iput v7, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    :goto_0
    const-string v3, "VideoEditor"

    const-string v4, "restore from preference, bucket id %s, Uri %s, start time %d, end time %d, paused position %d"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v7, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget v7, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    iget v7, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/mediatek/vlw/Utils;->getUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "bucketId"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "start"

    invoke-interface {v3, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "end"

    const-wide/32 v5, 0x927c0

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "pos"

    invoke-interface {v3, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private pause()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const-string v1, "VideoEditor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() mCurrentPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mCurrentState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mTargetState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    if-ne v1, v4, :cond_0

    iput v5, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VLWMediaController;->pause()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    if-ne v1, v4, :cond_3

    iput v5, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->pause()V

    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/vlw/VideoEditor;->updatePausePlay(Z)V

    goto :goto_1
.end method

.method private play()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "VideoEditor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "play() mCurrentPos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mTargetState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VLWMediaController;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/vlw/VideoEditor$6;

    invoke-direct {v1, p0}, Lcom/mediatek/vlw/VideoEditor$6;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VLWMediaController;->play()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    if-eq v0, v3, :cond_3

    iput v3, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    :cond_3
    invoke-direct {p0, v3}, Lcom/mediatek/vlw/VideoEditor;->updatePausePlay(Z)V

    goto :goto_0
.end method

.method private queryTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    if-nez p2, :cond_1

    const-string v3, "VideoEditor"

    const-string v4, "Uri is null, return null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "query Uri "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    :try_start_0
    invoke-virtual {v1, p1, p2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "VideoEditor"

    const-string v4, "error when release retriver"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_2
    const-string v3, "VideoEditor"

    const-string v4, "corrupt video file "

    invoke-static {v3, v4, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v3, "VideoEditor"

    const-string v4, "error when release retriver"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_3
    move-exception v0

    :try_start_4
    const-string v3, "VideoEditor"

    const-string v4, "error: "

    invoke-static {v3, v4, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    const-string v3, "VideoEditor"

    const-string v4, "error when release retriver"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v3

    :try_start_6
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_5

    :goto_2
    throw v3

    :catch_5
    move-exception v0

    const-string v4, "VideoEditor"

    const-string v5, "error when release retriver"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private queryTitle(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/net/Uri;

    const/4 v8, 0x0

    if-nez p1, :cond_1

    const-string v1, "VideoEditor"

    const-string v3, "Uri is null, return null"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_display_name"

    aput-object v3, v2, v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "_display_name"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    :goto_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    goto :goto_1

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private queryTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "VideoEditor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queryTitle, bucketId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-static {p0, p1}, Lcom/mediatek/vlw/Utils;->queryFolderInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    if-ltz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private resetInfoPanel()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mSetWallpaper:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mSetWallpaper:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "VideoEditor"

    const-string v1, "resetInfoPanel, show folder info"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iput-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const v0, 0x7f0a000b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v0, "VideoEditor"

    const-string v1, "resetInfoPanel, show media controller"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iput-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mFolderInfo:Landroid/widget/TextView;

    :cond_4
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    if-nez v0, :cond_2

    const v0, 0x7f0a000a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vlw/VLWMediaController;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Lcom/mediatek/vlw/VLWMediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Lcom/mediatek/vlw/VLWMediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCallback:Lcom/mediatek/vlw/VLWMediaController$Callback;

    invoke-virtual {v0, v1}, Lcom/mediatek/vlw/VLWMediaController;->addCallback(Lcom/mediatek/vlw/VLWMediaController$Callback;)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mMediaController:Lcom/mediatek/vlw/VLWMediaController;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    goto :goto_0
.end method

.method private restoreMountedData()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevStartTime:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevEndTime:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevCurrentPos:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    return-void
.end method

.method private restoreSavedInstance(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const-string v4, "uri"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iput-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    const-string v4, "VideoEditor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() restore saved uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v4, "bucketId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    const-string v4, "VideoEditor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() restore saved BucketId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const-string v4, "start"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget v4, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    if-eq v4, v1, :cond_1

    if-eqz v1, :cond_1

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    const-string v4, "VideoEditor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() restore saved start time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v4, "end"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget v4, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    if-eq v4, v1, :cond_2

    const v4, 0x927c0

    if-eq v1, v4, :cond_2

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    const-string v4, "VideoEditor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() restore saved end time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "pos"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget v4, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    if-eq v4, v1, :cond_3

    if-eqz v1, :cond_3

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    const-string v4, "VideoEditor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() restore saved position="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private saveSettings()V
    .locals 6

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "bucketId"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "uri"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "start"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "end"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "pos"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "VideoEditor"

    const-string v2, "save settings, bucketId %s, Uri %s, start time %d, end time %d, paused position %d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget v5, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private saveUnmountedData()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevBucketId:Ljava/lang/String;

    const-string v0, "VideoEditor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveUnmountedData, mPrevBucketId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevBucketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevUri:Landroid/net/Uri;

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevStartTime:I

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevEndTime:I

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPrevCurrentPos:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHavaUnmountedData:Z

    return-void
.end method

.method private startPlayback()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-direct {p0, p0, v1}, Lcom/mediatek/vlw/VideoEditor;->checkThumbnailBitmap(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->resetInfoPanel()V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/vlw/VideoEditor;->mIsOpening:Z

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    new-instance v2, Lcom/mediatek/vlw/VideoEditor$7;

    invoke-direct {v2, p0}, Lcom/mediatek/vlw/VideoEditor$7;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->updateInfo()V

    return-void
.end method

.method private stopPlayback()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    :cond_0
    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    return-void
.end method

.method private updateInfo()V
    .locals 7

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/mediatek/vlw/Utils;->getUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/mediatek/vlw/Utils;->queryFolderInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<b>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<b>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08001c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<b>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<b>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/mediatek/vlw/VideoEditor;->queryTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/mediatek/vlw/VideoEditor$8;

    invoke-direct {v5, p0, v3, v2}, Lcom/mediatek/vlw/VideoEditor$8;-><init>(Lcom/mediatek/vlw/VideoEditor;Ljava/lang/String;Landroid/text/Spanned;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v4}, Lcom/mediatek/vlw/VideoEditor;->queryTitle(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/mediatek/vlw/VideoEditor$9;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/vlw/VideoEditor$9;-><init>(Lcom/mediatek/vlw/VideoEditor;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private updatePausePlay(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x2

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    iput v1, p0, Lcom/mediatek/vlw/VideoEditor;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public captureVideo(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-static {p0}, Lcom/mediatek/vlw/Utils;->queryResolutionRatio(Landroid/content/Context;)F

    move-result v1

    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "To captureVideo, ratio="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "identity"

    const-string v4, "com.mediatek.vlw"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "ratio"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult request code = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", resultCode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v3, "VideoEditor"

    const-string v4, "unknown request"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_1
    invoke-direct {p0, v6, v7, v7}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZZ)V

    const/4 v2, 0x0

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p0, v1}, Lcom/mediatek/vlw/Utils;->getVideoPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    :goto_1
    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PICK_VIDEO_REQUEST, uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->startPlayback()V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    goto :goto_1

    :pswitch_1
    const-string v3, "bucketId"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/mediatek/vlw/Utils;->getUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput v6, p0, Lcom/mediatek/vlw/VideoEditor;->mMode:I

    :cond_3
    if-eqz v1, :cond_0

    invoke-direct {p0, v6, v6}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZ)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUriInvalid:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_4
    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PICK_FOLDER_REQUEST,  bucketId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoEditor;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " videos selected, uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->startPlayback()V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {p0, v1}, Lcom/mediatek/vlw/Utils;->getVideoPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-direct {p0, v6, v7}, Lcom/mediatek/vlw/VideoEditor;->clear(ZZ)V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PICK_CAMERA_REQUEST, uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->startPlayback()V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    const-string v1, "vlw"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mSharedPref:Landroid/content/SharedPreferences;

    const v1, 0x7f0a0006

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    const v1, 0x7f0a0007

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mVideoTitle:Landroid/widget/TextView;

    const v1, 0x7f0a0008

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    const v1, 0x7f0a000f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mSetWallpaper:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->loadSettings()V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoEditor;->restoreSavedInstance(Landroid/os/Bundle;)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->startPlayback()V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    const-string v1, "VideoEditor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() register receiver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/vlw/VideoEditor$SelectVideo;-><init>(Lcom/mediatek/vlw/VideoEditor;Lcom/mediatek/vlw/VideoEditor$1;)V

    invoke-virtual {v0}, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->createDialog()Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v0, "VideoEditor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() unregister receiver: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->stopPlayback()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->pause()V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayPause:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->pause()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->resetInfoPanel()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->updateInfo()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    const-string v1, "uri"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "start"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "end"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "pos"

    iget v2, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "bucketId"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mBucketId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "VideoEditor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSaveInstanceState() mUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mStartTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mStartTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mEndTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mEndTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCurrentPos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentPos:I

    goto :goto_0
.end method

.method public runInUIThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public selectDefaultVideo(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/vlw/VideoChooser;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public selectVideo(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoEditor;->mClosed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    return-void
.end method

.method public setLiveWallpaper(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->checkUri()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f08000a

    invoke-static {p0, v2, v5}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mSetWallpaper:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->saveSettings()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->findLiveWallpaper()V

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    if-nez v2, :cond_1

    const-string v2, "VideoEditor"

    const-string v3, "Can not find Video Live Wallpaper package."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->stopPlayback()V

    :try_start_0
    const-string v2, "VideoEditor"

    const-string v3, "Set Video Live Wallpaper."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/app/IWallpaperManager;->setWallpaperComponent(Landroid/content/ComponentName;)V

    const/high16 v2, 0x3f000000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/high16 v3, 0x3f000000

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setResult(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    iput-boolean v5, p0, Lcom/mediatek/vlw/VideoEditor;->mClosed:Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "VideoEditor"

    const-string v3, "Failure setting wallpaper"

    invoke-static {v2, v3, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public updatePausePlay(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor;->mPlayer:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoEditor;->mCurrentState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->pause()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor;->play()V

    goto :goto_0
.end method
