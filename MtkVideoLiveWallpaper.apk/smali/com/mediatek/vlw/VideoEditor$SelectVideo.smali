.class Lcom/mediatek/vlw/VideoEditor$SelectVideo;
.super Ljava/lang/Object;
.source "VideoEditor.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectVideo"
.end annotation


# instance fields
.field private mAdapter:Lcom/mediatek/vlw/AddAdapter;

.field final synthetic this$0:Lcom/mediatek/vlw/VideoEditor;


# direct methods
.method private constructor <init>(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/vlw/VideoEditor;Lcom/mediatek/vlw/VideoEditor$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/vlw/VideoEditor;
    .param p2    # Lcom/mediatek/vlw/VideoEditor$1;

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoEditor$SelectVideo;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    return-void
.end method

.method private cleanup()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    return-void
.end method


# virtual methods
.method createDialog()Landroid/app/Dialog;
    .locals 4

    new-instance v2, Lcom/mediatek/vlw/AddAdapter;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {v2, v3}, Lcom/mediatek/vlw/AddAdapter;-><init>(Lcom/mediatek/vlw/VideoEditor;)V

    iput-object v2, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->mAdapter:Lcom/mediatek/vlw/AddAdapter;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->mAdapter:Lcom/mediatek/vlw/AddAdapter;

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1, p0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object v1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->cleanup()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->cleanup()V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->mAdapter:Lcom/mediatek/vlw/AddAdapter;

    invoke-virtual {v3, p2}, Lcom/mediatek/vlw/AddAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/vlw/AddAdapter$ListItem;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, v0, Lcom/mediatek/vlw/AddAdapter$ListItem;->mActionTag:I

    packed-switch v3, :pswitch_data_0

    const-string v3, "VideoEditor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown item actionTag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/vlw/AddAdapter$ListItem;->mActionTag:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "video/*"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v1, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.mediatek.action.PICK_VIDEO_FOLDER"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "video/*"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v1, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor$SelectVideo;->this$0:Lcom/mediatek/vlw/VideoEditor;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    return-void
.end method
