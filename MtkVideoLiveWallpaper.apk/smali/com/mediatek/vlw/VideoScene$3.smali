.class Lcom/mediatek/vlw/VideoScene$3;
.super Landroid/content/BroadcastReceiver;
.source "VideoScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v9, "storage_volume"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-nez v4, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    const-string v10, "file"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Receive intent action="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " path="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9, v4}, Lcom/mediatek/vlw/VideoScene;->access$1800(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v10, Ljava/util/ArrayList;

    const/4 v11, 0x2

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$1902(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_2
    if-eqz v4, :cond_3

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v9

    invoke-virtual {v9}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_7

    :cond_4
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v9

    if-eqz v9, :cond_7

    if-eqz v4, :cond_7

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$502(Lcom/mediatek/vlw/VideoScene;Z)Z

    :cond_6
    :goto_0
    return-void

    :cond_7
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/vlw/Utils;->swapSdcardUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_d

    const/4 v9, 0x0

    const-string v10, "/"

    invoke-virtual {v8, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x0

    const-string v10, "/"

    invoke-virtual {v7, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    invoke-static {v6}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    const-string v9, "VideoScene"

    const-string v10, "The video belonging sdcard unmounted"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v8}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$802(Lcom/mediatek/vlw/VideoScene;I)I

    :goto_1
    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Restore the video last time. mPrevUri="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mPrevBucketId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mPrevStartTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mPrevEndTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mPrevCurrentPos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2102(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2202(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v9, "bucketId"

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v9, "uri"

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v9, "pos"

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    int-to-long v10, v10

    invoke-interface {v3, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_9

    const-string v9, "start"

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    int-to-long v10, v10

    invoke-interface {v3, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v9, "end"

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    int-to-long v10, v10

    invoke-interface {v3, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_9
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$600(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v9

    if-nez v9, :cond_a

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    int-to-long v10, v10

    invoke-static {v9, v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;J)J

    :cond_a
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$402(Lcom/mediatek/vlw/VideoScene;Z)Z

    goto/16 :goto_0

    :cond_b
    invoke-static {v7}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_c
    const-string v9, "VideoScene"

    const-string v10, "cannot reload videos selected last time"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_d
    invoke-static {v8}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_e
    invoke-static {v7}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_f
    const-string v9, "VideoScene"

    const-string v10, "video file selected last time does not exists"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    const-string v9, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_11

    const-string v9, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_11

    const-string v9, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_11

    const-string v9, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    :cond_11
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v9

    if-eqz v9, :cond_12

    const-string v9, "VideoScene"

    const-string v10, "Has been shutdown, Ignore"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_12
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_13

    if-eqz v4, :cond_13

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_13
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v8, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "action: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " revert to default video. sdcard path: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " absolute path: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mUri: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f080007

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2202(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2102(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2402(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2502(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v10}, Lcom/mediatek/vlw/VideoScene;->getCurrentPosition()I

    move-result v10

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$2602(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1600(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$1200(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v9}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :cond_14
    const-string v9, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v8, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$2800(Lcom/mediatek/vlw/VideoScene;)Z

    goto/16 :goto_0
.end method
