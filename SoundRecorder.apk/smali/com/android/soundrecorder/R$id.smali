.class public final Lcom/android/soundrecorder/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final acceptButton:I = 0x7f0b0013

.field public static final app_icon:I = 0x7f0b001d

.field public static final app_name:I = 0x7f0b0024

.field public static final bottomLinearLayout:I = 0x7f0b0028

.field public static final btn_goon_play:I = 0x7f0b0020

.field public static final btn_goon_record:I = 0x7f0b001f

.field public static final btn_parent:I = 0x7f0b001e

.field public static final btn_pause:I = 0x7f0b0021

.field public static final btn_stop:I = 0x7f0b0022

.field public static final buttonParent:I = 0x7f0b0016

.field public static final currState:I = 0x7f0b0008

.field public static final deleteButton:I = 0x7f0b002b

.field public static final discardButton:I = 0x7f0b0012

.field public static final divider_img:I = 0x7f0b002a

.field public static final empty_view:I = 0x7f0b0029

.field public static final exitButtons:I = 0x7f0b0011

.field public static final fileListButton:I = 0x7f0b001b

.field public static final file_name:I = 0x7f0b0025

.field public static final firstLine:I = 0x7f0b0010

.field public static final frameLayout:I = 0x7f0b000f

.field public static final message:I = 0x7f0b0026

.field public static final pauseRecordingButton:I = 0x7f0b0018

.field public static final playButton:I = 0x7f0b0019

.field public static final recordButton:I = 0x7f0b0017

.field public static final record_effect:I = 0x7f0b002e

.field public static final record_file_checkbox:I = 0x7f0b0000

.field public static final record_file_duration:I = 0x7f0b0003

.field public static final record_file_icon:I = 0x7f0b001c

.field public static final record_file_name:I = 0x7f0b0001

.field public static final record_file_title:I = 0x7f0b0002

.field public static final record_format:I = 0x7f0b002c

.field public static final record_mode:I = 0x7f0b002d

.field public static final recordfileMessage2Layout:I = 0x7f0b0007

.field public static final recordingFileName:I = 0x7f0b0009

.field public static final recording_file_list_view:I = 0x7f0b0027

.field public static final secondLine:I = 0x7f0b0014

.field public static final stateLED:I = 0x7f0b000b

.field public static final stateMessage1:I = 0x7f0b000d

.field public static final stateMessage2:I = 0x7f0b000c

.field public static final stateMessage2Layout:I = 0x7f0b000a

.field public static final stateProgressBar:I = 0x7f0b000e

.field public static final stopButton:I = 0x7f0b001a

.field public static final timerView:I = 0x7f0b0006

.field public static final timerViewLayout:I = 0x7f0b0005

.field public static final txt_name:I = 0x7f0b0023

.field public static final uvMeter:I = 0x7f0b0015

.field public static final whole_view:I = 0x7f0b0004


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
