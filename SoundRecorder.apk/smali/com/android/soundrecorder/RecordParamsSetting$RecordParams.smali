.class public Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;
.super Ljava/lang/Object;
.source "RecordParamsSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/RecordParamsSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecordParams"
.end annotation


# instance fields
.field public mAudioChannels:I

.field public mAudioEffect:[Z

.field public mAudioEncoder:I

.field public mAudioEncodingBitRate:I

.field public mAudioSamplingRate:I

.field public mExtension:Ljava/lang/String;

.field public mHDRecordMode:I

.field public mMimeType:Ljava/lang/String;

.field public mOutputFormat:I

.field public mRemainingTimeCalculatorBitRate:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioChannels:I

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncoder:I

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEncodingBitRate:I

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mRemainingTimeCalculatorBitRate:I

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioSamplingRate:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mExtension:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mMimeType:Ljava/lang/String;

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mHDRecordMode:I

    iput v1, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mOutputFormat:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;->mAudioEffect:[Z

    return-void
.end method
