.class public Lcom/android/soundrecorder/Player;
.super Ljava/lang/Object;
.source "Player.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/Player$PlayerListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SR/Player"


# instance fields
.field private mCurrentFilePath:Ljava/lang/String;

.field private mListener:Lcom/android/soundrecorder/Player$PlayerListener;

.field private mPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>(Lcom/android/soundrecorder/Player$PlayerListener;)V
    .locals 1
    .param p1    # Lcom/android/soundrecorder/Player$PlayerListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    iput-object p1, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    invoke-interface {v0, p0, p1}, Lcom/android/soundrecorder/Player$PlayerListener;->onStateChanged(Lcom/android/soundrecorder/Player;I)V

    return-void
.end method


# virtual methods
.method public getCurrentProgress()I
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileDuration()I
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public goonPlayback()Z
    .locals 2

    iget-object v1, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Player;->setState(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/android/soundrecorder/Player;->handleException(Ljava/lang/Exception;)Z

    move-result v1

    goto :goto_0
.end method

.method public handleException(Ljava/lang/Exception;)Z
    .locals 3
    .param p1    # Ljava/lang/Exception;

    const-string v0, "SR/Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<handleException> the exception is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    const/16 v1, 0x8

    invoke-interface {v0, p0, v1}, Lcom/android/soundrecorder/Player$PlayerListener;->onError(Lcom/android/soundrecorder/Player;I)V

    const/4 v0, 0x0

    return v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/android/soundrecorder/Player;->stopPlayback()Z

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    const/16 v1, 0x8

    invoke-interface {v0, p0, v1}, Lcom/android/soundrecorder/Player$PlayerListener;->onError(Lcom/android/soundrecorder/Player;I)V

    const/4 v0, 0x0

    return v0
.end method

.method public pausePlayback()Z
    .locals 2

    iget-object v1, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Player;->setState(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/android/soundrecorder/Player;->handleException(Ljava/lang/Exception;)Z

    move-result v1

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    :cond_0
    iput-object v1, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    return-void
.end method

.method public setCurrentFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    return-void
.end method

.method public startPlayback()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/soundrecorder/Player;->mListener:Lcom/android/soundrecorder/Player$PlayerListener;

    const/16 v4, 0x9

    invoke-interface {v3, p0, v4}, Lcom/android/soundrecorder/Player$PlayerListener;->onError(Lcom/android/soundrecorder/Player;I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_2

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/android/soundrecorder/Player;->mCurrentFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    const-string v2, "SR/Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<startPlayback> The length of recording file is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Player;->setState(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/android/soundrecorder/Player;->handleException(Ljava/lang/Exception;)Z

    move-result v2

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/android/soundrecorder/Player;->handleException(Ljava/lang/Exception;)Z

    move-result v2

    goto :goto_0
.end method

.method public stopPlayback()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Player;->setState(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/soundrecorder/Player;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/android/soundrecorder/Player;->handleException(Ljava/lang/Exception;)Z

    move-result v1

    goto :goto_0
.end method
