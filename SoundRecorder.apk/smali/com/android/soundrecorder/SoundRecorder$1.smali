.class Lcom/android/soundrecorder/SoundRecorder$1;
.super Ljava/lang/Object;
.source "SoundRecorder.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/SoundRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/SoundRecorder;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder$1;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onServiceConnected> Service connected"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder$1;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    check-cast p2, Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;

    invoke-virtual {p2}, Lcom/android/soundrecorder/SoundRecorderService$SoundRecorderBinder;->getService()Lcom/android/soundrecorder/SoundRecorderService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SoundRecorder;->access$002(Lcom/android/soundrecorder/SoundRecorder;Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder$1;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v0}, Lcom/android/soundrecorder/SoundRecorder;->access$100(Lcom/android/soundrecorder/SoundRecorder;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onServiceDisconnected> Service dis connected"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder$1;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SoundRecorder;->access$002(Lcom/android/soundrecorder/SoundRecorder;Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService;

    return-void
.end method
