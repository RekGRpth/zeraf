.class public Lcom/android/soundrecorder/SoundRecorder;
.super Landroid/app/Activity;
.source "SoundRecorder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;
.implements Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;
.implements Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;
.implements Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;


# static fields
.field private static final AUDIO_NOT_LIMIT_TYPE:Ljava/lang/String; = "audio/*"

.field public static final DIALOG_SELECT_EFFECT:I = 0x2

.field private static final DIALOG_SELECT_FORMAT:I = 0x1

.field private static final DIALOG_SELECT_MODE:I = 0x0

.field private static final DIALOG_TAG_SELECT_EFFECT:Ljava/lang/String; = "SelectEffect"

.field private static final DIALOG_TAG_SELECT_FORMAT:Ljava/lang/String; = "SelectFormat"

.field private static final DIALOG_TAG_SELECT_MODE:Ljava/lang/String; = "SelectMode"

.field private static final DONE:I = 0x64

.field public static final DOWHAT:Ljava/lang/String; = "dowhat"

.field public static final EMPTY:Ljava/lang/String; = ""

.field private static final EXTRA_MAX_BYTES:Ljava/lang/String; = "android.provider.MediaStore.extra.MAX_BYTES"

.field private static final INFLATER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

.field public static final INIT:Ljava/lang/String; = "init"

.field private static final INTENT_ACTION_MAIN:Ljava/lang/String; = "android.intent.action.MAIN"

.field private static final LIST_MENUITEM_VIEW_NAME:Ljava/lang/String; = "com.android.internal.view.menu.ListMenuItemView"

.field private static final MAX_FILE_SIZE_NULL:J = -0x1L

.field private static final MMS_FILE_LIMIT:I = 0xbe

.field private static final NULL_STRING:Ljava/lang/String; = ""

.field private static final ONE_SECOND:J = 0x3e8L

.field private static final OPTIONMENU_SELECT_EFFECT:I = 0x2

.field private static final OPTIONMENU_SELECT_FORMAT:I = 0x0

.field private static final OPTIONMENU_SELECT_MODE:I = 0x1

.field private static final PATH:Ljava/lang/String; = "path"

.field public static final PLAY:Ljava/lang/String; = "play"

.field public static final RECORD:Ljava/lang/String; = "record"

.field private static final REQURST_FILE_LIST:I = 0x1

.field private static final SOUND_RECORDER_DATA:Ljava/lang/String; = "sound_recorder_data"

.field private static final TAG:Ljava/lang/String; = "SR/SoundRecorder"

.field private static final THREE_BUTTON_WEIGHT_SUM:I = 0x3

.field public static final THREE_LEVELS:I = 0x3

.field private static final TIME_BASE:I = 0x3c

.field private static final TIME_NINE_MIN:I = 0x21c

.field private static final TWO_BUTTON_WEIGHT_SUM:I = 0x2

.field public static final TWO_LEVELS:I = 0x2

.field private static sListMenuItemViewClass:Ljava/lang/Class;

.field private static sListMenuItemViewConstructor:Ljava/lang/reflect/Constructor;


# instance fields
.field private mAcceptButton:Landroid/widget/Button;

.field private mBackPressed:Z

.field private mButtonParent:Landroid/widget/LinearLayout;

.field private mCurrentState:I

.field private mDiscardButton:Landroid/widget/Button;

.field private mDoWhat:Ljava/lang/String;

.field private mDoWhatFilePath:Ljava/lang/String;

.field private mExitButtons:Landroid/widget/LinearLayout;

.field private mFileListButton:Landroid/widget/ImageButton;

.field private mFileName:Ljava/lang/String;

.field private mFirstLine:Landroid/widget/ImageView;

.field private mFrameLayout:Landroid/widget/FrameLayout;

.field private mHasFileSizeLimitation:Z

.field private mIsStopService:Z

.field private mMaxFileSize:J

.field private mMenu:Landroid/view/Menu;

.field private mOnSaveInstanceStateHasRun:Z

.field private mPauseRecordingButton:Landroid/widget/ImageButton;

.field private mPlayButton:Landroid/widget/ImageButton;

.field private mPlayingStateImageView:Landroid/widget/ImageView;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mRecordButton:Landroid/widget/ImageButton;

.field private mRecordingFileNameTextView:Landroid/widget/TextView;

.field private mRecordingStateImageView:Landroid/widget/ImageView;

.field private mRemainingTimeTextView:Landroid/widget/TextView;

.field private mRequestedType:Ljava/lang/String;

.field private mRunFromLauncher:Z

.field private mSecondLine:Landroid/widget/ImageView;

.field private mSelectEffectArray:[Z

.field private mSelectEffectArrayTemp:[Z

.field private mSelectEffectMultiChoiceClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field private mSelectEffectOkListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSelectedFormat:I

.field private mSelectedMode:I

.field private mService:Lcom/android/soundrecorder/SoundRecorderService;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mSetResultAfterSave:Z

.field private mStateProgressBar:Landroid/widget/ProgressBar;

.field private mStateTextView:Landroid/widget/TextView;

.field private mStopButton:Landroid/widget/ImageButton;

.field private mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

.field private mTimerFormat:Ljava/lang/String;

.field private mTimerTextView:Landroid/widget/TextView;

.field private mVUMeter:Lcom/android/soundrecorder/VUMeter;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/util/AttributeSet;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/soundrecorder/SoundRecorder;->INFLATER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    sput-object v3, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewClass:Ljava/lang/Class;

    sput-object v3, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewConstructor:Ljava/lang/reflect/Constructor;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v1, 0x3

    const/4 v0, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    iput v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    const-string v0, "audio/*"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhatFilePath:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mBackPressed:Z

    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mOnSaveInstanceStateHasRun:Z

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsStopService:Z

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSetResultAfterSave:Z

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$1;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$1;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mServiceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$2;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$2;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$3;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$3;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$4;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$4;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectOkListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$5;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$5;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectMultiChoiceClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/SoundRecorderService;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/soundrecorder/SoundRecorder;Lcom/android/soundrecorder/SoundRecorderService;)Lcom/android/soundrecorder/SoundRecorderService;
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Lcom/android/soundrecorder/SoundRecorderService;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initWhenHaveService()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/soundrecorder/SoundRecorder;)[Z
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/soundrecorder/SoundRecorder;[Z)[Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # [Z

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/soundrecorder/SoundRecorder;)[Z
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$402(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 0
    .param p0    # Ljava/lang/Class;

    sput-object p0, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewClass:Ljava/lang/Class;

    return-object p0
.end method

.method static synthetic access$500()Ljava/lang/reflect/Constructor;
    .locals 1

    sget-object v0, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewConstructor:Ljava/lang/reflect/Constructor;

    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/reflect/Constructor;)Ljava/lang/reflect/Constructor;
    .locals 0
    .param p0    # Ljava/lang/reflect/Constructor;

    sput-object p0, Lcom/android/soundrecorder/SoundRecorder;->sListMenuItemViewConstructor:Ljava/lang/reflect/Constructor;

    return-object p0
.end method

.method static synthetic access$600()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/android/soundrecorder/SoundRecorder;->INFLATER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<acquireWakeLock>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private hideStorageHint()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/soundrecorder/OnScreenHint;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    :cond_0
    return-void
.end method

.method private initFromIntent()Z
    .locals 9

    const-wide/16 v7, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v2, "SR/SoundRecorder"

    const-string v5, "<initFromIntent> start"

    invoke-static {v2, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v2, "SR/SoundRecorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<initFromIntent> Intent is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.android.soundrecorder.SoundRecorder"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    move v2, v4

    :goto_0
    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/android/soundrecorder/RecordParamsSetting;->isAvailableRequestType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    :cond_1
    const-string v2, "android.provider.MediaStore.extra.MAX_BYTES"

    invoke-virtual {v0, v2, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    iget-wide v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    cmp-long v2, v5, v7

    if-eqz v2, :cond_2

    move v3, v4

    :cond_2
    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    :cond_3
    const-string v2, "SR/SoundRecorder"

    const-string v3, "<initFromIntent> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    :goto_1
    return v3

    :cond_4
    move v2, v3

    goto :goto_0

    :cond_5
    const-string v2, "SR/SoundRecorder"

    const-string v4, "<initFromIntent> return false"

    invoke-static {v2, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private initResourceRefs()V
    .locals 6

    const/16 v3, 0x8

    const/4 v5, 0x0

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<initResourceRefs> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefsWhenNoService()V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v2}, Lcom/android/soundrecorder/SoundRecorderService;->getRecorder()Lcom/android/soundrecorder/Recorder;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/VUMeter;->setRecorder(Lcom/android/soundrecorder/Recorder;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<initResourceRefs> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private initResourceRefsWhenNoService()V
    .locals 2

    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    const v0, 0x7f0b001a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v0, 0x7f0b001b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    const v0, 0x7f0b0016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v0, 0x7f0b000e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f0b0006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    const v0, 0x7f0b0008

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v0, 0x7f0b0009

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    const v0, 0x7f0b0011

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    const v0, 0x7f0b0015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/soundrecorder/VUMeter;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v0, 0x7f0b0012

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    const v0, 0x7f0b0010

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    const v0, 0x7f0b0014

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    const v0, 0x7f0b000f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    return-void
.end method

.method private initWhenHaveService()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<initWhenHaveService> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->setErrorListener(Lcom/android/soundrecorder/SoundRecorderService$OnErrorListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->setEventListener(Lcom/android/soundrecorder/SoundRecorderService$OnEventListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->setStateChangedListener(Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setShowNotification(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0, p0}, Lcom/android/soundrecorder/SoundRecorderService;->setUpdateTimeViewListener(Lcom/android/soundrecorder/SoundRecorderService$OnUpdateTimeViewListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v0

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<initWhenHaveService> mCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefs()V

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v0, :cond_2

    const/4 v0, 0x2

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x3

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<initWhenHaveService> stop record when run from other ap"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    :cond_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSetResultAfterSave:Z

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<initWhenHaveService> save record when run from other ap"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->saveRecord()Z

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->restoreRecordParamsSettings()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateOptionsMenu()V

    const-string v0, "record"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickRecordButton()V

    :cond_3
    :goto_1
    iput-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhatFilePath:Ljava/lang/String;

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<initWhenHaveService> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    goto :goto_0

    :cond_5
    const-string v0, "play"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhatFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->playFile(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private releaseWakeLock()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<releaseWakeLock>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private removeOldFragmentByTag(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<removeOldFragmentByTag> start"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<removeOldFragmentByTag> oldFragment = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<removeOldFragmentByTag> remove oldFragment success"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v2, "SR/SoundRecorder"

    const-string v3, "<removeOldFragmentByTag> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private resetUi()V
    .locals 7

    const/16 v6, 0x8

    const/4 v3, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefsWhenNoService()V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40000000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private restoreDialogFragment()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "SelectFormat"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_0
    const-string v2, "SelectMode"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_1
    const-string v2, "SelectEffect"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v2, v0

    check-cast v2, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectMultiChoiceClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    invoke-virtual {v2, v3}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnMultiChoiceListener(Landroid/content/DialogInterface$OnMultiChoiceClickListener;)V

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectOkListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_2
    return-void
.end method

.method private restoreRecordParamsSettings()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<restoreRecordParamsSettings> "

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    const-string v2, "sound_recorder_data"

    invoke-virtual {p0, v2, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    invoke-static {p0}, Lcom/mediatek/soundrecorder/ext/ExtensionHelper;->getExtension(Landroid/content/Context;)Lcom/mediatek/soundrecorder/ext/IQualityLevel;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/soundrecorder/ext/IQualityLevel;->getLevelNumber()I

    move-result v0

    if-ne v6, v0, :cond_4

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "selected_recording_format"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    if-eq v7, v2, :cond_1

    iput v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "selected_recording_mode"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    if-gez v2, :cond_2

    iput v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    :cond_2
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_aec"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v5

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_agc"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v6

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_ns"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v7

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_aec_tmp"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v5

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_agc_tmp"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v6

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "selected_recording_effect_ns_tmp"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    aput-boolean v3, v2, v7

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-virtual {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService;->setSelectedFormat(I)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-virtual {v2, v3}, Lcom/android/soundrecorder/SoundRecorderService;->setSelectedMode(I)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    invoke-virtual {v2, v3, v5}, Lcom/android/soundrecorder/SoundRecorderService;->setSelectEffectArray([ZZ)V

    :cond_3
    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSelectedFormat is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mSelectedMode is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    const/4 v2, 0x3

    if-ne v2, v0, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "selected_recording_format"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    if-gez v2, :cond_1

    iput v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    goto/16 :goto_0
.end method

.method private setTimerViewTextSize(I)V
    .locals 4
    .param p1    # I

    const/16 v0, 0x64

    div-int/lit8 v1, p1, 0x3c

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_0
.end method

.method private showDialogFragment(ILandroid/os/Bundle;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    const-string v0, "SR/SoundRecorder"

    const-string v2, "<showDialogFragment> start"

    invoke-static {v0, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {v6}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<showDialogFragment> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_0
    const-string v0, "SelectFormat"

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/soundrecorder/RecordParamsSetting;->getFormatStringIDArray(Landroid/content/Context;)[I

    move-result-object v0

    const v2, 0x7f080013

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/android/soundrecorder/SelectDialogFragment;->newInstance([I[Ljava/lang/CharSequence;IZI[Z)Lcom/android/soundrecorder/SelectDialogFragment;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    const-string v0, "SelectFormat"

    invoke-virtual {v7, v6, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<showDialogFragment> show select format dialog"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const-string v0, "SelectMode"

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->getModeStringIDArray()[I

    move-result-object v0

    const v2, 0x7f080014

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/android/soundrecorder/SelectDialogFragment;->newInstance([I[Ljava/lang/CharSequence;IZI[Z)Lcom/android/soundrecorder/SelectDialogFragment;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    const-string v0, "SelectMode"

    invoke-virtual {v7, v6, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<showDialogFragment> show select mode dialog"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "SelectEffect"

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->getEffectStringIDArray()[I

    move-result-object v0

    const v2, 0x7f080015

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    move v3, v8

    move v4, v8

    invoke-static/range {v0 .. v5}, Lcom/android/soundrecorder/SelectDialogFragment;->newInstance([I[Ljava/lang/CharSequence;IZI[Z)Lcom/android/soundrecorder/SelectDialogFragment;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectOkListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    move-object v0, v7

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectMultiChoiceClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnMultiChoiceListener(Landroid/content/DialogInterface$OnMultiChoiceClickListener;)V

    const-string v0, "SelectEffect"

    invoke-virtual {v7, v6, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private showStorageHint(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/soundrecorder/OnScreenHint;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/soundrecorder/OnScreenHint;

    move-result-object v0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    :goto_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/soundrecorder/OnScreenHint;->show()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v0, p1}, Lcom/android/soundrecorder/OnScreenHint;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateOptionsMenu()V
    .locals 8

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v6, "SR/SoundRecorder"

    const-string v7, "<updateOptionsMenu>"

    invoke-static {v6, v7}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    if-nez v6, :cond_1

    const-string v4, "SR/SoundRecorder"

    const-string v5, "<updateOptionsMenu> mMenu == null, return"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v6

    if-ne v4, v6, :cond_5

    move v0, v4

    :cond_2
    :goto_1
    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectFormat()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    invoke-interface {v6, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectMode()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    invoke-interface {v5, v4}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    invoke-static {}, Lcom/android/soundrecorder/RecordParamsSetting;->canSelectEffect()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_5
    move v0, v5

    goto :goto_1
.end method

.method private updateRemainingTimerView(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, ""

    if-gez p1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<updateRemainingTimerView> mRemainingTimeTextView.setText: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x3c

    if-ge p1, v1, :cond_1

    const v1, 0x7f080031

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    rem-int/lit8 v1, p1, 0x3c

    if-nez v1, :cond_2

    const v1, 0x7f080030

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    div-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v1, 0x7f080017

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    div-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    rem-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateUiOnIdleState()V
    .locals 11

    const/high16 v6, 0x7f050000

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x0

    const-string v4, "SR/SoundRecorder"

    const-string v5, "<updateUiOnIdleState> start"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFilePath()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v5, 0x40000000

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez v0, :cond_1

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setFocusable(Z)V

    :goto_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v5, 0x0

    iput v5, v4, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    :goto_1
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    div-int/lit8 v6, v2, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    rem-int/lit8 v6, v2, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, "SR/SoundRecorder"

    const-string v5, "<updateUiOnIdleState> end"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    :cond_1
    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v5, 0x40400000

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v5, 0x7f02000f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v5, 0x7f020019

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v1

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFileDurationInSecond()J

    move-result-wide v4

    long-to-int v2, v4

    :cond_3
    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private updateUiOnPausePlayingState()V
    .locals 7

    const/16 v5, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUiOnPausePlayingState> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40400000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v2, 0x7f02000f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v2, 0x7f02000c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerTextView()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const-wide/16 v2, 0x64

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInMillSecond()J

    move-result-wide v4

    mul-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFileDurationInMillSecond()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUiOnPausePlayingState> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUiOnPauseRecordingState()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUiOnPauseRecordingState> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40000000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v2, 0x7f08000f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerTextView()V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUiOnPauseRecordingState> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private updateUiOnPlayingState()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<updateUiOnPlayingState> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v1, 0x40400000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v1, 0x7f02000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerTextView()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<updateUiOnPlayingState> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUiOnRecordingState()V
    .locals 9

    const/4 v8, 0x4

    const/16 v7, 0x8

    const/4 v0, 0x1

    const/4 v3, 0x0

    const-string v4, "SR/SoundRecorder"

    const-string v5, "<updateUiOnRecordingState> start"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f050000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v5, 0x40000000

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setFocusable(Z)V

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    :goto_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v5, 0x7f020018

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v5, 0x7f08002c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFirstLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSecondLine:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->getRemainingTime()J

    move-result-wide v4

    long-to-int v1, v4

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    if-eqz v4, :cond_4

    const/16 v4, 0xbe

    if-ge v1, v4, :cond_3

    :cond_0
    :goto_1
    if-lez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/SoundRecorder;->updateRemainingTimerView(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerTextView()V

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<updateUiOnRecordingState> end"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    const/16 v4, 0x21c

    if-lt v1, v4, :cond_0

    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method protected addOptionsMenuInflaterFactory()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    new-instance v1, Lcom/android/soundrecorder/SoundRecorder$6;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorder$6;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onActivityResult> start"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, -0x1

    if-eq v2, p2, :cond_0

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onActivityResult> resultCode != RESULT_OK, return"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    move-object v1, p3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onActivityResult> bundle == null, return"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v2, "dowhat"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v3, "play"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "path"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "path"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhatFilePath:Ljava/lang/String;

    :cond_3
    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onActivityResult> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onBackPressed> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onBackPressed> Activity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mBackPressed:Z

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onBackPressed> mService.saveRecord()"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->saveRecord()Z

    :goto_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onBackPressed> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->storeRecordParamsSettings()V

    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onClick> Activity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> discardButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickDiscardButton()V

    goto :goto_0

    :pswitch_2
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> recordButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickRecordButton()V

    goto :goto_0

    :pswitch_3
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> playButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickPlayButton()V

    goto :goto_0

    :pswitch_4
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> stopButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickStopButton()V

    goto :goto_0

    :pswitch_5
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> acceptButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickAcceptButton()V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickFileListButton()V

    goto :goto_0

    :pswitch_7
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> pauseRecordingButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickPauseRecordingButton()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0012
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method onClickAcceptButton()V
    .locals 4

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v1, v0, :cond_1

    const/4 v1, 0x4

    if-ne v1, v0, :cond_2

    :cond_1
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClickAcceptButton> mService.stopPlay() first"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->saveRecord()Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method onClickDiscardButton()V
    .locals 4

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v1, v0, :cond_0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_1

    :cond_0
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClickDiscardButton> mService.stopPlay() first"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->discardRecord()Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method onClickFileListButton()V
    .locals 3

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClickFileListButton> mService.reset()"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method onClickPauseRecordingButton()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->pauseRecord()Z

    :cond_0
    return-void
.end method

.method onClickPlayButton()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->playCurrentFile()Z

    :cond_0
    return-void
.end method

.method onClickRecordButton()V
    .locals 7

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iget v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    invoke-static {v3, v4, v5, v6}, Lcom/android/soundrecorder/RecordParamsSetting;->getRecordParams(Ljava/lang/String;II[Z)Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    long-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/android/soundrecorder/SoundRecorderService;->record(Lcom/android/soundrecorder/RecordParamsSetting$RecordParams;I)Z

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][SoundRecorder] recording end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method onClickStopButton()V
    .locals 6

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Performance test][SoundRecorder] recording stop end ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v2

    const/4 v3, 0x5

    if-eq v3, v2, :cond_1

    const/4 v3, 0x4

    if-ne v3, v2, :cond_3

    :cond_1
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onClickStopButton> mService.stopPlay()"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->stopPlay()Z

    :cond_2
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Performance test][SoundRecorder] recording stop end ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v3, 0x2

    if-eq v3, v2, :cond_4

    const/4 v3, 0x3

    if-ne v3, v2, :cond_2

    :cond_4
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onClickStopButton> mService.stopRecord()"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onConfigurationChanged> start"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    const/4 v2, -0x1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    :cond_0
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefs()V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    :cond_1
    if-ltz v2, :cond_2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_2
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onConfigurationChanged> end"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onCreate> start, Activity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->addOptionsMenuInflaterFactory()V

    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initFromIntent()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v1, :cond_1

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onCreate> PowerManager == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    const/4 v1, 0x6

    const-string v2, "SR/SoundRecorder"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_1
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onCreate> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onCreateOptionsMenu> begin"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    new-instance v0, Landroid/view/MenuInflater;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onCreateOptionsMenu> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onDestroy> start, Activity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsStopService:Z

    if-eqz v0, :cond_0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onDestroy> stop service"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->releaseWakeLock()V

    :cond_1
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onDestroy> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onError(I)V
    .locals 3
    .param p1    # I

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onError> errorCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mOnSaveInstanceStateHasRun:Z

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    :cond_0
    return-void
.end method

.method public onEvent(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    packed-switch p1, :pswitch_data_0

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onEvent> event out of range, event code = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onEvent> EVENT_SAVE_SUCCESS"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->getSaveFileUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/view/View;->setEnabled(Z)V

    const v3, 0x7f080001

    invoke-static {p0, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->getToast(Landroid/content/Context;I)V

    :cond_0
    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v3, :cond_1

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onEvent> mSetResultAfterSave = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSetResultAfterSave:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSetResultAfterSave:Z

    if-eqz v3, :cond_2

    const/4 v3, -0x1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onEvent> finish"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onEvent> Activity = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Performance test][SoundRecorder] recording save end ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSetResultAfterSave:Z

    goto :goto_1

    :pswitch_1
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onEvent> EVENT_DISCARD_SUCCESS"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorderService;->reset()Z

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onEvent> finish"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onEvent> Activity = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :pswitch_2
    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onEvent> EVENT_STORAGE_MOUNTED"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "error_dialog"

    invoke-direct {p0, v3}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0b002c

    if-ne v1, v0, :cond_1

    invoke-direct {p0, v3, v2}, Lcom/android/soundrecorder/SoundRecorder;->showDialogFragment(ILandroid/os/Bundle;)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    const v1, 0x7f0b002d

    if-ne v1, v0, :cond_2

    const/4 v1, 0x0

    invoke-direct {p0, v1, v2}, Lcom/android/soundrecorder/SoundRecorder;->showDialogFragment(ILandroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0b002e

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArray:[Z

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Z

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectEffectArrayTemp:[Z

    const/4 v1, 0x2

    invoke-direct {p0, v1, v2}, Lcom/android/soundrecorder/SoundRecorder;->showDialogFragment(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onPause> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onPause> Activity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mBackPressed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->stopRecord()Z

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onPause> mService.saveRecord()"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->saveRecord()Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mBackPressed:Z

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onPause> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onPrepareOptionsMenu> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mMenu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateOptionsMenu()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onPrepareOptionsMenu> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onRestoreInstanceState> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->restoreDialogFragment()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->restoreRecordParamsSettings()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onRestoreInstanceState> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onResume> start mRunFromLauncher = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Activity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mOnSaveInstanceStateHasRun:Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-nez v0, :cond_2

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onResume> start service"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onResume> fail to start service"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onResume> bind service"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/soundrecorder/SoundRecorderService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onResume> fail to bind service"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->resetUi()V

    :goto_1
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onResume> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initWhenHaveService()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onSaveInstanceState> start"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mOnSaveInstanceStateHasRun:Z

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v0}, Lcom/android/soundrecorder/SoundRecorderService;->storeRecordParamsSettings()V

    :cond_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onSaveInstanceState> end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStateChanged(I)V
    .locals 7
    .param p1    # I

    const/4 v5, 0x2

    const/4 v6, 0x1

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onStateChanged> change from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v2, :cond_0

    if-ne p1, v5, :cond_2

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->acquireWakeLock()V

    :cond_0
    :goto_0
    iput p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    if-ne v6, v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v2}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v2}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInSecond()I

    move-result v0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    div-int/lit8 v5, v0, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    rem-int/lit8 v4, v0, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateOptionsMenu()V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->releaseWakeLock()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v4, "SR/SoundRecorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<onStop> start, Activity = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mCurrentState:I

    if-ne v4, v2, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->isCurrentFileWaitToSave()Z

    move-result v4

    if-nez v4, :cond_2

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4, p0}, Lcom/android/soundrecorder/SoundRecorderService;->isListener(Lcom/android/soundrecorder/SoundRecorderService$OnStateChangedListener;)Z

    move-result v0

    const-string v4, "SR/SoundRecorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<onStop> isListener = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v4}, Lcom/android/soundrecorder/SoundRecorderService;->setAllListenerSelf()V

    :cond_0
    const-string v4, "SR/SoundRecorder"

    const-string v5, "<onStop> unbind service"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v4}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsStopService:Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->hideStorageHint()V

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onStop> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method public setSelectedFormat(I)V
    .locals 3
    .param p1    # I

    invoke-static {p1}, Lcom/android/soundrecorder/RecordParamsSetting;->getSelectFormat(I)I

    move-result v0

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setSelectedFormat(I)V

    :cond_0
    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<setSelectedFormat> mSelectedFormat = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setSelectedMode(I)V
    .locals 3
    .param p1    # I

    invoke-static {p1}, Lcom/android/soundrecorder/RecordParamsSetting;->getSelectMode(I)I

    move-result v0

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/SoundRecorderService;->setSelectedMode(I)V

    :cond_0
    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<setSelectedMode> mSelectedMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setTimerTextView()V
    .locals 6

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v2}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInSecond()I

    move-result v0

    :cond_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    div-int/lit8 v5, v0, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    rem-int/lit8 v5, v0, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateTimerView()V
    .locals 12

    const/4 v11, 0x2

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInSecond()I

    move-result v4

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v3

    const/4 v6, 0x4

    if-ne v6, v3, :cond_0

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFileDurationInMillSecond()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    cmp-long v6, v0, v6

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const-wide/16 v7, 0x64

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v9}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentProgressInMillSecond()J

    move-result-wide v9

    mul-long/2addr v7, v9

    div-long/2addr v7, v0

    long-to-int v7, v7

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    new-array v7, v11, [Ljava/lang/Object;

    const/4 v8, 0x0

    div-int/lit8 v9, v4, 0x3c

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    rem-int/lit8 v9, v4, 0x3c

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(I)V

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v6

    if-ne v11, v6, :cond_2

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->getRemainingTime()J

    move-result-wide v6

    long-to-int v2, v6

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v6}, Lcom/android/soundrecorder/SoundRecorderService;->isStorageLower()Z

    move-result v6

    if-eqz v6, :cond_4

    const v6, 0x7f08001b

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/SoundRecorder;->showStorageHint(Ljava/lang/String;)V

    :goto_1
    iget-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    if-nez v6, :cond_1

    const/16 v6, 0x21c

    if-ge v2, v6, :cond_2

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/soundrecorder/SoundRecorder;->updateRemainingTimerView(I)V

    :cond_2
    return-void

    :cond_3
    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->hideStorageHint()V

    goto :goto_1
.end method

.method public updateUi()V
    .locals 5

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUi> start"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentFilePath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<updateUi> mService.getCurrentFilePath() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ""

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    :cond_0
    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<updateUi> mRecordingFileNameTextView.setText : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v2, 0x7f080032

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->hideStorageHint()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mService:Lcom/android/soundrecorder/SoundRecorderService;

    invoke-virtual {v1}, Lcom/android/soundrecorder/SoundRecorderService;->getCurrentState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUi> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUiOnIdleState()V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUiOnPausePlayingState()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUiOnRecordingState()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUiOnPauseRecordingState()V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUiOnPlayingState()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method
