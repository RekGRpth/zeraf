.class public Lcom/mediatek/soundrecorder/ext/ExtensionHelper;
.super Ljava/lang/Object;
.source "ExtensionHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getExtension(Landroid/content/Context;)Lcom/mediatek/soundrecorder/ext/IQualityLevel;
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v7, 0x0

    const-class v5, Lcom/mediatek/soundrecorder/ext/IQualityLevel;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Landroid/content/pm/Signature;

    invoke-static {p0, v5, v6}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, v7}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/soundrecorder/ext/IQualityLevel;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v4, v3

    :goto_1
    return-object v4

    :catch_0
    move-exception v0

    new-instance v3, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;

    invoke-direct {v3}, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;

    invoke-direct {v3}, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;-><init>()V

    :goto_2
    move-object v4, v3

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;

    invoke-direct {v3}, Lcom/mediatek/soundrecorder/ext/DefaultQualityLevel;-><init>()V

    goto :goto_2
.end method
