.class public Lcom/mediatek/settings/CellBroadcastActivity;
.super Lcom/android/phone/TimeConsumingPreferenceActivity;
.source "CellBroadcastActivity.java"


# static fields
.field private static final BUTTON_CB_CHECKBOX_KEY:Ljava/lang/String; = "enable_cellBroadcast"

.field private static final BUTTON_CB_SETTINGS_KEY:Ljava/lang/String; = "cbsettings"

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/CellBroadcastActivity"


# instance fields
.field private mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

.field private mCBSetting:Landroid/preference/Preference;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mSimId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    iput-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iput-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBSetting:Landroid/preference/Preference;

    new-instance v0, Lcom/mediatek/settings/CellBroadcastActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/CellBroadcastActivity$1;-><init>(Lcom/mediatek/settings/CellBroadcastActivity;)V

    iput-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/settings/CellBroadcastActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/CellBroadcastActivity;

    invoke-direct {p0}, Lcom/mediatek/settings/CellBroadcastActivity;->setScreenEnabled()V

    return-void
.end method

.method private setScreenEnabled()V
    .locals 3

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v1, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-eq v2, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f06000a

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "simId"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    const-string v2, "enable_cellBroadcast"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/mediatek/settings/CellBroadcastCheckBox;

    iput-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    const-string v2, "cbsettings"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBSetting:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    invoke-virtual {v2}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f0d00f2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "sub_title_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "sub_title_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget v3, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    invoke-virtual {v2, p0, v4, v3}, Lcom/mediatek/settings/CellBroadcastCheckBox;->init(Lcom/mediatek/phone/TimeConsumingPreferenceListener;ZI)V

    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SIM_INFO_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_2
    return-void

    :cond_3
    const v2, 0x7f0d00f3

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mCBSetting:Landroid/preference/Preference;

    if-ne p2, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "simId"

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
