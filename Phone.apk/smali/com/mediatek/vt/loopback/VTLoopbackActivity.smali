.class public Lcom/mediatek/vt/loopback/VTLoopbackActivity;
.super Landroid/app/Activity;
.source "VTLoopbackActivity.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "VTLoopbackActivity"


# instance fields
.field private mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "VTLoopbackActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v1, "VTLoopbackActivity:onCreate"

    invoke-virtual {p0, v1}, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->log(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f040042

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    new-instance v1, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {v1, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    const v1, 0x7f08011f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    const v1, 0x7f08010e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v1, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTLoopBackInstance(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->initVTInCallScreen()V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/settings/VTSettingUtils;->updateVTSettingState(I)V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/settings/VTSettingUtils;->updateVTEngineerModeValues()V

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->internalAnswerVTCallPre()V

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    sget-object v2, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_OPEN:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTScreenMode(Lcom/android/phone/Constants$VTScreenMode;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "VTLoopbackActivity:onDestroy()"

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->log(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissVTDialogs()V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "VTLoopbackActivity:onResume"

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissVTDialogs()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "VTLoopbackActivity:onStop"

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTLoopbackActivity;->mVTInCallScreenLoopback:Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onStop()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
