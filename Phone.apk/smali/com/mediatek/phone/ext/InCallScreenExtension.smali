.class public Lcom/mediatek/phone/ext/InCallScreenExtension;
.super Ljava/lang/Object;
.source "InCallScreenExtension.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dismissDialogs()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method

.method public handleOnscreenButtonClick(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/app/Activity;Lcom/mediatek/phone/ext/IInCallScreen;Lcom/android/internal/telephony/CallManager;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/mediatek/phone/ext/IInCallScreen;
    .param p4    # Lcom/android/internal/telephony/CallManager;

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    return-void
.end method

.method public onDisconnect(Lcom/android/internal/telephony/Connection;)Z
    .locals 1
    .param p1    # Lcom/android/internal/telephony/Connection;

    const/4 v0, 0x0

    return v0
.end method

.method public onPhoneStateChanged(Lcom/android/internal/telephony/CallManager;)Z
    .locals 1
    .param p1    # Lcom/android/internal/telephony/CallManager;

    const/4 v0, 0x0

    return v0
.end method

.method public setupMenuItems(Landroid/view/Menu;Lcom/mediatek/phone/ext/IInCallControlState;)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Lcom/mediatek/phone/ext/IInCallControlState;

    return-void
.end method

.method public updateScreen(Lcom/android/internal/telephony/CallManager;Z)Z
    .locals 1
    .param p1    # Lcom/android/internal/telephony/CallManager;
    .param p2    # Z

    const/4 v0, 0x0

    return v0
.end method
