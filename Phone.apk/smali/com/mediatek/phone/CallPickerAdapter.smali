.class public Lcom/mediatek/phone/CallPickerAdapter;
.super Landroid/widget/BaseAdapter;
.source "CallPickerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/phone/CallPickerAdapter$1;,
        Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field private mFirstCallerInfoName:Ljava/lang/String;

.field private mFirstSimColor:I

.field mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mOperatorNameFirstCall:Ljava/lang/String;

.field private mOperatorNameSecondCall:Ljava/lang/String;

.field private mSecondCallerInfoName:Ljava/lang/String;

.field private mSecondSimColor:I

.field private mSimDarkBorderMap:[I

.field private mSimIndicatorPaddingLeft:I

.field private mSimIndicatorPaddingRight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimDarkBorderMap:[I

    iput-object p1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/phone/CallPickerAdapter;->mItems:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimIndicatorPaddingLeft:I

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimIndicatorPaddingRight:I

    return-void

    :array_0
    .array-data 4
        0x7f02009b
        0x7f02009e
        0x7f02009c
        0x7f02009f
    .end array-data
.end method

.method private tripHyphen(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_1

    const/16 v3, 0x20

    if-eq v0, v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private updateCallOperatorBackground(Lcom/android/internal/telephony/Call;Landroid/widget/TextView;I)V
    .locals 4
    .param p1    # Lcom/android/internal/telephony/Call;
    .param p2    # Landroid/widget/TextView;
    .param p3    # I

    const/4 v3, 0x0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->isGeminiSupport()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v1, v0, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f02009d

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimIndicatorPaddingLeft:I

    iget v2, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimIndicatorPaddingRight:I

    invoke-virtual {p2, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimDarkBorderMap:[I

    if-eqz v1, :cond_1

    if-ltz p3, :cond_1

    iget-object v1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimDarkBorderMap:[I

    array-length v1, v1

    if-ge p3, v1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSimDarkBorderMap:[I

    aget v1, v1, p3

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f02009f

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public getCallerInfoName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mFirstCallerInfoName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSecondCallerInfoName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getOperatorColor(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mFirstSimColor:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSecondSimColor:I

    goto :goto_0
.end method

.method public getOperatorName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mOperatorNameFirstCall:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/CallPickerAdapter;->mOperatorNameSecondCall:Ljava/lang/String;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v8, 0x0

    move-object v6, p2

    const/4 v3, 0x0

    if-nez v6, :cond_0

    iget-object v7, p0, Lcom/mediatek/phone/CallPickerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    new-instance v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;

    invoke-direct {v3, p0, v8}, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;-><init>(Lcom/mediatek/phone/CallPickerAdapter;Lcom/mediatek/phone/CallPickerAdapter$1;)V

    const v7, 0x7f040009

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f08001b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    const v7, 0x7f08001a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mOperator:Landroid/widget/TextView;

    const v7, 0x7f08001c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;

    iget-object v7, p0, Lcom/mediatek/phone/CallPickerAdapter;->mItems:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/Call;

    invoke-virtual {p0, p1}, Lcom/mediatek/phone/CallPickerAdapter;->getOperatorName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/mediatek/phone/CallPickerAdapter;->getCallerInfoName(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_4

    if-eqz v2, :cond_4

    invoke-direct {p0, v2}, Lcom/mediatek/phone/CallPickerAdapter;->tripHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    if-eqz v5, :cond_3

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mOperator:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/mediatek/phone/CallPickerAdapter;->getOperatorColor(I)I

    move-result v8

    invoke-direct {p0, v1, v7, v8}, Lcom/mediatek/phone/CallPickerAdapter;->updateCallOperatorBackground(Lcom/android/internal/telephony/Call;Landroid/widget/TextView;I)V

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mOperator:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-object v6

    :cond_4
    if-eqz v2, :cond_5

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    if-eqz v0, :cond_2

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, v3, Lcom/mediatek/phone/CallPickerAdapter$ViewHolder;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setCallerInfoName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mFirstCallerInfoName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSecondCallerInfoName:Ljava/lang/String;

    return-void
.end method

.method public setOperatorColor(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mFirstSimColor:I

    iput p2, p0, Lcom/mediatek/phone/CallPickerAdapter;->mSecondSimColor:I

    return-void
.end method

.method public setOperatorName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/phone/CallPickerAdapter;->mOperatorNameFirstCall:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/phone/CallPickerAdapter;->mOperatorNameSecondCall:Ljava/lang/String;

    return-void
.end method
