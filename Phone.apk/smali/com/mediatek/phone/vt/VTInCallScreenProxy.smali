.class public Lcom/mediatek/phone/vt/VTInCallScreenProxy;
.super Ljava/lang/Object;
.source "VTInCallScreenProxy.java"

# interfaces
.implements Lcom/mediatek/phone/vt/IVTInCallScreen;


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "VTInCallScreenProxy"


# instance fields
.field private mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

.field private mInCallScreen:Lcom/android/phone/InCallScreen;

.field private mIsInflate:Z

.field private mIsLocaleChanged:Z

.field private mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;


# direct methods
.method public constructor <init>(Lcom/android/phone/InCallScreen;Lcom/android/phone/DTMFTwelveKeyDialer;)V
    .locals 0
    .param p1    # Lcom/android/phone/InCallScreen;
    .param p2    # Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mInCallScreen:Lcom/android/phone/InCallScreen;

    iput-object p2, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    return-void
.end method

.method private isInflated()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsInflate:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "VTInCallScreenProxy"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public dismissVTDialogs()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->dismissVTDialogs()V

    goto :goto_0
.end method

.method public getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    goto :goto_0
.end method

.method public handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public initCommonVTState()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->initCommonVTState()V

    goto :goto_0
.end method

.method public initDialingSuccessVTState()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->initDialingSuccessVTState()V

    goto :goto_0
.end method

.method public initDialingVTState()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->initDialingVTState()V

    goto :goto_0
.end method

.method public initVTInCallScreen()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mInCallScreen:Lcom/android/phone/InCallScreen;

    if-nez v1, :cond_0

    const-string v1, "mInCallScreen is null, just return"

    invoke-direct {p0, v1}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsInflate:Z

    if-eqz v1, :cond_1

    const-string v1, "already inflate, just return"

    invoke-direct {p0, v1}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mInCallScreen:Lcom/android/phone/InCallScreen;

    const v2, 0x7f080072

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mInCallScreen:Lcom/android/phone/InCallScreen;

    const v2, 0x7f08010e

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mediatek/phone/vt/VTInCallScreen;

    iput-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mInCallScreen:Lcom/android/phone/InCallScreen;

    invoke-virtual {v1, v2}, Lcom/mediatek/phone/vt/VTInCallScreen;->setInCallScreenInstance(Lcom/android/phone/InCallScreen;)V

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-virtual {v1, v2}, Lcom/mediatek/phone/vt/VTInCallScreen;->setDialer(Lcom/android/phone/DTMFTwelveKeyDialer;)V

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v1}, Lcom/mediatek/phone/vt/VTInCallScreen;->initVTInCallScreen()V

    iget-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsLocaleChanged:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v1}, Lcom/mediatek/phone/vt/VTInCallScreen;->notifyLocaleChange()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsLocaleChanged:Z

    :cond_2
    iget-object v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v1}, Lcom/mediatek/phone/vt/VTInCallScreen;->registerForVTPhoneStates()V

    iput-boolean v3, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsInflate:Z

    invoke-static {}, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->getInstance()Lcom/mediatek/phone/vt/VTInCallScreenFlags;

    move-result-object v1

    iput-boolean v3, v1, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTIsInflate:Z

    goto :goto_0
.end method

.method public internalAnswerVTCallPre()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->initVTInCallScreen()V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "inflate failed"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->internalAnswerVTCallPre()V

    goto :goto_0
.end method

.method public notifyLocaleChange()V
    .locals 1

    const-string v0, "NotifyLocaleChange"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsLocaleChanged:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->notifyLocaleChange()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->onDestroy()V

    goto :goto_0
.end method

.method public onDisconnectVT(Lcom/android/internal/telephony/Connection;IZ)Z
    .locals 1
    .param p1    # Lcom/android/internal/telephony/Connection;
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/phone/vt/VTInCallScreen;->onDisconnectVT(Lcom/android/internal/telephony/Connection;IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v0, "onKeyDown"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/phone/vt/VTInCallScreen;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const-string v0, "onPrepareOptionsMenu"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onReceiveVTManagerStartCounter()V
    .locals 1

    const-string v0, "onReceiveVTManagerStartCounter"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->onReceiveVTManagerStartCounter()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    const-string v0, "onStop"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->onStop()V

    goto :goto_0
.end method

.method public refreshAudioModePopup()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->refreshAudioModePopup()V

    goto :goto_0
.end method

.method public resetVTFlags()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->resetVTFlags()V

    goto :goto_0
.end method

.method public setVTScreenMode(Lcom/android/phone/Constants$VTScreenMode;)V
    .locals 1
    .param p1    # Lcom/android/phone/Constants$VTScreenMode;

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->setVTScreenMode(Lcom/android/phone/Constants$VTScreenMode;)V

    goto :goto_0
.end method

.method public setVTVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mIsInflate:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->setVTVisible(Z)V

    goto :goto_0
.end method

.method public setupMenuItems(Landroid/view/Menu;)V
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->setupMenuItems(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public showReCallDialog(ILjava/lang/String;I)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const-string v0, "showReCallDialog"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/phone/vt/VTInCallScreen;->showReCallDialog(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public stopRecord()V
    .locals 1

    const-string v0, "stopRecord"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreen;->stopRecord()V

    goto :goto_0
.end method

.method public updateElapsedTime(J)V
    .locals 1
    .param p1    # J

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/phone/vt/VTInCallScreen;->updateElapsedTime(J)V

    goto :goto_0
.end method

.method public updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V
    .locals 1
    .param p1    # Lcom/android/phone/Constants$VTScreenMode;

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    goto :goto_0
.end method

.method public updateVideoCallRecordState(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->isInflated()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenProxy;->mVTInCallScreen:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/vt/VTInCallScreen;->updateVideoCallRecordState(I)V

    goto :goto_0
.end method
