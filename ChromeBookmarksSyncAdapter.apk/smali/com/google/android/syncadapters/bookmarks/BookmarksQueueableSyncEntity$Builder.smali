.class public Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
.super Ljava/lang/Object;
.source "BookmarksQueueableSyncEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-direct {v0}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    return-void
.end method

.method private getIntegerFromCursor(Landroid/database/Cursor;ILjava/lang/Integer;)Ljava/lang/Integer;
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Ljava/lang/Integer;

    :try_start_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p3

    :cond_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Ljava/lang/Long;

    :try_start_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p3

    :cond_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    return-object v0
.end method

.method public clear()Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    # invokes: Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->wipeAllFields()V
    invoke-static {v0}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->access$000(Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;)V

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsEmpty:Z

    return-object p0
.end method

.method public parse(Landroid/database/Cursor;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    # invokes: Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->wipeAllFields()V
    invoke-static {v2}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->access$000(Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsEmpty:Z

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_TITLE:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_URL:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_FAVICON:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mFavicon:[B

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_LOCAL_ID:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalId:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_LOCAL_PARENT_ID:I

    const/4 v4, 0x0

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalParentId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v2, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_IS_FOLDER:I

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {p0, p1, v2, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getIntegerFromCursor(Landroid/database/Cursor;ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    iput-boolean v2, v3, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsFolder:Z

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_POSITION:I

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mPosition:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_LOCAL_VERSION:I

    const/4 v4, 0x0

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalVersion:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v2, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_IS_DELETED:I

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {p0, p1, v2, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getIntegerFromCursor(Landroid/database/Cursor;ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    iput-boolean v2, v3, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsDeleted:Z

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v3, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_DIRTY:I

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getIntegerFromCursor(Landroid/database/Cursor;ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    iput-boolean v0, v2, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsDirty:Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_REMOTE_PARENT_ID:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteParentId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_REMOTE_VERSION:I

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteVersion:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_SERVER_UNIQUE_TAG:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mServerUniqueTag:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_CLIENT_UNIQUE_TAG:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mClientUniqueTag:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_DATE_CREATED:I

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateCreated:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_DATE_MODIFIED:I

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateModified:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_INSERT_AFTER:I

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->getLongFromCursor(Landroid/database/Cursor;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalInsertAfter:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_REMOTE_INSERT_AFTER:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteInsertAfter:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    sget v1, Lcom/google/android/syncadapters/bookmarks/BookmarksSyncAdapter;->PROJ_REMOTE_ID:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object p0

    :cond_1
    move v2, v1

    goto/16 :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public parse(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 9
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    # invokes: Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->wipeAllFields()V
    invoke-static {v5}, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->access$000(Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;)V

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean v8, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsEmpty:Z

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getSpecifics()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    move-result-object v4

    sget-object v5, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks;->bookmark:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    invoke-virtual {v4, v5}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->hasExtension(Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;)Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks;->bookmark:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    invoke-virtual {v4, v5}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->getExtension(Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;

    invoke-virtual {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;->hasUrl()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;->getUrl()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mUrl:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;->hasFavicon()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v6, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;->hasFavicon()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Bookmarks$BookmarkSpecifics;->getFavicon()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v5

    :goto_0
    iput-object v5, v6, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mFavicon:[B

    :cond_1
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasName()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mTitle:Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasOriginatorCacheGuid()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getOriginatorCacheGuid()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mClientGuid:Ljava/lang/String;

    :cond_3
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getClientDefinedUniqueTag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object v1, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mClientUniqueTag:Ljava/lang/String;

    :cond_4
    :try_start_0
    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getOriginatorClientItemId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalId:Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasIdString()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getIdString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteId:Ljava/lang/String;

    :cond_5
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasParentIdString()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getParentIdString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteParentId:Ljava/lang/String;

    :cond_6
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getServerDefinedUniqueTag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object v3, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mServerUniqueTag:Ljava/lang/String;

    :cond_7
    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getDeleted()Z

    move-result v6

    iput-boolean v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsDeleted:Z

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean v8, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsEmpty:Z

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getFolder()Z

    move-result v6

    iput-boolean v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsFolder:Z

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getPositionInParent()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mPosition:Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getVersion()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteVersion:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasCtime()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getCtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateCreated:Ljava/lang/Long;

    :cond_8
    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->hasMtime()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-virtual {p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;->getMtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateModified:Ljava/lang/Long;

    :cond_9
    return-object p0

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    iget-object v5, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalId:Ljava/lang/Long;

    goto/16 :goto_1
.end method

.method public setClientGuid(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mClientGuid:Ljava/lang/String;

    return-object p0
.end method

.method public setClientUniqueTag(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mClientUniqueTag:Ljava/lang/String;

    return-object p0
.end method

.method public setDateCreated(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateCreated:Ljava/lang/Long;

    return-object p0
.end method

.method public setDateModified(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mDateModified:Ljava/lang/Long;

    return-object p0
.end method

.method public setFavicon([B)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mFavicon:[B

    return-object p0
.end method

.method public setIsDeleted(Z)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsDeleted:Z

    return-object p0
.end method

.method public setIsDirty(Z)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsDirty:Z

    return-object p0
.end method

.method public setIsEmpty(Z)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsEmpty:Z

    return-object p0
.end method

.method public setIsFolder(Z)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-boolean p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mIsFolder:Z

    return-object p0
.end method

.method public setLocalId(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalId:Ljava/lang/Long;

    return-object p0
.end method

.method public setLocalInsertAfter(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalInsertAfter:Ljava/lang/Long;

    return-object p0
.end method

.method public setLocalParentId(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalParentId:Ljava/lang/Long;

    return-object p0
.end method

.method public setLocalVersion(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mLocalVersion:Ljava/lang/Long;

    return-object p0
.end method

.method public setPosition(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mPosition:Ljava/lang/Long;

    return-object p0
.end method

.method public setRemoteId(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteId:Ljava/lang/String;

    return-object p0
.end method

.method public setRemoteInsertAfter(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteInsertAfter:Ljava/lang/String;

    return-object p0
.end method

.method public setRemoteParentId(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteParentId:Ljava/lang/String;

    return-object p0
.end method

.method public setRemoteVersion(J)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mRemoteVersion:Ljava/lang/Long;

    return-object p0
.end method

.method public setServerUniqueTag(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mServerUniqueTag:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mTitle:Ljava/lang/String;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity$Builder;->mEntity:Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;

    iput-object p1, v0, Lcom/google/android/syncadapters/bookmarks/BookmarksQueueableSyncEntity;->mUrl:Ljava/lang/String;

    return-object p0
.end method
