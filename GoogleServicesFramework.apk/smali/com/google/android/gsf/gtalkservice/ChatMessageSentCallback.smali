.class public Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;
.super Ljava/lang/Object;
.source "ChatMessageSentCallback.java"

# interfaces
.implements Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbacks;


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final sUris:[Landroid/net/Uri;

.field private static final sValues:Landroid/content/ContentValues;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Lcom/google/android/gsf/TalkContract$Messages;->CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gsf/TalkContract$Messages;->OTR_MESSAGES_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sUris:[Landroid/net/Uri;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "consolidation_key"

    aput-object v1, v0, v3

    const-string v1, "send_status"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->PROJECTION:[Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sValues:Landroid/content/ContentValues;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    sget-object v0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sValues:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    sget-object v0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sValues:Landroid/content/ContentValues;

    const-string v1, "send_status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method private onMessageSentInternal(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V
    .locals 12
    .param p1    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;->getPacketId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v10, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sUris:[Landroid/net/Uri;

    array-length v2, v2

    if-ge v10, v2, :cond_1

    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sUris:[Landroid/net/Uri;

    aget-object v1, v2, v10

    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sValues:Landroid/content/ContentValues;

    const-string v3, "packet_id=?"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_6

    const/4 v2, 0x1

    if-eq v8, v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "expected 0 or 1, not "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->PROJECTION:[Ljava/lang/String;

    const-string v3, "packet_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_2

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v2, 0x0

    aput-object v9, v4, v2

    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->PROJECTION:[Ljava/lang/String;

    const-string v3, "consolidation_key=? and send_status=0"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    if-nez v7, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    aput-object v9, v4, v2

    sget-object v2, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->sValues:Landroid/content/ContentValues;

    const-string v3, "_id=?"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    const/4 v2, 0x1

    if-eq v11, v2, :cond_4

    const-string v2, "GTalkService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "marking consolidation row "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " as sent >>> "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " (should be 1)"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v7, 0x0

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    const/4 v7, 0x0

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    throw v2
.end method


# virtual methods
.method public onMessageSent(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->onMessageSentInternal(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->notifyActiveChatSessionsOfChange(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V

    return-void
.end method

.method public onMessagesSent(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->onMessageSentInternal(Lcom/google/android/gsf/gtalkservice/rmq/Rmq2Manager$MessageQueueCallbackEntry;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/ChatMessageSentCallback;->mService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v2, p1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->notifyActiveChatSessionsOfChanges(Ljava/util/ArrayList;)V

    return-void
.end method
