.class public Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;
.super Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;
.source "GTalkImSession.java"

# interfaces
.implements Lorg/jivesoftware/smack/PacketListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession$1;
    }
.end annotation


# instance fields
.field private mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

.field private mLock:Ljava/lang/Object;

.field private mSharedStatusPacketFilter:Lorg/jivesoftware/smack/filter/PacketFilter;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;-><init>(Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private isLoginAttempted()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getIntendedPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    monitor-exit p0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GTalkImSession] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private saveStatusList(Ljava/util/List;Lcom/google/android/gtalkservice/Presence;Lcom/google/android/gtalkservice/Presence$Show;)V
    .locals 3
    .param p2    # Lcom/google/android/gtalkservice/Presence;
    .param p3    # Lcom/google/android/gtalkservice/Presence$Show;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gtalkservice/Presence;",
            "Lcom/google/android/gtalkservice/Presence$Show;",
            ")V"
        }
    .end annotation

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, p3, v2}, Lcom/google/android/gtalkservice/Presence;->setStatus(Lcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private translateSharedPresence(Lcom/google/android/gtalkservice/Presence;)Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;-><init>(Lcom/google/android/gtalkservice/Presence;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setTo(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public addContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->createContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    return-void
.end method

.method public addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    .locals 4
    .param p1    # Lcom/google/android/gtalkservice/IGroupChatInvitationListener;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in addGroupChatInvitationListener"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public blockContact(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->blockContact(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v2, "GTalkService"

    const-string v3, "exception in blockContact"

    invoke-static {v2, v3, v0}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public clearContactFlags(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->clearContactFlags(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v2, "GTalkService"

    const-string v3, "exception in clearContactFlags"

    invoke-static {v2, v3, v0}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public closeAllChatSessions()V
    .locals 11

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->closeChatSessions(JJJJZ)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v10

    const-string v1, "GTalkService"

    const-string v2, "exception in closeAllChatSessions"

    invoke-static {v1, v2, v10}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
.end method

.method public createGroupChatSession(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->createGroupChatSession(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in createGroupChatSession"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public declineGroupChatInvitation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->declineGroupChatInvitation(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in declineGroupChatInvitation"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public editContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->editContactNickname(JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getAccountId()J
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getUsername()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "GTalkService"

    const-string v2, "GTalkImSession.getUsername got null"

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccount()Lcom/google/android/gsf/gtalkservice/Account;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/Account;->getUsername()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public hideContact(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->hideContact(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v2, "GTalkService"

    const-string v3, "exception in hideContact"

    invoke-static {v2, v3, v0}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public initConnection(Lorg/jivesoftware/smack/XMPPConnection;)V
    .locals 4

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mSharedStatusPacketFilter:Lorg/jivesoftware/smack/filter/PacketFilter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getGTalkConnectionAccountIdFilter()Lorg/jivesoftware/smack/filter/AccountIdFilter;

    move-result-object v0

    new-instance v1, Lorg/jivesoftware/smack/filter/AndFilter;

    new-instance v2, Lorg/jivesoftware/smack/filter/PacketTypeFilter;

    const-class v3, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    invoke-direct {v2, v3}, Lorg/jivesoftware/smack/filter/PacketTypeFilter;-><init>(Ljava/lang/Class;)V

    invoke-direct {v1, v2, v0}, Lorg/jivesoftware/smack/filter/AndFilter;-><init>(Lorg/jivesoftware/smack/filter/PacketFilter;Lorg/jivesoftware/smack/filter/PacketFilter;)V

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mSharedStatusPacketFilter:Lorg/jivesoftware/smack/filter/PacketFilter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mSharedStatusPacketFilter:Lorg/jivesoftware/smack/filter/PacketFilter;

    invoke-virtual {p1, p0, v0}, Lorg/jivesoftware/smack/XMPPConnection;->addPacketListener(Lorg/jivesoftware/smack/PacketListener;Lorg/jivesoftware/smack/filter/PacketFilter;)V

    :cond_1
    return-void
.end method

.method public inviteContactsToGroupchat(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->inviteContactsToGroupchat(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in inviteContactsToGroupchat"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public joinGroupChatSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->joinGroupChatSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in joinGroupChatSession"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public login(Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v9, 0x1

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v6

    new-instance v7, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;-><init>(I)V

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->addConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->setLastLoginState(Z)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getConnectionError()Lcom/google/android/gtalkservice/ConnectionError;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gtalkservice/ConnectionError;->getError()I

    move-result v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "login: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v7

    invoke-static {v7, v8, p1}, Lcom/google/android/gsf/gtalkservice/Log;->sanitizeUsername(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", hostConn.connState = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Lcom/google/android/gtalkservice/ConnectionState;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", hostConn.error="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Lcom/google/android/gtalkservice/ConnectionError;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->log(Ljava/lang/String;)V

    const/4 v5, 0x0

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getGTalkService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getMainEndpoint()Lcom/google/android/gsf/gtalkservice/AndroidEndpoint;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/Endpoint;->sendHeartbeatToServer()Z

    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->asyncGoOnlineForGoogleTalk()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->onLoggedIn()V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v6

    invoke-virtual {v6, v1, v0, v9}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->setConnectionStateAndError(IIZ)V

    :cond_2
    :goto_1
    return-void

    :pswitch_1
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->requestToRefreshAuthToken()V

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    :pswitch_2
    :try_start_2
    invoke-virtual {v3}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getGTalkService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getReconnectManager()Lcom/google/android/gsf/gtalkservice/ReconnectManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/gtalkservice/ReconnectManager;->retryConnection(Z)V

    const/4 v4, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :pswitch_4
    const-string v6, "GTalkService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "login: already ONLINE, no-op"

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->log(Ljava/lang/String;)V

    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public logout()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    new-instance v2, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/google/android/gsf/gtalkservice/Endpoint$SpecialConnectionEvent;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->addConnectionEvent(Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionEvent;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->setLastLoginState(Z)V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->isLoginAttempted()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GTalkService"

    const-string v2, "[GTalkImSession] logout: not logged in"

    invoke-static {v1, v2}, Lcom/google/android/gsf/gtalkservice/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "GTalkService"

    invoke-static {v1, v5}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "logout: send unavailable presence"

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->log(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/google/android/gtalkservice/Presence;

    sget-object v2, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v3, 0x0

    invoke-direct {v1, v4, v2, v3, v4}, Lcom/google/android/gtalkservice/Presence;-><init>(ZLcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->setAndSendPresence(Lcom/google/android/gtalkservice/Presence;)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/ConnectionState;->getState()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->setConnectionState(IZ)Z

    move-result v0

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->notifyConnectionListeners()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->onLoggedOut()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method protected onLoggedIn()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->onLoggedIn()V

    return-void
.end method

.method protected onLoggedOut()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->onLoggedOut()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->setRosterFetched(Z)V

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->onLoggedOut()V

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getStatusBarNotifier()Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getGTalkConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSettingsMap()Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->dismissNotificationsForAccount(JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    return-void
.end method

.method public pinContact(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->pinContact(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v2, "GTalkService"

    const-string v3, "exception in pinContact"

    invoke-static {v2, v3, v0}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public processPacket(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 13
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v9, 0x1

    instance-of v10, p1, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    if-eqz v10, :cond_8

    move-object v4, p1

    check-cast v4, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    new-instance v2, Lcom/google/android/gtalkservice/Presence;

    sget-object v10, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gtalkservice/Presence;->getCapabilities()I

    move-result v12

    invoke-direct {v2, v9, v10, v11, v12}, Lcom/google/android/gtalkservice/Presence;-><init>(ZLcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;I)V

    const/4 v3, 0x0

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusMax()I

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v2, v8}, Lcom/google/android/gtalkservice/Presence;->setStatusMax(I)V

    const/4 v3, 0x1

    :cond_0
    :goto_0
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusListMax()I

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v2, v7}, Lcom/google/android/gtalkservice/Presence;->setStatusListMax(I)V

    :cond_1
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusListContentsMax()I

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v2, v6}, Lcom/google/android/gtalkservice/Presence;->setStatusListContentsMax(I)V

    :cond_2
    :goto_2
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->hasStatusMinVersion()Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusMinVersion()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_c

    :goto_3
    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setAllowInvisibility(Z)V

    :cond_3
    :goto_4
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getDefaultStatusList()Ljava/util/List;

    move-result-object v9

    sget-object v10, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-direct {p0, v9, v2, v10}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->saveStatusList(Ljava/util/List;Lcom/google/android/gtalkservice/Presence;Lcom/google/android/gtalkservice/Presence$Show;)V

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getDndStatusList()Ljava/util/List;

    move-result-object v9

    sget-object v10, Lcom/google/android/gtalkservice/Presence$Show;->DND:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-direct {p0, v9, v2, v10}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->saveStatusList(Ljava/util/List;Lcom/google/android/gtalkservice/Presence;Lcom/google/android/gtalkservice/Presence$Show;)V

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getShow()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_e

    const-string v9, "dnd"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    sget-object v9, Lcom/google/android/gtalkservice/Presence$Show;->DND:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setShow(Lcom/google/android/gtalkservice/Presence$Show;)V

    :goto_5
    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatus()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setStatus(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->isInvisible()Z

    move-result v9

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setInvisible(Z)Z

    iget-object v9, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->getCapabilities()I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setCapabilities(I)V

    :cond_4
    if-eqz v3, :cond_5

    new-instance v9, Lcom/google/android/gtalkservice/Presence;

    invoke-direct {v9, v2}, Lcom/google/android/gtalkservice/Presence;-><init>(Lcom/google/android/gtalkservice/Presence;)V

    iput-object v9, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getIntendedPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v1

    sget-object v9, Lcom/google/android/gtalkservice/Presence$Show;->AWAY:Lcom/google/android/gtalkservice/Presence$Show;

    if-eq v1, v9, :cond_6

    sget-object v9, Lcom/google/android/gtalkservice/Presence$Show;->EXTENDED_AWAY:Lcom/google/android/gtalkservice/Presence$Show;

    if-ne v1, v9, :cond_f

    :cond_6
    sget-boolean v9, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    if-eqz v9, :cond_7

    const-string v9, "User is IDLE. stash the new shared presence away for later"

    invoke-direct {p0, v9}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->log(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->setSharedPresence(Lcom/google/android/gtalkservice/Presence;)V

    :cond_8
    :goto_6
    return-void

    :cond_9
    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v10}, Lcom/google/android/gtalkservice/Presence;->getStatusMax()I

    move-result v10

    invoke-virtual {v2, v10}, Lcom/google/android/gtalkservice/Presence;->setStatusMax(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v10}, Lcom/google/android/gtalkservice/Presence;->getStatusListMax()I

    move-result v10

    invoke-virtual {v2, v10}, Lcom/google/android/gtalkservice/Presence;->setStatusListMax(I)V

    goto/16 :goto_1

    :cond_b
    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v10}, Lcom/google/android/gtalkservice/Presence;->getStatusListContentsMax()I

    move-result v10

    invoke-virtual {v2, v10}, Lcom/google/android/gtalkservice/Presence;->setStatusListContentsMax(I)V

    goto/16 :goto_2

    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_3

    :cond_d
    iget-object v9, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->mDefaultPresence:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->allowInvisibility()Z

    move-result v9

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setAllowInvisibility(Z)V

    goto/16 :goto_4

    :cond_e
    sget-object v9, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-virtual {v2, v9}, Lcom/google/android/gtalkservice/Presence;->setShow(Lcom/google/android/gtalkservice/Presence$Show;)V

    goto/16 :goto_5

    :cond_f
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->setPresence(Lcom/google/android/gtalkservice/Presence;)Z

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnectionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->notifySelfPresenceChanged()Z

    goto :goto_6
.end method

.method public pruneOldChatSessions(JJJZ)V
    .locals 10
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # Z

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v1

    move-wide v3, p1

    move-wide v5, p3

    move-wide v7, p5

    move/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->closeChatSessions(JJJJZ)V

    return-void
.end method

.method public removeContact(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getAccountId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->removeContact(JLjava/lang/String;)V

    return-void
.end method

.method public removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    .locals 4
    .param p1    # Lcom/google/android/gtalkservice/IGroupChatInvitationListener;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getSessionContext()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "GTalkService"

    const-string v3, "exception in removeGroupChatInvitationListener"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected translatePresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;
    .locals 4
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isInvisible()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->translateSharedPresence(Lcom/google/android/gtalkservice/Presence;)Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    move-result-object v0

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->translateXmppPresence(Lcom/google/android/gtalkservice/Presence;)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession$1;->$SwitchMap$com$google$android$gtalkservice$Presence$Show:[I

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkImSession;->translateSharedPresence(Lcom/google/android/gtalkservice/Presence;)Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
