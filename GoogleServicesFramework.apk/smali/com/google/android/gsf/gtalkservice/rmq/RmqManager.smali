.class public Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;
.super Ljava/lang/Object;
.source "RmqManager.java"


# instance fields
.field private mAckingInterval:I

.field private mNumPacketsReceivedSinceLastAck:I

.field private mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

.field private final mResendQueue:Ljava/util/LinkedList;

.field private mRmq:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

.field private mRmqAckLock:Ljava/lang/Object;

.field private mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/RmqServer2DeviceIdStore;


# direct methods
.method static synthetic access$000(Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->resendPackets()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Rmq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RmqMgr] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private processRmqAck(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 10
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const-wide/16 v8, -0x1

    instance-of v7, p1, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;

    if-eqz v7, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;->getType()Lorg/jivesoftware/smack/packet/IQ$Type;

    move-result-object v6

    sget-object v7, Lorg/jivesoftware/smack/packet/IQ$Type;->SET:Lorg/jivesoftware/smack/packet/IQ$Type;

    if-ne v6, v7, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;->getAckId()J

    move-result-wide v1

    cmp-long v7, v1, v8

    if-eqz v7, :cond_1

    sget-boolean v7, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GOT RmqAck "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mRmq:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v7, v1, v2}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->removeMessagesUntil(J)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    instance-of v7, p1, Lcom/google/android/gsf/gtalkservice/extensions/RmqLastIdReceived;

    if-eqz v7, :cond_1

    move-object v5, p1

    check-cast v5, Lcom/google/android/gsf/gtalkservice/extensions/RmqLastIdReceived;

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/extensions/RmqLastIdReceived;->getType()Lorg/jivesoftware/smack/packet/IQ$Type;

    move-result-object v6

    sget-object v7, Lorg/jivesoftware/smack/packet/IQ$Type;->SET:Lorg/jivesoftware/smack/packet/IQ$Type;

    if-ne v6, v7, :cond_1

    invoke-virtual {v5}, Lcom/google/android/gsf/gtalkservice/extensions/RmqLastIdReceived;->getLastId()J

    move-result-wide v3

    cmp-long v7, v3, v8

    if-eqz v7, :cond_1

    sget-boolean v7, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GOT RmqLastIdReceived "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_3
    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mRmq:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v7, v3, v4}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->removeMessagesUntil(J)V

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->resendRmqPackets()V

    goto :goto_0
.end method

.method private processRmqId(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 6
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getRmqId()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v3, p1, Lorg/jivesoftware/smack/packet/IQ;

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getError()Lorg/jivesoftware/smack/packet/XMPPError;

    move-result-object v3

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processS2d: skipping error IQ packet with rmqId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processS2dMessage: rmq-id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->setLastReceivedRmqIdFromServer(J)V

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mRmqAckLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mNumPacketsReceivedSinceLastAck:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mNumPacketsReceivedSinceLastAck:I

    iget v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mNumPacketsReceivedSinceLastAck:I

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->getAckingInterval()I

    move-result v5

    if-lt v3, v5, :cond_5

    const/4 v0, 0x1

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mNumPacketsReceivedSinceLastAck:I

    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rmqIdReceived: mNumPacketsReceivedSinceLastAck="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mNumPacketsReceivedSinceLastAck:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0, v1, v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->sendRmqAck(J)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private resendPackets()V
    .locals 5

    :goto_0
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mResendQueue:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_0

    monitor-exit v2

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/jivesoftware/smack/packet/Packet;

    sget-boolean v1, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resend packet "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/jivesoftware/smack/packet/Packet;->getRmqId()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    invoke-interface {v1, v0}, Lcom/google/android/gsf/gtalkservice/PacketSender;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resendRmqPackets()V
    .locals 8

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mRmq:Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;

    invoke-interface {v4}, Lcom/google/android/gsf/gtalkservice/rmq/ReliableMessageQueue;->getRmq1Packets()Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    const-string v4, "GTalkService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "no rmq packets to resend"

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mResendQueue:Ljava/util/LinkedList;

    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    :try_start_2
    invoke-virtual {v2, v1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->getPacketAt(I)Lorg/jivesoftware/smack/packet/Packet;

    move-result-object v3

    sget-boolean v4, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resendRmqPackets: add packet "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/jivesoftware/smack/packet/Packet;->getRmqId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " to queue"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->log(Ljava/lang/String;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mResendQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager$1;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager$1;-><init>(Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v4

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;->close()V

    throw v4
.end method

.method private sendRmqAck(J)V
    .locals 2
    .param p1    # J

    new-instance v0, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;-><init>()V

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/extensions/RmqAck;->setAckId(J)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mPacketSender:Lcom/google/android/gsf/gtalkservice/PacketSender;

    invoke-interface {v1, v0}, Lcom/google/android/gsf/gtalkservice/PacketSender;->sendPacketOverMcsConnection(Lorg/jivesoftware/smack/packet/Packet;)Z

    return-void
.end method

.method private setLastReceivedRmqIdFromServer(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mS2dIdStore:Lcom/google/android/gsf/gtalkservice/rmq/RmqServer2DeviceIdStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqServer2DeviceIdStore;->setLastReceivedRmqIdFromServer(J)V

    return-void
.end method


# virtual methods
.method public getAckingInterval()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->mAckingInterval:I

    return v0
.end method

.method public processS2dMessage(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->processRmqId(Lorg/jivesoftware/smack/packet/Packet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/rmq/RmqManager;->processRmqAck(Lorg/jivesoftware/smack/packet/Packet;)V

    return-void
.end method
