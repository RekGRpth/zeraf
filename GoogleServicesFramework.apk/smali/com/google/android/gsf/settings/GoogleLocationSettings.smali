.class public Lcom/google/android/gsf/settings/GoogleLocationSettings;
.super Landroid/app/Fragment;
.source "GoogleLocationSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/settings/GoogleLocationSettings$LocationSettingsChangedListener;,
        Lcom/google/android/gsf/settings/GoogleLocationSettings$Activity;
    }
.end annotation


# instance fields
.field private mAllowHistory:Landroid/widget/CheckBox;

.field mAllowHistoryChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mAllowLocation:Landroid/widget/Switch;

.field private mAllowLocationSummary:Landroid/widget/TextView;

.field private mAllowLocationTitle:Landroid/widget/TextView;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogShowing:Z

.field private mErrorMessage:Landroid/widget/TextView;

.field private mGoogleAccounts:[Landroid/accounts/Account;

.field mItemListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field private mLocationAccessButton:Landroid/widget/Button;

.field private mLocationHistoryButton:Landroid/widget/Button;

.field private mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

.field private mLocationHistorySection:Landroid/view/View;

.field private mLocationHistoryState:[I

.field mModifyCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

.field mQueryCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

.field private mTargetHistoryState:[I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/gsf/settings/GoogleLocationSettings$6;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$6;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mItemListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    new-instance v0, Lcom/google/android/gsf/settings/GoogleLocationSettings$7;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$7;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistoryChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/google/android/gsf/settings/GoogleLocationSettings$8;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$8;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mQueryCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    new-instance v0, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$9;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mModifyCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->onAllowLocationChanged(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/settings/GoogleLocationSettings;)[I
    .locals 1
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mTargetHistoryState:[I

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/gsf/settings/GoogleLocationSettings;)Z
    .locals 1
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;

    iget-boolean v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialogShowing:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialogShowing:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-direct {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistoryDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-direct {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->reloadAllowHistoryState()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gsf/settings/GoogleLocationSettings;[I)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # [I

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->applyNewHistoryStates([I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gsf/settings/GoogleLocationSettings;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->setAllowHistoryQuietly(Z)V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/gsf/settings/GoogleLocationSettings;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Landroid/app/Dialog;

    iput-object p1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->onAllowHistoryChanged(Z)V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/gsf/settings/GoogleLocationSettings;[I)[I
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # [I

    iput-object p1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryState:[I

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/gsf/settings/GoogleLocationSettings;I)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/settings/GoogleLocationSettings;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->setErrorText(I)V

    return-void
.end method

.method private applyNewHistoryStates([I)V
    .locals 3
    .param p1    # [I

    const v0, 0x7f0600cd

    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->setErrorText(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

    iget-object v1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mModifyCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gsf/settings/LocationHistoryClient;->modifyLocationHistoryEnabled([Landroid/accounts/Account;[ILcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V

    return-void
.end method

.method static copyPackagePrefixLists(Landroid/content/ContentResolver;Z)V
    .locals 6
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Z

    const-string v4, "locationPackagePrefixBlacklist"

    invoke-static {p0, v4}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getSecureSettingOrEmpty(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "locationPackagePrefixWhitelist"

    invoke-static {p0, v4}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getSecureSettingOrEmpty(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez p1, :cond_2

    const-string v4, "masterLocationPackagePrefixBlacklist"

    invoke-static {p0, v4}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getSecureSettingOrEmpty(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "masterLocationPackagePrefixWhitelist"

    invoke-static {p0, v4}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getSecureSettingOrEmpty(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "locationPackagePrefixBlacklist"

    invoke-static {p0, v4, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "locationPackagePrefixWhitelist"

    invoke-static {p0, v4, v3}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "locationPackagePrefixBlacklist"

    const-string v5, ""

    invoke-static {p0, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "locationPackagePrefixWhitelist"

    const-string v5, ""

    invoke-static {p0, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private getGlobalLocationAccessEnabled()Z
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "location_providers_allowed"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/StringTokenizer;

    const-string v3, ","

    invoke-direct {v1, v0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getSecureSettingOrEmpty(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private initView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0005

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationSummary:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistorySection:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mErrorMessage:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationAccessButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationAccessButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private onAllowHistoryChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistoryDialog()V

    return-void
.end method

.method private onAllowLocationChanged(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettingHelper;->setUseLocationForServices(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->copyPackagePrefixLists(Landroid/content/ContentResolver;Z)V

    return-void
.end method

.method private reloadAllowHistoryState()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

    invoke-virtual {v0}, Lcom/google/android/gsf/settings/LocationHistoryClient;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

    iget-object v1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mQueryCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/settings/LocationHistoryClient;->queryLocationHistoryEnabled([Landroid/accounts/Account;Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V

    const v0, 0x7f0600cc

    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->setErrorText(I)V

    :cond_0
    return-void
.end method

.method private setAllowHistoryQuietly(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistoryChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private setErrorText(I)V
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mErrorMessage:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mErrorMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mErrorMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showAllowHistoryDialog()V
    .locals 7

    const/4 v5, 0x1

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    new-array v0, v4, [Z

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mTargetHistoryState:[I

    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mGoogleAccounts:[Landroid/accounts/Account;

    aget-object v4, v4, v3

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryState:[I

    aget v4, v4, v3

    if-ne v4, v5, :cond_0

    move v4, v5

    :goto_1
    aput-boolean v4, v0, v3

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mTargetHistoryState:[I

    iget-object v6, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryState:[I

    aget v6, v6, v3

    aput v6, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0600c9

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mItemListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    invoke-virtual {v2, v1, v0, v4}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x104000a

    new-instance v5, Lcom/google/android/gsf/settings/GoogleLocationSettings$2;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$2;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/google/android/gsf/settings/GoogleLocationSettings$3;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$3;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/google/android/gsf/settings/GoogleLocationSettings$4;

    invoke-direct {v4, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$4;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    new-instance v5, Lcom/google/android/gsf/settings/GoogleLocationSettings$5;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$5;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method private showAllowHistorySection(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0007

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistorySection:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationAccessButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_2

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_1
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "Activity not available"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.apps.maps"

    const-string v4, "com.google.googlenav.settings.LatitudeSettingsActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    if-eqz p3, :cond_0

    const-string v0, "dialog"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialogShowing:Z

    :cond_0
    const/high16 v0, 0x7f030000

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->initView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialogShowing:Z

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v0, 0x1

    const/16 v4, 0x8

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/gsf/settings/LocationHistoryClient;

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gsf/settings/LocationHistoryClient;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistoryClient:Lcom/google/android/gsf/settings/LocationHistoryClient;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getGlobalLocationAccessEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistorySection:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    invoke-virtual {v2, v4}, Landroid/widget/Switch;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationTitle:Landroid/widget/TextView;

    const v3, 0x7f0600ca

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationSummary:Landroid/widget/TextView;

    const v3, 0x7f0600cb

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gsf/settings/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v0, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    invoke-virtual {v2, v0}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistorySection:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistorySection(Z)V

    if-eqz v0, :cond_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    new-instance v2, Lcom/google/android/gsf/settings/GoogleLocationSettings$1;

    invoke-direct {v2, p0}, Lcom/google/android/gsf/settings/GoogleLocationSettings$1;-><init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mLocationHistorySection:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocation:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationTitle:Landroid/widget/TextView;

    const v3, 0x7f0600c3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowLocationSummary:Landroid/widget/TextView;

    const v3, 0x7f0600c4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialogShowing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "dialog"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method
