.class Lcom/google/android/gsf/checkin/EventLogService$1;
.super Landroid/os/AsyncTask;
.source "EventLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/checkin/EventLogService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/checkin/EventLogService;

.field final synthetic val$parentContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/checkin/EventLogService;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    iput-object p2, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->val$parentContext:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/EventLogService$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    invoke-virtual {v0}, Lcom/google/android/gsf/checkin/EventLogService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    const-string v1, "EventLogService"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gsf/checkin/EventLogService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/checkin/EventLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/DropBoxManager;

    # getter for: Lcom/google/android/gsf/checkin/EventLogService;->sAggregator:Lcom/google/android/gsf/checkin/EventLogAggregator;
    invoke-static {}, Lcom/google/android/gsf/checkin/EventLogService;->access$100()Lcom/google/android/gsf/checkin/EventLogAggregator;

    move-result-object v9

    monitor-enter v9

    :try_start_0
    const-string v0, "lastLog"

    const-wide/16 v1, 0x0

    invoke-interface {v8, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v0, "lastData"

    const-wide/16 v4, 0x0

    invoke-interface {v8, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    :try_start_1
    const-string v0, "EventLogService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Aggregate from "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " (log), "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " (data)"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/google/android/gsf/checkin/EventLogService;->sAggregator:Lcom/google/android/gsf/checkin/EventLogAggregator;
    invoke-static {}, Lcom/google/android/gsf/checkin/EventLogService;->access$100()Lcom/google/android/gsf/checkin/EventLogAggregator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->val$parentContext:Landroid/content/Context;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/checkin/EventLogAggregator;->aggregate(Landroid/content/Context;JJLandroid/os/DropBoxManager;)J

    move-result-wide v0

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "lastLog"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "lastData"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-class v1, Lcom/google/android/gsf/checkin/ServiceDumpSys;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_3
    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "dumpsys:"

    aput-object v3, v0, v2

    invoke-static {v7, v0}, Lcom/google/android/gsf/Gservices;->getStringsByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    const-string v3, "dump.tmp"

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/checkin/EventLogService;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v6, v2}, Lcom/google/android/gsf/checkin/ServiceDumpSys;->dumpServices(Ljava/util/Map;Landroid/os/DropBoxManager;Ljava/io/File;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "EventLogService"

    const-string v2, "Can\'t aggregate logs"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/EventLogService$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gsf/checkin/EventLogService;->mWorkerTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/google/android/gsf/checkin/EventLogService;->access$202(Lcom/google/android/gsf/checkin/EventLogService;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    new-instance v0, Lcom/android/common/OperationScheduler;

    iget-object v1, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    const-string v2, "EventLogService"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/checkin/EventLogService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->onSuccess()V

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    # invokes: Lcom/google/android/gsf/checkin/EventLogService;->scheduleAggregation(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/gsf/checkin/EventLogService;->access$000(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gsf/checkin/EventLogService$1;->this$0:Lcom/google/android/gsf/checkin/EventLogService;

    invoke-virtual {v0}, Lcom/google/android/gsf/checkin/EventLogService;->stopSelf()V

    return-void
.end method
