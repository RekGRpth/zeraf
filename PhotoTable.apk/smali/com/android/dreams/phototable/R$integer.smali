.class public final Lcom/android/dreams/phototable/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dreams/phototable/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final bad_image_skip_limit:I = 0x7f080012

.field public static final carousel_drop_period:I = 0x7f080002

.field public static final drop_deceleration_exponent:I = 0x7f080011

.field public static final fast_drop:I = 0x7f080001

.field public static final flip_duration:I = 0x7f080004

.field public static final image_queue_size:I = 0x7f08000a

.field public static final image_ratio:I = 0x7f080007

.field public static final image_throw_rotatioan:I = 0x7f08000e

.field public static final max_crop_ratio:I = 0x7f08000d

.field public static final max_image_rotation:I = 0x7f080009

.field public static final max_post_albums:I = 0x7f08000c

.field public static final now_drop:I = 0x7f080003

.field public static final num_images_to_preload:I = 0x7f080014

.field public static final recycle_image_pool_size:I = 0x7f080013

.field public static final redeal_count:I = 0x7f080006

.field public static final soft_landing_distance:I = 0x7f080010

.field public static final soft_landing_time:I = 0x7f08000f

.field public static final table_capacity:I = 0x7f080005

.field public static final table_damping:I = 0x7f08000b

.field public static final table_drop_period:I = 0x7f080000

.field public static final table_ratio:I = 0x7f080008


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
