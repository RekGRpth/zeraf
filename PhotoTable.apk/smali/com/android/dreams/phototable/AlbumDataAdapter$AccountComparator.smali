.class public Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;
.super Ljava/lang/Object;
.source "AlbumDataAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dreams/phototable/AlbumDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
        ">;"
    }
.end annotation


# instance fields
.field private final recency:Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;

    invoke-direct {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;->recency:Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/dreams/phototable/PhotoSource$AlbumData;Lcom/android/dreams/phototable/PhotoSource$AlbumData;)I
    .locals 6
    .param p1    # Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    .param p2    # Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    iget-object v4, p1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    iget-object v5, p2, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;->recency:Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;

    invoke-virtual {v4, p1, p2}, Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;->compare(Lcom/android/dreams/phototable/PhotoSource$AlbumData;Lcom/android/dreams/phototable/PhotoSource$AlbumData;)I

    move-result v4

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->getType()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    const/4 v2, 0x1

    const-class v4, Lcom/android/dreams/phototable/LocalSource;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x0

    :cond_1
    const-class v4, Lcom/android/dreams/phototable/LocalSource;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x0

    :cond_2
    const-class v4, Lcom/android/dreams/phototable/StockSource;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x2

    :cond_3
    const-class v4, Lcom/android/dreams/phototable/StockSource;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v2, 0x2

    :cond_4
    if-ne v0, v2, :cond_5

    iget-object v4, p1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    iget-object v5, p2, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    :cond_5
    sub-int v4, v0, v2

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    float-to-int v4, v4

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    check-cast p2, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    invoke-virtual {p0, p1, p2}, Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;->compare(Lcom/android/dreams/phototable/PhotoSource$AlbumData;Lcom/android/dreams/phototable/PhotoSource$AlbumData;)I

    move-result v0

    return v0
.end method
