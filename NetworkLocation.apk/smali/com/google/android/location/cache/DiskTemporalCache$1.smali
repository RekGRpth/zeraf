.class Lcom/google/android/location/cache/DiskTemporalCache$1;
.super Ljava/lang/Object;
.source "DiskTemporalCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/cache/DiskTemporalCache;->lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/cache/DiskTemporalCache;

.field final synthetic val$diskValue:Lcom/google/android/location/data/TemporalObject;

.field final synthetic val$key:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/location/cache/DiskTemporalCache;Ljava/lang/Object;Lcom/google/android/location/data/TemporalObject;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    iput-object p2, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$diskValue:Lcom/google/android/location/data/TemporalObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lcom/google/android/location/cache/TemporalLRUCache;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$200(Lcom/google/android/location/cache/DiskTemporalCache;)Ljava/io/File;

    move-result-object v7

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$diskValue:Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v6}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v1, v7, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$300(Lcom/google/android/location/cache/DiskTemporalCache;)Ljavax/crypto/SecretKey;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherInputStream(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$400(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/data/ProtoFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/location/data/ProtoFactory;->newProto()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, Lcom/google/android/location/data/TemporalObject;

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$400(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/data/ProtoFactory;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/google/android/location/data/ProtoFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$diskValue:Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v7}, Lcom/google/android/location/data/TemporalObject;->getCreationTimeInMillisSinceEpoch()J

    move-result-wide v7

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7, v5}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    const-string v6, "DiskTemporalCache"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Successfully loaded element "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from disk"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v6

    :catchall_1
    move-exception v6

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_0
    move-exception v0

    :try_start_7
    const-string v6, "DiskTemporalCache"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File not on disk even though in disk LRU: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :try_start_8
    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lcom/google/android/location/cache/TemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$500(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v6

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :catchall_3
    move-exception v6

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    throw v6

    :catch_1
    move-exception v0

    :try_start_b
    const-string v6, "DiskTemporalCache"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException while reading element from disk: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lcom/google/android/location/cache/TemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    invoke-static {v6}, Lcom/google/android/location/cache/DiskTemporalCache;->access$500(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/cache/DiskTemporalCache$1;->val$key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_4
    move-exception v6

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :try_start_e
    throw v6
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3
.end method
