.class abstract Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;
.super Ljava/lang/Object;
.source "TemporalLRUCacheSavers.java"

# interfaces
.implements Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/cache/TemporalLRUCacheSavers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "BaseTemporalLRUCacheSaver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final cacheType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field private final creationTimeTag:I

.field private final entryTag:I

.field private final entryType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field private final lastUsedTimeTag:I

.field private final version:I


# direct methods
.method protected constructor <init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Lcom/google/gmm/common/io/protocol/ProtoBufType;III)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBufType;
    .param p3    # Lcom/google/gmm/common/io/protocol/ProtoBufType;
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->version:I

    iput-object p2, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->cacheType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    iput-object p3, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    iput p4, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryTag:I

    iput p5, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->creationTimeTag:I

    iput p6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->lastUsedTimeTag:I

    return-void
.end method

.method private createProto(Lcom/google/android/location/cache/TemporalLRUCache;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalLRUCache",
            "<TK;TV;>;)",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    new-instance v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->cacheType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/android/location/cache/TemporalLRUCache;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/data/TemporalObject;

    new-instance v4, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v5}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0, v4, v2, v6}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->writeKeyAndValueToProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V

    iget v6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->creationTimeTag:I

    invoke-virtual {v5}, Lcom/google/android/location/data/TemporalObject;->getCreationTimeInMillisSinceEpoch()J

    move-result-wide v7

    invoke-virtual {v4, v6, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    iget v6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->lastUsedTimeTag:I

    invoke-virtual {v5}, Lcom/google/android/location/data/TemporalObject;->getLastUsedTimeInMillisSinceEpoch()J

    move-result-wide v7

    invoke-virtual {v4, v6, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    iget v6, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryTag:I

    invoke-virtual {v3, v6, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    return-object v3
.end method


# virtual methods
.method public load(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/InputStream;)V
    .locals 16
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalLRUCache",
            "<TK;TV;>;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->read()I

    move-result v10

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->version:I

    if-eq v10, v13, :cond_0

    new-instance v13, Ljava/io/IOException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Version mismatch while reading LRU cache file (expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->version:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", but "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    new-instance v8, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->cacheType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v13

    if-nez v13, :cond_1

    new-instance v13, Ljava/io/IOException;

    const-string v14, "Corrupted LRU cache file: proto not valid"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryTag:I

    invoke-virtual {v8, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v7, :cond_2

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->entryTag:I

    invoke-virtual {v8, v13, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->loadKeyFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->loadValueFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->creationTimeTag:I

    invoke-virtual {v9, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->lastUsedTimeTag:I

    invoke-virtual {v9, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    new-instance v11, Lcom/google/android/location/data/TemporalObject;

    invoke-direct {v11, v12, v1, v2}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    invoke-virtual {v11, v5, v6}, Lcom/google/android/location/data/TemporalObject;->updateLastUsedTime(J)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected abstract loadKeyFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")TK;"
        }
    .end annotation
.end method

.method protected abstract loadValueFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")TV;"
        }
    .end annotation
.end method

.method public save(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/OutputStream;)V
    .locals 2
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalLRUCache",
            "<TK;TV;>;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->createProto(Lcom/google/android/location/cache/TemporalLRUCache;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;->version:I

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v0, p2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method protected abstract writeKeyAndValueToProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            "TK;TV;)V"
        }
    .end annotation
.end method
