.class public Lcom/google/android/location/cache/ModelState;
.super Ljava/lang/Object;
.source "ModelState.java"


# instance fields
.field final clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/DiskTemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/LevelSelector;",
            ">;"
        }
    .end annotation
.end field

.field final featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/DiskTemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/LevelModel;",
            ">;"
        }
    .end annotation
.end field

.field final macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/FileTemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/location/cache/FileTemporalCache;Lcom/google/android/location/cache/DiskTemporalCache;Lcom/google/android/location/cache/DiskTemporalCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/FileTemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/location/cache/DiskTemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/LevelSelector;",
            ">;",
            "Lcom/google/android/location/cache/DiskTemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/LevelModel;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    iput-object p2, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    iput-object p3, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    return-void
.end method

.method public static create(Lcom/google/android/location/os/Os;)Lcom/google/android/location/cache/ModelState;
    .locals 3
    .param p0    # Lcom/google/android/location/os/Os;

    invoke-interface {p0}, Lcom/google/android/location/os/Os;->persistentStateDir()Ljava/io/File;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/location/os/Os;->getEncryptionKeyOrNull()Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/location/os/Os;->getExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-static {v2, v0, v1, p0}, Lcom/google/android/location/cache/ModelState;->createInternal(Ljava/util/concurrent/ExecutorService;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)Lcom/google/android/location/cache/ModelState;

    move-result-object v2

    return-object v2
.end method

.method private static createInternal(Ljava/util/concurrent/ExecutorService;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)Lcom/google/android/location/cache/ModelState;
    .locals 22
    .param p0    # Ljava/util/concurrent/ExecutorService;
    .param p1    # Ljava/io/File;
    .param p2    # Ljavax/crypto/SecretKey;
    .param p3    # Lcom/google/android/location/os/FileSystem;

    new-instance v5, Ljava/io/File;

    const-string v3, "macs"

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/location/cache/FileTemporalCache;

    const/16 v3, 0x190

    sget-object v4, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->LONG_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/cache/FileTemporalCache;-><init>(ILcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)V

    new-instance v12, Ljava/io/File;

    const-string v3, "selectors"

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v6, Lcom/google/android/location/cache/DiskTemporalCache;

    const/4 v7, 0x2

    const/16 v8, 0x14

    new-instance v10, Lcom/google/android/location/data/LevelSelectorFactory;

    invoke-direct {v10}, Lcom/google/android/location/data/LevelSelectorFactory;-><init>()V

    sget-object v11, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->STRING_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    move-object/from16 v9, p0

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    invoke-direct/range {v6 .. v14}, Lcom/google/android/location/cache/DiskTemporalCache;-><init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/data/ProtoFactory;Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)V

    new-instance v19, Ljava/io/File;

    const-string v3, "models"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdirs()Z

    move-object/from16 v0, p3

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v13, Lcom/google/android/location/cache/DiskTemporalCache;

    const/4 v14, 0x4

    const/16 v15, 0xa

    new-instance v17, Lcom/google/android/location/data/LevelModelFactory;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/location/data/LevelModelFactory;-><init>()V

    sget-object v18, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->STRING_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    move-object/from16 v16, p0

    move-object/from16 v20, p2

    move-object/from16 v21, p3

    invoke-direct/range {v13 .. v21}, Lcom/google/android/location/cache/DiskTemporalCache;-><init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/data/ProtoFactory;Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)V

    new-instance v3, Lcom/google/android/location/cache/ModelState;

    invoke-direct {v3, v2, v6, v13}, Lcom/google/android/location/cache/ModelState;-><init>(Lcom/google/android/location/cache/FileTemporalCache;Lcom/google/android/location/cache/DiskTemporalCache;Lcom/google/android/location/cache/DiskTemporalCache;)V

    return-object v3
.end method

.method private isResponseValid(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 5
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-virtual {p1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_3

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/16 v4, 0x17

    if-eq v3, v4, :cond_1

    move v1, v2

    goto :goto_0
.end method

.method private updateLevelModels(Lcom/google/android/location/data/ModelRequest;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p3    # J

    const/4 v1, 0x3

    invoke-virtual {p2, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getModelId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p3, p4}, Lcom/google/android/location/cache/DiskTemporalCache;->insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "ModelState"

    const-string v2, "Malformed reply does not have model."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateLevelSelectors(Lcom/google/android/location/data/ModelRequest;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p3    # J

    const/4 v1, 0x2

    invoke-virtual {p2, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getModelId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p3, p4}, Lcom/google/android/location/cache/DiskTemporalCache;->insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "ModelState"

    const-string v2, "Malformed reply does not have model cluster."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateMacAddresses(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V
    .locals 6
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J

    invoke-static {p1}, Lcom/google/android/location/data/MacToLevelClusterFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v3, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5, p2, p3}, Lcom/google/android/location/cache/FileTemporalCache;->insertValue(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateNotFoundLevelModel(Lcom/google/android/location/data/ModelRequest;J)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getModelId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/google/android/location/cache/DiskTemporalCache;->insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    return-void
.end method

.method private updateNotFoundLevelSelector(Lcom/google/android/location/data/ModelRequest;J)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getModelId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2, p3}, Lcom/google/android/location/cache/DiskTemporalCache;->insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    return-void
.end method


# virtual methods
.method public deleteCacheFiles()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/FileTemporalCache;->deleteCacheFile()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->deleteCacheFilesAndDir()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->deleteCacheFilesAndDir()V

    return-void
.end method

.method public discardOldCacheEntries(J)V
    .locals 6
    .param p1    # J

    const-wide/32 v4, 0x240c8400

    sub-long v2, p1, v4

    const-wide/32 v4, 0xa4cb800

    sub-long v0, p1, v4

    iget-object v4, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-virtual {v4, v0, v1, v0, v1}, Lcom/google/android/location/cache/FileTemporalCache;->discardOldEntries(JJ)V

    iget-object v4, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v4, v2, v3, v2, v3}, Lcom/google/android/location/cache/DiskTemporalCache;->discardOldEntries(JJ)V

    iget-object v4, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v4, v2, v3, v2, v3}, Lcom/google/android/location/cache/DiskTemporalCache;->discardOldEntries(JJ)V

    return-void
.end method

.method public getLevelClusterFeatureId(JJ)Ljava/lang/String;
    .locals 2
    .param p1    # J
    .param p3    # J

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Lcom/google/android/location/cache/FileTemporalCache;->lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLevelModel(Ljava/lang/String;J)Lcom/google/android/location/data/LevelModel;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/cache/DiskTemporalCache;->lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/LevelModel;

    return-object v0
.end method

.method public getLevelSelector(Ljava/lang/String;J)Lcom/google/android/location/data/LevelSelector;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/cache/DiskTemporalCache;->lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/LevelSelector;

    return-object v0
.end method

.method public hasLevelModelInMemoryOrDisk(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/DiskTemporalCache;->hasValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasLevelSelectorInMemoryOrDisk(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/DiskTemporalCache;->hasValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public load()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/FileTemporalCache;->load()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->load()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->load()V

    const-string v0, "ModelState"

    const-string v1, "Done loading ModelState from disk"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public save()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->macAddressToLevelClusterFeatureId:Lcom/google/android/location/cache/FileTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/FileTemporalCache;->save()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->clusterFeatureIdToLevelSelector:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->save()V

    iget-object v0, p0, Lcom/google/android/location/cache/ModelState;->featureIdToLevelModel:Lcom/google/android/location/cache/DiskTemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->save()V

    const-string v0, "ModelState"

    const-string v1, "Wrote ModelState to disk"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateMacsFromGlsResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z
    .locals 3
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J

    invoke-direct {p0, p1}, Lcom/google/android/location/cache/ModelState;->isResponseValid(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateMacAddresses(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public updateModelsFromGlsModelResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;JLcom/google/android/location/data/ModelRequest;)Z
    .locals 6
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J
    .param p4    # Lcom/google/android/location/data/ModelRequest;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p4, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/location/cache/ModelState;->isResponseValid(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    const/16 v5, 0x17

    if-ne v4, v5, :cond_4

    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL_SELECTOR:Lcom/google/android/location/data/ModelRequest$Type;

    if-ne v4, v5, :cond_2

    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateNotFoundLevelSelector(Lcom/google/android/location/data/ModelRequest;J)V

    :goto_1
    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL:Lcom/google/android/location/data/ModelRequest$Type;

    if-ne v4, v5, :cond_3

    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateNotFoundLevelModel(Lcom/google/android/location/data/ModelRequest;J)V

    goto :goto_1

    :cond_3
    const-string v3, "ModelState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown request type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL_SELECTOR:Lcom/google/android/location/data/ModelRequest$Type;

    if-ne v4, v5, :cond_5

    invoke-direct {p0, p4, v1, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateLevelSelectors(Lcom/google/android/location/data/ModelRequest;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    :goto_2
    move v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL:Lcom/google/android/location/data/ModelRequest$Type;

    if-ne v4, v5, :cond_6

    invoke-direct {p0, p4, v1, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateLevelModels(Lcom/google/android/location/data/ModelRequest;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V

    goto :goto_2

    :cond_6
    const-string v3, "ModelState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown request type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p4}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
