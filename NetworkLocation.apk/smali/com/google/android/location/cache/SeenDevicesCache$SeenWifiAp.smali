.class public Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;
.super Ljava/lang/Object;
.source "SeenDevicesCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/cache/SeenDevicesCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SeenWifiAp"
.end annotation


# instance fields
.field private final device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

.field private lastCollectionMillisSinceBoot:J


# direct methods
.method public constructor <init>(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->lastCollectionMillisSinceBoot:J

    iput-object p1, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;JJJ)V
    .locals 0
    .param p0    # Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;
    .param p1    # J
    .param p3    # J
    .param p5    # J

    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->adjustTimestamps(JJJ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;)V
    .locals 0
    .param p0    # Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    invoke-direct {p0}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->clearGlsResult()V

    return-void
.end method

.method private adjustTimestamps(JJJ)V
    .locals 11
    .param p1    # J
    .param p3    # J
    .param p5    # J

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v5, 0x5265c00

    mul-long v3, v0, v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->convertTimeSinceBoot(JJJJ)J

    move-result-wide v9

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-direct {p0, v9, v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getDaysSinceBoot(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v5, 0x5265c00

    mul-long v3, v0, v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->convertTimeSinceBoot(JJJJ)J

    move-result-wide v9

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-direct {p0, v9, v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getDaysSinceBoot(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_1
    return-void
.end method

.method private clearGlsResult()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/location/utils/Utils;->clearField(Lcom/google/gmm/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/utils/Utils;->clearField(Lcom/google/gmm/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/utils/Utils;->clearField(Lcom/google/gmm/common/io/protocol/ProtoBuf;I)V

    return-void
.end method

.method private convertTimeSinceBoot(JJJJ)J
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # J

    add-long v0, p3, p1

    sub-long/2addr v0, p5

    invoke-static {p7, p8, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private getDaysSinceBoot(J)I
    .locals 12
    .param p1    # J

    const-wide/32 v2, 0x927c0

    long-to-double v8, p1

    const-wide/high16 v10, 0x3ff0000000000000L

    mul-double/2addr v8, v10

    const-wide v10, 0x4194997000000000L

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-long v4, v8

    move-wide v6, v4

    const-wide/32 v8, 0x5265c00

    mul-long/2addr v8, v4

    sub-long/2addr v8, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v8, 0x5265c00

    sub-long/2addr v8, v2

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    :cond_0
    long-to-int v8, v6

    return v8
.end method

.method private setCollectionProb(F)V
    .locals 2
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setFloat(IF)V

    return-void
.end method


# virtual methods
.method public getDatabaseVersion()I
    .locals 2

    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDownloadDaysSinceBoot()I
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public getLastSeenDaysSinceBoot()I
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method setGlsResult(JFI)V
    .locals 0
    .param p1    # J
    .param p3    # F
    .param p4    # I

    invoke-virtual {p0, p1, p2, p4}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setGlsResult(JI)V

    invoke-direct {p0, p3}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setCollectionProb(F)V

    return-void
.end method

.method setGlsResult(JI)V
    .locals 2
    .param p1    # J
    .param p3    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setReadingTime(J)V

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/utils/Utils;->clearField(Lcom/google/gmm/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-void
.end method

.method setReadingTime(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getDaysSinceBoot(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-void
.end method
