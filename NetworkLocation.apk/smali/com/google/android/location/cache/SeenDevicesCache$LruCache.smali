.class public Lcom/google/android/location/cache/SeenDevicesCache$LruCache;
.super Ljava/util/LinkedHashMap;
.source "SeenDevicesCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/cache/SeenDevicesCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LruCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final capacity:I

.field private final stats:Lcom/google/android/location/cache/Stats;


# direct methods
.method public constructor <init>(ILcom/google/android/location/cache/Stats;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/location/cache/Stats;

    const/high16 v0, 0x3f400000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->capacity:I

    iput-object p2, p0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->stats:Lcom/google/android/location/cache/Stats;

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/location/cache/SeenDevicesCache$LruCache;)I
    .locals 1
    .param p0    # Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    iget v0, p0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->capacity:I

    return v0
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->capacity:I

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->stats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v1}, Lcom/google/android/location/cache/Stats;->addCapacityEviction()V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
