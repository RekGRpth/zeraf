.class Lcom/google/android/location/localizer/MaxLreLocalizer$LargerClusterFirst;
.super Ljava/lang/Object;
.source "MaxLreLocalizer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/localizer/MaxLreLocalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LargerClusterFirst"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/localizer/MaxLreLocalizer;


# direct methods
.method private constructor <init>(Lcom/google/android/location/localizer/MaxLreLocalizer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/localizer/MaxLreLocalizer$LargerClusterFirst;->this$0:Lcom/google/android/location/localizer/MaxLreLocalizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/localizer/MaxLreLocalizer;Lcom/google/android/location/localizer/MaxLreLocalizer$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/localizer/MaxLreLocalizer;
    .param p2    # Lcom/google/android/location/localizer/MaxLreLocalizer$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/localizer/MaxLreLocalizer$LargerClusterFirst;-><init>(Lcom/google/android/location/localizer/MaxLreLocalizer;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;)I
    .locals 2
    .param p1    # Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;
    .param p2    # Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;

    invoke-virtual {p2}, Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;->getApCluster()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;->getApCluster()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;

    check-cast p2, Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/localizer/MaxLreLocalizer$LargerClusterFirst;->compare(Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;Lcom/google/android/location/localizer/MaxLreLocalizer$ApCluster;)I

    move-result v0

    return v0
.end method
