.class Lcom/google/android/location/localizer/WifiLocationEstimator;
.super Ljava/lang/Object;
.source "WifiLocationEstimator.java"

# interfaces
.implements Lcom/google/android/location/localizer/WifiLocalizerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/localizer/WifiLocationEstimator$1;,
        Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;,
        Lcom/google/android/location/localizer/WifiLocationEstimator$ByMacAddress;
    }
.end annotation


# static fields
.field private static final LOGGER:Ljava/util/logging/Logger;

.field private static final NULL_OUTLIERS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/location/localizer/WifiLocationEstimator;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/localizer/WifiLocationEstimator;->LOGGER:Ljava/util/logging/Logger;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/localizer/WifiLocationEstimator;->NULL_OUTLIERS:Ljava/util/Set;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private computeBearing(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)D
    .locals 17
    .param p1    # Lcom/google/android/location/data/Position;
    .param p2    # Lcom/google/android/location/data/Position;

    move-object/from16 v0, p1

    iget v11, v0, Lcom/google/android/location/data/Position;->latE7:I

    invoke-static {v11}, Lcom/google/android/location/localizer/LocalizerUtil;->e7ToRad(I)D

    move-result-wide v1

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/location/data/Position;->latE7:I

    invoke-static {v11}, Lcom/google/android/location/localizer/LocalizerUtil;->e7ToRad(I)D

    move-result-wide v3

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v11, v11

    const-wide v13, 0x416312d000000000L

    div-double/2addr v11, v13

    move-object/from16 v0, p1

    iget v13, v0, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v13, v13

    const-wide v15, 0x416312d000000000L

    div-double/2addr v13, v15

    sub-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    mul-double v9, v11, v13

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v15

    mul-double/2addr v13, v15

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v15

    mul-double/2addr v13, v15

    sub-double v7, v11, v13

    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v11

    return-wide v11
.end method

.method private computeCenter(Lcom/google/android/location/data/Pair;Lcom/google/android/location/data/Pair;)Lcom/google/android/location/data/Pair;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/Double;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/Double;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/Double;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/Double;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    sub-double v6, v16, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v2, v20, v22

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    mul-double v4, v20, v22

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    add-double v20, v20, v22

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    add-double v22, v22, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    add-double v24, v24, v2

    mul-double v22, v22, v24

    mul-double v24, v4, v4

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    add-double v20, v20, v2

    move-wide/from16 v0, v20

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v20

    add-double v18, v14, v20

    new-instance v20, Lcom/google/android/location/data/Pair;

    invoke-static {v12, v13}, Lcom/google/android/location/localizer/LocalizerUtil;->normalizeLatRadians(D)D

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v21

    invoke-static/range {v18 .. v19}, Lcom/google/android/location/localizer/LocalizerUtil;->normalizeLngRadians(D)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    invoke-direct/range {v20 .. v22}, Lcom/google/android/location/data/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v20
.end method

.method private computeCircleSize(IILcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;ILcom/google/android/location/data/Pair;)D
    .locals 18
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/google/android/location/data/Position;
    .param p4    # Lcom/google/android/location/data/Position;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/location/data/Position;",
            "Lcom/google/android/location/data/Position;",
            "I",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    mul-int v11, p5, p5

    mul-int v16, p1, p1

    mul-int v17, p2, p2

    invoke-static/range {p3 .. p3}, Lcom/google/android/location/localizer/LocalizerUtil;->getLatRadians(Lcom/google/android/location/data/Position;)D

    move-result-wide v1

    invoke-static/range {p3 .. p3}, Lcom/google/android/location/localizer/LocalizerUtil;->getLngRadians(Lcom/google/android/location/data/Position;)D

    move-result-wide v3

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/localizer/LocalizerUtil;->accurateDistanceMetersFromRadians(DDDD)D

    move-result-wide v5

    double-to-int v12, v5

    mul-int v13, v12, v12

    add-int v5, v11, v16

    sub-int v5, v5, v17

    int-to-double v5, v5

    mul-int/lit8 v7, p1, 0x2

    mul-int v7, v7, p5

    int-to-double v7, v7

    div-double v9, v5, v7

    add-int v5, v16, v13

    int-to-double v5, v5

    mul-int/lit8 v7, p1, 0x2

    mul-int/2addr v7, v12

    int-to-double v7, v7

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    return-wide v14
.end method

.method private computeGroupIntersection(Ljava/util/List;)Lcom/google/android/location/data/Position;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;)",
            "Lcom/google/android/location/data/Position;"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-virtual {v3}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-virtual {v3}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeIntersection(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)Lcom/google/android/location/data/Position;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private computeIntersection(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)Lcom/google/android/location/data/Position;
    .locals 28
    .param p1    # Lcom/google/android/location/data/Position;
    .param p2    # Lcom/google/android/location/data/Position;

    invoke-static/range {p1 .. p2}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v20

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v3}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v16

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v3}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v17

    sub-int v3, v16, v17

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    move/from16 v0, v20

    if-gt v0, v3, :cond_1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object/from16 p1, p2

    goto :goto_0

    :cond_1
    invoke-direct/range {p0 .. p2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeBearing(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)D

    move-result-wide v7

    add-int v3, v20, v17

    move/from16 v0, v16

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/location/data/Position;->latE7:I

    move-object/from16 v0, p2

    iget v10, v0, Lcom/google/android/location/data/Position;->latE7:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/location/data/Position;->lngE7:I

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/location/data/Position;->lngE7:I

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/location/localizer/WifiLocationEstimator;->distanceHeadingToLatLng(IIID)Lcom/google/android/location/data/Pair;

    move-result-object v26

    const-wide v18, 0x400921fb54442d18L

    add-double v13, v18, v7

    add-int v3, v20, v16

    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v12

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/location/localizer/WifiLocationEstimator;->distanceHeadingToLatLng(IIID)Lcom/google/android/location/data/Pair;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeCenter(Lcom/google/android/location/data/Pair;Lcom/google/android/location/data/Pair;)Lcom/google/android/location/data/Pair;

    move-result-object v23

    add-int v3, v16, v17

    move/from16 v0, v20

    if-le v0, v3, :cond_2

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v15

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v17

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v19

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v21

    invoke-static/range {v15 .. v22}, Lcom/google/android/location/localizer/LocalizerUtil;->accurateDistanceMetersFromRadians(DDDD)D

    move-result-wide v24

    :goto_1
    new-instance p1, Lcom/google/android/location/data/Position;

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/google/android/location/localizer/LocalizerUtil;->radToDegreesE7(D)I

    move-result v9

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/google/android/location/localizer/LocalizerUtil;->radToDegreesE7(D)I

    move-result v3

    move-wide/from16 v0, v24

    double-to-int v15, v0

    invoke-static {v15}, Lcom/google/android/location/localizer/LocalizerUtil;->metersToMm(I)I

    move-result v15

    move-object/from16 v0, p1

    invoke-direct {v0, v9, v3, v15}, Lcom/google/android/location/data/Position;-><init>(III)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v15, p0

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v21, v23

    invoke-direct/range {v15 .. v21}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeCircleSize(IILcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;ILcom/google/android/location/data/Pair;)D

    move-result-wide v24

    goto :goto_1
.end method

.method private computeLocation(Ljava/util/List;)Lcom/google/android/location/data/Position;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;)",
            "Lcom/google/android/location/data/Position;"
        }
    .end annotation

    const v9, 0x249f0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/localizer/WifiLocationEstimator;->findGroup(Ljava/util/Set;Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeGroupIntersection(Ljava/util/List;)Lcom/google/android/location/data/Position;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    const/4 v7, 0x0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/location/data/Position;

    :goto_2
    iget v7, v6, Lcom/google/android/location/data/Position;->accuracyMm:I

    if-ge v7, v9, :cond_3

    new-instance v0, Lcom/google/android/location/data/Position$PositionBuilder;

    invoke-direct {v0, v6}, Lcom/google/android/location/data/Position$PositionBuilder;-><init>(Lcom/google/android/location/data/Position;)V

    iput v9, v0, Lcom/google/android/location/data/Position$PositionBuilder;->accuracyMm:I

    invoke-virtual {v0}, Lcom/google/android/location/data/Position$PositionBuilder;->build()Lcom/google/android/location/data/Position;

    move-result-object v6

    :cond_3
    return-object v6

    :cond_4
    invoke-direct {p0, v4}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeWeightedCentroid(Ljava/util/List;)Lcom/google/android/location/data/Position;

    move-result-object v6

    goto :goto_2
.end method

.method private computePrefetchOutliers(Lcom/google/android/location/data/Position;Ljava/util/Map;)Ljava/util/Set;
    .locals 6
    .param p1    # Lcom/google/android/location/data/Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/Position;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/data/Position;

    invoke-static {v2, p1}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v4

    const/16 v5, 0xfa

    if-le v4, v5, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private computeWeightedCentroid(Ljava/util/List;)Lcom/google/android/location/data/Position;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/Position;",
            ">;)",
            "Lcom/google/android/location/data/Position;"
        }
    .end annotation

    const/16 v16, 0x0

    const/16 v21, 0x0

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v21

    if-ge v0, v3, :cond_1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/location/data/Position;

    add-int/lit8 v3, v21, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/location/data/Position;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeBearing(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)D

    move-result-wide v7

    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/location/data/Position;->latE7:I

    move-object/from16 v0, v19

    iget v5, v0, Lcom/google/android/location/data/Position;->lngE7:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v3}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v6

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/location/localizer/WifiLocationEstimator;->distanceHeadingToLatLng(IIID)Lcom/google/android/location/data/Pair;

    move-result-object v24

    move-object/from16 v0, v20

    iget v10, v0, Lcom/google/android/location/data/Position;->latE7:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/google/android/location/data/Position;->lngE7:I

    move-object/from16 v0, v20

    iget v3, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v3}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v12

    const-wide v3, 0x400921fb54442d18L

    add-double v13, v3, v7

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/location/localizer/WifiLocationEstimator;->distanceHeadingToLatLng(IIID)Lcom/google/android/location/data/Pair;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeCenter(Lcom/google/android/location/data/Pair;Lcom/google/android/location/data/Pair;)Lcom/google/android/location/data/Pair;

    move-result-object v15

    if-nez v16, :cond_0

    move-object/from16 v16, v15

    :goto_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v15}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeCenter(Lcom/google/android/location/data/Pair;Lcom/google/android/location/data/Pair;)Lcom/google/android/location/data/Pair;

    move-result-object v16

    goto :goto_1

    :cond_1
    new-instance v4, Lcom/google/android/location/data/Position$PositionBuilder;

    invoke-direct {v4}, Lcom/google/android/location/data/Position$PositionBuilder;-><init>()V

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/google/android/location/localizer/LocalizerUtil;->radToDegreesE7(D)I

    move-result v5

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/google/android/location/localizer/LocalizerUtil;->radToDegreesE7(D)I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/google/android/location/data/Position$PositionBuilder;->latLng(II)Lcom/google/android/location/data/Position$PositionBuilder;

    move-result-object v17

    const v23, 0x7fffffff

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/location/data/Position;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position$PositionBuilder;Lcom/google/android/location/data/Position;)I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    move/from16 v23, v18

    goto :goto_2

    :cond_3
    invoke-static/range {v23 .. v23}, Lcom/google/android/location/localizer/LocalizerUtil;->metersToMm(I)I

    move-result v3

    move-object/from16 v0, v17

    iput v3, v0, Lcom/google/android/location/data/Position$PositionBuilder;->accuracyMm:I

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/data/Position$PositionBuilder;->build()Lcom/google/android/location/data/Position;

    move-result-object v3

    return-object v3
.end method

.method private createEmptyReply()Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;
    .locals 4

    new-instance v0, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/localizer/WifiLocationEstimator;->NULL_OUTLIERS:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;-><init>(Lcom/google/android/location/data/Position;ILjava/util/Set;)V

    return-object v0
.end method

.method private determineIntersectCountBounds(Ljava/util/List;)Lcom/google/android/location/data/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;)",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const v3, 0x7fffffff

    const/high16 v2, -0x80000000

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getIntersectionCount()I

    move-result v4

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getIntersectionCount()I

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/google/android/location/data/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/location/data/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method private findGroup(Ljava/util/Set;Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;)Ljava/util/List;
    .locals 3
    .param p2    # Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;>;",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-direct {p0, v2, p2}, Lcom/google/android/location/localizer/WifiLocationEstimator;->intersects(Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private intersects(Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;)Z
    .locals 5
    .param p1    # Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;
    .param p2    # Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-virtual {p1}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v0

    iget v3, v1, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v3}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v3

    iget v4, v2, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-static {v4}, Lcom/google/android/location/localizer/LocalizerUtil;->mmToMeters(I)I

    move-result v4

    add-int/2addr v3, v4

    if-gt v0, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private removeOutliers(Ljava/util/List;I)V
    .locals 3
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;I)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->getIntersectionCount()I

    move-result v2

    if-ne v2, p2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateIntersectCount(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->setIntersectionCount(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    add-int/lit8 v3, v1, 0x1

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-direct {p0, v5, v4}, Lcom/google/android/location/localizer/WifiLocationEstimator;->intersects(Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->incIntersectionCount()V

    invoke-virtual {v4}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->incIntersectionCount()V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method distanceHeadingToLatLng(IIID)Lcom/google/android/location/data/Pair;
    .locals 19
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIID)",
            "Lcom/google/android/location/data/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    move/from16 v0, p3

    int-to-double v11, v0

    const-wide v13, 0x4158554c00000000L

    div-double v1, v11, v13

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/localizer/LocalizerUtil;->e7ToRad(I)D

    move-result-wide v3

    invoke-static/range {p2 .. p2}, Lcom/google/android/location/localizer/LocalizerUtil;->e7ToRad(I)D

    move-result-wide v7

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    mul-double/2addr v13, v15

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->asin(D)D

    move-result-wide v5

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v17

    mul-double v15, v15, v17

    sub-double/2addr v13, v15

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v11

    add-double v9, v7, v11

    const-wide v11, 0x400921fb54442d18L

    add-double/2addr v11, v9

    const-wide v13, 0x401921fb54442d18L

    rem-double/2addr v11, v13

    const-wide v13, 0x400921fb54442d18L

    sub-double v9, v11, v13

    new-instance v11, Lcom/google/android/location/data/Pair;

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/google/android/location/data/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v11
.end method

.method public getEstimatedPosition(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;
    .locals 13
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;J)",
            "Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/localizer/WifiLocationEstimator;->createEmptyReply()Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v10

    :goto_0
    return-object v10

    :cond_1
    const/4 v6, 0x1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/location/data/WifiApPosition;

    iget-object v10, v8, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    sget-object v11, Lcom/google/android/location/data/WifiApPosition$PositionType;->MEDIUM_CONFIDENCE:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-eq v10, v11, :cond_3

    iget-object v10, v8, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    sget-object v11, Lcom/google/android/location/data/WifiApPosition$PositionType;->HIGH_CONFIDENCE:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-ne v10, v11, :cond_2

    :cond_3
    const/4 v6, 0x0

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/location/data/WifiApPosition;

    if-nez v6, :cond_6

    iget-object v10, v8, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    sget-object v11, Lcom/google/android/location/data/WifiApPosition$PositionType;->MEDIUM_CONFIDENCE:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-eq v10, v11, :cond_6

    iget-object v10, v8, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    sget-object v11, Lcom/google/android/location/data/WifiApPosition$PositionType;->HIGH_CONFIDENCE:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-ne v10, v11, :cond_5

    :cond_6
    new-instance v12, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/location/data/Position;

    invoke-direct {v12, v10, v11}, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;-><init>(Ljava/lang/Long;Lcom/google/android/location/data/Position;)V

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    new-instance v10, Lcom/google/android/location/localizer/WifiLocationEstimator$ByMacAddress;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/google/android/location/localizer/WifiLocationEstimator$ByMacAddress;-><init>(Lcom/google/android/location/localizer/WifiLocationEstimator;Lcom/google/android/location/localizer/WifiLocationEstimator$1;)V

    invoke-static {v0, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_8
    invoke-direct {p0, v0}, Lcom/google/android/location/localizer/WifiLocationEstimator;->updateIntersectCount(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/localizer/WifiLocationEstimator;->determineIntersectCountBounds(Ljava/util/List;)Lcom/google/android/location/data/Pair;

    move-result-object v5

    iget-object v10, v5, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    iget-object v11, v5, Lcom/google/android/location/data/Pair;->second:Ljava/lang/Object;

    if-eq v10, v11, :cond_a

    const/4 v7, 0x1

    :goto_2
    if-eqz v7, :cond_9

    iget-object v10, v5, Lcom/google/android/location/data/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-direct {p0, v0, v10}, Lcom/google/android/location/localizer/WifiLocationEstimator;->removeOutliers(Ljava/util/List;I)V

    :cond_9
    if-nez v7, :cond_8

    invoke-direct {p0, v0}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computeLocation(Ljava/util/List;)Lcom/google/android/location/data/Position;

    move-result-object v1

    iget v10, v1, Lcom/google/android/location/data/Position;->accuracyMm:I

    const v11, 0x16e360

    if-le v10, v11, :cond_b

    sget-object v10, Lcom/google/android/location/localizer/WifiLocationEstimator;->LOGGER:Ljava/util/logging/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ignoring computed location since accuracy too high: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Lcom/google/android/location/data/Position;->accuracyMm:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mm."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/localizer/WifiLocationEstimator;->createEmptyReply()Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v10

    goto/16 :goto_0

    :cond_a
    const/4 v7, 0x0

    goto :goto_2

    :cond_b
    const/16 v2, 0x50

    invoke-direct {p0, v1, p1}, Lcom/google/android/location/localizer/WifiLocationEstimator;->computePrefetchOutliers(Lcom/google/android/location/data/Position;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v10

    if-lez v10, :cond_c

    sget-object v10, Lcom/google/android/location/localizer/WifiLocationEstimator;->LOGGER:Ljava/util/logging/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Not returning location for the following outliers: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_c
    new-instance v10, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    invoke-direct {v10, v1, v2, v9}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;-><init>(Lcom/google/android/location/data/Position;ILjava/util/Set;)V

    goto/16 :goto_0
.end method
