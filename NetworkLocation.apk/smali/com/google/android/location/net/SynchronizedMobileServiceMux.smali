.class public Lcom/google/android/location/net/SynchronizedMobileServiceMux;
.super Ljava/lang/Object;
.source "SynchronizedMobileServiceMux.java"


# static fields
.field private static instance:Lcom/google/android/location/net/SynchronizedMobileServiceMux;


# instance fields
.field private delegate:Lcom/google/masf/MobileServiceMux;


# direct methods
.method private constructor <init>(Lcom/google/masf/MobileServiceMux;)V
    .locals 0
    .param p1    # Lcom/google/masf/MobileServiceMux;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->delegate:Lcom/google/masf/MobileServiceMux;

    return-void
.end method

.method public static declared-synchronized getSingleton()Lcom/google/android/location/net/SynchronizedMobileServiceMux;
    .locals 3

    const-class v1, Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->instance:Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Please call init() before calling getInstance."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->instance:Lcom/google/android/location/net/SynchronizedMobileServiceMux;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized mightInitialize(Landroid/content/Context;Lcom/google/masf/MobileServiceMux$Configuration;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/masf/MobileServiceMux$Configuration;

    const-class v2, Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->instance:Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/gmm/common/android/AndroidConfig;

    invoke-direct {v1, p0}, Lcom/google/gmm/common/android/AndroidConfig;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/gmm/common/Config;->setConfig(Lcom/google/gmm/common/Config;)V

    new-instance v0, Lcom/google/masf/MobileServiceMux$Configuration;

    invoke-direct {v0}, Lcom/google/masf/MobileServiceMux$Configuration;-><init>()V

    invoke-static {p1}, Lcom/google/masf/MobileServiceMux;->initialize(Lcom/google/masf/MobileServiceMux$Configuration;)V

    new-instance v1, Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    invoke-static {}, Lcom/google/masf/MobileServiceMux;->getSingleton()Lcom/google/masf/MobileServiceMux;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;-><init>(Lcom/google/masf/MobileServiceMux;)V

    sput-object v1, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->instance:Lcom/google/android/location/net/SynchronizedMobileServiceMux;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public declared-synchronized submitRequest(Lcom/google/masf/protocol/Request;Z)V
    .locals 1
    .param p1    # Lcom/google/masf/protocol/Request;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->delegate:Lcom/google/masf/MobileServiceMux;

    invoke-virtual {v0, p1, p2}, Lcom/google/masf/MobileServiceMux;->submitRequest(Lcom/google/masf/protocol/Request;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
