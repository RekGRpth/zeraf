.class abstract Lcom/google/android/location/activity/ActivityDetectionScheduler$State;
.super Ljava/lang/Object;
.source "ActivityDetectionScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/activity/ActivityDetectionScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "State"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;


# direct methods
.method private constructor <init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/activity/ActivityDetectionScheduler;
    .param p2    # Lcom/google/android/location/activity/ActivityDetectionScheduler$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;-><init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V

    return-void
.end method


# virtual methods
.method protected activityDetectionEnabledChanged(Z)V
    .locals 4
    .param p1    # Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    new-instance v1, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;

    iget-object v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;-><init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$1;)V

    # invokes: Lcom/google/android/location/activity/ActivityDetectionScheduler;->changeState(Lcom/google/android/location/activity/ActivityDetectionScheduler$State;)V
    invoke-static {v0, v1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->access$500(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$State;)V

    :cond_0
    return-void
.end method

.method protected alarmRing()V
    .locals 0

    return-void
.end method

.method protected screenStateChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    # setter for: Lcom/google/android/location/activity/ActivityDetectionScheduler;->screenOn:Z
    invoke-static {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->access$302(Lcom/google/android/location/activity/ActivityDetectionScheduler;Z)Z

    return-void
.end method

.method protected stateEntered()V
    .locals 0

    return-void
.end method

.method protected stateExit()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    # invokes: Lcom/google/android/location/activity/ActivityDetectionScheduler;->cancelAlarm()V
    invoke-static {v0}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->access$900(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V

    return-void
.end method

.method protected travelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    return-void
.end method
