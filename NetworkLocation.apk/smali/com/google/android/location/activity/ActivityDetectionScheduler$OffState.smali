.class Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;
.super Lcom/google/android/location/activity/ActivityDetectionScheduler$State;
.source "ActivityDetectionScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/activity/ActivityDetectionScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OffState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;


# direct methods
.method private constructor <init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;-><init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/activity/ActivityDetectionScheduler;
    .param p2    # Lcom/google/android/location/activity/ActivityDetectionScheduler$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;-><init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V

    return-void
.end method


# virtual methods
.method protected activityDetectionEnabledChanged(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;->this$0:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    # getter for: Lcom/google/android/location/activity/ActivityDetectionScheduler;->hasAccelerometer:Z
    invoke-static {v0}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->access$200(Lcom/google/android/location/activity/ActivityDetectionScheduler;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method
