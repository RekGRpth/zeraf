.class public Lcom/google/android/location/os/real/AndroidLogger;
.super Ljava/lang/Object;
.source "AndroidLogger.java"

# interfaces
.implements Lcom/google/android/location/utils/logging/LoggerInterface;


# instance fields
.field private final eventLog:Lcom/google/android/location/os/EventLog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/EventLog;)V
    .locals 0
    .param p1    # Lcom/google/android/location/os/EventLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    return-void
.end method

.method private log(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-static {p3, p1, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/EventLog;->addLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x6

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/AndroidLogger;->log(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Throwable;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/AndroidLogger;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/EventLog;->addLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public isLoggable(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x5

    if-gt v0, p2, :cond_0

    invoke-static {p1, p2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/AndroidLogger;->log(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
