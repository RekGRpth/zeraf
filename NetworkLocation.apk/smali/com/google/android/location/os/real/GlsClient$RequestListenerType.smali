.class final enum Lcom/google/android/location/os/real/GlsClient$RequestListenerType;
.super Ljava/lang/Enum;
.source "GlsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/GlsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RequestListenerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/os/real/GlsClient$RequestListenerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

.field public static final enum DEVICE_LOCATION_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

.field public static final enum MODEL_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

.field public static final enum QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    const-string v1, "MODEL_QUERY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->MODEL_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    const-string v1, "DEVICE_LOCATION_QUERY"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->DEVICE_LOCATION_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->MODEL_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->DEVICE_LOCATION_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->$VALUES:[Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/os/real/GlsClient$RequestListenerType;
    .locals 1

    const-class v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/os/real/GlsClient$RequestListenerType;
    .locals 1

    sget-object v0, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->$VALUES:[Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v0}, [Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    return-object v0
.end method
