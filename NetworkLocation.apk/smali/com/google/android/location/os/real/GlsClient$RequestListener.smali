.class Lcom/google/android/location/os/real/GlsClient$RequestListener;
.super Ljava/lang/Object;
.source "GlsClient.java"

# interfaces
.implements Lcom/google/masf/protocol/Request$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/GlsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestListener"
.end annotation


# instance fields
.field private multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

.field private volatile payLoadLength:J

.field private final tag:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/location/os/real/GlsClient;

.field private final type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;


# direct methods
.method constructor <init>(Lcom/google/android/location/os/real/GlsClient;Lcom/google/android/location/os/real/GlsClient$RequestListenerType;)V
    .locals 2
    .param p2    # Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    iput-object p1, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->payLoadLength:J

    iput-object p2, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->getTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->tag:Ljava/lang/String;

    return-void
.end method

.method private getEndPoint()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/location/os/real/GlsClient$1;->$SwitchMap$com$google$android$location$os$real$GlsClient$RequestListenerType:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "g:loc/ql"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "g:loc/dl"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getTag()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/location/os/real/GlsClient$1;->$SwitchMap$com$google$android$location$os$real$GlsClient$RequestListenerType:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "GlsClient"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "GlsClient-query"

    goto :goto_0

    :pswitch_1
    const-string v0, "GlsClient-model-query"

    goto :goto_0

    :pswitch_2
    const-string v0, "GlsClient-device-location-query"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private notifyFailure()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/location/os/real/GlsClient$1;->$SwitchMap$com$google$android$location$os$real$GlsClient$RequestListenerType:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v0}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v0}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v0}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private shouldQueryNlpParameters()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized requestCompleted(Lcom/google/masf/protocol/Request;Lcom/google/masf/protocol/Response;)V
    .locals 7
    .param p1    # Lcom/google/masf/protocol/Request;
    .param p2    # Lcom/google/masf/protocol/Response;

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    if-eq p1, v4, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Response to unexpected request."

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_0
    const/4 v2, 0x0

    if-eqz p2, :cond_2

    :try_start_1
    invoke-virtual {p2}, Lcom/google/masf/protocol/Response;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/masf/protocol/Response;->getStatusCode()I

    move-result v4

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_2

    new-instance v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v3, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    # invokes: Lcom/google/android/location/os/real/GlsClient;->extractPlatformKey(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    invoke-static {v3}, Lcom/google/android/location/os/real/GlsClient;->access$500(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->shouldQueryNlpParameters()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;
    invoke-static {v4}, Lcom/google/android/location/os/real/GlsClient;->access$100(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/NlpParametersState;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/location/os/NlpParametersState;->setParameters(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    move-object v2, v3

    :cond_2
    :goto_0
    :try_start_3
    sget-object v4, Lcom/google/android/location/os/real/GlsClient$1;->$SwitchMap$com$google$android$location$os$real$GlsClient$RequestListenerType:[I

    iget-object v5, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v5}, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->type:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is unhandled."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v0

    :goto_1
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->tag:Ljava/lang/String;

    const-string v5, "requestCompleted()"

    invoke-static {v4, v5, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v4}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :goto_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_1
    :try_start_4
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v4}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_2

    :pswitch_2
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;
    invoke-static {v4}, Lcom/google/android/location/os/real/GlsClient;->access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/location/os/real/CallbackRunner;->glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v2, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized requestFailed(Lcom/google/masf/protocol/Request;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/masf/protocol/Request;
    .param p2    # Ljava/lang/Exception;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->tag:Ljava/lang/String;

    const-string v1, "requestFailed"

    invoke-static {v0, v1, p2}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->notifyFailure()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method declared-synchronized sendRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 8
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Gls request still outstanding."

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_0
    const/4 v4, 0x1

    :try_start_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    # invokes: Lcom/google/android/location/os/real/GlsClient;->createPlatformProfile(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    invoke-static {v5, p2}, Lcom/google/android/location/os/real/GlsClient;->access$000(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->shouldQueryNlpParameters()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # getter for: Lcom/google/android/location/os/real/GlsClient;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;
    invoke-static {v5}, Lcom/google/android/location/os/real/GlsClient;->access$100(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/NlpParametersState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/os/NlpParametersState;->getParameterId()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_1
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    if-lez v4, :cond_3

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # invokes: Lcom/google/android/location/os/real/GlsClient;->generateDebugInfo()Z
    invoke-static {v4}, Lcom/google/android/location/os/real/GlsClient;->access$200(Lcom/google/android/location/os/real/GlsClient;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x63

    invoke-virtual {v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/protocol/LocserverMessageTypes;->GDEBUG_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v4, 0x63

    invoke-virtual {v3, v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->this$0:Lcom/google/android/location/os/real/GlsClient;

    # invokes: Lcom/google/android/location/os/real/GlsClient;->updateDebugProfileLocked(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    invoke-static {v4, v0}, Lcom/google/android/location/os/real/GlsClient;->access$300(Lcom/google/android/location/os/real/GlsClient;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_3
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v4, Lcom/google/android/location/os/real/MultipartZippedRequest;

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->getEndPoint()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/location/os/real/MultipartZippedRequest;-><init>(Ljava/lang/String;I[B)V

    iput-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    invoke-virtual {v4, p0}, Lcom/google/android/location/os/real/MultipartZippedRequest;->setListener(Lcom/google/masf/protocol/Request$Listener;)V

    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/location/os/real/MultipartZippedRequest;->setSentCount(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v5, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    monitor-enter v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    invoke-virtual {v4}, Lcom/google/android/location/os/real/MultipartZippedRequest;->getBodyStreamLength()I

    move-result v4

    int-to-long v6, v4

    iput-wide v6, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->payLoadLength:J

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_0
    :try_start_6
    invoke-static {}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->getSingleton()Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->multipartRequest:Lcom/google/android/location/os/real/MultipartZippedRequest;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->submitRequest(Lcom/google/masf/protocol/Request;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v1

    :try_start_7
    iget-object v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->tag:Ljava/lang/String;

    const-string v5, "query(): unable to write request to payload"

    invoke-static {v4, v5, v1}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->notifyFailure()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_8
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v4
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move-exception v1

    const-wide/16 v4, 0x0

    :try_start_a
    iput-wide v4, p0, Lcom/google/android/location/os/real/GlsClient$RequestListener;->payLoadLength:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_0
.end method
