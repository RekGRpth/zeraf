.class Lcom/google/android/location/os/real/CallbackRunner;
.super Ljava/lang/Object;
.source "CallbackRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/CallbackRunner$1;,
        Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;,
        Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;,
        Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;,
        Lcom/google/android/location/os/real/CallbackRunner$MyHandler;
    }
.end annotation


# static fields
.field public static ALARM_WAKEUP_ACTIVITY_DETECTION:Ljava/lang/String;

.field public static ALARM_WAKEUP_CACHE_UPDATER:Ljava/lang/String;

.field public static ALARM_WAKEUP_LOCATOR:Ljava/lang/String;


# instance fields
.field private final alarmWakeLock:Landroid/os/PowerManager$WakeLock;

.field private broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

.field private callbacks:Lcom/google/android/location/os/Callbacks;

.field private final context:Landroid/content/Context;

.field private final eventLog:Lcom/google/android/location/os/EventLog;

.field private handler:Landroid/os/Handler;

.field private final lock:Ljava/lang/Object;

.field private final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field private final phoneStateListener:Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

.field private quitCalled:Z

.field private final runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

.field private final thread:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/os/EventLog;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/location/os/NlpParametersState;
    .param p3    # Lcom/google/android/location/os/EventLog;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/CallbackRunner;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    iput-object p3, p0, Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".nlp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".ALARM_WAKEUP_LOCATOR"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_LOCATOR:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".ALARM_WAKEUP_CACHE_UPDATER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_CACHE_UPDATER:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".ALARM_WAKEUP_ACTIVITY_DETECTION"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_ACTIVITY_DETECTION:Ljava/lang/String;

    new-instance v2, Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

    invoke-direct {v2, p0, v5}, Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;-><init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->phoneStateListener:Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

    new-instance v2, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    invoke-direct {v2, p0, v5}, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;-><init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    const-string v4, "NetworkLocationCallbackRunner"

    invoke-direct {v2, v5, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->thread:Ljava/lang/Thread;

    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const-string v2, "NetworkLocationCallbackRunner"

    invoke-virtual {v1, v6, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/location/os/real/CallbackRunner;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/location/os/real/CallbackRunner;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/location/os/real/CallbackRunner;)Z
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-boolean v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;)Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/NlpParametersState;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->phoneStateListener:Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/location/os/real/CallbackRunner;Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # Landroid/net/ConnectivityManager;
    .param p2    # Lcom/google/android/location/os/Callbacks;

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/os/real/CallbackRunner;->notifyNetworkEvent(Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/location/os/real/CallbackRunner;ILjava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/location/os/real/CallbackRunner;III)V
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/location/os/real/CallbackRunner;I)V
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(I)V

    return-void
.end method

.method static synthetic access$900(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/location/os/real/CallbackRunner;->isAirplaneModeEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static isAirplaneModeEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private notifyNetworkEvent(Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V
    .locals 4
    .param p1    # Landroid/net/ConnectivityManager;
    .param p2    # Lcom/google/android/location/os/Callbacks;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v1, v2, v2}, Lcom/google/android/location/os/EventLog;->addNetworkChanged(ZZ)V

    invoke-interface {p2, v2, v2}, Lcom/google/android/location/os/Callbacks;->dataNetworkChanged(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/os/EventLog;->addNetworkChanged(ZZ)V

    invoke-interface {p2, v2, v3}, Lcom/google/android/location/os/Callbacks;->dataNetworkChanged(ZZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/location/os/EventLog;->addNetworkChanged(ZZ)V

    invoke-interface {p2, v3, v2}, Lcom/google/android/location/os/Callbacks;->dataNetworkChanged(ZZ)V

    goto :goto_0
.end method

.method private sendMessage(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessage(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    invoke-static {v0, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cellRequestScan()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x4

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V

    return-void
.end method

.method public glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/16 v0, 0x19

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public join()V
    .locals 1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/NlpParametersState;

    const/16 v0, 0x1c

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public quit(Z)V
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    if-eqz v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    const-string v3, "NetworkLocationCallbackRunner"

    const-string v4, "quit"

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z

    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    :goto_1
    const/4 v1, 0x0

    invoke-static {v3, v4, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public setActivityDetectionEnabled(Z)V
    .locals 2
    .param p1    # Z

    const/16 v0, 0x1f

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public setPeriod(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V
    .locals 1
    .param p1    # Lcom/google/android/location/data/NetworkLocation;

    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public start(Lcom/google/android/location/os/Callbacks;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/Callbacks;

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner;->runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->monitoringEvents:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner;->runnable:Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
