.class public Lcom/google/android/location/os/NlpParametersState;
.super Ljava/lang/Object;
.source "NlpParametersState.java"


# static fields
.field static final DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;


# instance fields
.field final clock:Lcom/google/android/location/os/Clock;

.field final fileSystem:Lcom/google/android/location/os/FileSystem;

.field private final listener:Lcom/google/android/location/utils/OnChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/utils/OnChangeListener",
            "<",
            "Lcom/google/android/location/os/NlpParametersState;",
            ">;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;

.field volatile parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

.field volatile updateTimeMillisSinceBoot:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/Clock;Lcom/google/android/location/os/FileSystem;Lcom/google/android/location/utils/OnChangeListener;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/Clock;
    .param p2    # Lcom/google/android/location/os/FileSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/os/Clock;",
            "Lcom/google/android/location/os/FileSystem;",
            "Lcom/google/android/location/utils/OnChangeListener",
            "<",
            "Lcom/google/android/location/os/NlpParametersState;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    iput-object p2, p0, Lcom/google/android/location/os/NlpParametersState;->fileSystem:Lcom/google/android/location/os/FileSystem;

    iput-object p3, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    return-void
.end method

.method private closeStream(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized fallBackToDefault()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v1

    sget-object v2, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v0

    sget-object v2, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    if-eq v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    invoke-interface {v2, p0}, Lcom/google/android/location/utils/OnChangeListener;->onChange(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private getFile(Lcom/google/android/location/os/FileSystem;)Ljava/io/File;
    .locals 3
    .param p1    # Lcom/google/android/location/os/FileSystem;

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lcom/google/android/location/os/FileSystem;->nlpParamsStateDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_params"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getLastUpdateSinceBoot(J)J
    .locals 9
    .param p1    # J

    iget-object v8, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v8}, Lcom/google/android/location/os/Clock;->millisSinceEpoch()J

    move-result-wide v4

    iget-object v8, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v8}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v2

    sub-long v0, v4, v2

    cmp-long v8, p1, v4

    if-lez v8, :cond_0

    move-wide p1, v4

    :cond_0
    sub-long v6, p1, v0

    return-wide v6
.end method

.method private static getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I
    .locals 1
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-wide v2, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/os/NlpParametersState;->isExpired(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->fallBackToDefault()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private isExpired(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z
    .locals 7
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-ne p1, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v0, v3, v5

    add-long v3, p2, v0

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private save()V
    .locals 10

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->fileSystem:Lcom/google/android/location/os/FileSystem;

    invoke-direct {p0, v4}, Lcom/google/android/location/os/NlpParametersState;->getFile(Lcom/google/android/location/os/FileSystem;)Ljava/io/File;

    move-result-object v3

    const/4 v0, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-ne v4, v5, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    :cond_2
    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->fileSystem:Lcom/google/android/location/os/FileSystem;

    invoke-interface {v4, v3}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v4, 0x2

    :try_start_2
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-wide v6, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v4}, Lcom/google/android/location/os/Clock;->bootTime()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->write([B)V

    monitor-exit v5

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    move-exception v2

    move-object v0, v1

    :goto_2
    :try_start_5
    const-string v4, "NlpParametersState"

    const-string v5, "Failed to write parameter state."

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-direct {p0, v0}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v4

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    throw v4

    :catchall_2
    move-exception v4

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v2

    goto :goto_2
.end method


# virtual methods
.method public getCellCacheTtlMillis()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public getCellDatabaseVersion()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxCacheRefillEntrys()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getParameterId()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v0

    return v0
.end method

.method public getWifiCacheTtlMillis()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public getWifiDatabaseVersion()I
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public isDefault()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->getParameters()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 8

    iget-object v3, p0, Lcom/google/android/location/os/NlpParametersState;->fileSystem:Lcom/google/android/location/os/FileSystem;

    invoke-direct {p0, v3}, Lcom/google/android/location/os/NlpParametersState;->getFile(Lcom/google/android/location/os/FileSystem;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/NlpParametersState;->load(Ljava/io/DataInputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v3, "NlpParametersState"

    const-string v4, "Using NlpParameterId: %d."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v7}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v1

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->fallBackToDefault()V

    goto :goto_0
.end method

.method load(Ljava/io/DataInputStream;)V
    .locals 8
    .param p1    # Ljava/io/DataInputStream;

    :try_start_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->fallBackToDefault()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-direct {p0, p1}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/google/android/location/os/NlpParametersState;->getLastUpdateSinceBoot(J)J

    move-result-wide v2

    new-instance v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/location/os/NlpParametersState;->isExpired(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v6, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iput-object v1, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iput-wide v2, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v5}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v5

    sget-object v7, Lcom/google/android/location/os/NlpParametersState;->DEFAULT_PARAMS:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v7}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v7

    if-eq v5, v7, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    invoke-interface {v5, p0}, Lcom/google/android/location/utils/OnChangeListener;->onChange(Ljava/lang/Object;)V

    :cond_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v0

    :try_start_5
    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->fallBackToDefault()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-direct {p0, p1}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_2
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->fallBackToDefault()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v5

    invoke-direct {p0, p1}, Lcom/google/android/location/os/NlpParametersState;->closeStream(Ljava/io/Closeable;)V

    throw v5
.end method

.method public setParameters(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 8
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v5, p0, Lcom/google/android/location/os/NlpParametersState;->lock:Ljava/lang/Object;

    monitor-enter v5

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {v4}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v4

    int-to-long v2, v4

    invoke-static {p1}, Lcom/google/android/location/os/NlpParametersState;->getNlpParameterId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v4

    int-to-long v0, v4

    iput-object p1, p0, Lcom/google/android/location/os/NlpParametersState;->parameters:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v4}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/os/NlpParametersState;->updateTimeMillisSinceBoot:J

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const-string v4, "NlpParametersState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Updating NlpParameters. New version: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/os/NlpParametersState;->save()V

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/location/os/NlpParametersState;->listener:Lcom/google/android/location/utils/OnChangeListener;

    invoke-interface {v4, p0}, Lcom/google/android/location/utils/OnChangeListener;->onChange(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
