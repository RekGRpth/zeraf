.class Lcom/google/android/location/os/EventLog$12;
.super Lcom/google/android/location/os/EventLog$Entry;
.source "EventLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/os/EventLog;->addAlarmReset(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/os/EventLog;

.field final synthetic val$alarmTime:J

.field final synthetic val$osClientId:I


# direct methods
.method constructor <init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JIJ)V
    .locals 0
    .param p2    # Lcom/google/android/location/os/Event;
    .param p3    # J

    iput-object p1, p0, Lcom/google/android/location/os/EventLog$12;->this$0:Lcom/google/android/location/os/EventLog;

    iput p5, p0, Lcom/google/android/location/os/EventLog$12;->val$osClientId:I

    iput-wide p6, p0, Lcom/google/android/location/os/EventLog$12;->val$alarmTime:J

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/location/os/EventLog$Entry;-><init>(Lcom/google/android/location/os/Event;J)V

    return-void
.end method


# virtual methods
.method protected dumpExtras(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1    # Ljava/io/PrintWriter;

    iget v0, p0, Lcom/google/android/location/os/EventLog$12;->val$osClientId:I

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    iget-wide v0, p0, Lcom/google/android/location/os/EventLog$12;->val$alarmTime:J

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    return-void
.end method
