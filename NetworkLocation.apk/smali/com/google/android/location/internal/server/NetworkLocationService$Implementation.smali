.class Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;
.super Lcom/google/android/location/internal/INetworkLocationInternal$Stub;
.source "NetworkLocationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/internal/server/NetworkLocationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Implementation"
.end annotation


# instance fields
.field private final packageManager:Landroid/content/pm/PackageManager;

.field final synthetic this$0:Lcom/google/android/location/internal/server/NetworkLocationService;


# direct methods
.method private constructor <init>(Lcom/google/android/location/internal/server/NetworkLocationService;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    invoke-direct {p0}, Lcom/google/android/location/internal/INetworkLocationInternal$Stub;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/NetworkLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->packageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/internal/server/NetworkLocationService;Lcom/google/android/location/internal/server/NetworkLocationService$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/internal/server/NetworkLocationService;
    .param p2    # Lcom/google/android/location/internal/server/NetworkLocationService$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;-><init>(Lcom/google/android/location/internal/server/NetworkLocationService;)V

    return-void
.end method

.method private checkPermissionsAndClearIdentity()V
    .locals 4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "Access is restricted to packages signed with the same certificate."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-void
.end method

.method private eventLogString(Landroid/location/Location;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/location/Location;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x3e80

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    if-eqz p1, :cond_0

    const-string v2, "RMI for "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # invokes: Lcom/google/android/location/internal/server/NetworkLocationService;->dump(Ljava/io/PrintWriter;)V
    invoke-static {v2, v1}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$200(Lcom/google/android/location/internal/server/NetworkLocationService;Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public cancelLocationUpdates(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 1
    .param p1    # Lcom/google/android/location/internal/ILocationListener;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->checkPermissionsAndClearIdentity()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # getter for: Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/ServiceThread;->removeLocationListener(Lcom/google/android/location/internal/ILocationListener;)V

    return-void
.end method

.method public getDebugDump()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->checkPermissionsAndClearIdentity()V

    sget-object v0, Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;->ANDROID:Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    invoke-virtual {v2}, Lcom/google/android/location/internal/server/NetworkLocationService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->eventLogString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getRmiInfo(Landroid/location/Location;)[B
    .locals 4
    .param p1    # Landroid/location/Location;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->checkPermissionsAndClearIdentity()V

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # getter for: Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    invoke-static {v1}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/location/internal/server/ServiceThread;->getReportedLocation(Landroid/location/Location;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/location/data/NetworkLocation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # getter for: Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    invoke-static {v1}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;

    move-result-object v2

    move-object v1, v0

    check-cast v1, Lcom/google/android/location/data/NetworkLocation;

    invoke-virtual {v2, v1}, Lcom/google/android/location/internal/server/ServiceThread;->signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # getter for: Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    invoke-static {v1}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->eventLogString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/google/android/location/internal/server/ServiceThread;->getLocationDebugInfo(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B

    move-result-object v1

    return-object v1
.end method

.method public requestLocationUpdates(ILcom/google/android/location/internal/ILocationListener;I)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/location/internal/ILocationListener;
    .param p3    # I

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->checkPermissionsAndClearIdentity()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;->this$0:Lcom/google/android/location/internal/server/NetworkLocationService;

    # getter for: Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    invoke-static {v0}, Lcom/google/android/location/internal/server/NetworkLocationService;->access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;

    move-result-object v0

    invoke-virtual {v0, p2, p1, p3}, Lcom/google/android/location/internal/server/ServiceThread;->addLocationListener(Lcom/google/android/location/internal/ILocationListener;II)V

    return-void
.end method
