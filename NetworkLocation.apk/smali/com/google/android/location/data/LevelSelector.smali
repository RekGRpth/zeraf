.class public Lcom/google/android/location/data/LevelSelector;
.super Ljava/lang/Object;
.source "LevelSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/data/LevelSelector$LevelResult;
    }
.end annotation


# instance fields
.field private final bagPredictionToFeatureId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;


# direct methods
.method public constructor <init>(Lcom/google/android/location/data/SignalStrengthBagPredictor;Ljava/util/Map;)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/SignalStrengthBagPredictor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/SignalStrengthBagPredictor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/data/LevelSelector;->bagPredictionToFeatureId:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/location/data/LevelSelector;->levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    return-void
.end method


# virtual methods
.method public classifyFloor(Ljava/util/Map;)Lcom/google/android/location/data/LevelSelector$LevelResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/data/LevelSelector$LevelResult;"
        }
    .end annotation

    new-instance v7, Lcom/google/android/location/data/LevelSelector$LevelResult;

    invoke-direct {v7}, Lcom/google/android/location/data/LevelSelector$LevelResult;-><init>()V

    iget-object v8, p0, Lcom/google/android/location/data/LevelSelector;->levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v8, p1}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->computePrediction(Ljava/util/Map;)[F

    move-result-object v5

    const/high16 v8, 0x3f800000

    array-length v9, v5

    int-to-float v9, v9

    div-float v6, v8, v9

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v4, v0, v2

    iget-object v8, p0, Lcom/google/android/location/data/LevelSelector;->bagPredictionToFeatureId:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v7, v1, v6}, Lcom/google/android/location/data/LevelSelector$LevelResult;->incrementLevelProbBy(Ljava/lang/String;F)V

    goto :goto_1

    :cond_1
    invoke-virtual {v7}, Lcom/google/android/location/data/LevelSelector$LevelResult;->computeMostProbable()V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/data/LevelSelector;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/data/LevelSelector;

    iget-object v3, p0, Lcom/google/android/location/data/LevelSelector;->bagPredictionToFeatureId:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/android/location/data/LevelSelector;->bagPredictionToFeatureId:Ljava/util/Map;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/data/LevelSelector;->levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iget-object v4, v0, Lcom/google/android/location/data/LevelSelector;->levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v3, v4}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/data/LevelSelector;->bagPredictionToFeatureId:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/data/LevelSelector;->levelClassifier:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v2}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method
