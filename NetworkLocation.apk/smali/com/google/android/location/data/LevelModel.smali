.class public Lcom/google/android/location/data/LevelModel;
.super Ljava/lang/Object;
.source "LevelModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/data/LevelModel$LevelModelResult;
    }
.end annotation


# instance fields
.field private final latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

.field private final levelNumberE3:I

.field private final lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;


# direct methods
.method public constructor <init>(Lcom/google/android/location/data/SignalStrengthBagPredictor;Lcom/google/android/location/data/SignalStrengthBagPredictor;I)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/SignalStrengthBagPredictor;
    .param p2    # Lcom/google/android/location/data/SignalStrengthBagPredictor;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iput-object p2, p0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iput p3, p0, Lcom/google/android/location/data/LevelModel;->levelNumberE3:I

    return-void
.end method


# virtual methods
.method public computeBestLocation(Ljava/util/Map;)Lcom/google/android/location/data/LevelModel$LevelModelResult;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/data/LevelModel$LevelModelResult;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->computePrediction(Ljava/util/Map;)[F

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->computePrediction(Ljava/util/Map;)[F

    move-result-object v12

    invoke-static {v10}, Lcom/google/android/location/learning/MathUtils;->computeMeanStdDev([F)Lcom/google/android/location/learning/MathUtils$MeanStdDev;

    move-result-object v11

    invoke-static {v12}, Lcom/google/android/location/learning/MathUtils;->computeMeanStdDev([F)Lcom/google/android/location/learning/MathUtils$MeanStdDev;

    move-result-object v13

    iget v1, v11, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    float-to-double v1, v1

    iget v3, v13, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    float-to-double v3, v3

    iget v5, v11, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    iget v6, v11, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->stdDev:F

    add-float/2addr v5, v6

    float-to-double v5, v5

    iget v7, v13, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    iget v8, v13, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->stdDev:F

    add-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/data/LatLngUtils;->distanceInM(DDDD)D

    move-result-wide v15

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v1}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->numTrees()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v1}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->numTrees()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    const v9, 0x469c4000

    :goto_0
    new-instance v14, Lcom/google/android/location/data/LevelModel$LevelModelResult;

    iget v1, v11, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    float-to-int v1, v1

    iget v2, v13, Lcom/google/android/location/learning/MathUtils$MeanStdDev;->mean:F

    float-to-int v2, v2

    float-to-int v3, v9

    invoke-direct {v14, v1, v2, v3}, Lcom/google/android/location/data/LevelModel$LevelModelResult;-><init>(III)V

    return-object v14

    :cond_1
    const/high16 v1, 0x40000000

    double-to-float v2, v15

    mul-float/2addr v1, v2

    const/high16 v2, 0x447a0000

    mul-float v9, v1, v2

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/data/LevelModel;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/data/LevelModel;

    iget v3, p0, Lcom/google/android/location/data/LevelModel;->levelNumberE3:I

    iget v4, v0, Lcom/google/android/location/data/LevelModel;->levelNumberE3:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iget-object v4, v0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v3, v4}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iget-object v4, v0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v3, v4}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getLevelNumberE3()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/LevelModel;->levelNumberE3:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/location/data/LevelModel;->levelNumberE3:I

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/data/LevelModel;->latPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v2}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/data/LevelModel;->lngPredictor:Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-virtual {v2}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method
