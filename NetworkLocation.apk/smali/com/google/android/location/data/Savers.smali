.class public Lcom/google/android/location/data/Savers;
.super Ljava/lang/Object;
.source "Savers.java"


# static fields
.field public static final LONG_SAVER:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_SAVER:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/location/data/Savers$1;

    invoke-direct {v0}, Lcom/google/android/location/data/Savers$1;-><init>()V

    sput-object v0, Lcom/google/android/location/data/Savers;->STRING_SAVER:Lcom/google/android/location/data/Persistent;

    new-instance v0, Lcom/google/android/location/data/Savers$2;

    invoke-direct {v0}, Lcom/google/android/location/data/Savers$2;-><init>()V

    sput-object v0, Lcom/google/android/location/data/Savers;->LONG_SAVER:Lcom/google/android/location/data/Persistent;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
