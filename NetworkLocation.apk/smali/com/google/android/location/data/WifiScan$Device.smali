.class public final Lcom/google/android/location/data/WifiScan$Device;
.super Ljava/lang/Object;
.source "WifiScan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/data/WifiScan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Device"
.end annotation


# instance fields
.field public final frequency:S

.field public final mac:Ljava/lang/Long;

.field public final rssi:I

.field public final ssid:Ljava/lang/String;


# direct methods
.method public constructor <init>(JILjava/lang/String;S)V
    .locals 1
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    iput p3, p0, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    if-nez p4, :cond_0

    const-string p4, ""

    :cond_0
    iput-object p4, p0, Lcom/google/android/location/data/WifiScan$Device;->ssid:Ljava/lang/String;

    iput-short p5, p0, Lcom/google/android/location/data/WifiScan$Device;->frequency:S

    return-void
.end method

.method public static append(Ljava/lang/StringBuilder;Lcom/google/android/location/data/WifiScan$Device;)V
    .locals 2
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Lcom/google/android/location/data/WifiScan$Device;

    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/data/WifiUtils;->macValueToHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", rssi="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", ssid="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/google/android/location/data/WifiScan$Device;->ssid:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", frequency="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v0, p1, Lcom/google/android/location/data/WifiScan$Device;->frequency:S

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public static dump(Ljava/io/PrintWriter;Lcom/google/android/location/data/WifiScan$Device;)V
    .locals 2
    .param p0    # Ljava/io/PrintWriter;
    .param p1    # Lcom/google/android/location/data/WifiScan$Device;

    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/location/data/WifiScan$Device;->dumpMac(Ljava/io/PrintWriter;J)V

    const-string v0, ", rssi="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, ", ssid="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/location/data/WifiScan$Device;->ssid:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, ", frequency="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-short v0, p1, Lcom/google/android/location/data/WifiScan$Device;->frequency:S

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void
.end method

.method public static dumpMac(Ljava/io/PrintWriter;J)V
    .locals 9
    .param p0    # Ljava/io/PrintWriter;
    .param p1    # J

    const/16 v8, 0x2d

    const-wide/16 v6, 0xff

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x28

    shr-long v2, p1, v2

    and-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p0, v8}, Ljava/io/PrintWriter;->print(C)V

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x20

    shr-long v2, p1, v2

    and-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p0, v8}, Ljava/io/PrintWriter;->print(C)V

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x18

    shr-long v2, p1, v2

    and-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p0, v8}, Ljava/io/PrintWriter;->print(C)V

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x10

    shr-long v2, p1, v2

    and-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p0, v8}, Ljava/io/PrintWriter;->print(C)V

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x8

    shr-long v2, p1, v2

    and-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {p0, v8}, Ljava/io/PrintWriter;->print(C)V

    const-string v0, "%02X"

    new-array v1, v5, [Ljava/lang/Object;

    and-long v2, p1, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    return-void
.end method


# virtual methods
.method public createProto(Z)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 4
    .param p1    # Z

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/data/WifiScan$Device;->ssid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Device [mac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rssi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
