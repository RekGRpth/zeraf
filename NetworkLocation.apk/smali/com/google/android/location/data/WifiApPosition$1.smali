.class final Lcom/google/android/location/data/WifiApPosition$1;
.super Ljava/lang/Object;
.source "WifiApPosition.java"

# interfaces
.implements Lcom/google/android/location/data/Persistent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/data/WifiApPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/data/Persistent",
        "<",
        "Lcom/google/android/location/data/WifiApPosition;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public load(Ljava/io/DataInput;)Lcom/google/android/location/data/WifiApPosition;
    .locals 9
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    sget-object v1, Lcom/google/android/location/data/Position;->SAVER:Lcom/google/android/location/data/Persistent;

    invoke-interface {v1, p1}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/location/data/Position;

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v5

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v7

    new-instance v0, Lcom/google/android/location/data/WifiApPosition;

    iget v1, v8, Lcom/google/android/location/data/Position;->latE7:I

    iget v2, v8, Lcom/google/android/location/data/Position;->lngE7:I

    iget v3, v8, Lcom/google/android/location/data/Position;->accuracyMm:I

    iget v4, v8, Lcom/google/android/location/data/Position;->confidence:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/data/WifiApPosition;-><init>(IIIII)V

    # setter for: Lcom/google/android/location/data/WifiApPosition;->outlierCount:I
    invoke-static {v0, v7}, Lcom/google/android/location/data/WifiApPosition;->access$002(Lcom/google/android/location/data/WifiApPosition;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v6

    throw v6
.end method

.method public bridge synthetic load(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/data/WifiApPosition$1;->load(Ljava/io/DataInput;)Lcom/google/android/location/data/WifiApPosition;

    move-result-object v0

    return-object v0
.end method

.method public save(Lcom/google/android/location/data/WifiApPosition;Ljava/io/DataOutput;)V
    .locals 2
    .param p1    # Lcom/google/android/location/data/WifiApPosition;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    sget-object v1, Lcom/google/android/location/data/Position;->SAVER:Lcom/google/android/location/data/Persistent;

    invoke-interface {v1, p1, p2}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    iget v1, p1, Lcom/google/android/location/data/WifiApPosition;->horizontalUncertaintyMm:I

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeInt(I)V

    # getter for: Lcom/google/android/location/data/WifiApPosition;->outlierCount:I
    invoke-static {p1}, Lcom/google/android/location/data/WifiApPosition;->access$000(Lcom/google/android/location/data/WifiApPosition;)I

    move-result v1

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeInt(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0
.end method

.method public bridge synthetic save(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/location/data/WifiApPosition;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/data/WifiApPosition$1;->save(Lcom/google/android/location/data/WifiApPosition;Ljava/io/DataOutput;)V

    return-void
.end method
