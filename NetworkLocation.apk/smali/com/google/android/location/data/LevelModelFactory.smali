.class public Lcom/google/android/location/data/LevelModelFactory;
.super Lcom/google/android/location/data/ProtoFactory;
.source "LevelModelFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/data/ProtoFactory",
        "<",
        "Lcom/google/android/location/data/LevelModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {p0, v0}, Lcom/google/android/location/data/ProtoFactory;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    return-void
.end method

.method private getLevelNumberE3(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method


# virtual methods
.method public create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/data/LevelModel;
    .locals 11
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v9, 0x3

    const/4 v8, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    invoke-virtual {p1, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v9, 0x1

    :try_start_0
    invoke-virtual {v3, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/location/data/ModelFactory;->createMacToAttributeId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v7

    const/4 v9, 0x2

    invoke-virtual {v3, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/location/data/ModelFactory;->createBagOfTrees(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/learning/BagOfTrees;

    move-result-object v1

    const/4 v9, 0x3

    invoke-virtual {v3, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/location/data/ModelFactory;->createBagOfTrees(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/learning/BagOfTrees;

    move-result-object v5

    if-eqz v1, :cond_0

    if-eqz v5, :cond_0

    new-instance v2, Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-direct {v2, v7, v1}, Lcom/google/android/location/data/SignalStrengthBagPredictor;-><init>(Ljava/util/Map;Lcom/google/android/location/learning/BagOfTrees;)V

    new-instance v6, Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-direct {v6, v7, v5}, Lcom/google/android/location/data/SignalStrengthBagPredictor;-><init>(Ljava/util/Map;Lcom/google/android/location/learning/BagOfTrees;)V

    invoke-direct {p0, v3}, Lcom/google/android/location/data/LevelModelFactory;->getLevelNumberE3(Lcom/google/gmm/common/io/protocol/ProtoBuf;)I

    move-result v4

    new-instance v9, Lcom/google/android/location/data/LevelModel;

    invoke-direct {v9, v2, v6, v4}, Lcom/google/android/location/data/LevelModel;-><init>(Lcom/google/android/location/data/SignalStrengthBagPredictor;Lcom/google/android/location/data/SignalStrengthBagPredictor;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v8, v9

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v9, "LevelModelFactory"

    const-string v10, "Binary data of model corrupted."

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/data/LevelModelFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/data/LevelModel;

    move-result-object v0

    return-object v0
.end method
