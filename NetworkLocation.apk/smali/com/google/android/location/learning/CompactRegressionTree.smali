.class public Lcom/google/android/location/learning/CompactRegressionTree;
.super Ljava/lang/Object;
.source "CompactRegressionTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;
    }
.end annotation


# instance fields
.field rootIndex:I

.field private final treeNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    return-void
.end method

.method public static read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/CompactRegressionTree;
    .locals 6
    .param p0    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    new-instance v3, Lcom/google/android/location/learning/CompactRegressionTree;

    invoke-direct {v3, v2}, Lcom/google/android/location/learning/CompactRegressionTree;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/google/android/location/learning/CompactRegressionTree;->setRoot(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {p0}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    move-result-object v1

    iget-object v5, v3, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/learning/CompactRegressionTree;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/learning/CompactRegressionTree;

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    iget v4, v0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public eval([F)F
    .locals 5
    .param p1    # [F

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Could not compute value of regression tree. No root of the tree was defined"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget v0, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$000(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    if-ne v3, v4, :cond_1

    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$100(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)Lcom/google/android/location/learning/LinearEquation;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/location/learning/LinearEquation;->eval([F)F

    move-result v3

    return v3

    :cond_1
    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$200(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I

    move-result v3

    aget v2, p1, v3

    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$300(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)F

    move-result v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_2

    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$400(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I

    move-result v0

    goto :goto_0

    :cond_2
    # getter for: Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I
    invoke-static {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->access$500(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public setRoot(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Root: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree;->rootIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    const-string v3, "(%d) %s\n"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/google/android/location/learning/CompactRegressionTree;->treeNodes:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    invoke-virtual {v2}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
