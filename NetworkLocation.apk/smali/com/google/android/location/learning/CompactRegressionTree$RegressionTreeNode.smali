.class Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;
.super Ljava/lang/Object;
.source "CompactRegressionTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/learning/CompactRegressionTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RegressionTreeNode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
    }
.end annotation


# instance fields
.field private equation:Lcom/google/android/location/learning/LinearEquation;

.field private leftTreeNodeIndex:I

.field private rightTreeNodeIndex:I

.field private splitIndex:I

.field private splitValue:F

.field private type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;


# direct methods
.method constructor <init>(IFII)V
    .locals 1
    .param p1    # I
    .param p2    # F
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->INNER:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    iput-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    iput p1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    iput p2, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    iput p3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    iput p4, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/learning/LinearEquation;)V
    .locals 1
    .param p1    # Lcom/google/android/location/learning/LinearEquation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    iput-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    iput-object p1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)Lcom/google/android/location/learning/LinearEquation;
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)F
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;)I
    .locals 1
    .param p0    # Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    return v0
.end method

.method static read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;
    .locals 8
    .param p0    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    invoke-static {}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->values()[Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    move-result-object v7

    aget-object v5, v7, v6

    sget-object v7, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    if-ne v5, v7, :cond_0

    invoke-static {p0}, Lcom/google/android/location/learning/LinearEquation;->read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/LinearEquation;

    move-result-object v0

    new-instance v7, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    invoke-direct {v7, v0}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;-><init>(Lcom/google/android/location/learning/LinearEquation;)V

    :goto_0
    return-object v7

    :cond_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v4

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    new-instance v7, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    invoke-direct {v7, v3, v4, v1, v2}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;-><init>(IFII)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;

    iget-object v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    iget-object v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    iget v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    iget v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    iget v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    iget v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    iget-object v4, v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    invoke-virtual {v3, v4}, Lcom/google/android/location/learning/LinearEquation;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    invoke-virtual {v1}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->ordinal()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    add-int v0, v1, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    invoke-virtual {v1}, Lcom/google/android/location/learning/LinearEquation;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->type:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    sget-object v1, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Leaf: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->equation:Lcom/google/android/location/learning/LinearEquation;

    invoke-virtual {v1}, Lcom/google/android/location/learning/LinearEquation;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[%d] <= %f (%d) : (%d)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->splitValue:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->leftTreeNodeIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;->rightTreeNodeIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
