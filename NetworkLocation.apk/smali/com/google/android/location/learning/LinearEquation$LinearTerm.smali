.class Lcom/google/android/location/learning/LinearEquation$LinearTerm;
.super Ljava/lang/Object;
.source "LinearEquation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/learning/LinearEquation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LinearTerm"
.end annotation


# instance fields
.field public final coefficient:F

.field public final index:I


# direct methods
.method public constructor <init>(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    iput p2, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    return-void
.end method

.method public static read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/LinearEquation$LinearTerm;
    .locals 3
    .param p0    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    new-instance v2, Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    invoke-direct {v2, v1, v0}, Lcom/google/android/location/learning/LinearEquation$LinearTerm;-><init>(IF)V

    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    iget v3, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    iget v4, v0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    iget v4, v0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "%+.4f"

    new-array v1, v4, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%+.4f * [%d]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
