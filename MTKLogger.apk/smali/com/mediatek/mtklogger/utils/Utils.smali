.class public Lcom/mediatek/mtklogger/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final ACTION_ADB_CMD:Ljava/lang/String; = "com.mediatek.mtklogger.ADB_CMD"

.field public static final ACTION_EXP_HAPPENED:Ljava/lang/String; = "com.mediatek.log2server.EXCEPTION_HAPPEND"

.field public static final ACTION_IPO_BOOT:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field public static final ACTION_IPO_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field public static final ACTION_LOG_STATE_CHANGED:Ljava/lang/String; = "com.mediatek.mtklogger.intent.action.LOG_STATE_CHANGED"

.field public static final ACTION_MDLOGGER_RESTART_DONE:Ljava/lang/String; = "com.mediatek.mdlogger.AUTOSTART_COMPLETE"

.field public static final ACTION_REMAINING_STORAGE_LOW:Ljava/lang/String; = "com.mediatek.mdlogger.REMAINING_STORAGE_LOW"

.field public static final ACTION_RUNNING_STAGE_CHANGED:Ljava/lang/String; = ""

.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "com.mediatek.mtklogger.MTKLoggerService"

.field public static final ACTION_TAGLOG_TO_LOG2SERVER:Ljava/lang/String; = "com.mediatek.syslogger.taglog"

.field public static final ADB_COMMAND_START:Ljava/lang/String; = "start"

.field public static final ADB_COMMAND_STOP:Ljava/lang/String; = "stop"

.field public static final ADB_COMMAND_SWITCH_TAGLOG:Ljava/lang/String; = "switch_taglog"

.field public static final AEE_EXP_PATH:Ljava/lang/String; = "aee_exp"

.field public static final ANDROID_VERSION_NUMBER:F

.field public static final BROADCAST_KEY_MDLOG_PATH:Ljava/lang/String; = "ModemLogPath"

.field public static final BROADCAST_KEY_MOBILELOG_PATH:Ljava/lang/String; = "MobileLogPath"

.field public static final BROADCAST_KEY_NETLOG_PATH:Ljava/lang/String; = "NetLogPath"

.field public static final BROADCAST_KEY_TAGLOG_RESULT:Ljava/lang/String; = "TaglogResult"

.field public static final BROADCAST_VAL_LOGTOOL_STOPPED:Ljava/lang/String; = "LogToolStopped"

.field public static final BROADCAST_VAL_TAGLOG_CANCEL:Ljava/lang/String; = "Cancel"

.field public static final BROADCAST_VAL_TAGLOG_FAILED:Ljava/lang/String; = "Failed"

.field public static final BROADCAST_VAL_TAGLOG_SUCCESS:Ljava/lang/String; = "Successful"

.field public static final CHECK_CMD_DURATION:I = 0x2710

.field public static final CONFIG_FILE_NAME:Ljava/lang/String; = "log_settings"

.field public static final CUSTOMIZE_CONFIG_FILE:Ljava/lang/String; = "/system/etc/mtklog-config.prop"

.field public static final DEFAULT_ADB_CMD_TARGET:I = 0x0

.field public static final DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LOG_PATH_TYPE:Ljava/lang/String; = "/mnt/sdcard"

.field public static final DEFAULT_LOG_SIZE:I = 0xc8

.field public static final DEFAULT_STORAGE_WATER_LEVEL:I = 0x1e

.field public static final DEFAULT_VALUE_NEED_RECOVER_RUNNING:Z = false

.field public static final DEFAULT_VALUE_SYSTME_TIME_CHANGE:I = 0x0

.field public static final DUAL_MODEM_LOG_PATH:Ljava/lang/String; = "dualmdlog"

.field public static final DURATION_CHECK_LOG_FOLDER:I = 0x1388

.field public static final DURATION_START_EACH_LOG:I = 0x12c

.field public static final DURATION_WAIT_LOG_INSTANCE_READY:I = 0x1f4

.field public static final EXTRA_ADB_CMD_NAME:Ljava/lang/String; = "cmd_name"

.field public static final EXTRA_ADB_CMD_TARGET:Ljava/lang/String; = "cmd_target"

.field public static final EXTRA_AFFECTED_LOG_TYPE:Ljava/lang/String; = "affected_log_type"

.field public static final EXTRA_FAIL_REASON:Ljava/lang/String; = "fail_reason"

.field public static final EXTRA_KEY_EXP_FROM_REBOOT:Ljava/lang/String; = "from_reboot"

.field public static final EXTRA_KEY_EXP_NAME:Ljava/lang/String; = "db_filename"

.field public static final EXTRA_KEY_EXP_PATH:Ljava/lang/String; = "path"

.field public static final EXTRA_KEY_EXP_ZZ:Ljava/lang/String; = "zz_filename"

.field public static final EXTRA_KEY_FROM_MAIN_ACTIVITY:Ljava/lang/String; = "extra_key_from_main_activity"

.field public static final EXTRA_KEY_FROM_TAGLOG:Ljava/lang/String; = "extra_key_from_taglog"

.field public static final EXTRA_LOG_NEW_STATE:Ljava/lang/String; = "log_new_state"

.field public static final EXTRA_REMAINING_STORAGE:Ljava/lang/String; = "remaining_storage"

.field public static final EXTRA_RUNNING_STAGE_CHANGE_EVENT:Ljava/lang/String; = "stage_event"

.field public static final EXTRA_SERVICE_STARTUP_TYPE:Ljava/lang/String; = "startup_type"

.field public static final EXTRA_VALUE_FROM_REBOOT:Ljava/lang/String; = "FROM_REBOOT"

.field public static final FAIL_REASON_DETAIL_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final FILTER_FILE:Ljava/lang/String; = "catcher_filter.bin"

.field public static final FLAG_REBOOT_ISSUE_SYSTEM_CARSH:Ljava/lang/String; = "2:"

.field public static final KEY_BEGIN_RECORDING_TIME:Ljava/lang/String; = "begin_recording_time"

.field public static final KEY_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_CONFIG_TAGLOG_ENABLED:Ljava/lang/String; = "com.mediatek.log.taglog.enabled"

.field public static final KEY_LOG2SERVER_SWITCH:Ljava/lang/String; = "log2server_dialog_show"

.field public static final KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_LOG_SIZE_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_LOG_SIZE_NETWORK:Ljava/lang/String; = "networklog_logsize"

.field public static final KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_MODEM_ASSERT_FILE_PATH:Ljava/lang/String; = "md_assert_file_path"

.field public static final KEY_MODEM_EXCEPTION_PATH:Ljava/lang/String; = "modem_exception_path"

.field public static final KEY_MODEM_MODE:Ljava/lang/String; = "log_mode"

.field public static final KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_REBOOT_EXCEPTION_DB:Ljava/lang/String; = "debug.mtk.aee.db"

.field public static final KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_START_AUTOMATIC_MOBILE:Ljava/lang/String; = "mobilelog_autostart"

.field public static final KEY_START_AUTOMATIC_MODEM:Ljava/lang/String; = "modemlog_autostart"

.field public static final KEY_START_AUTOMATIC_NETWORK:Ljava/lang/String; = "networklog_autostart"

.field public static final KEY_START_UI_REASON:Ljava/lang/String; = "reason_start"

.field public static final KEY_STATUS_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_STATUS_MOBILE:Ljava/lang/String; = "mobilelog_enable"

.field public static final KEY_STATUS_MODEM:Ljava/lang/String; = "modemlog_enable"

.field public static final KEY_STATUS_NETWORK:Ljava/lang/String; = "networklog_enable"

.field public static final KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

.field public static final KEY_SYSTEM_PROPERTY_NETLOG_RUNNING_FLAG:Ljava/lang/String;

.field public static final KEY_SYSTEM_PROPERTY_NETLOG_SAVING_PATH:Ljava/lang/String;

.field public static final KEY_SYSTME_TIME_CHANGE_FLAG:Ljava/lang/String; = "system_time_reset"

.field public static final KEY_TAGGING_DB:Ljava/lang/String; = "tagging_db"

.field public static final KEY_TAGGING_DEST:Ljava/lang/String; = "tagging_dest"

.field public static final KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_TAGGING_MOBILE:Ljava/lang/String; = "tagging_mobile"

.field public static final KEY_TAGGING_MODEM:Ljava/lang/String; = "tagging_modem"

.field public static final KEY_TAGGING_NETWORK:Ljava/lang/String; = "tagging_network"

.field public static final KEY_TAG_LOG_COMPRESSING:Ljava/lang/String; = "tag_log_compressing"

.field public static final KEY_TAG_LOG_ONGOING:Ljava/lang/String; = "tag_log_ongoing"

.field public static final LOG_NAME_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOG_PATH_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOG_PATH_TYPE_EXTERNAL_SD:Ljava/lang/String; = "/mnt/sdcard2"

.field public static final LOG_PATH_TYPE_INTERNAL_SD:Ljava/lang/String; = "/mnt/sdcard"

.field public static final LOG_PATH_TYPE_PHONE:Ljava/lang/String; = "/data"

.field public static final LOG_PHONE_STORAGE:I = 0x7f07001a

.field public static final LOG_PHONE_STORAGE_CMD:Ljava/lang/String; = "Log2emmc"

.field public static final LOG_PHONE_STORAGE_KEY:Ljava/lang/String; = "1"

.field public static final LOG_RUNNING_STATUS_MD1:I = 0x1

.field public static final LOG_RUNNING_STATUS_MD1_MD2:I = 0x3

.field public static final LOG_RUNNING_STATUS_MD2:I = 0x2

.field public static final LOG_RUNNING_STATUS_STOPPED:I = 0x0

.field public static final LOG_RUNNING_STATUS_UNKNOWN:I = -0x1

.field public static final LOG_SD_CARD:I = 0x7f07001b

.field public static final LOG_SD_CARD_CMD:Ljava/lang/String; = "Log2sd"

.field public static final LOG_SD_CARD_KEY:Ljava/lang/String; = "2"

.field public static final LOG_SIZE_MODE1_SIZE:I = 0x3e8

.field public static final LOG_TREE_FILE:Ljava/lang/String; = "file_tree.txt"

.field public static final LOG_TYPE_MOBILE:I = 0x1

.field public static final LOG_TYPE_MODEM:I = 0x2

.field public static final LOG_TYPE_NETWORK:I = 0x4

.field public static final LOG_TYPE_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final MANUAL_SAVE_LOG:Ljava/lang/String; = "SaveLogManually"

.field public static final MOBILE_LOG_PATH:Ljava/lang/String; = "mobilelog"

.field public static final MODEM_LOG_PATH:Ljava/lang/String; = "mdlog"

.field public static final MODEM_MODE_IDLE:Ljava/lang/String; = "0"

.field public static final MODEM_MODE_SD:Ljava/lang/String; = "2"

.field public static final MODEM_MODE_USB:Ljava/lang/String; = "1"

.field public static final MSG_CMD_TIMEOUT_BASE:I = 0x3e8

.field public static final MSG_LOG_STATE_CHANGED:I = 0x1

.field public static final MSG_RUNNING_STAGE_CHANGE:I = 0x3

.field public static final MSG_SD_TIMEOUT:I = 0x2

.field public static final MTKLOG_PATH:Ljava/lang/String; = "/mtklog/"

.field public static final NETWORK_LOG_PATH:Ljava/lang/String; = "netlog"

.field public static final NETWORK_STATE:Ljava/lang/String; = "state"

.field public static final NETWORK_STATE_MOBILE:Ljava/lang/String; = "mobile"

.field public static final NETWORK_STATE_NONE:Ljava/lang/String; = "none"

.field public static final NETWORK_STATE_WIFI:Ljava/lang/String; = "wifi"

.field public static final PROP_MONKEY:Ljava/lang/String; = "ro.monkey"

.field public static final REASON_CMD_TIMEOUT:Ljava/lang/String; = "9"

.field public static final REASON_COMMON:Ljava/lang/String; = "11"

.field public static final REASON_DAEMON_DIE:Ljava/lang/String; = "5"

.field public static final REASON_DAEMON_UNKNOWN:Ljava/lang/String; = "1"

.field public static final REASON_LOG_FOLDER_CREATE_FAIL:Ljava/lang/String; = "10"

.field public static final REASON_LOG_FOLDER_DELETED:Ljava/lang/String; = "8"

.field public static final REASON_SEND_CMD_FAIL:Ljava/lang/String; = "4"

.field public static final REASON_STORAGE_FULL:Ljava/lang/String; = "3"

.field public static final REASON_STORAGE_NOT_READY:Ljava/lang/String; = "2"

.field public static final REASON_STORAGE_UNAVAILABLE:Ljava/lang/String; = "7"

.field public static final REASON_UNSUPPORTED_LOG:Ljava/lang/String; = "6"

.field public static final RESERVED_STORAGE_SIZE:I = 0xa

.field public static final RUNNING_STAGE_IDLE:I = 0x0

.field public static final RUNNING_STAGE_POLLING_LOG:I = 0x3

.field public static final RUNNING_STAGE_STARTING_LOG:I = 0x1

.field public static final RUNNING_STAGE_STOPPING_LOG:I = 0x2

.field public static final SDCARD_SIZE:Ljava/lang/String; = "sdcardSize"

.field public static final SD_LACK_OF_SPACE:I = 0x192

.field public static final SD_NORMAL:I = 0x191

.field public static final SD_NOT_EXIST:I = 0x193

.field public static final SD_NOT_WRITABLE:I = 0x194

.field public static final SD_TIMEOUT:I = 0x9c40

.field public static final SERVICE_SHUTDOWN_TYPE_BAD_STORAGE:Ljava/lang/String; = "storage_full_or_lost"

.field public static final SERVICE_SHUTDOWN_TYPE_IPO:Ljava/lang/String; = "ipo_shutdown"

.field public static final SERVICE_SHUTDOWN_TYPE_SD_TIMEOUT:Ljava/lang/String; = "sd_timeout"

.field public static final SERVICE_STARTUP_TYPE_ADB:Ljava/lang/String; = "adb"

.field public static final SERVICE_STARTUP_TYPE_BOOT:Ljava/lang/String; = "boot"

.field public static final SERVICE_STARTUP_TYPE_EXCEPTION_HAPPEN:Ljava/lang/String; = "exception_happen"

.field public static final SERVICE_STARTUP_TYPE_IPO:Ljava/lang/String; = "ipo"

.field public static final SERVICE_STARTUP_TYPE_STORAGE_RECOVERY:Ljava/lang/String; = "storage_recovery"

.field public static final SERVICE_STARTUP_TYPE_UPDATE:Ljava/lang/String; = "update"

.field public static final SERVICE_STARTUP_TYPE_USER:Ljava/lang/String; = "user"

.field public static final SETTINGS_HAS_STARTED_DEBUG_MODE:Ljava/lang/String; = "hasStartedDebugMode"

.field public static final SETTINGS_IS_SWITCH_CHECKED:Ljava/lang/String; = "isSwitchChecked"

.field public static final STAGE_EVENT_POLLING_DONE:I = 0x4

.field public static final STAGE_EVENT_START_LOG:I = 0x1

.field public static final STAGE_EVENT_START_POLLING:I = 0x3

.field public static final STAGE_EVENT_STOP_LOG:I = 0x2

.field public static final START_CMD_PREFIX:Ljava/lang/String; = "runshell_command_start_"

.field public static final START_UI_REASON_LOW_STORAGE:Ljava/lang/String; = "low_storage"

.field public static final STOP_CMD_PREFIX:Ljava/lang/String; = "runshell_command_stop_"

.field public static final STORAGE_STATE_FULL:I = -0x2

.field public static final STORAGE_STATE_NOT_READY:I = -0x1

.field public static final STORAGE_STATE_OK:I = 0x1

.field public static final TAG:Ljava/lang/String; = "MTKLogger"

.field public static final TAGLOG_CONFIG_VALUE_DISABLE:I = 0x0

.field public static final TAGLOG_CONFIG_VALUE_ENABLE:I = 0x1

.field public static final TAGLOG_CONFIG_VALUE_INVALID:I = -0x1

.field public static final TAG_ASTL1:Ljava/lang/String; = "MDLog_ASTL1"

.field public static final TAG_DAK:Ljava/lang/String; = "MDLog_DAK"

.field public static final TAG_DMDSPMLT:Ljava/lang/String; = "MDLog_DMDSPMLT"

.field public static final TAG_L1:Ljava/lang/String; = "MDLog_L1"

.field public static final TAG_LOG_ENABLE:Ljava/lang/String; = "tagLogEnable"

.field public static final TAG_LOG_PATH:Ljava/lang/String; = "taglog"

.field public static final TAG_MD2GMLT:Ljava/lang/String; = "MDLog_MD2GMLT"

.field public static final TAG_PS:Ljava/lang/String; = "MDLog_PS"

.field public static final TIMEOUT_CMD:I = 0x4e20

.field public static final VALUE_BEGIN_RECORDING_TIME_DEFAULT:J = 0x0L

.field public static final VALUE_LOG_RUNNING_STATUS_IN_SYSPROP_OFF:Ljava/lang/String; = "0"

.field public static final VALUE_LOG_RUNNING_STATUS_IN_SYSPROP_ON:Ljava/lang/String; = "1"

.field public static final VALUE_START_AUTOMATIC_DEFAULT:Z = false

.field public static final VALUE_START_AUTOMATIC_OFF:Z = false

.field public static final VALUE_START_AUTOMATIC_ON:Z = true

.field public static final VALUE_STATUS_DEFAULT:I = 0x0

.field public static final VALUE_STATUS_RUNNING:I = 0x1

.field public static final VALUE_STATUS_STOPPED:I = 0x0

.field public static final ZZ_INTERNAL_LENGTH:I = 0xa

.field private static mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v5, "MTKLogger/Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Andriod version string = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v4, 0x40800000

    const-string v5, "."

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const-string v5, "."

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v5, "MTKLogger/Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Formated version number = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_0
    sput v4, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    const-string v5, "MTKLogger/Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Android version number="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    sget v5, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v5, v5

    const-wide v7, 0x4010cbc6a7ef9db2L

    cmpl-double v5, v5, v7

    if-ltz v5, :cond_2

    const-string v5, "persist.mtklog.log2sd.path"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

    const-string v5, "debug.netlog.writtingpath"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_NETLOG_SAVING_PATH:Ljava/lang/String;

    const-string v5, "debug.mtklog.netlog.Running"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_NETLOG_RUNNING_FLAG:Ljava/lang/String;

    :goto_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    const v6, 0x7f07000c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    const v6, 0x7f07000d

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    const v6, 0x7f07000b

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    const-string v6, "networklog_enable"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    const-string v6, "mobilelog_enable"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    const-string v6, "modemlog_enable"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    const-string v6, "mobilelog_autostart"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    const-string v6, "modemlog_autostart"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    const-string v6, "networklog_autostart"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_SIZE_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "mobilelog_logsize"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "modemlog_logsize"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "networklog_logsize"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_NETLOG_RUNNING_FLAG:Ljava/lang/String;

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    const-string v6, "debug.MB.running"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    const-string v6, "debug.mdlogger.Running"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    const v6, 0x7f070001

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    const v6, 0x7f070002

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    const v6, 0x7f070003

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    const-string v6, "need_recovery_mobile"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    const-string v6, "need_recovery_modem"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    const-string v6, "need_recovery_network"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    const-string v6, "netlog"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    const-string v6, "mobilelog"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    const-string v6, "mdlog"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "1"

    const v7, 0x7f070059

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "2"

    const v7, 0x7f07005a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "3"

    const v7, 0x7f07005b

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "4"

    const v7, 0x7f07005c

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "5"

    const v7, 0x7f07005d

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "6"

    const v7, 0x7f07005e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "7"

    const v7, 0x7f07005f

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "8"

    const v7, 0x7f070060

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "9"

    const v7, 0x7f070061

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "10"

    const v7, 0x7f070062

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    const-string v6, "11"

    const v7, 0x7f070063

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.mobile.enabled"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.modem.enabled"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.net.enabled"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.mobile.maxsize"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.modem.maxsize"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const-string v6, "com.mediatek.log.net.maxsize"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const/16 v6, 0x12c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const/16 v6, 0x258

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_SIZE_MAP:Landroid/util/SparseArray;

    const/16 v6, 0xc8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    const-string v6, "tagging_mobile"

    invoke-virtual {v5, v9, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    const-string v6, "tagging_modem"

    invoke-virtual {v5, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    const-string v6, "tagging_network"

    invoke-virtual {v5, v11, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/4 v5, 0x0

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    return-void

    :catch_0
    move-exception v0

    const-string v5, "MTKLogger/Utils"

    const-string v6, "Fail to parse version number directly, just use the first character"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "4."

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/high16 v4, 0x40800000

    goto/16 :goto_0

    :cond_1
    const v4, 0x40133333

    goto/16 :goto_0

    :cond_2
    const-string v5, "persist.radio.log2sd.path"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

    const-string v5, "persist.radio.writtingpath"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_NETLOG_SAVING_PATH:Ljava/lang/String;

    const-string v5, "persist.radio.netlog.Running"

    sput-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_NETLOG_RUNNING_FLAG:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkLogStarted(Landroid/content/SharedPreferences;)Z
    .locals 6
    .param p0    # Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    const/4 v4, 0x1

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {p0, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v4, v3, :cond_0

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static deleteFolder(Ljava/io/File;)V
    .locals 5
    .param p0    # Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    const-string v2, "MTKLogger/Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delete file :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    aget-object v2, v0, v1

    invoke-static {v2}, Lcom/mediatek/mtklogger/utils/Utils;->deleteFolder(Ljava/io/File;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getAvailableStorageSize(Ljava/lang/String;)I
    .locals 13
    .param p0    # Ljava/lang/String;

    const/4 v7, 0x1

    :goto_0
    const/4 v9, 0x3

    if-gt v7, v9, :cond_0

    :try_start_0
    new-instance v8, Landroid/os/StatFs;

    invoke-direct {v8, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSize()I

    move-result v9

    int-to-long v3, v9

    invoke-virtual {v8}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v9

    int-to-long v0, v9

    mul-long v9, v0, v3

    const-wide/32 v11, 0x100000

    div-long/2addr v9, v11

    long-to-int v2, v9

    const-string v9, "MTKLogger"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "-->getAvailableStorageSize(), path="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", size="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "MB"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v2

    :catch_0
    move-exception v5

    const-string v9, "MTKLogger"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Fail to get storage info from ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] by StatFs, try again(index="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-wide/16 v9, 0xc8

    :try_start_1
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_1
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    :cond_0
    const-string v9, "MTKLogger"

    const-string v10, "-->getAvailableStorageSize(), fail to get it by StatFs, unknown exception happen."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getCurrentVolumeState(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v1, :cond_0

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    sput-object v1, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mediatek/mtklogger/utils/Utils;->getVolumeState(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getFileSize(Ljava/lang/String;)J
    .locals 13
    .param p0    # Ljava/lang/String;

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    return-wide v8

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    :cond_2
    move-wide v8, v6

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_4

    array-length v10, v3

    if-nez v10, :cond_5

    :cond_4
    const-string v10, "MTKLogger"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Loop folder ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] get a null/empty list"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v0, v3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v1, v0, v4

    if-nez v1, :cond_6

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/mtklogger/utils/Utils;->getFileSize(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    goto :goto_2
.end method

.method public static getLogPath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v1, :cond_0

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    sput-object v1, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    const/4 v0, 0x0

    const-string v1, "/mnt/sdcard"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/mediatek/storage/StorageManagerEx;->getInternalStoragePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    const-string v1, "MTKLogger/Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to get detail log path string for type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return empty to avoid NullPointerException."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    :cond_1
    return-object v0

    :cond_2
    const-string v1, "/mnt/sdcard2"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/mediatek/storage/StorageManagerEx;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "/data"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "/data"

    goto :goto_0

    :cond_4
    const-string v1, "MTKLogger/Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported log path type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getLogPathType()Ljava/lang/String;
    .locals 9

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "MTKLogger/Utils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "-->getLogPathType(), key="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    const-string v6, "/system/etc/mtklog-config.prop"

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->KEY_SYSTEM_PROPERTY_LOG_PATH_TYPE:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v5, "/mnt/sdcard"

    :cond_1
    return-object v5

    :catch_0
    move-exception v4

    const-string v6, "MTKLogger/Utils"

    const-string v7, "Fail to close opened customization file."

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v3

    :goto_1
    :try_start_3
    const-string v6, "MTKLogger/Utils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Try to initial default log path, but read customize config file error!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    const-string v6, "MTKLogger/Utils"

    const-string v7, "Fail to close opened customization file."

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v0, :cond_2

    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_3
    throw v6

    :catch_3
    move-exception v4

    const-string v7, "MTKLogger/Utils"

    const-string v8, "Fail to close opened customization file."

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :catchall_1
    move-exception v6

    move-object v0, v1

    goto :goto_2

    :catch_4
    move-exception v3

    move-object v0, v1

    goto :goto_1
.end method

.method public static getVolumeState(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const-string v5, "Unknown"

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "MTKLogger/Utils"

    const-string v9, "Empty pathString when cal getVolumnState"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v5

    :goto_0
    return-object v6

    :cond_0
    :try_start_0
    const-class v7, Landroid/os/storage/StorageManager;

    const-string v8, "getVolumeState"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    if-eqz v4, :cond_2

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v8, :cond_1

    const-string v8, "storage"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/storage/StorageManager;

    sput-object v8, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    :cond_1
    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->mStorageManager:Landroid/os/storage/StorageManager;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    invoke-virtual {v4, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v5

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v8, "MTKLogger/Utils"

    const-string v9, "Fail to access StorageManager.getVolumnState(). No such method."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :try_start_1
    const-class v2, Landroid/os/Environment;

    const-string v8, "getStorageState"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v2, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v8, 0x0

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    invoke-virtual {v3, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v5, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v6, v5

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v8, "MTKLogger/Utils"

    const-string v9, "Fail to access Environment.getStorageState(). No such method."

    invoke-static {v8, v9, v1}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-object v6, v5

    goto :goto_0
.end method

.method public static logd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static loge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public static logi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
