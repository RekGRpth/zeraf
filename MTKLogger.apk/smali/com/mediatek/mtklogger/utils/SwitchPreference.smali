.class public Lcom/mediatek/mtklogger/utils/SwitchPreference;
.super Landroid/preference/Preference;
.source "SwitchPreference.java"


# instance fields
.field private isChecked:Z

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mCheckBox:Landroid/widget/CheckBox;

    iput-boolean v1, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    iput-object p1, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mContext:Landroid/content/Context;

    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->setLayoutResource(I)V

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isPersistent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/utils/SwitchPreference;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/utils/SwitchPreference;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->callOnStateChangeListener(Z)V

    return-void
.end method

.method private callOnStateChangeListener(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->setChecked(Z)V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->callChangeListener(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f09003e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isPersistent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->persistBoolean(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/mtklogger/utils/SwitchPreference$1;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference$1;-><init>(Lcom/mediatek/mtklogger/utils/SwitchPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method protected onClick()V
    .locals 2

    invoke-super {p0}, Landroid/preference/Preference;->onClick()V

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->setChecked(Z)V

    goto :goto_1
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/utils/SwitchPreference;->isChecked:Z

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/utils/SwitchPreference;->notifyChanged()V

    return-void
.end method
