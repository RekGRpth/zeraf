.class public Lcom/mediatek/mtklogger/settings/ModemLogSettings;
.super Landroid/preference/PreferenceActivity;
.source "ModemLogSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final KEY_MD_CATEGORY:Ljava/lang/String; = "modemlog_category"

.field public static final KEY_MD_LOGSIZE:Ljava/lang/String; = "modemlog_logsize"

.field public static final KEY_MD_MODE:Ljava/lang/String; = "log_mode"

.field public static final KEY_MD_PREFERENCE_SCREEN:Ljava/lang/String; = "modemlog_preference_screen"

.field private static final LIMIT_LOG_SIZE:I = 0x258


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

.field private mDefaultSharedPreferences:Landroid/content/SharedPreferences;

.field private mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

.field private mMdAutoStartPre:Landroid/preference/CheckBoxPreference;

.field private mMdLogModeList:Landroid/preference/ListPreference;

.field private mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

.field private mSdcardSize:I

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const-string v0, "MTKLogger/ModemLogSettings"

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/settings/ModemLogSettings;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/ModemLogSettings;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/mtklogger/settings/ModemLogSettings;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/settings/ModemLogSettings;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->setAllPreferencesEnable(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mtklogger/settings/ModemLogSettings;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/ModemLogSettings;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/mtklogger/settings/ModemLogSettings;)I
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/ModemLogSettings;

    iget v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mSdcardSize:I

    return v0
.end method

.method private findViews()V
    .locals 6

    const v5, 0x7f07003a

    const-string v1, "log_mode"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    sget v1, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v1, v1

    const-wide v3, 0x400ffdf3b645a1cbL

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    const-string v1, "modemlog_logsize"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/EditTextPreference;

    iput-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    :goto_0
    const-string v1, "modemlog_autostart"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdAutoStartPre:Landroid/preference/CheckBoxPreference;

    return-void

    :cond_0
    const-string v1, "modemlog_preference_screen"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    const-string v1, "modemlog_logsize"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    new-instance v1, Lcom/mediatek/mtklogger/settings/ModemLogSettings$1;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings$1;-><init>(Lcom/mediatek/mtklogger/settings/ModemLogSettings;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    const-string v2, "modemlog_logsize"

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setKey(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v5}, Landroid/preference/EditTextPreference;->setTitle(I)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v5}, Landroid/preference/EditTextPreference;->setDialogTitle(I)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    const v2, 0x7f07003b

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setSummary(I)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    const-string v2, "600"

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private getIntByObj(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initViews()V
    .locals 12

    const/16 v11, 0x258

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v5, "MTKLogger/ModemLogSettings"

    const-string v8, "initViews()"

    invoke-static {v5, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "log_settings"

    invoke-virtual {p0, v5, v7}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v5, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-direct {v5, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v8, "modemlog_switch"

    invoke-interface {v5, v8, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v5}, Lcom/mediatek/mtklogger/utils/Utils;->checkLogStarted(Landroid/content/SharedPreferences;)Z

    move-result v0

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    invoke-virtual {v5, v1}, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;->setChecked(Z)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    if-nez v0, :cond_2

    move v5, v6

    :goto_0
    invoke-virtual {v8, v5}, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;->setEnabled(Z)V

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    move v5, v6

    :goto_1
    invoke-direct {p0, v5}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->setAllPreferencesEnable(Z)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v8, "log_mode"

    const-string v9, ""

    invoke-interface {v5, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f070073

    invoke-virtual {p0, v5}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "MTKLogger/ModemLogSettings"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No default log mode value stored in default shared preference, try to get it from string res, logModeValue="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_1

    const-string v5, "MTKLogger/ModemLogSettings"

    const-string v8, "Fail to select the given mode, just take the first one."

    invoke-static {v5, v8}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v5, v4}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    iget-object v8, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v8

    aget-object v8, v8, v4

    invoke-virtual {v5, v8}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v5}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/widget/EditText;->setInputType(I)V

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v8, "sdcardSize"

    invoke-virtual {v5, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mSdcardSize:I

    const/4 v5, 0x4

    new-array v3, v5, [Ljava/lang/Object;

    const v5, 0x7f07000d

    invoke-virtual {p0, v5}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v7

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v6

    iget v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mSdcardSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v10

    const/4 v6, 0x3

    const-string v5, "/mnt/sdcard2"

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0x7f07001b

    :goto_2
    invoke-virtual {p0, v5}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v6

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    const v6, 0x7f07003c

    invoke-virtual {p0, v6, v3}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/EditTextPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    move v5, v7

    goto/16 :goto_0

    :cond_3
    move v5, v7

    goto/16 :goto_1

    :cond_4
    const v5, 0x7f07001a

    goto :goto_2
.end method

.method private removeManualTitle()V
    .locals 2

    const-string v1, "modemlog_preference_screen"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    const-string v1, "modemlog_category"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method private setAllPreferencesEnable(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setListeners()V
    .locals 2

    const-string v0, "MTKLogger/ModemLogSettings"

    const-string v1, "setListeners()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdAutoStartPre:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    new-instance v1, Lcom/mediatek/mtklogger/settings/ModemLogSettings$2;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings$2;-><init>(Lcom/mediatek/mtklogger/settings/ModemLogSettings;)V

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;->setOnCheckedChangeListener(Lcom/mediatek/mtklogger/settings/LogSwitchListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogSizeLimitPre:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/mediatek/mtklogger/settings/ModemLogSettings$3;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings$3;-><init>(Lcom/mediatek/mtklogger/settings/ModemLogSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->addPreferencesFromResource(I)V

    sget v0, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v0, v0

    const-wide v2, 0x400ffdf3b645a1cbL

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;-><init>(Landroid/preference/PreferenceActivity;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->removeManualTitle()V

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->findViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->initViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->setListeners()V

    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mBarSwitch:Lcom/mediatek/mtklogger/settings/OptionalActionBarSwitch;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->free()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x2

    const-string v2, "MTKLogger/ModemLogSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preference Change Key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newValue : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "log_mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    const-string v2, "MTKLogger/ModemLogSettings"

    const-string v3, "Fail to select the given mode, ignore."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mMdLogModeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "modemlog_logsize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-direct {p0, p2}, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->getIntByObj(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->setLogSize(II)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "modemlog_autostart"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/ModemLogSettings;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v5, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->setAutoStart(IZ)Z

    goto :goto_1
.end method
