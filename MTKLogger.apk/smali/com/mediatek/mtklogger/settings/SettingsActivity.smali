.class public Lcom/mediatek/mtklogger/settings/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final ACTION_DUMP:Ljava/lang/String; = "com.mediatek.logdumper.DUMP_ACTION"

.field private static final EXTRA_TARGET_VALUE:Ljava/lang/String; = "target_value"

.field public static final KEY_ADVANCED_LOG_STORAGE_LOCATION:Ljava/lang/String; = "log_storage_location"

.field public static final KEY_ADVANCED_RUN_COMMAND:Ljava/lang/String; = "run_command"

.field public static final KEY_ADVANCED_RUN_EXCEPTION_HISTORY:Ljava/lang/String; = "run_exception_history"

.field public static final KEY_ADVANCED_SETTINGS_CATEGORY:Ljava/lang/String; = "advanced_settings_category"

.field public static final KEY_EXCEPTIONREPORTER_ENABLE:Ljava/lang/String; = "exceptionreporter_enable"

.field public static final KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEY_MB_AUTOSTART:Ljava/lang/String; = "mobilelog_autostart"

.field public static final KEY_MB_SWITCH:Ljava/lang/String; = "mobilelog_switch"

.field public static final KEY_MD_AUTOSTART:Ljava/lang/String; = "modemlog_autostart"

.field public static final KEY_MD_SWITCH:Ljava/lang/String; = "modemlog_switch"

.field public static final KEY_NT_AUTOSTART:Ljava/lang/String; = "networklog_autostart"

.field public static final KEY_NT_SWITCH:Ljava/lang/String; = "networklog_switch"

.field public static final KEY_TAGLOG_ENABLE:Ljava/lang/String; = "taglog_enable"

.field public static final KEY_UI_DEBUG_MODE_ENABLE:Ljava/lang/String; = "ui_debug_mode_enable"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDefaultSharedPreferences:Landroid/content/SharedPreferences;

.field mExceptionHistory:Landroid/preference/PreferenceScreen;

.field private mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

.field private mLogStorageLocationList:Landroid/preference/ListPreference;

.field private mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

.field private mMbSwitchPre:Landroid/preference/SwitchPreference;

.field private mMdSwitchPre:Landroid/preference/SwitchPreference;

.field private mNtSwitchPre:Landroid/preference/SwitchPreference;

.field private mRunCmdEditText:Landroid/preference/EditTextPreference;

.field private mSdcardSize:I

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mTaglogEnable:Landroid/preference/CheckBoxPreference;

.field private mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    const-string v1, "mobilelog_switch"

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    const-string v1, "modemlog_switch"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    const-string v1, "networklog_switch"

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;

    const-string v1, "mobilelog_autostart"

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;

    const-string v1, "modemlog_autostart"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;

    const-string v1, "networklog_autostart"

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const-string v0, "MTKLogger/SettingsActivity"

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSdcardSize:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/preference/SwitchPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMbSwitchPre:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/mtklogger/settings/SettingsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setSdcardSize()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mtklogger/settings/SettingsActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSdcardSize:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/preference/SwitchPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMdSwitchPre:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/preference/SwitchPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mNtSwitchPre:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/mtklogger/settings/SettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/settings/SettingsActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private findViews()V
    .locals 2

    const-string v0, "mobilelog_switch"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMbSwitchPre:Landroid/preference/SwitchPreference;

    const-string v0, "modemlog_switch"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMdSwitchPre:Landroid/preference/SwitchPreference;

    const-string v0, "networklog_switch"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mNtSwitchPre:Landroid/preference/SwitchPreference;

    const-string v0, "taglog_enable"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    const-string v0, "exceptionreporter_enable"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    const-string v0, "ui_debug_mode_enable"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    const-string v0, "log_storage_location"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    const-string v0, "run_command"

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mRunCmdEditText:Landroid/preference/EditTextPreference;

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "run_exception_history"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    return-void
.end method

.method private initViews()V
    .locals 9

    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "log_settings"

    invoke-virtual {p0, v3, v5}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v3, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-direct {v3, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    const-string v3, "MTKLogger/SettingsActivity"

    const-string v6, "Hide log2server_app"

    invoke-static {v3, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "advanced_settings_category"

    invoke-virtual {p0, v3}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v8, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v8, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v6, "eng"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "MTKLogger/SettingsActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initViews() BuildType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "advanced_settings_category"

    invoke-virtual {p0, v3}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v8, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "tagLogEnable"

    invoke-interface {v3, v6, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "log2server_dialog_show"

    invoke-static {v3, v6, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setSdcardSize()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->updateUI()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setLogStorageEntries()V

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "tagLogEnable"

    invoke-interface {v6, v7, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_4
    :try_start_0
    iget-object v6, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v7, "log2server_dialog_show"

    invoke-static {v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v4, :cond_7

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    if-eqz v3, :cond_6

    iget-object v6, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    :goto_4
    invoke-virtual {v6, v3}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    :cond_6
    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "hasStartedDebugMode"

    invoke-interface {v3, v6, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "hasStartedDebugMode"

    invoke-interface {v3, v6, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-direct {p0, v5}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setUIDebugMode(Z)V

    goto :goto_1

    :cond_7
    move v3, v5

    goto :goto_2

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_3

    :cond_8
    move v3, v5

    goto :goto_4
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMbSwitchPre:Landroid/preference/SwitchPreference;

    new-instance v1, Lcom/mediatek/mtklogger/settings/SettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity$1;-><init>(Lcom/mediatek/mtklogger/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMdSwitchPre:Landroid/preference/SwitchPreference;

    new-instance v1, Lcom/mediatek/mtklogger/settings/SettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity$2;-><init>(Lcom/mediatek/mtklogger/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mNtSwitchPre:Landroid/preference/SwitchPreference;

    new-instance v1, Lcom/mediatek/mtklogger/settings/SettingsActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity$3;-><init>(Lcom/mediatek/mtklogger/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mTaglogEnable:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/mediatek/mtklogger/settings/SettingsActivity$4;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity$4;-><init>(Lcom/mediatek/mtklogger/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mUIDebugModeEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mRunCmdEditText:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_2
    return-void
.end method

.method private setLogStorageEntries()V
    .locals 8

    const v5, 0x7f07001b

    const v6, 0x7f07001a

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v4, "/mnt/sdcard"

    invoke-static {p0, v4}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/mediatek/mtklogger/utils/Utils;->getVolumeState(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v6}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v4, "1"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v4, "/mnt/sdcard2"

    invoke-static {p0, v4}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/mediatek/mtklogger/utils/Utils;->getVolumeState(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v5}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v4, "2"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_2
    iget-object v7, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/CharSequence;

    invoke-virtual {v7, v4}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/CharSequence;

    invoke-virtual {v7, v4}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    const-string v4, "/mnt/sdcard2"

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v2, "2"

    :goto_1
    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-virtual {v4, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    const-string v4, "2"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    :cond_3
    const-string v2, "1"

    goto :goto_1

    :cond_4
    move v4, v6

    goto :goto_2
.end method

.method private setSdcardSize()V
    .locals 5

    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    div-int/lit16 v0, v3, 0x400

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v3

    mul-int/2addr v3, v0

    div-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSdcardSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "MTKLogger/SettingsActivity"

    const-string v4, "setSdcardSize() : StatFs error, maybe currentLogPath is invalid"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    iput v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSdcardSize:I

    goto :goto_0
.end method

.method private setUIDebugMode(Z)V
    .locals 3
    .param p1    # Z

    :try_start_0
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/view/IWindowManager;->enableGuiLog(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MTKLogger/SettingsActivity"

    const-string v2, "Exception happens when set ui debug mode"

    invoke-static {v1, v2, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateUI()V
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v2, "MTKLogger/SettingsActivity"

    const-string v5, "updateUI()"

    invoke-static {v2, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/mediatek/mtklogger/utils/Utils;->checkLogStarted(Landroid/content/SharedPreferences;)Z

    move-result v0

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMbSwitchPre:Landroid/preference/SwitchPreference;

    if-nez v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMdSwitchPre:Landroid/preference/SwitchPreference;

    if-nez v0, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mNtSwitchPre:Landroid/preference/SwitchPreference;

    if-nez v0, :cond_3

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_4

    :cond_0
    const-string v2, "MTKLogger/SettingsActivity"

    const-string v5, "Log storage entry is null or empty, disable storage set item"

    invoke-static {v2, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    invoke-virtual {v2, v4}, Landroid/preference/ListPreference;->setEnabled(Z)V

    :goto_3
    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMbSwitchPre:Landroid/preference/SwitchPreference;

    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "mobilelog_switch"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mMdSwitchPre:Landroid/preference/SwitchPreference;

    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "modemlog_switch"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mNtSwitchPre:Landroid/preference/SwitchPreference;

    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "networklog_switch"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mLogStorageLocationList:Landroid/preference/ListPreference;

    if-nez v0, :cond_5

    move v4, v3

    :cond_5
    invoke-virtual {v2, v4}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_3
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040003

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->findViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->initViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setListeners()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->free()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v1, "MTKLogger/SettingsActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Preference Change Key : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newValue : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v4, "exceptionreporter_enable"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "log2server_dialog_show"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    invoke-static {v4, v5, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionReporterEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_0

    move v3, v2

    :cond_0
    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    :cond_1
    :goto_1
    return v2

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ui_debug_mode_enable"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setUIDebugMode(Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v3, "log_storage_location"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "/mnt/sdcard2"

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "2"

    :goto_2
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "2"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x7f07001b

    :goto_3
    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    const-string v1, "2"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "Log2sd"

    :goto_4
    invoke-virtual {v3, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->runCommand(Ljava/lang/String;)Z

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->setSdcardSize()V

    goto :goto_1

    :cond_5
    const-string v0, "1"

    goto :goto_2

    :cond_6
    const v1, 0x7f07001a

    goto :goto_3

    :cond_7
    const-string v1, "Log2emmc"

    goto :goto_4

    :cond_8
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v3, "run_command"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->runCommand(Ljava/lang/String;)Z

    goto/16 :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mtklogger/settings/SettingsActivity;->mExceptionHistory:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/mediatek/mtklogger/exceptionreporter/ExceptionReportManager;->runExceptionHistory(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "MTKLogger/SettingsActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/settings/SettingsActivity;->updateUI()V

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    return-void
.end method
