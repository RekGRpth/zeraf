.class Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;
.super Landroid/content/BroadcastReceiver;
.source "MTKLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/MTKLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Storage status changed, action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", current logPathType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$000(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "/mnt/sdcard2"

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$000(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "/mnt/sdcard"

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$000(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    const/4 v1, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-static {v5}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z
    invoke-static {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$100(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Z

    move-result v4

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AffectedPath="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", new got currentLogPath="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isAnyLogRuning?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", cached recording log path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_3
    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "SD card change was not happend in current log path, ignore."

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    const-string v5, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_6
    sget-boolean v5, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isInIPOShutdown:Z

    if-eqz v5, :cond_7

    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Device is in IPO shut down phase, mark reason as ipo_shutdown."

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v6, "ipo_shutdown"

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v5, v8, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Send ipo_shutdown to native, stop service now."

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-virtual {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->stopSelf()V

    goto :goto_0

    :cond_7
    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v6, "storage_full_or_lost"

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v5, v8, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    goto :goto_0

    :cond_8
    const-string v5, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$400(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z
    invoke-static {}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$500()Z

    move-result v5

    if-eqz v5, :cond_9

    # setter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$502(Z)Z

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # getter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;
    invoke-static {v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$600(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v5, v9, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    goto :goto_0

    :cond_9
    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v6, "storage_recovery"

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v5, v9, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    goto :goto_0

    :cond_a
    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported broadcast action for SD card. action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v5, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Phone storage is low now. What should I do? "

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v6, "storage_full_or_lost"

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v5, v8, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    goto/16 :goto_0
.end method
