.class public Lcom/mediatek/mtklogger/framework/MTKLoggerService;
.super Landroid/app/Service;
.source "MTKLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MTKLogger/MTKLoggerService"

.field public static isInIPOShutdown:Z

.field private static isStarted:Z

.field private static isWaitingSDReady:Z

.field private static mNM:Landroid/app/NotificationManager;


# instance fields
.field private failReasonStr:Ljava/lang/String;

.field public mBind:Lcom/mediatek/mtklogger/IMTKLoggerManager$Stub;

.field private mCachedStartStopCmd:I

.field private mCurrentAffectedLogType:I

.field private mCurrentLogPathType:Ljava/lang/String;

.field private mCurrentRecordingLogPath:Ljava/lang/String;

.field private mCurrentRunningStatus:I

.field private mDefaultSharedPreferences:Landroid/content/SharedPreferences;

.field private mGlobalRunningStage:I

.field private mIPOIntentFilter:Landroid/content/IntentFilter;

.field private mIPOReceiver:Landroid/content/BroadcastReceiver;

.field private mLogFolderMonitorThreadStopFlag:Z

.field private mLogInstanceMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/mediatek/mtklogger/framework/LogInstance;",
            ">;"
        }
    .end annotation
.end field

.field mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

.field private mNativeStateHandler:Landroid/os/Handler;

.field private mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

.field private mRemainingStorage:I

.field private mSDStatusIntentFilter:Landroid/content/IntentFilter;

.field private mServiceStartType:Ljava/lang/String;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mStorageStatusReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStarted:Z

    sput-boolean v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    sput-boolean v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isInIPOShutdown:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogInstanceMap:Landroid/util/SparseArray;

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    iput v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mGlobalRunningStage:I

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    iput v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$1;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$2;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOReceiver:Landroid/content/BroadcastReceiver;

    iput v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    iput v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    iput v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    new-instance v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$4;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mBind:Lcom/mediatek/mtklogger/IMTKLoggerManager$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/mtklogger/framework/MTKLoggerService;I)Lcom/mediatek/mtklogger/framework/LogInstance;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;Landroid/os/Handler;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/os/Message;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleLogStateChangeMsg(Landroid/os/Handler;Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleGlobalRunningStageChange(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->beginTagLog()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)I
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getGlobalRunningStage()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    return p0
.end method

.method static synthetic access$600(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStorageReady()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->checkRemainingStorage()V

    return-void
.end method

.method private beginTagLog()Z
    .locals 3

    const-string v1, "MTKLogger/MTKLoggerService"

    const-string v2, "-->beginTagLog()"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "path"

    const-string v2, "SaveLogManually"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/mtklogger/taglog/TagLogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->beginTag(Landroid/content/Intent;)V

    const/4 v1, 0x1

    return v1
.end method

.method private changeLogRunningStatus(ZLjava/lang/String;)V
    .locals 11
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "-->changeLogRunningStatus(), enable?"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", reason=["

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "]"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v5, :cond_1

    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "SharedPreference instance is null"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    if-eqz p1, :cond_7

    const-string v5, "boot"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "ipo"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_2
    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v6, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-interface {v9, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-ne v7, v5, :cond_3

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_4
    const-string v5, "storage_recovery"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v0, v5

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2

    :cond_6
    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " affectedLog="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v0, :cond_0

    invoke-virtual {p0, v0, p2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->startRecording(ILjava/lang/String;)Z

    goto/16 :goto_0

    :cond_7
    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v7, v5, :cond_a

    move v2, v7

    :goto_4
    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_START_AUTOMATIC_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->DEFAULT_CONFIG_LOG_AUTO_START_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v6, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-interface {v9, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-ne v7, v5, :cond_b

    move v4, v7

    :goto_5
    if-nez v2, :cond_9

    if-eqz v4, :cond_8

    const-string v5, "sd_timeout"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_9
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v0, v5

    if-eqz v2, :cond_8

    const-string v5, "storage_full_or_lost"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->KEY_NEED_RECOVER_RUNNING_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_3

    :cond_a
    move v2, v8

    goto :goto_4

    :cond_b
    move v4, v8

    goto :goto_5

    :cond_c
    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " affectedLog="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v0, :cond_0

    invoke-virtual {p0, v0, p2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->stopRecording(ILjava/lang/String;)Z

    goto/16 :goto_0
.end method

.method private checkRemainingStorage()V
    .locals 10

    const v9, 0x7f070009

    const/4 v8, 0x0

    const v7, 0x7f020006

    const/16 v5, 0x1e

    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/mtklogger/utils/Utils;->getAvailableStorageSize(Ljava/lang/String;)I

    move-result v3

    if-ge v3, v5, :cond_3

    iget v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    if-lt v4, v5, :cond_3

    :cond_0
    const-string v4, "MTKLogger/MTKLoggerService"

    const-string v5, "Remaining log storage drop below water level, give a notification now"

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    if-nez v4, :cond_1

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    sput-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    :cond_1
    const-string v4, "MTKLogger/MTKLoggerService"

    const-string v5, "Log storage drop down below water level, give out a notification"

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iput v7, v1, Landroid/app/Notification;->icon:I

    invoke-virtual {p0, v9}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.mediatek.mtklogger"

    const-string v6, "com.mediatek.mtklogger.MainActivity"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v4, 0x20000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, "reason_start"

    const-string v5, "low_storage"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p0, v9}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f07000a

    invoke-virtual {p0, v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, p0, v4, v5, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    sget-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_2
    :goto_0
    iput v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    return-void

    :cond_3
    iget v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    if-lez v4, :cond_2

    iget v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    if-ge v4, v5, :cond_2

    if-lt v3, v5, :cond_2

    sget-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    if-nez v4, :cond_4

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    sput-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    :cond_4
    const-string v4, "MTKLogger/MTKLoggerService"

    const-string v5, "Log storage resume upto water level, clear former notification"

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v7}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method private dealWithAdbCommand(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const-string v4, "MTKLogger/MTKLoggerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-->dealWithAdbCommand(), logTypeCluster="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", command="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "start"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "adb"

    invoke-virtual {p0, p1, v4}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->startRecording(ILjava/lang/String;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "stop"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->stopRecording(ILjava/lang/String;)Z

    goto :goto_0

    :cond_2
    sget-object v4, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int v4, v3, p1

    if-eqz v4, :cond_3

    invoke-direct {p0, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v4, "MTKLogger/MTKLoggerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send adb command ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] to log "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v2, Lcom/mediatek/mtklogger/framework/LogInstance;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    if-eqz v0, :cond_4

    const/16 v4, 0x8

    invoke-virtual {v0, v4, p2}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    :cond_4
    const-string v4, "MTKLogger/MTKLoggerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "When dealWithAdbCommand(), fail to get log instance handler of log ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v4, "MTKLogger/MTKLoggerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Fail to get log instance("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") when dealing with adb command"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private getGlobalRunningStage()I
    .locals 9

    iget v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mGlobalRunningStage:I

    const/4 v5, 0x0

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/mtklogger/framework/LogInstance;->getGlobalRunningStage()I

    move-result v4

    if-le v4, v3, :cond_0

    move v5, v2

    move v3, v4

    goto :goto_0

    :cond_1
    const-string v6, "MTKLogger/MTKLoggerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<--getGlobalRunningStage(), current stage="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogInstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    invoke-static {p1, p0, v1}, Lcom/mediatek/mtklogger/framework/LogInstance;->getInstance(ILandroid/content/Context;Landroid/os/Handler;)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogInstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogInstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mtklogger/framework/LogInstance;

    return-object v1
.end method

.method private handleGlobalRunningStageChange(I)V
    .locals 4
    .param p1    # I

    const-string v1, "MTKLogger/MTKLoggerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-->handleGlobalRunningStageChange(), stageEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", 1:start; 2:stop; 3:polling; 4:polling done."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "stage_event"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-void
.end method

.method private handleLogStateChangeMsg(Landroid/os/Handler;Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/os/Message;

    const/4 v8, 0x0

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "-->handleLogStateChangeMsg(), mCachedStartStopCmd = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget v4, p2, Landroid/os/Message;->what:I

    const/16 v5, 0x3e8

    if-le v4, v5, :cond_4

    add-int/lit16 v1, v4, -0x3e8

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    and-int/2addr v5, v1

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    xor-int/2addr v5, v1

    iput v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "9"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    :cond_0
    :goto_0
    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    if-nez v5, :cond_2

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Notify other modules current state with broadcast. mCurrentAffectedLogType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", currentRunningStatus="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", failReasonStr=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.mediatek.mtklogger.intent.action.LOG_STATE_CHANGED"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "affected_log_type"

    iget v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "log_new_state"

    iget v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "fail_reason"

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateStartRecordingTime()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateLogFolderMonitor()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    :goto_1
    iput v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    iput v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    const-string v5, ""

    iput-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    iput v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mGlobalRunningStage:I

    :cond_2
    :goto_2
    return-void

    :cond_3
    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Attention: timeout message should have been removed."

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    iget v1, p2, Landroid/os/Message;->arg1:I

    iget v3, p2, Landroid/os/Message;->arg2:I

    iget-object v2, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    const-string v5, "MTKLogger/MTKLoggerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " MSG_LOG_STATE_CHANGED, logType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", result="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", reason=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_5

    const-string v6, ""

    move-object v5, v2

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->failReasonStr:Ljava/lang/String;

    :cond_5
    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    and-int/2addr v5, v1

    if-eqz v5, :cond_6

    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Still waiting start/stop command response, mark it as finished."

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    xor-int/2addr v5, v1

    iput v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    add-int/lit16 v5, v1, 0x3e8

    invoke-virtual {p1, v5}, Landroid/os/Handler;->removeMessages(I)V

    :goto_3
    if-lez v3, :cond_7

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    or-int/2addr v5, v1

    iput v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    goto/16 :goto_0

    :cond_6
    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "No cached start/stop command for this log, treat it as self change"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    or-int/2addr v5, v1

    iput v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    goto :goto_3

    :cond_7
    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    and-int/2addr v5, v1

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    xor-int/2addr v5, v1

    iput v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    goto/16 :goto_0

    :cond_8
    const-string v5, "MTKLogger/MTKLoggerService"

    const-string v6, "Unknown message"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method private isAnyLogRunning()Z
    .locals 7

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v5, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v4, v3, :cond_0

    const/4 v1, 0x1

    :cond_1
    const-string v3, "MTKLogger/MTKLoggerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<--isAnyLogRunning()? "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method private isStorageReady()Z
    .locals 4

    const/4 v1, 0x1

    const-string v2, "/data"

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "MTKLogger/MTKLoggerService"

    const-string v3, "For phone internal storage, assume it\'s already ready"

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentVolumeState(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "mounted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateLogFolderMonitor()V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z

    move-result v0

    const-string v1, "MTKLogger/MTKLoggerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-->updateLogFolderMonitor(), isLogRunning="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mLogFolderMonitorThreadStopFlag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;)V

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    invoke-virtual {v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;->start()V

    iput-boolean v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    const-string v1, "MTKLogger/MTKLoggerService"

    const-string v2, "Log is running, so start monitor log folder"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    if-nez v1, :cond_0

    const-string v1, "MTKLogger/MTKLoggerService"

    const-string v2, "Log is stopped, so need to stop log folder monitor if any exist."

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    invoke-virtual {v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;->interrupt()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mMonitorLogFolderThread:Lcom/mediatek/mtklogger/framework/MTKLoggerService$LogFolderMonitor;

    :cond_2
    sget-object v1, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    if-nez v1, :cond_3

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    sput-object v1, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    :cond_3
    sget-object v1, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNM:Landroid/app/NotificationManager;

    const v2, 0x7f020006

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    iput v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mRemainingStorage:I

    goto :goto_0
.end method

.method private updateLogStatus()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x1

    const-string v7, "MTKLogger/MTKLoggerService"

    const-string v8, "-->updateLogStatus()"

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/mtklogger/framework/LogInstance;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/framework/LogInstance;-><init>(Landroid/content/Context;)V

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v7, "0"

    invoke-static {v3, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "MTKLogger/MTKLoggerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Native log("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") running status="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "1"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v8, v7, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v1, v8, v7, v10}, Lcom/mediatek/mtklogger/framework/LogInstance;->updateStatusBar(IIZ)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v4

    iget-object v7, v4, Lcom/mediatek/mtklogger/framework/LogInstance;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    if-eqz v7, :cond_0

    iget-object v7, v4, Lcom/mediatek/mtklogger/framework/LogInstance;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    const/16 v8, 0x9

    invoke-virtual {v7, v8}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_0
    const-string v7, "MTKLogger/MTKLoggerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "When updateLogStatus(), fail to get log instance handler of log ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v8, v7, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_TITLE_RES_IN_STSTUSBAR_MAP:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v1, v8, v7, v11}, Lcom/mediatek/mtklogger/framework/LogInstance;->updateStatusBar(IIZ)V

    goto/16 :goto_0

    :cond_2
    sput-boolean v10, Lcom/mediatek/mtklogger/framework/LogReceiver;->bootupFlag:Z

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateLogFolderMonitor()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateStartRecordingTime()V

    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.mediatek.mtklogger.intent.action.LOG_STATE_CHANGED"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_3
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRecordingLogPath:Ljava/lang/String;

    goto :goto_1
.end method

.method private updateStartRecordingTime()V
    .locals 8

    const-wide/16 v6, 0x0

    const-string v3, "MTKLogger/MTKLoggerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-->updateStartRecordingTime(), mCurrentRunningStatus="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isAnyLogRunning()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "begin_recording_time"

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "begin_recording_time"

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v3, v0, v6

    if-gtz v3, :cond_0

    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "Former log start time is 0, set to current system time."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "begin_recording_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "system_time_reset"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method


# virtual methods
.method public getLogInstanceRunningStatus(I)I
    .locals 5
    .param p1    # I

    const-string v2, "MTKLogger/MTKLoggerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-->getLogInstanceRunningStatus(), logType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/LogInstance;->getLogRunningStatus()I

    move-result v1

    :cond_0
    const-string v2, "MTKLogger/MTKLoggerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<--getLogInstanceRunningStatus(), status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "MTKLogger/MTKLoggerService"

    const-string v1, "-->onBind()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mBind:Lcom/mediatek/mtklogger/IMTKLoggerManager$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MTKLogger/MTKLoggerService"

    const-string v1, "-->onCreate()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "log_settings"

    invoke-virtual {p0, v0, v2}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSDStatusIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mPhoneStorageIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentLogPathType:Ljava/lang/String;

    sput-boolean v2, Lcom/mediatek/mtklogger/framework/LogReceiver;->canKillSelf:Z

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const-string v1, "MTKLogger/MTKLoggerService"

    const-string v2, "-->onDestroy()"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MTKLogger/MTKLoggerService"

    const-string v2, "Monkey is running, MTKLoggerService destroy failed!"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mIPOReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mLogFolderMonitorThreadStopFlag:Z

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v3, "MTKLogger/MTKLoggerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-->onStartCommand(), isStarted="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStarted:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", old mServiceStartType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_7

    const-string v3, "startup_type"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    const-string v3, "MTKLogger/MTKLoggerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " new mServiceStartType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    sget-boolean v3, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStarted:Z

    if-nez v3, :cond_4

    const-string v3, "MTKLogger/MTKLoggerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MTKLoggerService.onStartCommand mServiceStartType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", thread name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateLogStatus()V

    const-string v3, "boot"

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ipo"

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    sput-boolean v7, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isInIPOShutdown:Z

    const-string v3, "debug.mtk.aee.db"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "2:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "mobilelog_enable"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v8, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "mobilelog_enable"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStorageReady()Z

    move-result v3

    if-eqz v3, :cond_8

    sput-boolean v7, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-direct {p0, v8, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V

    :cond_3
    :goto_1
    sput-boolean v8, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isStarted:Z

    :cond_4
    sget-boolean v3, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    if-nez v3, :cond_9

    const-string v3, "adb"

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "cmd_target"

    invoke-virtual {p1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v3, "cmd_name"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->dealWithAdbCommand(ILjava/lang/String;)V

    :cond_5
    :goto_2
    if-nez p1, :cond_6

    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "Check whether need to resume TagLog process"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/mtklogger/taglog/TagLogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->resumeTag()V

    :cond_6
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v3

    return v3

    :cond_7
    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "intent == null, maybe this service is restarted by system."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "At bootup/IPO time, SD is not ready yet, wait."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v8, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "Storage is not ready yet, waiting for mounted signal."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    const-wide/32 v5, 0x9c40

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    :cond_9
    const-string v3, "update"

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "Modem restart finished, update log running status now."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->updateLogStatus()V

    goto :goto_2

    :cond_a
    const-string v3, "exception_happen"

    iget-object v4, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mServiceStartType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "MTKLogger/MTKLoggerService"

    const-string v4, "Got exception happens message, begin to tag log now."

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/mtklogger/taglog/TagLogManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->beginTag(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method public startRecording(ILjava/lang/String;)Z
    .locals 12
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const-string v8, "MTKLogger/MTKLoggerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-->startRecording(), logTypeCluster="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", reason="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    const-string v8, "MTKLogger/MTKLoggerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MTKLoggerService.startRecording() thread name = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    if-eqz v8, :cond_0

    const-string v8, "MTKLogger/MTKLoggerService"

    const-string v9, "Server is busy dealing former command, wait till it is free please"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_0
    sget-boolean v8, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z

    if-eqz v8, :cond_1

    const-string v8, "MTKLogger/MTKLoggerService"

    const-string v9, "Server is waiting for SD ready, wait till it is OK please"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    iput p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    and-int/2addr v8, v6

    if-eqz v8, :cond_2

    invoke-direct {p0, v6}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v3, v5, Lcom/mediatek/mtklogger/framework/LogInstance;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    if-eqz v3, :cond_4

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    add-int/lit16 v10, v6, 0x3e8

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    const-wide/16 v10, 0x4e20

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    int-to-double v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x4000000000000000L

    invoke-static {v10, v11}, Ljava/lang/Math;->log(D)D

    move-result-wide v10

    div-double v0, v8, v10

    const-string v8, "MTKLogger/MTKLoggerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "When start recording, for log ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "], delay index="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v8, "storage_recovery"

    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "MTKLogger/MTKLoggerService"

    const-string v9, "For storage recovery event, wait more time for its ready"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x64

    :cond_3
    const/4 v8, 0x1

    invoke-virtual {v3, v8, p2}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    double-to-int v9, v0

    mul-int/lit16 v9, v9, 0x12c

    add-int/2addr v9, v2

    int-to-long v9, v9

    invoke-virtual {v3, v8, v9, v10}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v7, 0x1

    goto :goto_1

    :cond_4
    const-string v8, "MTKLogger/MTKLoggerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "When startRecording(), fail to get log instance handler  of log ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const-string v11, "4"

    invoke-virtual {v8, v9, v6, v10, v11}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    :cond_5
    const-string v8, "MTKLogger/MTKLoggerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Fail to get log instance of type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const-string v11, "6"

    invoke-virtual {v8, v9, v6, v10, v11}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    :cond_6
    if-eqz v7, :cond_7

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleGlobalRunningStageChange(I)V

    const/4 v8, 0x1

    iput v8, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mGlobalRunningStage:I

    :cond_7
    move v8, v7

    goto/16 :goto_0
.end method

.method public stopRecording(ILjava/lang/String;)Z
    .locals 13
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v6, "MTKLogger/MTKLoggerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "-->stopRecording(), logTypeCluster="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", reason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    iget v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    if-eqz v6, :cond_0

    const-string v6, "MTKLogger/MTKLoggerService"

    const-string v7, "Server is busy dealing former command, wait till it\'s free please"

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v4

    :goto_0
    return v5

    :cond_0
    iput p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentAffectedLogType:I

    iput p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    iput v10, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCurrentRunningStatus:I

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mCachedStartStopCmd:I

    and-int/2addr v6, v3

    if-eqz v6, :cond_1

    invoke-direct {p0, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->getLogInstance(I)Lcom/mediatek/mtklogger/framework/LogInstance;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v0, v2, Lcom/mediatek/mtklogger/framework/LogInstance;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    add-int/lit16 v8, v3, 0x3e8

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    const-wide/16 v8, 0x4e20

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v6, 0x3

    invoke-virtual {v0, v6, p2}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const-string v6, "MTKLogger/MTKLoggerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "When stopRecording(), fail to get log instance handler  of log ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    const-string v7, "4"

    invoke-virtual {v6, v11, v3, v10, v7}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    :cond_3
    const-string v6, "MTKLogger/MTKLoggerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Fail to get log instance of logtype "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mNativeStateHandler:Landroid/os/Handler;

    const-string v7, "6"

    invoke-virtual {v6, v11, v3, v10, v7}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    :cond_4
    if-eqz v4, :cond_5

    invoke-direct {p0, v12}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleGlobalRunningStageChange(I)V

    iput v12, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->mGlobalRunningStage:I

    :cond_5
    move v5, v4

    goto/16 :goto_0
.end method
