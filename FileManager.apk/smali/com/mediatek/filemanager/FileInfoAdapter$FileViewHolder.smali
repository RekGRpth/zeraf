.class Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;
.super Ljava/lang/Object;
.source "FileInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/FileInfoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FileViewHolder"
.end annotation


# instance fields
.field protected mIcon:Landroid/widget/ImageView;

.field protected mName:Landroid/widget/TextView;

.field protected mSize:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
    .param p2    # Landroid/widget/TextView;
    .param p3    # Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mName:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mSize:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/mediatek/filemanager/FileInfoAdapter$FileViewHolder;->mIcon:Landroid/widget/ImageView;

    return-void
.end method
