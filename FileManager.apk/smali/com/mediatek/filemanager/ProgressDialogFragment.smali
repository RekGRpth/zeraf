.class public Lcom/mediatek/filemanager/ProgressDialogFragment;
.super Landroid/app/DialogFragment;
.source "ProgressDialogFragment.java"


# static fields
.field private static final CANCEL:Ljava/lang/String; = "cancel"

.field private static final CURRENTNUM:Ljava/lang/String; = "currentNumber"

.field private static final MESSAGE:Ljava/lang/String; = "message"

.field private static final PROGRESS:Ljava/lang/String; = "progress"

.field private static final STYLE:Ljava/lang/String; = "style"

.field public static final TAG:Ljava/lang/String; = "ProgressDialogFragment"

.field private static final TITLE:Ljava/lang/String; = "title"

.field private static final TOTAL:Ljava/lang/String; = "total"

.field private static final TOTALNUM:Ljava/lang/String; = "totalNumber"


# instance fields
.field private mCancelListener:Landroid/view/View$OnClickListener;

.field private mProgressNum:Landroid/widget/TextView;

.field private mSavedCurrentNum:I

.field private mSavedTotalNum:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput v0, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedCurrentNum:I

    iput v0, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedTotalNum:I

    iput-object v1, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mCancelListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/filemanager/ProgressDialogFragment;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/ProgressDialogFragment;

    iget-object v0, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mCancelListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static newInstance(IIII)Lcom/mediatek/filemanager/ProgressDialogFragment;
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    new-instance v1, Lcom/mediatek/filemanager/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/mediatek/filemanager/ProgressDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "style"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "cancel"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "message"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v11, 0x0

    const/4 v13, -0x1

    invoke-virtual {p0, v11}, Landroid/app/DialogFragment;->setCancelable(Z)V

    new-instance v3, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v3, v10}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v11}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v3, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const/4 v0, 0x0

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    const-string v10, "style"

    const/4 v11, 0x1

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const-string v10, "title"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-eq v7, v13, :cond_0

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->setTitle(I)V

    :cond_0
    const-string v10, "cancel"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v13, :cond_1

    const/4 v11, -0x3

    invoke-virtual {p0, v1}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v10, 0x0

    check-cast v10, Landroid/os/Message;

    invoke-virtual {v3, v11, v12, v10}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    :cond_1
    const-string v10, "message"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v13, :cond_2

    invoke-virtual {p0, v4}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_2
    const-string v10, "total"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-eq v8, v13, :cond_3

    invoke-virtual {v3, v8}, Landroid/app/ProgressDialog;->setMax(I)V

    :cond_3
    const-string v10, "progress"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-eq v5, v13, :cond_4

    invoke-virtual {v3, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_4
    iget-object v10, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    if-eqz v10, :cond_5

    const-string v10, "currentNumber"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v10, "totalNumber"

    invoke-virtual {v0, v10, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-eq v2, v13, :cond_5

    if-eq v9, v13, :cond_5

    iget-object v10, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    new-instance v10, Lcom/mediatek/filemanager/ProgressDialogFragment$2;

    invoke-direct {v10, p0}, Lcom/mediatek/filemanager/ProgressDialogFragment$2;-><init>(Lcom/mediatek/filemanager/ProgressDialogFragment;)V

    invoke-virtual {v3, v10}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object v3

    :cond_6
    move-object v0, p1

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v2, 0x102001b

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/filemanager/ProgressDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/mediatek/filemanager/ProgressDialogFragment$1;-><init>(Lcom/mediatek/filemanager/ProgressDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    const-string v1, "total"

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getMax()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "progress"

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "currentNumber"

    iget v2, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedCurrentNum:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "totalNumber"

    iget v2, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedTotalNum:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 7

    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    iget-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    if-nez v5, :cond_0

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    const v5, 0x1020264

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->setSingleLine()V

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v5, v3, Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_0

    move-object v2, v3

    check-cast v2, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setCancelListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mCancelListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setProgress(Lcom/mediatek/filemanager/service/ProgressInfo;)V
    .locals 6
    .param p1    # Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/ProgressDialog;

    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/ProgressInfo;->getProgeress()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/ProgressInfo;->getUpdateInfo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/ProgressInfo;->getTotal()J

    move-result-wide v3

    long-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMax(I)V

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/ProgressInfo;->getCurrentNumber()I

    move-result v3

    iput v3, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedCurrentNum:I

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/ProgressInfo;->getTotalNumber()J

    move-result-wide v3

    long-to-int v3, v3

    iput v3, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedTotalNum:I

    iget-object v3, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mProgressNum:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedCurrentNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/filemanager/ProgressDialogFragment;->mSavedTotalNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method
