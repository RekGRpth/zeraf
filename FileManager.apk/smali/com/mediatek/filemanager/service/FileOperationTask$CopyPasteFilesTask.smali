.class Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;
.super Lcom/mediatek/filemanager/service/FileOperationTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/service/FileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CopyPasteFilesTask"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CopyPasteFilesTask"


# instance fields
.field mDstFolder:Ljava/lang/String;

.field mSrcList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/filemanager/FileInfoManager;",
            "Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    iput-object p5, p0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 24
    .param p1    # [Ljava/lang/Void;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    monitor-enter v21

    :try_start_0
    const-string v2, "CopyPasteFilesTask"

    const-string v3, "doInBackground..."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v20, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;

    invoke-direct/range {v20 .. v20}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v15, v1}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFileList(Ljava/util/List;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v19

    if-gez v19, :cond_0

    const-string v2, "CopyPasteFilesTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doInBackground,ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    monitor-exit v21

    :goto_0
    return-object v2

    :cond_0
    new-instance v17, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$PasteMediaStoreHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$PasteMediaStoreHelper;-><init>(Lcom/mediatek/filemanager/service/MediaStoreHelper;)V

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mSrcList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v14}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v11, v2, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/filemanager/service/FileOperationTask;->isEnoughSpace(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "CopyPasteFilesTask"

    const-string v3, "doInBackground, not enough space."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    monitor-exit v21

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    new-array v0, v2, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v2, Lcom/mediatek/filemanager/service/ProgressInfo;

    const-string v3, ""

    const/4 v4, 0x0

    const-wide/16 v5, 0x64

    const/4 v7, 0x0

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v8

    invoke-direct/range {v2 .. v9}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v2, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    const/high16 v2, 0x200000

    new-array v10, v2, [B

    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    invoke-interface {v15, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_4
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->mDstFolder:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getDstFile(Ljava/util/HashMap;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$PasteMediaStoreHelper;->updateRecords()V

    const-string v2, "CopyPasteFilesTask"

    const-string v3, "doInBackground,user cancel."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    monitor-exit v21

    goto/16 :goto_0

    :cond_5
    if-nez v12, :cond_6

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v3, 0x0

    new-instance v4, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v5, -0xe

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v13}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v12}, Lcom/mediatek/filemanager/service/FileOperationTask;->mkdir(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper;->addRecord(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v13, v12}, Lcom/mediatek/filemanager/service/FileOperationTask;->addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    const-wide/16 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateCurrentNumber(J)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Lcom/mediatek/filemanager/service/FileOperationTask;->updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v13}, Ljava/io/File;->canRead()Z

    move-result v2

    if-nez v2, :cond_9

    :cond_8
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v3, 0x0

    new-instance v4, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v5, -0xa

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    const-wide/16 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateCurrentNumber(J)V

    goto/16 :goto_2

    :cond_9
    const-wide/16 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateCurrentNumber(J)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v13, v12, v1}, Lcom/mediatek/filemanager/service/FileOperationTask;->copyFile([BLjava/io/File;Ljava/io/File;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v19

    const/4 v2, -0x7

    move/from16 v0, v19

    if-ne v0, v2, :cond_a

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$PasteMediaStoreHelper;->updateRecords()V

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    monitor-exit v21

    goto/16 :goto_0

    :cond_a
    if-gez v19, :cond_b

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v3, 0x0

    new-instance v4, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v5, 0x1

    move/from16 v0, v19

    invoke-direct {v4, v0, v5}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    const-wide/16 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateCurrentNumber(J)V

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper;->addRecord(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v13, v12}, Lcom/mediatek/filemanager/service/FileOperationTask;->addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V

    goto/16 :goto_2

    :cond_c
    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/filemanager/service/MultiMediaStoreHelper$PasteMediaStoreHelper;->updateRecords()V

    const-string v2, "CopyPasteFilesTask"

    const-string v3, "doInBackground,return success."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
