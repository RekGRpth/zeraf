.class public Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;
.super Ljava/lang/Object;
.source "FileManagerOperationActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/FileManagerOperationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ActionModeCallBack"
.end annotation


# instance fields
.field private mSelectPopupMenu:Landroid/widget/PopupMenu;

.field private mSelectedAll:Z

.field private mTextSelect:Landroid/widget/Button;

.field final synthetic this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;


# direct methods
.method protected constructor <init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectedAll:Z

    iput-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;
    .param p1    # Landroid/widget/PopupMenu;

    iput-object p1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;Landroid/view/View;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->createSelectPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;)V
    .locals 0
    .param p0    # Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;

    invoke-direct {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->updateSelectPopupMenu()V

    return-void
.end method

.method private createSelectPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-direct {v0, v1, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    return-object v0
.end method

.method private updateSelectPopupMenu()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x7f0c0025

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    invoke-direct {p0, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->createSelectPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCount()I

    move-result v2

    if-eq v2, v1, :cond_2

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f08001e

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    iput-boolean v5, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectedAll:Z

    goto :goto_0

    :cond_2
    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f08001d

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    iput-boolean v4, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectedAll:Z

    goto :goto_0
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 9
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_0
    return v1

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v4, v4, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/mediatek/filemanager/FileInfoManager;->savePasteList(ILjava/util/List;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    :cond_0
    :goto_1
    move v1, v3

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/mediatek/filemanager/FileInfoManager;->savePasteList(ILjava/util/List;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showDeleteDialog()V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$1000(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V

    goto :goto_1

    :pswitch_5
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->showRenameDialog()V

    goto :goto_1

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$1100(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Ljava/io/File;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$1200(Lcom/mediatek/filemanager/FileManagerOperationActivity;Ljava/io/File;)V

    goto :goto_1

    :pswitch_7
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v4, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mService:Lcom/mediatek/filemanager/service/FileManagerService;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    new-instance v6, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;

    iget-object v7, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v6, v7, v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$DetailInfoListener;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity;Lcom/mediatek/filemanager/FileInfo;)V

    invoke-virtual {v4, v5, v1, v6}, Lcom/mediatek/filemanager/service/FileManagerService;->getDetailInfo(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    goto :goto_1

    :pswitch_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v4, v4, Lcom/mediatek/filemanager/AbsBaseActivity;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/filemanager/utils/DrmManager;->showProtectionInfoDialog(Landroid/app/Activity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    goto/16 :goto_1

    :pswitch_9
    iget-boolean v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectedAll:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1, v3}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    :goto_2
    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->updateActionMode()V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1, v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x7f0c0017
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v3, 0x7f030001

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    const v3, 0x7f0c0003

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    new-instance v4, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack$1;

    invoke-direct {v4, p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack$1;-><init>(Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v3, 0x1

    return v3
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1    # Landroid/view/ActionMode;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$1300(Lcom/mediatek/filemanager/FileManagerOperationActivity;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v0}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$202(Lcom/mediatek/filemanager/FileManagerOperationActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectPopupMenu:Landroid/widget/PopupMenu;

    :cond_1
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget-boolean v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mSelectedAll:Z

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v1, v1, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->updateActionMode()V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/FileInfoAdapter;->setAllItemChecked(Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0025
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 12
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v7

    if-nez v7, :cond_3

    const v8, 0x7f0c0018

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c0019

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c001a

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_0
    if-eqz v7, :cond_0

    const/16 v8, 0x7d0

    if-le v7, v8, :cond_4

    :cond_0
    const v8, 0x7f0c0017

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    const v8, 0x7f0c001e

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    if-nez v7, :cond_a

    const v8, 0x7f0c001b

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c001c

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    :goto_2
    const/4 v8, 0x1

    return v8

    :cond_3
    const v8, 0x7f0c0018

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c0019

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c001a

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v8

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/filemanager/utils/DrmManager;->isRightsStatus(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    :cond_5
    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_7

    :cond_6
    const v8, 0x7f0c0017

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_7
    const v8, 0x7f0c0017

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_8
    const v8, 0x7f0c0017

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v4}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_9

    const v8, 0x7f0c0017

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_a
    const/4 v8, 0x1

    if-ne v7, v8, :cond_d

    const v8, 0x7f0c001c

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v8

    if-eqz v8, :cond_b

    const v8, 0x7f0c001b

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_b
    const v8, 0x7f0c001d

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v9, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v8

    const/4 v10, 0x0

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v8

    invoke-static {v9, v8}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$900(Lcom/mediatek/filemanager/FileManagerOperationActivity;Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_c

    const v8, 0x7f0c001d

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_c
    iget-object v8, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v8, v8, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v8}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedFileInfoItemsList()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->isDrmFile()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v1}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/mediatek/filemanager/utils/DrmManager;->checkDrmObjectType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mediatek/filemanager/utils/DrmManager;->getInstance()Lcom/mediatek/filemanager/utils/DrmManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/mediatek/filemanager/utils/DrmManager;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    const v9, 0x7f0c001e

    const/4 v10, 0x0

    const v11, 0x2050062

    invoke-interface {p2, v8, v9, v10, v11}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_d
    const v8, 0x7f0c001c

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0c001b

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_2
.end method

.method public updateActionMode()V
    .locals 4

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/AbsBaseActivity;->mAdapter:Lcom/mediatek/filemanager/FileInfoAdapter;

    invoke-virtual {v2}, Lcom/mediatek/filemanager/FileInfoAdapter;->getCheckedItemsCount()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-virtual {v2}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->mTextSelect:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    iget-object v2, v2, Lcom/mediatek/filemanager/FileManagerOperationActivity;->mActionModeCallBack:Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;

    invoke-direct {v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->updateSelectPopupMenu()V

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Landroid/view/ActionMode;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileManagerOperationActivity$ActionModeCallBack;->this$0:Lcom/mediatek/filemanager/FileManagerOperationActivity;

    invoke-static {v2}, Lcom/mediatek/filemanager/FileManagerOperationActivity;->access$200(Lcom/mediatek/filemanager/FileManagerOperationActivity;)Landroid/view/ActionMode;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    return-void
.end method
