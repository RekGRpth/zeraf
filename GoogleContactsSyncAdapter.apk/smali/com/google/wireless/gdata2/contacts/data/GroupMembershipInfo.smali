.class public Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;
.super Ljava/lang/Object;
.source "GroupMembershipInfo.java"


# instance fields
.field private deleted:Z

.field private group:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->setGroup(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->setDeleted(Z)V

    return-void
.end method


# virtual methods
.method public getGroup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->group:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->deleted:Z

    return v0
.end method

.method public setDeleted(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->deleted:Z

    return-void
.end method

.method public setGroup(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->group:Ljava/lang/String;

    return-void
.end method

.method public toString(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuffer;

    const-string v0, "GroupMembershipInfo"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->group:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, " group:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->group:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v0, " deleted:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->deleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    return-void
.end method

.method public validate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/gdata2/contacts/data/GroupMembershipInfo;->group:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/wireless/gdata2/parser/ParseException;

    const-string v1, "the group must be present"

    invoke-direct {v0, v1}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
