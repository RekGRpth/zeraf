.class public Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;
.super Ljava/lang/Object;
.source "BatchInterrupted.java"


# instance fields
.field private error:I

.field private reason:Ljava/lang/String;

.field private success:I

.field private total:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public setErrorCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;->error:I

    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;->reason:Ljava/lang/String;

    return-void
.end method

.method public setSuccessCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;->success:I

    return-void
.end method

.method public setTotalCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/gdata2/data/batch/BatchInterrupted;->total:I

    return-void
.end method
