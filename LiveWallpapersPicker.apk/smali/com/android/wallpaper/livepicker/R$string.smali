.class public final Lcom/android/wallpaper/livepicker/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/livepicker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final application_name:I = 0x7f080000

.field public static final configure_wallpaper:I = 0x7f080004

.field public static final live_wallpaper_empty:I = 0x7f080006

.field public static final live_wallpaper_loading:I = 0x7f080009

.field public static final live_wallpaper_picker_label:I = 0x7f080002

.field public static final live_wallpaper_picker_title:I = 0x7f080001

.field public static final live_wallpaper_preview_title:I = 0x7f080003

.field public static final no_external_storage:I = 0x7f08000c

.field public static final no_external_storage_title:I = 0x7f08000b

.field public static final set_live_wallpaper:I = 0x7f080007

.field public static final wallpaper_cancel:I = 0x7f08000a

.field public static final wallpaper_instructions:I = 0x7f080005

.field public static final wallpaper_title_and_author:I = 0x7f080008


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
