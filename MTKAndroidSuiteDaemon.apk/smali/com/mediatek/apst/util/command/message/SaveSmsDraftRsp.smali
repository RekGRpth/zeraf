.class public Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "SaveSmsDraftRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private date:J

.field private insertedId:J

.field private threadId:J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x100

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->date:J

    return-wide v0
.end method

.method public getInsertedId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->insertedId:J

    return-wide v0
.end method

.method public getThreadId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->threadId:J

    return-wide v0
.end method

.method public setDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->date:J

    return-void
.end method

.method public setInsertedId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->insertedId:J

    return-void
.end method

.method public setThreadId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->threadId:J

    return-void
.end method
