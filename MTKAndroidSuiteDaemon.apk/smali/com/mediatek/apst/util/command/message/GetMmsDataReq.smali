.class public Lcom/mediatek/apst/util/command/message/GetMmsDataReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "GetMmsDataReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private isBackup:Z

.field private mImportList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getImportList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->mImportList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getIsBackup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->isBackup:Z

    return v0
.end method

.method public setImportList(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->mImportList:Ljava/util/LinkedList;

    return-void
.end method

.method public setIsBackup(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->isBackup:Z

    return-void
.end method
