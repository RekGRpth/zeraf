.class public Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "MediaGetStorageStateRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private mExternalStoragePath:Ljava/lang/String;

.field private mInternalStoragePath:Ljava/lang/String;

.field private mSdSwap:Z

.field private storageState:[Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getStorageState()[Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->storageState:[Z

    return-object v0
.end method

.method public getmExternalStoragePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mExternalStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method public getmInternalStoragePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mInternalStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method public ismSdSwap()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mSdSwap:Z

    return v0
.end method

.method public setStorageState([Z)V
    .locals 0
    .param p1    # [Z

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->storageState:[Z

    return-void
.end method

.method public setmExternalStoragePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mExternalStoragePath:Ljava/lang/String;

    return-void
.end method

.method public setmInternalStoragePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mInternalStoragePath:Ljava/lang/String;

    return-void
.end method

.method public setmSdSwap(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->mSdSwap:Z

    return-void
.end method
