.class public Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "AsyncDeleteBookmarkReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private deleteIds:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/high16 v0, 0x1100000

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getDeleteIds()[I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkReq;->deleteIds:[I

    return-object v0
.end method

.method public setDeleteIds([I)V
    .locals 0
    .param p1    # [I

    iput-object p1, p0, Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkReq;->deleteIds:[I

    return-void
.end method
