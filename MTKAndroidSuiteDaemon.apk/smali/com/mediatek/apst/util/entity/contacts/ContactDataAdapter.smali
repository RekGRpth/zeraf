.class public abstract Lcom/mediatek/apst/util/entity/contacts/ContactDataAdapter;
.super Ljava/lang/Object;
.source "ContactDataAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readRaw(Ljava/nio/ByteBuffer;I)Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/GroupMembership;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/StructuredName;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/StructuredName;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Phone;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Phone;

    invoke-virtual {v2, p0, p1}, Lcom/mediatek/apst/util/entity/contacts/Phone;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Photo;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Photo;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Photo;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Email;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Email;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Email;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Im;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Im;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Im;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/StructuredPostal;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Organization;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Organization;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Organization;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Note;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Note;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Note;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_9
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Nickname;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Nickname;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Nickname;->readRaw(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :pswitch_a
    new-instance v0, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/contacts/Website;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/mediatek/apst/util/entity/contacts/Website;

    invoke-virtual {v2, p0}, Lcom/mediatek/apst/util/entity/contacts/Website;->readRaw(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
