.class public Lcom/mediatek/apst/util/entity/app/ApplicationInfo;
.super Ljava/lang/Object;
.source "ApplicationInfo.java"

# interfaces
.implements Lcom/mediatek/apst/util/entity/IRawTransferable;
.implements Ljava/io/Serializable;


# static fields
.field public static final TYPE_DOWNLOADED:I = 0x2

.field public static final TYPE_SYSTEM:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private apkSize:J

.field private dataDirectory:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private iconBytes:[B

.field private label:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private requestedPermissions:[Ljava/lang/String;

.field private sdkVersion:I

.field private sourceDirectory:Ljava/lang/String;

.field private type:I

.field private uid:I

.field private versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApkSize()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->apkSize:J

    return-wide v0
.end method

.method public getDataDirectory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->dataDirectory:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getIconBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->iconBytes:[B

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestedPermissions()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->requestedPermissions:[Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sdkVersion:I

    return v0
.end method

.method public getSourceDirectory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sourceDirectory:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->type:I

    return v0
.end method

.method public getUid()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->uid:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public readRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->type:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->apkSize:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sdkVersion:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->uid:I

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBytes(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->iconBytes:[B

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->label:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->description:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->versionName:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sourceDirectory:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->dataDirectory:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getStringArray(Ljava/nio/ByteBuffer;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->requestedPermissions:[Ljava/lang/String;

    return-void
.end method

.method public setApkSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->apkSize:J

    return-void
.end method

.method public setDataDirectory(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->dataDirectory:Ljava/lang/String;

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->description:Ljava/lang/String;

    return-void
.end method

.method public setIconBytes([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->iconBytes:[B

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->label:Ljava/lang/String;

    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->packageName:Ljava/lang/String;

    return-void
.end method

.method public setRequestedPermissions([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->requestedPermissions:[Ljava/lang/String;

    return-void
.end method

.method public setSdkVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sdkVersion:I

    return-void
.end method

.method public setSourceDirectory(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sourceDirectory:Ljava/lang/String;

    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->type:I

    return-void
.end method

.method public setUid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->uid:I

    return-void
.end method

.method public setVersionName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->versionName:Ljava/lang/String;

    return-void
.end method

.method public writeRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->type:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->apkSize:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sdkVersion:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->uid:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->iconBytes:[B

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBytes(Ljava/nio/ByteBuffer;[B)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->label:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->description:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->versionName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->sourceDirectory:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->dataDirectory:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/app/ApplicationInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putStringArray(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V

    return-void
.end method
