.class public interface abstract Lcom/mediatek/apst/target/event/ISimStateListener;
.super Ljava/lang/Object;
.source "ISimStateListener.java"


# static fields
.field public static final SIM_ID:Ljava/lang/String; = "sim_id"

.field public static final SIM_INFO:Ljava/lang/String; = "sim_info"

.field public static final SIM_INFO_FLAG:Ljava/lang/String; = "sim_info_flag"

.field public static final STATE:Ljava/lang/String; = "state"


# virtual methods
.method public abstract onSimStateChanged(Lcom/mediatek/apst/target/event/Event;)V
.end method
