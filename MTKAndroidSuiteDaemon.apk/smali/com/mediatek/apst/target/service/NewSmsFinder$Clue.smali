.class public Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;
.super Ljava/lang/Object;
.source "NewSmsFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apst/target/service/NewSmsFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clue"
.end annotation


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mBody:Ljava/lang/String;

.field private mBox:I

.field private mDate:J


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mDate:J

    iput-object p3, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBody:Ljava/lang/String;

    iput p5, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBox:I

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBody:Ljava/lang/String;

    return-object v0
.end method

.method public getBox()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBox:I

    return v0
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mDate:J

    return-wide v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mAddress:Ljava/lang/String;

    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBody:Ljava/lang/String;

    return-void
.end method

.method public setBox(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mBox:I

    return-void
.end method

.method public setDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;->mDate:J

    return-void
.end method
