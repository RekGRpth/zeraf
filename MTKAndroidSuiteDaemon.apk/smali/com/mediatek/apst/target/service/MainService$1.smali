.class Lcom/mediatek/apst/target/service/MainService$1;
.super Ljava/lang/Thread;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/apst/target/service/MainService;->onSmsSent(Lcom/mediatek/apst/target/event/Event;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/apst/target/service/MainService;

.field final synthetic val$date:J

.field final synthetic val$sent:Z

.field final synthetic val$smsId:J


# direct methods
.method constructor <init>(Lcom/mediatek/apst/target/service/MainService;JJZ)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/apst/target/service/MainService$1;->this$0:Lcom/mediatek/apst/target/service/MainService;

    iput-wide p2, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$smsId:J

    iput-wide p4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$date:J

    iput-boolean p6, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$sent:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v2, v6, [J

    new-array v1, v6, [J

    iget-wide v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$smsId:J

    aput-wide v4, v2, v7

    iget-wide v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$date:J

    aput-wide v4, v1, v7

    iget-boolean v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$sent:Z

    if-eqz v4, :cond_0

    const/4 v0, 0x2

    :goto_0
    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual {v4, v2, v6, v1, v0}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->moveSmsToBox([JZ[JI)I

    new-instance v3, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;

    invoke-direct {v3}, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;-><init>()V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$100(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/util/communication/common/Dispatcher;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    iget-wide v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$smsId:J

    invoke-virtual {v3, v4, v5}, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;->setId(J)V

    iget-wide v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$date:J

    invoke-virtual {v3, v4, v5}, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;->setDate(J)V

    iget-boolean v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->val$sent:Z

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;->setSent(Z)V

    invoke-virtual {v3, v6}, Lcom/mediatek/apst/util/command/message/NotifyMessageSentReq;->setMessageType(I)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$1;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->access$200(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/BaseCommand;)Z

    return-void

    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method
