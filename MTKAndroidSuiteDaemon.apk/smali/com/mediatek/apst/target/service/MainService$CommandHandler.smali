.class Lcom/mediatek/apst/target/service/MainService$CommandHandler;
.super Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apst/target/service/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CommandHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/apst/target/service/MainService;


# direct methods
.method public constructor <init>(Lcom/mediatek/apst/target/service/MainService;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-direct {p0, p2}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    const-string v0, "MainService$CommandHandler"

    return-object v0
.end method

.method public handle(Lcom/mediatek/apst/util/command/BaseCommand;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v1}, Lcom/mediatek/apst/target/service/MainService;->access$900(Lcom/mediatek/apst/target/service/MainService;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v1}, Lcom/mediatek/apst/target/service/MainService;->access$500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MainService$MainHandler;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->terminate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "handle"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FeatureID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Token="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v1

    if-ne v6, v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleMainFrameFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "handle"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    const-string v4, "): Unsupported command."

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    new-instance v2, Lcom/mediatek/apst/util/command/UnsupportedRequestResponse;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v3

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/mediatek/apst/util/command/UnsupportedRequestResponse;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x10

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_3

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleContactsFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_3
    const/16 v1, 0x100

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_4

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleMessageFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_4
    const/16 v1, 0x1000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_5

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleApplicationFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_5
    const/high16 v1, 0x10000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_6

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleSyncFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_6
    const/high16 v1, 0x100000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_7

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleMediaFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_7
    const/high16 v1, 0x1000000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_8

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleCalendarFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_8
    const v1, 0x11000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_9

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleCalendarSyncFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto :goto_1

    :cond_9
    const/high16 v1, 0x1100000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_a

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleBookmarkFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto/16 :goto_1

    :cond_a
    const/high16 v1, 0x1110000

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    if-ne v1, v2, :cond_b

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handleBackupFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    goto/16 :goto_1

    :cond_b
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "handle"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    const-string v4, "Unsupported feature."

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public handleApplicationFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 6
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v0

    instance-of v1, p1, Lcom/mediatek/apst/util/command/app/AsyncGetAllAppInfoReq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v1}, Lcom/mediatek/apst/target/service/MainService;->access$300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    move-result-object v2

    new-instance v3, Lcom/mediatek/apst/target/service/MainService$CommandHandler$12;

    invoke-direct {v3, p0, v0}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$12;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    move-object v1, p1

    check-cast v1, Lcom/mediatek/apst/util/command/app/AsyncGetAllAppInfoReq;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/command/app/AsyncGetAllAppInfoReq;->getDestIconWidth()I

    move-result v1

    check-cast p1, Lcom/mediatek/apst/util/command/app/AsyncGetAllAppInfoReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/app/AsyncGetAllAppInfoReq;->getDestIconHeight()I

    move-result v5

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;->fastGetAllApplications(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;II)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public handleBackupFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 31
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v17

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAllGroupsForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$29;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$29;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllGroups(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    :cond_0
    :goto_0
    const/16 v26, 0x1

    :goto_1
    return v26

    :cond_1
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAllContsForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$30;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$30;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v26 .. v30}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllRawContactsForBackup(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;Ljava/lang/Long;Ljava/lang/Long;)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAllContsDataForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    check-cast p1, Lcom/mediatek/apst/util/command/backup/GetAllContsDataForBackupReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/backup/GetAllContsDataForBackupReq;->getRequestingDataTypes()Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/mediatek/apst/target/service/MainService$CommandHandler$31;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$31;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v29

    invoke-virtual/range {v26 .. v29}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllContactData(Ljava/util/List;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetPhoneListReq;

    move/from16 v26, v0

    if-eqz v26, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$32;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$32;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->asyncGetPhoneList(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/DelAllContactsReq;

    move/from16 v26, v0

    if-eqz v26, :cond_5

    new-instance v21, Lcom/mediatek/apst/util/command/backup/DelAllContactsRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllContactsRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteAllGroups()I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteContactForBackup()I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllContactsRsp;->setDeleteCount(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreGroupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_6

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/RestoreGroupReq;

    new-instance v21, Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/RestoreGroupReq;->getGroupList()Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateGroupForRestore(Ljava/util/List;)I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreGroupRsp;->setCount(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreContactsReq;

    move/from16 v26, v0

    if-eqz v26, :cond_7

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/RestoreContactsReq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v27

    new-instance v28, Lcom/mediatek/apst/target/service/MainService$CommandHandler$33;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$33;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    new-instance v29, Lcom/mediatek/apst/target/service/MainService$CommandHandler$34;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$34;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v30

    invoke-virtual/range {v26 .. v30}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->restoreDetailedContacts([BLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAllSmsForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$35;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$35;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->asyncGetAllSms(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetMmsDataForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$36;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$36;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    const/16 v29, 0x1

    const/16 v30, 0x0

    invoke-virtual/range {v26 .. v30}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMmsData(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;ZLjava/util/LinkedList;)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/DelAllMsgsForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_a

    new-instance v21, Lcom/mediatek/apst/util/command/backup/DelAllMsgsForBackupRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllMsgsForBackupRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->deleteAllMessages(Z)I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllMsgsForBackupRsp;->setDeletedCount(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreSmsReq;

    move/from16 v26, v0

    if-eqz v26, :cond_e

    new-instance v21, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;-><init>(I)V

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/RestoreSmsReq;

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MulMessageObserver;->stop()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->importSms([BLjava/util/ArrayList;)[J

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MulMessageObserver;->start()V

    if-nez v7, :cond_b

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_b
    if-eqz v24, :cond_d

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [J

    move-object/from16 v25, v0

    const/4 v6, 0x0

    :goto_3
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v6, v0, :cond_c

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Long;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    aput-wide v26, v25, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_c
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->setThreadIds([J)V

    :cond_d
    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/mediatek/apst/util/command/backup/RestoreSmsRsp;->setInsertedIds([J)V

    goto :goto_2

    :cond_e
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreMmsReq;

    move/from16 v26, v0

    if-eqz v26, :cond_13

    new-instance v21, Lcom/mediatek/apst/util/command/backup/RestoreMmsRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreMmsRsp;-><init>(I)V

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/RestoreMmsReq;

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MulMessageObserver;->onSelfChangeStart()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MulMessageObserver;->getMaxMmsId()J

    move-result-wide v26

    const-wide/16 v28, 0x0

    cmp-long v26, v26, v28

    if-nez v26, :cond_f

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object p1, v26, v27

    const-string v27, ">>restore start ,pdu table is null"

    invoke-static/range {v26 .. v27}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMaxMmsIdByInsert()J

    move-result-wide v10

    const-wide/16 v26, 0x3e8

    :try_start_0
    invoke-static/range {v26 .. v27}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v10, v11}, Lcom/mediatek/apst/target/service/MulMessageObserver;->setMaxMmsId(J)V

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->importMms([BLjava/util/ArrayList;)[J

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MulMessageObserver;->onSelfChangeDone()V

    if-nez v7, :cond_10

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_5
    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/RestoreMmsReq;->isIsLastImport()Z

    move-result v26

    if-eqz v26, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :cond_10
    if-eqz v24, :cond_12

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [J

    move-object/from16 v25, v0

    const/4 v6, 0x0

    :goto_6
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v6, v0, :cond_11

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Long;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    aput-wide v26, v25, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_11
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreMmsRsp;->setThreadIds([J)V

    :cond_12
    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/mediatek/apst/util/command/backup/RestoreMmsRsp;->setInsertedIds([J)V

    goto :goto_5

    :cond_13
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetEventsForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$37;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$37;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getEvents(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_14
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAttendeesForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$38;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$38;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getAttendees(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetRemindersForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v26

    new-instance v27, Lcom/mediatek/apst/target/service/MainService$CommandHandler$39;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$39;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getReminders(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_16
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/DelAllCalendarReq;

    move/from16 v26, v0

    if-eqz v26, :cond_17

    new-instance v21, Lcom/mediatek/apst/util/command/backup/DelAllCalendarRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllCalendarRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteAll()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_17
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreCalendarReq;

    move/from16 v26, v0

    if-eqz v26, :cond_18

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/RestoreCalendarReq;

    new-instance v21, Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/RestoreCalendarReq;->getEvent()Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertAllEvents(Ljava/util/List;)J

    move-result-wide v26

    move-object/from16 v0, v21

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;->setInsertedCount(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_18
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_19

    new-instance v22, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;-><init>(I)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8, v9}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->asynGetAllBookmarks(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->setmBookmarkDataList(Ljava/util/ArrayList;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/mediatek/apst/util/command/backup/GetAllBookmarkForBackupRsp;->setmBookmarkFolderList(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_19
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/DelAllBookmarkReq;

    move/from16 v26, v0

    if-eqz v26, :cond_1a

    new-instance v22, Lcom/mediatek/apst/util/command/backup/DelAllBookmarkRsp;

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/DelAllBookmarkRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->deleteAll()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_1a
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkReq;

    move/from16 v26, v0

    if-eqz v26, :cond_1b

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkReq;

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkReq;->getmBookmarkDataList()Ljava/util/ArrayList;

    move-result-object v8

    check-cast p1, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkReq;->getmBookmarkFolderList()Ljava/util/ArrayList;

    move-result-object v9

    new-instance v22, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkRsp;

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/RestoreBookmarkRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8, v9}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->insertBookmark(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_1b
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateReq;

    move/from16 v26, v0

    if-eqz v26, :cond_1c

    new-instance v21, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1200(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->checkSDCardState()[Z

    move-result-object v23

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "SDCard1 state :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    aget-boolean v27, v23, v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "SDCard2 state :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x1

    aget-boolean v27, v23, v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->setStorageState([Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSdSwap()Z

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->setmSdSwap(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getInternalStoragePathSD()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->setmInternalStoragePath(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaGetStorageStateRsp;->setmExternalStoragePath(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_1c
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/MediaBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_1f

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/MediaBackupReq;

    new-instance v21, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;-><init>(I)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/MediaBackupReq;->getBackupPaths()Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v15, v13}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->getFilesUnderDirs(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "oldPaths count: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "newPaths count: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v14, v0, [Ljava/lang/String;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v12, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    :goto_7
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    if-ge v6, v0, :cond_1d

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    aput-object v26, v14, v6

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/APST_BACKUP0"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v12, v6

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "oldPaths["

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "]: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    aget-object v27, v14, v6

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "newPaths["

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "]: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    aget-object v27, v12, v6

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_1d
    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;->setOldPaths([Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;->setNewPaths([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;->getOldPaths()[Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/apst/util/command/backup/MediaBackupRsp;->getNewPaths()[Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->renameFiles([Ljava/lang/String;[Ljava/lang/String;)[Z

    move-result-object v20

    if-nez v20, :cond_1e

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_1e
    const-string v26, "resultRe is not null"

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_8

    :cond_1f
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/MediaFileRenameReq;

    move/from16 v26, v0

    if-eqz v26, :cond_21

    new-instance v21, Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;-><init>(I)V

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/MediaFileRenameReq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/MediaFileRenameReq;->getOldPaths()[Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/MediaFileRenameReq;->getNewPaths()[Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->renameFiles([Ljava/lang/String;[Ljava/lang/String;)[Z

    move-result-object v19

    if-nez v19, :cond_20

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_20
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaFileRenameRsp;->setResults([Z)V

    goto :goto_9

    :cond_21
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;

    move/from16 v26, v0

    if-eqz v26, :cond_23

    new-instance v21, Lcom/mediatek/apst/util/command/backup/MediaRestoreRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaRestoreRsp;-><init>(I)V

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->getRestorePath()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->deleteAllFileUnder(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->deleteDirectory(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->createDirectory(Ljava/lang/String;)Z

    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_23
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/MediaRestoreOverReq;

    move/from16 v26, v0

    if-eqz v26, :cond_24

    new-instance v21, Lcom/mediatek/apst/util/command/backup/MediaRestoreOverRsp;

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/backup/MediaRestoreOverRsp;-><init>(I)V

    move-object/from16 v16, p1

    check-cast v16, Lcom/mediatek/apst/util/command/backup/MediaRestoreOverReq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v27, v0

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/command/backup/MediaRestoreOverReq;->getRestoreFilePath()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->scan(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_24
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/mediatek/apst/util/command/backup/GetAppForBackupReq;

    move/from16 v26, v0

    if-eqz v26, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/service/MainService;->access$300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    move-result-object v27

    new-instance v28, Lcom/mediatek/apst/target/service/MainService$CommandHandler$40;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$40;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v29

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/backup/GetAppForBackupReq;

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/backup/GetAppForBackupReq;->getDestIconWidth()I

    move-result v26

    check-cast p1, Lcom/mediatek/apst/util/command/backup/GetAppForBackupReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/backup/GetAppForBackupReq;->getDestIconHeight()I

    move-result v30

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v26

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;->fastGetAllApps2Backup(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;II)V

    goto/16 :goto_0

    :cond_25
    const/16 v26, 0x0

    goto/16 :goto_1
.end method

.method public handleBookmarkFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 5
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v2

    instance-of v4, p1, Lcom/mediatek/apst/util/command/bookmark/AsyncGetAllBookmarkInfoReq;

    if-eqz v4, :cond_0

    new-instance v3, Lcom/mediatek/apst/util/command/bookmark/AsyncGetAllBookmarkInfoRsp;

    invoke-direct {v3, v2}, Lcom/mediatek/apst/util/command/bookmark/AsyncGetAllBookmarkInfoRsp;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->asynGetAllBookmarks(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/util/command/bookmark/AsyncGetAllBookmarkInfoRsp;->setmBookmarkDataList(Ljava/util/ArrayList;)V

    invoke-virtual {v3, v1}, Lcom/mediatek/apst/util/command/bookmark/AsyncGetAllBookmarkInfoRsp;->setmBookmarkFolderList(Ljava/util/ArrayList;)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_0
    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    instance-of v4, p1, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkReq;

    if-eqz v4, :cond_1

    move-object v4, p1

    check-cast v4, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkReq;

    invoke-virtual {v4}, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkReq;->getBookmarkDataList()Ljava/util/ArrayList;

    move-result-object v0

    check-cast p1, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkReq;->getBookmarkFolderList()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v3, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkRsp;

    invoke-direct {v3, v2}, Lcom/mediatek/apst/util/command/bookmark/AsyncInsertBookmarkRsp;-><init>(I)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->insertBookmark(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    instance-of v4, p1, Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkReq;

    if-eqz v4, :cond_2

    new-instance v3, Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkRsp;

    invoke-direct {v3, v2}, Lcom/mediatek/apst/util/command/bookmark/AsyncDeleteBookmarkRsp;-><init>(I)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;->deleteAll()V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public handleCalendarFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 14
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v8

    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/CalendarBatchReq;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    new-instance v12, Lcom/mediatek/apst/util/command/calendar/CalendarBatchRsp;

    invoke-direct {v12, v8}, Lcom/mediatek/apst/util/command/calendar/CalendarBatchRsp;-><init>(I)V

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/service/MainService;->access$1002(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/ICommandBatch;)Lcom/mediatek/apst/util/command/ICommandBatch;

    move-object v7, p1

    check-cast v7, Lcom/mediatek/apst/util/command/calendar/CalendarBatchReq;

    const/4 v1, 0x0

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v12, 0x2

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    invoke-virtual {v7}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/command/BaseCommand;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v7}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ne v1, v11, :cond_0

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_0
    invoke-virtual {p0, v3}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handle(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_2
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/AddEventReq;

    if-eqz v11, :cond_4

    new-instance v9, Lcom/mediatek/apst/util/command/calendar/AddEventRsp;

    invoke-direct {v9, v8}, Lcom/mediatek/apst/util/command/calendar/AddEventRsp;-><init>(I)V

    move-object v6, p1

    check-cast v6, Lcom/mediatek/apst/util/command/calendar/AddEventReq;

    invoke-virtual {v6}, Lcom/mediatek/apst/util/command/calendar/AddEventReq;->getFromFeature()I

    move-result v11

    invoke-virtual {v9, v11}, Lcom/mediatek/apst/util/command/calendar/AddEventRsp;->setFromFeature(I)V

    invoke-virtual {v6}, Lcom/mediatek/apst/util/command/calendar/AddEventReq;->getEvent()Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    move-result-object v0

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    invoke-virtual {v11, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertEvent(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)J

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, Lcom/mediatek/apst/util/command/calendar/AddEventRsp;->setInsertedId(J)V

    const-wide/16 v11, -0x1

    cmp-long v11, v4, v11

    if-nez v11, :cond_3

    const/4 v11, 0x2

    invoke-virtual {v9, v11}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_1
    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v11, v9}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_2
    const/4 v11, 0x1

    :goto_3
    return v11

    :cond_3
    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual {v11, v4, v5, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getEvent(JZZ)Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/mediatek/apst/util/command/calendar/AddEventRsp;->setResult(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)V

    goto :goto_1

    :cond_4
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/DeleteEventReq;

    if-eqz v11, :cond_5

    new-instance v9, Lcom/mediatek/apst/util/command/calendar/DeleteEventRsp;

    invoke-direct {v9, v8}, Lcom/mediatek/apst/util/command/calendar/DeleteEventRsp;-><init>(I)V

    move-object v6, p1

    check-cast v6, Lcom/mediatek/apst/util/command/calendar/DeleteEventReq;

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    invoke-virtual {v6}, Lcom/mediatek/apst/util/command/calendar/DeleteEventReq;->getDeleteIds()[J

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteEvents([J)[Z

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/mediatek/apst/util/command/calendar/DeleteEventRsp;->setDeleteResults([Z)V

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v11, v9}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_2

    :cond_5
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/GetCalendarsReq;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    new-instance v12, Lcom/mediatek/apst/target/service/MainService$CommandHandler$18;

    invoke-direct {v12, p0, v8}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$18;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getCalendars(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_2

    :cond_6
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/GetEventsReq;

    if-eqz v11, :cond_7

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    new-instance v12, Lcom/mediatek/apst/target/service/MainService$CommandHandler$19;

    invoke-direct {v12, p0, v8}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$19;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getEvents(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_2

    :cond_7
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;

    if-eqz v11, :cond_9

    new-instance v9, Lcom/mediatek/apst/util/command/calendar/UpdateEventRsp;

    invoke-direct {v9, v8}, Lcom/mediatek/apst/util/command/calendar/UpdateEventRsp;-><init>(I)V

    move-object v6, p1

    check-cast v6, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;

    invoke-virtual {v6}, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->getNewOne()Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    move-result-object v0

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    invoke-virtual {v6}, Lcom/mediatek/apst/util/command/calendar/UpdateEventReq;->getUpdateId()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->updateEvent(JLcom/mediatek/apst/util/entity/calendar/CalendarEvent;)I

    move-result v10

    const/4 v11, 0x1

    if-ge v10, v11, :cond_8

    const/4 v11, 0x2

    invoke-virtual {v9, v11}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_8
    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v11, v9}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_2

    :cond_9
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/GetRemindersReq;

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    new-instance v12, Lcom/mediatek/apst/target/service/MainService$CommandHandler$20;

    invoke-direct {v12, p0, v8}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$20;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getReminders(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_2

    :cond_a
    instance-of v11, p1, Lcom/mediatek/apst/util/command/calendar/GetAttendeesReq;

    if-eqz v11, :cond_b

    iget-object v11, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v11

    new-instance v12, Lcom/mediatek/apst/target/service/MainService$CommandHandler$21;

    invoke-direct {v12, p0, v8}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$21;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getAttendees(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_2

    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_3
.end method

.method public handleCalendarSyncFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 12
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v10, 0x2

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v5

    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartReq;

    if-eqz v9, :cond_0

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;-><init>(I)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->isSyncNeedReinit()Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->setSyncNeedReinit(Z)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getLastSyncDate()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->setLastSyncDate(J)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getLocalAccountId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->setLocalAccountId(J)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->isSyncAble()Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/mediatek/apst/util/command/sync/CalendarSyncStartRsp;->setSyncAble(Z)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_0
    const/4 v9, 0x1

    :goto_1
    return v9

    :cond_0
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverReq;

    if-eqz v9, :cond_1

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverRsp;-><init>(I)V

    move-object v4, p1

    check-cast v4, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverReq;

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v4}, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverReq;->getSyncDate()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->updateSyncDate(J)Z

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getPcSyncEventsCount()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/mediatek/apst/util/command/sync/CalendarSyncOverRsp;->setEventsCount(I)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncInitReq;

    if-eqz v9, :cond_2

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncInitRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncInitRsp;-><init>(I)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getMaxEventId()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncInitRsp;->setCurrentMaxId(J)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_2
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllEventsReq;

    if-eqz v9, :cond_3

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllEventsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllEventsReq;->getEventIdLimit()J

    move-result-wide v1

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$22;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$22;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v1, v2, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->slowSyncGetAllEvents(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_3
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllRemindersReq;

    if-eqz v9, :cond_4

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllRemindersReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllRemindersReq;->getEventIdLimit()J

    move-result-wide v1

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$23;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$23;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v1, v2, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->slowSyncGetAllReminders(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_4
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;

    if-eqz v9, :cond_5

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;->getEventIdLimit()J

    move-result-wide v1

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$24;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$24;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v1, v2, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->slowSyncGetAllAttendees(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_5
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncAddEventsReq;

    if-eqz v9, :cond_7

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncAddEventsRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncAddEventsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncAddEventsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v0

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->slowSyncAddEvents([B)[B

    move-result-object v7

    if-nez v7, :cond_6

    invoke-virtual {v8, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_2
    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v8, v7}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_2

    :cond_7
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncInitReq;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$25;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$25;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncGetAllSyncFlags(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_8
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetEventsReq;

    if-eqz v9, :cond_9

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetEventsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetEventsReq;->getRequestedEventIds()[J

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$26;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$26;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v6, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncGetEvents([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_9
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;

    if-eqz v9, :cond_a

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetRemindersReq;->getRequestedEventIds()[J

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$27;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$27;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v6, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncGetReminders([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_a
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetAttendeesReq;

    if-eqz v9, :cond_b

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetAttendeesReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncGetAttendeesReq;->getRequestedEventIds()[J

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    new-instance v10, Lcom/mediatek/apst/target/service/MainService$CommandHandler$28;

    invoke-direct {v10, p0, v5}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$28;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v9, v6, v10, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncGetAttendees([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_b
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncAddEventsReq;

    if-eqz v9, :cond_d

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncAddEventsRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncAddEventsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncAddEventsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v3

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncAddEvents([B)[B

    move-result-object v7

    if-nez v7, :cond_c

    invoke-virtual {v8, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_3
    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v8, v7}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_3

    :cond_d
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsReq;

    if-eqz v9, :cond_e

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsRsp;-><init>(I)V

    move-object v4, p1

    check-cast v4, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsReq;

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v4}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsReq;->getDeleteIds()[J

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastDeleteEvents([J)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncDeleteEventsRsp;->setDeleteCount(I)V

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_e
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncUpdateEventsReq;

    if-eqz v9, :cond_10

    new-instance v8, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncUpdateEventsRsp;

    invoke-direct {v8, v5}, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncUpdateEventsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/CalendarFastSyncUpdateEventsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v3

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->fastSyncUpdateEvents([B)[B

    move-result-object v7

    if-nez v7, :cond_f

    invoke-virtual {v8, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_4
    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v9, v8}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v8, v7}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_4

    :cond_10
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method public handleContactsFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 50
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v40

    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/ContactsBatchReq;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    new-instance v4, Lcom/mediatek/apst/util/command/contacts/ContactsBatchRsp;

    move/from16 v0, v40

    invoke-direct {v4, v0}, Lcom/mediatek/apst/util/command/contacts/ContactsBatchRsp;-><init>(I)V

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/service/MainService;->access$1002(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/ICommandBatch;)Lcom/mediatek/apst/util/command/ICommandBatch;

    move-object/from16 v39, p1

    check-cast v39, Lcom/mediatek/apst/util/command/contacts/ContactsBatchReq;

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    invoke-virtual/range {v39 .. v39}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_0
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/apst/util/command/BaseCommand;

    add-int/lit8 v28, v28, 0x1

    invoke-virtual/range {v39 .. v39}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v28

    if-ne v0, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handle(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :goto_1
    const/4 v3, 0x1

    :goto_2
    return v3

    :cond_2
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactReq;

    if-eqz v3, :cond_4

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactReq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactReq;->getContactId()J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getContact(JZ)Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v26

    if-eqz v26, :cond_3

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v3

    const/16 v4, 0x51a

    move-object/from16 v0, v43

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3, v4}, Lcom/mediatek/apst/util/command/contacts/GetDetailedContactRsp;->setDetailedContact(Lcom/mediatek/apst/util/entity/contacts/RawContact;Ljava/nio/ByteBuffer;I)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    goto :goto_3

    :cond_4
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AddGroupReq;

    if-eqz v3, :cond_6

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/AddGroupReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupReq;->getGroup()Lcom/mediatek/apst/util/entity/contacts/Group;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertGroup(Lcom/mediatek/apst/util/entity/contacts/Group;)J

    move-result-wide v33

    move-object/from16 v0, v43

    move-wide/from16 v1, v33

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->setInsertedId(J)V

    const-wide/16 v3, -0x1

    cmp-long v3, v33, v3

    if-nez v3, :cond_5

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, v43

    invoke-virtual {v0, v7}, Lcom/mediatek/apst/util/command/contacts/AddGroupRsp;->setResult(Lcom/mediatek/apst/util/entity/contacts/Group;)V

    goto :goto_4

    :cond_6
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AddContactReq;

    if-eqz v3, :cond_8

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/AddContactReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddContactReq;->getFromFeature()I

    move-result v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;->setFromFeature(I)V

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddContactReq;->getContact()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v15, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertContact(Lcom/mediatek/apst/util/entity/contacts/RawContact;Z)J

    move-result-wide v41

    const-wide/16 v3, 0x0

    cmp-long v3, v41, v3

    if-gez v3, :cond_7

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    move-object/from16 v0, v43

    move-wide/from16 v1, v41

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;->setInsertedId(J)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x1

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    move-object/from16 v0, v43

    move-wide/from16 v1, v41

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;->setInsertedId(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    const/4 v4, 0x1

    move-wide/from16 v0, v41

    invoke-virtual {v3, v0, v1, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getContact(JZ)Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/AddContactRsp;->setResult(Lcom/mediatek/apst/util/entity/contacts/RawContact;)V

    goto :goto_5

    :cond_8
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AddSimContactReq;

    if-eqz v3, :cond_a

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AddSimContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddSimContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/AddSimContactReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddSimContactReq;->getContact()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getPrimaryNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getStoreLocation()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertSimContact(Ljava/lang/String;Ljava/lang/String;I)J

    move-result-wide v35

    const-wide/16 v3, -0x1

    cmp-long v3, v35, v3

    if-nez v3, :cond_9

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AddContactDataReq;

    if-eqz v3, :cond_c

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AddContactDataRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddContactDataRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/AddContactDataReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddContactDataReq;->getData()Lcom/mediatek/apst/util/entity/contacts/ContactData;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v3, v0, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertContactData(Lcom/mediatek/apst/util/entity/contacts/ContactData;Z)J

    move-result-wide v31

    move-object/from16 v0, v43

    move-wide/from16 v1, v31

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/contacts/AddContactDataRsp;->setInsertedId(J)V

    const-wide/16 v3, -0x1

    cmp-long v3, v31, v3

    if-nez v3, :cond_b

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, v43

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddContactDataRsp;->setResult(Lcom/mediatek/apst/util/entity/contacts/ContactData;)V

    goto :goto_6

    :cond_c
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;

    if-eqz v3, :cond_10

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getGroupEntry()Lcom/mediatek/apst/util/entity/contacts/Group;

    move-result-object v7

    const/4 v8, 0x0

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-eqz v3, :cond_d

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getSimIndexes()[I

    move-result-object v8

    :cond_d
    if-eqz v8, :cond_e

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-nez v3, :cond_f

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getContactIds()[J

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getGroupId()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertGroupMembership([JJ)[J

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipRsp;->setInsertedIds([J)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getContactIds()[J

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipReq;->getGroupId()J

    move-result-wide v5

    invoke-virtual/range {v3 .. v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->insertGroupMembership([JJLcom/mediatek/apst/util/entity/contacts/Group;[I)[J

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/AddGroupMembershipRsp;->setInsertedIds([J)V

    goto :goto_7

    :cond_10
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;

    if-eqz v3, :cond_15

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/UpdateGroupRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/UpdateGroupRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/Group;

    move-result-object v7

    const/16 v37, 0x0

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-eqz v3, :cond_11

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;->getOldName()Ljava/lang/String;

    move-result-object v37

    :cond_11
    const/16 v47, 0x0

    if-eqz v37, :cond_12

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-nez v3, :cond_14

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;->getUpdateId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5, v7}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateGroup(JLcom/mediatek/apst/util/entity/contacts/Group;)I

    move-result v47

    :goto_8
    const/4 v3, 0x1

    move/from16 v0, v47

    if-ge v0, v3, :cond_13

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateGroupReq;->getUpdateId()J

    move-result-wide v4

    move-object/from16 v0, v37

    invoke-virtual {v3, v4, v5, v7, v0}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateGroup(JLcom/mediatek/apst/util/entity/contacts/Group;Ljava/lang/String;)I

    move-result v47

    goto :goto_8

    :cond_15
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactReq;

    if-eqz v3, :cond_17

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v9

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateRawContactReq;->getUpdateId()J

    move-result-wide v10

    const/16 v12, -0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v9 .. v16}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateContact(JILjava/lang/String;Ljava/lang/String;Lcom/mediatek/apst/util/entity/contacts/RawContact;Z)I

    move-result v48

    const/4 v3, 0x1

    move/from16 v0, v48

    if-ge v0, v3, :cond_16

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_17
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;

    if-eqz v3, :cond_19

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v16

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getUpdateId()J

    move-result-wide v17

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getSourceLocation()I

    move-result v19

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getSimName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getSimNumber()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getSimEmail()Ljava/lang/String;

    move-result-object v22

    const/16 v24, 0x1

    move-object/from16 v23, v15

    invoke-virtual/range {v16 .. v24}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateContact(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/apst/util/entity/contacts/RawContact;Z)I

    move-result v46

    const/4 v3, 0x1

    move/from16 v0, v46

    if-ge v0, v3, :cond_18

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactReq;->getUpdateId()J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getContact(JZ)Lcom/mediatek/apst/util/entity/contacts/RawContact;

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/UpdateDetailedContactRsp;->setResult(Lcom/mediatek/apst/util/entity/contacts/RawContact;)V

    goto :goto_9

    :cond_19
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;

    if-eqz v3, :cond_1b

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v9

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;->getOldName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;->getOldNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getDisplayName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/BaseContact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/contacts/BaseContact;->getPrimaryNumber()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateSimContactReq;->getSimId()I

    move-result v14

    invoke-virtual/range {v9 .. v14}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateSimContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v49

    const/4 v3, 0x1

    move/from16 v0, v49

    if-ge v0, v3, :cond_1a

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_1b
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataReq;

    if-eqz v3, :cond_1d

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataReq;->getNewOne()Lcom/mediatek/apst/util/entity/contacts/ContactData;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/UpdateContactDataReq;->getUpdateId()J

    move-result-wide v4

    const/4 v6, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateContactData(JLcom/mediatek/apst/util/entity/contacts/ContactData;Z)I

    move-result v45

    const/4 v3, 0x1

    move/from16 v0, v45

    if-ge v0, v3, :cond_1c

    const/4 v3, 0x2

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_1d
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/DeleteGroupReq;

    if-eqz v3, :cond_21

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/DeleteGroupRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/DeleteGroupReq;

    const/16 v27, 0x0

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-eqz v3, :cond_1e

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupReq;->getGroups()Ljava/util/ArrayList;

    move-result-object v27

    :cond_1e
    if-eqz v27, :cond_1f

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-nez v3, :cond_20

    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupReq;->getDeleteIds()[J

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteGroup([J)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupRsp;->setDeleteResults([Z)V

    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupReq;->getDeleteIds()[J

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v3, v4, v0}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteGroup([JLjava/util/ArrayList;)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteGroupRsp;->setDeleteResults([Z)V

    goto :goto_a

    :cond_21
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;

    if-eqz v3, :cond_22

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/DeleteContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/DeleteContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v16

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->getDeleteIds()[J

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->getSourceLocation()I

    move-result v19

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->getSimNames()[Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->getSimNumbers()[Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->getSimEmails()[Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v16 .. v22}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteContacts([JZI[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteContactRsp;->setDeleteResults([Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_22
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/DeleteAllContactsReq;

    if-eqz v3, :cond_23

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/DeleteAllContactsRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/DeleteAllContactsRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteAllContacts(Z)I

    move-result v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteAllContactsRsp;->setDeleteCount(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_23
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactReq;

    if-eqz v3, :cond_24

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactReq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactReq;->getNames()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactReq;->getNumbers()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactReq;->getSimId()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteSimContacts([Ljava/lang/String;[Ljava/lang/String;I)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteSimContactRsp;->setDeleteResults([Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_24
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;

    if-eqz v3, :cond_27

    new-instance v43, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataRsp;-><init>(I)V

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;->getGroupEntry()Lcom/mediatek/apst/util/entity/contacts/Group;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;->getSimIndexes()[I

    move-result-object v8

    if-eqz v8, :cond_25

    sget v3, Lcom/mediatek/apst/util/FeatureOptionControl;->CONTACT_N_USIMGROUP:I

    if-nez v3, :cond_26

    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;->getDeleteIds()[J

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteContactData([J)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataRsp;->setDeleteResults([Z)V

    :goto_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;->getDeleteIds()[J

    move-result-object v4

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataReq;->getGroupId()J

    move-result-wide v5

    invoke-virtual/range {v3 .. v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->deleteContactData([JJLcom/mediatek/apst/util/entity/contacts/Group;[I)[Z

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/contacts/DeleteContactDataRsp;->setDeleteResults([Z)V

    goto :goto_b

    :cond_27
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllGroupsReq;

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    new-instance v4, Lcom/mediatek/apst/target/service/MainService$CommandHandler$1;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v4, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$1;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllGroups(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    :cond_28
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllSimContactsReq;

    if-eqz v3, :cond_2a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1200(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible()Z

    move-result v44

    if-eqz v44, :cond_29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    new-instance v4, Lcom/mediatek/apst/target/service/MainService$CommandHandler$2;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v4, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$2;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllSimContacts(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    :cond_29
    new-instance v43, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllSimContactsRsp;

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllSimContactsRsp;-><init>(I)V

    const/4 v3, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    const/4 v3, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setProgress(I)V

    const/4 v3, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setTotal(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_2a
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllRawContactsReq;

    if-eqz v3, :cond_2b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    new-instance v4, Lcom/mediatek/apst/target/service/MainService$CommandHandler$3;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v4, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$3;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllRawContacts(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    :cond_2b
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllContactDataReq;

    if-eqz v3, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    check-cast p1, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllContactDataReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/contacts/AsyncGetAllContactDataReq;->getRequestingDataTypes()Ljava/util/ArrayList;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$4;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v5, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$4;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->asyncGetAllContactData(Ljava/util/List;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    :cond_2c
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/mediatek/apst/util/command/contacts/ImportDetailedContactsReq;

    if-eqz v3, :cond_2d

    move-object/from16 v38, p1

    check-cast v38, Lcom/mediatek/apst/util/command/contacts/ImportDetailedContactsReq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v3}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v3

    invoke-virtual/range {v38 .. v38}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$5;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v5, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$5;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    new-instance v6, Lcom/mediatek/apst/target/service/MainService$CommandHandler$6;

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v6, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$6;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v9

    invoke-virtual {v3, v4, v5, v6, v9}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastImportDetailedContacts([BLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    :cond_2d
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public handleMainFrameFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 13
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v12, 0x3

    const/4 v11, 0x2

    const-wide/16 v9, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v4

    instance-of v8, p1, Lcom/mediatek/apst/util/command/sysinfo/GreetingReq;

    if-eqz v8, :cond_1

    new-instance v5, Lcom/mediatek/apst/util/command/sysinfo/GreetingRsp;

    invoke-direct {v5, v4}, Lcom/mediatek/apst/util/command/sysinfo/GreetingRsp;-><init>(I)V

    const/16 v6, 0x51a

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GreetingRsp;->setVersionCode(I)V

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v6, v5}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_0
    move v6, v7

    :cond_0
    return v6

    :cond_1
    instance-of v8, p1, Lcom/mediatek/apst/util/command/sysinfo/SysInfoBatchReq;

    if-eqz v8, :cond_4

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    new-instance v8, Lcom/mediatek/apst/util/command/sysinfo/SysInfoBatchRsp;

    invoke-direct {v8, v4}, Lcom/mediatek/apst/util/command/sysinfo/SysInfoBatchRsp;-><init>(I)V

    invoke-static {v6, v8}, Lcom/mediatek/apst/target/service/MainService;->access$1002(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/ICommandBatch;)Lcom/mediatek/apst/util/command/ICommandBatch;

    move-object v3, p1

    check-cast v3, Lcom/mediatek/apst/util/command/sysinfo/SysInfoBatchReq;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v6, v11}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    invoke-virtual {v3}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/apst/util/command/BaseCommand;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v0, v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v6, v12}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_2
    invoke-virtual {p0, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handle(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v6, v7}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    goto :goto_0

    :cond_4
    instance-of v8, p1, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoReq;

    if-eqz v8, :cond_7

    new-instance v5, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;

    invoke-direct {v5, v4}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;-><init>(I)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getDevice()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setDevice(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getManufacturer()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setManufacturer(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setFirmwareVersion(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getModel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setModel(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/util/FeatureOptionControl;->getFeatureList()[I

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setFeatureOptionList([I)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSdMounted()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v5, v7}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdMounted(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSdWriteable()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdWriteable(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardPath(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdTotalSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardTotalSpace(J)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdAvailableSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardAvailableSpace(J)V

    :goto_2
    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1200(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->checkSDCardState()[Z

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSDCardAndEmmcState([Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getInternalTotalSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setInternalTotalSpace(J)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getInternalAvailableSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setInternalAvailableSpace(J)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getAvailableContactsCount()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setContactsCount(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMessagesCount()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setMessagesCount(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;->getApplicationsCount()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setApplicationsCount(I)V

    invoke-virtual {v5, v7}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setGemini(Z)V

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setGemini3Sim(Z)V

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setGemini4Sim(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim1Accessible()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim1Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim2Accessible()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim2Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim3Accessible()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim3Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim4Accessible()Z

    move-result v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim4Accessible(Z)V

    invoke-static {v6}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim1Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-static {v7}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim2Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-static {v11}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim3Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-static {v12}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSim4Info(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->getSlotInfoList()Ljava/util/List;

    move-result-object v8

    invoke-static {v6}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->getSlotInfoList()Ljava/util/List;

    move-result-object v8

    invoke-static {v7}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->getClassName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "handle"

    new-array v10, v7, [Ljava/lang/Object;

    aput-object p1, v10, v6

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Dual-SIM | SIM:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->isSimAccessible()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " | SIM1:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->isSim1Accessible()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " | SIM2:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->isSim2Accessible()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " | SIM3:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->isSim3Accessible()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " | SIM4:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->isSim4Accessible()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11}, Lcom/mediatek/apst/target/util/Debugger;->logD(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getAllSIMList()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSimInfoList(Ljava/util/List;)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1400(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/receiver/InternalReceiver;

    move-result-object v8

    if-eqz v8, :cond_6

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v6}, Lcom/mediatek/apst/target/service/MainService;->access$1400(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/receiver/InternalReceiver;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/apst/target/receiver/InternalReceiver;->getBatteryLevel()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setBatteryLevel(I)V

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v6}, Lcom/mediatek/apst/target/service/MainService;->access$1400(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/receiver/InternalReceiver;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/apst/target/receiver/InternalReceiver;->getBatteryScale()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setBatteryScale(I)V

    :goto_3
    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v6, v5}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdMounted(Z)V

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdWriteable(Z)V

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardPath(Ljava/lang/String;)V

    invoke-virtual {v5, v9, v10}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardTotalSpace(J)V

    invoke-virtual {v5, v9, v10}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setSdCardAvailableSpace(J)V

    goto/16 :goto_2

    :cond_6
    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setBatteryLevel(I)V

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetSysInfoRsp;->setBatteryScale(I)V

    goto :goto_3

    :cond_7
    instance-of v8, p1, Lcom/mediatek/apst/util/command/sysinfo/GetStorageReq;

    if-eqz v8, :cond_0

    new-instance v5, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;

    invoke-direct {v5, v4}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;-><init>(I)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSdMounted()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v5, v7}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdMounted(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSdWriteable()Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdWriteable(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardPath(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdTotalSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardTotalSpace(J)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdAvailableSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardAvailableSpace(J)V

    :goto_4
    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getInternalTotalSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setInternalTotalSpace(J)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getInternalAvailableSpace()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setInternalAvailableSpace(J)V

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v6, v5}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdMounted(Z)V

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdWriteable(Z)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardPath(Ljava/lang/String;)V

    invoke-virtual {v5, v9, v10}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardTotalSpace(J)V

    invoke-virtual {v5, v9, v10}, Lcom/mediatek/apst/util/command/sysinfo/GetStorageRsp;->setSdCardAvailableSpace(J)V

    goto :goto_4
.end method

.method public handleMediaFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 8
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v7, 0x2

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v1

    instance-of v4, p1, Lcom/mediatek/apst/util/command/media/GetContentDirectoriesReq;

    if-eqz v4, :cond_1

    new-instance v3, Lcom/mediatek/apst/util/command/media/GetContentDirectoriesRsp;

    invoke-direct {v3, v1}, Lcom/mediatek/apst/util/command/media/GetContentDirectoriesRsp;-><init>(I)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->getContentDirectories()[Lcom/mediatek/apst/util/entity/media/MediaInfo;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v3, v7}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_0
    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_1
    const/4 v4, 0x1

    :goto_2
    return v4

    :cond_0
    invoke-virtual {v3, v2}, Lcom/mediatek/apst/util/command/media/GetContentDirectoriesRsp;->setDirectories([Lcom/mediatek/apst/util/entity/media/MediaInfo;)V

    goto :goto_0

    :cond_1
    instance-of v4, p1, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesReq;

    if-eqz v4, :cond_3

    new-instance v3, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesRsp;

    invoke-direct {v3, v1}, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesRsp;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesReq;

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesReq;->getRequestedContentTypes()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->getFiles(I)[Lcom/mediatek/apst/util/entity/media/MediaInfo;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {v3, v7}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_3
    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v2}, Lcom/mediatek/apst/util/command/media/GetAllMediaFilesRsp;->setFiles([Lcom/mediatek/apst/util/entity/media/MediaInfo;)V

    goto :goto_3

    :cond_3
    instance-of v4, p1, Lcom/mediatek/apst/util/command/media/RenameFilesReq;

    if-eqz v4, :cond_5

    new-instance v3, Lcom/mediatek/apst/util/command/media/RenameFilesRsp;

    invoke-direct {v3, v1}, Lcom/mediatek/apst/util/command/media/RenameFilesRsp;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/mediatek/apst/util/command/media/RenameFilesReq;

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/apst/util/command/media/RenameFilesReq;->getOldPaths()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/apst/util/command/media/RenameFilesReq;->getNewPaths()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->renameFiles([Ljava/lang/String;[Ljava/lang/String;)[Z

    move-result-object v2

    if-nez v2, :cond_4

    invoke-virtual {v3, v7}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_4
    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v3, v2}, Lcom/mediatek/apst/util/command/media/RenameFilesRsp;->setResults([Z)V

    goto :goto_4

    :cond_5
    instance-of v4, p1, Lcom/mediatek/apst/util/command/media/MediaSyncOverReq;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v5}, Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;->scan(Landroid/content/Context;)V

    new-instance v3, Lcom/mediatek/apst/util/command/media/MediaSyncOverRsp;

    invoke-direct {v3, v1}, Lcom/mediatek/apst/util/command/media/MediaSyncOverRsp;-><init>(I)V

    iget-object v4, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v4, v3}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public handleMessageFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 37
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v28

    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/MessageBatchReq;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    new-instance v5, Lcom/mediatek/apst/util/command/message/MessageBatchRsp;

    move/from16 v0, v28

    invoke-direct {v5, v0}, Lcom/mediatek/apst/util/command/message/MessageBatchRsp;-><init>(I)V

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/service/MainService;->access$1002(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/ICommandBatch;)Lcom/mediatek/apst/util/command/ICommandBatch;

    move-object/from16 v27, p1

    check-cast v27, Lcom/mediatek/apst/util/command/message/MessageBatchReq;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    invoke-virtual/range {v27 .. v27}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/mediatek/apst/util/command/BaseCommand;

    add-int/lit8 v15, v15, 0x1

    invoke-virtual/range {v27 .. v27}, Lcom/mediatek/apst/util/command/RequestBatch;->getCommands()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v15, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->handle(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/service/MainService;->access$1102(Lcom/mediatek/apst/target/service/MainService;I)I

    :cond_2
    :goto_1
    const/4 v4, 0x1

    :goto_2
    return v4

    :cond_3
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/AsyncGetPhoneListReq;

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$7;

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v5, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$7;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->asyncGetPhoneList(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_1

    :cond_4
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/AsyncGetAllSmsReq;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$8;

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v5, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$8;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->asyncGetAllSms(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_1

    :cond_5
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/AsyncGetAllMmsReq;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$9;

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v5, v0, v1}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$9;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->asyncGetAllMms(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto :goto_1

    :cond_6
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/GetMmsResourceReq;

    if-eqz v4, :cond_7

    check-cast p1, Lcom/mediatek/apst/util/command/message/GetMmsResourceReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/message/GetMmsResourceReq;->getMmsId()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$10;

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-wide/from16 v2, v17

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$10;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;IJ)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    move-wide/from16 v0, v17

    invoke-virtual {v4, v5, v6, v0, v1}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getOneMmsResource(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;J)V

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;

    if-eqz v4, :cond_8

    move-object/from16 v4, p1

    check-cast v4, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;

    invoke-virtual {v4}, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->getIsBackup()Z

    move-result v21

    check-cast p1, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/apst/util/command/message/GetMmsDataReq;->getImportList()Ljava/util/LinkedList;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    new-instance v5, Lcom/mediatek/apst/target/service/MainService$CommandHandler$11;

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v21

    invoke-direct {v5, v0, v1, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$11;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;IZ)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v4, v5, v6, v0, v1}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMmsData(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;ZLjava/util/LinkedList;)V

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/ImportSmsReq;

    if-eqz v4, :cond_c

    new-instance v31, Lcom/mediatek/apst/util/command/message/ImportSmsRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportSmsRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/ImportSmsReq;

    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/MulMessageObserver;->stop()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v4, v5, v0}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->importSms([BLjava/util/ArrayList;)[J

    move-result-object v20

    if-nez v20, :cond_9

    const/4 v4, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/MulMessageObserver;->start()V

    goto/16 :goto_1

    :cond_9
    if-eqz v33, :cond_b

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v36, v0

    const/4 v15, 0x0

    :goto_5
    move-object/from16 v0, v36

    array-length v4, v0

    if-ge v15, v4, :cond_a

    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v36, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, v31

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportSmsRsp;->setThreadIds([J)V

    :cond_b
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportSmsRsp;->setInsertedIds([J)V

    goto :goto_3

    :catch_0
    move-exception v14

    invoke-virtual {v14}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :cond_c
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/ImportMmsReq;

    if-eqz v4, :cond_11

    new-instance v31, Lcom/mediatek/apst/util/command/message/ImportMmsRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportMmsRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/ImportMmsReq;

    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/MulMessageObserver;->getMaxMmsId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, ">>pdu table is null"

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/MulMessageObserver;->onSelfChangeStart()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->getMaxMmsIdByInsert()J

    move-result-wide v23

    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/MulMessageObserver;->onSelfChangeDone()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;

    move-result-object v4

    move-wide/from16 v0, v23

    invoke-virtual {v4, v0, v1}, Lcom/mediatek/apst/target/service/MulMessageObserver;->setMaxMmsId(J)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v4, v5, v0}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->importMms([BLjava/util/ArrayList;)[J

    move-result-object v20

    if-nez v20, :cond_e

    const/4 v4, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_7
    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ImportMmsReq;->isIsLastImport()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :catch_1
    move-exception v14

    invoke-virtual {v14}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_6

    :cond_e
    if-eqz v33, :cond_10

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v36, v0

    const/4 v15, 0x0

    :goto_8
    move-object/from16 v0, v36

    array-length v4, v0

    if-ge v15, v4, :cond_f

    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v36, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    :cond_f
    move-object/from16 v0, v31

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportMmsRsp;->setThreadIds([J)V

    :cond_10
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/ImportMmsRsp;->setInsertedIds([J)V

    goto :goto_7

    :cond_11
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/ClearMessageBoxReq;

    if-eqz v4, :cond_12

    new-instance v31, Lcom/mediatek/apst/util/command/message/ClearMessageBoxRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/ClearMessageBoxRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/ClearMessageBoxReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ClearMessageBoxReq;->getBox()I

    move-result v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ClearMessageBoxReq;->isKeepLockedMessage()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->clearMessageBox(IZ)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/ClearMessageBoxRsp;->setDeletedCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_12
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesReq;

    if-eqz v4, :cond_13

    new-instance v31, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesReq;->isKeepLockedMessage()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->deleteAllMessages(Z)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/DeleteAllMessagesRsp;->setDeletedCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_13
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;

    if-eqz v4, :cond_14

    new-instance v31, Lcom/mediatek/apst/util/command/message/DeleteMessageRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/DeleteMessageRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->getDeleteSmsIds()[J

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->getDeleteSmsDates()[J

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->deleteSms([JZ[J)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/DeleteMessageRsp;->setDeleteSmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->getDeleteMmsIds()[J

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/DeleteMessageReq;->getDeleteMmsDates()[J

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->deleteMms([JZ[J)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/DeleteMessageRsp;->setDeleteMmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_14
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;

    if-eqz v4, :cond_15

    new-instance v31, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;->getUpdateSmsIds()[J

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;->isRead()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->markSmsAsRead([JZ)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadRsp;->setUpdateSmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;->getUpdateMmsIds()[J

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadReq;->isRead()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->markMmsAsRead([JZ)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/MarkMessageAsReadRsp;->setUpdateMmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_15
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/LockMessageReq;

    if-eqz v4, :cond_16

    new-instance v31, Lcom/mediatek/apst/util/command/message/LockMessageRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/LockMessageRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/LockMessageReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/LockMessageReq;->getUpdateSmsIds()[J

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/LockMessageReq;->isLocked()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->lockSms([JZ)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/LockMessageRsp;->setUpdateSmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/LockMessageReq;->getUpdateMmsIds()[J

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/LockMessageReq;->isLocked()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->lockMms([JZ)I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/LockMessageRsp;->setUpdateMmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_16
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/SaveSmsDraftReq;

    if-eqz v4, :cond_18

    new-instance v31, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/SaveSmsDraftReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftReq;->getBody()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftReq;->getRecipients()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->saveSmsDraft(Ljava/lang/String;[Ljava/lang/String;)Lcom/mediatek/apst/util/entity/message/Sms;

    move-result-object v29

    if-eqz v29, :cond_17

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->setInsertedId(J)V

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/apst/util/entity/message/Message;->getThreadId()J

    move-result-wide v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->setThreadId(J)V

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/apst/util/entity/message/Message;->getDate()J

    move-result-wide v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/mediatek/apst/util/command/message/SaveSmsDraftRsp;->setDate(J)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_17
    const/4 v4, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    goto :goto_9

    :cond_18
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/SendSmsReq;

    if-eqz v4, :cond_1b

    new-instance v31, Lcom/mediatek/apst/util/command/message/SendSmsRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/SendSmsRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/SendSmsReq;

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SendSmsReq;->getSimId()I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->setSimId(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/SmsSender;->pause()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SendSmsReq;->getBody()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SendSmsReq;->getRecipients()[Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v7}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v7

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/SendSmsReq;->getSimId()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->sendSms(Ljava/lang/String;[Ljava/lang/String;Lcom/mediatek/apst/target/service/SmsSender;I)[Lcom/mediatek/apst/util/entity/message/Sms;

    move-result-object v30

    if-nez v30, :cond_19

    const/4 v4, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/SmsSender;->resume()V

    goto/16 :goto_1

    :cond_19
    move-object/from16 v0, v30

    array-length v4, v0

    new-array v0, v4, [J

    move-object/from16 v20, v0

    const-wide/16 v34, -0x1

    move-object/from16 v0, v30

    array-length v4, v0

    new-array v13, v4, [J

    const/4 v15, 0x0

    :goto_b
    move-object/from16 v0, v30

    array-length v4, v0

    if-ge v15, v4, :cond_1a

    aget-object v4, v30, v15

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v4

    aput-wide v4, v20, v15

    aget-object v4, v30, v15

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/message/Message;->getThreadId()J

    move-result-wide v34

    aget-object v4, v30, v15

    invoke-virtual {v4}, Lcom/mediatek/apst/util/entity/message/Message;->getDate()J

    move-result-wide v4

    aput-wide v4, v13, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_b

    :cond_1a
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->setInsertedIds([J)V

    move-object/from16 v0, v31

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->setThreadId(J)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Lcom/mediatek/apst/util/command/message/SendSmsRsp;->setDates([J)V

    goto :goto_a

    :cond_1b
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;

    if-eqz v4, :cond_1c

    new-instance v31, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getUpdateSmsIds()[J

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getUpdateSmsDates()[J

    move-result-object v7

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getBox()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->moveSmsToBox([JZ[JI)I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxRsp;->setUpdateSmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getUpdateSmsIds()[J

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getUpdateSmsDates()[J

    move-result-object v7

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxReq;->getBox()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->moveMmsToBox([JZ[JI)I

    move-result v25

    move-object/from16 v0, v31

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/MoveMessageToBoxRsp;->setUpdateMmsCount(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_1c
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;

    if-eqz v4, :cond_1e

    new-instance v31, Lcom/mediatek/apst/util/command/message/ResendSmsRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/ResendSmsRsp;-><init>(I)V

    move-object/from16 v26, p1

    check-cast v26, Lcom/mediatek/apst/util/command/message/ResendSmsReq;

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getSimId()I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/message/ResendSmsRsp;->setSimId(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/SmsSender;->pause()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getId()J

    move-result-wide v5

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getDate()J

    move-result-wide v7

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getBody()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getRecipient()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v11}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v11

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->getSimId()I

    move-result v12

    invoke-virtual/range {v4 .. v12}, Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;->resendSms(JJLjava/lang/String;Ljava/lang/String;Lcom/mediatek/apst/target/service/SmsSender;I)Lcom/mediatek/apst/util/entity/message/Sms;

    move-result-object v29

    if-nez v29, :cond_1d

    const/4 v4, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    const-string v4, "Sms to resend does not exist!"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/mediatek/apst/util/command/ResponseCommand;->setErrorMessage(Ljava/lang/String;)V

    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v4}, Lcom/mediatek/apst/target/service/MainService;->access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/apst/target/service/SmsSender;->resume()V

    goto/16 :goto_1

    :cond_1d
    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/apst/util/entity/message/Message;->getDate()J

    move-result-wide v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/mediatek/apst/util/command/message/ResendSmsRsp;->setDate(J)V

    goto :goto_c

    :cond_1e
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/apst/util/command/message/BeforeImportMmsReq;

    if-eqz v4, :cond_1f

    new-instance v31, Lcom/mediatek/apst/util/command/message/BeforeImportMmsRsp;

    move-object/from16 v0, v31

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/mediatek/apst/util/command/message/BeforeImportMmsRsp;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_1

    :cond_1f
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public handleSyncFeatures(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 11
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v8, 0x0

    const/4 v10, 0x2

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v4

    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSyncStartReq;

    if-eqz v9, :cond_1

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsSyncStartRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsSyncStartRsp;-><init>(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->isSyncNeedReinit()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/mediatek/apst/util/command/sync/ContactsSyncStartRsp;->setSyncNeedReinit(Z)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getLastSyncDate()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/apst/util/command/sync/ContactsSyncStartRsp;->setLastSyncDate(J)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    :goto_0
    const/4 v8, 0x1

    :cond_0
    return v8

    :cond_1
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverReq;

    if-eqz v9, :cond_2

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverRsp;-><init>(I)V

    move-object v3, p1

    check-cast v3, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverReq;

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v3}, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverReq;->getSyncDate()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->updateSyncDate(J)Z

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getAvailableContactsCount()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/mediatek/apst/util/command/sync/ContactsSyncOverRsp;->setContactsCount(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_2
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncInitReq;

    if-eqz v9, :cond_3

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncInitRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncInitRsp;-><init>(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getMaxRawContactsId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncInitRsp;->setCurrentMaxId(J)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_3
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncAddDetailedContactsReq;

    if-eqz v9, :cond_5

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncAddDetailedContactsRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncAddDetailedContactsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncAddDetailedContactsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->slowSyncAddDetailedContacts([B)[B

    move-result-object v6

    if-nez v6, :cond_4

    invoke-virtual {v7, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_1
    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v7, v6}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_1

    :cond_5
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllRawContactsReq;

    if-eqz v9, :cond_6

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllRawContactsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllRawContactsReq;->getContactIdLimit()J

    move-result-wide v0

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    new-instance v9, Lcom/mediatek/apst/target/service/MainService$CommandHandler$13;

    invoke-direct {v9, p0, v4}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$13;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v8, v0, v1, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->slowSyncGetAllRawContacts(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_6
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllContactDataReq;

    if-eqz v9, :cond_7

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllContactDataReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/ContactsSlowSyncGetAllContactDataReq;->getContactIdLimit()J

    move-result-wide v0

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    new-instance v9, Lcom/mediatek/apst/target/service/MainService$CommandHandler$14;

    invoke-direct {v9, p0, v4}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$14;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v8, v0, v1, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->slowSyncGetAllContactData(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_7
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncInitReq;

    if-eqz v9, :cond_8

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    new-instance v9, Lcom/mediatek/apst/target/service/MainService$CommandHandler$15;

    invoke-direct {v9, p0, v4}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$15;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastSyncGetAllSyncFlags(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_8
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetRawContactsReq;

    if-eqz v9, :cond_9

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetRawContactsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetRawContactsReq;->getRequestedContactIds()[J

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    new-instance v9, Lcom/mediatek/apst/target/service/MainService$CommandHandler$16;

    invoke-direct {v9, p0, v4}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$16;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v8, v5, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastSyncGetRawContacts([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_9
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetContactDataReq;

    if-eqz v9, :cond_a

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetContactDataReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncGetContactDataReq;->getRequestedContactIds()[J

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    new-instance v9, Lcom/mediatek/apst/target/service/MainService$CommandHandler$17;

    invoke-direct {v9, p0, v4}, Lcom/mediatek/apst/target/service/MainService$CommandHandler$17;-><init>(Lcom/mediatek/apst/target/service/MainService$CommandHandler;I)V

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v8, v5, v9, v10}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastSyncGetContactData([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_a
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncAddDetailedContactsReq;

    if-eqz v9, :cond_c

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncAddDetailedContactsRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncAddDetailedContactsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncAddDetailedContactsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastSyncAddDetailedContacts([B)[B

    move-result-object v6

    if-nez v6, :cond_b

    invoke-virtual {v7, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_2
    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {v7, v6}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_2

    :cond_c
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncUpdateDetailedContactsReq;

    if-eqz v9, :cond_e

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncUpdateDetailedContactsRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncUpdateDetailedContactsRsp;-><init>(I)V

    check-cast p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncUpdateDetailedContactsReq;

    invoke-virtual {p1}, Lcom/mediatek/apst/util/command/RawBlockRequest;->getRaw()[B

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v8}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastSyncUpdateDetailedContacts([B)[B

    move-result-object v6

    if-nez v6, :cond_d

    invoke-virtual {v7, v10}, Lcom/mediatek/apst/util/command/ResponseCommand;->setStatusCode(I)V

    :goto_3
    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v7, v6}, Lcom/mediatek/apst/util/command/RawBlockResponse;->setRaw([B)V

    goto :goto_3

    :cond_e
    instance-of v9, p1, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsReq;

    if-eqz v9, :cond_0

    new-instance v7, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsRsp;

    invoke-direct {v7, v4}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsRsp;-><init>(I)V

    move-object v3, p1

    check-cast v3, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsReq;

    iget-object v9, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-static {v9}, Lcom/mediatek/apst/target/service/MainService;->access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v9

    invoke-virtual {v3}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsReq;->getDeleteIds()[J

    move-result-object v10

    invoke-virtual {v9, v10, v8}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->fastDeleteContactsSourcedOnPhone([JZ)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/mediatek/apst/util/command/sync/ContactsFastSyncDeleteContactsRsp;->setDeleteCount(I)V

    iget-object v8, p0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;->this$0:Lcom/mediatek/apst/target/service/MainService;

    invoke-virtual {v8, v7}, Lcom/mediatek/apst/target/service/MainService;->onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto/16 :goto_0
.end method
