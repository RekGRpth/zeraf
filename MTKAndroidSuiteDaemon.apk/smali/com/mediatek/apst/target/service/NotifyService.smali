.class public Lcom/mediatek/apst/target/service/NotifyService;
.super Landroid/app/IntentService;
.source "NotifyService.java"


# static fields
.field public static final SIM_ID:Ljava/lang/String; = "simid"


# instance fields
.field private mSim1OK:Ljava/lang/Boolean;

.field private mSim2OK:Ljava/lang/Boolean;

.field private mSim3OK:Ljava/lang/Boolean;

.field private mSim4OK:Ljava/lang/Boolean;

.field private mSimOK:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "NotifyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private onSimStatusChange(Landroid/content/Intent;)V
    .locals 29
    .param p1    # Landroid/content/Intent;

    const-string v26, "Action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_0

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object p1, v26, v27

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "strAction:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v26, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const-string v26, "simid"

    const-wide/16 v27, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move-wide/from16 v2, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v21

    move-wide/from16 v0, v21

    long-to-int v0, v0

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Global;->getSimInfoById(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v23

    if-eqz v23, :cond_1

    invoke-virtual/range {v23 .. v23}, Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;->getSlotId()I

    move-result v24

    if-nez v24, :cond_a

    const/4 v4, 0x1

    :cond_1
    :goto_0
    const/16 v26, 0x0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v11

    const/16 v26, 0x1

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v14

    const/16 v26, 0x2

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v17

    const/16 v26, 0x3

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSimState(I)I

    move-result v20

    invoke-static {v11}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v10

    invoke-static {v14}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v13

    invoke-static/range {v17 .. v17}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v16

    invoke-static/range {v20 .. v20}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible(I)Z

    move-result v19

    const/16 v26, 0x0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v9

    const/16 v26, 0x1

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v12

    const/16 v26, 0x2

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v15

    const/16 v26, 0x3

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    move/from16 v0, v26

    if-ne v0, v10, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    new-instance v26, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v26 .. v26}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v27, "state"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_id"

    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info_flag"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    if-eqz v26, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    move/from16 v0, v26

    if-ne v0, v13, :cond_4

    if-eqz v5, :cond_5

    :cond_4
    new-instance v26, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v26 .. v26}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v27, "state"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_id"

    const/16 v28, 0x1

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v12}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info_flag"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim3OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    if-eqz v26, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim3OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    move/from16 v0, v26

    move/from16 v1, v16

    if-ne v0, v1, :cond_6

    if-eqz v4, :cond_7

    :cond_6
    new-instance v26, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v26 .. v26}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v27, "state"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_id"

    const/16 v28, 0x2

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v15}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info_flag"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim4OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    if-eqz v26, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/service/NotifyService;->mSim4OK:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    move/from16 v0, v26

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    if-eqz v7, :cond_9

    :cond_8
    new-instance v26, Lcom/mediatek/apst/target/event/Event;

    invoke-direct/range {v26 .. v26}, Lcom/mediatek/apst/target/event/Event;-><init>()V

    const-string v27, "state"

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_id"

    const/16 v28, 0x3

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    const-string v27, "sim_info_flag"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-virtual/range {v26 .. v28}, Lcom/mediatek/apst/target/event/Event;->put(Ljava/lang/String;Ljava/lang/Object;)Lcom/mediatek/apst/target/event/Event;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/apst/target/event/EventDispatcher;->dispatchSimStateChangedEvent(Lcom/mediatek/apst/target/event/Event;)V

    :cond_9
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim1OK:Ljava/lang/Boolean;

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim2OK:Ljava/lang/Boolean;

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim3OK:Ljava/lang/Boolean;

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/service/NotifyService;->mSim4OK:Ljava/lang/Boolean;

    return-void

    :cond_a
    const/16 v26, 0x1

    move/from16 v0, v26

    move/from16 v1, v24

    if-ne v0, v1, :cond_b

    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_b
    const/16 v26, 0x2

    move/from16 v0, v26

    move/from16 v1, v24

    if-ne v0, v1, :cond_c

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_c
    const/16 v26, 0x3

    move/from16 v0, v26

    move/from16 v1, v24

    if-ne v0, v1, :cond_1

    const/4 v7, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "NotifyService --> onHandleIntent"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/NotifyService;->onSimStatusChange(Landroid/content/Intent;)V

    :goto_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "NotifyService --> onHandleIntent End"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "intent is null"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
