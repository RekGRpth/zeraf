.class public abstract Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;
.super Ljava/lang/Object;
.source "ContextBasedProxy.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

.field private mProxyName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    iget-object v1, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    const-string v0, "ContextBasedProxy"

    iput-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mProxyName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    return-object v0
.end method

.method public getProxyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mProxyName:Ljava/lang/String;

    return-object v0
.end method

.method public registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    invoke-virtual {v0, p1}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    iget-object v1, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->setInnerContentResolver(Landroid/content/ContentResolver;)V

    return-void
.end method

.method public setProxyName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mProxyName:Ljava/lang/String;

    return-void
.end method

.method public unregisterSelfChangeObserver()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->mOCR:Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->unregisterSelfChangeObserver()V

    return-void
.end method
