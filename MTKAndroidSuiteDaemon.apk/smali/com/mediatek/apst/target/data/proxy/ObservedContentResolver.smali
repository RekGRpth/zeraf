.class public Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;
.super Ljava/lang/Object;
.source "ObservedContentResolver.java"


# instance fields
.field private mCR:Landroid/content/ContentResolver;

.field private mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    return-void
.end method

.method private selfChangeDone()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    invoke-interface {v0}, Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;->onSelfChangeDone()V

    :cond_0
    return-void
.end method

.method private selfChangeStart()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    invoke-interface {v0}, Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;->onSelfChangeStart()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "mSelfChangeOb is null"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    return-object v1

    :catch_0
    move-exception v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # [Landroid/content/ContentValues;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    return v1

    :catch_0
    move-exception v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2, p3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    return v1

    :catch_0
    move-exception v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getSelfChangeObserver()Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    monitor-enter v3

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    monitor-exit v3

    :goto_2
    return-object v1

    :catch_0
    move-exception v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 v4, 0x1

    aput-object p2, v2, v4

    const/4 v4, 0x0

    invoke-static {v2, v4, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V

    :try_start_5
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v1

    :goto_3
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    goto :goto_2

    :catch_2
    move-exception v0

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p2, v2, v4

    invoke-static {v2, v6, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6
.end method

.method public registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    return-void
.end method

.method public setInnerContentResolver(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    return-void
.end method

.method public unregisterSelfChangeObserver()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mSelfChangeOb:Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeStart()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->mCR:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->selfChangeDone()V

    return v1

    :catch_0
    move-exception v0

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    aput-object p4, v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
