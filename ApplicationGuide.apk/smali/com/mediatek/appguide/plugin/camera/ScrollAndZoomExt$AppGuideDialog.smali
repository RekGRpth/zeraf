.class Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;
.super Landroid/app/Dialog;
.source "ScrollAndZoomExt.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppGuideDialog"
.end annotation


# static fields
.field private static final MULTI_SIM:I = 0x2

.field public static final SCROLL_IN_CAMERA:I = 0x0

.field public static final ZOOM_IN_CAMERA:I = 0x1


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCurrentStep:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNextListener:Landroid/view/View$OnClickListener;

.field private mOkListener:Landroid/view/View$OnClickListener;

.field private mOrientation:I

.field private mPreview:Landroid/view/SurfaceView;

.field private mRightBtn:Landroid/widget/Button;

.field private mSetScreen:Z

.field private mTitle:Landroid/widget/TextView;

.field private final mVideoArray:[Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field final synthetic this$0:Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;


# direct methods
.method public constructor <init>(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;Landroid/app/Activity;)V
    .locals 3
    .param p2    # Landroid/app/Activity;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->this$0:Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    const v0, 0x7f060002

    invoke-direct {p0, p2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput v2, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOrientation:I

    iput-boolean v2, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mSetScreen:Z

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "scroll_left_bar.mp4"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "zoom_in_and_out.mp4"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mVideoArray:[Ljava/lang/String;

    new-instance v0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$1;

    invoke-direct {v0, p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$1;-><init>(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)V

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mNextListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$2;

    invoke-direct {v0, p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$2;-><init>(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)V

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOkListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$3;

    invoke-direct {v0, p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog$3;-><init>(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)V

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iput-object p2, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)I
    .locals 1
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    return v0
.end method

.method static synthetic access$208(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)I
    .locals 2
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;I)V
    .locals 0
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->prepareVideo(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;)V
    .locals 0
    .param p0    # Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;

    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->releaseMediaPlayer()V

    return-void
.end method

.method private acquireCpuWakeLock()V
    .locals 3

    const-string v1, "ScrollAndZoomExt"

    const-string v2, "Acquiring cpu wake lock"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->this$0:Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    # getter for: Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->access$600(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x3000001a

    const-string v2, "AppGuide"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0
.end method

.method private prepareVideo(I)V
    .locals 8
    .param p1    # I

    const-string v0, "ScrollAndZoomExt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareVideo step = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->this$0:Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    # getter for: Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->access$600(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mVideoArray:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    const-string v0, "ScrollAndZoomExt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "video path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    invoke-virtual {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->resizeSurfaceView()V

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "mMediaPlayer prepare()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "ScrollAndZoomExt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to open file; error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->releaseMediaPlayer()V

    goto :goto_0

    :catch_1
    move-exception v7

    const-string v0, "ScrollAndZoomExt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "media player is in illegal state; error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->releaseMediaPlayer()V

    goto :goto_0
.end method

.method private releaseCpuLock()V
    .locals 2

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "releaseCpuLock()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method private releaseMediaPlayer()V
    .locals 2

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "releaseMediaPlayer"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->releaseCpuLock()V

    invoke-virtual {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "onCompletion called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mNextListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOkListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x400

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOrientation:I

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v1, v1, 0x400

    if-eq v1, v3, :cond_0

    const-string v1, "ScrollAndZoomExt"

    const-string v2, " fullscreen = false"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->this$0:Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;

    # getter for: Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;->access$600(Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/high16 v1, 0x7f030000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mView:Landroid/view/View;

    const v2, 0x7f070002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    const v2, 0x104000a

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mView:Landroid/view/View;

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    const-string v1, "ScrollAndZoomExt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCurrentStep = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mView:Landroid/view/View;

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mPreview:Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mPreview:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "onPrepared called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method

.method public resizeSurfaceView()V
    .locals 9

    const-string v7, "ScrollAndZoomExt"

    const-string v8, "resizeSurfaceView()"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mSetScreen:Z

    if-eqz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mSetScreen:Z

    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v6

    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v4

    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v3

    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v1

    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mPreview:Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    int-to-float v7, v4

    int-to-float v8, v6

    div-float v5, v7, v8

    int-to-float v7, v1

    int-to-float v8, v3

    div-float v2, v7, v8

    cmpl-float v7, v2, v5

    if-lez v7, :cond_1

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v7, v3

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const-string v7, "ScrollAndZoomExt"

    const-string v8, "screenScale > videoScale"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v7, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mPreview:Landroid/view/SurfaceView;

    invoke-virtual {v7, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v7, v1

    div-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const-string v7, "ScrollAndZoomExt"

    const-string v8, "screenScale < videoScale"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public restoreOrientation()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOrientation:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "surfaceChanged called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "surfaceCreated called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->restoreOrientation()V

    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->acquireCpuWakeLock()V

    iput-object p1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mTitle:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mRightBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    iget v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mCurrentStep:I

    invoke-direct {p0, v0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->prepareVideo(I)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "ScrollAndZoomExt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceDestroyed called, mOrientation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mHolder:Landroid/view/SurfaceHolder;

    if-eq p1, v0, :cond_1

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "surfaceholder != mHolder, return"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->restoreOrientation()V

    invoke-direct {p0}, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->releaseCpuLock()V

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/appguide/plugin/camera/ScrollAndZoomExt$AppGuideDialog;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    const-string v0, "ScrollAndZoomExt"

    const-string v1, "mMediaPlayer.pause()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
