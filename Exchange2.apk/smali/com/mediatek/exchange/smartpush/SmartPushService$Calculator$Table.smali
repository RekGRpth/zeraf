.class Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;
.super Ljava/lang/Object;
.source "SmartPushService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Table"
.end annotation


# static fields
.field private static final DURATION_COLUMN:I = 0x2

.field private static final MAIL_COLUMN:I = 0x1

.field private static final SCALE_NUM:I = 0xc

.field private static final TIME_COLUMN:I


# instance fields
.field private mResults:[F

.field private mSummaries:[[I


# direct methods
.method private constructor <init>()V
    .locals 3

    const/16 v2, 0xc

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    filled-new-array {v2, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mResults:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/exchange/smartpush/SmartPushService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/exchange/smartpush/SmartPushService$1;

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;-><init>()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->inputData(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;)V
    .locals 0
    .param p0    # Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->startCalculate()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;)[F
    .locals 1
    .param p0    # Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;

    iget-object v0, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mResults:[F

    return-object v0
.end method

.method private inputData(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;

    invoke-static {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;->access$800(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;)J

    move-result-wide v4

    const-wide/32 v6, 0x6ddd00

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result v3

    invoke-static {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;->access$900(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v4, v4, v3

    const/4 v5, 0x1

    aget v6, v4, v5

    int-to-long v6, v6

    invoke-static {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;->access$1000(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;)J

    move-result-wide v8

    add-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, v4, v5

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v4, v4, v3

    const/4 v5, 0x2

    aget v6, v4, v5

    int-to-long v6, v6

    invoke-static {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;->access$1000(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;)J

    move-result-wide v8

    add-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, v4, v5

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v4, v4, v3

    const/4 v5, 0x0

    aget v6, v4, v5

    int-to-long v6, v6

    invoke-static {v0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;->access$1000(Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$HabitData$TableData;)J

    move-result-wide v8

    add-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, v4, v5

    goto :goto_0

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private printResult()V
    .locals 5

    const-string v1, "SmartPushService"

    const-string v2, "------------------------------------\n"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    const-string v1, "SmartPushService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mResults:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SmartPushService"

    const-string v2, "\n"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startCalculate()V
    .locals 14

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v10, 0x0

    :goto_0
    const/16 v11, 0xc

    if-ge v10, v11, :cond_0

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x0

    aget v11, v11, v12

    add-int/2addr v7, v11

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x1

    aget v11, v11, v12

    add-int/2addr v6, v11

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x2

    aget v11, v11, v12

    add-int/2addr v5, v11

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_0
    if-nez v7, :cond_1

    if-nez v6, :cond_1

    if-nez v5, :cond_1

    const/4 v10, 0x0

    :goto_1
    const/16 v11, 0xc

    if-ge v10, v11, :cond_7

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mResults:[F

    const/4 v12, 0x0

    aput v12, v11, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    :goto_2
    const/16 v11, 0xc

    if-ge v10, v11, :cond_6

    if-nez v7, :cond_3

    const/4 v9, 0x0

    :goto_3
    if-nez v6, :cond_4

    const/4 v4, 0x0

    :goto_4
    if-nez v5, :cond_5

    const/4 v1, 0x0

    :goto_5
    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x0

    aget v11, v11, v12

    iget-object v12, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v12, v12, v10

    const/4 v13, 0x1

    aget v12, v12, v13

    add-int v8, v11, v12

    const/4 v0, 0x0

    if-eqz v8, :cond_2

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x0

    aget v11, v11, v12

    int-to-float v11, v11

    int-to-float v12, v8

    div-float v0, v11, v12

    :cond_2
    mul-float v2, v4, v0

    mul-float v3, v1, v0

    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mResults:[F

    const v12, 0x3f19999a

    mul-float/2addr v12, v9

    const v13, 0x3dcccccd

    mul-float/2addr v13, v2

    add-float/2addr v12, v13

    const v13, 0x3e99999a

    mul-float/2addr v13, v3

    add-float/2addr v12, v13

    aput v12, v11, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_3
    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x0

    aget v11, v11, v12

    int-to-float v11, v11

    int-to-float v12, v7

    div-float v9, v11, v12

    goto :goto_3

    :cond_4
    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x1

    aget v11, v11, v12

    int-to-float v11, v11

    int-to-float v12, v6

    div-float v4, v11, v12

    goto :goto_4

    :cond_5
    iget-object v11, p0, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->mSummaries:[[I

    aget-object v11, v11, v10

    const/4 v12, 0x2

    aget v11, v11, v12

    int-to-float v11, v11

    int-to-float v12, v5

    div-float v1, v11, v12

    goto :goto_5

    :cond_6
    invoke-direct {p0}, Lcom/mediatek/exchange/smartpush/SmartPushService$Calculator$Table;->printResult()V

    :cond_7
    return-void
.end method
