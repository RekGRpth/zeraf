.class public Lcom/android/emailcommon/service/EmailExternalConstants;
.super Ljava/lang/Object;
.source "EmailExternalConstants.java"


# static fields
.field public static final ACTION_BACKGROUND_SEND:Ljava/lang/String; = "com.android.email.action.BACKGROUND_SEND"

.field public static final ACTION_BACKGROUND_SEND_MULTIPLE:Ljava/lang/String; = "com.android.email.action.BACKGROUND_SEND_MULTIPLE"

.field public static final ACTION_DELIVER_RESULT:Ljava/lang/String; = "com.android.email.action.DELIVER_RESULT"

.field public static final ACTION_DIRECT_SEND:Ljava/lang/String; = "com.android.email.action.DIRECT_SEND"

.field public static final ACTION_SEND_RESULT:Ljava/lang/String; = "com.android.email.action.SEND_RESULT"

.field public static final ACTION_UPDATE_INBOX:Ljava/lang/String; = "com.android.email.action.UPDATE_INBOX"

.field public static final ACTION_UPDATE_INBOX_RESULT:Ljava/lang/String; = "com.android.email.action.UPDATE_INBOX_RESULT"

.field public static final EXTRA_ACCOUNT:Ljava/lang/String; = "com.android.email.extra.ACCOUNT"

.field public static final EXTRA_RESULT:Ljava/lang/String; = "com.android.email.extra.RESULT"

.field public static final OMACP_CAPABILITY_ACTION:Ljava/lang/String; = "com.mediatek.omacp.capability"

.field public static final OMACP_SETTING_ACTION:Ljava/lang/String; = "com.mediatek.omacp.settings"

.field public static final OMACP_SETTING_RESULT_ACTION:Ljava/lang/String; = "com.mediatek.omacp.settings.result"

.field public static final OMAPCP_CAPABILITY_RESULT_ACTION:Ljava/lang/String; = "com.mediatek.omacp.capability.result"

.field public static final RESULT_FAIL:I = 0x1

.field public static final RESULT_SUCCESS:I = 0x0

.field public static final TYPE_DELIVER:I = 0x2

.field public static final TYPE_SEND:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
