.class public final Lcom/android/exchange/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f05000e

.field public static final exception_cancel:I = 0x7f05000f

.field public static final exception_updated:I = 0x7f050010

.field public static final exchange_name:I = 0x7f050001

.field public static final exchange_name_alternate:I = 0x7f050002

.field public static final meeting_accepted:I = 0x7f050003

.field public static final meeting_allday:I = 0x7f05000b

.field public static final meeting_allday_recurring:I = 0x7f05000c

.field public static final meeting_canceled:I = 0x7f050006

.field public static final meeting_declined:I = 0x7f050004

.field public static final meeting_recurring:I = 0x7f05000a

.field public static final meeting_tentative:I = 0x7f050005

.field public static final meeting_updated:I = 0x7f050007

.field public static final meeting_when:I = 0x7f050008

.field public static final meeting_where:I = 0x7f050009

.field public static final notification_exchange_calendar_added:I = 0x7f05000d

.field public static final policy_app_blacklist:I = 0x7f05001e

.field public static final policy_app_whitelist:I = 0x7f05001f

.field public static final policy_bluetooth_restricted:I = 0x7f05001d

.field public static final policy_dont_allow_attachments:I = 0x7f050023

.field public static final policy_dont_allow_browser:I = 0x7f050019

.field public static final policy_dont_allow_consumer_email:I = 0x7f05001a

.field public static final policy_dont_allow_html:I = 0x7f050018

.field public static final policy_dont_allow_internet_sharing:I = 0x7f05001b

.field public static final policy_dont_allow_irda:I = 0x7f050017

.field public static final policy_dont_allow_pop_imap:I = 0x7f050016

.field public static final policy_dont_allow_storage_cards:I = 0x7f050011

.field public static final policy_dont_allow_text_messaging:I = 0x7f050015

.field public static final policy_dont_allow_unsigned_apps:I = 0x7f050012

.field public static final policy_dont_allow_unsigned_installers:I = 0x7f050013

.field public static final policy_dont_allow_wifi:I = 0x7f050014

.field public static final policy_html_truncation:I = 0x7f050021

.field public static final policy_max_attachment_size:I = 0x7f050024

.field public static final policy_require_encryption:I = 0x7f050026

.field public static final policy_require_manual_sync_roaming:I = 0x7f050025

.field public static final policy_require_sd_encryption:I = 0x7f050022

.field public static final policy_require_smime:I = 0x7f05001c

.field public static final policy_text_truncation:I = 0x7f050020

.field public static final wipe_all_messages:I = 0x7f050000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
