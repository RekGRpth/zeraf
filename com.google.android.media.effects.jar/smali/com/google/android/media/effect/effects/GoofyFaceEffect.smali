.class public Lcom/google/android/media/effect/effects/GoofyFaceEffect;
.super Landroid/media/effect/FilterGraphEffect;
.source "GoofyFaceEffect.java"


# static fields
.field private static final FACE_DETECTOR_MAX_SIZE:I = 0x140

.field private static final mGoofyGraph:Ljava/lang/String; = "@import android.filterpacks.base;\n@import android.filterpacks.imageproc;\n@import com.google.android.filterpacks.facedetect;\n\n\n@filter GLTextureSource srcTex {\n  texId = 0;\n  repeatFrame = true;\n}\n\n@filter FrameBranch branch1 {\n  outputs = 2;\n}\n\n@filter ToPackedGrayFilter toPackedGray {\n}\n\n@filter MultiFaceTrackerFilter faceTracker {\n  quality = 0.0f;\n  smoothness = 0.2f;\n  minEyeDist = 30.0f;\n  rollRange = 45.0f;\n  numSkipFrames = 9;\n  trackingError = 0.8;\n  mouthOnlySmoothing = 0;\n  useAffineCorrection = 1;\n  patchSize = 15;\n}\n\n@filter GoofyFastRenderFilter goofyRenderer {\n  currentEffect = 0;\n  distortionAmount = 1.0f;\n}\n\n@filter GLTextureTarget trgTex {\n  texId = 0;\n}\n\n@connect srcTex[frame]           => branch1[in];\n@connect branch1[out0]           => toPackedGray[image];\n@connect branch1[out1]           => goofyRenderer[image];\n@connect toPackedGray[image]     => faceTracker[image];\n@connect faceTracker[faces]      => goofyRenderer[faces];\n@connect goofyRenderer[outimage] => trgTex[frame];\n"


# instance fields
.field private mFirstTime:Z

.field private mInputHeight:I

.field private mInputWidth:I


# direct methods
.method public constructor <init>(Landroid/media/effect/EffectContext;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/media/effect/EffectContext;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    const-string v3, "@import android.filterpacks.base;\n@import android.filterpacks.imageproc;\n@import com.google.android.filterpacks.facedetect;\n\n\n@filter GLTextureSource srcTex {\n  texId = 0;\n  repeatFrame = true;\n}\n\n@filter FrameBranch branch1 {\n  outputs = 2;\n}\n\n@filter ToPackedGrayFilter toPackedGray {\n}\n\n@filter MultiFaceTrackerFilter faceTracker {\n  quality = 0.0f;\n  smoothness = 0.2f;\n  minEyeDist = 30.0f;\n  rollRange = 45.0f;\n  numSkipFrames = 9;\n  trackingError = 0.8;\n  mouthOnlySmoothing = 0;\n  useAffineCorrection = 1;\n  patchSize = 15;\n}\n\n@filter GoofyFastRenderFilter goofyRenderer {\n  currentEffect = 0;\n  distortionAmount = 1.0f;\n}\n\n@filter GLTextureTarget trgTex {\n  texId = 0;\n}\n\n@connect srcTex[frame]           => branch1[in];\n@connect branch1[out0]           => toPackedGray[image];\n@connect branch1[out1]           => goofyRenderer[image];\n@connect toPackedGray[image]     => faceTracker[image];\n@connect faceTracker[faces]      => goofyRenderer[faces];\n@connect goofyRenderer[outimage] => trgTex[frame];\n"

    const-string v4, "srcTex"

    const-string v5, "trgTex"

    const-class v6, Landroid/filterfw/core/OneShotScheduler;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Landroid/media/effect/FilterGraphEffect;-><init>(Landroid/media/effect/EffectContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mFirstTime:Z

    iput v7, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputWidth:I

    iput v7, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputHeight:I

    return-void
.end method


# virtual methods
.method public apply(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/16 v4, 0x140

    iget-boolean v3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mFirstTime:Z

    if-eqz v3, :cond_2

    if-le p2, p3, :cond_1

    invoke-static {v4, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int v3, v1, p3

    div-int v0, v3, p2

    :goto_0
    iget-object v3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    const-string v4, "toPackedGray"

    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    move-result-object v2

    const-string v3, "owidth"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "oheight"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mFirstTime:Z

    iput p2, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputWidth:I

    iput p3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputHeight:I

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/media/effect/FilterGraphEffect;->apply(IIII)V

    return-void

    :cond_1
    invoke-static {v4, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    mul-int v3, v0, p2

    div-int v1, v3, p3

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputWidth:I

    if-ne p2, v3, :cond_3

    iget v3, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mInputHeight:I

    if-eq p3, v3, :cond_0

    :cond_3
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "GoofyFaceEffect can\'t change input size!"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    const-string v1, "currentEffect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    const-string v2, "goofyRenderer"

    invoke-virtual {v1, v2}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    move-result-object v0

    const-string v1, "currentEffect"

    invoke-virtual {v0, v1, p2}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "strength"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/media/effect/effects/GoofyFaceEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    const-string v2, "goofyRenderer"

    invoke-virtual {v1, v2}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    move-result-object v0

    const-string v1, "distortionAmount"

    invoke-virtual {v0, v1, p2}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
