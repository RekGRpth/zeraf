.class public Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;
.super Landroid/filterfw/core/Filter;
.source "FaceMetaSmoothFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    }
.end annotation


# instance fields
.field private mLastPositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;",
            ">;"
        }
    .end annotation
.end field

.field final mMaximumGap:J

.field final mMaximumPredictionGap:J

.field final mSmoothingRate:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-wide/16 v1, 0x12c

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x3f000000

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mSmoothingRate:F

    iput-wide v1, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mMaximumGap:J

    iput-wide v1, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mMaximumPredictionGap:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    return-void
.end method

.method private getCurrentPositions(Lcom/google/android/filterpacks/facedetect/FaceMeta;)Ljava/util/Vector;
    .locals 18
    .param p1    # Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/filterpacks/facedetect/FaceMeta;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;",
            ">;"
        }
    .end annotation

    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_1

    new-instance v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;-><init>(Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX0(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY0(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX1(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY1(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipX(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipY(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipX(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipY(I)F

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getId(I)I

    move-result v14

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    iput-wide v11, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    const/4 v14, 0x0

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    const/4 v14, 0x0

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    iget v15, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    sub-float/2addr v14, v15

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    iget v0, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    add-float/2addr v14, v15

    const/high16 v15, 0x40000000

    div-float v2, v14, v15

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    iget v15, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    sub-float/2addr v14, v15

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    iget v0, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    add-float/2addr v14, v15

    const/high16 v15, 0x40000000

    div-float v3, v14, v15

    iget-wide v14, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    sub-long v14, v11, v14

    long-to-float v14, v14

    const/high16 v15, 0x447a0000

    div-float v1, v14, v15

    iget v14, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    div-float v15, v2, v1

    iget v0, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    const/high16 v16, 0x3f000000

    mul-float v15, v15, v16

    add-float/2addr v14, v15

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    iget v14, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    div-float v15, v3, v1

    iget v0, v13, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    const/high16 v16, 0x3f000000

    mul-float v15, v15, v16

    add-float/2addr v14, v15

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-virtual {v10, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v15, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    iget-wide v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    const-wide/16 v16, 0x12c

    sub-long v16, v11, v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_3

    iget v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-wide v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    cmp-long v14, v14, v11

    if-eqz v14, :cond_2

    iget-wide v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    const-wide/16 v16, 0x12c

    sub-long v16, v11, v16

    cmp-long v14, v14, v16

    if-lez v14, :cond_2

    invoke-virtual {v4}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->clone()Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    move-result-object v8

    iget v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    const/high16 v15, 0x447a0000

    div-float/2addr v14, v15

    iget-wide v15, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    sub-long v15, v11, v15

    long-to-float v15, v15

    mul-float v2, v14, v15

    iget v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    const/high16 v15, 0x447a0000

    div-float/2addr v14, v15

    iget-wide v15, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    sub-long v15, v11, v15

    long-to-float v15, v15

    mul-float v3, v14, v15

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    add-float/2addr v14, v2

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    iget v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    add-float/2addr v14, v3

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    iget v14, v4, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    iput v14, v8, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-virtual {v10, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v9}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    invoke-virtual {v14, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    return-object v10
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 12
    .param p1    # Landroid/filterfw/core/FilterContext;

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v2

    const-string v9, "faces"

    invoke-virtual {p0, v9}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v1

    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    invoke-direct {p0, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->getCurrentPositions(Lcom/google/android/filterpacks/facedetect/FaceMeta;)Ljava/util/Vector;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v4, v9, [Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    invoke-virtual {v8, v4}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-class v9, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    array-length v10, v4

    const/4 v11, 0x2

    invoke-static {v9, v10, v11}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v6

    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v9

    if-ge v3, v9, :cond_0

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setId(II)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX0(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY0(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX1(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY1(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeX(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeY(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeX(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeY(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthX(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthY(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipX(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipY(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipX(IF)V

    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipY(IF)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const-string v9, "faces"

    invoke-virtual {p0, v9, v6}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    return-void
.end method

.method public setupPorts()V
    .locals 3

    const-class v1, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    return-void
.end method
