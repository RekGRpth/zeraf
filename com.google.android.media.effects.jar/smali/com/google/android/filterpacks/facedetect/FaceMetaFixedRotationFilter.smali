.class public Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;
.super Landroid/filterfw/core/Filter;
.source "FaceMetaFixedRotationFilter.java"


# instance fields
.field private mRotation:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "rotation"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    return-void
.end method

.method private RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # F

    const/high16 v2, 0x3f800000

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Landroid/filterfw/geometry/Point;

    invoke-direct {v0, p2, p3}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Landroid/filterfw/geometry/Point;

    sub-float v1, v2, p3

    invoke-direct {v0, v1, p2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/filterfw/geometry/Point;

    sub-float v1, v2, p2

    sub-float/2addr v2, p3

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/filterfw/geometry/Point;

    sub-float v1, v2, p2

    invoke-direct {v0, p3, v1}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 11
    .param p1    # Landroid/filterfw/core/FilterContext;

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v2

    const-string v8, "faces"

    invoke-virtual {p0, v8}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v1

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    if-nez v8, :cond_0

    const-string v8, "faces"

    invoke-virtual {p0, v8, v1}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const-class v8, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    invoke-virtual {v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v9

    const/4 v10, 0x2

    invoke-static {v8, v9, v10}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v5

    invoke-virtual {v5}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v8

    if-ge v3, v8, :cond_1

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getId(I)I

    move-result v8

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setId(II)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX0(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY0(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX0(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY0(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX1(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY1(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX1(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY1(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeX(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeY(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeX(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeY(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthX(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthY(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipX(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipY(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipX(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipY(IF)V

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->mRotation:I

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipX(I)F

    move-result v9

    invoke-virtual {v0, v3}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipY(I)F

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->RotatePointInFrame(IFF)Landroid/filterfw/geometry/Point;

    move-result-object v7

    iget v8, v7, Landroid/filterfw/geometry/Point;->x:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipX(IF)V

    iget v8, v7, Landroid/filterfw/geometry/Point;->y:F

    invoke-virtual {v4, v3, v8}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipY(IF)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    const-string v8, "faces"

    invoke-virtual {p0, v8, v5}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v5}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    goto/16 :goto_0
.end method

.method public setupPorts()V
    .locals 3

    const-class v1, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaFixedRotationFilter;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    return-void
.end method
