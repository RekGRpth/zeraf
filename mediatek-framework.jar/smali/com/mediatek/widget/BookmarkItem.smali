.class public Lcom/mediatek/widget/BookmarkItem;
.super Ljava/lang/Object;
.source "BookmarkItem.java"


# instance fields
.field mContent:Landroid/graphics/Bitmap;

.field mInfo:Ljava/lang/String;

.field mResId:I

.field mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/widget/BookmarkItem;->mResId:I

    iput p1, p0, Lcom/mediatek/widget/BookmarkItem;->mResId:I

    iput-object p2, p0, Lcom/mediatek/widget/BookmarkItem;->mContent:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/mediatek/widget/BookmarkItem;->mTitle:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/widget/BookmarkItem;->mInfo:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/mediatek/widget/BookmarkItem;-><init>(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/mediatek/widget/BookmarkItem;-><init>(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/widget/BookmarkItem;)V
    .locals 4
    .param p1    # Lcom/mediatek/widget/BookmarkItem;

    iget v0, p1, Lcom/mediatek/widget/BookmarkItem;->mResId:I

    iget-object v1, p1, Lcom/mediatek/widget/BookmarkItem;->mContent:Landroid/graphics/Bitmap;

    iget-object v2, p1, Lcom/mediatek/widget/BookmarkItem;->mTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/widget/BookmarkItem;->mInfo:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/mediatek/widget/BookmarkItem;-><init>(ILandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getContentBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/widget/BookmarkItem;->mContent:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getInfoString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/widget/BookmarkItem;->mInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/widget/BookmarkItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setBitmapResource(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/widget/BookmarkItem;->mResId:I

    return-void
.end method

.method public setContentBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/widget/BookmarkItem;->mContent:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setInfoString(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/widget/BookmarkItem;->mInfo:Ljava/lang/String;

    return-void
.end method

.method public setTitleString(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/widget/BookmarkItem;->mTitle:Ljava/lang/String;

    return-void
.end method
