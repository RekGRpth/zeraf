.class public Lcom/mediatek/msglogger/MessageLoggerWrapper;
.super Lcom/mediatek/common/msgmonitorservice/IMessageLoggerWrapper$Stub;
.source "MessageLoggerWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MessageLoggerWrapper"

.field protected static mMsgLogger:Landroid/os/MessageMonitorLogger;


# direct methods
.method public constructor <init>(Landroid/os/MessageMonitorLogger;)V
    .locals 0
    .param p1    # Landroid/os/MessageMonitorLogger;

    invoke-direct {p0}, Lcom/mediatek/common/msgmonitorservice/IMessageLoggerWrapper$Stub;-><init>()V

    sput-object p1, Lcom/mediatek/msglogger/MessageLoggerWrapper;->mMsgLogger:Landroid/os/MessageMonitorLogger;

    return-void
.end method


# virtual methods
.method public dumpAllMessageHistory()V
    .locals 1

    sget-object v0, Lcom/mediatek/msglogger/MessageLoggerWrapper;->mMsgLogger:Landroid/os/MessageMonitorLogger;

    invoke-virtual {v0}, Landroid/os/MessageMonitorLogger;->dumpAllMessageHistory()V

    return-void
.end method

.method public dumpMSGHistorybyName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/mediatek/msglogger/MessageLoggerWrapper;->mMsgLogger:Landroid/os/MessageMonitorLogger;

    invoke-virtual {v0, p1}, Landroid/os/MessageMonitorLogger;->dumpMessageHistory(Ljava/lang/String;)V

    return-void
.end method

.method public unregisterMsgLogger(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/mediatek/msglogger/MessageLoggerWrapper;->mMsgLogger:Landroid/os/MessageMonitorLogger;

    invoke-virtual {v0, p1}, Landroid/os/MessageMonitorLogger;->unregisterMsgLogger(Ljava/lang/String;)V

    return-void
.end method
